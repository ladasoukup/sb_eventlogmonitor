<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

function sessie_regenerate_id() {
	$randlen = 32;
	$randval = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$random = "";
	for ($i = 1; $i <= $randlen; $i++) {
		$random .= substr($randval, rand(0,(strlen($randval) - 1)), 1);
	}
	if (session_id(md5($random))) {
		return true;
	} else {
		return false;
	}
}

$_SESSION["evtlog_user"] = md5(uniqid());
$_SESSION["evtlog_pass"] = md5(uniqid());
$_SESSION["evtlog_user_group"] = "";
session_destroy();
sessie_regenerate_id();
setcookie(session_name(), session_id(), ini_get("session.cookie_lifetime"), "/");

$smarty->assign("version", "");
$template_file = "!login.tpl";
?>