<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
if (!isset($_GET["id"])) $_GET["id"] = "";

$query = $safesql->query("SELECT * FROM ".DB_PREFIX."filters[ WHERE id=%I]", array($_GET["id"]));
$query_data = $db->get_results($query, ARRAY_A);

if (sizeof($query_data) > 1) {
	$file_name = "SB_EventLog_Monitor-FILETRS-BACKUP-" . date("Ymd_His") . ".xml";
} else {
	$file_name = "SB_EventLog_Monitor-FILTER-" . rawurlencode($query_data[0]["filter_title"]) . ".xml";
}

$xml_data = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
$xml_data .= "<event_filters>\n";
if (is_array($query_data)) {
	foreach($query_data as $data) {
		$xml_data .= "	<filter>\n";
		$xml_data .= "		<filter_title>". $data["filter_title"] ."</filter_title>\n";
		$xml_data .= "		<filter_data>". $data["filter_data"] ."</filter_data>\n";
		$xml_data .= "		<filter_isNoise>". $data["filter_isNoise"] ."</filter_isNoise>\n";
		$xml_data .= "		<filter_isAlert>". $data["filter_isAlert"] ."</filter_isAlert>\n";
		$xml_data .= "		<filter_email_ignore>". $data["filter_email_ignore"] ."</filter_email_ignore>\n";
		$xml_data .= "		<filter_priority>". $data["filter_priority"] ."</filter_priority>\n";
		$xml_data .= "	</filter>\n";
	}
}
$xml_data .= "</event_filters>\n";

header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=\"" . $file_name . "\"");
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: private");
echo $xml_data;
die();
?>