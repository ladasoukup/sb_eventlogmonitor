<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if ((empty($_GET["f_evt_error"])) && (empty($_GET["f_evt_warning"])) && (empty($_GET["f_evt_audit_failure"])) && (empty($_GET["f_evt_audit_success"])) && (empty($_GET["f_evt_information"]))) {
	$_GET["f_evt_error"] = "error"; $_GET["f_evt_warning"] = "warning"; $_GET["f_evt_audit_failure"] = "audit_failure"; $_GET["f_evt_audit_success"] = "audit_success"; $_GET["f_evt_information"] = "information";
}
$colors = array();
$data_groups = array(); $data_groups_txt = array();
if (!empty($_GET["f_evt_error"])) { $data_groups[] = "error"; $data_groups_txt[] = "##Error##"; $colors[] = "0xFF7C7C"; }
if (!empty($_GET["f_evt_warning"])) { $data_groups[] = "warning"; $data_groups_txt[] = "##Warning##"; $colors[] = "0xECFF6B"; }
if (!empty($_GET["f_evt_information"])) { $data_groups[] = "information"; $data_groups_txt[] = "##Information##"; $colors[] = "0xFFFFFF"; }
if (!empty($_GET["f_evt_audit_success"])) { $data_groups[] = "audit_success"; $data_groups_txt[] = "##Audit success##"; $colors[] = "0xC1FFD1"; }
if (!empty($_GET["f_evt_audit_failure"])) { $data_groups[] = "audit_failure"; $data_groups_txt[] = "##Audit failure##"; $colors[] = "0xFCDE9F"; }

$loop_max = sizeof($data_groups);
for($hours=$time_total_steps; $hours>=0; $hours-=$time_step) {
	$curr_data = array();
	$smarty->assign("grid_loops", ($time_total_steps / ($time_step * 2)));
	for($loop=0; $loop<$loop_max; $loop++) {
		$time_from = $start_timestamp - (3600 * $hours);
		$time_to = $start_timestamp - (3600 * ($hours - $time_step));
		$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='%s' AND evt_time_generated BETWEEN '%s' AND '%s'[ AND evt_computer In (%Q)][ AND evt_computer='%S']", array($data_groups[$loop], date("Y-m-d H:i:s", $time_from), date("Y-m-d H:i:s", $time_to), $computers_in_group, $_GET["evt_computer"]));
		$value = $db->get_var($query);
		if ($value > $max_value) $max_value = $value;
		$url = "index.php?module=showEventLog&amp;sub=".$_GET["evt_computer"]."&amp;sub_group=".$_GET["sub_group"]."&amp;f_evt_".$data_groups[$loop]."=".$data_groups[$loop]."&amp;f_date_from=".date("Y-m-d H:i:s", $time_from)."&amp;f_date_to=".date("Y-m-d H:i:s", $time_to);
		$curr_data[] = array("value" => $value, "name" => $data_groups_txt[$loop], "color" => $colors[$loop], "url" => $url);
	}
	$curr_array = array("name" => date("d.m. H:i", $time_from) . " - " . date("H:i", $time_to), "data" => $curr_data);
	$chart_data[] = $curr_array;
}
?>