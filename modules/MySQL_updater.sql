CREATE TABLE sbevtlog_alerts (
  id int(11) NOT NULL auto_increment,
  evt_computer varchar(128) default NULL,
  evt_code int(9) default NULL,
  evt_type varchar(32) default NULL,
  evt_category varchar(255) default NULL,
  evt_logfile varchar(128) default NULL,
  evt_message text,
  evt_source varchar(128) default NULL,
  evt_user varchar(128) default NULL,
  evt_time_generated datetime default NULL,
  evt_noise tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (id),
  KEY idx_summary (evt_type(8),evt_time_generated,evt_noise,evt_computer(32),id),
  KEY idx_view (evt_computer(32),evt_type(8),evt_noise,evt_time_generated),
  KEY idx_view2 (evt_computer(32),evt_type(8),evt_source(16),evt_category(8),evt_time_generated,evt_code,evt_noise),
  KEY idx_alerts_com (evt_time_generated,evt_computer(64))
);

CREATE TABLE sbevtlog_autocomplete (
  ac_field varchar(32) NOT NULL default '',
  ac_values text,
  PRIMARY KEY  (ac_field)
);

CREATE TABLE sbevtlog_cfg (
  id int(11) NOT NULL auto_increment,
  cfg_var varchar(255) default NULL,
  cfg_val varchar(255) default NULL,
  cfg_group varchar(255) default NULL,
  cfg_desc varchar(255) default NULL,
  PRIMARY KEY  (id),
  KEY idx_var (cfg_var(32))
);

CREATE TABLE sbevtlog_computers (
  id int(11) NOT NULL auto_increment,
  computer_name varchar(255) default NULL,
  computer_domain varchar(255) default NULL,
  computer_description varchar(255) default NULL,
  computer_group varchar(128) default NULL,
  computer_os varchar(255) default NULL,
  computer_harvest_interval int(9) NOT NULL default '3600',
  computer_enable tinyint(4) default '0',
  computer_monitored_by varchar(255) default NULL,
  computer_next_harvest datetime NOT NULL default '0000-00-00 00:00:00',
  computer_last_reported datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (id),
  KEY idx_computer_name (computer_name(32),id)
);

CREATE TABLE sbevtlog_cron (
  id int(11) unsigned NOT NULL auto_increment,
  cron_title char(128) NOT NULL default '',
  cron_time datetime default '0000-00-00 00:00:00',
  cron_repeat int(10) unsigned default NULL,
  cron_module char(255) default NULL,
  cron_params char(255) default NULL,
  cron_priority tinyint(3) unsigned default '50',
  cron_enable tinyint(1) unsigned default '0',
  PRIMARY KEY  (id),
  KEY idx_cron (cron_enable,cron_time,cron_priority),
  KEY idx_cron_mod (cron_module(32))
);

CREATE TABLE sbevtlog_events (
  id int(11) NOT NULL auto_increment,
  evt_computer varchar(128) default NULL,
  evt_code int(9) default NULL,
  evt_type varchar(32) default NULL,
  evt_category varchar(255) default NULL,
  evt_logfile varchar(128) default NULL,
  evt_message text,
  evt_source varchar(128) default NULL,
  evt_user varchar(128) default NULL,
  evt_time_generated datetime default NULL,
  evt_noise tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (id),
  KEY idx_summary (evt_type(8),evt_time_generated,evt_noise,evt_computer(32),id),
  KEY idx_view (evt_computer(32),evt_type(8),evt_noise,evt_time_generated),
  KEY idx_view2 (evt_computer(32),evt_type(8),evt_source(16),evt_category(8),evt_time_generated,evt_code,evt_noise),
  KEY evt_comp (evt_computer)
);

CREATE TABLE sbevtlog_filters (
  id int(11) NOT NULL auto_increment,
  filter_title varchar(255) default NULL,
  filter_data text NOT NULL,
  filter_datum datetime default NULL,
  filter_isNoise tinyint(4) NOT NULL default '0',
  filter_isAlert tinyint(4) NOT NULL default '0',
  filter_isEmail tinyint(4) NOT NULL default '0',
  filter_mailto text,
  filter_email_ignore smallint(4) default '0',
  filter_active tinyint(4) NOT NULL default '0',
  filter_priority smallint(4) default '250',
  PRIMARY KEY  (id),
  KEY filter_is (filter_isNoise,filter_isAlert,filter_isEmail),
  KEY filter_active (filter_active)
);

CREATE TABLE sbevtlog_groups_access (
  id int(11) NOT NULL auto_increment,
  groups_computer varchar(255) NOT NULL default '',
  groups_user varchar(255) NOT NULL default '',
  access_level tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (id),
  KEY idx_uac (groups_computer(32),groups_user(32),access_level)
);

CREATE TABLE sbevtlog_groups_computer (
  id int(11) NOT NULL auto_increment,
  id_sub int(11) default '0',
  computer_group varchar(255) default NULL,
  computer_group_desc varchar(255) default NULL,
  PRIMARY KEY  (id),
  KEY idx_computer (id,id_sub,computer_group)
);

CREATE TABLE sbevtlog_groups_user (
  id int(11) NOT NULL auto_increment,
  id_sub int(11) default NULL,
  user_group varchar(255) default NULL,
  user_group_desc varchar(255) default NULL,
  access_summary tinyint(4) default '0',
  access_events tinyint(4) default '0',
  access_alerts tinyint(4) default '0',
  access_settings tinyint(4) default '0',
  PRIMARY KEY  (id),
  KEY idx_user_group (user_group(1),access_summary,access_events,access_alerts,access_settings)
);

CREATE TABLE sbevtlog_queue_alert (
  id int(11) NOT NULL auto_increment,
  filter_id int(11) NOT NULL default '0',
  filter_title varchar(255) default NULL,
  evt_computer varchar(255) default NULL,
  queue_mail_from varchar(255) default NULL,
  queue_mailto tinytext,
  queue_email text,
  queue_mail_title varchar(255) default NULL,
  queue_email_count int(11) default NULL,
  queue_wait_to datetime default NULL,
  PRIMARY KEY  (id),
  KEY key_check (filter_id,evt_computer),
  KEY key_send (queue_wait_to)
);

CREATE TABLE sbevtlog_queue_mail (
  id int(11) NOT NULL auto_increment,
  mail_date datetime default NULL,
  mail_priority tinyint(4) unsigned default '50',
  mail_from varchar(255) default NULL,
  mail_to varchar(255) default NULL,
  mail_subject varchar(255) default NULL,
  mail_text text,
  error_count smallint(5) unsigned default '0',
  PRIMARY KEY  (id),
  KEY idx_error (error_count),
  KEY idx_maildate (mail_date)
);

CREATE TABLE sbevtlog_secrets (
  ip_addr char(32) NOT NULL default '',
  hostname char(255) default NULL,
  agent_version char(16) default NULL,
  secret char(15) NOT NULL default '',
  secret_expire datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (ip_addr),
  KEY key_expire (secret_expire)
);
