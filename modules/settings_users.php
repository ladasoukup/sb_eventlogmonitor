<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
$pass_file = "./.htpasswd";

if (!empty($_POST)) {
	//print_r($_POST); echo "<hr />";
	// EDIT or DELETE
	$newPASSfile = "";
	for($loop=0; $loop<sizeof($_POST["user"]); $loop++){
		if (!empty($_POST["pass_n1"][$loop])) {
			// PASSWORD change
			if ($_POST["pass_n1"][$loop] == $_POST["pass_n2"][$loop]) {
				$_POST["pass"][$loop] = crypt($_POST["pass_n1"][$loop], CRYPT_STD_DES);
			} else {
				// PWD not match!
			}
		}
		if ((isset($_POST["user_delete"][$loop])) && ($_POST["user_delete"][$loop] == "YES")) {
			
		} else {
			$newPASSfile .= $_POST["user"][$loop] . ":" . $_POST["pass"][$loop] . ":" . $_POST["user_group"][$loop] . ":" . $_POST["email"][$loop] . "\n";
		}
	}
	
	if (!empty($_POST["new_user"])) {
		// ADD NEW user
		if ((!empty($_POST["new_pass_n1"])) && ($_POST["new_pass_n1"] == $_POST["new_pass_n2"])) {
			$newPASSfile .= $_POST["new_user"] . ":" . crypt($_POST["new_pass_n1"], CRYPT_STD_DES) . ":" . $_POST["new_user_group"] . ":" . $_POST["new_email"] . "\n";
		} else {
			// PWD not match!
			$newPASSfile .= $_POST["new_user"] . ":-INCORECT PASSWORD-" . ":" . $_POST["new_user_group"] . ":" . $_POST["new_email"] . "\n";
		}
	}
	
	// STORE NEW PWD file
	file_put_contents($pass_file, $newPASSfile);
	//echo "<hr />"; echo nl2br($newPASSfile);
	$template_file = "settings_edit_ok.tpl";
} else {
	$pass_RAW = file($pass_file);
	if (is_array($pass_RAW)) {
		foreach($pass_RAW as $line) {
			@list($user, $pass, $group, $email) = explode(":", trim($line));
			$users[] = array("user" => $user, "pass" => $pass, "user_group" => $group, "email" => $email);
		}
	}
	$smarty->assign("users", $users);
	$smarty->assign("user_group", $this->GetGroupsUser(true));
}