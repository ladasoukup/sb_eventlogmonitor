<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
// Last alerts
$query = "SELECT DISTINCT evt_computer FROM ".DB_PREFIX."alerts ORDER BY evt_time_generated DESC LIMIT 10";
$last_alerts_computers = $db->get_col($query, 0);
if (is_array($last_alerts_computers)) {
	foreach($last_alerts_computers as $evt_computer) {
		$query = $safesql->query("SELECT * FROM ".DB_PREFIX."alerts WHERE evt_computer='%s' ORDER BY evt_time_generated DESC LIMIT 1", array($evt_computer));
		$last_alerts[] = $db->get_row($query, ARRAY_A);
	}
}
$rss_server_url = $this->auto_home_url();

$smarty->assign("last_alerts", $last_alerts);
$smarty->assign("rss_date", date("Y-m-d H:i:s"));
$smarty->assign("rss_server_url", $rss_server_url);

?>