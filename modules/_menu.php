<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
$mainmenu = array(
	array("title" => "##Summary##", "module" => "showSummary"),
	array("title" => "##Reports##", "module" => "reports"),
	array("title" => "---", "module" => ""),
	array("title" => "##EventLog##", "module" => "showEventLog"),
	array("title" => "##Alerts##", "module" => "showAlerts"),
	array("title" => "---", "module" => ""),
	array("title" => "##Settings##", "module" => "settings"),
	array("title" => "##About##", "module" => "About")
);
$smarty->assign("mainmenu", $mainmenu);

// AutoRefresh
if ($this->is_AutoRefresh()) {
	$autoRefresh = array(
		array("title" => "##Never##", "value" => "0"),
		array("title" => "##Every 1 minute##", "value" => "60"),
		array("title" => "##Every 5 minutes##", "value" => "300"),
		array("title" => "##Every 10 minutes##", "value" => "600"),
		array("title" => "##Every 15 minutes##", "value" => "900"),
		array("title" => "##Every 30 minutes##", "value" => "1800")
	);
	$smarty->assign("autoRefresh", $autoRefresh);
}
?>
