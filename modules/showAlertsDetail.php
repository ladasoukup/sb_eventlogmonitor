<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

$del_cache = false;
if ($_GET["action"] == 'repaired') {
	$query = $safesql->query("UPDATE ".DB_PREFIX."alerts SET evt_noise='1' WHERE id=%i", array($_GET["id"]));
	$db->query($query);
	$del_cache = true;
}
if ($_GET["action"] == 'notrepaired') {
	$query = $safesql->query("UPDATE ".DB_PREFIX."alerts SET evt_noise='0' WHERE id=%i", array($_GET["id"]));
	$db->query($query);
	$del_cache = true;
}

if ($del_cache === true) {
	$smarty->clear_cache(null, "showSummary");
	$smarty->clear_cache(null, "showAlerts");
	$smarty->clear_cache(null, "showAlertsDetail");
	$smarty->assign("reload_parent", "True");
}

$query = $safesql->query("SELECT * FROM ".DB_PREFIX."alerts WHERE id=%i", array($_GET["id"]));
$smarty->assign("event_data", $db->get_row($query, ARRAY_A));
?>