<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
set_time_limit(0);
global $core, $template_file;
if (!isset($_GET["step"])) $_GET["step"] = 1;

if (($_GET["step"] == 1) || empty($_GET["step"])) {
	$smarty->clear_all_cache();
	$template_file = "MySQL_updater_1.tpl";
}

if ($_GET["step"] == 2) {

	include_once("./modules/MySQL_updater_functions.php");
	
	$mysql_schema = file_get_contents("./modules/MySQL_updater.sql");
	$mysql_schema = str_replace("TABLE sbevtlog_", "TABLE " . DB_PREFIX, $mysql_schema);
	//$this->Debug(nl2br($mysql_schema));
	
	if ($this->debug_on === true) { /* $db->debug_all = true; */ }
	
	ob_start();
	make_db_current($mysql_schema);
	$smarty->assign("MySQL_updater_log", ob_get_contents());
	include("./modules_cron/db_optimize.php");
	ob_end_clean();
	$template_file = "MySQL_updater_2.tpl";
}

if ($_GET["step"] == 3) {
	
	$xml_path = "./modules_cron/_modules_cron.xml";
	if (file_exists($xml_path)) {
		$xml_data = simplexml_load_file($xml_path);
		foreach ($xml_data->module as $xml_line){
			$xml_line = $this->object2array($xml_line);
			
			$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."cron WHERE cron_module='%s'", array($xml_line["cron_module"]));
			$ret = $db->get_var($query);
			if ($ret != 1) {
				$query = $safesql->query("DELETE FROM ".DB_PREFIX."cron WHERE cron_module='%s'", array($xml_line["cron_module"]));
				$db->query($query);
				
				$query_data = array($xml_line["cron_title"], $xml_line["cron_repeat"],$xml_line["cron_module"],$xml_line["cron_params"],$xml_line["cron_priority"],$xml_line["cron_enable"]);
				$query = $safesql->query("INSERT INTO ".DB_PREFIX."cron SET cron_title='%s', cron_time='0000-00-00 00:00:00', cron_repeat='%s', cron_module='%s', cron_params='%s', cron_priority='%s', cron_enable='%s'", $query_data);
				$db->query($query);
			}
		}
	} else {
		trigger_error("File \"settings_global.xml\" is missing!", E_USER_ERROR);
		die();
	}
	$template_file = "MySQL_updater_3.tpl";
}

if ($_GET["step"] == 4) {
	
	$xml_path = "./modules/settings_global.xml";
	if (file_exists($xml_path)) {
		$xml_data = simplexml_load_file($xml_path);
		foreach ($xml_data->cfg as $xml_line){
			$xml_line = $this->object2array($xml_line);
			
			if (!empty($xml_line["cfg_val"])) {
				$temp = $core->GetConfig($xml_line["cfg_var"]);
				if (empty($temp)) {
					$core->PutConfig($xml_line["cfg_var"], $xml_line["cfg_val"]);
				}
			}
		}
	} else {
		trigger_error("File \"settings_global.xml\" is missing!", E_USER_ERROR);
		die();
	}
	
	$smarty->clear_all_cache();
	$core->PutConfig("app_version", $core->version);
	$template_file = "MySQL_updater_4.tpl";
}
?>