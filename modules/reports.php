<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

// SUBMENU
$submenu[] = array("sub" => "groups", "title" => "##Report by group##");
$submenu[] = array("sub" => "computers", "title" => "##report by computer##");
$submenu[] = array("sub" => "evt_type", "title" => "##Report by Event type##");
$submenu[] = array("sub" => "user", "title" => "##Report by User##");
$smarty->assign("submenu_title", "##Reports##");
$smarty->assign("submenu", $submenu);

// computers & groups
$all_groups = $this->GetGroups(true);
$all_computers = $this->GetComputers($_GET["sub_group"], true);
$smarty->assign("all_groups", $all_groups);
$smarty->assign("all_computers", $all_computers);

if (!empty($_GET["sub_group"])) {
	$computers_in_group = $this->GetComputers($_GET["sub_group"], false);
} else {
	$computers_in_group = "";
}

for ($loop=-1; $loop<7; $loop++) {
	$time_stamp[$loop] = mktime(0, 0, 0, date("m"), date("d"), date("Y")) - ($loop * 86400);
}

$reports_time_periods = array("##last 12 hours##", "##last 24 hours##", "##today##  (".date("j. ##F##", $time_stamp[0]).")", "##yesterday## (".date("j. ##F##", $time_stamp[1]).")" , "##two days ago## (".date("j. ##F##", $time_stamp[2]).")", "##three days ago## (".date("j. ##F##", $time_stamp[3]).")", "##four days ago## (".date("j. ##F##", $time_stamp[4]).")", "##five days ago## (".date("j. ##F##", $time_stamp[5]).")", "##last week##");
$smarty->assign("reports_time_periods", $reports_time_periods);

switch ($_GET["time_period"]) {
	case 0:
		$start_timestamp = mktime(date("H"), 0, 0, date("m"), date("d"), date("Y"));
		$time_total_steps = 12;
		$time_step = 0.5;
		break;
	case 1:
		$start_timestamp = mktime(date("H"), 0, 0, date("m"), date("d"), date("Y"));
		$time_total_steps = 24;
		$time_step = 1;
		break;
	case 2:
		$start_timestamp = $time_stamp[-1];
		$time_total_steps = 24;
		$time_step = 1;
		break;
	case 3:
		$start_timestamp = $time_stamp[0];
		$time_total_steps = 24;
		$time_step = 1;
		break;
	case 4;
		$start_timestamp = $time_stamp[1];
		$time_total_steps = 24;
		$time_step = 2;
		break;
	case 5:
		$start_timestamp = $time_stamp[2];
		$time_total_steps = 24;
		$time_step = 2;
		break;
	case 6:
		$start_timestamp = $time_stamp[3];
		$time_total_steps = 24;
		$time_step = 2;
		break;
	case 7:
		$start_timestamp = $time_stamp[4];
		$time_total_steps = 24;
		$time_step = 2;
		break;
	case 8:
		$start_timestamp = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
		$time_total_steps = 168;
		$time_step = 12;
		break;
	default: 
		$start_timestamp = mktime(date("H"), 0, 0, date("m"), date("d"), date("Y"));
		$time_total_steps = 24;
		$time_step = 1;
}
$max_value = 0;

// Insert data-prepare module
if(!empty($_GET["sub"])) {
	$data_module = "./modules/reports_" . $_GET["sub"] . ".php";
	if (file_exists($data_module)) {
		$template_file = "reports_" . $_GET["sub"] . ".tpl";
		include $data_module;
	} else {
		// todo...
	}
}

// create graph
if (!empty($_GET["sub"])) {
	if ($max_value > 100) $smarty->assign("graph_y_grid_step", round($max_value / 10, -1));
	if ($max_value == 0) $chart_data = array();
	$this->charts_generate_xml($chart_data, $_GET["sub"], "reports");
}

if ($group_access_denied === true) $template_file = "_error_group_access_denied.tpl";
?>