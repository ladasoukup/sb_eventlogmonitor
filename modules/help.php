<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
global $langID;
if (!isset($_GET["hlp_main"])) $_GET["hlp_main"] = "";
if (!isset($_GET["hlp_sub"])) $_GET["hlp_sub"] = "";

// try to load HELP
$hlp_base_path = "./help/" . $langID . "/help-";
$hlp_default_base_path = "./help/en/help-";
$hlp_default = $hlp_base_path . "!error.tpl";

if (!$smarty->template_exists($hlp_default)) {
	$hlp_default = $hlp_default_base_path . "!error.tpl";
}

if (!empty($_GET["hlp_sub"])) { $hlp_sub_page = "-" . $_GET["hlp_sub"]; } else { $hlp_sub_page = ""; }
$hlp_file = $hlp_base_path . $_GET["hlp_main"] . $hlp_sub_page . ".tpl";


if ($smarty->template_exists($hlp_file)) {
	$smarty->assign("help_template", $hlp_file);
} else {
	$hlp_file = $hlp_default_base_path . $_GET["hlp_main"] . $hlp_sub_page . ".tpl";
	if ($smarty->template_exists($hlp_file)) {
		$smarty->assign("help_template", $hlp_file);
	} else {
		$smarty->assign("help_template", $hlp_default);
	}
}
?>