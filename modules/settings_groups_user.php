<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	// $this->DebugArray($_POST);
	$template_file = "settings_edit_ok.tpl";
	
	// UPDATE or DELETE
	for ($loop=0; $loop < sizeof($_POST["id"]); $loop++) {
		$query = $safesql->query("SELECT user_group FROM ".DB_PREFIX."groups_user WHERE id=%i", array($_POST["id"][$loop]));
		$user_group = $db->get_var($query);
		if ((isset($_POST["groups_user_delete"][$loop])) && ($_POST["groups_user_delete"][$loop] == "YES")) {
			$query_data = array($_POST["id"][$loop]);
			$query = $safesql->query("DELETE FROM ".DB_PREFIX."groups_user WHERE id=%i", $query_data);
			$db->query($query);
			$query = $safesql->query("DELETE FROM ".DB_PREFIX."groups_access WHERE groups_user='%s'", array($user_group));
			$db->query($query);
		} else {
			$query_data = array(strtoupper($_POST["user_group"][$loop]), $_POST["access_summary"][$loop], $_POST["access_events"][$loop], $_POST["access_alerts"][$loop], $_POST["access_settings"][$loop], $_POST["id"][$loop]);
			$query = $safesql->query("UPDATE ".DB_PREFIX."groups_user SET user_group='%s', access_summary=%i, access_events=%i, access_alerts=%i, access_settings=%i WHERE id=%i", $query_data);
			$db->query($query);
			$query = $safesql->query("UPDATE ".DB_PREFIX."groups_access SET groups_user='%s' WHERE groups_user='%s'", array($_POST["user_group"][$loop], $user_group));
			$db->query($query);
		}
	}
	
	// NEW
	if (!empty($_POST["new_user_group"])) {
		$query_data = array(strtoupper($_POST["new_user_group"]), $_POST["new_access_summary"], $_POST["new_access_events"], $_POST["new_access_alerts"], $_POST["new_access_settings"]);
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."groups_user SET user_group='%s', access_summary=%i, access_events=%i, access_alerts=%i, access_settings=%i", $query_data);
		// $this->Debug($query);
		$db->query($query);
	}
	$smarty->clear_all_cache();
} else {
	
	$groups_user = $db->get_results("SELECT * FROM ".DB_PREFIX."groups_user ORDER BY user_group", ARRAY_A);
	$smarty->assign("groups_user", $groups_user);
}
?>