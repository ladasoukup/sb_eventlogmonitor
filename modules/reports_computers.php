<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
if (!isset($_GET["f_evt_type"])) $_GET["f_evt_type"] = "error";

$max_data_groups = 20;
$colors = array();
$data_groups = $this->GetComputers($_GET["sub_group"], false);
$data_groups_txt = $data_groups;

$time_from = $start_timestamp;
$time_to = $start_timestamp - (3600 * ($time_total_steps - $time_step));
$query = $safesql->query("SELECT count(id) as total_count, evt_computer FROM ".DB_PREFIX."events WHERE evt_type='%s' AND evt_time_generated BETWEEN '%s' AND '%s'[ AND evt_computer In (%Q)] GROUP BY evt_computer ORDER BY total_count DESC LIMIT %i", array($_GET["f_evt_type"], date("Y-m-d H:i:s", $time_to), date("Y-m-d H:i:s", $time_from), $data_groups, $max_data_groups));
$data_groups = $db->get_col($query, 1);
$data_groups_txt = $data_groups;

$loop_max = sizeof($data_groups);
for($hours=$time_total_steps; $hours>=0; $hours-=$time_step) {
	$curr_data = array();
	$time_from = $start_timestamp - (3600 * $hours);
	$time_to = $start_timestamp - (3600 * ($hours - $time_step));

	for($loop=0; $loop<$loop_max; $loop++) {
		$query = $safesql->query("SELECT count(id) as total_count FROM ".DB_PREFIX."events WHERE evt_type='%s' AND evt_time_generated BETWEEN '%s' AND '%s'[ AND evt_computer In (%Q)]", array($_GET["f_evt_type"], date("Y-m-d H:i:s", $time_from), date("Y-m-d H:i:s", $time_to), $data_groups[$loop]));
		$value = $db->get_var($query);
		
		if ($value > $max_value) $max_value = $value;
		$url = "index.php?module=showEventLog&amp;sub=".$data_groups_txt[$loop]."&amp;sub_group=".$_GET["sub_group"]."&amp;f_evt_".$_GET["f_evt_type"]."=".$_GET["f_evt_type"]."&amp;f_date_from=".date("Y-m-d H:i:s", $time_from)."&amp;f_date_to=".date("Y-m-d H:i:s", $time_to);
		$curr_data[] = array("value" => $value, "name" => $data_groups_txt[$loop], "url" => $url);
	}
	$curr_array = array("name" => date("d.m. H:i", $time_from) . " - " . date("H:i", $time_to), "data" => $curr_data);
	$chart_data[] = $curr_array;
}
?>