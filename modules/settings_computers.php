<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	$template_file = "settings_edit_ok.tpl";

	// UPDATE / DELETE
	for ($loop=0; $loop < sizeof($_POST["id"]); $loop++) {
		if ((isset($_POST["s_computer_delete"][$loop])) && ($_POST["s_computer_delete"][$loop] == "YES")) {
			// DELETE
			$query = $safesql->query("DELETE FROM ".DB_PREFIX."computers WHERE id = %i", array($_POST["id"][$loop]));
			$db->query($query);
			//$db->debug();

			// delete DAT file
			unlink("./data/" . $_POST["computer_name"][$loop] . ".dat");

			// delete cache files
			$smarty->clear_cache(null, "showAlertsDetail|" . $_POST["computer_name"][$loop]);
			$smarty->clear_cache(null, "showEventLogDetail|" . $_POST["computer_name"][$loop]);

			// delete ALL records from DB!!!
			$query = $safesql->query("DELETE FROM ".DB_PREFIX."events WHERE evt_computer = '%s'", array($_POST["computer_name"][$loop]));
			$db->query($query);
			//$db->debug();

		} else {
			// UPDATE
			unset($query_data);
			$query_data[0] = $_POST["computer_harvest_interval"][$loop] * $_POST["computer_harvest_interval_unit"][$loop];
			$query_data[1] = strtoupper($_POST["computer_group"][$loop]);
			$query_data[2] = $_POST["computer_enable"][$loop];
			$query_data[3] = $_POST["id"][$loop];
			$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_harvest_interval = %i, computer_group = '%s', computer_enable = %i WHERE id = %i", $query_data);

			$db->query($query);
			//$db->debug();
		}
	}

	// NEW
	if (!empty($_POST["new_computer_name"])) {
		// check if computer exists or not...
		$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."computers WHERE computer_name='%s'", array($_POST["new_computer_name"]));
		if ($db->get_var($query) > 0) {
			// computer already exists...
			$template_file = "settings_edit_error_computer.tpl";
		} else {
			// OK, add computer to db.
			unset($query_data);
			$query_data[0] = strtolower($_POST["new_computer_name"]);
			$query_data[1] = strtoupper($_POST["new_computer_group"]);
			$query_data[2] = $_POST["new_computer_harvest_interval"] * $_POST["new_computer_harvest_interval_unit"];
			$query_data[3] = $_POST["new_computer_enable"];
			$query = $safesql->query("INSERT INTO ".DB_PREFIX."computers SET computer_name = '%s', computer_group = '%s', computer_harvest_interval = %i, computer_enable = %i", $query_data);
			$db->query($query);
			//$db->debug();
			$smarty->clear_cache(null, "showEventLog");
			$smarty->clear_cache(null, "showSummary");
		}
	}
	$smarty->clear_cache(null, "showEventLog");
	$smarty->clear_cache(null, "showSummary");
} else {
	$query = $safesql->query("SELECT * FROM ".DB_PREFIX."computers[ WHERE computer_group LIKE '%~%S~%' OR computer_group='%S'] ORDER BY computer_name", array($_GET["f_group"], $_GET["f_group"]));
	$data = $db->get_results($query, ARRAY_A);
	//$db->debug();
	$s_computer = array();
	if (is_array($data)){
		foreach($data as $line){
			if ($line["computer_harvest_interval"] >= 172800){
				$line["computer_harvest_interval"] = round($line["computer_harvest_interval"]/86400,1);
				$line["computer_harvest_interval_unit"] = 86400;
			} else if ($line["computer_harvest_interval"] >= 7200){
				$line["computer_harvest_interval"] = round($line["computer_harvest_interval"]/3600,1);
				$line["computer_harvest_interval_unit"] = 3600;
			} else if ($line["computer_harvest_interval"] >= 60){
				$line["computer_harvest_interval"] = round($line["computer_harvest_interval"]/60);
				$line["computer_harvest_interval_unit"] = 60;
			}
			$s_computer[] = $line;
		}
	}
	$smarty->assign("s_computers", $s_computer);
	$smarty->assign("groups_computer", $this->GetGroups(true, true));
}
?>