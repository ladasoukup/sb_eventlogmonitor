<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");


$import_file = $_FILES["filter_import"]["tmp_name"];
if (is_uploaded_file($import_file)) {
	
	$xml_data = simplexml_load_file($import_file);
		foreach ($xml_data->filter as $xml_line){
			$xml_line = $this->object2array($xml_line);
			$this->DebugArray($xml_line);
			
			$query_data = array($xml_line["filter_title"], $xml_line["filter_data"], $xml_line["filter_isNoise"], $xml_line["filter_isAlert"], $xml_line["filter_email_ignore"], $xml_line["filter_priority"]);
			$query = $safesql->query("INSERT INTO ".DB_PREFIX."filters SET filter_title='%s', filter_data='%s', filter_isNoise=%i, filter_isAlert=%i, filter_email_ignore=%i, filter_priority=%i, filter_active=0", $query_data);
			$this->Debug($query);
			$db->query($query);
			
		}
	
	$smarty->assign("import", "ok");
} else {
	$smarty->assign("import", "error");
}

$template_file = "settings_filters_import.tpl";
?>