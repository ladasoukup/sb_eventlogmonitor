<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
$this->Enable_AutoRefresh();
$perPage = 50;

// SUBMENU
$submenu = $this->GetComputers($_GET["sub_group"], true);
$smarty->assign("submenu", $submenu);
// SUBMENU - groups
$submenu_group = $this->GetGroups(true);
$smarty->assign("submenu_group", $submenu_group);

if ($group_access_denied === true) $template_file = "_error_group_access_denied.tpl";

if (empty($_GET["order"])) $_GET["order"] = "evt_time_generated";
if (empty($_GET["sort"])) $_GET["sort"] = "DESC";
if ((empty($_GET["f_evt_error"])) && (empty($_GET["f_evt_warning"])) && (empty($_GET["f_evt_audit_failure"])) && (empty($_GET["f_evt_audit_success"])) && (empty($_GET["f_evt_information"]))) {
	$_GET["f_evt_error"] = "error"; $_GET["f_evt_warning"] = "warning"; $_GET["f_evt_audit_failure"] = "audit_failure"; $_GET["f_evt_audit_success"] = "audit_success"; $_GET["f_evt_information"] = "information";
}

$query_evt_type = array($_GET["f_evt_error"], $_GET["f_evt_warning"], $_GET["f_evt_audit_failure"], $_GET["f_evt_audit_success"], $_GET["f_evt_information"]);
$query_evt_noise = array(0);
if ($_GET["f_evt_noise"] == "True") $query_evt_noise[] = 1;
$query_cols = "id, evt_computer, evt_code, evt_type, evt_category, evt_logfile, evt_source, evt_user, evt_time_generated, evt_noise";
if (!empty($_GET["sub_group"])) {
	$computers_in_group = $this->GetComputers($_GET["sub_group"], false);
} else {
	$computers_in_group = "";
}
$query_params = array($_GET["sub"], $computers_in_group, $query_evt_type, $_GET["f_evt_code"], $_GET["f_evt_source"], $_GET["f_evt_category"], $_GET["f_evt_user"], $_GET["f_date_from"], $_GET["f_date_to"], $query_evt_noise, $_GET["order"], strtoupper($_GET["sort"]), $_GET["from"], $perPage);

$query = "SELECT ".$query_cols." FROM ".DB_PREFIX."alerts";
$query .= " WHERE 1=1[ AND evt_computer='%S']";
$query .= "[ AND evt_computer in (%Q)]";
$query .= "[ AND evt_type in (%Q)]";
$query .= "[ AND evt_code = %I][ AND evt_source LIKE '%%%S%%'][ AND evt_category LIKE '%%%S%%'][ AND evt_user LIKE '%%%S%%']";
$query .= "[ AND evt_time_generated > '%S 00:00:00'][ AND evt_time_generated < '%S 23:59:59'][ AND evt_noise in (%Q)]";
$query .= " ORDER BY %s %s";
$query .= " LIMIT [%I, ]%i";


$query = $safesql->query($query, $query_params);
$event_table_head[] = array("db_col" => "evt_time_generated", "title" => "##Date and time##");
if (empty($_GET["sub"])) { 
	$event_table_head[] = array("db_col" => "evt_computer", "title" => "##Computer##");
	$smarty->assign("show_evt_computer", "true");
}
$event_table_head[] = array("db_col" => "evt_code", "title" => "##Event code##");
$event_table_head[] = array("db_col" => "evt_category", "title" => "##Event category##");
$event_table_head[] = array("db_col" => "evt_source", "title" => "##Event source##");
$event_table_head[] = array("db_col" => "evt_user", "title" => "##User##");

$list_newer = $_GET["from"] - $perPage;
$list_older = $_GET["from"] + $perPage;
if ($list_newer < 0) $list_newer = 0;

$smarty->assign("event_table_head", $event_table_head);
$smarty->assign("event_data", $db->get_results($query, ARRAY_A));
$smarty->assign("event_detail_module", "showAlertsDetail");
$smarty->assign("list_newer", $list_newer);
$smarty->assign("list_older", $list_older);
if ($_GET["from"] == 0) $smarty->assign("BLOCK_list_newer", "true");
// if ( !check for max! ) $smarty->assign("BLOCK_list_older", "true");

//$db->debug();
//echo $query;
?>