<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
$this->Enable_AutoRefresh();

$evt_types = array("error", "warning", "information", "audit_success", "audit_failure");

// Time intervals
$summary_time_interals = array("##last 12 hours##", "##today##", "##yesterday##", "##two days ago##", "##last week##", "##all##");
$smarty->assign("summary_time_interals", $summary_time_interals);

if (empty($_GET["time_period"])) $_GET["time_period"] = "0";
switch ($_GET["time_period"]) {
case 0:
	$sumary_time_period_start = date("Y-m-d H:i:s", time() - 43200);
	$sumary_time_period_end = date("Y-m-d H:i:s");
	break;
case 1:
	$sumary_time_period_start = date("Y-m-d 00:00:00");
	$sumary_time_period_end = date("Y-m-d 23:59:59");
	break;
case 2:
	$sumary_time_period_start = date("Y-m-d 00:00:00", time() - 86400);
	$sumary_time_period_end = date("Y-m-d 23:59:59", time() - 86400);
	break;
case 3:
	$sumary_time_period_start = date("Y-m-d 00:00:00", time() - 172800);
	$sumary_time_period_end = date("Y-m-d 23:59:59", time() - 172800);
	break;
case 4:
	$sumary_time_period_start = date("Y-m-d 00:00:00", time() - 604800);
	$sumary_time_period_end = date("Y-m-d 23:59:59");
	break;
default:
	$sumary_time_period_start = "";
	$sumary_time_period_end = "";
}

// SUBMENU - groups
$submenu_group = $this->GetGroups(true);
$smarty->assign("submenu_group", $submenu_group);
if (!empty($_GET["sub_group"])) {
	$computers_in_group = $this->GetComputers($_GET["sub_group"], false);
} else {
	$computers_in_group = "";
}

if ($group_access_denied === true) $template_file = "_error_group_access_denied.tpl";

$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."computers[ WHERE computer_group='%S']", array($_GET["sub_group"]));
$smarty->assign("summary_count_computers", $db->get_var($query));

// Events Summary
$date_today = date("Y-m-d"); $date_yesterday = date("Y-m-d", time()-86400); $date_2days = date("Y-m-d", time()-172800);
foreach ($evt_types as $db_name) {
	$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query2 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_today." 00:00:00' AND evt_time_generated < '".$date_today." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query3 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_yesterday." 00:00:00' AND evt_time_generated < '".$date_yesterday." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query4 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_time_generated < '".$date_2days." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$summary_count_evt[] = array(
		"db_name" => $db_name,
		"count" => $db->get_var($query),
		"count_today" => $db->get_var($query2),
		"count_yesterday" => $db->get_var($query3),
		"count_2days" => $db->get_var($query4)
	);
}

$summary_count_sum = $summary_count_evt[0]["count"] + $summary_count_evt[1]["count"] + $summary_count_evt[2]["count"] + $summary_count_evt[3]["count"] + $summary_count_evt[4]["count"];
$smarty->assign("summary_count_sum", $summary_count_sum);
$smarty->assign("summary_count_evt", $summary_count_evt);
$smarty->assign("date_today", $date_today);
$smarty->assign("date_yesterday", $date_yesterday);
$smarty->assign("date_2days", $date_2days);

// Events Summary - graf
$chart_data = array( 	array("value" => $summary_count_evt[0]["count"], "color" => "0xFF7C7C", "name" => "##Error##", "url" => "index.php?module=showEventLog&sub=&sub_group=".$_GET["sub_group"]."&f_evt_error=error&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_evt[1]["count"], "color" => "0xECFF6B", "name" => "##Warning##", "url" => "index.php?module=showEventLog&sub=&sub_group=".$_GET["sub_group"]."&f_evt_warning=warning&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_evt[2]["count"], "color" => "0xFFFFFF", "name" => "##Information##", "url" => "index.php?module=showEventLog&sub=&sub_group=".$_GET["sub_group"]."&f_evt_information=information&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_evt[3]["count"], "color" => "0xC1FFD1", "name" => "##Audit success##", "url" => "index.php?module=showEventLog&sub=&sub_group=".$_GET["sub_group"]."&f_evt_audit_success=audit_success&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_evt[4]["count"], "color" => "0xFCDE9F", "name" => "##Audit failure##", "url" => "index.php?module=showEventLog&sub=&sub_group=".$_GET["sub_group"]."&f_evt_audit_failure=audit_failure&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
					);
$this->charts_generate_xml($chart_data, "event", "summary");

// Alerts Summary
foreach ($evt_types as $db_name) {
	$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query2 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_today." 00:00:00' AND evt_time_generated < '".$date_today." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query3 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_yesterday." 00:00:00' AND evt_time_generated < '".$date_yesterday." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query4 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_time_generated < '".$date_2days." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$summary_count_alerts[] = array(
		"db_name" => $db_name,
		"count" => $db->get_var($query),
		"count_today" => $db->get_var($query2),
		"count_yesterday" => $db->get_var($query3),
		"count_2days" => $db->get_var($query4)
	);
}

$alert_filters_count = $db->get_var("SELECT count(id) FROM ".DB_PREFIX."filters WHERE filter_active=1");
$summary_count_alerts_sum = $summary_count_alerts[0]["count"] + $summary_count_alerts[1]["count"] + $summary_count_alerts[2]["count"] + $summary_count_alerts[3]["count"] + $summary_count_alerts[4]["count"];
$smarty->assign("summary_count_alerts_sum", $summary_count_alerts_sum);
$smarty->assign("summary_count_alerts", $summary_count_alerts);
$smarty->assign("alert_filters_count", $alert_filters_count);

// Alerts Summary - graf
$chart_data = array( 	array("value" => $summary_count_alerts[0]["count"], "color" => "0xFF7C7C", "name" => "##Error##", "url" => "index.php?module=showAlerts&sub=&sub_group=".$_GET["sub_group"]."&f_evt_error=error&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_alerts[1]["count"], "color" => "0xECFF6B", "name" => "##Warning##", "url" => "index.php?module=showAlerts&sub=&sub_group=".$_GET["sub_group"]."&f_evt_warning=warning&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_alerts[2]["count"], "color" => "0xFFFFFF", "name" => "##Information##", "url" => "index.php?module=showAlerts&sub=&sub_group=".$_GET["sub_group"]."&f_evt_information=information&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_alerts[3]["count"], "color" => "0xC1FFD1", "name" => "##Audit success##", "url" => "index.php?module=showAlerts&sub=&sub_group=".$_GET["sub_group"]."&f_evt_audit_success=audit_success&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
						array("value" => $summary_count_alerts[4]["count"], "color" => "0xFCDE9F", "name" => "##Audit failure##", "url" => "index.php?module=showAlerts&sub=&sub_group=".$_GET["sub_group"]."&f_evt_audit_failure=audit_failure&amp;f_date_from=".$date_2days."&amp;f_date_to=".$date_today, "url_target" => "_self"),
					);
$this->charts_generate_xml($chart_data, "alert", "summary");

// Top Error computers
$chart_colors = array ("ECFF6B", "C1FFD1", "FCDE9F", "B7F7FB", "FFC6C6", "E2DA00", "E3E3E3");
$smarty->assign("summary_graph_colors", $chart_colors);

foreach($evt_types as $summary_type) {
	$query_data = array($summary_type, $sumary_time_period_start, $sumary_time_period_end, $computers_in_group);
	$query = $safesql->query("SELECT evt_computer, count(id) as evt_count  FROM ".DB_PREFIX."events WHERE evt_type='%s' AND evt_noise = 0[ AND evt_time_generated>'%S'][ AND evt_time_generated<'%S'][ AND evt_computer in (%Q)] group by evt_computer ORDER BY evt_count DESC LIMIT 7", $query_data);
	$query_data = $db->get_results($query, ARRAY_A);
	//$db->debug();
	$summary_data[$summary_type] = $query_data;
	$smarty->assign("summary_" . $summary_type, $query_data);
	$smarty->assign("sumary_time_period_start", $sumary_time_period_start);
	$smarty->assign("sumary_time_period_end", $sumary_time_period_end);
	
	// graph
	$chart_data = array();
	for($loop = 0; $loop < sizeof($summary_data[$summary_type]); $loop++){
		$chart_url = "index.php?module=showEventLog&sub=".$summary_data[$summary_type][$loop]["evt_computer"]."&sub_group=".$_GET["sub_group"]."&f_evt_".$summary_type."=".$summary_type."&f_date_from=".$sumary_time_period_start."&f_date_to=".$sumary_time_period_end;
		$chart_data[] = array("value" => $summary_data[$summary_type][$loop]["evt_count"], "color" => "0x" . $chart_colors[$loop], "name" => $summary_data[$summary_type][$loop]["evt_computer"], "url" => $chart_url, "url_target" => "_self");
	}
	$this->charts_generate_xml($chart_data, $summary_type, "summary");
}

// Last alerts
$query = $safesql->query("SELECT DISTINCT evt_computer FROM ".DB_PREFIX."alerts WHERE evt_time_generated>'%s'[ AND evt_computer in (%Q)] ORDER BY evt_time_generated DESC LIMIT 10", array(date("Y-m-d 00:00:00", time() - 172800), $computers_in_group));
$last_alerts_computers = $db->get_col($query, 0);
$last_alerts = "";
if (is_array($last_alerts_computers)) {
	foreach($last_alerts_computers as $evt_computer) {
		$query = $safesql->query("SELECT * FROM ".DB_PREFIX."alerts WHERE evt_computer='%s' AND evt_noise = 0 ORDER BY evt_time_generated DESC LIMIT 1", array($evt_computer));
		$last_alerts[] = $db->get_row($query, ARRAY_A);
	}
}
$smarty->assign("last_alerts", $last_alerts);
?>