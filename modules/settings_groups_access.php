<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	$template_file = "settings_edit_ok.tpl";
	// $this->DebugArray($_POST);
	
	if ((isset($_POST["access_table"])) && (is_array($_POST["access_table"]))){
		$db->query("TRUNCATE ".DB_PREFIX."groups_access");
		$i = 0;
		foreach ($_POST["access_table"] as $groups_computer => $groups_user_A) {
			foreach($groups_user_A as $groups_user => $access_level) {
				if ($groups_computer == $i) $groups_computer = "";
				$query_data = array($groups_computer, $groups_user, $access_level);
				$query = $safesql->query("INSERT INTO ".DB_PREFIX."groups_access SET groups_computer='%s', groups_user='%s', access_level=%i", $query_data);
				$db->query($query);
				//$db->debug();
			}
			$i++;
		}
	}
	$smarty->clear_all_cache();
} else {
	$groups_computer = $this->GetGroups(true, true);
	$groups_user = $this->GetGroupsUser(false);
	
	$access_table = array();
	if ((is_array($groups_computer)) && (is_array($groups_user))) {
		foreach($groups_computer as $computer) {
			$user_A = array();
			foreach($groups_user as $user) {
				$user_A[$user] = $this->CanAccessGroup($computer, $user);
			}
			$access_table[$computer] = $user_A;
		}
	}
	
	$smarty->assign("groups_computer", $groups_computer);
	$smarty->assign("groups_user", $groups_user);
	$smarty->assign("access_table", $access_table);
	$smarty->assign("cols_count", sizeof($groups_user) + 1);
}
?>