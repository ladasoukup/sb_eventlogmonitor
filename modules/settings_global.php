<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	$this->DebugArray($_POST);
	
	// DELETE ALL
	$db->query("TRUNCATE ".DB_PREFIX."cfg");
	
	// UPDATE
	for ($loop=0; $loop < sizeof($_POST["cfg_var"]); $loop++) {
		unset($query_data);
		$query_data[] = $_POST["cfg_var"][$loop];
		$query_data[] = $_POST["cfg_val"][$loop];
		$query_data[] = $_POST["cfg_group"][$loop];
		$query_data[] = "";
		
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."cfg SET cfg_var='%s', cfg_val = '%s', cfg_group = '%s', cfg_desc = '%s'", $query_data);
		$db->query($query);
		//$db->debug();
	}
	$db->query("INSERT INTO ".DB_PREFIX."cfg SET cfg_var='app_version', cfg_val = '".$this->version."'");
	$template_file = "settings_edit_ok.tpl";
} else {
	
	$xml_path = "./modules/settings_global.xml";
	if (file_exists($xml_path)) {
		$xml_data = simplexml_load_file($xml_path);
		foreach ($xml_data->cfg as $xml_line){
			$xml_line = $this->object2array($xml_line);
			
			$temp = $this->GetConfig($xml_line["cfg_var"]);
			if (!empty($temp)) $xml_line["cfg_val"] = $temp;
			
			$global_cfg[] = $xml_line;
		}
	} else {
		trigger_error("File \"settings_global.xml\" is missing!", E_USER_ERROR);
		die();
	}
	
	$smarty->assign("global_cfg", $global_cfg);
}