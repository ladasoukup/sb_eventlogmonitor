<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	$this->DebugArray($_POST);
	$template_file = "settings_edit_ok.tpl";
	
	// UPDATE
	if (isset($_POST["groups_computer"])) {
		foreach($_POST["groups_computer"] as $group_orig => $group_new) {
			if ($group_orig != $group_new) {
				// Do update!
				$this->debug("Rename group: " . $group_orig . " => " . $group_new);
				$query_data = array(strtoupper($group_new), strtoupper($group_orig));
				$query = $safesql->query("UPDATE ".DB_PREFIX."groups_computer SET computer_group='%s' WHERE computer_group='%s'", $query_data);
				$db->query($query);
				$query = $safesql->query("UPDATE ".DB_PREFIX."groups_access SET groups_computer='%s' WHERE groups_computer='%s'", $query_data);
				$db->query($query);
				
				// update computers...
				$computers = $this->GetComputers(strtoupper($group_orig));
				if (is_array($computers)) {
					foreach ($computers as $computer) {
						$query = $safesql->query("SELECT computer_group FROM ".DB_PREFIX."computers WHERE computer_name='%s'", array($computer));
						$RAW_group = $db->get_var($query);
						$RAW_group_new = str_replace("~".$group_orig."~", "~".$group_new."~", $RAW_group);
						if ($RAW_group == $RAW_group_new) $RAW_group_new = $group_new;
						$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_group='%s' WHERE computer_name='%s'", array($RAW_group_new, $computer));
						$db->query($query);
						$this->debug("Update computer: " . $query);
					}
				}
				
				// update RULES...
				$query = $safesql->query("SELECT * FROM ".DB_PREFIX."filters", array());
				$data = $db->get_results($query, ARRAY_A);
				if (is_array($data)) {
					foreach ($data as $filter) {
						$filter_data_new = array();
						$filter_data = unserialize($filter["filter_data"]);
						for($loop=0; $loop<sizeof($filter_data); $loop++) {
							if ($filter_data[$loop]["evt_data_type"] == "evt_computer_group") {
								$this->debug("Group comparation found...");
								if ($filter_data[$loop]["evt_data_value"] == strtoupper($group_orig)) {
									$this->debug("Group found. Changing...");
									$filter_data[$loop]["evt_data_value"] = strtoupper($group_new);
									$filter_data_new[] = $filter_data[$loop];
								} else {
									$filter_data_new[] = $filter_data[$loop];
								}
							} else {
								$filter_data_new[] = $filter_data[$loop];
							}
						}
						$filter_data_new = serialize($filter_data_new);
						$query = $safesql->query("UPDATE ".DB_PREFIX."filters SET filter_data='%s' WHERE id=%i", array($filter_data_new, $filter["id"]));
						$db->query($query);
						$this->debug($query);
					}
				}
			}
		}
	}
	
	// DELETE
	if (isset($_POST["groups_computer_delete"])) {
		foreach($_POST["groups_computer_delete"] as $group_to_delete => $value) {
			// Do update!
			$this->debug("Delete group: " . $group_to_delete);
			$query_data = array(strtoupper($group_to_delete));
			$query = $safesql->query("DELETE FROM ".DB_PREFIX."groups_computer WHERE computer_group='%s'", $query_data);
			$db->query($query);
			$query = $safesql->query("DELETE FROM ".DB_PREFIX."groups_access WHERE groups_computer='%s'", $query_data);
			$db->query($query);
			
			// update computers
			$computers = $this->GetComputers(strtoupper($group_to_delete));
			if (is_array($computers)) {
				foreach ($computers as $computer) {
					if (!empty($computer)) {
						$query = $safesql->query("SELECT computer_group FROM ".DB_PREFIX."computers WHERE computer_name='%s'", array($computer));
						$RAW_group = $db->get_var($query);
						$RAW_group = str_replace("~~", "", $RAW_group);
						$RAW_group_A = explode("~", $RAW_group);
						$RAW_group_new = "";
						for ($loop=0; $loop<sizeof($RAW_group_A); $loop++) {
							if ($RAW_group_A[$loop] != strtoupper($group_to_delete)) {
								$RAW_group_new .= $RAW_group_A[$loop] . "~";
							}
						}
						if (!empty($RAW_group_new)) $RAW_group_new = "~~" . $RAW_group_new . "~";
						$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_group='%s' WHERE computer_name='%s'", array($RAW_group_new, $computer));
						$db->query($query);
						$this->debug("Update computer: " . $query);
					}
				}
			}
			
			// update RULES...
			$query = $safesql->query("SELECT * FROM ".DB_PREFIX."filters", array());
			$data = $db->get_results($query, ARRAY_A);
			if (is_array($data)) {
				foreach ($data as $filter) {
					$filter_data_new = array();
					$filter_data = unserialize($filter["filter_data"]);
					for($loop=0; $loop<sizeof($filter_data); $loop++) {
						if ($filter_data[$loop]["evt_data_type"] == "evt_computer_group") {
							$this->debug("Group comparation found...");
							if ($filter_data[$loop]["evt_data_value"] == strtoupper($group_to_delete)) {
								$this->debug("Group found. deleting...");
							} else {
								$filter_data_new[] = $filter_data[$loop];
							}
						} else {
							$filter_data_new[] = $filter_data[$loop];
						}
					}
					$filter_data_new = serialize($filter_data_new);
					$query = $safesql->query("UPDATE ".DB_PREFIX."filters SET filter_data='%s' WHERE id=%i", array($filter_data_new, $filter["id"]));
					$db->query($query);
					$this->debug($query);
				}
			}
		}
	}

	// NEW
	if (!empty($_POST["new_groups_computer"])) {
		// check if computer exists or not...
		$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."groups_computer WHERE computer_group='%s'", array(strtoupper($_POST["new_groups_computer"])));
		if ($db->get_var($query) == 0) {
			$query = $safesql->query("INSERT INTO ".DB_PREFIX."groups_computer SET computer_group='%s'", array(strtoupper($_POST["new_groups_computer"])));
			$db->query($query);
		}
	}
	
	$smarty->clear_cache(null, "showEventLog");
	$smarty->clear_cache(null, "showAlerts");
	$smarty->clear_cache(null, "showSummary");
} else {
	
	$groups_computer = $this->GetGroups(false, true);
	$smarty_groups_computer = array();
	if (is_array($groups_computer)) {
		foreach($groups_computer as $group) {
			$computers = $this->GetComputers($group, false);
			$computers_implode = "";
			if (is_array($computers)) $computers_implode = implode(", ", $computers);
			$smarty_groups_computer[] = array("group" => $group, "computers" => $computers_implode);
		}
	}
	
	$smarty->assign("groups_computer", $smarty_groups_computer);
}
?>