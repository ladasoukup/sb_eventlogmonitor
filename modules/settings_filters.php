<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	//print_r($_POST); echo "<hr />";
	
	// EDIT or DELETE
	for ($loop=0; $loop < sizeof($_POST["id"]); $loop++){
		if ((isset($_POST["filter_delete"][$loop])) && ($_POST["filter_delete"][$loop] == "YES")) {
			// DELETE
			$query_data = array($_POST["id"][$loop]);
			$query = $safesql->query("DELETE FROM ".DB_PREFIX."filters WHERE id = %i", $query_data);
		} else {
			// EDIT
			if ($_POST["filter_is"][$loop] == "noise") {
				$isNoise = 1; $isAlert = 0;
			} else if ($_POST["filter_is"][$loop] == "alert") {
				$isNoise = 0; $isAlert = 1;
			} else {
				$isNoise = 0; $isAlert = 0;
			}
			if ((isset($_POST["filter_isEmail"][$loop])) && ($_POST["filter_isEmail"][$loop] == "YES")) $isEmail = 1; else $isEmail = 0;
			if (empty($_POST["filter_mailto"][$loop])) $isEmail = 0;
			
			$query_data = array($_POST["filter_title"][$loop], $isNoise, $isAlert, $isEmail, $_POST["filter_mailto"][$loop], $_POST["filter_email_ignore"][$loop], $_POST["filter_active"][$loop], $_POST["filter_priority"][$loop], $_POST["id"][$loop]);
			$query = $safesql->query("UPDATE ".DB_PREFIX."filters SET filter_title = '%s', filter_isNoise = %i, filter_isAlert = %i, filter_isEmail = %i, filter_mailto = '%s', filter_email_ignore=%i, filter_active = %i, filter_priority = %i WHERE id = %i", $query_data);
		}
		//echo $query . "<br />\n";
		$db->query($query);
	}
	
	// NEW
	if (!empty($_POST["new_filter_title"])) {
		if ($_POST["new_filter_is"] == "noise") {
			$isNoise = 1; $isAlert = 0;
		} else if ($_POST["new_filter_is"] == "alert") {
			$isNoise = 0; $isAlert = 1;
		} else {
			$isNoise = 0; $isAlert = 0;
		}
		if ((isset($_POST["new_filter_isEmail"])) && ($_POST["new_filter_isEmail"] == "YES")) $isEmail = 1; else $isEmail = 0;
		if (empty($_POST["new_filter_mailto"])) $isEmail = 0;
		
		$query_data = array($_POST["new_filter_title"], $isNoise, $isAlert, $isEmail, $_POST["new_filter_mailto"], $_POST["new_filter_email_ignore"]);
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."filters SET filter_title = '%s', filter_data='', filter_isNoise = %i, filter_isAlert = %i, filter_isEmail = %i, filter_mailto = '%s', filter_email_ignore=%i,  filter_active = 0", $query_data);
		//echo $query . "<br />\n";
		$db->query($query);
		
	}
	
	$template_file = "settings_edit_ok.tpl";
	
} else {
	
	$query = $safesql->query("SELECT * FROM ".DB_PREFIX."filters ORDER BY filter_active DESC, filter_priority ASC, filter_title ASC", array());
	$filters = $db->get_results($query, ARRAY_A);
	//$db->debug();
	
	$smarty->assign("filters", $filters);
	for($loop=10; $loop <= 500; $loop+=10) $priority_array[] = $loop;
	$smarty->assign("priority", $priority_array);
}
?>