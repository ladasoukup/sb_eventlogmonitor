<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
$smarty->caching = 0;

// SUBMENU
$submenu[] = array("sub" => "computers", "title" => "##Monitored computers##");
$submenu[] = array("sub" => "groups_computer", "title" => "##Computer groups##");
$submenu[] = array("sub" => "users", "title" => "##Users &amp; Passwords##");
$submenu[] = array("sub" => "groups_user", "title" => "##User groups##");
$submenu[] = array("sub" => "groups_access", "title" => "##Access control##");
$submenu[] = array("sub" => "filters", "title" => "##Alert filters##");
$submenu[] = array("sub" => "global", "title" => "##Global config##");
$submenu[] = array("sub" => "cron", "title" => "##Scheduled tasks##");
$submenu[] = array("sub" => "emails", "title" => "##Email queue##");

$submenu[] = array("sub" => "alertqueue", "title" => "##Alert queue##");
$submenu[] = array("sub" => "rpc", "title" => "##RPC secrets##");

$smarty->assign("submenu_title", "##Settings##");
$smarty->assign("submenu", $submenu);
// SUBMENU

$include_file = "./modules/settings_" . $_GET["sub"] . ".php";
if (file_exists($include_file)) {
	$template_file = "settings_" . $_GET["sub"] . ".tpl";
	include($include_file);
}
?>