<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	//print_r($_POST); echo "<hr />";
	// SAVE rule...
	for ($loop=0; $loop < sizeof($_POST["evt_data_type"]); $loop++) {
		if (!isset($_POST["evt_rule_condition"][$loop])) $_POST["evt_rule_condition"][$loop] = "";
		if (!isset($_POST["evt_data_type"][$loop])) $_POST["evt_data_type"][$loop] = "";
		if (!isset($_POST["evt_comparation"][$loop])) $_POST["evt_comparation"][$loop] = "";
		if (!isset($_POST["evt_data_value"][$loop])) $_POST["evt_data_value"][$loop] = "";
		
		$filter_data[] = array(	"evt_rule_condition" => $_POST["evt_rule_condition"][$loop],
								"evt_data_type" => $_POST["evt_data_type"][$loop],
								"evt_comparation" => $_POST["evt_comparation"][$loop],
								"evt_data_value" => str_replace("\\\\", "\\", $_POST["evt_data_value"][$loop])
								);
	}
	$filter_data = serialize($filter_data);
	$query = $safesql->query("UPDATE ".DB_PREFIX."filters SET filter_data='%s' WHERE id=%i", array($filter_data, $_GET["id"]));
	$db->query($query);
	
	$template_file = "settings_edit_ok_wnd_close.tpl";
} else {
	
	$query = $safesql->query("SELECT filter_data FROM ".DB_PREFIX."filters WHERE id=%i LIMIT 1", array($_GET["id"]));
	$filter_data = $db->get_var($query);
	if (!empty($filter_data)) {
		$smarty->assign("filter_data", unserialize($filter_data));
	} else {
		$smarty->assign("filter_data", array(0));
	}
	//print_r(unserialize($filter_data)); echo "<hr />";
}

$actb_evt_computer = implode(",", $this->GetComputers("", false, "'"));
$actb_evt_computer_group = implode(",", $this->GetGroups(false, true, "'"));

$smarty->assign("actb_evt_computer", $actb_evt_computer);
$smarty->assign("actb_evt_computer_group", $actb_evt_computer_group);
$smarty->assign("actb_evt_source", unserialize($db->get_var("SELECT ac_values FROM ".DB_PREFIX."autocomplete WHERE ac_field='evt_source'")));
$smarty->assign("actb_evt_user", unserialize($db->get_var("SELECT ac_values FROM ".DB_PREFIX."autocomplete WHERE ac_field='evt_user'")));
$smarty->assign("actb_evt_logfile", unserialize($db->get_var("SELECT ac_values FROM ".DB_PREFIX."autocomplete WHERE ac_field='evt_logfile'")));
$smarty->assign("actb_evt_category", unserialize($db->get_var("SELECT ac_values FROM ".DB_PREFIX."autocomplete WHERE ac_field='evt_category'")));
$smarty->assign("actb_evt_code", unserialize($db->get_var("SELECT ac_values FROM ".DB_PREFIX."autocomplete WHERE ac_field='evt_code'")));
?>