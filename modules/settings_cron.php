<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

if (!empty($_POST)) {
	//print_r($_POST); echo "<hr />";
	
	// DELETE ALL
	$db->query("TRUNCATE ".DB_PREFIX."cron");
	
	// UPDATE
	for ($loop=0; $loop < sizeof($_POST["cron_module"]); $loop++) {
		unset($query_data);
		if (empty($_POST["cron_time"][$loop])) {
			$cron_time = "0000-00-00 00:00:00";
		} else {
			$cron_time = $_POST["cron_time"][$loop];
		}
		
		$query_data[] = $_POST["cron_title"][$loop];
		$query_data[] = $cron_time;
		$query_data[] = $_POST["cron_repeat"][$loop] * $_POST["cron_repeat_unit"][$loop];
		$query_data[] = $_POST["cron_module"][$loop];
		$query_data[] = $_POST["cron_params"][$loop];
		$query_data[] = $_POST["cron_priority"][$loop];
		$query_data[] = $_POST["cron_enable"][$loop];
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."cron SET cron_title='%s', cron_time = '%s', cron_repeat = %i, cron_module = '%s', cron_params = '%s', cron_priority = %i, cron_enable = %i", $query_data);
		
		$db->query($query);
		//$db->debug();
	}
	
	$template_file = "settings_edit_ok.tpl";
} else {
	
	
	$xml_path = "./modules_cron/_modules_cron.xml";
	if (file_exists($xml_path)) {
		$xml_data = simplexml_load_file($xml_path);
		foreach ($xml_data->module as $xml_line){
			$xml_line = $this->object2array($xml_line);
			$query = $safesql->query("SELECT * FROM ".DB_PREFIX."cron WHERE cron_module='%s'", array($xml_line["cron_module"]));
			$user_data = $data = $db->get_row($query, ARRAY_A);
			//$db->debug();
			
			if (is_array($user_data)) {
				$xml_line["cron_time"] = $user_data["cron_time"];
				$xml_line["cron_repeat"] = $user_data["cron_repeat"];
				$xml_line["cron_params"] = $user_data["cron_params"];
				$xml_line["cron_priority"] = $user_data["cron_priority"];
				$xml_line["cron_enable"] = $user_data["cron_enable"];
			}
			
			if ($xml_line["cron_repeat"] >= 172800){
				$xml_line["cron_repeat"] = round($xml_line["cron_repeat"]/86400,1);
				$xml_line["cron_repeat_unit"] = 86400;
			} else if ($xml_line["cron_repeat"] >= 7200){
				$xml_line["cron_repeat"] = round($xml_line["cron_repeat"]/3600,1);
				$xml_line["cron_repeat_unit"] = 3600;
			} else if ($xml_line["cron_repeat"] >= 60){
				$xml_line["cron_repeat"] = round($xml_line["cron_repeat"]/60);
				$xml_line["cron_repeat_unit"] = 60;
			}
			
			$cron_modules[] = $xml_line;
		}
		
		$smarty->assign("s_cron", $cron_modules);
		$smarty->assign("priority", array(10, 20, 30, 40, 50, 60, 70, 80, 90));
		
		// $this->DebugArray($cron_modules, true); die();
	} else {
		trigger_error("File \"_modules_cron.xml\" is missing!", E_USER_ERROR);
		die();
	}
}
?>