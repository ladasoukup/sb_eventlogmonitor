<?php
define("AdminEmail", "root@soundboss.cz");
define("PageCharSet", "UTF-8");
define("Language", "cs");
define("UserAuth", true);

// SQL SETTINGS
define("EZSQL_DB_USER", "root");			// <-- db user
define("EZSQL_DB_PASSWORD", "root");		// <-- db password
define("EZSQL_DB_NAME", "sbeventlogmonitor");		// <-- db pname
define("EZSQL_DB_HOST", "localhost");			// <-- server host
//define("EZSQL_DB_CHARSET", "utf8");			// <-- server CHARSET (leave empty for default)
define("DB_PREFIX","sbEvtLog_");

// SMARTY SETTINGS
define("SMARTY_template_dir", "./templates/");
define("SMARTY_cache_dir", "./templates_cache/");
define("SMARTY_compile_check", true);
define("SMARTY_caching", false);
define("SMARTY_cache_lifetime", (60 * 120));

// PHP ERROR REPORTING
/*
define("ERROR_LOG_FILE", "./_logs/".date("Y-m-d").".log");
error_reporting(E_ALL & ~E_NOTICE);
ini_set("log_errors", true);
ini_set("error_log", ERROR_LOG_FILE);
ini_set("display_errors", true);
*/

?>
