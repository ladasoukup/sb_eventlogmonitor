'// WMI-EVENTS.VBS Collects Windows EventLogs and saves them in a formatted file for future parsing
'// Modified for eventlogmonitor project: http://sourceforge.net/projects/eventlogmonitor/
'// Modified by Joe Vandermark - Dow Jones & Company, joe.vandermark@dowjones.com


'// Declare Objects and Variables
Const FOR_APPENDING = 8
Const TRISTATE_USE_DEFAULT = -2
Const wbemFlagReturnImmediately = &h10
Const wbemFlagForwardOnly = &h20	

'// User configuration
Dim strDebug :strDebug = 2		'// Set to "1" if you run interactively and want to see each step of the collection
Dim strLog :strLog = 1		'// Set to "1" if you want to log event collection
Dim intLogDays :intLogDays = 14		'// Set to number of days to keep log files 
Dim strNewEvtDays :strNewEvtDays = 14		'// Days to go back in event logs when collecting on new systems
Dim intMaxEvents :intMaxEvents = 500		'// Limit maximum events to be read per remote server, per script run
Dim intPingTO :intPingTO = 750		'// Timeout in milliseconds to wait for each reply
Dim intPingSize :intPingSize = 32		'// Ping buffer size (bytes)
Dim intPings :intPings = 2		'// Number of echo requests to send
Dim strNotify :strNotify = 0		'// Set to "1" if you want to be mailing on collection failure (ping)
Dim strShowLogConfig :strShowLogConfig = 1   '// Set to "1" if you want to log eventlog configuration for each system

'// Email notification 
Dim strMailFrom, strMailServer, strMailTo, intMailPort, intSendusing
strMailFrom = "fromme@me.com"		'// Email Address of who is "sending" the notifications
strMailTo = "tome@me.com"		'// Email Address of where notification are sent
strMailServer = "mailserver.me.com"		'// Name or IP of SMTP Server
intMailPort = 25		'// Server port (typically 25)
intSendusing = 2		'// Send using 1=Local SMTP service, 2=Remote SMTP service
intMailPriority = 2		'// Mail priority 0=Low, 1=Normal, 2=High
'// END User configuration

Dim szDateMax
Dim objNetwork, strCollectorName
Set objNetwork = CreateObject("Wscript.Network")
strCollectorName = objNetwork.Computername
Set objNetwork = Nothing

Dim strStartTime :strStartTime = Now()
Dim intHour :intHour = Hour(Time)
Dim strLogsDir :strLogsDir = Replace(Wscript.ScriptFullName,"data\" & Wscript.ScriptName,"runlogs")
Dim strToday :strToday = GetDate(Now,"yyyy", 4) & GetDate(Now,"m", 2) & GetDate(Now,"d", 2)
Dim strLogFile :strLogFile = strToday & ".txt"
Dim strLastReported

Dim strStartHostTime

Dim objArgs
Set objArgs = Wscript.Arguments
Dim xmlDoc, xmlRoot, SpewEvents, temp

Dim fso
Set fso = Wscript.CreateObject("Scripting.FileSystemObject")
Dim strLine, wmi_target, strEventTypes, ret

'// If logging is enabled, ensure runlogs directory is present and do log cleanup
If strLog = 1 Then
	If Not fso.FolderExists(strLogsDir) Then fso.CreateFolder(strLogsDir)
	If intHour = 9 Then Call CleanOldFiles(strLogsDir,intLogDays,".txt")
End If

'/////////////////////
'// Functions and Subs

Sub Notify(strErr)
	If strDebug > "0" Then Wscript.Echo " *Sending alert notification to " & strMailTo

	Set objMessage = CreateObject("CDO.Message")
	objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = intSendusing
	objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = strMailServer
	objMessage.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = intMailPort
	objMessage.Configuration.Fields.Update

	objMessage.To = strMailTo
	objMessage.From = strMailFrom
	objMessage.Subject = "EventLogMonitor Notification - " & UCASE(strCollectorName)

	Set cdoConfig = CreateObject("CDO.Configuration")
	objMessage.Fields.Item("urn:schemas:httpmail:importance") = intMailPriority
	objMessage.Fields.Update

	objMessage.TextBody = strErr
	objMessage.Send
	set objMessage = nothing
End Sub

Function GetMaxDate(fso, strComputer)
	Dim fileState
	'On Error Resume Next
	szDateMax = ""
	If strDebug > "0" Then TraceLog("  - In GetMaxDate sub-routine...")

	If Not fso.FileExists(strComputer + ".dat") Then 
		fso.CreateTextFile(strComputer + ".dat")
	Else
		Set fileState = fso.OpenTextFile(strComputer + ".dat")
		szDateMax = Trim(fileState.ReadLine)
		fileState.Close
		Set fileState = Nothing
		On Error Goto 0
	End If
	If szDateMax = "" Then 
		szDateMax = GetDate(DateAdd("d",-strNewEvtDays,Now), "yyyy", 4) & GetDate(DateAdd("d",-strNewEvtDays,Now), "m", 2) & GetDate(DateAdd("d",-strNewEvtDays,Now), "d", 2) & "010000.000000-300"
		GetMaxDate = szDateMax
	End If
	If strDebug > 1 Then TraceLog("  - Using MaxDate of " & szDateMax)
End Function

Sub SaveMaxDate(fso, strComputer, szDateMax)
	Dim fileState
	If strDebug > 0 Then TraceLog("  - In SaveMaxDate sub-routine...")
	If strDebug > 1 Then TraceLog("  - Will save MaxDate of " & szDateMax)
	Set fileState = fso.CreateTextFile(strComputer + ".dat", True)
	fileState.WriteLine szDateMax
	fileState.Close
	Set fileState = Nothing
End Sub

Function GetEvents(strComputer, strEventTypes)
	Dim objWMIService, colItems, objItem, strOSName, strOSFullName, strComputerDomain, intEventsFound
	Dim strComputerDescription, strEventFilter
	If strDebug > "0" Then Wscript.echo " - In GetEvents sub-routine..."
	On Error Resume Next
	Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
	If Err.Number <> 0 Then
		TraceLog("   !!! Caught exception, skipping system " & strComputer & ".. Error Number: " & Err.Number & " Error Description: " & Err.Description)
		If Abs(Err.Number) = "2147417851" Then
			'// Try shell out to recycle WinMgmt
			'DOSCMD("psexec.exe \\" & strComputer & " net stop winmgmt /y")
			'DOSCMD("psexec.exe \\" & strComputer & " net start winmgmt")
		End If
		Err.Clear
		Exit Function
	End if

	'// GET Computer OS & Computer Description
	Set colItems = objWMIService.ExecQuery("SELECT Description,Name,ServicePackMajorVersion FROM Win32_OperatingSystem",,48)
	For Each objItem In colItems
		If InStr(objItem.Name, "|") Then
			strOSName = Left(objItem.Name, InStr(objItem.Name, "|") - 1)
		Else
			strOSName = objItem.Name
		End If
		strOSFullName = strOSName & " SP" & objItem.ServicePackMajorVersion
		strComputerDescription = objItem.Description
	Next
	
	'// GET Computer Domain
	Set colItems = objWMIService.ExecQuery("SELECT Domain FROM Win32_ComputerSystem",,48) 
	For Each objItem in colItems 
		strComputerDomain = objItem.Domain
	Next
	
	'// Set computer_last_reported
	strLastReported = GetDate(Now,"yyyy", 4) & "-" & GetDate(Now,"m", 2) & "-" & GetDate(Now,"d", 2) & " " & GetDate(Now,"h", 2) & ":" & GetDate(Now,"n", 2) & ":" & GetDate(Now,"s", 2)

	'// GET EVENTS
	If strDebug > "0" Then TraceLog(" - Retrieving Events")
	intEventsFound = 0
	
	Call GetMaxDate(fso, strComputer)
	
	'// Dow Jones filter out noisy FMS Source
	'// If you don't want to collect specific "noisy" events ever, use this..
	'// Nice if you get a noisy event often (10+ /sec) this will speed up collection
	strEventFilter = " AND SourceName <> 'JUNK'"

	If (strEventTypes = "2") Then
		If strDebug > "0" Then Wscript.Echo " - Collecting only Errors"
		strEventFilter = " AND EventType='1'"
	End If
	
	If (strEventTypes = "3") Then
		If strDebug > "0" Then Wscript.Echo " - Collecting only Errors and Warnings"
		strEventFilter = " AND (EventType='1' OR EventType='2')"
	End If
	
	If (strEventTypes = "4") Then
		If strDebug > "0" Then Wscript.Echo " - Collecting only Errors, Warnings and Informations"
		strEventFilter = " AND (EventType='1' OR EventType='2' OR EventType='3')"
	End If
	
	On Error Resume Next
	If strDebug > "0" Then TraceLog("   - SELECT * FROM Win32_NTLogEvent WHERE TimeWritten > '" & szDateMax & "'" & strEventFilter & "', ""WQL"", wbemFlagReturnImmediately + wbemFlagForwardOnly")
	Set colLoggedEvents = objWMIService.ExecQuery("SELECT * FROM Win32_NTLogEvent WHERE TimeWritten > '" & szDateMax & "'" + strEventFilter, "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
	If Err.Number <> 0 Then
		TraceLog("   !!! Caught exception, skipping system " & strComputer & ".. Error Number: " & Err.Number & " Error Description: " & Err.Description)
		Err.Clear
		Exit Function
	End if

	'// CODES - EventType
	'' 1 	Error"
	'' 2 	Warning"
	'' 3 	Information"
	'' 4 	Security audit success"
	'' 5 	Security audit failure
	
	'// strEventTypes
	'' 1 - ALL
	'' 2 - Error
	'' 3 - Error + Warning
	'' 4 - Error + Warning + Information

	SpewEvents = False

	Set xmlDoc = Wscript.CreateObject("Msxml2.DOMDocument")
	Set xmlRoot = xmlDoc.createElement("events")
	xmlDoc.appendChild(xmlRoot)

	'// Set Computer OS
	Set xmlEvent = xmlDoc.createElement("computer_os")
	xmlEvent.Text = strOSFullName
	xmlRoot.appendChild(xmlEvent)
	'// Set Computer Description
	set xmlEvent = xmlDoc.createElement("computer_description")
	xmlEvent.Text = strComputerDescription
	xmlRoot.appendChild(xmlEvent)
	'// Set Computer Domain
	set xmlEvent = xmlDoc.createElement("computer_domain")
	xmlEvent.Text = strComputerDomain
	xmlRoot.appendChild(xmlEvent)
	'// Set Computer Last Reported
	set xmlEvent = xmlDoc.createElement("computer_last_reported")
	xmlEvent.Text = strLastReported
	xmlRoot.appendChild(xmlEvent)

	For Each evt In colLoggedEvents
		SpewEvents = True

		If (CStr(evt.TimeGenerated) > szDateMax) Then
			szDateMax = CStr(evt.TimeGenerated)
		End If

		Set xmlEvent = xmlDoc.createElement("event")
		xmlRoot.appendChild(xmlEvent)

		Set xmlTemp = xmlDoc.createElement("code")
		xmlTemp.Text = evt.EventCode
		xmlEvent.appendChild(xmlTemp)

		Set xmlTemp = xmlDoc.createElement("type")
		xmlTemp.Text = evt.EventType
		xmlEvent.appendChild(xmlTemp)

		Set xmlTemp = xmlDoc.createElement("category")
		xmlTemp.Text = evt.CategoryString & ""
		xmlEvent.appendChild(xmlTemp)

		Set xmlTemp = xmlDoc.createElement("logfile")
		xmlTemp.Text = evt.Logfile
		xmlEvent.appendChild(xmlTemp)

		Set xmlTemp = xmlDoc.createElement("message")
		temp = evt.Message
		If (IsNull(temp)) Then
			temp = "n/a"
			If strDebug > "0" Then Wscript.Echo " - FIXING empty evt.Message"
		End If
		temp = replace(temp, "<", "&lt;")
		temp = replace(temp, ">", "&gt;")

		xmlTemp.Text = temp & ""
		xmlEvent.appendChild(xmlTemp)

		Set xmlTemp = xmlDoc.createElement("source")
		xmlTemp.Text = evt.SourceName & ""
		xmlEvent.appendChild(xmlTemp)

		Set xmlTemp = xmlDoc.createElement("time-generated")
		xmlTemp.Text = evt.TimeGenerated
		xmlEvent.appendChild(xmlTemp)

		Set xmlTemp = xmlDoc.createElement("user")
		xmlTemp.Text = evt.User & ""
		xmlEvent.appendChild(xmlTemp)
		
		intEventsFound = intEventsFound + 1
		If intEventsFound = intMaxEvents Then
			TraceLog(" - Exiting GetEvents, exceeded " & intMaxEvents & " new events. Last event inserted was generated at " & evt.TimeGenerated)
			Exit For
		End If
	Next
	If (SpewEvents) Then
		Dim fileXML
		Dim XMLcharset
		XMLcharset = "<?xml version=""1.0"" encoding=""UTF-16""?>"

		Set fileXML = fso.CreateTextFile(strComputer + "." + szDateMax + ".evt-xml", True, True)
		fileXML.Write XMLcharset
		fileXML.Write xmlDoc.xml
		fileXML.Close
	End If
	Call SaveMaxDate(fso,strComputer,szDateMax)
	If strDebug > "0" Then
		If intEventsFound = 1 Then
			TraceLog(" - Found 1 event.")
		Else
			TraceLog(" - Found " & intEventsFound & " events.")
		End If
		If strDebug > "0" Then Wscript.Echo " - GetEvents sub-routine complete. (Re)reading WMI-EVENTS.TXT..."
	End If
	On Error GoTo 0
End Function

Function GetDate(strDate, Part, Length)
	DIM strDatePart
	strDatePart = DatePart(Part, strDate)
	While Len(strDatePart) < Length
		strDatePart = "0" & strDatePart
	Wend
	GetDate = strDatePart
End Function

Sub TraceLog(Message)
	If strLog = 1 Then
		Dim oFSO, oLogFile
		Set oFSO = Wscript.CreateObject("Scripting.FileSystemObject")
		Set oLogFile = oFSO.OpenTextFile(strLogsDir & "\" & strLogFile, FOR_APPENDING, True, TRISTATE_USE_DEFAULT)
		oLogFile.WriteLine(Message)
		oLogFile.Close
		Set oLogFile = Nothing
	End If
	If strDebug > "0" Then
		Wscript.Echo Message
	End If
End Sub

Sub CleanOldFiles(FileDir,CleanDays,Filter)
	Dim ObjFS, ObjLogFile, ObjFile, ObjTopFolder, intDaysOld
	Set ObjFS = CreateObject("Scripting.FileSystemObject")
	Set ObjTopFolder = ObjFS.GetFolder(FileDir)
	TraceLog(Now)
	TraceLog(" CleanOldFiles Sub Running")
	TraceLog(" Working on folder " & FileDir)
	TraceLog(" Files older than " & CleanDays & " days")
	TraceLog(" Files matching filer: " & Filter)
	Wscript.Sleep 1000
	For each ObjLogFile in ObjTopFolder.files
		Set ObjFile = ObjFS.GetFile(ObjLogFile)
		If datediff("d",ObjFile.DateLastModified,Date()) > CleanDays and instr(lcase(ObjLogFile),lcase(Filter)) then
			TraceLog("  Removing file: " & FileDir & "\" & ObjFile.name & " (" & ObjFile.DateLastModified & ")")
			ObjFile.Delete
		End If
		Set ObjFile = nothing
	Next

	Set ObjTopFolder = nothing
	Set ObjFS = Nothing
End Sub

Function IsConnectible(strServer)
	If strDebug = 2 Then TraceLog("Attempting to ping " & strServer & "...")
	Dim oShell, oExCmd
	Set oShell = CreateObject("Wscript.Shell")
	Set oExCmd = oShell.Exec("ping -n " & intPings & " -w " & intPingTO & " -l " & intPingSize & " " & strServer)
	Select Case InStr(oExCmd.StdOut.Readall,"TTL=")
		Case 0 IsConnectible = False
		Case Else IsConnectible = True
	End Select
	Set oShell = Nothing
End Function

Function WMIDateStringToDate(dtmDate)
WScript.Echo dtm: 
	WMIDateStringToDate = CDate(Mid(dtmDate, 5, 2) & "/" & _
	Mid(dtmDate, 7, 2) & "/" & Left(dtmDate, 4) _
	& " " & Mid (dtmDate, 9, 2) & ":" & Mid(dtmDate, 11, 2) & ":" & Mid(dtmDate,13, 2))
End Function

Sub GetEvtLogConfig(strServer)
	Set objWMIService = GetObject("winmgmts:\\" & strServer & "\root\CIMV2")
	Set colItems = objWMIService.ExecQuery("SELECT * FROM Win32_NTEventlogFile", "WQL", wbemFlagReturnImmediately + wbemFlagForwardOnly)
	For Each objItem In colItems
		strLastModified = WMIDateStringToDate(objItem.LastModified)
		strLogfileName = objItem.LogfileName
		intNumberOfRecords = objItem.NumberOfRecords
		TraceLog("LogFile: " & strLogfileName & " last modified " & strLastModified & " has " & intNumberOfRecords & " records")
	Next
End Sub

Sub DOSCMD(strCMD)
	Dim WSHShell
	If strDebug > 2 Then TraceLog(strCmd)
	Set WSHShell = CreateObject("Wscript.Shell")
	WSHShell.Run "CMD /c" & strCMD, 1, 1
	set WSHShell=Nothing
End Sub

'/////////////////////
'// Start collection

Dim objWMITargets
Set objWMITargets = fso.OpenTextFile("WMI-EVENTS.txt")
TraceLog("------------------------------------------------------------------------" & vbCRLF & Now & " " & WScript.ScriptName & " STARTING")
objWMITargets.ReadLine
Do Until objWMITargets.AtEndOfStream
	'On Error Resume Next
	strLine = trim(objWMITargets.ReadLine)
	strLine = Split(strLine, ";")
	wmi_target = trim(strLine(0))
	strEventTypes = trim(strLine(1))
	TraceLog(Now & " Working on host " & wmi_target)
	strStartHostTime = Timer()
	'// Ping remote server and call GetEvents if up
	If IsConnectible(wmi_target) Then
		If strShowLogConfig = "1" Then 
			If strDebug > "0" Then TraceLog("Ping success, getting eventlog configuration")
			Call GetEvtLogConfig(wmi_target)
			If strDebug > "0" Then TraceLog("Start reading events")
		Else
			If strDebug > "0" Then TraceLog("Ping success, start reading events")
		End If
		ret = GetEvents(wmi_target, strEventTypes)
	Else
		TraceLog("*" & Now & " " & wmi_target & " was not pingable")
		If strNotify <> 0 Then Notify(Now & " " & wmi_target & " failed EventLog collection due to ping test failure")
	End If
	strTimeHostTaken = Timer() - strStartHostTime
	If strDebug > "0" Then TraceLog(Now & " Done with host " & wmi_target & ", took " & strTimeHostTaken & "s")
Loop
objWMITargets.Close
TraceLog(Now & " " & WScript.ScriptName & " COMPLETE, it took " & DateDiff("n",strStartTime,Now) & " minutes to run")

