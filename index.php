<?php
// SBEventLogMonitor 2006
// (c)2006-2007 Ladislav Soukup [root@soundboss.cz]
session_start();
define("APP_TOKEN", "SB_ELM");
setcookie("test_cookie", "OK");
include_once "./config.php";
include_once "./class_smarty/Smarty.class.php";
$smarty = new Smarty();
$smarty->template_dir = SMARTY_template_dir;
$smarty->compile_dir = SMARTY_cache_dir;
$smarty->cache_dir = SMARTY_cache_dir;
$smarty->compile_check = SMARTY_compile_check;
$smarty->caching = SMARTY_caching;
$smarty->cache_lifetime = SMARTY_cache_lifetime;
include_once "./class_core.php";
$core = new sb_core;
// START OUTPUT !!!
if (isset($_COOKIE["EventLog_LangID"])) $langID = $_COOKIE["EventLog_LangID"];
$core->GetDefaults();
$cacheID = $core->GetCacheID($langID);

if ($_GET["module"] == "rss") {
	// RSS feeds
	// TODO: enable / disable in global config
	header('Content-type: text/xml; charset='.PageCharSet, true);
} else {
	header('Content-type: text/html; charset='.PageCharSet, true);
	if (UserAuth) include_once "./user-auth.php";
}

$template_file = $_GET["module"] . ".tpl";
if (!$smarty->is_cached($template_file, $cacheID)) {
	// SQL CLASS
	include_once "./class_ezsql.php";
	$db->hide_errors();
	$db->query("SHOW TABLE STATUS FROM `".EZSQL_DB_NAME."`;");
	if ($db->last_error != null){
		$smarty->load_filter('output','i18n');
		$core->LoadTranslationTable(Language);
		$smarty->display("_error_mysql.tpl");
		die();
	}
	if (defined('EZSQL_DB_CHARSET')) $core->ezsql_set_charset(EZSQL_DB_CHARSET);
	include_once "./class_safesql.php";
	$safesql = new SafeSQL_MySQL;
	
	// TRANSLATION TABLE
	$smarty->load_filter('output','i18n');
	$core->GetTranslations();
	if (empty($GLOBALS['_i18n_LangTable_'][$langID])) $langID="cs";
	$core->LoadTranslationTable($langID);
	$smarty->assign("langID", $langID);
	
	// Check if MySQL tables requires update...
	if ($core->version != $core->GetConfig("app_version")) {
		$template_file = "MySQL_updater.tpl";
		$core->ModuleRun("MySQL_updater");
		$smarty->display($template_file);
		die();
	}
	
	
	// Check Access
	$group_access_denied = false;
	//set default group
	if ($_GET["sub_group"] == "-*-NULL-*-") {
		$all_sub_groups = $core->GetGroups(true);
		if (!isset($all_sub_groups[0])) $all_sub_groups[0] = "";
		$_GET["sub_group"] = $all_sub_groups[0];
		$_SERVER["QUERY_STRING"] = "module=".$_GET["module"]."&time_period=".$_GET["time_period"]."&sub_group=".$_GET["sub_group"];
	}
	
	if ($_SESSION["evtlog_user_group"] != "") {
		// check access to this module
		$module_access_level = $core->CanAccessModule($_GET["module"], $_SESSION["evtlog_user_group"]);
		if ($module_access_level == 0) {
			$core->ModuleRun("_menu");
			$smarty->display("_error_module_access_denied.tpl");
			die();
		}
		// check access to this group
		$group_access_level = $core->CanAccessGroup($_GET["sub_group"], $_SESSION["evtlog_user_group"]);
		if ($group_access_level == 0) {
			$group_access_denied = true;
		}
	}
	// MAIN MODULE
	$core->ModuleRun($_GET["module"]);
	$core->ModuleRun("_menu");
}
$smarty->display($template_file, $cacheID);

// END OUTPUT !!!
?>