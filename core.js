var win = null;
var IdArray = new Array();
var IdArray_pos = 0;
var ShowLoadingBar = true;
var WarningBeforePageLeave = false;
var PageUnloadWarningText = "...this text should be loaded by page template...";

window.onbeforeunload = PageUnload;
function PageUnload() {
	if (WarningBeforePageLeave == true) {
		// todo:  TRANSLATE...
		return PageUnloadWarningText;
	}
	
	if (ShowLoadingBar == true) {
		document.getElementById('img_loader').style.display = 'block';
	}
}

function SettingsHighlight(tab_row_ID, btn_submit_ID, showSaveWarning) {
	document.getElementById(tab_row_ID).style.background = '#FFED8D';
	document.getElementById(btn_submit_ID).style.background = "#dedcd6 url('templates_img/button_menu_bg.gif')";
	
	if (showSaveWarning == true) WarningBeforePageLeave = true;
}

function OpenWindow_build_settings(w, h) {
	LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
	TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
	settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',resizable=yes,scrollbars=yes';
	return settings;
}
function OpenWindow(mypage, wnd_name, w, h) {
	if (win) { win.close();	}
	settings = OpenWindow_build_settings(w, h);
	win = window.open(mypage, wnd_name, settings)
	win.focus();
}

function GuiSwitchLang() {
	var langID = document.getElementById("LangSelect").value;
	SetCookie("EventLog_LangID", langID);
	window.location.reload();
}

function SwitchSubGroup(sub_group, base_path) {
	base_path = replaceAll(base_path, "***SUB_GROUP***", sub_group);
	window.location.href = base_path;
}

function setAutoRefersh(refreshTime) {
	SetCookie('SBEventLogMonitor_refresh', refreshTime);
	setTimeout("window.location.reload()", (refreshTime * 1000));
}

function StartAutoRefresh() {
	var JSdo_AutoRefres = GetCookie('SBEventLogMonitor_refresh');
	if (JSdo_AutoRefres > 45) {
		setTimeout("window.location.reload()", (JSdo_AutoRefres * 1000));
		document.getElementById('page_auto_refresh').value = JSdo_AutoRefres;
	}
}

function IdArray_reset(id) {
	IdArray = new Array();
	IdArray_pos = 0;
}
function IdArray_add(id) {
	IdArray[IdArray_pos] = id;
	IdArray_pos++;
}
function IdArray_find(value, wnd) {
	var ret = -1;

	if (wnd == true) {
		for (loop=0; loop<opener.IdArray_pos; loop++) {
			if (opener.IdArray[loop] == value) ret = loop;
		}
	} else {
		for (loop=0; loop<IdArray_pos; loop++) {
			if (IdArray[loop] == value) ret = loop;
		}
	}
	return(ret);
}
function IdArray_prev(value, wnd) {
	var ret = -1;
	var currId = IdArray_find(value, wnd);
	var newId = currId - 1;
	
	if (newId < 0) newId = currId;
	if (wnd == true) {
		ret = opener.IdArray[newId];
	} else {
		ret = IdArray[newId];
	}
	return(ret);
}
function IdArray_next(value, wnd) {
	var ret = -1;
	var currId = IdArray_find(value, wnd);
	var newId = currId + 1;
	
	if (wnd == true) {
		if (newId > opener.IdArray_pos) newId = currId;
		ret = opener.IdArray[newId];
	} else {
		if (newId > IdArray_pos) newId = currId;
		ret = IdArray[newId];
	}
	return(ret);
}


function replaceAll(oldStr, findStr, repStr) {
	var srchNdx = 0;
	var newStr = "";
	while (oldStr.indexOf(findStr,srchNdx) != -1) {
		newStr += oldStr.substring(srchNdx,oldStr.indexOf(findStr,srchNdx));
		newStr += repStr;
		srchNdx = (oldStr.indexOf(findStr,srchNdx) + findStr.length);
	}
	newStr += oldStr.substring(srchNdx,oldStr.length);
	return newStr;
}

function SetCookie (name, value){
	var argv = SetCookie.arguments;
	var argc = SetCookie.arguments.lenght;
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;
	document.cookie = name + "=" + escape (value) + ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + ((path == null) ? "" : ("; path = " + path)) + ((domain == null) ? "" : ("; domain=" + domain)) + ((secure == true) ? "; secure" : "");
}

function GetCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function DelCookie(name) {
	createCookie(name,"",-1);
}