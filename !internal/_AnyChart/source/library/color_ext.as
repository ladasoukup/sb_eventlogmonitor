﻿class library.color_ext
{
	function color_ext()
	{
	}
	//transfer color
	function transfer(clr:Number,c:Number):Number
	{
		var r:Number = Math.floor(clr/65536);
		var g:Number = Math.floor((clr-r*65536)/256);
		var b:Number = Math.floor(clr-r*65536-g*256);
		r=Math.floor(r*c);
		if (r<0)
		{
			r=0;
		}
		if (r>255)
		{
			r=255;
		}
		g=Math.floor(g*c);
		if (g<0)
		{
			g=0;
		}
		if (g>255)
		{
			g=255;
		}
		b=Math.floor(b*c);
		if (b<0)
		{
			b=0;
		}
		if (b>255)
		{
			b=255;
		}
		var res:Number;
		res=r*65536+g*256+b;
		return (res);
	}
	//--------------
	function RGB2HSB(clr:Number):Object
	{
		var R:Number = Math.floor(clr/65536);
		var G:Number = Math.floor((clr-R*65536)/256);
		var B:Number = Math.floor(clr-R*65536-G*256);		
		
		var H=0;
		var S=0;
		var Br=0;
		
		var res=new Object();
		if ((B<=G)&&(G<R))
		{
			H=(G-B)*60/(R-B);
			Br=R;
			S=(R-B)*100/R;		
		}
		if ((B<R)&&(R<=G))
		{
			H=60+(G-R)*60/(G-B);
			Br=G;
			S=(G-B)*100/G;		
		}
		if ((R<=B)&&(B<G))
		{
			H=120+(B-R)*60/(G-R);
			Br=G;
			S=(G-R)*100/G;		
		}
		if ((R<G)&&(G<=B))
		{
			H=180+(B-G)*60/(B-R);
			Br=B;
			S=(B-R)*100/B;		
		}
		if ((G<=R)&&(R<B))
		{
			H=240+(R-G)*60/(B-G);
			Br=B;
			S=(B-G)*100/B;		
		}
		if ((G<B)&&(B<=R))
		{
			H=300+(R-B)*60/(R-G);
			Br=R;
			S=(R-G)*100/R;		
		}
		if((R==G)&&(G==B))
		{
			H=0;
			Br=R;
			S=0;		
		}
		res.H=Math.floor(H+0.5);
		res.S=Math.floor(S+0.5);
		if (Br==254)
		{
			res.B=99
		}
		if (Br==1)
		{
			res.B=1
		}
		if ((Br!=254)&&(Br!=1))
		{
			res.B=Math.floor(Br*100/255+0.5)
		}
		return res;
	} 
	//--------------------------
	function HSB2RGB(clr:Object):Number
	{
		var R:Number=0;
		var G:Number=0;
		var B:Number=0;
		clr.H = clr.H%360;
		if ((0<=clr.H)&&(clr.H<60))
		{
			R = clr.B*255/100;
			B = R*(1-clr.S/100);
			G = (R-B)*clr.H/60+B;	
		}
		if ((60<=clr.H)&&(clr.H<120))
		{
			G = clr.B*255/100;
			B = G*(1-clr.S/100);
			R = (G-B)*(1-(clr.H-60)/60)+B;
		}
		if ((120<=clr.H)&&(clr.H<180))
		{
			G = clr.B*255/100;
			R = G*(1-clr.S/100);
			B = (G-R)*(clr.H-120)/60+R;
		}
		if ((180<=clr.H)&&(clr.H<=240))
		{
			B = clr.B*255/100;
			R = B*(1-clr.S/100);
			G = (B-R)*(1-(clr.H-180)/60)+R;
		}
		if ((240<=clr.H)&&(clr.H<300))
		{
			B = clr.B*255/100;
			G = B*(1-clr.S/100);
			R = (B-G)*(clr.H-240)/60+G;
		}
		if ((300<=clr.H)&&(clr.H<360))
		{
			R = clr.B*255/100;
			G = R*(1-clr.S/100);
			B = (R-G)*(1-(clr.H-300)/60)+G;
		}
		R = Math.floor(R+0.5);
		G = Math.floor(G+0.5);
		B = Math.floor(B+0.5);
		return (R*65536+G*256+B);
	}
	//get colors
	function getColors(tone:Number,l:Number):Array
	{
		var res = new Array();
		var i:Number;
		
		var h:Number = RGB2HSB(tone).H;
		var s:Number = RGB2HSB(tone).S;
		var b:Number = RGB2HSB(tone).B;
		
		var dh:Number = 360/l;
		for (i=0;i<l;i++)
		{
			res[i] = HSB2RGB({H:i*dh,S:s,B:b});
		}
		
		return res;
	}
}