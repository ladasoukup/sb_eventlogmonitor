﻿class library.functions
{
	private var start_time:Number;
	private var my_interval:Number;
	
	private static var instance;
	
	function functions()
	{		
		functions.instance = this;
	}
	
	public static function getInstance():functions {
		if (!functions.instance)
			functions.instance = new functions();
		return functions.instance;
	}
	
	//get maximum array value
	public function getMax(arr:Array):Number
	{
		var max:Number;
		var i:Number;
		if (arr[0].length==undefined)
		{
			for (i=0;i<arr.length;i++)
			{
				arr[i] = Number(arr[i]);
				if (!isNaN(arr[i]) && (max == undefined || arr[i]>max))
				{
					max = arr[i];
				}
			}
		}else
		{
			var j:Number = 0;
			for (i=0;i<arr.length;i++)
			{
				for (j=0;j<arr[i].length;j++)
				{
					arr[i][j] = Number(arr[i][j]);
					if (!isNaN(arr[i][j]) && (max == undefined || arr[i][j]>max))
					{
						max = arr[i][j];
					}
				}
			}
		}
		return max;
	}
	//get minimum array value
	public function getMin(arr:Array):Number
	{
		var min:Number;
		var i:Number;
		if (arr[0].length==undefined)
		{
			for (i=0;i<arr.length;i++)
			{
				arr[i] = Number(arr[i]);
				if (!isNaN(arr[i]) && (min == undefined || arr[i]<min))
				{
					min = arr[i];
				}
			}
		}else
		{
			var j:Number;
			for (i=0;i<arr.length;i++)
			{
				for (j=0;j<arr[i].length;j++)
				{
					arr[i][j] = Number(arr[i][j]);
					if (!isNaN(arr[i][j]) && (min == undefined || arr[i][j]<min))
					{
						min = arr[i][j];
					}
				}
			}
		}
		return min;
	}
	//------------------------------------------
	function roundDecimal( num : Number, places : Number ) : Number
	{
    var places = Math.pow(10, places);
    return Math.round( num * places ) / places;
	}
	//------------------------------------------
	function config_text(txt:String,pos:Number,th_s:String,dec_s:String,prefix,postfix:String):String
	{
		var tmp = Number(txt); // storing as Number
		var i:Number = tmp > 0 ? Math.floor(tmp) : -Math.floor(Math.abs(tmp)); //closest Integer
		var f:Number = Number(txt) - i; //decimal digits
		var tmp_f:String = String(f);
		if (tmp_f.length>=3)
		{
			tmp_f = f>=0 ? tmp_f.substring(2,tmp_f.length) : tmp_f.substring(3,tmp_f.length);
			var zeros_count:Number = 0;
			while (tmp_f.charAt(zeros_count)=='0')
			{
				zeros_count++;
			}
			f = parseInt(tmp_f,10);
		}else
		{
			f = 0;
		}
		//set thousand separator
		var i_s:String = String(Math.abs(i));
		var new_i_s:String = '';
		if (i_s.length > 3)
		{
			var j:Number;
			for (j=(length(i_s)-1);j>=0;j--)
			{
				new_i_s = i_s.substr(j,1)+new_i_s;
				if (Math.round((j-length(i_s))/3)==((j-length(i_s))/3))
				{
					if (j>0)
						new_i_s = th_s + new_i_s;
				}
			}
		}else {
			new_i_s = i_s;
		}
			
		var f_s:String = String(f);
		for (j=0;j<zeros_count;j++)
		{
			f_s = '0'+f_s;
		}
		if (length(f_s)>pos)
		{
			f = Number('0.'+f_s);
			f_s = String(Math.round(f*Math.pow(10,pos))/Math.pow(10,pos));			
			f_s = f_s.substring(2,f_s.length);
		}
		if (length(f_s)<pos)
		{
			while (length(f_s)<pos)
			{
				f_s = f_s + '0';
			}
		}
		var new_f_s:String = f_s;
		var res:String;
		if (pos>0)
		{
			res = new_i_s + dec_s + new_f_s + postfix;
		}else
		{
			res = new_i_s + postfix;
		}
		if (tmp<0)
		{
			res = '-' + prefix + res;
		}else
		{
			res = prefix + res;
		}
		return res;
	}
	//grad to rad
	function Grad2Rad(num:Number):Number
	{
		var res = num*Math.PI/180;
		return res;
	}
	public function setDelay(d:Number,target_obj:Object):Void
	{
		this.start_time = getTimer();
		clearInterval(this.my_interval);
		this.my_interval = setInterval(check_delay,1,d,target_obj,this);
	}
	public function setHideDelay(d:Number,target_obj:Object):Void
	{
		this.start_time = getTimer();
		clearInterval(this.my_interval);
		this.my_interval = setInterval(check_delay_2,1,d,target_obj,this);
	}
	private function check_delay(d:Number,target_obj:Object,ths:Object):Void
	{
		if ((getTimer()-ths.start_time)>=d)
		{
			target_obj.paint();
			clearInterval(ths.my_interval);
		}
	}
	private function check_delay_2(d:Number,target_obj:Object,ths:Object):Void
	{
		if ((getTimer()-ths.start_time)>=d)
		{
			target_obj.nxt.paint();
			target_obj._hide();
			clearInterval(ths.my_interval);
		} 
	}
	
	public function createHintText(pattern:String, name:String, value:String):String {
		var argument:String = arguments[3];
		var blockName:String = arguments[4];
		
		name = (name == undefined) ? '' : name;
		value = (value == undefined) ? '' : value;
		argument = (argument == undefined) ? '' : argument;
		blockName = (blockName == undefined) ? '' : blockName;
		
		if (pattern.indexOf('{NAME}') != -1)
			pattern = this.replaceString(pattern,'{NAME}',name);
			
		if (pattern.indexOf('{VALUE}') != -1)
			pattern = this.replaceString(pattern,'{VALUE}',value);
			
		if (pattern.indexOf('{ARGUMENT}') != -1)
			pattern = this.replaceString(pattern,'{ARGUMENT}',argument);
		
		if (pattern.indexOf('{BLOCK_NAME}') != -1)
			pattern = this.replaceString(pattern,'{BLOCK_NAME}',blockName);
		
		return pattern;
	}
	
	public function getCandlestickHintText(pattern:String, name:String, open:String, close:String, high:String, low:String):String {

		if (pattern.indexOf('{NAME}') != -1)
			pattern = this.replaceString(pattern,'{NAME}',name);
		
		if (pattern.indexOf('{OPEN}') != -1)
			pattern = this.replaceString(pattern,'{OPEN}',open);
			
		if (pattern.indexOf('{CLOSE}') != -1)
			pattern = this.replaceString(pattern,'{CLOSE}',close);
			
		if (pattern.indexOf('{HIGH}') != -1)
			pattern = this.replaceString(pattern,'{HIGH}',high);
		
		if (pattern.indexOf('{LOW}') != -1)
			pattern = this.replaceString(pattern,'{LOW}',low);

		
		return pattern;
	}
	
	public function createHintTextWithOpenClose(pattern:String, name:String, open:String, close:String, blockName:String):String {
		if (pattern.indexOf('{NAME}') != -1)
			pattern = this.replaceString(pattern,'{NAME}',name);
			
		if (pattern.indexOf('{OPEN}') != -1)
			pattern = this.replaceString(pattern,'{OPEN}',open);
			
		if (pattern.indexOf('{CLOSE}') != -1)
			pattern = this.replaceString(pattern,'{CLOSE}',close);
			
		if (pattern.indexOf('{BLOCK_NAME}') != -1)
			pattern = this.replaceString(pattern,'{BLOCK_NAME}',blockName);
		
		return pattern;
	}
	
	public function replaceString(str:String, a:String, b:String):String {
		if (b == undefined)
			b = '';
		var res = str.split(a).join(b);
		return (res == undefined) ? '' : res;
	}
	
	public static function getTextFieldBoundBox(field:TextField):Object {
		var rotation:Number = field._rotation;
		field._rotation = 0;
		var initH:Number = field._height;
		field._rotation = rotation;

		while (rotation > 360)
			rotation -= 360;

		while (rotation < 0)
			rotation += 360;
			
		var boundBox:Object = {};
		if (rotation == 0) {
			boundBox.top = field._y;
			boundBox.bottom = field._y + field._height;
			boundBox.left = field._x;
			boundBox.right = field._x + field._width;
		}else if (rotation > 0 && rotation <= 90) {
			boundBox.top = field._y;
			boundBox.bottom = boundBox.top + field._height;
			boundBox.left = field._x - initH*Math.sin(rotation*Math.PI/180);
			boundBox.right = boundBox.left + field._width;
		}else if (rotation > 90 && rotation <= 180) {
			boundBox.top = field._y + initH*Math.cos(rotation*Math.PI/180);
			boundBox.bottom = boundBox.top + field._height;
			boundBox.left = field._x - field._width;
			boundBox.right = boundBox.left + field._width;
		}else if (rotation > 180 && rotation <= 270) {
			boundBox.top = field._y - field._height;
			boundBox.bottom = boundBox.top + field._height;
			boundBox.left = field._x - field._width - initH*Math.sin(rotation*Math.PI/180);
			boundBox.right = boundBox.left + field._width;
		}else if (rotation > 270) {
			boundBox.bottom = field._y + initH*Math.cos(rotation*Math.PI/180);
			boundBox.top = boundBox.bottom - field._height;
			boundBox.left = field._x;
			boundBox.right = boundBox.left + field._width;
		}
		return boundBox;
	}
	
	public static function changeTextFieldY(field:TextField,y:Number):Void {
		var boundBox:Object = getTextFieldBoundBox(field);
		var dy:Number = boundBox.top - field._y;
		field._y = y-dy;
	}
	
	public static function changeTextFieldX(field:TextField,x:Number):Void {
		var boundBox:Object = getTextFieldBoundBox(field);
		var dx:Number = boundBox.left - field._x;
		field._x = x-dx;
	}
	
	public static function isCrossBounds(field1:TextField,field2:TextField):Boolean {		
		return isYCrossBounds(field1,field2) && isXCrossBounds(field1,field2);
	}
	
	public static function isYCrossBounds(field1:TextField,field2:TextField):Boolean {
		var boundBox1:Object = getTextFieldBoundBox(field1);
		var boundBox2:Object = getTextFieldBoundBox(field2);		
		
		return isYCrossBoundBoxes(boundBox1,boundBox2);
	}
	
	public static function isXCrossBounds(field1:TextField,field2:TextField):Boolean {
		var boundBox1:Object = getTextFieldBoundBox(field1);
		var boundBox2:Object = getTextFieldBoundBox(field2);		
		
		return isXCrossBoundBoxes(boundBox1,boundBox2);
	}
	
	public static function isYCrossBoundBoxes(boundBox1:Object, boundBox2:Object):Boolean {
		var crossY1:Boolean = (boundBox1.top >= boundBox2.top && boundBox1.top <= boundBox2.bottom);
		var crossY2:Boolean = (boundBox1.bottom >= boundBox2.top && boundBox1.bottom <= boundBox2.bottom);
		
		return crossY1 || crossY2;
	}
	
	public static function isXCrossBoundBoxes(boundBox1:Object, boundBox2:Object):Boolean {
		var crossX1:Boolean = (boundBox1.left  >= boundBox2.left && boundBox1.left <= boundBox2.right);
		var crossX2:Boolean = (boundBox1.right >= boundBox2.left && boundBox1.right <= boundBox2.right);
		
		return crossX1 || crossX2;
	}
	
	public static function rotateField(field:TextField,angle:Number):Void {
		if (angle == 0)
			return;
			
		if (angle < 0)
			angle = 360 + angle;
			
		field._rotation = 0;
		var h:Number = field.textHeight + 4;
		var dx:Number = h/2 * Math.cos((90 - angle) * Math.PI/180);
		var dy:Number = - h/2 * Math.sin((90 - angle) * Math.PI/180);
		if (angle == 0) {
			dy = 0;
			dx = -1/2 * field._width;
		}
		if (angle == 180) {
			dy = h;
			dx = 1/2 * field._width;
		}		
		field._x += dx;
		field._y += dy;
		field._rotation = angle;
	}
}