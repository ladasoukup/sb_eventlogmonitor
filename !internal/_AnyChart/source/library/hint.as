﻿import mx.events.EventDispatcher
class library.hint
{
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;

	//position
	private var h_horizontal_position:String;
	private var h_vertical_position:String;
	//size
	private var h_width:Number;
	private var h_height:Number;
	private var h_auto_size:Boolean;
	//border
	public var border_enabled:Boolean;
	public var border_color:Number;
	public var border_size:Number;
	public var border_alpha:Number = 100;
	//background
	public var background_enabled:Boolean;
	public var background_color:Number;
	public var background_alpha:Number = 100;
	
	private var h_background:MovieClip;
	//text
	private var h_text:String;
	//text_format
	private var h_text_format:TextFormat;
	//hint text field
	private var h_text_field:TextField;
	//target movie:
	private var h_target_mc:MovieClip;
	private var h_movie:MovieClip;
	//--------------------------------------------------
	//getters and setters
	//set hint horizontal position
	function set horizontal_position(pos:String):Void
	{
		if ((pos!='left') and (pos!='right') and (pos!='center'))
		{
			_root.showError("Hint horizontal position sould be 'left', 'right' or 'center'");
		}else
		{
			h_horizontal_position = pos;
		}
	}
	//set hint vertical position
	function set vertical_position(pos:String):Void
	{
		if ((pos!='top') and (pos!='bottom'))
		{
			_root.showError("Hint vertical position should be 'top' or 'bottom'");
		}else
		{
			h_vertical_position = pos;
		}
	}
	//set hint width
	function set width(new_width:Number):Void
	{
		if (new_width<=0)
		{
			if (!h_auto_size)
			{
				_root.showError('Hint width should be positive');
			}
		}else
		{
			h_width = new_width;
			h_text_field._width = h_width;
		}
	}
	//set hint height
	function set height(new_height:Number):Void
	{
		if (new_height<=0)
		{
			if (!h_auto_size)
			{
				_root.showError('Hint height should be positive');
			}
		}else
		{
			h_height = new_height;
			h_text_field._height = h_height;
		}
	}
	//set hint auto size
	function set auto_size(as:Boolean):Void
	{
		h_auto_size = as;
		h_text_field.autoSize = h_auto_size;
	}	
	//set hint text
	function set text(txt:String):Void
	{
		h_text = txt;
		h_text_field.htmlText = txt;
	}
	//set hint text format
	function set text_format(tf:TextFormat):Void
	{
		h_text_format = tf;
		h_text_field.setTextFormat(h_text_format);
	}
	//----------------------------------------------------
	//class constructor:
	function hint(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		h_target_mc = target_mc;
	}
	
	private var tmp_mc:MovieClip;
	
	//show hint:
	function show()
	{
		h_movie = _root;
		var tmp_mc:MovieClip;
		tmp_mc = h_movie;
		this.tmp_mc = tmp_mc;
		var ths = this;
		//position
		var x:Number;
		var y:Number;
		var dx:Number;
		var dy:Number = 0;
		//show
		h_target_mc.onRollOver = h_target_mc.onDragOver = function()
		{
			ths.dispatchEvent ( {type: 'onShow'});
			x = tmp_mc._xmouse;
			y = tmp_mc._ymouse;
			tmp_mc.localToGlobal(localCords);
			//show hint text field
			tmp_mc.createEmptyMovieClip('hint_background',tmp_mc.getNextHighestDepth());
			ths.h_background = tmp_mc['hint_background'];
			tmp_mc.createTextField('hint_tf',tmp_mc.getNextHighestDepth(),x,y,ths.h_width,ths.h_height);			
			ths.h_background._x = x;
			ths.h_background._y = y;			
			ths.h_text_field = tmp_mc['hint_tf'];
			ths.h_text_field.selectable = false;
			ths.h_text_field.html = true;
			ths.h_text_field.autoSize = ths.h_auto_size;
			if (!ths.h_auto_size)
			{
				ths.h_text_field.multiline = true;
			}
			ths.h_text_field.html = true;
			ths.h_text_field.htmlText = ths.h_text;
			ths.h_text_field.setTextFormat(ths.h_text_format);
			//set position and dx and dy:
			switch (ths.h_horizontal_position)
			{
				case 'left':
				  dx = - ths.h_text_field._width;
				break;
				case 'center':
				  dx = - (ths.h_text_field._width/2);
				break;
			}
			switch (ths.h_vertical_position)
			{
				case 'top':
				  dy = - ths.h_text_field._height;
				break;
				case 'bottom':
				  dy = 0;
				break;
			}
			ths.h_text_field._x += dx;
			ths.h_text_field._y += dy;
			
			var localCords:Object = {x:ths.h_text_field._x,y:ths.h_text_field._y};			
			var newCords:Object = {};
			ths.h_text_field._parent.localToGlobal(localCords);
			if (localCords.x < 5) {
				newCords.x = 5;
				newCords.y = 0;
				ths.h_text_field._parent.globalToLocal(newCords);
				ths.h_text_field._x = newCords.x;
			}else if ((localCords.x + ths.h_text_field._width)>= (Stage.width - 5)) {
				newCords.x = Stage.width - 5 - ths.h_text_field._width;
				newCords.y = 0;
				ths.h_text_field._parent.globalToLocal(newCords);
				ths.h_text_field._x = newCords.x;
			}
			if (localCords.y < 5) {
				newCords.x = 0;
				newCords.y = 5;
				ths.h_text_field._parent.globalToLocal(newCords);
				ths.h_text_field._y = newCords.y;
			}else if ((localCords.y + ths.h_text_field._height) >= (Stage.height - 5)) {
				newCords.x = 0;
				newCords.y = Stage.height - 5 -  ths.h_text_field._height;
				ths.h_text_field._parent.globalToLocal(newCords);
				ths.h_text_field._y = newCords.y;
			}
			
			if (ths.background_enabled)
			{
				ths.h_background.beginFill(ths.background_color,ths.background_alpha);
			}
			ths.h_background._x = ths.h_text_field._x;
			ths.h_background._y = ths.h_text_field._y;
			ths.h_background.moveTo(0,0);
			if (ths.border_enabled)
			{
				ths.h_background.lineStyle(ths.border_size,ths.border_color,ths.border_alpha);
			}
			ths.h_background.lineTo(ths.h_text_field._width,0);
			ths.h_background.lineTo(ths.h_text_field._width,ths.h_text_field._height);
			ths.h_background.lineTo(0,ths.h_text_field._height);
			ths.h_background.lineTo(0,0);
			if (ths.background_enabled)
			{
				ths.h_background.endFill();
			}

			//moving:
			this.onMouseMove = function()
			{
				ths.h_text_field._x = tmp_mc._xmouse+dx;
  			    ths.h_text_field._y = tmp_mc._ymouse+dy;
				
				var localCords:Object = {x:ths.h_text_field._x,y:ths.h_text_field._y};			
				if (localCords.x < 5)
					ths.h_text_field._x = 5;
				else if ((localCords.x + ths.h_text_field._width) >= (Stage.width - 5))
					ths.h_text_field._x = Stage.width - 5 - ths.h_text_field._width;
				if (localCords.y < 5)
					ths.h_text_field._y = 5;
				else if ((localCords.y + ths.h_text_field._height) >= (Stage.height - 5))
					ths.h_text_field._y = Stage.height - 5 - ths.h_text_field._height;
					
				ths.h_background._x = ths.h_text_field._x;
				ths.h_background._y = ths.h_text_field._y;
			}
			
		}
		//remove:
		h_target_mc.onRollOut = h_target_mc.onDragOut = function()
		{
			ths.dispatchEvent ( {type: 'onUnShow'});
			ths.h_text_field.removeTextField();
			tmp_mc['hint_tf'].removeTextField();
			tmp_mc['hint_background'].removeMovieClip();
			delete this.onMouseMove;
		}
	}
	
	function remove() {
		this.h_text_field.removeTextField();
		this.h_background.removeMovieClip();
		tmp_mc['hint_tf'].removeTextField();
	}
}