﻿/**
 * Class for embedding AnyChart into Flash
 * @version 1.0
 * @copyright AnyChart.Com
 */
 
class AnyChart {
	
	/**
	 * complete handler for loading anychart swf	 
	 */
	public var onLoad:Function;
	
	/**
	 * Loader for anychart swf
	 * @private
	 */
	private var loader:MovieClipLoader;
	
	/**
	 * Container for anychart swf
	 * @private
	 */
	private var targetMovie:MovieClip;	
	
	/**
	 * Constructor
	 * @return AnyChart
	 */
	public function AnyChart(targetMovie:MovieClip) {
		this.targetMovie = targetMovie;
		loader = new MovieClipLoader();
		loader.addListener(this);
	}
	
	/**
	 * Set anychart swf
	 * @param String swf swf path
	 */
	public function setSWF(swf:String):Void {
		loader.loadClip(swf,targetMovie);
	}
	
	/**
	 * Set xml for chart
	 * @param String data
	 */
	public function setXMLSourceAsString(data:String):Void {
		targetMovie.setXMLTextCall(data);
	}
	
	/**
	 * Set only xml data for chart
	 * @param String data
	 */
	public function setXMLDataAsString(data:String):Void {
		targetMovie.setXMLDataCall(data);
	}
	
	/**
	 * Sets XML file for chart
	 * @param String path path to xml
	 */
	public function setXMLFile(path:String):Void {
		targetMovie.XMLFile = path;
		targetMovie.only_data = false;
		targetMovie.gotoAndPlay(2);
	}
	
	
	/**
	 * Load init handler for anychart swf
	 * @private
	 */
	private function onLoadInit(targetMovie):Void {
		this.onLoad();
	}
}