﻿this._lockroot = true;
for (var j:String in _root) {
	if (typeof(_root[j]) == 'string' || typeof(_root[j]) == 'number')
		continue;
	if (!j instanceof Function && j!='unshowAbout' && j!='showAbout' && j != 'instance1' && j!='verdana_embed') {
		_root[j].removeMovieClip();
		_root[j].unloadMovie();
		_root[j].removeTextField();
		_root[j] = null;
	}		
}
#include "frames/buildInfo.as"

_root.STATUS = (_root.waiting == undefined) ? 'Waiting for data' : _root.waiting;

//frame 1
_root.KEY_LOADED = true;

import library.registration;
registration.def();
import flash.external.*;

//------------------------------------------------------------------
//
// load reginfo
//
//------------------------------------------------------------------

var path:String = _root._url;
var qIndex:Number = path.indexOf('?');
if (qIndex != undefined)
{
	path = path.substr(0,qIndex);
}
path = path.substr(0,path.lastIndexOf('/'));

var regInfoMovie:MovieClip = this.createEmptyMovieClip('regInfoContainer',this.getNextHighestDepth());
regInfoMovie._alpha = 0;


//--------------------------------------------------------------------------------------
var methodName:String = "SetXMLText";
var instance:Object = null;
var method:Function = setXMLTextCall;
ExternalInterface.addCallback(methodName, instance, method);
methodName = "SetXMLData";
instance = null;
method = setXMLDataCall;
ExternalInterface.addCallback(methodName, instance, method);

function setXMLTextCall() 
{
	_root.ws.initContainer();
	if (!_root.KEY_LOADED)
		return;
	_root.errorReport.removeMovieClip();
	_root.errorReport.unloadMovie();
	_root.ticksContainer.removeMovieClip();
	_root.ticksContainer.unloadMovie();
	for (var j:String in _root) {
		_root[j].remove();
		_root[j].removeTextField();
		_root[j].removeMovieClip();
		_root[j].unloadMovie();
		_root.ws = null;
	}
	_root['containerForLabels'].removeMovieClip();
	XMLsText = arguments[0];
	_root.XMLData = XMLsText;
	_root.only_data = false;
	
	var doc:XML = new XML();
	doc.ignoreWhite = true;
	doc.parseXML(obr(XMLsText));	
	doc.ignoreWhite = true;
	if (doc.firstChild.nodeName != 'root' || !doc.firstChild) {
		_root.XMLFile = null;
		_root.XMLData = null;
		_root.doc = null;
		_root.showError("No chart data available");
	}else {
		_root.doc = doc;
		_root.gotoAndPlay(3);
	}
}

this.setXMLTextCall = setXMLTextCall;



function setXMLDataCall()
{	
	_root.errorReport.removeMovieClip();
	_root.errorReport.unloadMovie();
	_root.ticksContainer.removeMovieClip();
	_root.ticksContainer.unloadMovie();
	for (var j in _root) {
		if (_root[j].remove) {
			if (j != 'ws')
				_root[j].remove();
			else			
				_root[j].removeLabels();
		}
	}
	_root['containerForLabels'].removeMovieClip();
	var UNDELETED_LIST:Array = new Array('errorReport','trial_tf','ws_xml','ws','itemHandler2','i','fileParams','obr','showError','onEnterFrame','onData','menu','my_cm','method','instance','loader','loaderListener','regInfoMovie','qIndex','unshowAbout','showAbout','itemHandler','setXMLDataCall','setXMLTextCall','trial_field','backgroundOfText','tf_0','field_bg_0','legend_movie_0','chart_movie_0','errorReport');
	if (!_root.KEY_LOADED)
		return;
	XMLsText = arguments[0];
	_root.XMLData = XMLsText;
	_root.only_data = true;
	_root.gotoAndPlay(2);
}

this.setXMLDataCall = setXMLDataCall;

//global fonts embeding
_global.verdana_embed_tf = verdana_embed.getTextFormat();
verdana_embed._alpha = 0;
//-----------------------------------------------
//trial caption
this._lockroot = true;
//set menu
var my_cm:ContextMenu = new ContextMenu();
my_cm.hideBuiltInItems();
my_cm.customItems.push(new ContextMenuItem("About AnyChart", itemHandler));
my_cm.customItems.push(new ContextMenuItem("Print chart",printItemHandler));
function itemHandler(obj, item) 
{
	showAbout();
}

function printItemHandler(obj, item) {
//	ws.w_background_mc._visible = false;
	var bgClear:MovieClip = ws.w_background_mc.createEmptyMovieClip('bgClear',ws.w_background_mc.getNextHighestDepth());
	bgClear.beginFill(0xFFFFFF,100);
	bgClear.moveTo(0,0);
	bgClear.lineTo(Stage.width,0);
	bgClear.lineTo(Stage.width,Stage.height);
	bgClear.lineTo(0,Stage.height);
	bgClear.lineTo(0,0);
	bgClear.endFill();
	var printJob:PrintJob = new PrintJob();
	// display print dialog box
	if (printJob.start()) {
		// boolean to track whether addPage succeeded, change this to a counter
		// if more than one call to addPage is possible
		var pageAdded:Boolean = false;
	
		pageAdded = printJob.addPage(_root,{xMin:0,xMax:Stage.width,yMin:0,yMax:Stage.height},{printAsBitmap:true});
		// send pages from the spooler to the printer
		if (pageAdded) {
			printJob.send(); 
		}
	}
//	ws.w_background_mc._visible = true;
	bgClear.removeMovieClip();
}

//--------------------------------------------
//only data refresh
var only_data:Boolean = false;
if (_parent.XMLFile!=undefined)
{
	XMLFile = unescape(_parent.XMLFile);
}
if (_parent.XMLSource!=undefined)
{
	XMLData = unescape(_parent.XMLSource);
}
if (_parent.XMLData!=undefined)
{
	XMLData = unescape(_parent.XMLSource);
}
var is_data_loaded:Boolean = false;
var is_movie_loaded:Boolean = false;
_root.onData = function()
{
	is_data_loaded = true;
	if (XMLFile!=undefined)
	{
		_root.XMLFile = XMLFile;
	}
	if (XMLSource!=undefined)
	{
		_root.XMLSource = XMLSource;
	}
	if (XMLData!=undefined)
	{
		_root.XMLData = XMLData;
	}
	if (_root.KEY_LOADED && ((XMLFile!=undefined) or (XMLSource!=undefined) or (XMLData!=undefined))&&(is_movie_loaded))
	{
		this.gotoAndPlay(2);
	}
}
//use codepage for multilanguage
System.useCodepage = false;
//-----------------------------------------------------------------------------------------------
function showAbout()
{
	if (!_root.KEY_LOADED)
		return;
	_root._alpha = 50;
	_root.attachMovie('about','_about',_root.getNextHighestDepth());
	_root['_about']._alpha = 150;
	if (ws!=undefined)
	{
		_root['_about']._x = ws.x + ws.width/2;
		_root['_about']._y = ws.y + ws.height/2;
		if (_root['_about']._width>ws.width)
		{
			_root['_about']._width = ws.x + ws.width;
			_root['_about']._x = _root['_about']._width/2;
		}
		if (_root['_about']._height>ws.height)
		{
			_root['_about']._height = ws.y + ws.height;
			_root['_about']._y = _root['_about']._height/2;
		}
	}else
	{
		_root['_about']._x = Stage.width/2;		
		_root['_about']._y = Stage.height/2;
	}
}
function unshowAbout()
{
	if (!_root.KEY_LOADED)
		return;
	_root._alpha = 100;
	_root['_about'].removeMovieClip();
}
//--------------------------------------------
//preloader
_root.onEnterFrame = function()
{
	if ((this.getBytesLoaded()==this.getBytesTotal())&&(this.getBytesTotal()>40))
	{
		is_movie_loaded = true;
		if ((XMLFile!=undefined) || (XMLSource!=undefined) || (XMLData!=undefined))
		{
			if (_root.KEY_LOADED)
			{
				_root.onEnterFrame = function(){};
				_root.PRELOADER.statusText.text = (_root.waiting == undefined) ? 'Waiting for data' : _root.waiting;
				_root.gotoAndStop(2);
			}
		}
	}
}
//--------------------------------------------
if (_root.errorReport) {
	_root.errorReport.unloadMovie();
	_root.errorReport.removeMovieClip();
}
//--------------------------------------------
//showing error method
_root.showError = function(txt:String):Void {
	_root.errorReport = _root.attachMovie('error','errorReport',_root.getNextHighestDepth(),{x:Stage.width/2,y:Stage.height/2});
	_root.errorReport._visible = false;
	_root.errorReport.onCreate = function():Void {
		this._x = Stage.width/2;
		this._y = Stage.height/2;
		this._visible = true;
		_root.errorReport.setText(txt);
	}
	throw new Error(txt);
}

function obr(str)
{
	/**var r = unescape(str);
	var res='';
	for (var i=0;i<length(r);i++)
	{
		if (r.charAt(i)=='>')
		{
			res+=r.charAt(i);
			k=i;
			while (r.charAt(k)!='<')
			{
				k++;
				if (k>=length(str))
				{
					break;
				}
				i=k-1;
			}
		}else
		{
			res+=r.charAt(i);
		}
	}**/
	return (str);
}

_root.menu = my_cm;
Stage.scaleMode = 'noScale';
Stage.align = 'TL';
stop();