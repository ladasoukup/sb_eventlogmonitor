﻿_root.onMouseDown = undefined;
//loading xml document or set XML with using XMLData
stop();
if (!fileParams) {
	fileParams = new Array();
	for (var j:String in _root)
	{
		if (typeof(_root[j]) == 'string')
			if (j != 'methodName' && j!='SetXMLData' && j != 'path' && j!='BUILD_STR' && j!='VERSION' && j!='XMLFile' && j!='STATUS' && j!='$version') {
				fileParams.push({name:j,value:_root[j]});					
			}
	}
}
if (XMLData!=undefined)
{
	var doc:XML = new XML();
	doc.ignoreWhite = true;
	doc.parseXML(obr(XMLData));	
	doc.ignoreWhite = true;
	if (doc.firstChild.nodeName != 'root' || !doc.firstChild) {
		XMLFile = null;
		XMLData = null;
		doc = null;
		_root.showError("No chart data available");
	}else {
		_root.errorReport.unloadMovie();
		_root.errorReport.removeMovieClip();
		_root.ticksContainer.removeMovieClip();
		_root.ticksContainer.unloadMovie();
		_root.ws.initContainer(true);
		_root.gotoAndPlay(3);
	}
}else if (XMLFile!=undefined)
{
	var doc:XML = new XML();
	var date:Date = new Date();
	var url:String = new String();
	var initUrl:String = XMLFile;
	if (XMLFile.indexOf('?')==-1)
	{
		if (DEBUG_MODE == true)
			url = XMLFile;
		else
			url = XMLFile +'?XMLCallDate='+date.getTime();
	}else
	{
		url = XMLFile;
		for (var i:Number = 0;i<fileParams.length;i++)
		{
			url += '&'+fileParams[i]['name']+'='+fileParams[i]['value'];
		}
		if (DEBUG_MODE != true)
			url +=  '&XMLCallDate='+date.getTime();
	}
	doc.ignoreWhite = true;
	_root.STATUS = (_root.loading == undefined) ? 'Loading data...' : _root.loading;	
	//on load event function:
	doc.onLoad = function(success:Boolean):Void {
		if (success) {
			switch (this.status) {
				case 0 :
					for (var j:String in _root) {
						_root[j].remove();
					}
					_root.errorReport.unloadMovie();
					_root.errorReport.removeMovieClip();
					_root.ticksContainer.removeMovieClip();
					_root.ticksContainer.unloadMovie();
					_root.ws.initContainer(true);
					_root.only_data = false;
					_root.onMouseDown = undefined;
					if (this.firstChild.nodeName != 'root' || !this.firstChild) {
						doc = null;
						_root.showError("No chart data available");
					}else {
						_root.gotoAndPlay(3);
					}
						
					break;
				case -2 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("A CDATA section was not properly terminated.");
					break;
				case -3 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("The XML declaration was not properly terminated.");
					break;
				case -4 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("The DOCTYPE declaration was not properly terminated.");
					break;
				case -5 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("A comment was not properly terminated.");
					break;
				case -6 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("An XML element was malformed.");
					break;
				case -7 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("Out of memory.");
					break;
				case -8 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("An attribute value was not properly terminated.");
					break;
				case -9 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("A start-tag was not matched with an end-tag.");
					break;
				case -10 :
					_root.onMouseDown = function() {
						_root.gotoAndPlay(1);
					}
					_root.showError("An end-tag was encountered without a matching start-tag.");
					break;
				default :
					_root.showError("An unknown error has occurred.");
					break;
			}

			return;
		}
		_root.onMouseDown = function() {
			_root.gotoAndPlay(1);
		}
		_root.showError("XML loading failed:<br />"+initUrl);
	}
	doc.onHTTPStatus = function(httpStatus:Number) {
		this.httpStatus = httpStatus;
		if(httpStatus < 100)
//        	_root.showError("Flash error while loading xml");
			return;
		else if(httpStatus < 200)
			_root.showError("Informational error while loading xml");
		else if(httpStatus < 300)
			return;
		else if(httpStatus < 400)
			return;
		else if(httpStatus < 500)
			_root.showError("Client error while loading xml");
		else if(httpStatus < 600)
			_root.showError("Server error while loading xml");
	}
	doc.load(url);
}
//-----------------------------------

stop();