﻿class objects.trend
{
	//line painig positions
	private var l_x:Number;
	private var l_y:Number;
	private var l_end_x:Number;
	private var l_end_y:Number;
	
	private var l_x_pos:Number;
	private var l_y_pos:Number;
	
	//line orientation
	private var l_orientation:String;
	
	//line attributes
	private var l_size:Number;
	private var l_color:Number;
	private var l_alpha:Number;
	
	//line text
	private var l_text:String;
	private var l_text_position:String;
	private var l_text_rotation:Number;
	private var l_text_format:TextFormat;
	
	//line value and maximum and minimum
	private var l_start_value;	
	private var l_end_value;
	private var l_maximum_value:Number;
	private var l_minimum_value:Number;
	private var l_width:Number;
	private var l_height:Number;
	
	//line movie
	private var l_mc:MovieClip;
	
	//------------------------------------
	function set x(x_:Number):Void
	{
		l_x_pos = x_;
	}
	function set y(y_:Number):Void
	{
		l_y_pos = y_;
	}
	//set line values
	function set start_value(val):Void
	{
		l_start_value = val;
	}
	//set line end value
	function set end_value(val):Void
	{
		l_end_value = val;
	}
	function get start_value()
	{
		return l_start_value;
	}
	function get end_value()
	{
		return l_end_value;
	}
	//set maximum painted value
	function set maximum_value(max:Number):Void
	{
		l_maximum_value = max;
	}
	//set minimum value
	function set minimum_value(min:Number):Void
	{
		l_minimum_value = min;
	}
	//set paint area attributes
	function set width(w:Number):Void
	{
		l_width = w;
	}
	function set height(h:Number):Void
	{
		l_height = h;
	}
	//set text
	function set text(txt:String):Void
	{
		l_text = txt;
	}
	function set text_rotation(r:Number):Void
	{
		l_text_rotation = r;
	}
	//set text position
	function set text_position(pos:String):Void
	{
		if (l_orientation=='horizontal')
		{
			if ((pos!='left') and (pos!='right'))
			{
				_root.showError("line object text position should be 'left' or 'right'");
			}else
			{
				l_text_position = pos;
			}
		}else
		{
			if ((pos!='top') and (pos!='bottom'))
			{
				_root.showError("line object text position should be 'top' or 'bottom'");
			}else
			{
				l_text_position = pos;
			}
		}
	}
	//set text format
	function set text_format(tf:TextFormat):Void
	{
		l_text_format = tf;
	}
	//set line size
	function set size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("line size should be between 0 and 255");
		}else
		{
			l_size = s;
		}
	}
	//set line color
	function set color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("line color should be between 0 and 0xFFFFFF");			
		}else
		{
			l_color = clr;
		}
	}
	//set line alpha
	function set alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("line alpha should be between 0 and 100");
		}else
		{
			l_alpha = a;
		}
	}
	//------------------------------------
	//constructor
	function trend(target_mc:MovieClip,o:String)
	{
		var pos:Object = {x:0,y:0};
		target_mc._parent.localToGlobal(pos);		
		target_mc = workspace.workspace_.getInstance().getObjectsContainer();
		target_mc._parent.globalToLocal(pos);
		target_mc._x = pos.x;
		target_mc._y = pos.y;
		l_size = 1;
		l_alpha = 100;
		l_color = 0xFF0000;
		l_text_rotation = 0;
		if ((o!='horizontal') and (o!='vertical'))
		{
			_root.showError("line orientation should be 'horizontal' or 'vertical'");
		}else
		{
			l_orientation = o;
		}
		var i:Number = target_mc.getNextHighestDepth();		
		l_mc = target_mc.createEmptyMovieClip('line_obj_movie_'+i,i);		
	}
	//paint
	function paint():Void
	{
		var pos:Object = {x:l_x_pos + workspace.workspace_.getInstance().x,y:l_y_pos + workspace.workspace_.getInstance().y};
		_root.localToGlobal(pos);
		workspace.workspace_.getInstance().getObjectsContainer().globalToLocal(pos);
		l_mc._x = pos.x;
		l_mc._y = pos.y;

		get_position();
		l_mc.lineStyle(l_size,l_color,l_alpha,true,"none","none");
		l_mc.moveTo(l_x,l_y);
		l_mc.lineTo(l_end_x,l_end_y);
		//show text
		var x_pos:Number;
		var y_pos:Number;
		var dx:Number;
		var dy:Number;
		
		var pos:Object = {};
		if (l_orientation=='horizontal')
		{
			dy = -0.5;
			y_pos = l_y;
			
			pos.x = 0;
			pos.y = y_pos;
			l_mc.localToGlobal(pos);
			workspace.workspace_.getInstance().workspace_mc.globalToLocal(pos);
			if (l_text_position=='left')
			{
				pos.x = workspace.workspace_.getLeftX();
				dx = -1;
			}else
			{
				pos.x = workspace.workspace_.getRightX();
				dx = 0;
			}			
		}else
		{
			dx = -0.5;
			x_pos = l_x;
			pos.x = x_pos;
			pos.y = 0;
			l_mc.localToGlobal(pos);
			workspace.workspace_.getInstance().workspace_mc.globalToLocal(pos);
			if (l_text_position=='top')
			{
				y_pos = workspace.workspace_.getTopY();
				dy = -1;
			}else
			{
				y_pos = workspace.workspace_.getBottomY();
				dy = 0;
			}
		}
		if (l_text!=undefined)
		{
			var id:Number = workspace.workspace_.getInstance().container.getNextHighestDepth();
			workspace.workspace_.getInstance().container.createTextField("line_text_f_"+id,id,pos.x,pos.y,0,0);
			var f:TextField = workspace.workspace_.getInstance().container["line_text_f_"+id];
			
			f.selectable = false;
			f.autoSize = true;
			if (l_text_rotation==0)
			{
				f.html = true;
				f.htmlText = l_text;
				f.setTextFormat(l_text_format);
			}else
			{
				f.embedFonts = true;
				f.text = l_text;
				var tf:TextFormat = _global[l_text_format.font];
				tf.size = l_text_format.size;
				tf.color = l_text_format.color;
				f.setTextFormat(tf);
				f._rotation = l_text_rotation;
			}
			f._x += dx*f._width;
			f._y += dy*f._height;
		}
	}
	function remove():Void
	{
		l_mc.removeMovieClip();
	}
	//-----------------------------------------------
	private function get_position():Void
	{
		var isInversed:Boolean = arguments[0] != undefined ? Boolean(arguments[0]) : false;
		var max:Number;
		if (l_orientation=='horizontal')
		{
			max = l_height;
		}else
		{
			max = l_width;
		}
		if (isInversed) {
			var tmp:Number = l_maximum_value;			
			l_maximum_value = l_minimum_value;
			l_minimum_value = tmp;
		}
		var d:Number = max/(l_maximum_value-l_minimum_value);
		if (l_orientation=='horizontal')
		{
			l_x = workspace.workspace_.getLeftX();
			l_end_x = workspace.workspace_.getRightX();
			var pos1:Object = {x:l_x,y:0};
			var pos2:Object = {x:l_end_x,y:0};
			workspace.workspace_.getInstance().localToGlobal(pos1);
			workspace.workspace_.getInstance().localToGlobal(pos2);
			l_mc.globalToLocal(pos1);
			l_mc.globalToLocal(pos2);
			l_x = pos1.x;
			l_end_x = pos2.x;
			
			l_y = (l_maximum_value-l_start_value)*d;
			l_end_y = (l_maximum_value-l_end_value)*d;
		}else
		{
			l_x = (l_start_value-l_minimum_value)*d;
			l_end_x = (l_end_value-l_minimum_value)*d;
			
			l_y = 0;
			l_end_y = l_height;
			var pos1:Object = {y:l_y,y:0};
			var pos2:Object = {y:l_end_y,y:0};
			workspace.workspace_.getInstance().localToGlobal(pos1);
			workspace.workspace_.getInstance().localToGlobal(pos2);
			l_mc.globalToLocal(pos1);
			l_mc.globalToLocal(pos2);
			l_y = pos1.y;
			l_end_y = pos2.y;
		}
	}
}