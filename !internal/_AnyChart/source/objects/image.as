﻿class objects.image
{
	//position
	private var i_x:Number;
	private var i_y:Number;
	//size
	private var i_width:Number;
	private var i_height:Number;
	private var i_position:String;
	//attributes
	private var i_url:String;
	private var i_url_target:String;
	private var i_source:String;	
	//image movie
	private var i_mc:MovieClip;
	//-------------------------------------
	function set x(new_x:Number):Void
	{
		i_x = new_x;
	}
	function set y(new_y:Number):Void
	{
		i_y = new_y;
	}
	function set width(w:Number):Void
	{
		if (w<=0)
		{
			_root.showError("width should be positive");
		}else
		{
			i_width = w;
		}
	}
	function set height(h:Number):Void
	{
		if (h<=0)
		{
			_root.showError("height should be positive");
		}else
		{
			i_height = h;
		}
	}
	function set position(p:String):Void
	{
		if ((p!='stretch') and (p!='tile') and (p!='center'))
		{
			_root.showError("image position values are 'stretch','tile' and 'center'");
		}else
		{
			i_position = p;
		}
	}
	function set url(u:String):Void
	{
		i_url = u;
	}
	function set url_target(u:String):Void
	{
		i_url_target = u;
	}
	function set source(s:String):Void
	{
		i_source = s;
	}
	//-------------------------------------
	function image(target_mc:MovieClip)
	{
		i_url_target = '_blank';
		i_position = 'center';
		var i:Number = target_mc.getNextHighestDepth();
		i_mc = target_mc.createEmptyMovieClip('obj_'+i,i);
	}
	//--------------------------------------
	function show():Void
	{
		var ths = this;
		if (i_position=='tile')
		{
			i_mc.createEmptyMovieClip('jpg',i_mc.getNextHighestDepth());
			i_mc._x = i_x;
			i_mc._y = i_y;
			var i_loader = new MovieClipLoader();
			i_loader.loadClip(i_source,i_mc.jpg);
			i_loader.onLoadInit = function(target)
			{
				var hg:Number = target._width;
				var wd:Number = target._height;
				var cnt_x:Number = ths.i_width/wd;
				var cnt_y:Number = ths.i_height/hg;
				var i:Number;
				var j:Number;
				for (i=0;i<cnt_x;i++)
				{
					for (j=0;j<cnt_y;j++)
					{
						ths.i_mc.createEmptyMovieClip('jpg_'+i+':'+j,ths.i_mc.getNextHighestDepth());
						loadMovie(ths.i_source,ths.i_mc['jpg_'+i+':'+j]);
					    ths.i_mc['jpg_'+i+':'+j]._x = wd*i;
					    ths.i_mc['jpg_'+i+':'+j]._y = hg*j;
					}
				}
			}
		}else
		{
			i_mc._x = i_x;
			i_mc._y = i_y;
			i_mc.createEmptyMovieClip('jpg',i_mc.getNextHighestDepth());
			var i_loader = new MovieClipLoader();
			i_loader.loadClip(i_source,i_mc.jpg);
			i_loader.onLoadInit = function(target) 
  		    {
			    if (ths.i_position=='stretch')
			    {
				    target._height = ths.i_height;
				    target._width = ths.i_width;	
			    }
			    if (ths.i_position=='center')
			    {
			  	    target._x = ths.i_width/2-target._width/2;
				    target._y = ths.i_height/2-target._height/2;
			    }
			}
		}
		if (i_url!=undefined)
		{
			i_mc.onRelease = function()
			{
				getURL(ths.i_url,ths.i_url_target);
			}
		}
	}	
	//---------------------------------------
	function remove():Void
	{
		i_mc.removeMovieClip();
	}
}