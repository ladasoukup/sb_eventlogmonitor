﻿class objects.area
{
	//area painting properties
	private var a_x:Number;
	private var a_y:Number;
	private var a_end_x:Number;
	private var a_end_y:Number;
	
	//area position
	private var a_x_pos:Number;
	private var a_y_pos:Number;
	
	//area orientation
	private var a_orientation:String;
	
	//area lines attributes
	private var a_lines_enabled:Boolean;
	private var a_size:Number;
	private var a_color:Number;
	private var a_alpha:Number;
	
	//background attributes
	private var a_background_enabled:Boolean;
	private var a_background_color:Number;
	private var a_background_alpha:Number;
	
	//lines text
	private var a_first_text:String;
	private var a_second_text:String;
	private var a_text_position:String;
	private var a_text_rotation:Number;
	private var a_text_format:TextFormat;
	
	public var text_x_offset:Number = 0;
	public var text_y_offset:Number = 0;
	
	//line value and maximum and minimum
	private var a_first_value;	
	private var a_second_value;
	
	//config position attributes
	private var a_maximum_value:Number;
	private var a_minimum_value:Number;
	private var a_width:Number;
	private var a_height:Number;
	
	//area movie
	private var a_mc:MovieClip;
	
	//------------------------------------
	//set area positions
	function set x(x_:Number):Void
	{
		a_x_pos = x_;
	}
	function set y(y_:Number):Void
	{
		a_y_pos = y_;
	}
	//set first line value
	function set first_value(val):Void
	{
		a_first_value = val;
	}
	//set second line value
	function set second_value(val):Void
	{
		a_second_value = val;	
	}
	function get first_value()
	{
		return a_first_value;
	}
	function get second_value()
	{
		return a_second_value;
	}
	function set text_rotation(r:Number):Void
	{
		a_text_rotation = r;
	}
	//set maximum painted value
	function set maximum_value(max:Number):Void
	{
		a_maximum_value = max;
	}
	//set minimum value
	function set minimum_value(min:Number):Void
	{
		a_minimum_value = min;
	}
	//set paint area attributes
	function set width(w:Number):Void
	{
		a_width = w;
	}
	function set height(h:Number):Void
	{
		a_height = h;
	}
	//set text
	function set first_text(txt:String):Void
	{
		a_first_text = txt;
	}
	function set second_text(txt:String):Void
	{
		a_second_text = txt;
	}
	//set text position
	function set text_position(pos:String):Void
	{
		if (a_orientation=='horizontal')
		{
			if ((pos!='left') and (pos!='right'))
			{
				_root.showError("area object text position should be 'left' or 'right'");
			}else
			{
				a_text_position = pos;
			}
		}else
		{
			if ((pos!='top') and (pos!='bottom'))
			{
				_root.showError("area object text position should be 'top' or 'bottom'");
			}else
			{
				a_text_position = pos;
			}
		}
	}
	//set text format
	function set text_format(tf:TextFormat):Void
	{
		a_text_format = tf;
	}
	//turns lines on\off
	function set lines_enabled(e:Boolean):Void
	{
		a_lines_enabled = e;
	}
	//set lines size
	function set size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("area lines size should be between 0 and 255");
		}else
		{
			a_size = s;
		}
	}
	//set lines color
	function set color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("area lines color should be between 0 and 0xFFFFFF");			
		}else
		{
			a_color = clr;
		}
	}
	//set lines alpha
	function set alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("area lines alpha should be between 0 and 100");
		}else
		{
			a_alpha = a;
		}
	}
	//set backgorund enabled
	function set background_enabled(e:Boolean):Void
	{
		a_background_enabled = e;
	}
	//set background color
	function set background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("background color should be between 0 and 0xFFFFFF");
		}else
		{
			a_background_color = clr;
		}
	}
	//set background opacity
	function set background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("background alpha should be between 0 and 100");
		}else
		{
			a_background_alpha = a;
		}
	}
	//------------------------------------
	//constructor
	function area(target_mc:MovieClip,o:String)
	{
		var pos:Object = {x:0,y:0};
		target_mc._parent.localToGlobal(pos);		
		target_mc = workspace.workspace_.getInstance().getObjectsContainer();
		target_mc._parent.globalToLocal(pos);
		target_mc._x = pos.x;
		target_mc._y = pos.y;
		
		a_text_rotation = 0;
		a_lines_enabled = true;
		a_size = 1;
		a_alpha = 100;
		a_color = 0xFF0000;
		a_background_enabled = true;
		a_background_color = 0xFF0000;
		a_background_alpha = 30;
		if ((o!='horizontal') and (o!='vertical'))
		{
			_root.showError("area orientation should be 'horizontal' or 'vertical'");
		}else
		{
			a_orientation = o;
		}
		var i:Number = target_mc.getNextHighestDepth();
		a_mc = target_mc.createEmptyMovieClip('area_obj_movie_'+i,i);		
		
		a_text_format = new TextFormat();
		a_text_format.font = 'Verdana';
	}
	//paint
	function paint():Void
	{
		var isInversed:Boolean = arguments[0] == undefined ? false : Boolean(arguments[0]);
		a_first_value = Number(a_first_value);
		a_second_value = Number(a_second_value);

		var pos:Object = {x:a_x_pos + workspace.workspace_.getInstance().x,y:a_y_pos + workspace.workspace_.getInstance().y};
		_root.localToGlobal(pos);
		workspace.workspace_.getInstance().getObjectsContainer().globalToLocal(pos);
		a_mc._x = pos.x;
		a_mc._y = pos.y;

		get_position(isInversed);
		//paint
		if (a_background_enabled)
		{
			a_mc.beginFill(a_background_color,a_background_alpha);
		}
		//--
		if (a_orientation=='horizontal')
		{
			a_mc.lineStyle(a_size,a_color,a_alpha,true,"none","none");
			a_mc.moveTo(a_x,a_y);
			a_mc.lineTo(a_end_x,a_y);
			a_mc.lineStyle();
			a_mc.lineTo(a_end_x,a_end_y);
			a_mc.lineStyle(a_size,a_color,a_alpha,true,"none","none");
			a_mc.lineTo(a_x,a_end_y);
			a_mc.lineStyle();
			a_mc.lineTo(a_x,a_y);
		}else
		{
			a_mc.lineStyle();
			a_mc.moveTo(a_x,a_y);
			a_mc.lineTo(a_end_x,a_y);
			a_mc.lineStyle(a_size,a_color,a_alpha,true,"none","none");
			a_mc.lineTo(a_end_x,a_end_y);
			a_mc.lineStyle();
			a_mc.lineTo(a_x,a_end_y);
			a_mc.lineStyle(a_size,a_color,a_alpha,true,"none","none");
			a_mc.lineTo(a_x,a_y);
		}
		//--
		if (a_background_enabled)
		{
			a_mc.endFill();
		}
		//show text
		var x_pos:Number;
		var y_pos:Number;
		var x_pos_:Number;
		var y_pos_:Number;
		var dx:Number;
		var dy:Number;
		
		var pos1:Object = {};
		var pos2:Object = {};
		
		if (a_orientation=='horizontal')
		{
			dy = -0.5;
			y_pos = a_y;
			y_pos_ = a_end_y;
			pos1.x = 0;
			pos1.y = y_pos;
			pos2.x = 0;
			pos2.y = y_pos_;
			a_mc.localToGlobal(pos1);
			a_mc.localToGlobal(pos2);
			workspace.workspace_.getInstance().workspace_mc.globalToLocal(pos1);
			workspace.workspace_.getInstance().workspace_mc.globalToLocal(pos2);
			if (a_text_position=='left')
			{
				x_pos = x_pos_ = workspace.workspace_.getLeftX();
				dx = -1;
			}else
			{
				x_pos_ = x_pos = workspace.workspace_.getRightX();
				dx = 0;
			}
			pos1.x = x_pos;
			pos2.x = x_pos_;
		}else
		{
			dx = -0.5;
			x_pos = a_x;
			x_pos_ = a_end_x;
			pos1.y = 0;
			pos1.x = x_pos;
			pos2.y = 0;
			pos2.x = x_pos_;
			a_mc.localToGlobal(pos1);
			a_mc.localToGlobal(pos2);
			workspace.workspace_.getInstance().workspace_mc.globalToLocal(pos1);
			workspace.workspace_.getInstance().workspace_mc.globalToLocal(pos2);
			if (a_text_position=='top')
			{
				y_pos = y_pos_ = workspace.workspace_.getTopY();
				dy = -1;
			}else
			{
				y_pos = y_pos_ = workspace.workspace_.getBottomY();
				dy = 0;
			}
			pos1.y = y_pos;
			pos2.y = y_pos_;
		}
		if (a_first_text!=undefined)
		{
			var id:Number = workspace.workspace_.getInstance().container.getNextHighestDepth();
			workspace.workspace_.getInstance().container.createTextField("line_text_f1_"+id,id,pos1.x,pos1.y,0,0);
			var f:TextField = workspace.workspace_.getInstance().container["line_text_f1_"+id];
			f.selectable = false;
			f.autoSize = true;
			if (a_text_rotation==0)
			{
				f.html = true;
				f.htmlText = a_first_text;
				f.setTextFormat(a_text_format);
			}else
			{
				f.embedFonts = true;
				f.text = a_first_text;
				var tf:TextFormat = _global[a_text_format.font];
				tf.size = a_text_format.size;
				tf.color = a_text_format.color;
				f.setTextFormat(tf);
				f._rotation = a_text_rotation;				
			}
			f._x += dx*f._width;
			f._y += dy*f._height;
			f._x += this.text_x_offset;
			f._y += this.text_y_offset;
		}
		if (a_second_text!=undefined)
		{
			var id:Number = workspace.workspace_.getInstance().container.getNextHighestDepth();
			workspace.workspace_.getInstance().container.createTextField("line_text_f1_"+id,id,pos2.x,pos2.y,0,0);
			var f:TextField = workspace.workspace_.getInstance().container["line_text_f1_"+id];
			f.selectable = false;
			f.autoSize = true;
			if (a_text_rotation==0)
			{
				f.html = true;
				f.htmlText = a_second_text;
				f.setTextFormat(a_text_format);
			}else
			{
				f.embedFonts = true;
				f.text = a_second_text;
				var tf:TextFormat = _global[a_text_format.font];
				tf.size = a_text_format.size;
				tf.color = a_text_format.color;
				f.setTextFormat(tf);
				f._rotation = a_text_rotation;				
			}
			f._x += dx*f._width;
			f._y += dy*f._height;
			f._x += this.text_x_offset;
			f._y += this.text_y_offset;
		}
	}
	private function remove():Void
	{
		a_mc.removeMovieClip();
	}
	//-----------------------------------------------
	private function get_position(isInversed:Boolean):Void
	{
		var max:Number;
		if (a_orientation=='horizontal')
		{
			max = a_height;
		}else
		{
			max = a_width;
		}
		if (isInversed) {
			var tmp:Number = a_maximum_value;
			a_maximum_value = a_minimum_value;
			a_minimum_value = tmp;
		}
		var d:Number = max/(a_maximum_value-a_minimum_value);
		if (a_orientation=='horizontal')
		{
			var pos:Object = {x:0,y:0};
			workspace.workspace_.getInstance().workspace_mc.localToGlobal(pos);
			a_mc.globalToLocal(pos);
			
			var pos1:Object = {x:workspace.workspace_.getRightX(),y:0};
			workspace.workspace_.getInstance().workspace_mc.localToGlobal(pos1);
			a_mc.globalToLocal(pos1);
			
			a_x = pos.x;
			a_end_x = pos1.x;
			
			a_y = (a_maximum_value-a_first_value)*d;
			a_end_y = (a_maximum_value-a_second_value)*d;;
		}else
		{
			var pos:Object = {x:0,y:0};
			workspace.workspace_.getInstance().workspace_mc.localToGlobal(pos);
			a_mc.globalToLocal(pos);
			
			var pos1:Object = {y:workspace.workspace_.getBottomY(),x:0};
			workspace.workspace_.getInstance().workspace_mc.localToGlobal(pos1);
			a_mc.globalToLocal(pos1);
			
			a_x = (a_first_value-a_minimum_value)*d;
			a_end_x = (a_second_value-a_minimum_value)*d;
			
			a_y = pos.y;
			a_end_y = pos1.y;
		}
	}
}