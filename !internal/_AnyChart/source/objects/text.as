﻿class objects.text
{
	//text field
	private var t_text_field:TextField;
	private var t_url:String;
	private var t_url_target:String;
	private var t_text_rotation:Number;
	private var t_text_format:TextFormat;
	private var t_text_:String;
	
	private var t_background:MovieClip;
	
	public var background_color:Number = 0xFFFFFF;
	public var background_enabled:Boolean = false;
	public var background_alpha:Number = 100;
	
	public var border_enabled:Boolean = false;
	public var border_size:Number = 1;
	public var border_color:Number = 0x000000;
	public var border_alpha:Number = 100;
	
	//-----------------------------------
	//getters and setters
	function set x(new_x:Number):Void
	{
		t_text_field._x = new_x;
	}
	function set y(new_y:Number):Void
	{
		t_text_field._y = new_y;
	}
	function set width(w:Number):Void
	{
		t_text_field._width = w;
	}
	function set height(h:Number):Void
	{
		t_text_field._height = h;
	}
	function set auto_size(e:Boolean):Void
	{
		t_text_field.autoSize = e;
		if (!e)
		{
			t_text_field.multiline = true;
		}
	}
	
	function set text_(txt:String):Void
	{
		t_text_ = txt;
		t_text_field.htmlText = txt;
	}
	function set text_format(tf:TextFormat):Void
	{
		t_text_format = tf;
		t_text_field.setTextFormat(tf);
		if (t_text_rotation!=0)
		{
			t_text_field.html = false;
			t_text_field.embedFonts = true;
			tf = _global[t_text_format.font];
			tf.size = t_text_format.size;
			tf.color = t_text_format.color;
			t_text_field.text = t_text_;
			t_text_field.setTextFormat(tf);
			t_text_field._rotation = t_text_rotation;
		}
	}
	function set url(u:String):Void
	{
		t_url = u;
	}
	function set url_target(t:String):Void
	{
		t_url_target = t;
	}
	function set text_rotation(r:Number):Void
	{
		t_text_rotation = r;
	}
	//constructor
	function text(target_mc:MovieClip)
	{
		t_text_rotation = 0;
		var i:Number = 0;
		t_url_target = '_blank';
		while (target_mc['tf_'+i]!=undefined)
		{
			i++;
		}
		t_background = target_mc.createEmptyMovieClip('field_bg_'+i,target_mc.getNextHighestDepth());
		target_mc.createTextField('tf_'+i,target_mc.getNextHighestDepth(),0,0,0,0);
		t_text_field = target_mc['tf_'+i];
		t_text_field.html = true;
		t_text_field.selectable = false;
	}
	function show():Void
	{		
		if (t_text_field.autoSize=='none')
		{
			t_text_field.multiline = true;
			t_text_field.wordWrap = true;
		}
		if (this.border_enabled)
		{
			t_background.lineStyle(this.border_size,this.border_color,this.border_alpha);
		}
		if (this.background_enabled)
		{
			t_background.beginFill(this.background_color,this.background_alpha);
		}
		t_background._x = t_text_field._x;
		t_background._y = t_text_field._y;
		t_background.moveTo(0,0);
		t_background.lineTo(t_text_field._width,0);
		t_background.lineTo(t_text_field._width,t_text_field._height);
		t_background.lineTo(0,t_text_field._height);
		t_background.lineTo(0,0);
		if (this.background_enabled)
		{
			t_background.endFill();
		}
		t_background._rotation = this.t_text_field._rotation;
		if (t_text_rotation==0)
		{
			if (t_url!=undefined)
			{
				t_text_field.htmlText = "<a href='"+t_url+"' target='"+t_url_target+"'>"+t_text_field.htmlText+"</a>";
			}else
			{
				t_text_field.htmlText = t_text_field.htmlText;
			}
		}
	}
	function remove():Void
	{
		t_text_field.removeTextField();
	}
}