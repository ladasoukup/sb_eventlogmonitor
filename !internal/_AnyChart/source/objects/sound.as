﻿class objects.sound
{
	private var s_source:String;
	private var s_start_second:Number;
	private var s_loops:Number;
	
	//----------------------------------------
	private var s_count:Number;
	private var s_sound:Sound;
	private var numLoadID:Number;
	//----------------------------------------
	//getters and setters
	//set source
	function set source(s:String):Void
	{
		s_source = s;
	}
	//set start second
	function set start_second(n:Number):Void
	{
		s_start_second = n;
	}
	//set loops
	function set loops(l:Number):Void
	{
		s_loops = l;
	}
	//-------------------------------------------
	//constructor
	function sound()	
	{
		s_sound = new Sound();
		s_count = 0;
		s_start_second = 0;
		s_loops = 1;
	}
	//--------------
	function start()
	{
		s_sound.loadSound(s_source,false);
		numLoadID = setInterval(timer,1000,this);
	}
	//--------------
	private function timer(ths):Void
	{
		if (ths.s_count==ths.s_start_second)
		{
			ths.start_sound();
			clearInterval(ths.numLoadID);
			delete ths.numLoadID;
		}
		ths.s_count++;
	}
	//----------------
	private function start_sound():Void
	{
		if (s_sound.getBytesTotal()==s_sound.getBytesLoaded())
		{
			s_sound.start(0,s_loops);
		}
	}
}