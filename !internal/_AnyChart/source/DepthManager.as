﻿class DepthManager {
	
	public static var WORKSPACE_BACKGROUND_DEPTH:Number = 1;
	public static var WORKSPACE_DEPTH:Number = 2;
	public static var OBJECTS_CONTAINER_BACK_DEPTH:Number = 3;
	public static var CHART_DEPTH:Number = 4;
	public static var OBJECTS_CONTAINER_FRONT_CHART_DEPTH:Number = 6;
	public static var OBJECTS_CONTAINER_BACK_GRID_DEPTH:Number = 7;
	public static var GRID_START_DEPTH:Number = 8;
	public static var OBJECTS_CONTAINER_FRONT_GRID:Number = 12;
	public static var TICKS_CONTAINER:Number = 13;
	public static var LEGEND_DEPTH:Number = 14;
	public static var LABELS_CONTAINER_DEPTH:Number = 15;
	
	public static var OBJECTS_CONTAINER_DEPTH:Number;
	
	public static function setGridBottom():Void {
		CHART_DEPTH = 8;		
		GRID_START_DEPTH = 4;
		OBJECTS_CONTAINER_BACK_GRID_DEPTH = OBJECTS_CONTAINER_BACK_DEPTH;
		OBJECTS_CONTAINER_FRONT_GRID = 7;
		OBJECTS_CONTAINER_FRONT_CHART_DEPTH = 10;
	}
	
	public static function setObjectsTopChart():Void {
		OBJECTS_CONTAINER_DEPTH = OBJECTS_CONTAINER_FRONT_CHART_DEPTH;
	}
	
	public static function setObjectsBottomGrid():Void {
		OBJECTS_CONTAINER_DEPTH = OBJECTS_CONTAINER_BACK_GRID_DEPTH;
	}
	public static function setObjectsTopGrid():Void {
		OBJECTS_CONTAINER_DEPTH = OBJECTS_CONTAINER_FRONT_GRID;
	}

}