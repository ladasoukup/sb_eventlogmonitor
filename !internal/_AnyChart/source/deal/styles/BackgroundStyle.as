﻿import styles.Style;

class styles.BackgroundStyle extends Style {
	
	public var enabled:Boolean = true;
	public var color:Number = 0xFFFFFF;
	public var alpha:Number = 100;
	
	public function BackgroundStyle() {
		super();
		enabled = true;
		color = 0xFFFFFF;
		alpha = 100;
	}
		
	public static function convertFromStyle(style:Style):BackgroundStyle
	{
		var res:BackgroundStyle = new BackgroundStyle();
		if ((style!=null)&&(style.id!=null))
		{
			res.id = style.id;
			res.enabled = style.enabled;
			res.type = style.type;
			if (style.color != null)
				res.color = Number(style.color);
			if (style.alpha != null)
				res.alpha = Number(style.alpha);
		}
		return res;
	}
}