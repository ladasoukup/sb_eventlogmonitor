﻿import styles.Style;

class styles.BorderStyle extends Style {

	public var enabled:Boolean = true;
	public var color:Number = 0x000000;
	public var alpha:Number = 100;
	public var thickness:Number = 1;
		
	public function BorderStyle() {
		super();
		enabled = true;
		color = 0x000000;
		alpha = 100;
		thickness = 1;
	}
		
	public static function convertFromStyle(style:Style):BorderStyle {
		var res:BorderStyle = new BorderStyle();
		if (style != null) {
			res.id = style.id;
			res.enabled = style.enabled;
			res.type = style.type;
			if (style.color != null)
				res.color = Number(style.color);
			if (style.alpha != null)
				res.alpha = Number(style.alpha);
			if (style.thickness != null)
				res.thickness = Number(style.thickness);
		}
		return res;
	}
}