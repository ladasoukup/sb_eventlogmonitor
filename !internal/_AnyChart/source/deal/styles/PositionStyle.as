﻿import styles.Style

class styles.PositionStyle extends Style {
	
	public var left:Number = 0;
	public var top:Number = 0;
	public var radius:Number = 0;
	public var align:String = 'left';
	public var position:String = 'absolute';
		
	public function PositionStyle() {
		left = top = radius = 0;
		align = 'left';
		position = 'absolute';
	}
		
	public static function convertFromStyle(style:Style):PositionStyle {
		var res:PositionStyle = new PositionStyle();			
		if (style != null) {
			res.id = style.id;
			res.type = style.type;
			if (style.position != null)
				res.position = style.position;
			if (style.left != null)
				res.left = Number(style.left);
			if (style.top != null)
				res.top = Number(style.top);
			if (style.radius != null)
				res.radius = Number(style.radius);
			if (style.align != null)
				res.align = String(style.align);
		}
		return res;
	}
}