﻿import errors.ErrorMessage;
	
dynamic class styles.Style {
	
	public var id:String;
	public var type:String;
		
	public static function getFromXMLAttributes(node:XML):Style
	{
		var style:Style = new Style();
		for (var attribute:String in node.attributes) {
			if (attribute=='enabled') {
				if (node.attributes[attribute]=='yes')
					style['enabled'] = true;
				else
					style['enabled'] = false;
			}else {
				style[attribute] = node.attributes[attribute];
			}
		}
		for (var i:Number = 0;i<node.childNodes.length;i++) {
			style[node.childNodes[i].nodeName] = node.childNodes[i];
		}
		return style;
	}
	
	public function checkType(needType:String,errorMessage:String):Void	{
		if (needType != this.type)
			_root.showError(errorMessage);
	}

	public function checkExist(paramName:String):Void {
		if (this[paramName] == undefined) {
			var errorMessage:String = ErrorMessage.getAttributeRequiredMessage(paramName,'<style id="'+this.id+'" ...');
			_root.showError(errorMessage);
		}
	}
}