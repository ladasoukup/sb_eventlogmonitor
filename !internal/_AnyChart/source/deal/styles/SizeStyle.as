﻿import styles.Style;

class styles.SizeStyle extends Style {
	
	public var length:Number;
	public var width:Number;
	public var height:Number;
	public var radius:Number;
	public var innerRadius:Number;
	public var outerRadius:Number;
	public var startAngle:Number;
	public var endAngle:Number;
		
	public static function convertFromStyle(style:Style):SizeStyle {
		var res:SizeStyle = new SizeStyle();
		if (style != null) {
			res.id = style.id;
			res.type = style.type;
			if (style.width != null)
				res.width = Number(style.width);
			if (style.height != null)
				res.height = Number(style.height);
			if (style.radius != null)
				res.radius = Number(style.radius);
			if (style.innerRadius != null)
				res.innerRadius = Number(style.innerRadius);
			if (style.outerRadius != null)
				res.outerRadius = Number(style.outerRadius);
			if (style.startAngle != null)
				res.startAngle = Number(style.startAngle);
			if (style.endAngle != null)
				res.endAngle = Number(style.endAngle);
			if (style.length != null)
				res.length = Number(style.length);
		}
		return res;
	}
}