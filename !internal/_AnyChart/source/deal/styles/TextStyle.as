﻿import styles.Style;

class styles.TextStyle extends Style {

	public var color:Number = 0x000000;
	public var size:Number = 12;
	public var font:String = 'Verdana';	
	public var align:String = 'left';
	public var bold:Boolean = false;
	public var italic:Boolean = false;
	public var underline:Boolean = false;
	
	public function TextStyle() {
		super();
		color = 0x000000;
		size = 12;
		font = 'Verdana';
		align = 'left';
		bold = false;
		italic = false;
		underline = false;
	}
	
	public function getTextFormat():TextFormat {		
		var tf:TextFormat = new TextFormat();
		tf.color = this.color;
		tf.size = this.size;
		tf.font = this.font;
		tf.align = this.align;
		tf.bold = this.bold;
		tf.italic = this.italic;
		tf.underline = this.underline;
		return tf;
	}
		
	public static function convertFromStyle(style:Style):TextStyle {
		var res:TextStyle = new TextStyle();
		if (style != null) {
			res.id = style.id;
			res.type = style.type;
			if (style.color != null)
				res.color = style.color;
			if (style.size != null)
				res.size = style.size;
			if (style.font != null)
				res.font = style.font;
			if (style.align != null)
				res.align = style.align;
			if (style.bold != null)
				res.bold = style.bold.toLowerCase() == 'yes' ? true : false;
			if (style.italic != null)
				res.italic = style.italic.toLowerCase() == 'yes' ? true : false;
			if (style.underline != null)
				res.underline = style.underline.toLowerCase() == 'yes' ? true : false;
		}
		return res;
	}
}