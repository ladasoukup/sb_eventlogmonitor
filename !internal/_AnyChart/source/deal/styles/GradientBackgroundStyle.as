﻿import styles.BackgroundStyle;
import styles.Style;
import utils.XMLUtils;
	
class styles.GradientBackgroundStyle extends BackgroundStyle {
	
	public var colors:Array;
	public var ratios:Array;
	public var alphas:Array;
		
	public var rotation:Number;
	
	public var gradientType:String;
	public var background_type:String;
			
	public static function convertFromStyle(style:Style):GradientBackgroundStyle {
		
		var res:GradientBackgroundStyle = new GradientBackgroundStyle();
		if ((style!=null)&&(style.id!=null)) {
			res.enabled = style.enabled;
			res.id = style.id;
			res.type = style.type;
			res.background_type = style.background_type;
			if (res.background_type == 'gradient') {
				res.gradientType = style.gradient_type;
				res.ratios = new Array();
				res.colors = new Array();
				res.alphas = new Array();
				for (var i:Number = 0;i<style.colors.childNodes.length;i++) {
					res.colors.push(Number(style.colors.childNodes[i].firstChild.nodeValue));
				}
				for (var i:Number = 0;i<style.ratios.childNodes.length;i++) {
					res.ratios.push(Number(style.ratios.childNodes[i].firstChild.nodeValue));
				}
				for (var i:Number = 0;i<style.alphas.childNodes.length;i++) {
					res.alphas.push(Number(style.alphas.childNodes[i].firstChild.nodeValue));
				}
				res.rotation = Number(style.matrix.attributes.rotation);
			}else {
				res.color = Number(style.color);
				res.alpha = Number(style.alpha);
			}
		}
		return res;
	}
}