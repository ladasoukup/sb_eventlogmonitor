﻿import errors.ErrorMessage;
import styles.Style;

class styles.StylesContainer {

	private var _styles:Object;
		
	public function registerStyle(id:String, style:Style):Void {
		if (!this._styles)
			this._styles = new Object();
		this._styles[id] = style;
	}
		
	public function getStyleById(id:String):Style {
		var required:Boolean = arguments[1] == undefined ? true : Boolean(arguments[1]);
		if (id != undefined) {
			if (this._styles[id] != undefined)
				return this._styles[id];
			else {
				if (required) {
					_root.showError(ErrorMessage.getUnknownStyleMessage(id));					
				}else {
					return null;
				}
			}
		}
		return null;
	}
}