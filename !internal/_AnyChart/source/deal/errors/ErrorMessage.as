﻿import utils.StringUtils;

class errors.ErrorMessage {
	
	public static var BAD_STYLE_TYPE:String = "Bad {element} style type \n" + 
			"The 'type' attribute value in style with id='{id}' must be '{needType}', not '{currentType}'";
				
	public static var ATTRIBUTE_REQUIRED:String = "Attribute {name} must be setted in node " + 
			"{node}";
				
	public static var UNKNOWN_STYLE:String = "Unknown style with id = '{id}'";
	public static var UNKNOWN_SHAPE:String = "Unknown shape with id = '{id}'";
								
	public static function getStyleBadTypeMessage(element:String,styleId:String,needType:String,currentType:String):String {
		var msg:String = ErrorMessage.BAD_STYLE_TYPE;
		var res:String = StringUtils.replace(msg,'{id}',styleId);
		res = StringUtils.replace(res,'{needType}',needType);
		res = StringUtils.replace(res,'{currentType}',currentType);
		res = StringUtils.replace(res,'{element}',element);
		return res;
	}
		
	public static function getAttributeRequiredMessage(name:String, node:String):String	{
		var msg:String = ErrorMessage.ATTRIBUTE_REQUIRED; 
		msg = StringUtils.replace(msg,'{name}',name);
		msg = StringUtils.replace(msg,'{node}',node);
		return msg;
	}

	public static function getUnknownStyleMessage(id:String):String
	{
		var msg:String = ErrorMessage.UNKNOWN_STYLE;
		return StringUtils.replace(msg,'{id}',id);
	}
		
	public static function getUnknownShapeMessage(id:String):String
	{
		var msg:String = ErrorMessage.UNKNOWN_SHAPE;
		return StringUtils.replace(msg,'{id}',id);
	}
}