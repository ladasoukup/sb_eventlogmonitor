﻿class utils.XMLUtils {
	
	public static function getChildByName(node:XMLNode,name:String):XMLNode {
		for (var i:Number = 0;i<node.childNodes.length;i++) {
			if (node.childNodes[i].nodeName == name)
				return node.childNodes[i];
		}
		return null;
	}
}