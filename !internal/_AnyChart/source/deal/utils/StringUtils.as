﻿class utils.StringUtils {
	
	public static function replace(from:String,what:String,to:String):String {
		return from.split(what).join(to);
	}
}