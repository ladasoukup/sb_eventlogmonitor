﻿import display.IGaugeChartElement;
import chartObjects.TextObject;
import chartObjects.CircleObject;
import chartObjects.ShapeObject;

class chartObjects.ObjectFactory {
	public static function getInstance(chart:DealChart,container:MovieClip,node:XMLNode):IGaugeChartElement {
		var res:IGaugeChartElement;
		var d:Number = container.getNextHighestDepth();
		switch (node.nodeName) {
			case 'text':
				res = IGaugeChartElement(container.attachMovie('TextObject','textObj_'+d,d));
				res['stylesContainer'] = chart.stylesContainer;
				res['indicator'] = chart.indicator;
				res['configFromXML'](node);
				return res;
			break;
			case 'circle':
				res = IGaugeChartElement(container.attachMovie('CircleObject','textObj_'+d,d));
				res['stylesContainer'] = chart.stylesContainer;
				res['configFromXML'](node);
				return res;
			break;
			case 'shape':
				res = IGaugeChartElement(container.attachMovie('ShapeObject','textObj_'+d,d));
				res['stylesContainer'] = chart.stylesContainer;
				res['shapesContainer'] = chart.shapesContainer;
				res['configFromXML'](node);
				return res;
			break;
		}
		return null;
	}
}