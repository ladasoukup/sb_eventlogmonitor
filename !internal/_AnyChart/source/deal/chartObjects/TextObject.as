﻿import display.DisplayContainer;
import display.IGaugeChartElement;
import styles.*;
import display.Indicator;
import errors.ErrorMessage;
import utils.StringUtils;

class chartObjects.TextObject extends DisplayContainer implements IGaugeChartElement {

	public static var BACKGROUND_STYLE_TYPE:String = 'text_object_background';
	public static var BORDER_STYLE_TYPE:String = 'text_object_border';
	public static var POSITION_STYLE_TYPE:String = 'text_object_position';
	public static var SIZE_STYLE_TYPE:String = 'text_object_size';
	public static var TEXT_STYLE_TYPE:String = 'text_object_text';
		
	public var stylesContainer:StylesContainer;
	public var textStyle:TextStyle;
	public var positionStyle:PositionStyle;
	public var sizeStyle:SizeStyle;
	public var text:String;
	public var indicator:Indicator;
		
	public function configFromXML(node:XMLNode):Void {
		var msg:String;
		this.textStyle = TextStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.textStyleId));
		if (this.textStyle.id != null) {
			msg = ErrorMessage.getStyleBadTypeMessage('text',this.textStyle.id,TextObject.TEXT_STYLE_TYPE,this.textStyle.type);
			this.textStyle.checkType(TextObject.TEXT_STYLE_TYPE,msg);
		}
		var positionStyle:Style = this.stylesContainer.getStyleById(node.attributes.positionStyleId);
		if (positionStyle == null)
			_root.showError(ErrorMessage.getAttributeRequiredMessage('positionStyleId','<text ... />'));
		this.positionStyle = PositionStyle.convertFromStyle(positionStyle);
		msg = ErrorMessage.getStyleBadTypeMessage('text',this.positionStyle.id,TextObject.POSITION_STYLE_TYPE,this.positionStyle.type);
		this.positionStyle.checkType(TextObject.POSITION_STYLE_TYPE,msg);
		this.positionStyle.checkExist('left');
		this.positionStyle.checkExist('top');
		if (node.attributes.sizeStyleId != undefined) {
			this.sizeStyle = SizeStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.sizeStyleId));
			msg = ErrorMessage.getStyleBadTypeMessage('text',this.sizeStyle.id,TextObject.SIZE_STYLE_TYPE,this.sizeStyle.type);
			this.sizeStyle.checkType(TextObject.SIZE_STYLE_TYPE,msg);
		}
		this.backgroundStyle = BackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.backgroundStyleId));
		if (this.backgroundStyle.id != null) {
			msg = ErrorMessage.getStyleBadTypeMessage('text',this.backgroundStyle.id,TextObject.BACKGROUND_STYLE_TYPE,this.backgroundStyle.type);
			this.backgroundStyle.checkType(TextObject.BACKGROUND_STYLE_TYPE,msg);
		}
		this.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.borderStyleId));
		if (this.borderStyle.id != null) {
			msg = ErrorMessage.getStyleBadTypeMessage('text',this.borderStyle.id,TextObject.BORDER_STYLE_TYPE,this.borderStyle.type);
			this.borderStyle.checkType(TextObject.BORDER_STYLE_TYPE,msg);
		}
		if (node.attributes.text == undefined)
				_root.showError(ErrorMessage.getAttributeRequiredMessage('text','<text ... />'));
		this.text = this.getText(node.attributes.text);
	}
		
	public function draw():Void {
		var depth:Number = this.getNextHighestDepth();
		var tf:TextField = this.createTextField('field_'+depth,depth,0,0,0,0);
		if (this.backgroundStyle!=null) {
			tf.background = this.backgroundStyle.enabled;
			tf.backgroundColor = this.backgroundStyle.color;
		}
		if (this.borderStyle!=null) {
			tf.border = this.borderStyle.enabled;
			tf.borderColor = this.borderStyle.color;
		}
		if (this.sizeStyle!=null) {
			tf._width = this.sizeStyle.width;
			tf._height = this.sizeStyle.height;
		}else {
			tf.autoSize = true;
		}
		if (this.positionStyle!=null) {
			tf._x = this.positionStyle.left;
			tf._y = this.positionStyle.top;
		}
		tf.text = this.text;
		if (this.textStyle!=null) {
			var format:TextFormat = this.textStyle.getTextFormat();
			tf.setTextFormat(format);
		}
	}
	
	private function getText(text:String):String {			
		if (text.indexOf('{value}')!=-1) {
			var r:String = this.indicator.value.toString();
			text = StringUtils.replace(text,'{value}',r);
		}
		return text;
	}
}