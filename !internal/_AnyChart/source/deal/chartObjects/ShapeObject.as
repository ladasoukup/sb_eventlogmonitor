﻿import display.DisplayContainer;
import display.IGaugeChartElement;
import styles.StylesContainer;
import shapes.ShapesContainer;
import styles.Style;
import styles.BackgroundStyle;
import styles.BorderStyle;
import errors.ErrorMessage;
import styles.PositionStyle;
import display.DisplayObject;
	
class chartObjects.ShapeObject extends DisplayContainer implements IGaugeChartElement {

	public var stylesContainer:StylesContainer;
	public var shapesContainer:ShapesContainer;
		
	public var shape:DisplayObject;
		
	public function configFromXML(node:XMLNode):Void {
		var positionStyleS:Style = this.stylesContainer.getStyleById(node.attributes.positionStyleId);
		if (node.attributes.backgroundStyleId != undefined)
			this.backgroundStyle = BackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.backgroundStyleId));
		else
			this.backgroundStyle = new BackgroundStyle();
		if (node.attributes.borderStyleId != null)
			this.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.borderStyleId));
		else
			this.borderStyle = new BorderStyle();
		if (positionStyleS == null)
			_root.showError(ErrorMessage.getAttributeRequiredMessage('positionStyleId','<shape ... />'));
		var positionStyle:PositionStyle = PositionStyle.convertFromStyle(positionStyleS);
		this._x = positionStyle.left;
		this._y = positionStyle.top;
		if (node.attributes.shapeId == null)
			_root.showError(ErrorMessage.getAttributeRequiredMessage('shapeId','<shape ... />'));
		this.shape = this.shapesContainer.getShapeById(node.attributes.shapeId);
	}
		
	public function draw():Void	{
			super.applyStyles();
			this.shape.backgroundStyle = this.backgroundStyle;
			this.shape.borderStyle = this.borderStyle;
			this.shape.draw(this);
	}
}