﻿import display.ChartGraphics;
import display.DisplayObject;
import display.IGaugeChartElement;
import styles.StylesContainer;
import styles.SizeStyle;
import styles.PositionStyle;
import styles.BackgroundStyle;
import styles.BorderStyle;
	
class chartObjects.CircleObject extends DisplayObject implements IGaugeChartElement {
	
	public var stylesContainer:StylesContainer;
	public var radius:Number;
		
	public function configFromXML(node:XMLNode):Void {
		var sizeStyle:SizeStyle = SizeStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.sizeStyleId));
		var positionStyle:PositionStyle = PositionStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.positionStyleId));
		this.backgroundStyle = BackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.backgroundStyleId));
		this.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.borderStyleId));
		this._x = positionStyle.left;
		this._y = positionStyle.top;
		this.radius = sizeStyle.radius;
	}
		
	public function draw():Void {
		super.applyStyles();
		ChartGraphics.drawCircle(this,0,0,this.radius);
	}
}