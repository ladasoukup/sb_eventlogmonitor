﻿import display.DisplayObject;

interface shapes.IShape	{
	function draw(target:DisplayObject):Void;
	function config(node:XMLNode):Void;
}