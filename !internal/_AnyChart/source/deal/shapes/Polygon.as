﻿import display.DisplayObject;
import shapes.IShape;
	
class shapes.Polygon implements IShape {
	
	public var target:DisplayObject;
	public var points:Array;
		
	public function Polygon() {
		this.points = new Array();
	}
	
	public function registerPoint(x:Number,y:Number):Void {
		this.points.push({x:x,y:y});
	}
		
	public function draw(target:DisplayObject):Void	{		
		target.moveTo(this.points[0].x,this.points[0].y);
		for (var i:Number=1;i<this.points.length;i++)
			target.lineTo(this.points[i].x,this.points[i].y);
		target.endFill();
	}		
		
	public function config(node:XMLNode):Void {
		for (var i:Number = 0;i<node.childNodes.length;i++)
			this.registerPoint(node.childNodes[i].attributes.x,node.childNodes[i].attributes.y);
	}
}