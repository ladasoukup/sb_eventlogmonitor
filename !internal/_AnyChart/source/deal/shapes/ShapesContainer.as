﻿import shapes.IShape;
import display.DisplayObject;
import errors.ErrorMessage;
	
class shapes.ShapesContainer {
	
	private var _shapes:Object;
		
	public function registerShape(id:String, shape:IShape):Void	{
		if (!this._shapes)
			this._shapes = new Object();

		this._shapes[id] = shape;
	}
		
	public function getShapeById(id:String):DisplayObject {
		if (this._shapes[id] == null)
			_root.showError(ErrorMessage.getUnknownShapeMessage(id));
		return this._shapes[id];
	}
}