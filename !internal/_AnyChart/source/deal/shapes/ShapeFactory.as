﻿import shapes.*;

class shapes.ShapeFactory {
	public static var POLYGON:String = 'polygon';
		
	public static function getShape(node:XMLNode):IShape {
		var type:String = node.attributes.type;
		switch (type) {
			case ShapeFactory.POLYGON:
			  var shape:Polygon = new Polygon();
			  shape.config(node);
			  return shape;
		}
		return null;
	}
}