﻿import display.*;
import utils.*;
import styles.*;
import errors.*;
import shapes.*;
import chartObjects.ObjectFactory;
import flash.geom.Matrix;
import flash.external.ExternalInterface;

class DealChart extends MovieClip {
	
	private var chartData:XML;
	
	public function DealChart() {
		super();
		
		Stage.scaleMode = "noscale";
		Stage.align = "LT";
			
		addContextMenu();
		
		showPreloader();
		initObjects();
		initExternalInterface();
	}
	
	public var hasExternalData:Boolean = false;
	
	private function initExternalInterface():Void {
		ExternalInterface.addCallback('SetXMLText',this,setXMLTextClosure);
	}
	
	private function setXMLTextClosure(data:String):Void {
		container.removeMovieClip();
		showPreloader();
		this.initObjects();
		this.chartData = new XML();
		this.chartData.ignoreWhite = true;
		this.chartData.parseXML(data);
		onXMLLoad(true);
		this.draw();
	}
	
	private var container:MovieClip;
	private var objects:Array;
	private var areas:Array;
	private var grids:Array;
	private var chart:Chart;
	public var indicator:Indicator;
	private var indicatorHint:Hint;
	
	private var areasContainer:MovieClip;
	
	private function initObjects():Void {		
		container = this.createEmptyMovieClip('container',1);
		objects = new Array();
		areas = new Array();
		grids = new Array();
		chart = Chart(container.attachMovie('Chart','chart',container.getNextHighestDepth()));		
		areasContainer = container.createEmptyMovieClip('AreasContainer',container.getNextHighestDepth());
		indicator = Indicator(container.attachMovie('Indicator','indicator',container.getNextHighestDepth()));
		indicatorHint = Hint(container.attachMovie('Hint','hint',container.getNextHighestDepth()));
	}
	
	private var preloader:MovieClip;
	
	public function showPreloader():Void {
		preloader = attachMovie('Preloader','preloader',this.getNextHighestDepth());
	}
	
	private function hidePreloader():Void {
		preloader.removeMovieClip();
	}
	
	private function addContextMenu():Void {
		var my_cm:ContextMenu = new ContextMenu();
		my_cm.hideBuiltInItems();
		var aboutMovie:MovieClip;
		function showAbout(obj, item):Void {
			aboutMovie = _root.attachMovie('About','about',_root.getNextHighestDepth());
			_root._alpha = 100;
			aboutMovie._alpha = 150;
			aboutMovie._x = Stage.width/2;
			aboutMovie._y = Stage.height/2;
			aboutMovie.unshowAbout = function():Void {
				_root._alpha = 100;
				aboutMovie.removeMovieClip();
			}
		}
		function printItemHandler(obj, item):Void {
			var printJob:PrintJob = new PrintJob();
			// display print dialog box
			if (printJob.start()) {
				// boolean to track whether addPage succeeded, change this to a counter
				// if more than one call to addPage is possible
				var pageAdded:Boolean = false;			
				pageAdded = printJob.addPage(_root,{xMin:0,xMax:Stage.width,yMin:0,yMax:Stage.height});
				// send pages from the spooler to the printer
				if (pageAdded) {
					printJob.send(); 
				}
			}
		}
		my_cm.customItems.push(new ContextMenuItem("About AnyChart",showAbout));
		my_cm.customItems.push(new ContextMenuItem("Print chart",printItemHandler));
		_root.menu = my_cm;
	}
	
	public function loadXMLDocument(path:String):Void {
		var ths:DealChart = this;
		chartData = new XML();
		chartData.ignoreWhite = true;
		chartData.onLoad = function(success:Boolean):Void {
			ths.onXMLLoad(success);
		}
		chartData.load(path);
		_root.STATUS = (_root.loading == undefined) ? 'Loading data...' : _root.loading;	
	}
	
	private function onXMLLoad(success:Boolean):Void {		
		hidePreloader();
		if (success) {
			switch (chartData.status) {
				case 0 :
					if (chartData.firstChild.nodeName != 'root' || !chartData || !chartData.firstChild) {
						_root.showError("No chart data available");
					}else {
						parseXML();
					}						
					break;
				case -2 :
					_root.showError("A CDATA section was not properly terminated.");
					break;
				case -3 :
					_root.showError("The XML declaration was not properly terminated.");
					break;
				case -4 :
					_root.showError("The DOCTYPE declaration was not properly terminated.");
					break;
				case -5 :
					_root.showError("A comment was not properly terminated.");
					break;
				case -6 :
					_root.showError("An XML element was malformed.");
					break;
				case -7 :
					_root.showError("Out of memory.");
					break;
				case -8 :
					_root.showError("An attribute value was not properly terminated.");
					break;
				case -9 :
					_root.showError("A start-tag was not matched with an end-tag.");
					break;
				case -10 :
					_root.showError("An end-tag was encountered without a matching start-tag.");
					break;
				default :
					_root.showError("An unknown error has occurred.");
					break;
			}
		}else {
			_root.showError("XML loading failed");
		}
	}
	
	private function parseXML():Void {
		configStyles();
		configShapes();
		configChartObjects();
		configObjects();
		draw();
	}
	
	private function configObjects():Void {
		var objectsNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'objects');
		this.objects = new Array();
		for (var i:Number = 0;i<objectsNode.childNodes.length;i++) {		
			this.objects.push(ObjectFactory.getInstance(this,container,objectsNode.childNodes[i]));
		}
	}
		
	public var shapesContainer:ShapesContainer;
		
	private function configShapes():Void {
		this.shapesContainer = new ShapesContainer();
		var shapesNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'shapes');
		for (var i:Number = 0;i<shapesNode.childNodes.length;i++)
		{
			var shape:IShape = ShapeFactory.getShape(shapesNode.childNodes[i]);
			this.shapesContainer.registerShape(shapesNode.childNodes[i].attributes.id,shape);
		}
	}
	
	public var stylesContainer:StylesContainer;
		
	private function configStyles():Void {
		this.stylesContainer = new StylesContainer();
		var stylesNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'styles');
		for (var i:Number = 0;i<stylesNode.childNodes.length;i++) {
			stylesContainer.registerStyle(stylesNode.childNodes[i].attributes.id, Style.getFromXMLAttributes(stylesNode.childNodes[i]))
		}
	}
		
	private function configChartObjects():Void {
		this.configChart();
		this.configIndicator();
		this.configAreas();
		this.configGrids();
	}

	private function configChart():Void {
		var dataNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'data');
		var dealNode:XMLNode = XMLUtils.getChildByName(dataNode,'deal');
		var sizeStyleS:Style = stylesContainer.getStyleById(dealNode.attributes.sizeStyleId);
		if (sizeStyleS == null)	{
			_root.showError(ErrorMessage.getAttributeRequiredMessage('sizeStyleId','<deal />'));
		}
		
		var positionStyleS:Style = this.stylesContainer.getStyleById(dealNode.attributes.positionStyleId);
		if (positionStyleS == null)	{
			_root.showError(ErrorMessage.getAttributeRequiredMessage('positionStyleId','<deal />'));
		}
		this.chart.backgroundStyle = GradientBackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(dealNode.attributes.backgroundStyleId));
		this.chart.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(dealNode.attributes.borderStyleId));
		this.chart.sizeStyle = SizeStyle.convertFromStyle(sizeStyleS);
		this.chart.positionStyle = PositionStyle.convertFromStyle(positionStyleS);
		if (dealNode.attributes.maximum == null) {
			_root.showError(ErrorMessage.getAttributeRequiredMessage('maximum','<deal ... />'));
		}
		this.chart.maximumValue = dealNode.attributes.maximum;
		if (dealNode.attributes.minimum == null) {
			_root.showError(ErrorMessage.getAttributeRequiredMessage('minimum','<deal ... />'));
		}
		this.chart.minimumValue = dealNode.attributes.minimum;
		this.chart.init();
	}
				
	private function configAreas():Void {
		var dataNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'data');
		var areasNode:XMLNode = XMLUtils.getChildByName(dataNode,'areas');		
		for (var i:Number = 0;i<areasNode.childNodes.length;i++) {
			var node:XMLNode = areasNode.childNodes[i];
			var area:Area = Area(areasContainer.attachMovie('Area','area_'+i,areasContainer.getNextHighestDepth()));
			var sizeStyleS:Style = this.stylesContainer.getStyleById(node.attributes.sizeStyleId);
			if (sizeStyleS == null)
				_root.showError(ErrorMessage.getAttributeRequiredMessage('sizeStyleId','<area />'));
			area.sizeStyle = SizeStyle.convertFromStyle(sizeStyleS);
			area.startAngle = node.attributes.startAngle;
			area.endAngle   = node.attributes.endAngle;
			area.startValue = node.attributes.startValue;
			area.endValue = node.attributes.endValue;
			area.chart = this.chart;
			area.backgroundStyle = GradientBackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.backgroundStyleId));
			area.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.borderStyleId));
			area._x = chart._x;
			area._y = chart._y;
			area.init();
			this.areas.push(area);
		}		
	}
		
	private function configIndicator():Void	{
		var dataNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'data');
		var indicatorNode:XMLNode = XMLUtils.getChildByName(dataNode,'indicator');
		
		if (indicatorNode.attributes.value == null) {
			_root.showError(ErrorMessage.getAttributeRequiredMessage('value','<indicator ... />'));
		}
		this.indicator.value = indicatorNode.attributes.value;
		this.indicator.chart = this.chart;
		this.indicator.backgroundStyle = BackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(indicatorNode.attributes.backgroundStyleId));
		this.indicator.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(indicatorNode.attributes.borderStyleId));
		if (indicatorNode.attributes.shapeId == null)	{
			_root.showError(ErrorMessage.getAttributeRequiredMessage('shapeId','<indicator ... />'));
		}
		this.indicator.shape = this.shapesContainer.getShapeById(indicatorNode.attributes.shapeId);
		this.indicator._x = this.chart._x;
		this.indicator._y = this.chart._y;
		this.indicator.init();
		
		var indicatorHintNode:XMLNode = XMLUtils.getChildByName(indicatorNode,'hint');
		if (indicatorHintNode.attributes.enabled == 'yes') {
			this.indicatorHint.enabled = true;
			this.indicatorHint.backgroundStyle = BackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(indicatorHintNode.attributes.backgroundStyleId));
			this.indicatorHint.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(indicatorHintNode.attributes.borderStyleId));
			this.indicatorHint.sizeStyle = SizeStyle.convertFromStyle(this.stylesContainer.getStyleById(indicatorHintNode.attributes.sizeStyleId));
			this.indicatorHint.textStyle = TextStyle.convertFromStyle(this.stylesContainer.getStyleById(indicatorHintNode.attributes.textStyleId));
			this.indicatorHint.text = indicatorHintNode.attributes.text;
			this.indicatorHint.draw();
		}
	}
	
	private function configGrids():Void {
		var dataNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'data');
		var gridsNode:XMLNode = XMLUtils.getChildByName(dataNode,'scales');
		for (var i:Number = 0;i<gridsNode.childNodes.length;i++) {
			var node:XMLNode = gridsNode.childNodes[i];
			var grid:Grid = Grid(this.container.attachMovie('Grid','grid_'+i,container.getNextHighestDepth()));
			var sizeStyle:Style = this.stylesContainer.getStyleById(node.attributes.sizeStyleId);
			if (sizeStyle == null) {
				_root.showError(ErrorMessage.getAttributeRequiredMessage('sizeStyleId','<scale ... />'));
			}
			var positionStyle:Style = this.stylesContainer.getStyleById(node.attributes.positionStyleId);
			if (positionStyle == null) {
				_root.showError(ErrorMessage.getAttributeRequiredMessage('positionStyleId','<scale ... />'));
			}
			grid.sizeStyle = SizeStyle.convertFromStyle(sizeStyle);
			grid.positionStyle = PositionStyle.convertFromStyle(positionStyle);
			if (node.attributes.angleStep == null) {
				_root.showError(ErrorMessage.getAttributeRequiredMessage('angleStep','<scale .../>'));
			}
			grid.angleStep = Number(node.attributes.angleStep);
			if (node.attributes.startAngle == null) {
				_root.showError(ErrorMessage.getAttributeRequiredMessage('startAngle','<scale .../>'));
			}
			grid.startAngle = Number(node.attributes.startAngle);
			if (node.attributes.endAngle == null) {
				_root.showError(ErrorMessage.getAttributeRequiredMessage('endAngle','<scale .../>'));
			}				
			grid.endAngle = Number(node.attributes.endAngle);
			grid._x = this.chart._x;
			grid._y = this.chart._y;
			grid.chart = this.chart;
			grid.borderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.linesStyleId));
			grid.labelBackgroundStyle = BackgroundStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.labelBackgroundStyleId));
			grid.labelBorderStyle = BorderStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.labelBorderStyleId));
			var labelPositionStyle:Style = this.stylesContainer.getStyleById(node.attributes.labelPositionStyleId);
			if (labelPositionStyle == null)	{
				_root.showError(ErrorMessage.getAttributeRequiredMessage('labelPositionStyleId','<scale ... />'));
			}
			var labelSizeStyle:Style = this.stylesContainer.getStyleById(node.attributes.labelSizeStyleId);
			if (labelSizeStyle == null)	{
				_root.showError(ErrorMessage.getAttributeRequiredMessage('labelSizeStyleId','<scale ... />'));
			}
			grid.labelPositionStyle = PositionStyle.convertFromStyle(labelPositionStyle);
			grid.labelSizeStyle = SizeStyle.convertFromStyle(labelSizeStyle);
			grid.labelTextStyle = TextStyle.convertFromStyle(this.stylesContainer.getStyleById(node.attributes.labelTextStyleId));
			if (node.attributes.decimalPlaces != undefined) {
				grid.decimalPlaces = node.attributes.decimalPlaces;
			}
			if (node.attributes.labelsPostfix != undefined)
				grid.labelsPostfix = node.attributes.labelsPostfix;
			if (node.attributes.labelsPrefix != undefined)
				grid.labelsPrefix = node.attributes.labelsPrefix;
			if (node.attributes.labelsDecimalSeparator != undefined) {
				grid.labelsDecimalSeparator = node.attributes.labelsDecimalSeparator;
			}if (node.attributes.labelsThousandSeparator != undefined)
				grid.labelsThousandSeparator = node.attributes.labelsThousandSeparator;
				
			grid.init();
			this.grids.push(grid);
		}
	}
	
	private function draw():Void {
		this.clear();
		chart.draw();
		var i:Number = 0;
		//drawBackground();
		for (i=0;i<this.areas.length;i++) {
			this.areas[i].draw();
		}
		for (i=0;i<this.grids.length;i++) {
			this.grids[i].draw();
			this.grids[i]._x = this.chart._x;
			this.grids[i]._y = this.chart._y;			
		}
		this.indicator.draw();
		for (i=0;i<this.objects.length;i++)	{
			this.objects[i].draw();
		}
		if (this.indicatorHint.enabled)	{
			this.indicatorHint.init(this.indicator);
		}
		drawTitle();
		drawBackground();
	}
	
	private function drawTitle():Void {
		var titleNode:XMLNode = XMLUtils.getChildByName(chartData.firstChild,'title');		
		if (!titleNode || titleNode.attributes.text == undefined)
			return;
		var style:Style = stylesContainer.getStyleById(titleNode.attributes.positionStyleId);			
		var positionStyle:PositionStyle = PositionStyle.convertFromStyle(style);
		style = stylesContainer.getStyleById(titleNode.attributes.textStyleId);	
		var textStyle:TextStyle = TextStyle.convertFromStyle(style);			
		var field:TextField = container.createTextField('chartTitle',container.getNextHighestDepth(),0,0,0,0);
		field.selectable = false;
		field.text = titleNode.attributes.text;
		field._width = Stage.width;
		field.setTextFormat(textStyle.getTextFormat());
		field._x = positionStyle.left;
		field._y = positionStyle.top;
		field._height = field.textHeight + 5;
	}
	
	private function drawBackground():Void {
		var sourceStyle:Style = stylesContainer.getStyleById('background',false);			
		if (sourceStyle == null)
			return;
			
		var style:GradientBackgroundStyle = GradientBackgroundStyle.convertFromStyle(sourceStyle);
			
		if (style == null) 
			return;

		if (style.background_type == 'gradient') {
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(Stage.width,Stage.height,style.rotation);
			beginGradientFill(style.gradientType,style.colors,style.alphas,style.ratios,matrix);
		}else {
			beginFill(style.color,style.alpha);
		}
		ChartGraphics.drawRect(this,0,0,Stage.width,Stage.height);
	}
}