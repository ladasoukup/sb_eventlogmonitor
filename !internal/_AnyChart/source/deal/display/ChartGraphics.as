﻿import flash.geom.Rectangle;

class display.ChartGraphics {		
		
	public static function getArcX(angle:Number,radius:Number):Number {
		return (radius * Math.cos(angle * Math.PI/180));
	}
		
	public static function getArcY(angle:Number,radius:Number):Number {
		return -(radius * Math.sin(angle * Math.PI/180));
	}
	
	public static function drawRect(g:MovieClip,x:Number,y:Number,w:Number,h:Number):Void {
		g.moveTo(x,y);
		g.lineTo(x+w,y);
		g.lineTo(x+w,y+h);
		g.lineTo(x,y+h);
		g.lineTo(x,y);
	}
	
	public static function drawCircle(graphics:MovieClip, x:Number, y:Number, radius:Number):Void {
		var xp:Number = x - radius*Math.cos(0);
		var yp:Number = y - radius*Math.sin(0);
		graphics.moveTo(xp,yp);
		ChartGraphics.drawArc(graphics,x,y,radius,0,360);
		graphics.lineTo(xp,yp);
	}
	
	public static function drawCustomCircle(graphics:MovieClip,x:Number,y:Number,startAngle:Number,endAngle:Number,innerRadius:Number,outerRadius:Number):Void {
		var xp0:Number = x - outerRadius*Math.cos(Math.PI*endAngle/180);
		var yp0:Number = y - outerRadius*Math.sin(Math.PI*endAngle/180);
		var xp1:Number = x - innerRadius*Math.cos(Math.PI*startAngle/180);
		var yp1:Number = y - innerRadius*Math.sin(Math.PI*startAngle/180);
		graphics.moveTo(xp1,yp1);
		ChartGraphics.drawArc(graphics,x,y,innerRadius,startAngle,endAngle);
		graphics.lineTo(xp0,yp0);
		ChartGraphics.drawArc(graphics,x,y,outerRadius,endAngle,startAngle);
		graphics.lineTo(xp1,yp1);
		graphics.endFill();
	}
		
	public static function drawArc(graphics:MovieClip,x:Number,y:Number,radius:Number,startAngle:Number,endAngle:Number):Void {
		var xp:Number = x - radius*Math.cos(Math.PI*startAngle/180);
		var yp:Number = y - radius*Math.sin(Math.PI*startAngle/180);;
		var i:Number;
		if (endAngle>startAngle) {
			for (i = startAngle;i<=endAngle;i++) {
				xp = x - radius*Math.cos(Math.PI*i/180);
				yp = y - radius*Math.sin(Math.PI*i/180);
				graphics.lineTo(xp,yp);
			}
		}else {
			for (i = startAngle;i>=endAngle;i--) {
				xp = x - radius*Math.cos(Math.PI*i/180);
				yp = y - radius*Math.sin(Math.PI*i/180);
				graphics.lineTo(xp,yp);
			}
		}
		xp = x - radius*Math.cos(Math.PI*endAngle/180);
		yp = y - radius*Math.sin(Math.PI*endAngle/180);
		graphics.lineTo(xp,yp);
	}
		
	public static function getArcBounds(startAngle:Number,endAngle:Number,radius:Number):Rectangle {
		var rect:Rectangle = new Rectangle();
		var minHorizontalAngle:Number = startAngle;
		var maxHorizontalAngle:Number = startAngle;
		var minVerticalAngle:Number = startAngle;
		var maxVerticalAngle:Number = startAngle;
			
		var minH:Number;
		var maxH:Number;
		var minV:Number;
		var maxV:Number;
		
		for (var i:Number = startAngle;i<=endAngle;i++) {
			var c:Number = Math.cos(i * Math.PI/180);
			var s:Number = -Math.sin(i * Math.PI/180);				
			if (isNaN(minV) || minV > s) {
				minV = s;
				minVerticalAngle = i;
			}
			if (isNaN(maxV) || maxV < s) {
				maxV = s;
				maxVerticalAngle = i;
			}
			if (isNaN(minH) || minH > c) {
				minH = c;
				minHorizontalAngle = i;
			}
			if (isNaN(maxH) || maxH < c) {
				maxH = c;
				maxHorizontalAngle = i;
			}
		}			
		
		rect.x = getArcX(minHorizontalAngle,radius);
		rect.width = getArcX(maxHorizontalAngle,radius) - rect.x;
		rect.y = getArcY(minVerticalAngle,radius);
		rect.height = getArcY(maxVerticalAngle,radius) - rect.y;
		return rect;
	}
	
	public static function getCircleBounds(radius:Number):Rectangle {
		return new Rectangle(0,0,2*radius,2*radius);
	}
		
	public static function getAreaBounds(startAngle:Number,endAngle:Number,innerRadius:Number,outerRadius:Number):Rectangle {
		var rect:Rectangle = new Rectangle();
			
		var minHorizontalAngle:Number = startAngle;
		var maxHorizontalAngle:Number = startAngle;
		var minVerticalAngle:Number = startAngle;
		var maxVerticalAngle:Number = startAngle;
	
		var minH:Number;
		var maxH:Number;
		var minV:Number;
		var maxV:Number;
			
		for (var i:Number = startAngle;i<=endAngle;i++) {
			var c:Number = Math.cos(i * Math.PI/180);
			var s:Number = -Math.sin(i * Math.PI/180);				
			if (isNaN(minV) || minV > s) {
				minV = s;
				minVerticalAngle = i;
			}
			if (isNaN(maxV) || maxV < s) {
				maxV = s;
				maxVerticalAngle = i;
			}
			if (isNaN(minH) || minH > c) {
				minH = c;
				minHorizontalAngle = i;
			}
			if (isNaN(maxH) || maxH < c) {
				maxH = c;
				maxHorizontalAngle = i;
			}
		}
		
		var outerLeftX:Number = getArcX(minHorizontalAngle,outerRadius);
		var outerRightX:Number = getArcX(maxHorizontalAngle,outerRadius);
		var outerTopY:Number = getArcY(minVerticalAngle,outerRadius);
		var outerBottomY:Number = getArcY(maxVerticalAngle,outerRadius);
		var innerLeftX:Number = getArcX(minHorizontalAngle,innerRadius);
		var innerRightX:Number = getArcX(maxHorizontalAngle,innerRadius);
		var innerTopY:Number = getArcY(minVerticalAngle,innerRadius);
		var innerBottomY:Number = getArcY(maxVerticalAngle,innerRadius);
		
		var leftX:Number = Math.min(outerLeftX,innerLeftX);
		var rightX:Number = Math.min(outerRightX,innerRightX);
		var topY:Number = Math.min(outerTopY,innerTopY);
		var bottomY:Number = Math.min(outerBottomY,innerBottomY);
		
		rect.x = Math.min(leftX,rightX);
		rect.width = rightX - leftX;
		rect.y = Math.min(topY,bottomY);
		rect.height = bottomY - topY;
			
		return rect;
	}
}