﻿import styles.SizeStyle;
import styles.TextStyle;
import display.*;

class display.Hint extends DisplayContainer implements IGaugeChartElement {
	
	public var enabled:Boolean = false;
	public var sizeStyle:SizeStyle;
	public var text:String;
	public var textStyle:TextStyle;
		
	private var tf:TextField;
	
	public function Hint() {
		super();
		enabled = false;
	}
			
	public function draw():Void	{
		this.tf = this.createTextField('tf',this.getNextHighestDepth(),0,0,0,0);
		if (this.backgroundStyle!=null)	{
			this.tf.background = this.backgroundStyle.enabled;
			this.tf.backgroundColor = this.backgroundStyle.color;
		}
		if (this.borderStyle!=null)	{
			this.tf.border = this.borderStyle.enabled;
			this.tf.borderColor = this.borderStyle.color;
		}
		if (this.sizeStyle!=null) {
			this.tf._width = this.sizeStyle.width;
			this.tf._height = this.sizeStyle.height;
		}else
			this.tf.autoSize = true;

		this.tf.text = this.text;
		this.tf.selectable = false;
			
		if (this.textStyle!=null) {
			var format:TextFormat = this.textStyle.getTextFormat();
			tf.setTextFormat(format);
		}
		
		this._visible = false;
	}
		
	private var targetObject:MovieClip;
		
	public function init(targetObject:MovieClip):Void {
		this.targetObject = targetObject;
		var ths:Hint = this;
		targetObject.onRollOver = targetObject.onDragOver = function():Void {
			ths.show();
		}
		targetObject.onRollOut = targetObject.onDragOut = function():Void {
			ths.hide();
		}
	}
	
	private function show():Void {
		var ths:Hint = this;
		targetObject.onMouseMove = function():Void {
			ths.move();
		}			
		ths._visible = true;
	}
	
	private function move():Void {
		this._x = _root._xmouse;
		this._y = _root._ymouse - this.tf.height;
	}
	
	private function hide():Void {
		targetObject.onMouseMove = null;
		this._visible = false;
	}
}