﻿import styles.Style;
import styles.BackgroundStyle;
import styles.BorderStyle;
import styles.SizeStyle;
import errors.ErrorMessage;
import styles.PositionStyle;
import flash.geom.Rectangle;
import display.DisplayObject;
import display.IGaugeChartElement;
import display.ChartGraphics;

class display.Chart extends DisplayObject implements IGaugeChartElement	{
	
	public static var BACKGROUND_STYLE_TYPE:String = 'deal_background';
	public static var BORDER_STYLE_TYPE:String = 'deal_border';
	public static var SIZE_STYLE_TYPE:String = 'deal_size';
	public static var POSITION_STYLE_TYPE:String = 'deal_position';
		
	private var _minimumValue:Number;
	private var _maximumValue:Number;
	private var _valueScale:Number;
	
	public var outerRadius:Number;
	public var innerRadius:Number;
	
	private var _startAngle:Number = Number.NaN;
	private var _endAngle:Number = Number.NaN;
	
	public function set sizeStyle(sizeStyle:SizeStyle):Void {			
		var errorMessage:String = ErrorMessage.getStyleBadTypeMessage('deal',sizeStyle.id,Chart.SIZE_STYLE_TYPE,sizeStyle.type);
		sizeStyle.checkType(Chart.SIZE_STYLE_TYPE,errorMessage);
		sizeStyle.checkExist('innerRadius');
		sizeStyle.checkExist('outerRadius');
		sizeStyle.checkExist('startAngle');
		sizeStyle.checkExist('endAngle');
		this.innerRadius = sizeStyle.innerRadius;
		this.outerRadius = sizeStyle.outerRadius;
		this.startAngle  = sizeStyle.startAngle;
		this.endAngle = sizeStyle.endAngle;
	}
	
	public function set positionStyle(positionStyle:PositionStyle):Void	{
		if (positionStyle == null)
			_root.showError(ErrorMessage.getAttributeRequiredMessage('positionStyleId','<deal ... />'));
		var errorMessage:String = ErrorMessage.getStyleBadTypeMessage('deal',positionStyle.id,Chart.POSITION_STYLE_TYPE,positionStyle.type);
		positionStyle.checkType(Chart.POSITION_STYLE_TYPE,errorMessage);
		positionStyle.checkExist('left');
		positionStyle.checkExist('top');
		this._x = positionStyle.left;
		this._y = positionStyle.top;
	}
		
	public function get valueScale():Number	{
		return this._valueScale;
	}
	
	public function set startAngle(value:Number):Void {
		this._startAngle = value;
		this.calculateScale();
	}
	
	public function get startAngle():Number	{
		return this._startAngle;
	}
	
	public function set endAngle(value:Number):Void	{
		this._endAngle = value;
		this.calculateScale();
	}
	
	public function get endAngle():Number {
		return this._endAngle;
	}
	
	public function set maximumValue(value:Number):Void {
		this._maximumValue = value;
		this.calculateScale();
	}
	
	public function get maximumValue():Number {
		return this._maximumValue;
	}
	
	public function set minimumValue(value:Number):Void {
		this._minimumValue = value;
		this.calculateScale();
	}
	
	public function get minimumValue():Number {
		return this._minimumValue;
	}
	
	public function init():Void	{
		var errorMessage:String;
		if (this.backgroundStyle.id != null) {
			errorMessage = ErrorMessage.getStyleBadTypeMessage('deal',this.backgroundStyle.id,Chart.BACKGROUND_STYLE_TYPE,this.backgroundStyle.type);
			this.backgroundStyle.checkType(Chart.BACKGROUND_STYLE_TYPE,errorMessage);
		}
		if (this.borderStyle.id != null) {
			errorMessage = ErrorMessage.getStyleBadTypeMessage('deal',this.borderStyle.id,Chart.BORDER_STYLE_TYPE,this.borderStyle.type);
			this.borderStyle.checkType(Chart.BORDER_STYLE_TYPE,errorMessage);
		}
	}
	
	public function draw():Void {
		if (isNaN(startAngle) || isNaN(endAngle))
			return;
		var bounds:Rectangle = ChartGraphics.getCircleBounds(outerRadius);
		super.applyStyles(true,2*outerRadius,2*outerRadius,-outerRadius,-outerRadius);
		ChartGraphics.drawCustomCircle(this,0,0,this.startAngle,this.endAngle,this.innerRadius,this.outerRadius);
	}
	
	private function calculateScale():Void {
		this._valueScale = (this._maximumValue - this._minimumValue)/(this._endAngle - this._startAngle);
	}
}