﻿import styles.BorderStyle;
import styles.BackgroundStyle;
import styles.SizeStyle;
import errors.ErrorMessage;
import flash.geom.Rectangle;
import display.*;

class display.Area extends DisplayObject implements IGaugeChartElement {
	
	public static var BACKGROUND_STYLE_TYPE:String = 'area_background';
	public static var BORDER_STYLE_TYPE:String = 'area_border';
	public static var SIZE_STYLE_TYPE:String = 'area_size';
		
	public var startAngle:Number;
	public var endAngle:Number;
	public var startValue:Number;
	public var endValue:Number;
	public var innerRadius:Number;
	public var outerRadius:Number;
	public var chart:Chart;
		
	public function set sizeStyle(style:SizeStyle):Void	{
		var msg:String = ErrorMessage.getStyleBadTypeMessage('area',style.id,Area.SIZE_STYLE_TYPE,style.type);
		style.checkType(Area.SIZE_STYLE_TYPE,msg);
		style.checkExist('innerRadius');
		style.checkExist('outerRadius');
		this.innerRadius = style.innerRadius;
		this.outerRadius = style.outerRadius;
	}
		
	public function init():Void	{
		var msg:String;
		if (this.backgroundStyle.id != null) {
			msg = ErrorMessage.getStyleBadTypeMessage('area',this.backgroundStyle.id,Area.BACKGROUND_STYLE_TYPE,this.backgroundStyle.type);
			this.backgroundStyle.checkType(Area.BACKGROUND_STYLE_TYPE,msg);
		}
		if (this.borderStyle.id != null) {
			msg = ErrorMessage.getStyleBadTypeMessage('area',this.borderStyle.id,Area.BORDER_STYLE_TYPE,this.borderStyle.type);
			this.borderStyle.checkType(Area.BORDER_STYLE_TYPE,msg);				
		}
	}
		
	public function draw():Void {
		if ((this.startValue.toString() != 'NaN')&&(this.endValue.toString() != 'NaN'))	{
			this.startAngle = (this.startValue - this.chart.minimumValue)/this.chart.valueScale;
			this.endAngle = (this.endValue - this.chart.minimumValue)/this.chart.valueScale;
		}

		trace (outerRadius);
		super.applyStyles(true,outerRadius*2,outerRadius*2,-outerRadius,-outerRadius);
		ChartGraphics.drawCustomCircle(this,0,0,this.startAngle,this.endAngle,this.innerRadius,this.outerRadius);
	}
}