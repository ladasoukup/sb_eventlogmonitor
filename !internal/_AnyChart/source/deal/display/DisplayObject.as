﻿import styles.BackgroundStyle;
import styles.BorderStyle;
import styles.GradientBackgroundStyle;
import flash.geom.Matrix;
import display.ChartGraphics;

class display.DisplayObject extends MovieClip {

	public var backgroundStyle:BackgroundStyle;
	public var borderStyle:BorderStyle;
	
	public function applyStyles():Void {
		var isGradient:Boolean = arguments[0] == undefined ? false : Boolean(arguments[0]);
		var w:Number = arguments[1] == undefined ? 0 : Number(arguments[1]);		
		var h:Number = arguments[2] == undefined ? 0 : Number(arguments[2]);
		var dx:Number = arguments[3] == undefined ? 0 : Number(arguments[3]);
		var dy:Number = arguments[4] == undefined ? 0 : Number(arguments[4]);

		applyBackground(isGradient,w,h,dx,dy);
		
		if (borderStyle.enabled)
			lineStyle(borderStyle.thickness,borderStyle.color,borderStyle.alpha);
	}
	
	private function applyBackground(isGradient:Boolean,w:Number,h:Number,dx:Number,dy:Number):Void {
		if (!backgroundStyle.enabled)
			return;

		if (!isGradient) {
			beginFill(backgroundStyle.color,backgroundStyle.alpha);
			return;
		}
		
		var style:GradientBackgroundStyle = GradientBackgroundStyle(backgroundStyle);					
		if (style.background_type == 'gradient') {
			var m:Matrix = new Matrix();
			m.createGradientBox(w,h,style.rotation,dx,dy);
			trace (this+','+w+','+h+','+style.rotation+','+style.gradientType+','+dx+','+dy);
			beginGradientFill(style.gradientType,style.colors,style.alphas,style.ratios,m);
		}else {
			beginFill(backgroundStyle.color, backgroundStyle.alpha);
		}
	}
	
	public function draw():Void {};
}