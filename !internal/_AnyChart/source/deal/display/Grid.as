﻿import styles.TextStyle;
import styles.BackgroundStyle;
import styles.BorderStyle;
import styles.SizeStyle;
import styles.PositionStyle;
import errors.ErrorMessage;
import display.*;
	
class display.Grid extends DisplayContainer implements IGaugeChartElement {
	
	public static var LINES_STYLE_TYPE:String = 'scale_lines';
	public static var SIZE_STYLE_TYPE:String = 'scale_size';
	public static var POSITION_STYLE_TYPE:String = 'scale_position';
	public static var TEXT_STYLE_TYPE:String = 'scale_labels_text';
	public static var LABEL_POSITION_STYLE_TYPE:String = 'scale_labels_position';
	public static var LABEL_BACKGROUND_STYLE_TYPE:String = 'scale_labels_background';
	public static var LABEL_BORDER_STYLE_TYPE:String = 'scale_labels_border';
	public static var LABEL_SIZE_STYLE_TYPE:String = 'scale_labels_size';
	public var labelTextStyle:TextStyle;
	public var labelBackgroundStyle:BackgroundStyle;
	public var labelBorderStyle:BorderStyle;
	public var labelSizeStyle:SizeStyle;
	public var labelPositionStyle:PositionStyle;
	public var startAngle:Number;
	public var endAngle:Number;
	public var angleStep:Number;
	public var length:Number;
	public var radius:Number;
	public var chart:Chart;
	public var decimalPlaces:Number = 2;
	public var labelsPrefix:String = '';
	public var labelsPostfix:String = '';
	public var labelsThousandSeparator:String = '';
	public var labelsDecimalSeparator:String = '.';
	
	public function Grid() {
		labelsPrefix = '';
		labelsPostfix = '';
		decimalPlaces = 2;
		labelsThousandSeparator = '';
		labelsDecimalSeparator = '.';
	}
	
	public function set sizeStyle(style:SizeStyle):Void {
		var msg:String = ErrorMessage.getStyleBadTypeMessage('scale',style.id,Grid.SIZE_STYLE_TYPE,style.type);
		style.checkType(Grid.SIZE_STYLE_TYPE,msg);
		style.checkExist('length');
		this.length = style.length;
	}
		
	public function set positionStyle(style:PositionStyle):Void	{
		var msg:String = ErrorMessage.getStyleBadTypeMessage('scale',style.id,Grid.POSITION_STYLE_TYPE,style.type);
		style.checkType(Grid.POSITION_STYLE_TYPE,msg);
		style.checkExist('radius');
		this.radius = style.radius;
	}
		
	public function draw():Void	{
		super.applyStyles();
		for (var i:Number = this.startAngle;i<=this.endAngle;i+=this.angleStep)	{
			this.drawPoint(i);
			this.drawLabel(this.getLabel(i),i,i);
		}
	}
		
	public function init():Void	{
		var errorMessage:String;
		if (this.borderStyle.id != null) {
			errorMessage = ErrorMessage.getStyleBadTypeMessage('scale',this.borderStyle.id,Grid.LINES_STYLE_TYPE,this.borderStyle.type);
			this.borderStyle.checkType(Grid.LINES_STYLE_TYPE,errorMessage);
		}		
		if (this.labelBackgroundStyle.id != null) {
			errorMessage = ErrorMessage.getStyleBadTypeMessage('scale',this.labelBackgroundStyle.id,Grid.LABEL_BACKGROUND_STYLE_TYPE,this.labelBackgroundStyle.type);
			this.labelBackgroundStyle.checkType(Grid.LABEL_BACKGROUND_STYLE_TYPE,errorMessage);
		}
		if (this.labelBorderStyle.id != null) {
			errorMessage = ErrorMessage.getStyleBadTypeMessage('scale',this.labelBorderStyle.id,Grid.LABEL_BORDER_STYLE_TYPE,this.labelBorderStyle.type);
			this.labelBorderStyle.checkType(Grid.LABEL_BORDER_STYLE_TYPE,errorMessage);				
		}
		errorMessage = ErrorMessage.getStyleBadTypeMessage('scale',this.labelPositionStyle.id,Grid.LABEL_POSITION_STYLE_TYPE,this.labelPositionStyle.type);
		this.labelPositionStyle.checkType(Grid.LABEL_POSITION_STYLE_TYPE,errorMessage);				
		if (this.labelSizeStyle.id != null)	{
			errorMessage = ErrorMessage.getStyleBadTypeMessage('scale',this.labelSizeStyle.id,Grid.LABEL_SIZE_STYLE_TYPE,this.labelSizeStyle.type);
			this.labelSizeStyle.checkType(Grid.LABEL_SIZE_STYLE_TYPE,errorMessage);				
		}
		if (this.labelTextStyle.id != null)	{
			errorMessage = ErrorMessage.getStyleBadTypeMessage('scale',this.labelTextStyle.id,Grid.TEXT_STYLE_TYPE,this.labelTextStyle.type);
			this.labelTextStyle.checkType(Grid.TEXT_STYLE_TYPE,errorMessage);
		}
	}
		
	private function getLabel(angle:Number):String {
		var res:String = String(Number((angle - this.chart.startAngle)*chart.valueScale));
		if (Number(res) == 0)
			return '0';
		var decimalPosition:Number = res.indexOf('.');
		if (decimalPosition == -1)
			decimalPosition = res.length;
		var intString:String = res.substring(0,decimalPosition);
		var decimalString:String = '0.';
		if (res.substr(decimalPosition+1).length > 0)
			decimalString += res.substr(decimalPosition+1);
		else
			decimalString += '0';
			
		var decimal:Number = Math.round(Number(decimalString)*this.decimalPlaces)/this.decimalPlaces;			
		decimalString = String(decimal);
		decimalString = decimalString.substr(1);
		if (decimalString.length == 0) {
			decimalString = '.';
		}
		
		if ((decimalString.length-1) < decimalPlaces) {
			for (i = 0;i<(decimalPlaces - decimalString.length + 2);i++)
				decimalString += '0';
		}
		decimalString = decimalString.substr(1);
			
		var newIntString:String = '';
		for (var i:Number = 0;i < intString.length;i++) {
			var j:Number = intString.length - i - 1;
			if (Math.round(i/3)==i/3 && i>0) {
				newIntString = labelsThousandSeparator + newIntString;
			}
			newIntString = intString.charAt(j) + newIntString;
		}
		if (decimalPlaces > 0)
			return newIntString + labelsDecimalSeparator + decimalString;
		else
			return newIntString;
	}
		
	private function drawPoint(angle:Number):Void {
		var xpos:Number = -this.radius*Math.cos(angle*Math.PI/180);
		var ypos:Number = -this.radius*Math.sin(angle*Math.PI/180);
		var endXPos:Number = -(this.radius+this.length)*Math.cos(angle*Math.PI/180);
		var endYPos:Number = -(this.radius+this.length)*Math.sin(angle*Math.PI/180);
		this.moveTo(xpos,ypos);
		this.lineTo(endXPos,endYPos);
	}
		
	private function drawLabel(label:String,angle:Number,index:Number):Void {			
		var tf:TextField = this.createTextField('labelField@'+index,this.getNextHighestDepth(),0,0,0,0);
		tf.text = this.labelsPrefix + label + this.labelsPostfix;
		if (this.labelBackgroundStyle!=null) {
			tf.background = this.labelBackgroundStyle.enabled;
			tf.backgroundColor = this.labelBackgroundStyle.color;
		}
		if (this.labelBorderStyle!=null) {
			tf.border = this.labelBorderStyle.enabled;
			tf.borderColor = this.labelBorderStyle.color;
		}
		if (this.labelTextStyle!=null) {
			var format:TextFormat = new TextFormat();
			format.align = this.labelTextStyle.align.toLowerCase();
			format.bold = this.labelTextStyle.bold;
			format.italic = this.labelTextStyle.italic;
			format.underline = this.labelTextStyle.underline;
			format.color = this.labelTextStyle.color;
			format.size = this.labelTextStyle.size;
			format.font = this.labelTextStyle.font;
			tf.setTextFormat(format);
		}
		if (this.labelSizeStyle!=null && !isNaN(this.labelSizeStyle.width) && !isNaN(this.labelSizeStyle.height)) {
			tf._width = this.labelSizeStyle.width;
			tf._height = this.labelSizeStyle.height;
		}else {
			tf.autoSize = true;
		}			
		tf.selectable = false;
		var xpos:Number = -this.labelPositionStyle.radius*Math.cos(angle*Math.PI/180);
		var ypos:Number = -this.labelPositionStyle.radius*Math.sin(angle*Math.PI/180);
		var mdx:Number = 0;
		var mdy:Number = 0;
			
		var c:Number = Math.cos(angle * Math.PI/180);
		var s:Number = Math.sin(angle * Math.PI/180);
			
		c = Math.round(c*100)/100;
		s = Math.round(s*100)/100;
			
		if (c == 0)
			mdx = -1/2;
		else if (c > 0)
			mdx = -1;
		else if (c < 0)
			mdx = 0;
				
		if (s == 0)
			mdy = -1/2;
		else if (s > 0)
			mdy = -1;
		else
			mdy = 0;
		
		tf._x = xpos + tf._width*mdx;
		tf._y = ypos + tf._height*mdy;
	}
}