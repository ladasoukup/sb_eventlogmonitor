﻿import styles.BackgroundStyle;
import styles.BorderStyle;

class display.DisplayContainer extends MovieClip {
	
	public var backgroundStyle:BackgroundStyle;
	public var borderStyle:BorderStyle;
		
	public function applyStyles():Void {
		if ((this.backgroundStyle != null)&&(this.backgroundStyle.enabled))
			this.beginFill(this.backgroundStyle.color,this.backgroundStyle.alpha);
		if ((this.borderStyle != null)&&(this.borderStyle.enabled))
			this.lineStyle(this.borderStyle.thickness,this.borderStyle.color,this.borderStyle.alpha);
	}
}