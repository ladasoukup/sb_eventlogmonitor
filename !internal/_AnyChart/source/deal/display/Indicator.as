﻿import shapes.IShape;
import errors.ErrorMessage;
import display.DisplayContainer;
import display.IGaugeChartElement;
import display.Chart;
import display.DisplayObject;
	
class display.Indicator extends DisplayContainer implements IGaugeChartElement {
	
	public static var BACKGROUND_STYLE_TYPE:String = 'indicator_background';
	public static var BORDER_STYLE_TYPE:String = 'indicator_border';
		
	public var value:Number;
	public var chart:Chart;
	public var shape:DisplayObject;
	
	public function Indicator() {
		shape = DisplayObject(this.createEmptyMovieClip('shape',this.getNextHighestDepth()));
	}
	
	public function init():Void {
		var msg:String;
		if (this.backgroundStyle.id != null) {
			msg = ErrorMessage.getStyleBadTypeMessage('indicator',this.backgroundStyle.id,Indicator.BACKGROUND_STYLE_TYPE,this.backgroundStyle.type);
			this.backgroundStyle.checkType(Indicator.BACKGROUND_STYLE_TYPE,msg);
		}
		if (this.borderStyle.id != null) {
			msg = ErrorMessage.getStyleBadTypeMessage('indicator',this.borderStyle.id,Indicator.BORDER_STYLE_TYPE,this.borderStyle.type);
			this.borderStyle.checkType(Indicator.BORDER_STYLE_TYPE,msg);
		}
	}
		
	public function draw():Void	{
		super.applyStyles();
		this.shape.draw(this);
		this._rotation = (this.value - this.chart.minimumValue)/this.chart.valueScale;
	}
}