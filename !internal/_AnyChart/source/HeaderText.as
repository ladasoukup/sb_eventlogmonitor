﻿if (_root.IS_TRIAL == true) {
	var trial_tf:TextFormat = new TextFormat();
	trial_tf.align = 'center';
	trial_tf.font = 'Verdana';
	
	_root['backgroundOfText'].removeMovieClip();
	var bgMovie:MovieClip = _root.createEmptyMovieClip('backgroundOfText',_root.getNextHighestDepth());
	
	_root['trial_field'].removeTextField();
	_root.createTextField('trial_field',_root.getNextHighestDepth(),0,0,Stage.width,20);
	bgMovie.beginFill(0xCCCCCC,50);
	bgMovie.moveTo(0,0);
	bgMovie.lineTo(Stage.width,0);
	bgMovie.lineTo(Stage.width,20);
	bgMovie.lineTo(0,20);
	bgMovie.lineTo(0,0);
	bgMovie.endFill();
	_root['trial_field'].html = true;
	_root['trial_field'].htmlText = 'AnyChart '+_root.VERSION+' Trial version. <a href="http://anychart.com">http://anychart.com</a>';
	_root['trial_field'].setTextFormat(trial_tf); 
}