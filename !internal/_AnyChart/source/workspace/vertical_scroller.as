﻿import mx.events.EventDispatcher
class workspace.vertical_scroller
{
	//enabled
	private var s_enabled:Boolean;
	
	//position
	private var s_x:Number;
	private var s_y:Number;
	
	//rotation
	private var s_rotation:Number;
	
	//size
	private var s_width:Number;
	private var s_height:Number;
	
	//background
	private var s_background_enabled:Boolean;
	private var s_background_color:Number;
	private var s_background_alpha:Number;
	
	//border
	private var s_border_enabled:Boolean;
	private var s_border_size:Number;
	private var s_border_color:Number;
	private var s_border_alpha:Number;
	
	//button background
	private var s_button_background_enabled:Boolean;
	private var s_button_background_color:Number;
	private var s_button_background_alpha:Number;
	
	//button border
	private var s_button_border_enabled:Boolean;
	private var s_button_border_size:Number;
	private var s_button_border_color:Number;
	private var s_button_border_alpha:Number;
	
	//button triangle setings
	//background
	private var s_button_triangle_background_enabled:Boolean;
	private var s_button_triangle_background_color:Number;
	private var s_button_triangle_background_alpha:Number;
	//border
	private var s_button_triangle_border_enabled:Boolean;
	private var s_button_triangle_border_size:Number;
	private var s_button_triangle_border_alpha:Number;
	private var s_button_triangle_border_color:Number;
	
	//scrolled positions
	private var s_positions:Number;
	private var s_current_position:Number;
	private var s_scroll_height:Number;
	
	//scroller movie
	private var scroller_mc:MovieClip;
	private var scroll_mc:MovieClip;
	private var up_button:MovieClip;
	private var down_button:MovieClip;	
	//--------------------------------------------
	//getters and setters
	//turns scroller on/off
	function set enabled(e:Boolean):Void
	{
		s_enabled = e;
	}
	//set x position
	function set x(new_x:Number):Void
	{
		s_x = new_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		s_y = new_y;
	}
	//set width
	function set width(w:Number):Void
	{
		s_width = w;
	}
	//set rotation
	function set rotation(r:Number):Void
	{
		s_rotation = r;
	}
	//set height
	function set height(h:Number):Void
	{
		s_height = h;
	}
	//set background enabled
	function set background_enabled(e:Boolean):Void
	{
		s_background_enabled = e;
	}
	//set background alpha
	function set background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("scroller background alpha should be between 0 and 100");
		}else
		{
			s_background_alpha = a;
		}
	}
	//set background color
	function set background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("scroller background color should be between 0 and 0xFFFFFF");
		}else
		{
			s_background_color = clr;
		}
	}
	//set buttons background enabled
	function set buttons_background_enabled(e:Boolean):Void
	{
		s_button_background_enabled = e;
	}
	//set buttons backgorund color
	function set buttons_background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("scroller buttons background color should be between 0 and 0xFFFFFF");
		}else
		{
			s_button_background_color = clr;
		}
	}
	//set buttons background alpha
	function set buttons_background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("scroller buttons background alpha should be between 0 and 100");
		}else
		{
			s_button_background_alpha = a;
		}
	}	
	//set triangle background
	function set buttons_triangle_background_enabled(e:Boolean)
	{
		s_button_triangle_background_enabled = e;
	}	
	//set triangle background color
	function set buttons_triangle_background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("scroller buttons triangle background color should be between 0 and 0xFFFFFF");
		}else
		{
			s_button_triangle_background_color = clr;
		}
	}
	//set trinangle backgroudn alpha
	function set buttons_triangle_background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("scroller buttons triangle background alpha should be between 0 and 100");
		}else
		{
			s_button_triangle_background_alpha = a;
		}
	}
	//set triangle border enabled
	function set buttons_triangle_border_enabled(e:Boolean):Void
	{
		s_button_triangle_border_enabled = e;
	}
	//set border size
	function set buttons_triangle_border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("scroller buttons triangle border size should be between 0 and 255");
		}else
		{
			s_button_triangle_border_size = s;
		}
	}
	//set border alpha
	function set buttons_triangle_border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("scroller buttons triangle border alpha should be between 0 and 100");
		}else
		{
			s_button_triangle_border_alpha = a;
		}
	}
	//set border color
	function set buttons_triangle_border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("scroller buttons triangle border color should be between 0 and 0xFFFFFF");
		}else
		{
			s_button_triangle_border_color = clr;
		}
	}
	//set border enabled
	function set border_enabled(e:Boolean):Void
	{
		s_border_enabled = e;
	}
	//set border size
	function set border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("scroller border color should be between 0 and 0xFFFFFF");
		}else
		{
			s_border_color = clr;
		}
	}
	//set border alpha
	function set border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("scroller border alpha should be between 0 and 100");
		}else
		{
			s_border_alpha = a;
		}
	}
	//set border size
	function set border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("scroller border size should be between 0 and 255");
		}else
		{
			s_border_size = s;
		}
	}
	//set button border enabled
	function set button_border_enabled(e:Boolean):Void
	{
		s_button_border_enabled = e;
	}
	//set button border size
	function set button_border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("scroller button border size should be between 0 and 255");
		}else
		{
			s_button_border_size = s;
		}
	}
	//set button border alpha
	function set button_border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("scroller button border alpha should be between 0 and 100");
		}else
		{
			s_button_border_alpha = a;
		}
	}
	//set button border color
	function set button_border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("scroller button border color should be between 0 and 0xFFFFFF");
		}else
		{
			s_button_background_color = clr;
		}
	}
	//set scroller positions
	function set positions(pos:Number):Void
	{
		if ((pos<0) or (Math.round(pos)!=pos))
		{
			_root.showError("scroller positions should be positive and integer");
		}else
		{
			s_positions = pos;
		}
	}
	//get current position
	function get current_position():Number
	{
		return s_current_position;
	}
	//--------------------------------------------
	//constructor
	function vertical_scroller(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['scroll_bar_'+i]!=undefined)
		{
			i++;
		}
		scroller_mc = target_mc.createEmptyMovieClip('scroll_bar_'+i,target_mc.getNextHighestDepth());
		//init defaults
		s_enabled = true;
		s_width = 10;
		s_background_enabled = true;
		s_background_color = 0xF4EFEA;
		s_background_alpha = 100;
		s_border_enabled = false;
		s_border_size = 1;
		s_border_color = 0x000000;
		s_border_alpha = 100;
		s_button_background_enabled = true;
		s_button_background_color = 0xF4EFEA;
		s_button_background_alpha = 100;
		s_button_border_enabled = true;
		s_button_border_size = 0.5;
		s_button_border_alpha = 100;
		s_button_border_color = 0x000000;
		s_button_triangle_border_enabled = false;
		s_button_triangle_border_size = 1;
		s_button_triangle_border_alpha = 100;
		s_button_triangle_border_color = 0x000000;
		s_button_triangle_background_enabled = true;
		s_button_triangle_background_color = 0x000000;
		s_button_triangle_background_alpha = 100;
		s_positions = 0;
		s_rotation = 0;
	}
	//--------------------------------------------
	//paint
	function paint():Void
	{
		if (s_enabled)
		{
			scroller_mc._x = s_x;
			scroller_mc._y = s_y;
			scroller_mc._rotation = s_rotation;
			if (s_border_enabled)
			{
				scroller_mc.lineStyle(s_border_size,s_border_color,s_border_alpha);
			}
			if (s_background_enabled)
			{
				scroller_mc.beginFill(s_background_color,s_background_color);
			}
			//---
			scroller_mc.moveTo(0,0);
			scroller_mc.lineTo(s_width,0);
			scroller_mc.lineTo(s_width,s_height);
			scroller_mc.lineTo(0,s_height);
			scroller_mc.lineTo(0,0);
			//---
			if (s_background_enabled)
			{
				scroller_mc.endFill();
			}
			paint_up();		
			paint_down();
			paint_scroll_button();
		}
	}
	//paint up button
	private function paint_up():Void
	{
		up_button = scroller_mc.createEmptyMovieClip('up_button_movie',scroller_mc.getNextHighestDepth());
		if (s_button_border_enabled)
		{
			up_button.lineStyle(s_border_size,s_border_color,s_border_alpha);
		}
		if (s_button_background_enabled)
		{
			up_button.beginFill(s_button_background_color,s_button_background_alpha);
		}
		//---
		up_button.moveTo(0,0);
		up_button.lineTo(s_width,0);
		up_button.lineTo(s_width,s_width);
		up_button.lineTo(0,s_width);
		up_button.lineTo(0,0);
		//---
		if (s_button_background_enabled)
		{
			up_button.endFill();
		}
		//paint triangle
		if (s_button_triangle_background_enabled)
		{
			up_button.beginFill(s_button_triangle_background_color,s_button_triangle_background_alpha);
		}
		if (s_button_triangle_border_enabled)
		{
			up_button.lineStyle(s_button_triangle_border_size,s_button_triangle_border_color,s_button_triangle_border_alpha);
		}else
		{
			up_button.lineStyle();
		}
		//--
		//triangle size
		var width:Number = (3/4)*s_width;
		var x:Number = (s_width-width)/2;
		//--
		up_button.moveTo(x,s_width-x);
		up_button.lineTo(x+width,s_width-x);
		up_button.lineTo(x+width/2,x);
		up_button.lineTo(x,s_width-x);
		//--
		if (s_button_triangle_background_enabled)
		{
			up_button.endFill();
		}
		var ths = this;
		up_button.onRelease = function()
		{
			if ((ths.s_current_position-1)>=0)
			{
				ths.s_current_position -= 1;
				ths.scroll_mc._y -= ths.s_scroll_height;
				ths.dispatchEvent ( {type: 'onChange',target: ths, position: ths.s_current_position});
			}
		}
	}
	//paint down button
	private function paint_down():Void
	{
		down_button = scroller_mc.createEmptyMovieClip('down_button_movie',scroller_mc.getNextHighestDepth());
		down_button._y = s_height - s_width;
		if (s_button_border_enabled)
		{
			down_button.lineStyle(s_border_size,s_border_color,s_border_alpha);
		}
		if (s_button_background_enabled)
		{
			down_button.beginFill(s_button_background_color,s_button_background_alpha);
		}
		//---
		down_button.moveTo(0,0);
		down_button.lineTo(s_width,0);
		down_button.lineTo(s_width,s_width);
		down_button.lineTo(0,s_width);
		down_button.lineTo(0,0);
		//---
		if (s_button_background_enabled)
		{
			down_button.endFill();
		}
		//paint triangle
		if (s_button_triangle_background_enabled)
		{
			down_button.beginFill(s_button_triangle_background_color,s_button_triangle_background_alpha);
		}
		if (s_button_triangle_border_enabled)
		{
			down_button.lineStyle(s_button_triangle_border_size,s_button_triangle_border_color,s_button_triangle_border_alpha);
		}else
		{
			down_button.lineStyle();
		}
		//--
		//triangle size
		var width:Number = (3/4)*s_width;
		var x:Number = (s_width-width)/2;
		//--
		down_button.moveTo(x,x);
		down_button.lineTo(x+width,x);
		down_button.lineTo(x+width/2,s_width-x);
		down_button.lineTo(x,x);
		//--
		if (s_button_triangle_background_enabled)
		{
			down_button.endFill();
		}
		var ths = this;
		down_button.onRelease = function()
		{
			if ((ths.s_current_position+1)<ths.s_positions)
			{
				ths.s_current_position += 1;
				ths.scroll_mc._y += ths.s_scroll_height;
				ths.dispatchEvent ( {type: 'onChange',target: ths, position: ths.s_current_position});
			}
		}
	}
	//scroll button
	private function paint_scroll_button():Void
	{
		s_current_position = 0;
		var h:Number = (s_height-s_width*2)/s_positions;
		s_scroll_height = h;
		scroll_mc = scroller_mc.createEmptyMovieClip('scroll_button_movie',scroller_mc.getNextHighestDepth());
		scroll_mc._y = s_width;
		if (s_button_border_enabled)
		{
			scroll_mc.lineStyle(s_button_border_size,s_button_border_color,s_button_border_alpha);
		}
		if (s_button_background_enabled)
		{
			scroll_mc.beginFill(s_background_color,s_background_alpha);
		}
		//--
		scroll_mc.moveTo(0,0);
		scroll_mc.lineTo(s_width,0);
		scroll_mc.lineTo(s_width,h);
		scroll_mc.lineTo(0,h);
		scroll_mc.lineTo(0,0);
		//--
		var ths = this;
		if (s_button_background_enabled)
		{
			scroll_mc.endFill();
		}
		scroll_mc.onDragOut = function()
		{
			var y:Number = this._parent._ymouse - ths.s_width;
			y = y*ths.s_positions;
			y = Math.floor(y/(ths.s_height - ths.s_width*2));
			ths.s_current_position = y;
			ths.dispatchEvent ( {type: 'onChange',target: ths, position: ths.s_current_position});
			y = y*h;
			if (y<0)
			{
				this._y = ths.s_width;
			}else
			{
				if (y>(ths.s_height-h-ths.s_width*2))
				{
					this._y = ths.s_height - h - ths.s_width;
				}else
				{
					this._y = ths.s_width + y;
				}
			}
		}
	}	
}