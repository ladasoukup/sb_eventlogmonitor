﻿class workspace.legend
{
	public var dataSource:String;
	//eanbled
	private var l_enabled:Boolean;
	//position
	private var l_x:Number;
	private var l_y:Number;
	
	//data
	private var l_names:Array;
	private var l_values:Array;
	private var l_colors:Array;
	
	public var header_enabled:Boolean = true;
	
	//header captions
	private var l_header_names_column_text:String;
	private var l_header_values_column_text:String;
	
	//header background
	private var l_header_background_enabled:Boolean;
	private var l_header_background_color:Number;
	private var l_header_background_alpha:Number;
	
	//main background
	private var l_background_enabled:Boolean;
	private var l_background_color:Number;
	private var l_background_alpha:Number;	
	
	//legend border settings
	private var l_border_enabled:Boolean;
	private var l_border_size:Number;
	private var l_border_color:Number;
	private var l_border_alpha:Number;
	
	//names column width
	private var l_names_column_width:Number;
	//values column width
	private var l_values_column_width:Number;
	//colors column width
	private var l_colors_column_width:Number;	
	//header height
	private var l_header_height:Number;
	
	//rows count
	private var l_rows:Number;
	private var l_rows_auto_count:Boolean;
	
	//header text format
	private var l_header_text_format:TextFormat;
	
	//names column text format
	private var l_names_text_format:TextFormat;
	
	//values column text fomrat
	private var l_values_text_format:TextFormat;
	
	//vales column enabled
	private var l_values_column_enabled:Boolean;
	
	//names column enabled
	private var l_names_column_enabled:Boolean;
	
	//legend width
	private var l_width:Number;
	
	//rows height
	private var rows_height:Number;
	
	//movie clips
	private var legend_mc:MovieClip;
	private var header_mc:MovieClip;
	private var main_mc:MovieClip;
	
	//colors labels
	private var l_colors_items_width:Number;
	private var l_colors_items_height:Number;
	
	var scroller;
	
	//getters and setters
	//set x position
	function set enabled(e:Boolean):Void
	{
		l_enabled = e;
	}
	function set x(new_x:Number):Void
	{
		l_x = new_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		l_y = new_y;
	}
	//set names
	function set names(n:Array):Void
	{
		l_names = n;
	}
	//set values
	function set values(v:Array):Void
	{
		l_values = v;
	}
	//set colors
	function set colors(c:Array):Void
	{
		l_colors = c;
	}
	//set border enabled
	function set border_enabled(e:Boolean):Void
	{
		l_border_enabled = e;
	}
	//set border size
	function set border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("legend border size should be between 0 and 255");
		}else
		{
			l_border_size = s;
		}
	}
	//set names column name
	function set names_column_name(txt:String):Void
	{
		l_header_names_column_text = txt;
	}
	//set values column name
	function set values_column_name(txt:String):Void
	{
		l_header_values_column_text = txt;
	}
	//set border color
	function set border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("legend border color should be between 0 and 0xFFFFFF");
		}else
		{
			l_border_color = clr;
		}
	}
	//set border color
	function set border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("legend border alpha should be between 0 and 100");
		}else
		{
			l_border_alpha = a;
		}
	}
	//set names column width
	function set names_column_width(w:Number):Void
	{
		if (w<=0)
		{
			_root.showError("legend names column width should be positive");
		}else
		{
			l_names_column_width = w;
		}
	}
	//set values column width
	function set values_column_width(w:Number):Void
	{
		if (w<=0)
		{
			_root.showError("legend values column width should be positive");
		}else
		{
			l_values_column_width = w;
		}
	}
	//set colors column width
	function set colors_column_width(w:Number):Void
	{
		if (w<=0)
		{
			_root.showError("legend colors column width should be positive");
		}else
		{
			l_colors_column_width = w;
		}
	}
	//set rows count
	function set rows(r:Number):Void
	{
		if ((r<=0) or (Math.round(r)!=r))
		{
			_root.showError("legen rows should be positive and integer");
		}else
		{
			l_rows = r;
		}
	}
	//set rows auto count
	function set rows_auto_count(e:Boolean):Void
	{
		l_rows_auto_count = e;
	}
	//set header height
	function set header_height(h:Number):Void
	{
		if (h<=0)
		{
			_root.showError("header height should be positive");
		}else
		{
			l_header_height = h;
		}
	}
	//set header text format
	function set header_text_format(tf:TextFormat):Void
	{
		l_header_text_format = tf;
	}
	//set names column text format
	function set names_text_format(tf:TextFormat):Void
	{
		l_names_text_format = tf;
	}
	//set values column text format
	function set values_text_format(tf:TextFormat):Void
	{
		l_values_text_format = tf;
	}
	//set background
	function set background_enabled(e:Boolean):Void
	{
		l_background_enabled = e;
	}
	function set background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("background color should be between 0 and 0xFFFFFF");
		}else
		{
			l_background_color = clr;
		}
	}
	function set background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("background alpha should be between 0 and 100");
		}else
		{
			l_background_alpha = a;
		}
	}
	function set header_background_enabled(e:Boolean):Void
	{
		l_header_background_enabled = e;
	}
	function set header_background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("legend header background color should be between 0 and 0xFFFFFF");		
		}else
		{
			l_header_background_color = clr;
		}
	}
	function set header_background_alhpa(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("legend header background alpha should be between 0 and 100");
		}else
		{
			l_header_background_alpha = a;
		}
	}
	function set values_column_enabled(e:Boolean):Void
	{
		l_values_column_enabled = e;
	}
	function set names_column_enabled(e:Boolean):Void
	{
		l_names_column_enabled = e;
	}
	//---------------------------------
	//constructor
	function legend(target_mc:MovieClip)
	{
		var i:Number = 0;		
		legend_mc = target_mc.createEmptyMovieClip('legend_movie_'+i,DepthManager.LEGEND_DEPTH);
		//init defaults
		l_enabled = false;
		l_x = 0;
		l_y = 0;
		l_border_enabled = true;
		l_border_size = 1;
		l_border_color = 0x000000;
		l_border_alpha = 100;
		l_header_names_column_text = 'Name';
		l_header_values_column_text = 'Value';
		l_header_background_enabled = true;
		l_header_background_color = 0xEFE6DE;
		l_header_background_alpha = 100;
		l_background_enabled = true;
		l_background_color = 0xF4EFEA;
		l_background_alpha = 100;
		l_colors_column_width = 20;
		l_rows_auto_count = true;
		l_header_text_format = new TextFormat();
		l_header_text_format.font = 'Verdana';
		l_header_text_format.bold = true;
		l_colors_items_width = 10;
		l_colors_items_height = 10;
		l_names_text_format = new TextFormat();
		l_names_text_format.font = 'Verdana';
		l_values_text_format = new TextFormat();
		l_values_text_format.font = 'Verdana';
		scroller = new workspace.vertical_scroller(legend_mc);
	}
	
	private function getValuesWidth():Number {
		if (l_values_column_width != undefined) {
			return l_values_column_width;
		}
		
		var valueExt:Object = l_header_text_format.getTextExtent(l_header_values_column_text);
		var w:Number = valueExt.textFieldWidth;
		var max:Number = l_names.length;
		if (l_values.length > max)
			max = l_values.length;
		if (l_colors.length > max)
			max = l_colors.length;
		
		for (var i:Number = 0;i<max;i++) {
			valueExt = l_values_text_format.getTextExtent(l_values[i]);
			if (valueExt.textFieldWidth > w)
				w = valueExt.textFieldWidth;
		}
		
		
		l_values_column_width = w + 5;
		return w + 5;
	}
	
	private function getNamesWidth():Number {
		if (l_names_column_width != undefined)
			return l_names_column_width;
			
		var nameExt:Object = l_header_text_format.getTextExtent(l_header_names_column_text);
		var w:Number = nameExt.textFieldWidth;
		var max:Number = l_names.length;
		if (l_values.length > max)
			max = l_values.length;
		if (l_colors.length > max)
			max = l_colors.length;
		for (var i:Number = 0;i<max;i++) {
			nameExt = l_names_text_format.getTextExtent(l_names[i]);
			if (w < nameExt.textFieldWidth)
				w = nameExt.textFieldWidth;
		}
		l_names_column_width = w + 5;
		return w + 5;
	}
	
	//painting
	function paint():Void
	{
		if (l_enabled)
		{
			legend_mc._x = l_x;
			legend_mc._y = l_y;
			l_width = l_colors_column_width;
			//set columns for painting
			if (dataSource == 'blocks')
				l_values_column_enabled = false;
				
			if (l_values==undefined)
			{
				if (l_values_column_enabled==undefined)
				{
					l_values_column_enabled = false;
				}
			}else
			{
				if (l_values_column_enabled==undefined)
				{
					l_values_column_enabled = true;			
				}
				if (l_values_column_enabled)
				{
					l_width += getValuesWidth();
				}
			}
			if (l_names==undefined)
			{
				if (l_names_column_enabled==undefined)
				{
					l_names_column_enabled = false;
				}
			}else
			{
				if (l_names_column_enabled==undefined)
				{
					l_names_column_enabled = true;
				}
				if (l_names_column_enabled)
				{
					l_width += getNamesWidth();
				}
			}
			//---
			paint_header();		
			paint_main();
			show_data(0);
			//set scroller		
			scroller.x = l_width;
			scroller.y = 0;
			scroller.positions = l_colors.length - l_rows + 1;
			scroller.height = l_header_height + l_rows*rows_height;
			var evt_obj:Object = new Object();
			var ths = this;
			evt_obj.onChange = function(evts_obj:Object)
			{
				ths.show_data(evts_obj.position);
			}
			scroller.addEventListener('onChange',evt_obj);
			scroller.paint();
		}
	}
	function remove():Void
	{
		legend_mc.removeMovieClip();
	}
	//paint header
	private function paint_header():Void
	{
		if (!header_enabled)
			return;
		header_mc = legend_mc.createEmptyMovieClip("header_movie_",legend_mc.getNextHighestDepth());
		if (l_header_height==undefined)
		{
			var size_obj:Object = new Object();
			size_obj = l_header_text_format.getTextExtent('Wq');
			l_header_height = size_obj.textFieldHeight;
			size_obj = l_header_text_format.getTextExtent('Wq');
			if (size_obj.textFieldHeight>l_header_height)
			{
				l_header_height = size_obj.textFieldHeight;
			}
		}
		if (l_border_enabled)
		{
			header_mc.lineStyle(l_border_size,l_border_color,l_border_alpha);
		}
		if (l_background_enabled)
		{
			header_mc.beginFill(l_header_background_color,l_header_background_alpha);
		}
		//paint rect
		header_mc.moveTo(0,0);
		header_mc.lineTo(l_width,0);
		header_mc.lineTo(l_width,l_header_height);
		header_mc.lineTo(0,l_header_height);
		header_mc.lineTo(0,0);
		//--
		//show names column text
		var x_pos:Number = l_colors_column_width;
		if (l_names_column_enabled)
		{
			header_mc.createTextField('names_column_txt',header_mc.getNextHighestDepth(),x_pos,0,getNamesWidth(),l_header_height);
			header_mc['names_column_txt'].selectable = false;
			header_mc['names_column_txt'].html = true;
			header_mc['names_column_txt'].htmlText = l_header_names_column_text;
			header_mc['names_column_txt'].setTextFormat(l_header_text_format);
			x_pos += getNamesWidth();
		}
		//show values column text
		if (l_values_column_enabled)
		{
			header_mc.createTextField('values_column_txt',header_mc.getNextHighestDepth(),x_pos,0,getValuesWidth(),l_header_height);
			header_mc['values_column_txt'].selectable = false;
			header_mc['values_column_txt'].html = true;
			header_mc['values_column_txt'].htmlText = l_header_values_column_text;
			header_mc['values_column_txt'].setTextFormat(l_header_text_format);
		}
		if (l_background_enabled)
		{
			header_mc.endFill();
		}
		//show colors column header		
		show_rect(-1);
	}
	//show rectangle
	private function show_rect(n:Number,pos:Number):Void
	{
		var x:Number = (l_colors_column_width - l_colors_items_width)/2;
		var y:Number;
		var clr:Number;
		var target;
		if (n==-1)
		{
			if (header_enabled) {
				y = (l_header_height - l_colors_items_height)/2;
				clr = 0x000000;
				target = header_mc;
			}
		}else
		{
			if (header_enabled)
				y = (l_header_height - l_colors_items_height)/2 + rows_height*n;
			else {
				y = rows_height * n + l_colors_items_height/2;
			}
			clr = l_colors[pos];
			target = main_mc;
		}		
		clr = Number(clr);
		target['rects'+n].removeMovieClip();
		target.createEmptyMovieClip('rects'+n,target.getNextHighestDepth());		
		target['rects'+n].beginFill(clr);
		target['rects'+n].moveTo(x,y);
		target['rects'+n].lineTo(x+l_colors_items_width,y);
		target['rects'+n].lineTo(x+l_colors_items_width,y+l_colors_items_height);
		target['rects'+n].lineTo(x,y+l_colors_items_height);
		target['rects'+n].lineTo(x,y);
		target['rects'+n].endFill();
	}
	//show main part
	private function paint_main():Void
	{
		if (rows_height==undefined)
		{
			var size_obj:Object = new Object();
			size_obj = l_names_text_format.getTextExtent('Wq');
			rows_height = size_obj.textFieldHeight;
			size_obj = l_values_text_format.getTextExtent('Wq');
			if (size_obj.textFieldHeight>rows_height)
			{
				rows_height = size_obj.textFieldHeight;
			}
		}
		if (l_rows_auto_count)
		{
			if (l_rows==undefined)
			{
				l_rows = l_names.length;
			}else
			{
				if (l_rows>l_names.length)
				{
					l_rows = l_names.length;
				}
			}
		}
		if (!header_enabled) {
			l_header_height = 0;
		}
		main_mc = legend_mc.createEmptyMovieClip('main_movie',legend_mc.getNextHighestDepth());
		main_mc._y = l_header_height;
		if (l_border_enabled)
		{
			main_mc.lineStyle(l_border_size,l_border_color,l_border_alpha);
		}
		if (l_background_enabled)
		{
			main_mc.beginFill(l_background_color,l_background_alpha);
		}
		//---
		main_mc.moveTo(0,0);
		main_mc.lineTo(l_width,0);
		main_mc.lineTo(l_width,l_rows*rows_height);
		main_mc.lineTo(0,l_rows*rows_height);
		main_mc.lineTo(0,0);
		//---
		if (l_background_enabled)
		{
			main_mc.endFill();
		}
		main_mc.lineStyle();
	}
	//show data
	private function show_data(n:Number):Void
	{
		var i:Number;
		var x:Number;
		var y:Number;
		for (i=n;i<(n+l_rows);i++)
		{
			x = l_colors_column_width;
			y = rows_height*(i-n);
			//show name
			if (l_names_column_enabled)
			{
				main_mc['names_texts_'+(i-n)].removeTextField();
				if (l_names[i]!=undefined)
				{
					main_mc.createTextField('names_texts_'+(i-n),main_mc.getNextHighestDepth(),x,y,getNamesWidth(),rows_height);
					main_mc['names_texts_'+(i-n)].selectable = false;
					main_mc['names_texts_'+(i-n)].html = true;
					main_mc['names_texts_'+(i-n)].htmlText = l_names[i];
					main_mc['names_texts_'+(i-n)].setTextFormat(l_names_text_format);
				}
				x += getNamesWidth();
			}
			//show value
			if (l_values_column_enabled)
			{
				main_mc['values_texts_'+(i-n)].removeTextField();
				if (l_values[i]!=undefined)
				{
					main_mc.createTextField('values_texts_'+(i-n),main_mc.getNextHighestDepth(),x,y,getValuesWidth(),rows_height);
					main_mc['values_texts_'+(i-n)].selectable = false;
					main_mc['values_texts_'+(i-n)].html = true;
					main_mc['values_texts_'+(i-n)].htmlText = l_values[i];
					main_mc['values_texts_'+(i-n)].setTextFormat(l_values_text_format);
				}
			}
			//show color rect
			show_rect(i-n,i);
		}
	}
}