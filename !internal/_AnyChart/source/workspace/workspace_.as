﻿class workspace.workspace_
{
	
	private var _xDeepOffset:Number = 0;
	private var _yDeepOffset:Number = 0;
	
	public function set xDeepOffset(value:Number):Void {
		_xDeepOffset = value;
		checkHorizontalPositions();
	}
	
	public function set yDeepOffset(value:Number):Void {
		_yDeepOffset = value;
		checkVerticalPositions();
	}
	
	//position
	private var w_x:Number;
	private var w_y:Number;
	
	//chart area size
	private var w_chart_area_width:Number;
	private var w_chart_area_height:Number;
	private var w_chart_area_deep:Number;
	
	//base area size
	private var w_base_area_height:Number;
	private var w_base_area_deep:Number;
	
	//chart area background
	private var w_chart_area_background_enabled:Boolean;
	private var w_chart_area_background_color:Number;
	private var w_chart_area_background_alpha:Number;
	
	public var chart_area_background_type:String;
	public var chart_area_gradient_type:String;
	public var chart_area_gradient_colors:Array;
	public var chart_area_gradient_alphas:Array;
	public var chart_area_gradient_ratios:Array;
	public var chart_area_gradient_matrix:Object;
	
	//chart area image background
	private var w_chart_area_background_image_source:String;
	private var w_chart_area_background_image_position:String;
	
	//base area background
	private var w_base_area_background_enabled:Boolean;
	private var w_base_area_background_color:Number;
	private var w_base_area_background_alpha:Number;
	
	//chart area and base area enabled
	private var w_chart_area_enabled:Boolean;
	private var w_base_area_enabled:Boolean;
	
	//chart area border
	private var w_chart_area_border_enabled:Boolean;
	private var w_chart_area_border_size:Number;
	private var w_chart_area_border_alpha:Number;
	private var w_chart_area_border_color:Number;
	
	//base area border
	private var w_base_area_border_enabled:Boolean;
	private var w_base_area_border_size:Number;
	private var w_base_area_border_color:Number;
	private var w_base_area_border_alpha:Number;
	
	//3d rotation
	private var w_rotate_3d:Number;
	
	//workspace name
	private var w_name_text:String;
	private var w_name_text_format:TextFormat;
	public var name_spacing:Number = 5;
	
	//workspace subname
	private var w_subname_text:String;
	private var w_subname_text_format:TextFormat;
	public var subname_spacing:Number = 5;
	
	//movieClips
	public var workspace_mc:MovieClip;
	public var chart_area_mc:MovieClip;
	public var base_area_mc:MovieClip;
	
	//worksace x axis name
	private var w_x_axis_name_text:String;
	private var w_x_axis_name_text_format:TextFormat;
	public var x_axis_name_text_rotation:Number;
	public var x_axis_name_text_position:String;
	
	public var x_axis_smart_enabled:Boolean = false;
	public var y_axis_smart_enabled:Boolean = false;
	
	//workspace y axis name
	private var w_y_axis_name_text:String;
	private var w_y_axis_name_text_format:TextFormat;
	public var y_axis_name_text_rotation:Number = 0;
	public var y_axis_name_text_position:String;
	public var y_axis_name_text_direction:String = 'vertical';
	
	public var x_axis_name_text_direction:String = 'horizontal';
	
	//backgorund
	private var w_background_enabled:Boolean;
	private var w_background_color:Number;
	private var w_background_alpha:Number;	
	private var w_background_image_source:String;
	private var w_background_image_position:String;
	private var w_background_image_area_x:Number;
	private var w_background_image_area_y:Number;
	private var w_background_image_area_width:Number;
	private var w_background_image_area_height:Number;
	public var w_background_mc:MovieClip;
	
	public var background_type:String;
	public var background_gradient_type:String;
	public var background_gradient_colors:Array;
	public var background_gradient_alphas:Array;
	public var background_gradient_ratios:Array;
	public var background_gradient_matrix:Object;
	
	private var lib;
	private var clr_lib;
	private var w_c1:Number;
	private var w_c2:Number;
	
	public var chart_refresh_interval:Number;
	public var chart_refresh_source:String;
	public var chart_refresh_type:String;
	
	public var x_axis_dy:Number = 0;
	public var x_axis_dx:Number = 0;
	public var y_axis_dy:Number = 0;
	public var y_axis_dx:Number = 0;
	//-------------------------------------------
	//getters and setters
	//get x
	function get x():Number
	{
		return (w_x);
	}
	//get y
	function get y():Number
	{
		return (w_y);
	}
	//get width
	function get width():Number
	{
		return (w_chart_area_width);
	}
	//get height
	function get height():Number
	{
		return (w_chart_area_height);
	}
	//set x position
	function set x(new_x:Number):Void
	{
		w_x = new_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		w_y = new_y;
	}
	//set chart area width
	function set chart_area_width(w:Number):Void
	{
		if (w<=0)
		{
			_root.showError("Chart area width should be positive");
		}else
		{
			w_chart_area_width = w;
		}
	}
	function get chart_area_x():Number {
		var pos:Object = {x: chart_area_mc._x,y:0};
		workspace_mc.localToGlobal(pos);
		return pos.x;
	}
	function get chart_area_y():Number {
		var pos:Object = {y: chart_area_mc._y,x:0};
		workspace_mc.localToGlobal(pos);
		return pos.y;
	}
	function get chart_area_width():Number {
		return w_chart_area_width;
	}
	function get chart_area_height():Number {
		return w_chart_area_height;
	}
	//set chart area height
	function set chart_area_height(h:Number):Void
	{
		if (h<=0)
		{
			_root.showError("Chart height should be positive");
		}else
		{
			w_chart_area_height = h;
		}
	}
	//set chart area deep
	function set chart_area_deep(d:Number):Void
	{
		if (d<0)
		{
			_root.showError("Chart area deep sholud be positive");
		}else
		{
			w_chart_area_deep = d;
		}
	}
	//set base area height
	function set base_area_height(h:Number):Void
	{
		if (h<=0)
		{
			_root.showError("Base area height should be positive");
		}else
		{
			w_base_area_height = h;
		}
	}
	//set base area deep
	function set base_area_deep(d:Number):Void
	{
		if (d<0)
		{
			_root.showError("base area deep should be positive");
		}else
		{
			w_base_area_deep = d;
		}
	}
	//set rotation
	function set rotate_3d(r:Number):Void
	{
		if ((r>=180) or (r<=0))
		{
			_root.showError("Workspace 3d rotation should be between 0 and 180");
		}else
		{
			w_rotate_3d = r;
		}
	}
	//set chart area enabled
	function set chart_area_enabled(e:Boolean):Void
	{
		w_chart_area_enabled = e;
	}
	//set base area enabled
	function set base_area_enabled(e:Boolean):Void
	{
		w_base_area_enabled = e;
	}
	//set chart area border
	function set chart_area_border_enabled(e:Boolean):Void
	{
		w_chart_area_border_enabled = e;
	}
	//set chart area border size
	function set chart_area_border_size(s:Number):Void
	{
		if ((s<0) and (s>255))
		{
			_root.showError("Chart area border size should be between 0 and 255");
		}else
		{
			w_chart_area_border_size = s;
		}
	}
	//set chart area border alpha
	function set chart_area_border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("Chart area border alpha should be between 0 and 100");
		}else
		{
			w_chart_area_border_alpha = a;
		}
	}
	//set chart area border color
	function set chart_area_border_color(clr:Number):Void
	{
		if ((clr<0) and (clr>0xFFFFFF))
		{
			_root.showError("Chart area border color should be between 0 and 0xFFFFFF");
		}else
		{
			w_chart_area_border_color = clr;
		}
	}
	//set base area background enabled
	function set base_area_border_enabled(e:Boolean):Void
	{
		w_base_area_border_enabled = e;
	}
	//set base area border size
	function set base_area_border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("base area border size should be between 0 and 255");
		}else
		{
			w_base_area_border_size = s;
		}
	}
	//set base area border color
	function set base_area_border_color(clr:Number):Void
	{
		if ((clr<0) and (clr>0xFFFFFF))
		{
			_root.showError("base area border color should be between 0 and 0xFFFFFF");
		}else
		{
			w_base_area_border_color = clr;
		}
	}
	//set base area border alpha
	function set base_area_border_alpha(a:Number):Void
	{
		if ((a<0) and (a>100))
		{
			_root.showError("base area border alpha should be between 0 and 100");
		}else
		{
			w_base_area_border_alpha = a;
		}
	}
	//set chart area background enabled
	function set chart_area_background_enabled(e:Boolean):Void
	{
		w_chart_area_background_enabled = e;
	}
	//set chart area background color
	function set chart_area_background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("Chart area background color should be between 0 and 0xFFFFFF");
		}else
		{
			w_chart_area_background_color = clr;
		}
	}
	//set chart area background alpha
	function set chart_area_background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("Chart area background alpha should be between 0 and 100");
		}else
		{
			w_chart_area_background_alpha = a;
		}
	}
	//set base area background enabled
	function set base_area_background_enabled(e:Boolean):Void
	{
		w_base_area_background_enabled = e;
	}
	//set base area background color
	function set base_area_background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("base area background color should be between 0 and 0xFFFFFF");
		}else
		{
			w_base_area_background_color = clr;
		}
	}
	//set base area background alpha
	function set base_area_background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("base area background alpha should be between 0 and 100");
		}else
		{
			w_base_area_background_alpha = a;
		}
	}
	//set workspace name
	function set name(txt:String):Void
	{
		if (txt!='')
		{
			w_name_text = txt;
		}
	}
	//set workspace name text format
	function set name_text_format(tf:TextFormat):Void
	{
		w_name_text_format = tf;
	}
	//set workspace subname
	function set subname(txt:String):Void
	{
		if (txt!='')
		{
			w_subname_text = txt;
		}
	}
	//set workspace subname text format
	function set subname_text_format(tf:TextFormat):Void
	{
		w_subname_text_format = tf;
	}
	// set x axis name
	function set x_axis_name(txt:String):Void
	{
		w_x_axis_name_text = txt;
	}
	//set x axis name text format
	function set x_axis_name_text_format(tf:TextFormat):Void
	{
		w_x_axis_name_text_format = tf;
	}
	//set y axis name
	function set y_axis_name(txt:String):Void
	{
		w_y_axis_name_text = txt;
	}
	//set y axis name text format
	function set y_axis_name_text_format(tf:TextFormat):Void
	{
		w_y_axis_name_text_format = tf;
	}
	//set workspace background
	function set background_enabled(e:Boolean):Void
	{
		w_background_enabled = e;
	}
	function set background_color(clr:Number):Void
	{
		w_background_color = clr;
	}
	function set background_alpha(a:Number):Void
	{
		w_background_alpha = a;
	}
	function set background_image_source(s:String):Void
	{
		w_background_image_source = s;
	}
	function set background_image_position(pos:String):Void
	{
		w_background_image_position = pos;
	}
	function set background_image_x(x:Number):Void
	{
		w_background_image_area_x = x;
	}
	function set background_image_y(y:Number):Void
	{
		w_background_image_area_y = y;
	}
	function set background_image_width(w:Number):Void
	{
		w_background_image_area_width = w;
	}
	function set background_image_height(h:Number):Void
	{
		w_background_image_area_height = h;
	}
	function set chart_area_background_image_source(s:String):Void
	{
		w_chart_area_background_image_source = s;
	}
	function set chart_area_background_image_position(p:String):Void
	{
		w_chart_area_background_image_position = p;
	}
	
	private static var instance:workspace_;
	
	public static function getInstance() {
		return workspace_.instance;
	}
	
	//-------------------------------------------
	//constructor
	function workspace_(target_mc:MovieClip)
	{
		workspace_.instance = this;
		w_background_mc = target_mc.createEmptyMovieClip('workspace_background',DepthManager.WORKSPACE_BACKGROUND_DEPTH);
		if (target_mc['workspace_clip_']) {
			target_mc['workspace_clip_'].unloadMovie();
			target_mc['workspace_clip_'].removeMovieClip();			
		}
		workspace_mc = target_mc.createEmptyMovieClip('workspace_clip_',DepthManager.WORKSPACE_DEPTH);
		//functions library
		lib = new library.functions();
		clr_lib = new library.color_ext();
		//init defaults
		w_chart_area_enabled = true;
		w_base_area_enabled = true;
		w_x = 60;
		w_y = 10;
		w_chart_area_width = 480;
		w_chart_area_height = 300;
		w_chart_area_deep = 10;
		w_base_area_deep = 80;
		w_base_area_height = 20;
		w_chart_area_background_enabled = true;
		w_chart_area_background_alpha = 100;
		w_chart_area_background_color = 0xFFECAA;
		w_chart_area_border_enabled = false;
		w_chart_area_border_color = 0x000000;
		w_chart_area_border_size = 1;
		w_chart_area_border_alpha = 100;
		w_base_area_background_enabled = true;
		w_base_area_background_color = 0xFFC700;
		w_base_area_background_alpha = 100;
		w_base_area_border_enabled = false;
		w_base_area_border_size = 1;
		w_base_area_border_color = 0x000000;
		w_base_area_border_alpha = 100;
		w_rotate_3d = 45;
		w_c1 = 0.8;
		w_c2 = 0.6;
		w_name_text_format = new TextFormat();
		w_name_text_format.align = 'center';
		w_name_text_format.bold = true;
		w_name_text_format.font = 'Verdana';
		w_subname_text_format = new TextFormat();
		w_subname_text_format.align = 'center';
		w_subname_text_format.font = 'Verdana';
		w_x_axis_name_text_format = new TextFormat();
		w_x_axis_name_text_format.font = 'Verdana';
		w_x_axis_name_text_format.align = 'center';
		w_y_axis_name_text_format = new TextFormat();
		w_y_axis_name_text_format.font = 'Verdana';
		w_y_axis_name_text_format.align = 'center';
		w_background_enabled = false;
		w_background_alpha = 100;
		w_background_image_area_x = 0;
		w_background_image_area_y = 0;
		w_background_image_area_width = 550;
		w_background_image_area_height = 400;
		x_axis_name_text_rotation = 0;
		y_axis_name_text_rotation = 0;
	}
	
	private var objectsContainer:MovieClip;
	
	function getObjectsContainer():MovieClip {
		if (!objectsContainer) {
			objectsContainer = _root.createEmptyMovieClip('objectsContainer',DepthManager.OBJECTS_CONTAINER_DEPTH);
		}
		return objectsContainer;
	}
	
	//painting
	function paint():Void
	{
		show_background_();
		show_name();
		show_subname();
		workspace_mc._x = w_x;
		workspace_mc._y = w_y;
		paint_chart_area();
		paint_base_area();
		show_x_axis();
		show_y_axis();
		this.initRefresh();
	}
	
	function initContainerPosition() {
		container._x = w_x;
		container._y = w_y;
	}
	
	function remove():Void
	{
		removeLabels();
		objectsContainer.removeMovieClip();
		workspace_mc.removeMovieClip();
		chart_area_mc.removeMovieClip();
		base_area_mc.removeMovieClip();
	}
	
	function removeLabels():Void {
		container.removeMovieClip();
		container.unloadMovie();
	}
	//paint chart area
	private function paint_chart_area():Void
	{
		chart_area_mc = workspace_mc.createEmptyMovieClip('chart_area_movie',workspace_mc.getNextHighestDepth());
		if (w_chart_area_enabled)
		{			
			if (w_chart_area_border_enabled)
			{
				chart_area_mc.lineStyle(w_chart_area_border_size,w_chart_area_border_color,w_chart_area_border_alpha,false,"normal","square","miter");
			}
			chart_area_mc.moveTo(-w_chart_area_border_size/2,-w_chart_area_border_size/2);
			var dr:Number = (w_chart_area_border_size>=2.5) ? (w_chart_area_border_size/2) : (w_chart_area_border_size);
			if (w_chart_area_background_enabled)
			{
				if (chart_area_background_type == 'gradient')
				{					
					this.chart_area_gradient_matrix.matrixType = 'box';				
					if (isNaN(Number(this.chart_area_gradient_matrix.w)))
						this.chart_area_gradient_matrix.w = w_chart_area_width;
					if (isNaN(Number(this.chart_area_gradient_matrix.h)))
						this.chart_area_gradient_matrix.h = w_chart_area_height;
					if (isNaN(Number(this.chart_area_gradient_matrix.x)))
						this.chart_area_gradient_matrix.x = 0;
					if (isNaN(Number(chart_area_gradient_matrix.y)))
						this.chart_area_gradient_matrix.y = 0;

					chart_area_mc.beginGradientFill(this.chart_area_gradient_type,this.chart_area_gradient_colors,this.chart_area_gradient_alphas,this.chart_area_gradient_ratios,this.chart_area_gradient_matrix);					
				}else
				{
					chart_area_mc.beginFill(w_chart_area_background_color,w_chart_area_background_alpha);
				}
			}
			//paint main part
			chart_area_mc.lineTo(w_chart_area_width+dr,-w_chart_area_border_size/2);
			chart_area_mc.lineTo(w_chart_area_width+dr,w_chart_area_height+dr);
			chart_area_mc.lineTo(-w_chart_area_border_size/2,w_chart_area_height+dr);
			chart_area_mc.lineTo(-w_chart_area_border_size/2,-w_chart_area_border_size/2);
			//-
			if (w_chart_area_background_enabled)
			{
				chart_area_mc.endFill();
			}
			//paint up part
			if (w_chart_area_deep>0)
			{
				var dx:Number = w_chart_area_deep * Math.cos(lib.Grad2Rad(w_rotate_3d));
				var dy:Number = w_chart_area_deep * Math.sin(lib.Grad2Rad(w_rotate_3d));
				if (w_chart_area_background_enabled)
				{
					var clr:Number = clr_lib.transfer(w_chart_area_background_color,w_c1);
					chart_area_mc.beginFill(clr,w_chart_area_background_alpha);
				}
				chart_area_mc.moveTo(0,0);
				chart_area_mc.lineTo(dx,-dy);
				chart_area_mc.lineTo(w_chart_area_width+dx,-dy);
				chart_area_mc.lineTo(w_chart_area_width,0);
				chart_area_mc.lineTo(0,0);
				if (w_chart_area_background_enabled)
				{
					chart_area_mc.endFill();
					clr = clr_lib.transfer(w_chart_area_background_color,w_c2);
					chart_area_mc.beginFill(clr,w_chart_area_background_alpha);
				}
				//paint right part
				chart_area_mc.moveTo(w_chart_area_width,0);
				chart_area_mc.lineTo(w_chart_area_width+dx,-dy);
				chart_area_mc.lineTo(w_chart_area_width+dx,w_chart_area_height-dy);
				chart_area_mc.lineTo(w_chart_area_width,w_chart_area_height);
				chart_area_mc.lineTo(w_chart_area_width,0);
				if (w_chart_area_background_enabled)
				{
					chart_area_mc.endFill();
				}
				if (w_chart_area_background_image_source!=undefined)
				{
					var img = new objects.image(chart_area_mc);
					img.source = w_chart_area_background_image_source;
					img.position = w_chart_area_background_image_position;
					img.width = w_chart_area_width;
					img.height = w_chart_area_height;
					img.x = 0;
					img.y = 0;
					img.show();
				}
			}
		}
	}
	//paint base area
	private function paint_base_area():Void
	{
		if (w_base_area_enabled)
		{			
			base_area_mc = workspace_mc.createEmptyMovieClip('base_area_movie',workspace_mc.getNextHighestDepth());
			base_area_mc._y = w_chart_area_height;
			if (w_base_area_border_enabled)
			{
				base_area_mc.lineStyle(w_base_area_border_size,w_base_area_border_color,w_base_area_border_alpha);
			}
			if (w_base_area_background_enabled)
			{
				base_area_mc.beginFill(w_base_area_background_color,w_base_area_background_alpha);
			}
			//paint main part
			var dx:Number = w_base_area_deep * Math.cos(lib.Grad2Rad(w_rotate_3d));
			var dy:Number = w_base_area_deep * Math.sin(lib.Grad2Rad(w_rotate_3d));
			base_area_mc.moveTo(0,0);
			base_area_mc.lineTo(w_chart_area_width,0);
			base_area_mc.lineTo(w_chart_area_width-dx,dy);
			base_area_mc.lineTo(-dx,dy);
			base_area_mc.lineTo(0,0);
			//--
			if (w_base_area_background_enabled)
			{
				base_area_mc.endFill();
				var clr:Number = clr_lib.transfer(w_base_area_background_color,w_c2);
				base_area_mc.beginFill(clr,w_base_area_background_alpha);
			}
			//paint right part
			base_area_mc.moveTo(w_chart_area_width,0);
			base_area_mc.lineTo(w_chart_area_width,w_base_area_height);
			base_area_mc.lineTo(w_chart_area_width - dx,dy + w_base_area_height);
			base_area_mc.lineTo(w_chart_area_width - dx,dy);
			base_area_mc.lineTo(w_chart_area_width,0);
			//--
			if (w_base_area_background_enabled)
			{
				base_area_mc.endFill();
				clr = clr_lib.transfer(w_base_area_background_color,w_c1);
				base_area_mc.beginFill(clr,w_base_area_background_alpha);
			}
			//paint down part
			base_area_mc.moveTo(-dx,dy);
			base_area_mc.lineTo(w_chart_area_width-dx,dy);
			base_area_mc.lineTo(w_chart_area_width-dx,dy+w_base_area_height);
			base_area_mc.lineTo(-dx,dy+w_base_area_height);
			base_area_mc.lineTo(-dx,dy);
			if (w_base_area_background_enabled)
			{
				base_area_mc.endFill();
			}
			//--
		}
	}
	
	private var nameField:TextField;
	private var subnameField:TextField;
	
	//show name
	private function show_name():Void
	{
		if (w_name_text!=undefined)
		{
			var dy:Number = w_chart_area_deep * Math.sin(lib.Grad2Rad(w_rotate_3d));
			var tmp_obj:Object = new Object();
			tmp_obj = w_name_text_format.getTextExtent(w_name_text);			
			var h:Number = tmp_obj.textFieldHeight;
			var h_1:Number = 0;
			var y:Number = -h-dy;
			if (w_subname_text!=undefined)
			{
				tmp_obj = w_subname_text_format.getTextExtent(w_subname_text);			
				h_1 = tmp_obj.textFieldHeight;
				y -= h_1
			}
			nameField = container.createTextField("workspace_name_text_field",container.getNextHighestDepth(),0,y,w_chart_area_width,h);
			nameField.selectable = false;
			nameField.text = w_name_text;
			nameField.setTextFormat(w_name_text_format);
			w_y += h;
			w_chart_area_height -= h;
		}
	}
	//show subname
	private function show_subname():Void
	{
		if (w_subname_text!=undefined)
		{
			var dy:Number = w_chart_area_deep * Math.sin(lib.Grad2Rad(w_rotate_3d));
			var tmp_obj:Object = new Object();
			tmp_obj = w_subname_text_format.getTextExtent(w_subname_text);			
			var h:Number = tmp_obj.textFieldHeight;
			subnameField = workspace_mc.createTextField("workspace_subname_text_field",workspace_mc.getNextHighestDepth(),0,-h-dy,w_chart_area_width,h);
			subnameField.selectable = false;
			subnameField.text = w_subname_text;
			subnameField.setTextFormat(w_subname_text_format);
			w_y += h;
			w_chart_area_height -= h;
		}
	}
	
	public var x_axis_name_field:TextField;
	
	//x axis grid name
	private function show_x_axis():Void
	{
		initContainerPosition();
		if (w_x_axis_name_text!=undefined)
		{
			var tmp_obj:Object = new Object();
			tmp_obj = w_x_axis_name_text_format.getTextExtent(w_x_axis_name_text);			
			var h:Number = tmp_obj.textFieldHeight;
			container.createTextField("x_axis_name_text_field",workspace_mc.getNextHighestDepth(),0,w_chart_area_height,w_chart_area_width,h);
			this.x_axis_name_field = container['x_axis_name_text_field'];
			x_axis_name_field.selectable = false;
			x_axis_name_field.text = w_x_axis_name_text;			
			x_axis_name_field.multiline = true;
			x_axis_name_field.setTextFormat(w_x_axis_name_text_format);
			if (x_axis_name_text_direction == 'vertical') {
				x_axis_name_field.text = '';
				for (var i:Number = 0;i<w_x_axis_name_text.length;i++) {
					x_axis_name_field.text += w_x_axis_name_text.charAt(i)+"\n";
				}
				x_axis_name_field.text = x_axis_name_field.text.substr(0,x_axis_name_field.text.length - 1);
			}
			if (x_axis_name_text_rotation!=0)
			{
				x_axis_name_field.embedFonts = true;
				var tf:TextFormat = _global[w_x_axis_name_text_format.font];
				tf.size = w_x_axis_name_text_format.size;
				tf.color = w_x_axis_name_text_format.color;
				x_axis_name_field.setTextFormat(tf);
				x_axis_name_field._rotation = x_axis_name_text_rotation;
			}else {
				var tf:TextFormat = _global[w_x_axis_name_text_format.font];
				tf.size = w_x_axis_name_text_format.size;
				tf.color = w_x_axis_name_text_format.color;
				x_axis_name_field.setTextFormat(tf);
			}
			x_axis_name_field._y = w_chart_area_height;
			x_axis_name_field.autoSize = true;

			var bounds:Object = library.functions.getTextFieldBoundBox(x_axis_name_field);
			var w:Number = bounds.right - bounds.left;
			var h:Number = bounds.bottom - bounds.top;
			var pos:Object = {};			
			switch(x_axis_name_text_position) {
				case 'left_top':
					pos.x = 0;
					pos.y = - h - 5;
					break;
				case 'center_top':
					pos.x = (w_chart_area_width - w)/2;
					pos.y = - h - 5;
					break;
				case 'right_top':
					pos.x = w_chart_area_width  - w;
					pos.y = - h - 5;
					break;
				case 'left_bottom':
					pos.x = 0;
					pos.y = w_chart_area_height + 5;
					break;
				case 'center_bottom':
					pos.x = (w_chart_area_width - w)/2;
					pos.y = w_chart_area_height + 5;
					break;
				case 'right_bottom':
					pos.x = w_chart_area_width  - w;
					pos.y = w_chart_area_height + 5;
					break;				
			}
			workspace_mc.localToGlobal(pos);
			container.globalToLocal(pos);
			library.functions.changeTextFieldX(x_axis_name_field, this.x_axis_dx + pos.x);
			library.functions.changeTextFieldY(x_axis_name_field, this.x_axis_dy + pos.y);
		}
		
	}
	
	public var y_axis_name_field:TextField;
	
	//y axis grid name
	private function show_y_axis():Void
	{
		if (w_y_axis_name_text!=undefined)
		{
			var tmp_obj:Object = new Object();
			tmp_obj = w_y_axis_name_text_format.getTextExtent('W');			
			var w:Number = tmp_obj.textFieldWidth;
			workspace_mc.createTextField("y_axis_name_text_field",workspace_mc.getNextHighestDepth(),-w,0,0,0);
			y_axis_name_field = workspace_mc["y_axis_name_text_field"];
			y_axis_name_field.autoSize = true;
			y_axis_name_field.selectable = false;
			y_axis_name_field.html = true;
			y_axis_name_field.multiline = true;
			var i:Number;
			var txt:String = '';
			if (y_axis_name_text_direction == 'vertical') {
				for (i=0;i<w_y_axis_name_text.length;i++)
				{
					txt += w_y_axis_name_text.charAt(i)+'<br>';
				}
			}else {
				txt = w_y_axis_name_text;
			}
			y_axis_name_field.htmlText = txt;
			y_axis_name_field.setTextFormat(w_y_axis_name_text_format);
			if (y_axis_name_text_rotation!=0)
			{
				y_axis_name_field.html = false;
				y_axis_name_field.embedFonts = true;
				var tf:TextFormat = _global[w_y_axis_name_text_format.font];
				tf.size = w_y_axis_name_text_format.size;
				tf.color = w_y_axis_name_text_format.color;
				y_axis_name_field.multiline = true;
				if (y_axis_name_text_direction == 'vertical'){
					y_axis_name_field.text = '';
					for (var i:Number = 0;i<w_y_axis_name_text.length;i++) {
						y_axis_name_field.text += w_y_axis_name_text.charAt(i) + "\n";
					}
					y_axis_name_field.text = y_axis_name_field.text.substr(0,y_axis_name_field.text.length-1);
				}else
					y_axis_name_field.text = w_y_axis_name_text;
				y_axis_name_field.setTextFormat(tf);
				y_axis_name_field._rotation = y_axis_name_text_rotation;				
			}else {
				if (w_y_axis_name_text_format.font == 'verdana_embed_tf') {
					var tf:TextFormat = _global[w_y_axis_name_text_format.font];
					tf.size = w_y_axis_name_text_format.size;
					tf.color = w_y_axis_name_text_format.color;
					y_axis_name_field.setTextFormat(tf);
				}
			}
			var bounds:Object = library.functions.getTextFieldBoundBox(y_axis_name_field);
			var pos:Object = {};
			pos.y = (w_chart_area_height - bounds.bottom + bounds.top)/2;
			switch (this.y_axis_name_text_position) {
				case 'left_top':
					pos.x = - (bounds.right - bounds.left) - 5;
					pos.y = 0;
					break;
				case 'left_center':
					pos.x = - (bounds.right - bounds.left) - 5;
					pos.y = (w_chart_area_height - bounds.bottom + bounds.top)/2;
					break;
				case 'left_bottom':
					pos.x = - (bounds.right - bounds.left) - 5;
					pos.y = (w_chart_area_height - bounds.bottom + bounds.top);
					break;
				case 'right_top':
					pos.x = w_chart_area_width + 5;
					pos.y = 0;
					break;
				case 'right_center':
					pos.x = w_chart_area_width + 5;
					pos.y = (w_chart_area_height - bounds.bottom + bounds.top)/2;
					break;
				case 'right_bottom':
					pos.x = w_chart_area_width + 5;
					pos.y = (w_chart_area_height - bounds.bottom + bounds.top);
					break;
			}
			library.functions.changeTextFieldX(y_axis_name_field,pos.x + y_axis_dx);
			library.functions.changeTextFieldY(y_axis_name_field,pos.y + y_axis_dy);
		}
	}
	private function show_background_():Void
	{
		if (w_background_enabled)
		{
			w_background_mc._x = w_background_image_area_x;
			w_background_mc._y = w_background_image_area_y;
			if (this.background_type == 'gradient')
			{
				Stage.scaleMode = "noScale";
				this.background_gradient_matrix.matrixType = 'box';
				if (isNaN(Number(this.background_gradient_matrix.w)))
					this.background_gradient_matrix.w = Stage.width;
				if (isNaN(Number(this.background_gradient_matrix.h)))
					this.background_gradient_matrix.h = Stage.height;
				if (isNaN(Number(this.background_gradient_matrix.x)))
					this.background_gradient_matrix.x = 0;
				if (isNaN(Number(this.background_gradient_matrix.y)))
					this.background_gradient_matrix.y = 0;
					
				w_background_image_area_width = Stage.width;
				w_background_image_area_height = Stage.height;
				w_background_mc.beginGradientFill(this.background_gradient_type,this.background_gradient_colors,this.background_gradient_alphas,this.background_gradient_ratios,this.background_gradient_matrix);
			}else
			{
				w_background_mc.beginFill(w_background_color,w_background_alpha);
			}
			w_background_mc.moveTo(0,0);
			w_background_mc.lineTo(w_background_image_area_width,0);
			w_background_mc.lineTo(w_background_image_area_width,w_background_image_area_height);
			w_background_mc.lineTo(0,w_background_image_area_height);
			w_background_mc.lineTo(0,0);
			w_background_mc.endFill();
			if (w_background_image_source!=undefined)
			{
				var img = new objects.image(w_background_mc);
				img.source = w_background_image_source;
				img.x = 0;
				img.y = 0;
				img.width = w_background_image_area_width;
				img.height = w_background_image_area_height;
				img.position = w_background_image_position;
				img.show();
			}
		}
	}	
	
	private var refreshInterval:Number;
	
	private function initRefresh():Void
	{
		if (this.chart_refresh_interval)
		{
			_root.doRefresh = function() {
				clearInterval(_root.refreshInterval);
				//remove items:		
				_root.gotoAndPlay(1);
			}
			_root.refreshInterval = setInterval(_root.doRefresh,this.chart_refresh_interval);
		}
	}
	
	private var blockNamesTextFields:Array = new Array();
	private var topBlockNamesTextFields:Array = new Array();
	private var bottomBlockNamesTextFields:Array = new Array();
	private var leftBlockNamesTextFields:Array = new Array();
	private var rightBlockNamesTextFields:Array = new Array();
	
	public function addBlockNameTextField(position:String, placement:String, text:String, format:TextFormat, x:Number, y:Number, dx:Number, dy:Number, mdx:Number, mdy:Number, rotation:Number):Void {
		initContainerPosition();		
		
		var pos:Object = {x:x,y:y};
		container.globalToLocal(pos);
		
		container.moveTo(pos.x,0);
		container.lineTo(pos.x,pos.y);
		
		var id:Number = container.getNextHighestDepth();
		
		this.container.createTextField('blockName_'+id,this.container.getNextHighestDepth(),0,0,0,0);
		var field:TextField = container['blockName_'+id];
		field.autoSize = true;
		field.text = text;		
		field.selectable = false;		
		
		var fieldBounds:Object = library.functions.getTextFieldBoundBox(field);		
		var x1:Number = (fieldBounds.right - fieldBounds.left)*mdx + pos.x;
		var y1:Number = (fieldBounds.bottom - fieldBounds.top)*mdy + pos.y;
		
		if (rotation != 0) {
			var f:TextFormat = _global[format.font];
			f.size = format.size;
			f.color = format.color;
			field.setTextFormat(f);
			field.embedFonts = true;
		}else {			
			field.setTextFormat(format);
		}
		fieldBounds = library.functions.getTextFieldBoundBox(field);
		field._x = (fieldBounds.right - fieldBounds.left)*mdx + pos.x + dx;
		field._y = (fieldBounds.bottom - fieldBounds.top)*mdy + pos.y + dy;
		if (mdx == -1/2) {
			if (rotation != 0) {
				mdx = 0;
				field._x = pos.x + dx;			
			}
		}
		if (mdy == -1/2 || mdy == 1/2) {
			if (rotation != 0) {
				mdy = 0;
				field._y = pos.y + dy;
			}
		}
		library.functions.rotateField(field,rotation);
		field.offset = {dx:dx, dy:dy};		
		
		if (placement != 'chart') {
			blockNamesTextFields.push(field);
		}else {
			switch (position) {
				case 'left':
					leftBlockNamesTextFields.push(field);
					break;
				case 'right':
					rightBlockNamesTextFields.push(field);
					break;
				case 'top':
					topBlockNamesTextFields.push(field);
					break;
				case 'bottom':
					bottomBlockNamesTextFields.push(field);
					break;
			}
		}
		checkVerticalPositions();
		checkHorizontalPositions();
	}
	
	private var namesTextFields:Array;
	
	public function addSetNameTextField(position:String, ftext:String, format:TextFormat,x:Number,y:Number,rotation:Number,dx:Number,dy:Number,mdx:Number,mdy:Number):Void {
		var isHorizontal:Boolean = arguments[10] != undefined ? Boolean(arguments[10]) : false;
		var isChartAnchor:Boolean = arguments[11] != undefined ? Boolean(arguments[11]) : false;

		if (namesTextFields == undefined)
			namesTextFields = new Array();

		var id:Number = container.getNextHighestDepth();
		initContainerPosition();
		var pos:Object = {x:x,y:y};
		container.globalToLocal(pos);
		
/*		if (!isHorizontal && rotation != 0) {
			mdx = 0;
		}else if (isHorizontal && rotation != 0) {
			mdy = 0;
		}*/

		this.container.createTextField('setNameField_'+id,this.container.getNextHighestDepth(),0,0,0,0);
		var field:TextField = container['setNameField_'+id];
		if (rotation != 0) {
			field.embedFonts = true;
		}
		field.isChartAnchor = isChartAnchor;
		field.autoSize = true;
		field.selectable = false;
		field.text = ftext;
		field.position = position;
		field.setTextFormat(format);
		field.selectable = false;
		
		field.offset = {dx:dx, dy:dy};
		var fieldBounds:Object = library.functions.getTextFieldBoundBox(field);
		
		fieldBounds = library.functions.getTextFieldBoundBox(field);
		field._x = (fieldBounds.right - fieldBounds.left)*mdx + pos.x + dx;
		field._y = (fieldBounds.bottom - fieldBounds.top)*mdy + pos.y + dy;
		if (mdx == -1/2) {
			if (rotation != 0) {
				if (mdy == -1) {
					pos.y += field._height;
					field._y += field._height;
				}
				mdx = 0;
				field._x = pos.x + dx;			
			}
		}
		if (mdy == -1/2 || mdy == 1/2) {
			if (rotation != 0) {
				mdy = 0;
				field._y = pos.y + dy;
			}
		}
		library.functions.rotateField(field,rotation);
		namesTextFields.push(field);
		
		if (isHorizontal)
			checkHorizontalPositions();
		else
			checkVerticalPositions();

	}
	
	private var valuesTextFields:Array;
	
	public var container:MovieClip;
	
	public function initContainer() {
/*		if (_root['containerForLabels']) {
			_root['containerForLabels'].unloadMovie();
			_root['containerForLabels'].removeMovieClip();
		}
		if (container) {
			container.removeMovieClip();
			container.unloadMovie();
		}*/
		blockNamesTextFields = new Array();
		topBlockNamesTextFields = new Array();
		bottomBlockNamesTextFields = new Array();
		leftBlockNamesTextFields = new Array();
		rightBlockNamesTextFields = new Array();
		container = _root.createEmptyMovieClip('containerForLabels',DepthManager.LABELS_CONTAINER_DEPTH);
		initContainerPosition();
	}
	
	public function addSetValueTextField(position:String, ftext:String, format:TextFormat,x:Number,y:Number,rotation:Number,dx:Number,dy:Number,mdx:Number,mdy:Number):Void {

		var isHorizontal:Boolean = arguments[10] != undefined ? Boolean(arguments[10]) : false;
		
		if (valuesTextFields == undefined)
			valuesTextFields = new Array();
			
		initContainerPosition();
		var pos:Object ={x:x, y:y};
		container.globalToLocal(pos);
		
		if (!isHorizontal && rotation != 0) {
			mdx = -0.5;
		}else if (isHorizontal && rotation != 0) {
			mdy = 0;
		}
		
		var id:Number = container.getNextHighestDepth();
				
		this.container.createTextField('setValueField_'+id,id,0,0,0,0);
		var field:TextField = container['setValueField_'+id];
		
		if (rotation != 0)
			field.embedFonts = true;
			
		field.offset = {dx:dx, dy:dy};
		field.autoSize = true;
		field.selectable = false;
		field.text = ftext;
		field.setTextFormat(format);
		field.position = position;
		field.selectable = false;

		var fieldBounds:Object = library.functions.getTextFieldBoundBox(field);
		
		fieldBounds = library.functions.getTextFieldBoundBox(field);
		field._x = (fieldBounds.right - fieldBounds.left)*mdx + pos.x + dx;
		field._y = (fieldBounds.bottom - fieldBounds.top)*mdy + pos.y + dy;
		var autoPlace:Boolean = false;
		if (mdx == -1/2) {
			if (rotation != 0) {

				mdx = 0;
				if (mdy == -1) {
					pos.y += field._height;
					field._y += field._height;
				}
				field._x = pos.x + dx;
				autoPlace = true;
			}
		}
		if (mdy == -1/2 || mdy == 1/2) {
			if (rotation != 0) {
				mdy = 0;
				field._y = pos.y + dy;
				autoPlace = true;
			}
		}
		library.functions.rotateField(field,rotation);
		if (!autoPlace) {
			field._x = (fieldBounds.right - fieldBounds.left)*mdx + pos.x + dx;
			field._y = (fieldBounds.bottom - fieldBounds.top)*mdy + pos.y + dy;
		}


		valuesTextFields.push(field);
		if (isHorizontal)
			checkHorizontalPositions();
		else
			checkVerticalPositions();
	}
	
	private var xAxisCaptionsTextFields:Array;
	
	public function addXAxisGridCaption(text:String, format:TextFormat,x:Number,y:Number,rotation:Number,dx:Number,dy:Number,mdx:Number,mdy:Number):Void {
		if (xAxisCaptionsTextFields == undefined)
			xAxisCaptionsTextFields = new Array();
			
		initContainerPosition();
		var id:Number = container.getNextHighestDepth();
		var pos:Object ={x:x, y:y};
		container.globalToLocal(pos);
		
		this.container.createTextField('axisCaptionsTextFields_'+id,id,0,0,0,0);
		var field:TextField = container['axisCaptionsTextFields_'+id];
		var fformat:TextFormat;
		if (rotation != 0) {
			field.embedFonts = true;
			fformat = _global[format.font];
			fformat.size = format.size;
			fformat.color = format.color;
		}else {
			fformat = format;
		}
			
		field.offset = {dx:dx, dy:dy};
		field.autoSize = true;
		field.selectable = false;
		field.text = text;
		field.setTextFormat(fformat);
		field.selectable = false;
						
		var fieldBounds:Object = library.functions.getTextFieldBoundBox(field);
		field._x = (fieldBounds.right - fieldBounds.left)*mdx + pos.x + dx;
		field._y = (fieldBounds.bottom - fieldBounds.top)*mdy + pos.y + dy;
		if (mdx == -1/2) {
			if (rotation != 0) {
				mdx = 0;
				field._x = pos.x + dx;			
			}
		}
		if (mdy == -1/2 || mdy == 1/2) {
			if (rotation != 0) {
				mdy = 0;
				field._y = pos.y + dy;
			}
		}

		library.functions.rotateField(field,rotation);
		
		xAxisCaptionsTextFields.push(field);
		checkHorizontalPositions();
		checkVerticalPositions();
	}
	
	private var yAxisCaptionsTextFields:Array;
	
	public function addYAxisGridCaption(text:String, format:TextFormat,x:Number,y:Number,rotation:Number,dx:Number,dy:Number,mdx:Number,mdy:Number):Void {
		if (yAxisCaptionsTextFields == undefined)
			yAxisCaptionsTextFields = new Array();
		
		initContainerPosition();
		var id:Number = container.getNextHighestDepth();
		var pos:Object ={x:x, y:y};
		container.globalToLocal(pos);
	
		this.container.createTextField('axisCaptionsTextFields_'+id,id,0,0,0,0);
		var field:TextField = container['axisCaptionsTextFields_'+id];
		if (rotation != 0)
			field.embedFonts = true;
		
	
		field.offset = {dx:dx, dy:dy};
		field.autoSize = true;
		field.selectable = false;
		field.text = text;
		field.setTextFormat(format);
		field.selectable = false;
		

		var fieldBounds:Object = library.functions.getTextFieldBoundBox(field);
		field._x = (fieldBounds.right - fieldBounds.left)*mdx + pos.x + dx;
		field._y = (fieldBounds.bottom - fieldBounds.top)*mdy + pos.y + dy;
		if (mdx == -1/2) {
			if (rotation != 0) {
				mdx = 0;
				field._x = pos.x + dx;			
			}
		}
		if (mdy == -1/2 || mdy == 1/2) {
			if (rotation != 0) {
				mdy = 0;
				field._y = pos.y + dy;
			}
		}

		library.functions.rotateField(field,rotation);
		
		yAxisCaptionsTextFields.push(field);
		checkHorizontalPositions();
		checkVerticalPositions();
	}
	
	public var horizontalGridLabelsPosition:String;
	public var verticalGridLabelsPosition:String;
	
	public var extHorizontalGridLabelsPosition:String;
	public var extVerticalGridLabelsPosition:String;
	
	private var extYAxisCaptionsTextFields:Array;
	
	private var extXAxisCaptionsTextFields:Array;
	
	
	public function addExtYAxisGridCaption(text:String, format:TextFormat,x:Number,y:Number,rotation:Number,dx:Number,dy:Number,mdx:Number,mdy:Number):Void {
		if (extYAxisCaptionsTextFields == undefined)
			extYAxisCaptionsTextFields = new Array();
		initContainerPosition();
		var id:Number = container.getNextHighestDepth();
		var pos:Object ={x:x, y:y};
		container.globalToLocal(pos);
				
		this.container.createTextField('extaxisCaptionsTextFields_'+id,this.container.getNextHighestDepth(),0,0,0,0);
		var field:TextField = container['extaxisCaptionsTextFields_'+id];
		if (rotation != 0)
			field.embedFonts = true;
			
		field.offset = {dx:dx, dy:dy};
		field.autoSize = true;
		field.selectable = false;
		field._rotation = rotation;
		field.text = text;
		field.setTextFormat(format);
		field.selectable = false;
		
		var box:Object = library.functions.getTextFieldBoundBox(field);		
		field._x = pos.x + mdx*(box.right-box.left);
		field._y = pos.y + mdy*(box.right-box.left);
		
		
		extYAxisCaptionsTextFields.push(field);
		checkHorizontalPositions();
		checkVerticalPositions();
	}
	
	
	
	public function addExtXAxisGridCaption(text:String, format:TextFormat,x:Number,y:Number,rotation:Number,dx:Number,dy:Number,mdx:Number,mdy:Number):Void {
		if (extXAxisCaptionsTextFields == undefined)
			extXAxisCaptionsTextFields = new Array();
		
		if (rotation != 0)
			mdy = 0;
			
		var id:Number = container.getNextHighestDepth();
		var pos:Object ={x:x, y:y};
		initContainerPosition();
		container.globalToLocal(pos);
				
		
		this.container.createTextField('axisCaptionsTextFields_'+id,this.container.getNextHighestDepth(),0,0,0,0);
		var field:TextField = container['axisCaptionsTextFields_'+id];
		if (rotation != 0)
			field.embedFonts = true;
			
		field.offset = {dx:dx, dy:dy};
		field.autoSize = true;
		field.selectable = false;
		field._rotation = rotation;
		field.text = text;
		field.setTextFormat(format);
		field.selectable = false;

		var box:Object = library.functions.getTextFieldBoundBox(field);
		field._x = pos.x + mdx*(box.right-box.left);
		field._y = pos.y + mdy*(box.right-box.left);
		
		var box:Object = library.functions.getTextFieldBoundBox(field);
		library.functions.changeTextFieldX(field,pos.x + (box.right - box.left)*mdx + dx);
		library.functions.changeTextFieldY(field,pos.y + (box.bottom - box.top)*mdy + dy);
		
		extXAxisCaptionsTextFields.push(field);
		checkHorizontalPositions();
		checkVerticalPositions();
	}
	
	private function checkHorizontalPositions():Void {
		
		initContainerPosition();		
		//1. grid labels
		//2. set names/values
		//3. block names
		//4. axis name
		
		
		//check left
		
		var lXPos:Number = -5;
		var rXPos:Number = w_chart_area_width + 5 + _xDeepOffset;

		var newLXPos:Number = -5;
		var newRXPos:Number = rXPos;
		if (horizontalGridLabelsPosition == 'right') {
			for (var i:Number = 0;i < xAxisCaptionsTextFields.length; i++) {
				var box:Object = library.functions.getTextFieldBoundBox(xAxisCaptionsTextFields[i]);
				var w:Number = box.right - box.left;
				library.functions.changeTextFieldX(xAxisCaptionsTextFields[i], rXPos+xAxisCaptionsTextFields[i].offset.dx);
				if (rXPos + w + xAxisCaptionsTextFields[i].offset.dx> newRXPos)
					newRXPos = rXPos + w + xAxisCaptionsTextFields[i].offset.dx;
			}
		}else {
			for (var i:Number = 0;i < xAxisCaptionsTextFields.length; i++) {
				var box:Object = library.functions.getTextFieldBoundBox(xAxisCaptionsTextFields[i]);
				var w:Number = box.right - box.left;
				if ((-w + xAxisCaptionsTextFields[i].offset.dx)< newLXPos)
					newLXPos = lXPos-w;
				library.functions.changeTextFieldX(xAxisCaptionsTextFields[i], lXPos - w + xAxisCaptionsTextFields[i].offset.dx);
			}
		}
			
		lXPos = newLXPos;
		rXPos = newRXPos;

		if (extHorizontalGridLabelsPosition != undefined) {
			if (extHorizontalGridLabelsPosition == 'right') {
				for (var i:Number = 0;i<extXAxisCaptionsTextFields.length;i++) {
					var box:Object = library.functions.getTextFieldBoundBox(extXAxisCaptionsTextFields[i]);
					var w:Number = box.right - box.left;
					library.functions.changeTextFieldX(xAxisCaptionsTextFields[i], rXPos+extXAxisCaptionsTextFields[i].offset.dx);
					if (rXPos + w + extXAxisCaptionsTextFields[i].offset.dx> newRXPos)
						newRXPos = rXPos + w + xAxisCaptionsTextFields[i].offset.dx;
				}
			}else {
				for (var i:Number = 0;i < extXAxisCaptionsTextFields.length; i++) {
					var box:Object = library.functions.getTextFieldBoundBox(extXAxisCaptionsTextFields[i]);
					var w:Number = box.right - box.left;
					if ((lXPos -w + extXAxisCaptionsTextFields[i].offset.dx)< newLXPos)
						newLXPos = lXPos-w + extXAxisCaptionsTextFields[i].offset.dx;
					library.functions.changeTextFieldX(extXAxisCaptionsTextFields[i], lXPos - w + extXAxisCaptionsTextFields[i].offset.dx);
				}
			}
		}
		
		lXPos = newLXPos;
		rXPos = newRXPos;

		
		for (var i:Number = 0;i<namesTextFields.length;i++) {
			if (namesTextFields[i].isChartAnchor == false)
				continue;
				
			if (namesTextFields[i].position == 'left') {
				var box:Object = library.functions.getTextFieldBoundBox(namesTextFields[i]);
				if (box.left > 0)
					continue;
				var w:Number = box.right - box.left;
				library.functions.changeTextFieldX(namesTextFields[i],lXPos - w + namesTextFields[i].offset.dx);
				if (lXPos - w  + namesTextFields[i].offset.dx< newLXPos)
					newLXPos = lXPos - w + namesTextFields[i].offset.dx;
			}else if (namesTextFields[i].position == 'right') {
				var box:Object = library.functions.getTextFieldBoundBox(namesTextFields[i]);
				if (box.right < w_chart_area_width)
					continue;
				var w:Number = box.right - box.left;
				library.functions.changeTextFieldX(namesTextFields[i],rXPos + namesTextFields[i].offset.dx);
				if (newRXPos >= rXPos + w + namesTextFields[i].offset.dx)
					newRXPos = rXPos + w + namesTextFields[i].offset.dx;
			}
		}
		
		rXPos = newRXPos;
		lXPos = newLXPos;
		
		/*
		for (var i:Number = 0;i<valuesTextFields.length;i++) {
			if (valuesTextFields[i].position == 'left') {
				var box:Object = library.functions.getTextFieldBoundBox(valuesTextFields[i]);
				if (box.left > 0)
					continue;
				var w:Number = box.right - box.left;
				if (horizontalGridLabelsPosition == 'left') {					
					library.functions.changeTextFieldX(valuesTextFields[i],lXPos - w + valuesTextFields[i].offset.dx);
					if (lXPos - w  + valuesTextFields[i].offset.dx< newLXPos)
						newLXPos = lXPos - w + valuesTextFields[i].offset.dx;
				}
			}else if (valuesTextFields[i].position == 'right') {
				var box:Object = library.functions.getTextFieldBoundBox(valuesTextFields[i]);
				if (box.left > 0)
					continue;
				var w:Number = box.right - box.left;
				if (horizontalGridLabelsPosition == 'right') {
					var box:Object = library.functions.getTextFieldBoundBox(valuesTextFields[i]);
					if (box.right < w_chart_area_width)
						continue;
					library.functions.changeTextFieldX(valuesTextFields[i],rXPos + valuesTextFields[i].offset.dx);
					if (newRXPos >= rXPos + w + valuesTextFields[i].offset.dx)
						newRXPos = rXPos + w + valuesTextFields[i].offset.dx;
				}
			}
		} */
		
		rXPos = newRXPos;
		lXPos = newLXPos;
		
		//check left block names
		for (var i:Number = 0;i<leftBlockNamesTextFields.length;i++) {
			if (leftBlockNamesTextFields[i] == undefined)
				continue;
				
			var box:Object = library.functions.getTextFieldBoundBox(leftBlockNamesTextFields[i]);
			var w:Number = box.right - box.left;
			library.functions.changeTextFieldX(leftBlockNamesTextFields[i],lXPos - w + leftBlockNamesTextFields[i].offset.dx);
			if (newLXPos > lXPos - w + leftBlockNamesTextFields[i].offset.dx)
				newLXPos = lXPos - w + leftBlockNamesTextFields[i].offset.dx;
		}
		
		lXPos = newLXPos;
		
		//check right block names
		for (var i:Number = 0;i<rightBlockNamesTextFields.length;i++) {
			if (rightBlockNamesTextFields[i] == undefined)
				continue;
				
			var box:Object = library.functions.getTextFieldBoundBox(rightBlockNamesTextFields[i]);
			var w:Number = box.right - box.left;
			library.functions.changeTextFieldX(rightBlockNamesTextFields[i],rXPos + rightBlockNamesTextFields[i].offset.dx);
			if (newRXPos > rXPos + w + rightBlockNamesTextFields[i].offset.dx)
				newRXPos = rXPos + w + rightBlockNamesTextFields[i].offset.dx;
		}
		
		rXPos = newRXPos;
		
		if (y_axis_name_text_position.indexOf('left') !== -1) {
			var box:Object = library.functions.getTextFieldBoundBox(y_axis_name_field);
			var w:Number = box.right - box.left;
			library.functions.changeTextFieldX(y_axis_name_field,lXPos - w + y_axis_dx);
		}else {
			library.functions.changeTextFieldX(y_axis_name_field,rXPos + y_axis_dx);
		}
	}
	
	private function checkVerticalPositions():Void {

		initContainerPosition();		
		//1. grid labels
		//2. set names/values
		//3. block names
		//4. axis name
		var tYPos:Number = -5 + _yDeepOffset;
		var bYPos:Number = w_chart_area_height+5;
		
		var newTYPos:Number = tYPos;
		var newBYPos:Number = w_chart_area_height;

		if (verticalGridLabelsPosition == 'top') {
			for (var i:Number = 0;i < yAxisCaptionsTextFields.length; i++) {
				var box:Object = library.functions.getTextFieldBoundBox(yAxisCaptionsTextFields[i]);
				var h:Number = box.bottom - box.top;				
				library.functions.changeTextFieldY(yAxisCaptionsTextFields[i], tYPos - h + yAxisCaptionsTextFields[i].offset.dy);
				if (tYPos - h + yAxisCaptionsTextFields[i].offset.dy< newTYPos)
					newTYPos = tYPos - h + yAxisCaptionsTextFields[i].offset.dy;
			}
		}else {
			for (var i:Number = 0;i < yAxisCaptionsTextFields.length; i++) {
				var box:Object = library.functions.getTextFieldBoundBox(yAxisCaptionsTextFields[i]);
				var h:Number = box.bottom - box.top;
				if ((bYPos + h + yAxisCaptionsTextFields[i].offset.dy)> newBYPos)
					newBYPos = bYPos + h + yAxisCaptionsTextFields[i].offset.dy;
				library.functions.changeTextFieldY(yAxisCaptionsTextFields[i], bYPos + yAxisCaptionsTextFields[i].offset.dy);
			}
		}

		tYPos = newTYPos;
		bYPos = newBYPos;
		
		if (extVerticalGridLabelsPosition != undefined) {
			if (extVerticalGridLabelsPosition == 'top') {
				for (var i:Number = 0;i < extYAxisCaptionsTextFields.length; i++) {
					var box:Object = library.functions.getTextFieldBoundBox(extYAxisCaptionsTextFields[i]);
					var h:Number = box.bottom - box.top;				
					library.functions.changeTextFieldY(extYAxisCaptionsTextFields[i], tYPos - h + extYAxisCaptionsTextFields[i].offset.dy);
					if (tYPos - h + extYAxisCaptionsTextFields[i].offset.dy< newTYPos)
						newTYPos = tYPos - h + extYAxisCaptionsTextFields[i].offset.dy;
				}
			}else {
				for (var i:Number = 0;i < extYAxisCaptionsTextFields.length; i++) {
					var box:Object = library.functions.getTextFieldBoundBox(extYAxisCaptionsTextFields[i]);
					var h:Number = box.bottom - box.top;
					if ((bYPos + h + extYAxisCaptionsTextFields[i].offset.dy)> newBYPos)
						newBYPos = bYPos + h + extYAxisCaptionsTextFields[i].offset.dy;
					library.functions.changeTextFieldY(extYAxisCaptionsTextFields[i], bYPos + extYAxisCaptionsTextFields[i].offset.dy);
				}
			}
		}
		
		tYPos = newTYPos;
		bYPos = newBYPos;
		
		for (var i:Number = 0;i<namesTextFields.length;i++) {

			if (namesTextFields[i].isChartAnchor == false)
				continue;				
			if (namesTextFields[i].position == 'top') {
				var box:Object = library.functions.getTextFieldBoundBox(namesTextFields[i]);
					
				var h:Number = box.bottom - box.top;
				library.functions.changeTextFieldY(namesTextFields[i],tYPos - h + namesTextFields[i].offset.dy);
				if (tYPos - h  + namesTextFields[i].offset.dy< newTYPos)
					newTYPos = tYPos - h  + namesTextFields[i].offset.dy;
			}else if (namesTextFields[i].position == 'bottom') {
				var box:Object = library.functions.getTextFieldBoundBox(namesTextFields[i]);
				var h:Number = box.bottom - box.top;
				library.functions.changeTextFieldY(namesTextFields[i],bYPos + namesTextFields[i].offset.dy);
				if (newBYPos <= bYPos + h + namesTextFields[i].offset.dy)
					newBYPos = bYPos + h + namesTextFields[i].offset.dy;
			}
		}
		
		tYPos = newTYPos;
		bYPos = newBYPos;
		
		/*
		for (var i:Number = 0;i<valuesTextFields.length;i++) {
			if (valuesTextFields[i].position == 'top') {
				var box:Object = library.functions.getTextFieldBoundBox(valuesTextFields[i]);
				if (box.bottom < w_chart_area_height)
					continue;
					
				var h:Number = box.bottom - box.top;
				library.functions.changeTextFieldY(valuesTextFields[i],tYPos - h + valuesTextFields[i].offset.dy);
				if (tYPos - h  + namesTextFields[i].offset.dy< newTYPos)
					newTYPos = tYPos - h  + valuesTextFields[i].offset.dy;
			}else if (valuesTextFields[i].position == 'bottom') {
				var box:Object = library.functions.getTextFieldBoundBox(valuesTextFields[i]);
				if (box.bottom < w_chart_area_height)
					continue;
				var h:Number = box.bottom - box.top;
				library.functions.changeTextFieldY(valuesTextFields[i],bYPos + valuesTextFields[i].offset.dy);
				if (newBYPos <= bYPos + h + valuesTextFields[i].offset.dy)
					newBYPos = bYPos + h + valuesTextFields[i].offset.dy;
			}
		}*/
		
		tYPos = newTYPos;
		bYPos = newBYPos;
		
		//block names
		//check top block names
		for (var i:Number = 0;i<topBlockNamesTextFields.length;i++) {
			if (topBlockNamesTextFields[i] == undefined)
				continue;
				
			var box:Object = library.functions.getTextFieldBoundBox(topBlockNamesTextFields[i]);
			var h:Number = box.bottom - box.top;
			library.functions.changeTextFieldY(topBlockNamesTextFields[i],tYPos + topBlockNamesTextFields[i].offset.dy - h);
			if (newTYPos > tYPos - h + topBlockNamesTextFields[i].offset.dy)
				newTYPos = tYPos - h + topBlockNamesTextFields[i].offset.dy;
		}
		
		tYPos = newTYPos;
		
		//check bottom block names
		for (var i:Number = 0;i<bottomBlockNamesTextFields.length;i++) {
			if (bottomBlockNamesTextFields[i] == undefined)
				continue;
				
			var box:Object = library.functions.getTextFieldBoundBox(bottomBlockNamesTextFields[i]);
			var h:Number = box.bottom - box.top;
			library.functions.changeTextFieldY(bottomBlockNamesTextFields[i],bYPos + bottomBlockNamesTextFields[i].offset.dy);
			if (newBYPos < bYPos + bottomBlockNamesTextFields[i].offset.dy + h)
				newBYPos = bYPos + bottomBlockNamesTextFields[i].offset.dy + h;
		}
		
		bYPos = newBYPos;

		//axis title
		if (x_axis_name_text_position.indexOf('top') !== -1) {
			var box:Object = library.functions.getTextFieldBoundBox(x_axis_name_field);
			var h:Number = box.bottom - box.top;
			library.functions.changeTextFieldY(x_axis_name_field,tYPos - h);
			tYPos -= h;
		}else {
			library.functions.changeTextFieldY(x_axis_name_field,bYPos);
		}
		
		if (subnameField) {
			subnameField._y = tYPos - subnameField._height;
			tYPos -= subnameField._height;
		}
		
		if (nameField) {
			nameField._y = tYPos - nameField._height;
			tYPos -= nameField._height;
		}
	}
	
	public static function getLeftX():Number {
		return 0;
	}
	
	public static function getRightX():Number {
		var ws:workspace_ = workspace_.getInstance();
		return ws.chart_area_width;
	}
	
	public static function getTopY():Number {
		return 0;
	}
	
	public static function getBottomY():Number {
		var ws = workspace_.getInstance();
		return ws.w_chart_area_height;
	}
}