﻿import chart.Axis;

class workspace.grid
{	
	public var captions:Array;

	public var xsmart:Boolean = false;
	public var ysmart:Boolean = false;

	public var enabled:Boolean = false;

	public var gridX:Number;
	public var gridY:Number;
	public var gridWidth:Number;
	public var gridHeight:Number;

	public var otherGridWidth:Number;
	public var otherGridHeight:Number;
	public var otherGridX:Number;
	public var otherGridY:Number;
	
	public var auto_scale:Boolean = true;
	
	public var isExt:Boolean = false;
	
	//grid position
	private var g_x:Number;
	private var g_y:Number;
	
	//grid size
	private var g_width:Number;
	private var g_height:Number;
	
	//grid lines settings
	private var g_lines_size:Number;
	private var g_lines_color:Number;
	private var g_lines_alpha:Number;
	
	//show values
	private var g_start_value:Number;
	private var g_end_value:Number;
	
	//grid maximum value
	private var g_maximum_value:Number;
	private var g_minimum_value:Number;
	
	//grid lines step
	private var g_lines_step:Number
	//or set lines count
	private var g_lines_count:Number = 10;
	
	//grid orientation
	private var g_orientation:String;
	
	//ticks settings
	public var ticks:Array;
	
	//grid steps
	private var d_s:Number;
	private var d_v:Number;
	private var start_value:Number;
	
	private var initialized:Boolean = false;
	
	//captions settings
	private var g_caption_text_format:TextFormat;
	private var g_caption_position:String;
	private var g_caption_prefix:String;
	private var g_caption_postfix:String;
	private var g_caption_decimal_places:Number;
	private var g_caption_decimal_separator:String;
	private var g_caption_thousand_separator:String;
	private var g_caption_rotation:Number;
	private var g_caption_dx:Number;
	private var g_caption_dy:Number;
	
	private var lib;
	
	public var isInversed:Boolean;
	
	//movie clip
	private var grid_mc:MovieClip;
	
	public var start_grid_value:Number;
	public var end_grid_value:Number;
	
	public var captions_enabled:Boolean = true;
	
	public var swapAxis:Boolean = false;
	//--------------------------------------------------------
	//getters and setters
	function set grid_start_value(v:Number):Void
	{
		g_start_value = v;
	}
	function set grid_end_value(v:Number):Void
	{
		g_end_value = v;
	}
	function get x():Number
	{
		return g_x;
	}
	function get y():Number
	{
		return g_y;
	}
	function get width():Number
	{
		return g_width;
	}
	function get height():Number
	{
		return g_height;
	}
	//set x position
	function set x(new_x:Number):Void
	{
		g_x = new_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		g_y = new_y;
	}
	//set width
	function set width(w:Number):Void
	{
		g_width = w;
	}
	//set height
	function set height(h:Number):Void
	{
		g_height = h;
	}
	//set lines size
	function set lines_size(s:Number):Void
	{
		if ((s>0) and (s<255))
		{
			g_lines_size = s;
		}else
		{
			_root.showError("grid lines size should be between 0 and 255");
		}
	}
	//set lines color
	function set lines_color(c:Number):Void
	{
		if ((c<0) or (c>0xFFFFFF))
		{
			_root.showError("grid lines color should be between 0 and 0xFFFFFF");
		}else
		{
			g_lines_color = c;
		}
	}
	//set lines alpha
	function set lines_alpha(a:Number):Void
	{
		if (a<0)
			a = 0;
		else if (a>100)
			a = 100;
			
		g_lines_alpha = a;
	}
	//set maximum value
	function set maximum_value(m:Number):Void
	{
		g_maximum_value = m;
	}
	//get maximum value
	function get maximum_value():Number {
		return g_maximum_value;
	}
	//set minimum value
	function set minimum_value(m:Number):Void
	{
		g_minimum_value = m;
	}
	function get minimum_value():Number {
		return g_minimum_value;
	}
	//set lines count
	function set lines_count(c:Number):Void
	{
		g_lines_count = c;
	}
	
	function get lines_count():Number {
		return g_lines_count;
	}
	
	//set lines step
	function set lines_step(s:Number):Void
	{
		g_lines_step = s;
		this.auto_scale = false;
	}
	//set caption text format
	function set captions_text_format(tf:TextFormat):Void
	{
		g_caption_text_format = tf;
	}
	function set captions_rotation(r:Number):Void
	{
		g_caption_rotation = r;
	}
	function set captions_dx(d:Number):Void
	{
		g_caption_dx = d;
	}
	function set captions_dy(d:Number):Void
	{
		g_caption_dy = d;
	}
	//set captions position
	function set captions_position(pos:String):Void
	{
		if (g_orientation=='horizontal')
		{
			if ((pos!='left') and (pos!='right'))
			{
				_root.showError("grid captions position should be 'left' or 'right'");
			}else
			{
				g_caption_position = pos;
			}
		}else
		{
			if ((pos!='top') and (pos!='bottom'))
			{
				_root.showError("grid captions position should be 'top' or 'bottom'");
			}else
			{
				g_caption_position = pos;
			}
		}
	}
	//captions prefix
	function set captions_prefix(p:String):Void
	{
		g_caption_prefix = p;
	}
	//captions postfix
	function set captions_postfix(p:String):Void
	{
		g_caption_postfix = p;
	}
	//decimal places
	function set captions_decimal_places(p:Number):Void
	{
		if (p<0)
		{
			_root.showError("grid captions decimal places should be positive and integer");
		}else
		{
			g_caption_decimal_places = p;
		}
	}
	//decimal separator
	function set captions_decimal_separator(ds:String):Void
	{
		g_caption_decimal_separator = ds;
	}
	//thousand separator
	function set captions_thousand_separator(ts:String):Void
	{
		g_caption_thousand_separator = ts;
	}
	
	private var pos:Number;
	private var val_pos:Number;
	private var max_pos:Number;
	
	private var lines:Array;
	
	//--------------------------------------------------------
	//constructor
	function grid(target_mc:MovieClip,o:String)
	{
		this.captions = new Array();
		this.isInversed = false;
		
		var index:Number = arguments[2] != undefined ? arguments[2] : 0;
		var d:Number = DepthManager.GRID_START_DEPTH + index + (o == 'horizontal' ? 0 : 1);
		
		if (target_mc['workspace_grid_'+o+index]) {
			target_mc['workspace_grid_'+o+index].unloadMovie();
			target_mc['workspace_grid_'+o+index].removeMovieClip();
		}
		var gname:String = 'workspace_grid_'+o+index;
			
		grid_mc = target_mc.createEmptyMovieClip(gname,d);
		//set orientation
		if ((o!='horizontal') and (o!='vertical'))
		{
			_root.showError("grid orientation should be 'horizontal' or 'vertical'");
		}else
		{
			g_orientation = o;
		}
		//set defaults
		g_lines_size = 1;
		g_lines_color = 0x000000;
		g_lines_alpha = 100;
		if (g_orientation=='horizontal')
		{
			g_caption_position = 'left';
		}else
		{
			g_caption_position = 'bottom';
		}
		g_caption_text_format = new TextFormat();
		g_caption_text_format.font = 'Verdana';
		g_caption_prefix = '';
		g_caption_postfix = '';
		g_caption_decimal_separator = '.';
		g_caption_thousand_separator = '';
		g_caption_decimal_places = 2;
		lib = new library.functions();
		g_caption_rotation = 0;
		g_caption_dx = 0;
		g_caption_dy = 0;
		this.lines = new Array();
		this.ticks = new Array();
	}
	//----------------------------------------------------------
	//painting
	function init():Void
	{
		if (g_start_value==undefined)
		{
			g_start_value = g_minimum_value;
		}
		if (g_end_value==undefined)
		{
			g_end_value = g_maximum_value;
		}

		grid_mc._x = g_x;
		grid_mc._y = g_y;
		if (this.end_grid_value != undefined && g_orientation == 'horizontal')
		{
			if (this.end_grid_value < g_start_value)
				this.end_grid_value = g_start_value;

			if (this.start_grid_value > g_end_value)
				this.start_grid_value = g_end_value;
		}
		if (this.start_grid_value != undefined && g_orientation != 'horizontal')
		{
			if (this.start_grid_value < g_start_value)
				this.start_grid_value = g_start_value;

			if (this.end_grid_value > g_end_value)
				this.end_grid_value = g_end_value;
		}				

		if (this.isInversed)
			if (this.start_grid_value != undefined && this.end_grid_value != undefined)
			{
				var tmp:Number = this.start_grid_value;
				this.start_grid_value = this.end_grid_value;
				this.end_grid_value = tmp;
			}			

		if (this.g_lines_step != undefined)
			this.createLinesByStep();
		else
			this.createLinesWithAutoScale();
		
		if (lines.length > 1) {
			for (var i:Number = 0;i<this.lines.length;i++) {
				var line:Object = lines[i];
				if (!isNaN(line.x_1) && (gridX == undefined || line.x_1 < gridX) && (line.x_2 != line.x_1)) {
					gridX = line.x_1;
				}
				if (!isNaN(line.x_2) && (gridWidth == undefined || line.x_2 > gridWidth) && (line.x_2 != line.x_1)) {
					gridWidth = line.x_2;
				}
				if (!isNaN(line.y_1) && (gridY == undefined || line.y_1 < gridY) && (line.y_2 != line.y_1)) {
					gridY = line.y_1;
				}
				if (!isNaN(line.y_2) && (gridHeight == undefined || line.y_2 > gridHeight) && (line.y_2 != line.y_1)) {
					gridHeight = line.y_2;
				}
			}			
		}
		
		if (arguments[0] == undefined)
			this.initialized = true;
	}
	
	private var interval_size:Number;
	
	private function createLinesWithAutoScale():Void {		
		this.lines = new Array();
		
		var count:Number = (this.minimum_value - this.maximum_value)/this.interval_size;
		if (count.toString() == 'NaN')
			return;
			
		count = Math.abs(count);
		for (var i:Number = 0;i<=count;i++) {
			if (this.g_orientation == 'horizontal') {
				var scale:Number = this.g_height/(this.maximum_value - this.minimum_value);
				var zeroY:Number = this.maximum_value*scale;
				if (this.isInversed)
					zeroY = -this.minimum_value*scale;
				var val:Number = (this.minimum_value + this.interval_size * i);
				var y:Number = zeroY - val*scale;
				if (this.isInversed)
					y = zeroY + val*scale;
				var line:Object = {};
				line.value = val;
				line.x_1 = line.x_2 = 0;
				line.y_1 = line.y_2 = y;
				
				if (this.start_grid_value == undefined && this.end_grid_value == undefined && val >= this.minimum_value && val <= this.maximum_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value != undefined && val >= this.start_grid_value && val <= this.end_grid_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value == undefined && val >= this.end_grid_value)
					this.lines.push(line);
				else if (this.end_grid_value != undefined && this.start_grid_value==undefined && val <= this.end_grid_value)
					this.lines.push(line);
					
			}else {
				var scale:Number = this.g_width/(this.maximum_value - this.minimum_value);
				var zeroX:Number = -this.minimum_value*scale;
				var val:Number = (this.minimum_value + this.interval_size * i);;
				var x:Number = zeroX + val*scale;
				var line:Object = {};
				line.value = val;
				line.x_1 = line.x_2 = x;
				line.y_1 = line.y_2 = 0;
				
				if (this.start_grid_value == undefined && this.end_grid_value == undefined && val >= this.minimum_value && val <= this.maximum_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value != undefined && val >= this.start_grid_value && val <= this.end_grid_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value == undefined && val >= this.end_grid_value)
					this.lines.push(line);
				else if (this.end_grid_value != undefined && this.start_grid_value==undefined && val <= this.end_grid_value)
					this.lines.push(line);

			}
		}
	}
	
	private function createLinesByStep():Void {
		this.lines = new Array();
		var upCount:Number;
		var downCount:Number;
		if (this.minimum_value == 0) {
			upCount = Math.floor(this.maximum_value/this.g_lines_step);
			downCount = 0;
		}else if (this.minimum_value > 0) {
			upCount = Math.floor((this.maximum_value - this.minimum_value)/this.g_lines_step);
			downCount = 0;
		}else if (this.maximum_value > 0) {
			upCount = Math.floor(this.maximum_value/this.g_lines_step);
			downCount = Math.ceil(-this.minimum_value/this.g_lines_step);
		}else if (this.maximum_value == 0) {
			upCount = 0;
			downCount = Math.ceil(-this.minimum_value/this.g_lines_step);
		}else {
			upCount = 0;
			downCount = Math.ceil((Math.abs(this.minimum_value) - Math.abs(this.maximum_value))/this.g_lines_step);
		}
		upCount++;
		downCount++;
		
		if (this.isInversed) {
			var tmp:Number = this.start_grid_value;
			this.start_grid_value = this.end_grid_value;
			this.end_grid_value = tmp;
		}
		
		var i:Number;
		var scale:Number;
		var val:Number;
		if (this.g_orientation == 'horizontal') {
			var scale:Number = this.g_height/(this.maximum_value - this.minimum_value);
			var zeroY:Number = this.isInversed ? -this.minimum_value*scale : this.maximum_value*scale;
			for (i=0;i<upCount;i++) {
				if (this.start_grid_value == undefined || this.start_grid_value <= 0)
					val = this.minimum_value <= 0 ? (this.g_lines_step * i) : (this.g_lines_step * i + this.minimum_value);
				else {
					val = this.g_lines_step * i + this.start_grid_value;
				}
				var y:Number = zeroY - val*scale;
				if (this.isInversed)
					y = zeroY + val*scale;
				
				var line:Object = {};
				line.value = val;
				line.x_1 = line.x_2 = 0;
				line.y_1 = line.y_2 = y;
				if (this.start_grid_value == undefined && this.end_grid_value == undefined && val >= this.minimum_value && val <= this.maximum_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value != undefined && val >= this.start_grid_value && val <= this.end_grid_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value == undefined && val >= this.end_grid_value)
					this.lines.push(line);
				else if (this.end_grid_value != undefined && this.start_grid_value==undefined && val <= this.end_grid_value)
					this.lines.push(line);
					
			}
			for (i=0;i<downCount;i++) {
				if (this.end_grid_value == undefined || this.end_grid_value >= 0)
					val = this.maximum_value >= 0 ? (-this.g_lines_step * i) : (-this.g_lines_step * i + this.maximum_value);
				else
					val = this.end_grid_value - this.g_lines_step * i;
										
				var y:Number = zeroY - val*scale;
				if (this.isInversed)
					y = zeroY + val*scale;

				var line:Object = {};
				line.value = val;
				line.x_1 = line.x_2 = 0;
				line.y_1 = line.y_2 = y;
								
				if (this.start_grid_value == undefined && this.end_grid_value == undefined && val >= this.minimum_value && val <= this.maximum_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value != undefined && val >= this.start_grid_value && val <= this.end_grid_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value == undefined && val >= this.end_grid_value)
					this.lines.push(line);
				else if (this.end_grid_value != undefined && this.start_grid_value==undefined && val <= this.end_grid_value)
					this.lines.push(line);
			}
		}else {
			var scale:Number = this.g_width/(this.maximum_value - this.minimum_value);
			var zeroX:Number = -this.minimum_value*scale;
			
			for (i=0;i<upCount;i++) {
				
				if (this.start_grid_value == undefined || this.start_grid_value <= 0)
					val = this.minimum_value <= 0 ? (this.g_lines_step * i) : (this.g_lines_step * i + this.minimum_value);
				else
					val = this.start_grid_value + this.g_lines_step * i;
					
				var x:Number = zeroX + val*scale;
				var line:Object = {};
				line.value = val;
				line.x_1 = line.x_2 = x;
				line.y_1 = line.y_2 = 0;
				
				if (this.start_grid_value == undefined && this.end_grid_value == undefined && val >= this.minimum_value && val <= this.maximum_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value != undefined && val >= this.start_grid_value && val <= this.end_grid_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value == undefined && val >= this.end_grid_value)
					this.lines.push(line);
				else if (this.end_grid_value != undefined && this.start_grid_value==undefined && val <= this.end_grid_value)
					this.lines.push(line);
			}
			for (i=0;i<downCount;i++) {
				if (this.end_grid_value == undefined || this.end_grid_value >= 0)
					val = this.maximum_value >= 0 ? (-this.g_lines_step * i) : (-this.g_lines_step * i + this.maximum_value);
				else
					val = this.end_grid_value - this.g_lines_step * i;
				
					
				var x:Number = zeroX + val*scale;
				var line:Object = {};
				line.value = val;
				line.x_1 = line.x_2 = x;
				line.y_1 = line.y_2 = 0;
				
				if (this.start_grid_value == undefined && this.end_grid_value == undefined && val >= this.minimum_value && val <= this.maximum_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value != undefined && val >= this.start_grid_value && val <= this.end_grid_value)
					this.lines.push(line);
				else if (this.start_grid_value != undefined && this.end_grid_value == undefined && val >= this.end_grid_value)
					this.lines.push(line);
				else if (this.end_grid_value != undefined && this.start_grid_value==undefined && val <= this.end_grid_value)
					this.lines.push(line);
			}
		}
	}
	
	public function calculateScale():Void {
		var isHorizontalChart:Boolean = arguments[0] != undefined ? Boolean(arguments[0]) : false;
		var includeZero:Boolean = arguments[1] != undefined ? Boolean(arguments[1]) : false;
		
		var min:Number = this.g_minimum_value;
		var max:Number = this.g_maximum_value;		
		
		var max1:Number = Math.max(max,min);
		var min1:Number = Math.min(max,min);
		
		min = min1;
		max = max1;
		
		if (includeZero && (min > 0) && (!this.ysmart)) {
			min = 0;
			this.minimum_value = 0;
		}
			
		if ((this.g_orientation == 'horizontal' && this.ysmart == false) || (this.g_orientation != 'horizontal' && this.xsmart == false && !isHorizontalChart))
			if (!(this.g_orientation != 'horizontal' && isHorizontalChart && this.ysmart == true))
				return;
		
		if (this.g_orientation != 'horizontal' && isHorizontalChart && this.ysmart == false)
			return;
		
		trace ('smart');
		this.init(1);
		var axis:Axis = new Axis();
		axis.setData(max,min);
		this.maximum_value = axis.getTop();
		this.minimum_value = axis.getBottom();
		this.interval_size = axis.getIntervalSize();
	}
	
	public function paint():Void
	{
		grid_mc._x = g_x;
		grid_mc._y = g_y;
		if (!this.enabled)
			return;
			
		if (!this.initialized)
			this.init();
			
		for (var i:Number = 0;i<this.lines.length;i++)
			this.paint_line(this.lines[i]);

		this.show_ticks();
	}
	
	function remove():Void
	{
		grid_mc.removeMovieClip();
	}
	
	
	//piting line
	private function paint_line(line:Object):Void
	{
		
		line.value = lib.roundDecimal(line.value,2);
		var x_1:Number;
		var x_2:Number;
		var y_1:Number;
		var y_2:Number;
		if (g_orientation=='horizontal')
		{
			if (otherGridX != undefined)
				x_1 = otherGridX;
			else {
				var p:Object = {x:workspace.workspace_.getInstance().chart_area_x,y:0};
				grid_mc.globalToLocal(p);
				x_1 = p.x;
			}
			if (otherGridWidth != undefined)
				x_2 = otherGridWidth;
			else {
				var w:Number = workspace.workspace_.getInstance().chart_area_width;
				x_2 = x_1 + w;
			}
			y_1 = line.y_1;
			y_2 = line.y_2;
		}else
		{
			x_1 = line.x_1;
			x_2 = line.x_2;
			if (otherGridY != undefined)
				y_1 = otherGridY;
			else {
				var p:Object = {y:workspace.workspace_.getInstance().chart_area_y,x:0};
				grid_mc.globalToLocal(p);
				y_1 = p.y;
			}
			if (otherGridHeight != undefined)
				x_2 = otherGridHeight;
			else {
				var h:Number = workspace.workspace_.getInstance().chart_area_height;
				y_2 = y_1 + h;
			}
		}
		grid_mc.lineStyle(g_lines_size,g_lines_color,g_lines_alpha);
		grid_mc.moveTo(x_1,y_1);
		grid_mc.lineTo(x_2,y_2);
		show_caption(line);
	}
	//show line caption
	private function show_caption(line:Object):Void
	{
		if (!this.captions_enabled)
			return;
			
		var x_pos:Number;
		var y_pos:Number;
		var dx:Number;
		var dy:Number;
		
		
		if (g_orientation=='horizontal')
		{
			dy = 0.5;
			if (g_caption_position=='left')
			{
				x_pos = (this.otherGridX != undefined) ? this.otherGridX : 0;
				dx = 1;
			}else
			{
				x_pos = (this.otherGridWidth != undefined) ? this.otherGridWidth : g_width;
				dx = 0;
			}
			y_pos = line.y_1;
			var p:Number = y_pos;			
		}else
		{
			dx = 0.5;
			if (g_caption_position=='top')
			{
				y_pos = (this.otherGridY != undefined) ? this.otherGridY : 0;
				dy = 1;
			}else
			{
				y_pos = (this.otherGridHeight != undefined) ? this.otherGridHeight : g_height;
				dy = 0;
			}
			x_pos = line.x_1;
			var p:Number = x_pos;
		}
		
		var text:String = lib.config_text(String(line.value),g_caption_decimal_places,g_caption_thousand_separator,g_caption_decimal_separator,g_caption_prefix,g_caption_postfix);
		var pos:Object = {x:x_pos, y:y_pos};
		grid_mc.localToGlobal(pos);
		
		var format:TextFormat;
		if (g_caption_rotation == 0)
			format = g_caption_text_format;
		else {
			format = _global[g_caption_text_format.font];
			format.size = g_caption_text_format.size;
			format.color = g_caption_text_format.color;
		}
		
		var ws = workspace.workspace_.getInstance();
		if (g_orientation == 'horizontal') {			
			if (!isExt){				
				ws.horizontalGridLabelsPosition = g_caption_position;
				ws.addXAxisGridCaption(text, format,pos.x,pos.y,g_caption_rotation,g_caption_dx,g_caption_dy,-dx,-dy);
			}else {
				ws.extHorizontalGridLabelsPosition = g_caption_position;
				ws.addExtXAxisGridCaption(text, format,pos.x,pos.y,g_caption_rotation,g_caption_dx,g_caption_dy,-dx,-dy);
			}
		}else {			
			if (!isExt) {
				if (swapAxis) {
					ws.verticalGridLabelsPosition = g_caption_position;
					ws.addYAxisGridCaption(text, format,pos.x,pos.y,g_caption_rotation,g_caption_dx,g_caption_dy,-dx,-dy);
				}else {
					ws.verticalGridLabelsPosition = g_caption_position;
					ws.addYAxisGridCaption(text, format,pos.x,pos.y,g_caption_rotation,g_caption_dx,g_caption_dy,-dx,-dy);
				}
			}else {
				ws.extVerticalGridLabelsPosition = g_caption_position;
				ws.addExtYAxisGridCaption(text, format,pos.x,pos.y,g_caption_rotation,g_caption_dx,g_caption_dy,-dx,-dy);
			}
		}
	}
	
	public var majorTicks:Object;
	public var minorTicks:Object;
	
	private var ticksContainer:MovieClip;
	
	private function show_ticks():Void
	{
		var pos:Object = {x: grid_mc._x , y: grid_mc._y};
		grid_mc._parent.localToGlobal(pos);
		ticksContainer = _root.createEmptyMovieClip('ticksContainer',DepthManager.TICKS_CONTAINER);
		var i:Number;
		ticksContainer._x = pos.x;
		ticksContainer._y = pos.y;
		
		if (this.majorTicks == undefined)
			return;
		
		if (this.majorTicks.start == undefined)
			this.majorTicks.start = this.minimum_value;
				
		if (this.majorTicks.step == undefined)
			if (this.g_lines_step != undefined)
				this.majorTicks.step = this.g_lines_step;
			else
				this.majorTicks.step = this.interval_size;
		
		if (this.minorTicks != undefined)
			this.showMinorTicks();
			
		this.showMajorTicks();
	}	
	
	
	
	private function showMajorTicks():Void {
		var count:Number = (maximum_value - majorTicks.start)/majorTicks.step;
		if (isNaN(count) || count <= 1)
			return;
		for (var i:Number = 0;i <= count;i++) {
			showTick(majorTicks.start + i*majorTicks.step, majorTicks.lines);
		}
	}
	
	private function showMinorTicks():Void {
		
		var count:Number = (maximum_value - majorTicks.start)/majorTicks.step;
		count = Math.floor(count);
		if (isNaN(count) || count < 2)
			return;
			
		for (var i:Number = 0;i <= (count-1);i++) {
			var w:Number = majorTicks.step;
			if (minorTicks.step == undefined)
				minorTicks.step = w/5;
			var cnt:Number = Math.floor(w/minorTicks.step);
			if (isNaN(cnt))
				return;
			if (cnt <= 1)
				_root.showError('Grid minor ticks step should be less than grid major ticks step');
			for (var j:Number = 1;j<cnt;j++) {
				showTick(majorTicks.start + majorTicks.step * i + minorTicks.step * j, minorTicks.lines);
			}
		}
		return;
	}
	
	private function showTick(pos:Number, settings:Object):Void {
		var x_1:Number;
		var x_2:Number;
		var y_1:Number;
		var y_2:Number;
		
		var d1:Number = 0;
		var d2:Number = 0;
		switch (settings.type) {
			case 'inside':
				d1 = settings.length;
				break;
			case 'outside':
				d2 = settings.length;
				break;
			case 'cross':
				d1 = d2 = settings.length/2;
				break;
		}
		trace (d1+','+d2);
		if (g_orientation=='horizontal')
		{
			var scale:Number = this.g_height/(this.maximum_value - this.minimum_value);
			var zeroY:Number = this.isInversed ? -this.minimum_value*scale : this.maximum_value*scale;			
			var y:Number = zeroY - pos*scale;
			if (this.isInversed)
				y = zeroY + pos*scale;
			x_2 = d1;
			x_1 = -d2;
			y_1 = y;
			y_2 = y;
			if (g_caption_position=='left') {
				if (this.otherGridX != undefined) {
					x_1 += this.otherGridX;
					x_2 += this.otherGridX;
				}else {
					var p:Object = {x:workspace.workspace_.getInstance().chart_area_x,y:0};
					grid_mc.globalToLocal(p);
					var dx:Number = p.x;
					x_1 += dx;
					x_2 += dx;
				}
			}else {
				if (this.otherGridWidth != undefined) {
					x_2 = otherGridWidth - d1;
					x_1 = otherGridWidth + d2;
				}else {
					var p:Object = {x:workspace.workspace_.getInstance().chart_area_x,y:0};
					grid_mc.globalToLocal(p);
					var dx:Number = p.x;
					var w:Number = workspace.workspace_.getInstance().chart_area_width;
					x_2 = dx + w - d1;
					x_1 = dx + w + d2;
				}
			}
		}else
		{
			var scale:Number = this.g_width/(this.maximum_value - this.minimum_value);
			var zeroX:Number = -this.minimum_value*scale;
			var x:Number = zeroX + pos*scale;
			x_1 = x;
			x_2 = x;
			y_1 = - d1;
			y_2 = d2;
			if (g_caption_position=='top') {
				if (this.otherGridY != undefined) {
					y_1 = this.otherGridY - d2;
					y_2 = this.otherGridY + d1;
				}else {
					var p:Object = {y:workspace.workspace_.getInstance().chart_area_y,x:0};
					grid_mc.globalToLocal(p);
					var dy:Number = p.y;
					y_1 = dy - d2;
					y_2 = dy + d1;
				}
			}else {
				if (this.otherGridHeight != undefined) {
					y_2 = otherGridHeight - d1;
					y_1 = otherGridHeight + d2;
				}else {
					var p:Object = {y:workspace.workspace_.getInstance().chart_area_y,x:0};
					grid_mc.globalToLocal(p);
					var dy:Number = p.y;
					var h:Number = workspace.workspace_.getInstance().chart_area_height;
					y_2 = dy + h - d1;
					y_1 = dy + h + d2;
				}
			}
		}
		ticksContainer.lineStyle(settings.size,settings.color,settings.alpha,true,"normal","none","miter");
		ticksContainer.moveTo(x_1,y_1);
		ticksContainer.lineTo(x_2,y_2);
	}
	
}