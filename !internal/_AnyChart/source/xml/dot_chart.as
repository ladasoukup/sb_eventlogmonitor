﻿class xml.dot_chart
{
	private var chart;
	private var lib;
	//------------------------------------
	function dot_chart(doc:XML,_chart)
	{
		lib = new xml.library();
		chart = _chart;
		var num_type:Number = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{
			var num_chart:Number = lib.findNode(doc.firstChild.childNodes[num_type],'chart');
			if (num_chart!=undefined)
			{
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value!=undefined)
				{
					chart.maximum_value = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value!=undefined)
				{
					chart.minimum_value = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_argument!=undefined)
				{
					chart.maximum_argument = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_argument);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_argument!=undefined)
				{
					chart.minimum_argument = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_argument);
				}
				var num_hints:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'hints');
				if (num_hints!=undefined)
				{
					lib.config_hints(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_hints],chart);
				}
				var num_animation:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'animation');
				if (num_animation!=undefined)
				{
					lib.config_animation(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_animation],chart);
				}
				var num_names:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'names');
				if (num_names!=undefined)
				{
					lib.config_names(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_names],chart);
				}
				var num_values:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'values');
				if (num_values!=undefined)
				{
					lib.config_values(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_values],chart);
				}
				var num_arguments:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'arguments');
				if (num_arguments!=undefined)
				{
					config_arguments(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_arguments]);
				}
				var num_dots:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'dot_chart');
				if (num_dots!=undefined)
				{
					config_chart(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_dots]);
				}
			}
		}
		var num_data:Number = lib.findNode(doc.firstChild,'data');
		if (num_data!=undefined)
		{
			config_data(doc.firstChild.childNodes[num_data]);
		}
	}
	//config chart
	private function config_chart(node:XMLNode):Void
	{
		if (node.attributes.type!=undefined)
		{
			chart.default_dot_type = String(node.attributes.type);
		}
		if (node.attributes.image!=undefined)
		{
			chart.default_dot_image = String(node.attributes.image);
		}
		if (node.attributes.radius!=undefined)
		{
			chart.default_dot_radius = Number(node.attributes.radius);
		}
		if (node.attributes.left_space!=undefined)
		{
			chart.x += Number(node.attributes.left_space);
			chart.width -= Number(node.attributes.left_space);
		}
		if (node.attributes.right_space!=undefined)
		{
			chart.width -= Number(node.attributes.right_space);
		}
		if (node.attributes.up_space!=undefined)
		{
			chart.y += Number(node.attributes.up_space);
			chart.height -= Number(node.attributes.up_space);
		}
		if (node.attributes.down_space!=undefined)
		{
			chart.height -= Number(node.attributes.up_space);
		}
	}
	//config data
	private function config_data(node:XMLNode):Void
	{
		var i:Number;
		var j:Number;
		var n:Number = 0;
		var k:Number = 0;
		//------------------------
		var hint_texts:Array = new Array();
		var dots_type:Array = new Array();
		var background_enabled:Array = new Array();		
		var background_color:Array = new Array();
		var background_alpha:Array = new Array();
		var border_enabled:Array = new Array();
		var border_size:Array = new Array();
		var border_color:Array = new Array();
		var border_alpha:Array = new Array();
		var background_enabled_block:Array = new Array();
		var background_color_block:Array = new Array();
		var background_alpha_block:Array = new Array();
		var border_enabled_block:Array = new Array();
		var border_size_block:Array = new Array();
		var border_color_block:Array = new Array();
		var border_alpha_block:Array = new Array();
		var dots_radius:Array = new Array();
		var dots_radius_block:Array = new Array();
		var dots_images:Array = new Array();
		var dots_images_block:Array = new Array();
		var show_values_block:Array = new Array();
		var show_names_block:Array = new Array();
		var show_arguments:Array = new Array();
		var show_arguments_block:Array = new Array();
		var dots_urls:Array = new Array();
		var dots_urls_target:Array = new Array();
		var dots_sounds:Array = new Array();
		var names_rotations:Array = new Array();
		var values_rotations:Array = new Array();
		var names_dxs:Array = new Array();
		var values_dxs:Array = new Array();
		var names_dys:Array = new Array();
		var values_dys:Array = new Array();
		var block_names:Array = new Array();
		//------------------------
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				background_enabled[n] = new Array();
				background_color[n] = new Array();
				background_alpha[n] = new Array();
				border_enabled[n] = new Array();
				border_size[n] = new Array();
				border_color[n] = new Array();
				border_alpha[n] = new Array();
				dots_radius[n] = new Array();
				show_arguments[n] = new Array();
				dots_images[n] = new Array();
				dots_urls[n] = new Array();
				dots_urls_target[n] = new Array();
				dots_sounds[n] = new Array();
				names_rotations[n] = new Array();
				values_rotations[n] = new Array();
				names_dxs[n] = new Array();
				names_dys[n] = new Array();
				values_dxs[n] = new Array();
				values_dys[n] = new Array();
				hint_texts[n] = new Array();
				if (node.childNodes[i].attributes.name != undefined) {
					block_names[n] = String(node.childNodes[i].attributes.name);
				}
				if (node.childNodes[i].attributes.dots_type!=undefined)
				{
					dots_type[n] = String(node.childNodes[i].attributes.dots_type);
				}
				if (node.childNodes[i].attributes.background_enabled!=undefined)
				{
					background_enabled_block[n] = lib.getBool(node.childNodes[i].attributes.background_enabled);
				}
				if (node.childNodes[i].attributes.color!=undefined)
				{
					background_color_block[n] = lib.getColor(node.childNodes[i].attributes.color);
				}
				if (node.childNodes[i].attributes.alpha!=undefined)
				{
					background_alpha_block[n] = Number(node.childNodes[i].attributes.alpha);
				}
				if (node.childNodes[i].attributes.border_enabled!=undefined)
				{
					border_enabled_block[n] = lib.getBool(node.childNodes[i].attributes.border_enabled);
				}
				if (node.childNodes[i].attributes.border_size!=undefined)
				{
					border_size_block[n] = Number(node.childNodes[i].attributes.border_size);
				}
				if (node.childNodes[i].attributes.border_alpha!=undefined)
				{
					border_alpha_block[n] = Number(node.childNodes[i].attributes.border_alpha);
				}
				if (node.childNodes[i].attributes.border_color!=undefined)
				{
					border_color_block[n] = lib.getColor(node.childNodes[i].attributes.border_color);
				}
				if (node.childNodes[i].attributes.dots_radius!=undefined)
				{
					dots_radius_block[n] = Number(node.childNodes[i].attributes.dots_radius);
				}
				if (node.childNodes[i].attributes.dots_image!=undefined)
				{
					dots_images_block[n] = String(node.childNodes[i].attributes.dots_image);
				}
				if (node.childNodes[i].attributes.show_names!=undefined)
				{
					show_names_block[n] = lib.getBool(node.childNodes[i].attributes.show_names);
				}
				if (node.childNodes[i].attributes.show_values!=undefined)
				{
					show_values_block[n] = lib.getBool(node.childNodes[i].attributes.show_values);
				}
				if (node.childNodes[i].attributes.show_arguments!=undefined)
				{
					show_arguments_block[n] = lib.getBool(node.childNodes[i].attributes.show_arguments);
				}
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].attributes.hint_text != undefined)
						hint_texts[n][j] = String(node.childNodes[i].childNodes[j].attributes.hint_text);
						
					if (node.childNodes[i].childNodes[j].attributes.radius!=undefined)
					{
						dots_radius[n][k] = Number(node.childNodes[i].childNodes[j].attributes.radius);
					}
					if (node.childNodes[i].childNodes[j].attributes.image!=undefined)
					{
						dots_images[n][k] = String(node.childNodes[i].childNodes[j].attributes.image);
					}
					if (node.childNodes[i].childNodes[j].attributes.show_argument!=undefined)
					{
						show_arguments[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.show_argument);
					}
					if (node.childNodes[i].childNodes[j].attributes.color!=undefined)
					{
						background_color[n][k] = lib.getColor(node.childNodes[i].childNodes[j].attributes.color);
					}
					if (node.childNodes[i].childNodes[j].attributes.alpha!=undefined)
					{
						background_alpha[n][k] = Number(node.childNodes[i].childNodes[j].attributes.alpha);
					}
					if (node.childNodes[i].childNodes[j].attributes.background_enabled!=undefined)
					{
						background_enabled[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.background_enabled);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_enabled!=undefined)
					{
						border_enabled[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.border_enabled);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_size!=undefined)
					{
						border_size[n][k] = Number(node.childNodes[i].childNodes[j].attributes.border_size);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_color!=undefined)
					{
						border_color[n][k] = lib.getColor(node.childNodes[i].childNodes[j].attributes.border_color);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_alpha!=undefined)
					{
						border_alpha[n][k] = Number(node.childNodes[i].childNodes[j].attributes.border_alpha);
					}
					if (node.childNodes[i].childNodes[j].attributes.url!=undefined)
					{
						dots_urls[n][k] = String(node.childNodes[i].childNodes[j].attributes.url);
					}
					if (node.childNodes[i].childNodes[j].attributes.url_target!=undefined)
					{
						dots_urls_target[n][k] = String(node.childNodes[i].childNodes[j].attributes.url_target);
					}
					if (node.childNodes[i].childNodes[j].attributes.sound!=undefined)
					{
						dots_sounds[n][k] = String(node.childNodes[i].childNodes[j].attributes.sound);
					}
					if (node.childNodes[i].childNodes[j].attributes.name_rotation!=undefined)
					{
						names_rotations[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_rotation);
					}
					if (node.childNodes[i].childNodes[j].attributes.value_rotation!=undefined)
					{
						values_rotations[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_rotation);
					}
					if (node.childNodes[i].childNodes[j].attributes.name_x_offset!=undefined)
					{
						names_dxs[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_x_offset);
					}
					if (node.childNodes[i].childNodes[j].attributes.name_y_offset!=undefined)
					{
						names_dys[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_y_offset);
					}
					if (node.childNodes[i].childNodes[j].attributes.value_x_offset!=undefined)
					{
						values_dxs[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_x_offset);
					}
					if (node.childNodes[i].childNodes[j].attributes.value_y_offset!=undefined)
					{
						values_dys[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_y_offset);
					}
					k++;
				}
				n++;
			}
		}
		chart.block_names = block_names;
		chart.hint_texts = hint_texts;
		chart.background_enabled = background_enabled;
		chart.background_color = background_color;
		chart.background_alpha = background_alpha;
		chart.border_enabled = border_enabled;
		chart.border_size = border_size;
		chart.border_color = border_color;
		chart.border_alpha = border_alpha;
		chart.urls = dots_urls;
		chart.urls_target = dots_urls_target;
		chart.sounds = dots_sounds;
		chart.show_arguments = show_arguments;
		chart.dots_size = dots_radius;
		chart.dots_image = dots_images;
		chart.block_background_enabled = background_enabled_block;
		chart.block_background_color = background_color_block;
		chart.block_background_alpha = background_alpha_block;
		chart.block_border_enabled = border_enabled_block;
		chart.block_border_size = border_size_block;
		chart.block_border_color = border_color_block;
		chart.block_border_alpha = border_alpha_block;
		chart.block_dots_radius = dots_radius;
		chart.block_dots_image = dots_images;
		chart.block_dots_type = dots_type;
		chart.names_rotations = names_rotations;
		chart.values_rotations = values_rotations;
		chart.names_dxs = names_dxs;
		chart.names_dys = names_dys;
		chart.values_dxs = values_dxs;
		chart.values_dys = values_dys;
	}
	//configure chart arguments
	private function config_arguments(node:XMLNode):Void
	{
		if (node.attributes.show!=undefined)
		{
			chart.default_show_arguments = lib.getBool(node.attributes.show);
		}
		if (node.attributes.prefix!=undefined)
		{
			chart.arguments_prefix = String(node.attributes.prefix);
		}
		if (node.attributes.postfix!=undefined)
		{
			chart.arguments_postfix = String(node.attributes.postfix);
		}
		if (node.attributes.decimal_places!=undefined)
		{
			chart.arguments_decimal_places = Number(node.attributes.decimal_places);
		}
		if (node.attributes.decimal_separator!=undefined)
		{
			chart.arguments_decimal_separator = String(node.attributes.decimal_separator);
		}
		if (node.attributes.thousand_separator!=undefined)
		{
			chart.arguments_thousand_separator = String(node.attributes.thousand_separator);
		}
		var num_font:Number = lib.findNode(node,'font');
		if (num_font!=undefined)
		{
			chart.arguments_text_format = lib.configTextFormat(node.childNodes[num_font]);
		}
	}
}