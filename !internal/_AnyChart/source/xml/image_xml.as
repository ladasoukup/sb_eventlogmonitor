﻿class xml.image_xml
{
	private var node:XMLNode;
	private var lib;
	//-----------------------------------------
	function image_xml(doc:XML)
	{
		lib = new xml.library();
		var num_obj:Number = lib.findNode(doc.firstChild,'objects');
		if (num_obj!=undefined)
		{
			node = doc.firstChild.childNodes[num_obj];
		}
	}
	//---------------------------------------------
	function get_images():Array
	{
		var res:Array = new Array();
		var num:Number = 0;
		var i:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='image')
			{
				res[num] = new objects.image(_root);
				res[num].source = String(node.childNodes[i].attributes.source);
				res[num].x = Number(node.childNodes[i].attributes.x);
				res[num].y = Number(node.childNodes[i].attributes.y);
				if (node.childNodes[i].attributes.width!=undefined)
				{
					res[num].width = Number(node.childNodes[i].attributes.width);
				}
				if (node.childNodes[i].attributes.height!=undefined)
				{
					res[num].height = Number(node.childNodes[i].attributes.height);
				}
				if (node.childNodes[i].attributes.position!=undefined)
				{
					res[num].position = String(node.childNodes[i].attributes.position);
				}
				if (node.childNodes[i].attributes.url!=undefined)
				{
					res[num].url = String(node.childNodes[i].attributes.url);
				}
				if (node.childNodes[i].attributes.url_target!=undefined)
				{
					res[num].url_target = String(node.childNodes[i].attributes.url_target);
				}
				res[num].show();
				num++;
			}
		}
		return res;
	}
}