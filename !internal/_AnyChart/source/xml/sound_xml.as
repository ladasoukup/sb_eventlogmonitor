﻿class xml.sound_xml
{
	private var node:XMLNode;
	private var lib;
	private var sounds:Array;
	//--------------------------------
	function sound_xml(doc:XML)
	{
		lib = new xml.library();
		var num_objects:Number = lib.findNode(doc.firstChild,'objects');
		if (num_objects!=undefined)
		{
			node = doc.firstChild.childNodes[num_objects];
			get_sounds();
		}
	}
	//-----------------------------------
	private function get_sounds():Void
	{
		sounds = new Array();
		var i:Number = 0;
		var num:Number = 0;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='sound')
			{
				sounds[num] = new objects.sound();
				if (node.childNodes[i].attributes.source!=undefined)
				{
					sounds[num].source = String(node.childNodes[i].attributes.source);
				}
				if (node.childNodes[i].attributes.start_second!=undefined)
				{
					sounds[num].start_second = Number(node.childNodes[i].attributes.start_second);
				}
				if (node.childNodes[i].attributes.loops!=undefined)
				{
					sounds[num].loops = Number(node.childNodes[i].attributes.loops);
				}
				sounds[num].start();
				num++;
			}
		}
	}
}