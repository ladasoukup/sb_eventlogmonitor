﻿class xml.legend_xml
{
	private var lib;
	private var legend;
	function legend_xml(doc:XML,obj)
	{
		legend = obj;
		lib = new xml.library();
		var num_type:Number = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{
			var num_legend:Number = lib.findNode(doc.firstChild.childNodes[num_type],'legend');
			if (num_legend!=undefined)
			{
				config_legend(doc.firstChild.childNodes[num_type].childNodes[num_legend]);
			}
		}
	}
	private function config_legend(node:XMLNode):Void
	{
		if (node.attributes.data_source!=undefined) {
			legend.dataSource = node.attributes.data_source;
		}
		if (node.attributes.x!=undefined)
		{
			legend.x = Number(node.attributes.x);
		}
		if (node.attributes.y!=undefined)
		{
			legend.y = Number(node.attributes.y);
		}
		if (node.attributes.enabled!=undefined)
		{
			legend.enabled = lib.getBool(node.attributes.enabled);
		}
		if (node.attributes.rows!=undefined)
		{
			legend.rows = Number(node.attributes.rows);
		}
		if (node.attributes.rows_auto_count!=undefined)
		{
			legend.rows_auto_count = lib.getBool(node.attributes.rows_auto_count);
		}
		var num_bg:Number = lib.findNode(node,'background');
		if (num_bg!=undefined)
		{
			if (node.childNodes[num_bg].attributes.enabled!=undefined)
			{
				legend.background_enabled = lib.getBool(node.childNodes[num_bg].attributes.enabled);
			}
			if (node.childNodes[num_bg].attributes.color!=undefined)
			{
				legend.background_color = lib.getColor(node.childNodes[num_bg].attributes.color);
			}
			if (node.childNodes[num_bg].attributes.alpha!=undefined)
			{
				legend.background_alpha = Number(node.childNodes[num_bg].attributes.alpha);
			}
		}
		var num_brd:Number = lib.findNode(node,'border');
		if (num_brd!=undefined)
		{
			if (node.childNodes[num_brd].attributes.enabled!=undefined)
			{
				legend.border_enabled = lib.getBool(node.childNodes[num_brd].attributes.enabled);
			}
			if (node.childNodes[num_brd].attributes.size!=undefined)
			{
				legend.border_size = Number(node.childNodes[num_brd].attributes.size);
			}
			if (node.childNodes[num_brd].attributes.color!=undefined)
			{
				legend.border_color = lib.getColor(node.childNodes[num_brd].attributes.color);
			}
			if (node.childNodes[num_brd].attributes.alpha!=undefined)
			{
				legend.border_alpha = Number(node.childNodes[num_brd].attributes.alpha);
			}
		}
		var num_names:Number = lib.findNode(node,'names');
		if (num_names!=undefined)
		{
			if (node.childNodes[num_names].attributes.enabled!=undefined)
			{
				legend.names_column_enabled = lib.getBool(node.childNodes[num_names].attributes.enabled);
			}
			if (node.childNodes[num_names].attributes.width!=undefined)
			{
				legend.names_column_width = Number(node.childNodes[num_names].attributes.width);
			}
			var num_text:Number = lib.findNode(node.childNodes[num_names],'font');
			if (num_text!=undefined)
			{
				legend.names_text_format = lib.configTextFormat(node.childNodes[num_names].childNodes[num_text]);
			}
		}
		var num_values:Number = lib.findNode(node,'values');
		if (num_values!=undefined)
		{
			if (node.childNodes[num_values].attributes.enabled!=undefined)
			{
				legend.values_column_enabled = lib.getBool(node.childNodes[num_values].attributes.enabled);
			}
			if (node.childNodes[num_values].attributes.width!=undefined)
			{
				legend.values_column_width = Number(node.childNodes[num_values].attributes.width);
			}
			var num_text:Number = lib.findNode(node.childNodes[num_values],'font');
			if (num_text!=undefined)
			{
				legend.values_text_format = lib.configTextFormat(node.childNodes[num_values].childNodes[num_text]);
			}
		}
		var num_colors:Number = lib.findNode(node,'colors');
		if (num_colors!=undefined)
		{
			if (node.childNodes[num_colors].attributes.width!=undefined)
			{
				legend.colors_column_width = Number(node.childNodes[num_colors].attributes.width);
			}
		}
		var num_header:Number = lib.findNode(node,'header');
		if (num_header!=undefined)
		{
			if (node.childNodes[num_header].attributes.enabled != undefined) 
				legend.header_enabled = lib.getBool(node.childNodes[num_header].attributes.enabled);
				
			if (node.childNodes[num_header].attributes.height!=undefined)
			{
				legend.header_height = Number(node.childNodes[num_header].attributes.height);
			}
			if (node.childNodes[num_header].attributes.names!=undefined)
			{
				legend.names_column_name = String(node.childNodes[num_header].attributes.names);
			}
			if (node.childNodes[num_header].attributes.values!=undefined)
			{
				legend.values_column_name = String(node.childNodes[num_header].attributes.values);
			}
			var num_text:Number = lib.findNode(node.childNodes[num_header],'font');
			if (num_text!=undefined)
			{
				legend.header_text_format = lib.configTextFormat(node.childNodes[num_header].childNodes[num_text]);
			}
			var num_bg:Number = lib.findNode(node.childNodes[num_header],'background');
			if (num_bg!=undefined)
			{
				if (node.childNodes[num_header].childNodes[num_bg].attributes.enabled!=undefined)
				{
					legend.header_background_enabled = lib.getBool(node.childNodes[num_header].childNodes[num_bg].attributes.enabled);
				}
				if (node.childNodes[num_header].childNodes[num_bg].attributes.color!=undefined)
				{
					legend.header_background_color = lib.getColor(node.childNodes[num_header].childNodes[num_bg].attributes.color);
				}
				if (node.childNodes[num_header].childNodes[num_bg].attributes.alpha!=undefined)
				{
					legend.header_background_alpha = Number(node.childNodes[num_header].childNodes[num_bg].attributes.alpha);
				}
			}
		}
		var num_scroll:Number = lib.findNode(node,'scroller');
		if (num_scroll!=undefined)
		{
			if (node.childNodes[num_scroll].attributes.enabled!=undefined)
			{
				legend.scroller.enabled = lib.getBool(node.childNodes[num_scroll].attributes.enabled);
			}
		}

	}
}