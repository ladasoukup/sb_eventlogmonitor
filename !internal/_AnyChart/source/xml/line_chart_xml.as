﻿class xml.line_chart_xml
{
	private var chart;
	private var lib;
	private var type:String;

	//------------------------------------
	function line_chart_xml(doc:XML,_chart,t:String)
	{
		if (t!=undefined)
		{
			type = t;
		}
		lib = new xml.library();
		chart = _chart;
		var num_type:Number = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{			
			var num_chart:Number;
			if (t==undefined)
			{
				num_chart = lib.findNode(doc.firstChild.childNodes[num_type],'chart');
			}else
			{
				var nums:Array = lib.findNode(doc.firstChild.childNodes[num_type],'chart','all');
				var i:Number;
				for (i=0;i<nums.length;i++)
				{
					if (doc.firstChild.childNodes[num_type].childNodes[nums[i]].attributes.type==t)
					{
						num_chart = nums[i];
						break;
					}
				}
			}
			if (num_chart!=undefined)
			{
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value!=undefined)
				{
					chart.maximum_value = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value!=undefined)
				{
					chart.minimum_value = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_argument!=undefined)
				{
					chart.maximum_argument = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_argument);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_argument!=undefined)
				{
					chart.minimum_argument = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_argument);
				}
				var num_hints:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'hints');
				if (num_hints!=undefined)
				{
					lib.config_hints(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_hints],chart);
				}
				var num_animation:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'animation');
				if (num_animation!=undefined)
				{
					lib.config_animation(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_animation],chart);
				}
				var num_names:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'names');
				if (num_names!=undefined)
				{
					lib.config_names(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_names],chart);
				}
				var num_values:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'values');
				if (num_values!=undefined)
				{
					lib.config_values(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_values],chart);
				}
				var num_arguments:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'arguments');
				if (num_arguments!=undefined)
				{
					config_arguments(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_arguments]);
				}
				var num_lines:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'line_chart');
				if (num_lines!=undefined)
				{
					config_chart(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_lines]);
				}				
			}
		}
		var num_data:Number = lib.findNode(doc.firstChild,'data');
		config_data(doc.firstChild.childNodes[num_data]);
	}
	//config chart
	private function config_chart(node:XMLNode):Void
	{
		var num_dots:Number = lib.findNode(node,'dots');
		if (num_dots!=undefined)
		{
			if (node.childNodes[num_dots].attributes.type!=undefined)
			{
				chart.default_dots_type = String(node.childNodes[num_dots].attributes.type);
			}
			if (node.childNodes[num_dots].attributes.image!=undefined)
			{
				chart.default_dots_image = String(node.childNodes[num_dots].attributes.image);
			}
			if (node.childNodes[num_dots].attributes.radius!=undefined)
			{
				chart.default_dots_radius = Number(node.childNodes[num_dots].attributes.radius);
			}
			var num_bg:Number = lib.findNode(node.childNodes[num_dots],'background');
			if (num_bg!=undefined)
			{
				if (node.childNodes[num_dots].childNodes[num_bg].attributes.enabled!=undefined)
				{
					chart.default_background_enabled = lib.getBool(node.childNodes[num_dots].childNodes[num_bg].attributes.enabled);
				}
				if (node.childNodes[num_dots].childNodes[num_bg].attributes.color!=undefined)
				{
					chart.default_background_color = lib.getColor(node.childNodes[num_dots].childNodes[num_bg].attributes.color);
				}
				if (node.childNodes[num_dots].childNodes[num_bg].attributes.alpha!=undefined)
				{
					chart.default_background_alpha = Number(node.childNodes[num_dots].childNodes[num_bg].attributes.alpha);
				}
				if (node.childNodes[num_dots].childNodes[num_bg].attributes.auto_color!=undefined)
				{
					chart.background_auto_color = lib.getBool(node.childNodes[num_dots].childNodes[num_bg].attributes.auto_color);					
				}
			}
			var num_brdr:Number = lib.findNode(node.childNodes[num_dots],'border');
			if (num_brdr!=undefined)
			{
				if (node.childNodes[num_dots].childNodes[num_brdr].attributes.enabled!=undefined)
				{
					chart.default_border_enabled = lib.getBool(node.childNodes[num_dots].childNodes[num_brdr].attributes.enabled);
				}
				if (node.childNodes[num_dots].childNodes[num_brdr].attributes.size!=undefined)
				{
					chart.default_border_size = Number(node.childNodes[num_dots].childNodes[num_brdr].attributes.size);
				}
				if (node.childNodes[num_dots].childNodes[num_brdr].attributes.color!=undefined)
				{
					chart.default_border_color = lib.getColor(node.childNodes[num_dots].childNodes[num_brdr].attributes.color);
				}
				if (node.childNodes[num_dots].childNodes[num_brdr].attributes.alpha!=undefined)
				{
					chart.default_border_alpha = Number(node.childNodes[num_dots].childNodes[num_brdr].attributes.alpha);
				}
			}
		}
		var num_lines:Number = lib.findNode(node,'lines');
		if (num_lines!=undefined)
		{
			if (node.childNodes[num_lines].attributes.size!=undefined)
			{
				chart.default_lines_size = Number(node.childNodes[num_lines].attributes.size);
			}
			if (node.childNodes[num_lines].attributes.color!=undefined)
			{
				chart.default_lines_color = lib.getColor(node.childNodes[num_lines].attributes.color);
			}
			if (node.childNodes[num_lines].attributes.tone!=undefined)
			{
				chart.lines_tone = lib.getColor(node.childNodes[num_lines].attributes.tone);
			}
			if (node.childNodes[num_lines].attributes.auto_color!=undefined)
			{
				chart.lines_auto_color = lib.getBool(node.childNodes[num_lines].attributes.auto_color);
			}
			if (node.childNodes[num_lines].attributes.alpha!=undefined)
			{
				chart.default_lines_alpha = Number(node.childNodes[num_lines].attributes.alpha);
			}
		}
		if (node.attributes.smoothing_enabled!=undefined)
		{
			chart.smoothing_enabled = lib.getBool(node.attributes.smoothing_enabled);
		}
		if (node.attributes.smoothing_step!=undefined)
		{
			chart.smoothing_step = Number(node.attributes.smoothing_step);
		}
		if (node.attributes.left_space!=undefined)
		{
			chart.x += Number(node.attributes.left_space);
			chart.width -= Number(node.attributes.left_space);
		}
		if (node.attributes.right_space!=undefined)
		{
			chart.width -= Number(node.attributes.right_space);
		}
		if (node.attributes.up_space!=undefined)
		{
			chart.y += Number(node.attributes.up_space);
			chart.height -= Number(node.attributes.up_space);
		}
		if (node.attributes.down_space!=undefined)
		{
			chart.height -= Number(node.attributes.up_space);
		}
		var num_bl:Number = lib.findNode(node,'block_names');
		if (num_bl!=undefined)
		{
			if (node.childNodes[num_bl].attributes.position!=undefined)
			{
				chart.lines_names_position = String(node.childNodes[num_bl].attributes.position);
			}
			if (node.childNodes[num_bl].attributes.enabled!=undefined)
			{
				chart.lines_show_names = lib.getBool(node.childNodes[num_bl].attributes.enabled);
			}
			var num_fnt:Number = lib.findNode(node.childNodes[num_bl],'font');
			if (num_fnt!=undefined)
			{
				chart.lines_names_text_format = lib.configTextFormat(node.childNodes[num_bl].childNodes[num_fnt]);
			}
		}
	}
	//config data
	private function config_data(node:XMLNode):Void
	{
		var i:Number;
		var j:Number;
		var n:Number = 0;
		var k:Number = 0;
		//------------------------
		var names:Array = new Array();
		var values:Array = new Array();
		var _arguments:Array = new Array();
		//-----------------------------
		var maxValue:Number;
		var minValue:Number;
		var maxArgument:Number;
		var minArgument:Number;
		//-------------------------------
		
		var hint_texts:Array = new Array();
		
		var block_names:Array = new Array();
		var block_show_names:Array = new Array();
		var block_colors:Array = new Array();
		var block_alphas:Array = new Array();
		var block_sizes:Array = new Array();
		
		var dots_type:Array = new Array();
		var background_enabled:Array = new Array();		
		var background_color:Array = new Array();
		var background_alpha:Array = new Array();
		var border_enabled:Array = new Array();
		var border_size:Array = new Array();
		var border_color:Array = new Array();
		var border_alpha:Array = new Array();
		var background_enabled_block:Array = new Array();
		var background_color_block:Array = new Array();
		var background_alpha_block:Array = new Array();
		var border_enabled_block:Array = new Array();
		var border_size_block:Array = new Array();
		var border_color_block:Array = new Array();
		var border_alpha_block:Array = new Array();
		var dots_radius:Array = new Array();
		var dots_radius_block:Array = new Array();
		var dots_images:Array = new Array();
		var dots_images_block:Array = new Array();
		var show_values_block:Array = new Array();
		var show_names_block:Array = new Array();
		var show_arguments:Array = new Array();
		var show_arguments_block:Array = new Array();
		var dots_urls:Array = new Array();
		var dots_urls_target:Array = new Array();
		var dots_sounds:Array = new Array();
		var names_rotations:Array = new Array();
		var values_rotations:Array = new Array();
		var names_dxs:Array = new Array();
		var values_dxs:Array = new Array();
		var names_dys:Array = new Array();
		var values_dys:Array = new Array();
		var show_names:Array = new Array();
		var show_values:Array = new Array();
		//------------------------
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				hint_texts[n] = new Array();
				background_enabled[n] = new Array();
				background_color[n] = new Array();
				background_alpha[n] = new Array();
				border_enabled[n] = new Array();
				border_size[n] = new Array();
				border_color[n] = new Array();
				border_alpha[n] = new Array();
				dots_radius[n] = new Array();
				show_arguments[n] = new Array();
				dots_images[n] = new Array();
				dots_urls[n] = new Array();
				dots_urls_target[n] = new Array();
				dots_sounds[n] = new Array();
				names_rotations[n] = new Array();
				values_rotations[n] = new Array();
				names_dxs[n] = new Array();
				values_dxs[n] = new Array();
				names_dys[n] = new Array();
				values_dys[n] = new Array();
				names[n] = new Array();
				values[n] = new Array();
				_arguments[n] = new Array();
				show_names[n] = new Array();
				show_values[n] = new Array();
				if (node.childNodes[i].attributes.dots_type!=undefined)
				{
					dots_type[n] = String(node.childNodes[i].attributes.dots_type);
				}
				if (node.childNodes[i].attributes.background_enabled!=undefined)
				{
					background_enabled_block[n] = lib.getBool(node.childNodes[i].attributes.background_enabled);
				}
				if (node.childNodes[i].attributes.color!=undefined)
				{
					block_colors[n] = lib.getColor(node.childNodes[i].attributes.color);
				}
				if (node.childNodes[i].attributes.alpha!=undefined)
				{
					block_alphas[n] = Number(node.childNodes[i].attributes.alpha);
				}
				if (node.childNodes[i].attributes.dots_background_alpha!=undefined)
				{
					background_alpha_block[n] = Number(node.childNodes[i].attributes.dots_background_alpha);
				}
				if (node.childNodes[i].attributes.dots_background_color!=undefined)
				{
					background_color_block[n] = lib.getColor(node.childNodes[i].attributes.dots_background_color);
				}
				if (node.childNodes[i].attributes.name!=undefined)
				{
					block_names[n] = String(node.childNodes[i].attributes.name);
				}
				if (node.childNodes[i].attributes.size!=undefined)
				{
					block_sizes[i] = Number(node.childNodes[i].attributes.size);
				}
				if (node.childNodes[i].attributes.border_enabled!=undefined)
				{
					border_enabled_block[n] = lib.getBool(node.childNodes[i].attributes.border_enabled);
				}
				if (node.childNodes[i].attributes.border_size!=undefined)
				{
					border_size_block[n] = Number(node.childNodes[i].attributes.border_size);
				}
				if (node.childNodes[i].attributes.border_alpha!=undefined)
				{
					border_alpha_block[n] = Number(node.childNodes[i].attributes.border_alpha);
				}
				if (node.childNodes[i].attributes.border_color!=undefined)
				{
					border_color_block[n] = lib.getColor(node.childNodes[i].attributes.border_color);
				}
				if (node.childNodes[i].attributes.dots_radius!=undefined)
				{
					dots_radius_block[n] = Number(node.childNodes[i].attributes.dots_radius);
				}
				if (node.childNodes[i].attributes.dots_image!=undefined)
				{
					dots_images_block[n] = String(node.childNodes[i].attributes.dots_image);
				}
				if (node.childNodes[i].attributes.show_names!=undefined)
				{
					show_names_block[n] = lib.getBool(node.childNodes[i].attributes.show_names);
				}
				if (node.childNodes[i].attributes.show_values!=undefined)
				{
					show_values_block[n] = lib.getBool(node.childNodes[i].attributes.show_values);
				}
				if (node.childNodes[i].attributes.show_arguments!=undefined)
				{
					show_arguments_block[n] = lib.getBool(node.childNodes[i].attributes.show_arguments);
				}
				if (node.childNodes[i].attributes.block_name_enabled!=undefined)
				{
					block_show_names[n] = lib.getBool(node.childNodes[i].attributes.block_name_enabled);
				}
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].attributes.hint_text != undefined) 
						hint_texts[n][k] = String(node.childNodes[i].childNodes[j].attributes.hint_text);
						
					if (node.childNodes[i].childNodes[j].attributes.name != undefined)
						names[n][k] = String(node.childNodes[i].childNodes[j].attributes.name);
						
					values[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value);
					_arguments[n][k] = Number(node.childNodes[i].childNodes[j].attributes.argument);
					
					if ((maxValue == undefined) || (values[n][k] > maxValue))
						maxValue = values[n][k];
					
					if ((minValue == undefined) || (values[n][k] < minValue))
						minValue = values[n][k];
					
					if ((maxArgument == undefined) || (_arguments[n][k] > maxArgument))
						maxArgument = _arguments[n][k];
						
					if ((minArgument == undefined) || (_arguments[n][k] < minArgument))
						minArgument = _arguments[n][k];
					
					if (node.childNodes[i].childNodes[j].attributes.show_name!=undefined)
						show_names[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.show_name);
					
					if (node.childNodes[i].childNodes[j].attributes.show_value!=undefined)
						show_values[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.show_value);
					
					if (node.childNodes[i].childNodes[j].attributes.radius!=undefined)
					{
						dots_radius[n][k] = Number(node.childNodes[i].childNodes[j].attributes.radius);
					}
					if (node.childNodes[i].childNodes[j].attributes.image!=undefined)
					{
						dots_images[n][k] = String(node.childNodes[i].childNodes[j].attributes.image);
					}
					if (node.childNodes[i].childNodes[j].attributes.show_argument!=undefined)
					{
						show_arguments[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.show_argument);
					}
					if (node.childNodes[i].childNodes[j].attributes.color!=undefined)
					{
						background_color[n][k] = lib.getColor(node.childNodes[i].childNodes[j].attributes.color);
					}
					if (node.childNodes[i].childNodes[j].attributes.alpha!=undefined)
					{
						background_alpha[n][k] = Number(node.childNodes[i].childNodes[j].attributes.alpha);
					}
					if (node.childNodes[i].childNodes[j].attributes.background_enabled!=undefined)
					{
						background_enabled[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.background_enabled);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_enabled!=undefined)
					{
						border_enabled[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.border_enabled);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_size!=undefined)
					{
						border_size[n][k] = Number(node.childNodes[i].childNodes[j].attributes.border_size);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_color!=undefined)
					{
						border_color[n][k] = lib.getColor(node.childNodes[i].childNodes[j].attributes.border_color);
					}
					if (node.childNodes[i].childNodes[j].attributes.border_alpha!=undefined)
					{
						border_alpha[n][k] = Number(node.childNodes[i].childNodes[j].attributes.border_alpha);
					}
					if (node.childNodes[i].childNodes[j].attributes.url!=undefined)
					{
						dots_urls[n][k] = String(node.childNodes[i].childNodes[j].attributes.url);
					}
					if (node.childNodes[i].childNodes[j].attributes.url_target!=undefined)
					{
						dots_urls_target[n][k] = String(node.childNodes[i].childNodes[j].attributes.url_target);
					}
					if (node.childNodes[i].childNodes[j].attributes.sound!=undefined)
					{
						dots_sounds[n][k] = String(node.childNodes[i].childNodes[j].attributes.sound);
					}
					if (node.childNodes[i].childNodes[j].attributes.name_rotation!=undefined)
					{
						names_rotations[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_rotation);
					}
					if (node.childNodes[i].childNodes[j].attributes.value_rotation!=undefined)
					{
						values_rotations[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_rotation);
					}
					if (node.childNodes[i].childNodes[j].attributes.name_x_offset!=undefined)
					{
						names_dxs[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_x_offset);
					}
					if (node.childNodes[i].childNodes[j].attributes.name_y_offset!=undefined)
					{
						names_dys[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_y_offset);
					}
					if (node.childNodes[i].childNodes[j].attributes.value_x_offset!=undefined)
					{
						values_dxs[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_x_offset);
					}
					if (node.childNodes[i].childNodes[j].attributes.value_y_offset!=undefined)
					{
						values_dys[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_y_offset);
					}
					k++;
				}
				n++;
			}
		}
		
		chart.show_names = show_names;
		chart.show_values = show_values;

		
		chart.names = names;
		chart.values = values;
		chart.arguments = _arguments;
		chart.calculatedMaximumValue = maxValue;
		chart.calculatedMinimumValue = minValue;
		chart.calculatedMaximumArgument = maxArgument;
		chart.calculatedMinimumArgument = minArgument;
			
			
		chart.hint_texts = hint_texts;
		chart.background_enabled = background_enabled;
		chart.background_color = background_color;
		chart.background_alpha = background_alpha;
		chart.border_enabled = border_enabled;
		chart.border_size = border_size;
		chart.border_color = border_color;
		chart.border_alpha = border_alpha;
		chart.urls = dots_urls;
		chart.urls_target = dots_urls_target;
		chart.sounds = dots_sounds;
		chart.show_arguments = show_arguments;
		chart.dots_size = dots_radius;
		chart.dots_image = dots_images;
		chart.block_background_enabled = background_enabled_block;
		chart.block_background_color = background_color_block;
		chart.block_background_alpha = background_alpha_block;
		chart.block_border_enabled = border_enabled_block;
		chart.block_border_size = border_size_block;
		chart.block_border_color = border_color_block;
		chart.block_border_alpha = border_alpha_block;
		chart.block_dots_radius = dots_radius_block;
		chart.block_dots_image = dots_images;
		chart.block_dots_type = dots_type;
		chart.lines_names = block_names;
		chart.lines_show_names = block_show_names;
		chart.lines_color = block_colors;
		chart.lines_size = block_sizes;
		chart.lines_alpha = block_alphas;
		chart.names_rotations = names_rotations;
		chart.values_rotations = values_rotations;
		chart.names_dxs = names_dxs;
		chart.names_dys = names_dys;
		chart.values_dxs = values_dxs;
		chart.values_dys = values_dys;
	}
	//configure chart arguments
	private function config_arguments(node:XMLNode):Void
	{
		if (node.attributes.show!=undefined)
		{
			chart.default_show_arguments = lib.getBool(node.attributes.show);
		}
		if (node.attributes.prefix!=undefined)
		{
			chart.arguments_prefix = String(node.attributes.prefix);
		}
		if (node.attributes.postfix!=undefined)
		{
			chart.arguments_postfix = String(node.attributes.postfix);
		}
		if (node.attributes.decimal_places!=undefined)
		{
			chart.arguments_decimal_places = Number(node.attributes.decimal_places);
		}
		if (node.attributes.decimal_separator!=undefined)
		{
			chart.arguments_decimal_separator = String(node.attributes.decimal_separator);
		}
		if (node.attributes.thousand_separator!=undefined)
		{
			chart.arguments_thousand_separator = String(node.attributes.thousand_separator);
		}
		var num_font:Number = lib.findNode(node,'font');
		if (num_font!=undefined)
		{
			chart.arguments_text_format = lib.configTextFormat(node.childNodes[num_font]);
		}
	}
}