﻿class xml.area_xml
{
	private var areas:XMLNode;
	private var lib;
	private var o:String;
	private var target_mc:MovieClip;
	
	//constructor
	function area_xml(doc:XML)
	{
		lib = new xml.library();
		var num_obj:Number = lib.findNode(doc.firstChild,'objects');
		areas = doc.firstChild.childNodes[num_obj];
	}
	//config areas
	function get_areas(orn:String,t_mc:MovieClip,t:String,chart_type:String):Array
	{
		o = orn;
		target_mc = t_mc;
		if (t==undefined)
		{
			t = 'values';
		}
		var res:Array = new Array();
		if (t=='values')
		{
			res = get_values_areas(chart_type);
		}
		if (t=='arguments')
		{
			res = get_arguments_areas(chart_type);
		}
		return res;
	}
	private function get_values_areas(chart_type:String):Array
	{
		var i:Number;
		var res:Array = new Array();
		var num:Number = 0;
		for (i=0;i<areas.childNodes.length;i++)
		{
			if (areas.childNodes[i].nodeName=='area')
			{
				if ((areas.childNodes[i].attributes.chart_type==chart_type) or (chart_type==undefined))
				{
					if ((areas.childNodes[i].attributes.first_value!=undefined) and (areas.childNodes[i].attributes.second_value!=undefined))
					{
						res[num] = new objects.area(target_mc,o);				
						res[num].first_value = String(areas.childNodes[i].attributes.first_value);
						res[num].second_value = String(areas.childNodes[i].attributes.second_value);
						if (areas.childNodes[i].attributes.first_text!=undefined)
						{
							res[num].first_text = String(areas.childNodes[i].attributes.first_text);
						}
						if (areas.childNodes[i].attributes.second_text!=undefined)
						{
							res[num].second_text = String(areas.childNodes[i].attributes.second_text);
						}
						if (areas.childNodes[i].attributes.text_position!=undefined)
						{
							res[num].text_position = String(areas.childNodes[i].attributes.text_position);
						}
						if (areas.childNodes[i].attributes.text_rotation!=undefined)
						{
							res[num].text_rotation = Number(areas.childNodes[i].attributes.text_rotation);
						}
						if (areas.childNodes[i].attributes.text_x_offset!=undefined)
						{
							res[num].text_x_offset = Number(areas.childNodes[i].attributes.text_x_offset);
						}
						if (areas.childNodes[i].attributes.text_y_offset!=undefined)
						{
							res[num].text_y_offset = Number(areas.childNodes[i].attributes.text_y_offset);
						}
						var num_lines:Number = lib.findNode(areas.childNodes[i],'lines');
						if (num_lines!=undefined)
						{
							if (areas.childNodes[i].childNodes[num_lines].attributes.size!=undefined)
							{
								res[num].size = Number(areas.childNodes[i].childNodes[num_lines].attributes.size);
							}
							if (areas.childNodes[i].childNodes[num_lines].attributes.enabled!=undefined)
							{
								res[num].lines_enabled = lib.getBool(areas.childNodes[i].childNodes[num_lines].attributes.enabled);
							}
							if (areas.childNodes[i].childNodes[num_lines].attributes.color!=undefined)
							{
								res[num].color = lib.getColor(areas.childNodes[i].childNodes[num_lines].attributes.color);
							}
							if (areas.childNodes[i].childNodes[num_lines].attributes.alpha!=undefined)
							{
								res[num].alpha = Number(areas.childNodes[i].childNodes[num_lines].attributes.alpha);
							}
						}
						var num_bg:Number = lib.findNode(areas.childNodes[i],'background');
						if (num_bg!=undefined)
						{
							if (areas.childNodes[i].childNodes[num_bg].attributes.enabled!=undefined)
							{
								res[num].background_enabled = lib.getBool(areas.childNodes[i].childNodes[num_bg].attributes.enabled);
							}
							if (areas.childNodes[i].childNodes[num_bg].attributes.color!=undefined)
							{
								res[num].background_color = lib.getColor(areas.childNodes[i].childNodes[num_bg].attributes.color);
							}
							if (areas.childNodes[i].childNodes[num_bg].attributes.alpha!=undefined)
							{
								res[num].background_alpha = Number(areas.childNodes[i].childNodes[num_bg].attributes.alpha);
							}
						}
						var num_text:Number = lib.findNode(areas.childNodes[i],'font');
						if (num_text!=undefined)
						{
							res[num].text_format = lib.configTextFormat(areas.childNodes[i].childNodes[num_text]);
						}
						num++;
					}
				}
			}
		}
		return res;
	}
	private function get_arguments_areas(chart_type:String):Array
	{
		var i:Number;
		var res:Array = new Array();
		var num:Number = 0;
		for (i=0;i<areas.childNodes.length;i++)
		{
			if (areas.childNodes[i].nodeName=='area')
			{
				if ((areas.childNodes[i].attributes.chart_type==chart_type) or (chart_type==undefined))
				{
					if ((areas.childNodes[i].attributes.first_argument!=undefined) and (areas.childNodes[i].attributes.second_argument!=undefined))
					{
						res[num] = new objects.area(target_mc,'vertical');
						res[num].first_value = String(areas.childNodes[i].attributes.first_argument);
						res[num].second_value = String(areas.childNodes[i].attributes.second_argument);
						if (areas.childNodes[i].attributes.first_text!=undefined)
						{
							res[num].first_text = String(areas.childNodes[i].attributes.first_text);
						}
						if (areas.childNodes[i].attributes.second_text!=undefined)
						{
							res[num].second_text = String(areas.childNodes[i].attributes.second_text);
						}
						if (areas.childNodes[i].attributes.text_position!=undefined)
						{
							res[num].text_position = String(areas.childNodes[i].attributes.text_position);
						}
						if (areas.childNodes[i].attributes.text_rotation!=undefined)
						{
							res[num].text_rotation = Number(areas.childNodes[i].attributes.text_rotation);
						}
						if (areas.childNodes[i].attributes.text_x_offset!=undefined)
						{
							res[num].text_x_offset = Number(areas.childNodes[i].attributes.text_x_offset);
						}
						if (areas.childNodes[i].attributes.text_y_offset!=undefined)
						{
							res[num].text_y_offset = Number(areas.childNodes[i].attributes.text_y_offset);
						}
						var num_lines:Number = lib.findNode(areas.childNodes[i],'lines');
						if (num_lines!=undefined)
						{
							if (areas.childNodes[i].childNodes[num_lines].attributes.size!=undefined)
							{
								res[num].size = Number(areas.childNodes[i].childNodes[num_lines].attributes.size);
							}
							if (areas.childNodes[i].childNodes[num_lines].attributes.enabled!=undefined)
							{
								res[num].lines_enabled = lib.getBool(areas.childNodes[i].childNodes[num_lines].attributes.enabled);
							}
							if (areas.childNodes[i].childNodes[num_lines].attributes.color!=undefined)
							{
								res[num].color = lib.getColor(areas.childNodes[i].childNodes[num_lines].attributes.color);
							}
							if (areas.childNodes[i].childNodes[num_lines].attributes.alpha!=undefined)
							{
								res[num].alpha = Number(areas.childNodes[i].childNodes[num_lines].attributes.alpha);
							}
						}
						var num_bg:Number = lib.findNode(areas.childNodes[i],'background');
						if (num_bg!=undefined)
						{
							if (areas.childNodes[i].childNodes[num_bg].attributes.enabled!=undefined)
							{
								res[num].background_enabled = lib.getBool(areas.childNodes[i].childNodes[num_bg].attributes.enabled);
							}
							if (areas.childNodes[i].childNodes[num_bg].attributes.color!=undefined)
							{
								res[num].background_color = lib.getColor(areas.childNodes[i].childNodes[num_bg].attributes.color);
							}
							if (areas.childNodes[i].childNodes[num_bg].attributes.alpha!=undefined)
							{
								res[num].background_alpha = Number(areas.childNodes[i].childNodes[num_bg].attributes.alpha);
							}
						}
						var num_text:Number = lib.findNode(areas.childNodes[i],'font');
						if (num_text!=undefined)
						{
							res[num].text_format = lib.configTextFormat(areas.childNodes[i].childNodes[num_text]);
						}
						num++;
					}
				}
			}
		}
		return res;
	}
}