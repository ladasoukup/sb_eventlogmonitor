﻿class xml.trend_xml
{
	private var lib;
	private var xml_obj:XMLNode;
	private var o:String;
	private var target_mc:MovieClip;
	
	function trend_xml(doc:XML)
	{
		var i:Number;
		lib = new xml.library();
		xml_obj = doc.firstChild;
	}
	
	function get_lines(ort:String,t_mc:MovieClip,t:String,chart_type:String):Array
	{
		o = ort;
		target_mc = t_mc;
		var res:Array = new Array();
		if (t==undefined)
		{
			t='values';
		}
		if (t=='values')
		{
			res = get_values_lines(chart_type);
		}
		if (t=='arguments')
		{
			res = get_arguments_lines(chart_type);
		}
		return res;
	}	
	private function get_values_lines(chart_type:String):Array
	{
		var res = new Array();
		var i:Number;
		var num_objects:Number = lib.findNode(xml_obj,'objects');
		var num:Number = 0;
		if (num_objects!=undefined)
		{
			for (i=0;i<xml_obj.childNodes[num_objects].childNodes.length;i++)
			{
				if (xml_obj.childNodes[num_objects].childNodes[i].nodeName=='trend')
				{
					if ((xml_obj.childNodes[num_objects].childNodes[i].attributes.chart_type==chart_type) or (chart_type==undefined))
					{
						if ((xml_obj.childNodes[num_objects].childNodes[i].attributes.start_value!=undefined) and (xml_obj.childNodes[num_objects].childNodes[i].attributes.end_value!=undefined))
						{
							res[num] = new objects.trend(target_mc,o);					
							res[num].start_value = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.start_value);
							res[num].end_value = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.end_value);
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.size!=undefined)
							{
								res[num].size = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.size);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.color!=undefined)
							{
								res[num].color = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].attributes.color);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha!=undefined)
							{
								res[num].alpha = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text!=undefined)
							{
								res[num].text = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position!=undefined)
							{
								res[num].text_position = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation!=undefined)
							{
								res[num].text_rotation = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset!=undefined)
							{
								res[num].text_dx = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset!=undefined)
							{
								res[num].text_dy = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset);
							}
							var num_text:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i],'font');
							if (num_text!=undefined)
							{
								res[num].text_format = lib.configTextFormat(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_text]);
							}
							num++;
						}
					}
				}
			}
		}
		return res;
	}
	private function get_arguments_lines(chart_type:String):Array
	{
		var res = new Array();
		var i:Number;
		var num_objects:Number = lib.findNode(xml_obj,'objects');
		var num:Number = 0;
		if (num_objects!=undefined)
		{
			for (i=0;i<xml_obj.childNodes[num_objects].childNodes.length;i++)
			{
				if (xml_obj.childNodes[num_objects].childNodes[i].nodeName=='trend')
				{
					if ((xml_obj.childNodes[num_objects].childNodes[i].attributes.chart_type==chart_type) or (chart_type==undefined))
					{
						if ((xml_obj.childNodes[num_objects].childNodes[i].attributes.start_argument!=undefined) and (xml_obj.childNodes[num_objects].childNodes[i].attributes.end_argument!=undefined))
						{
							res[num] = new objects.trend(target_mc,'vertical');					
							res[num].start_value = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.start_argument);
							res[num].end_value = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.end_argument);
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.size!=undefined)
							{
								res[num].size = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.size);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.color!=undefined)
							{
								res[num].color = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].attributes.color);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha!=undefined)
							{
								res[num].alpha = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text!=undefined)
							{
								res[num].text = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position!=undefined)
							{
								res[num].text_position = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation!=undefined)
							{
								res[num].text_rotation = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset!=undefined)
							{
								res[num].text_dx = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset!=undefined)
							{
								res[num].text_dy = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset);
							}
							var num_text:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i],'font');
							if (num_text!=undefined)
							{
								res[num].text_format = lib.configTextFormat(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_text]);
							}
							num++;
						}
					}
				}
			}
		}
		return res;
	}
}