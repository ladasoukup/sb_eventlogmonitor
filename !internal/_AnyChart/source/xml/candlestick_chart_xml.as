﻿class xml.candlestick_chart_xml
{
	private var chart;
	private var lib;
	//-----------------------------------
	function candlestick_chart_xml(doc:XML,_chart)
	{
		chart = _chart;
		lib = new xml.library();
		var num_type = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{
			var num_chart:Number = lib.findNode(doc.firstChild.childNodes[num_type],'chart');
			if (num_chart!=undefined)
			{
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value!=undefined)
				{
					chart.grid_maximum = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value!=undefined)
				{
					chart.grid_minimum = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_index!=undefined)
				{
					chart.index_grid_maximum = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_index);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_index!=undefined)
				{
					chart.index_grid_minimum = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_index);
				}


				var num_c:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'candlestick_chart');
				if (num_c!=undefined)
				{
					config_chart(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_c]);
				}
				var num_hints:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'hints');
				if (num_hints!=undefined)
				{
					lib.config_hints(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_hints],chart);
				}
				var num_values:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'values');
				if (num_values!=undefined)
				{
					lib.config_values(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_values],chart);
				}
				var num_names:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'names');
				if (num_names != undefined) {
					lib.config_names(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_names],chart);
				}
			}
		}
		var num_data:Number = lib.findNode(doc.firstChild,'data');
		if (num_data!=undefined)
		{
			config_data(doc.firstChild.childNodes[num_data]);
		}
	}
	//------------------------------------------------------
	private function config_chart(node:XMLNode):Void
	{		
		if (node.attributes.left_space!=undefined)
		{
			chart.x += Number(node.attributes.left_space);
			chart.width -= Number(node.attributes.left_space);
		}
		if (node.attributes.right_space!=undefined)
		{
			chart.width -= Number(node.attributes.right_space);
		}
		if (node.attributes.up_space!=undefined)
		{
			chart.y += Number(node.attributes.up_space);
			chart.height -= Number(node.attributes.up_space);
		}
		if (node.attributes.down_space!=undefined)
		{
			chart.height -= Number(node.attributes.down_space);
		}
		if (node.attributes.candlestick_space != undefined) {
			chart.candlestick_space = Number(node.attributes.candlestick_space);
		}
		var num_bears:Number = lib.findNode(node,'bears');
		if (num_bears!=undefined)
		{
			var num_bg:Number = lib.findNode(node.childNodes[num_bears],'background');
			if (num_bg!=undefined)
			{
				if (node.childNodes[num_bears].childNodes[num_bg].attributes.enabled!=undefined)
				{
					chart.bears_background_enabled = lib.getBool(node.childNodes[num_bears].childNodes[num_bg].attributes.enabled);
				}
				if (node.childNodes[num_bears].childNodes[num_bg].attributes.color!=undefined)
				{					
					chart.bears_background_color = lib.getColor(node.childNodes[num_bears].childNodes[num_bg].attributes.color);
				}
				if (node.childNodes[num_bears].childNodes[num_bg].attributes.alpha!=undefined)
				{
					chart.bears_background_alpha = Number(node.childNodes[num_bears].childNodes[num_bg].attributes.alpha);
				}
			}
			var num_br:Number = lib.findNode(node.childNodes[num_bears],'border');
			if (num_br!=undefined)
			{
				if (node.childNodes[num_bears].childNodes[num_br].attributes.enabled!=undefined)
				{
					chart.bears_border_enabled = lib.getBool(node.childNodes[num_bears].childNodes[num_br].attributes.enabled);
				}
				if (node.childNodes[num_bears].childNodes[num_br].attributes.size!=undefined)
				{
					chart.bears_border_size = Number(node.childNodes[num_bears].childNodes[num_br].attributes.size);
				}
				if (node.childNodes[num_bears].childNodes[num_br].attributes.color!=undefined)
				{
					chart.bears_border_color = lib.getColor(node.childNodes[num_bears].childNodes[num_br].attributes.color);
				}
				if (node.childNodes[num_bears].childNodes[num_br].attributes.alpha!=undefined)
				{
					chart.bears_border_alpha = Number(node.childNodes[num_bears].childNodes[num_br].attributes.alpha);
				}
			}
		}
		var num_bulls:Number = lib.findNode(node,'bulls');
		if (num_bulls!=undefined)
		{
			var num_bg:Number = lib.findNode(node.childNodes[num_bulls],'background');
			if (num_bg!=undefined)
			{
				if (node.childNodes[num_bulls].childNodes[num_bg].attributes.enabled!=undefined)
				{
					chart.bulls_background_enabled = lib.getBool(node.childNodes[num_bulls].childNodes[num_bg].attributes.enabled);
				}
				if (node.childNodes[num_bulls].childNodes[num_bg].attributes.color!=undefined)
				{
					chart.bulls_background_color = lib.getColor(node.childNodes[num_bulls].childNodes[num_bg].attributes.color);
				}
				if (node.childNodes[num_bulls].childNodes[num_bg].attributes.alpha!=undefined)
				{
					chart.bulls_background_alpha = Number(node.childNodes[num_bulls].childNodes[num_bg].attributes.alhpa);
				}
			}
			var num_br:Number = lib.findNode(node.childNodes[num_bulls],'border');
			if (num_br!=undefined)
			{
				if (node.childNodes[num_bulls].childNodes[num_br].attributes.enabled!=undefined)
				{
					chart.bulls_border_enabled = lib.getBool(node.childNodes[num_bulls].childNodes[num_br].attributes.enabled);
				}
				if (node.childNodes[num_bulls].childNodes[num_br].attributes.size!=undefined)
				{
					chart.bulls_border_size = Number(node.childNodes[num_bulls].childNodes[num_br].attributes.size);
				}
				if (node.childNodes[num_bulls].childNodes[num_br].attributes.color!=undefined)
				{
					chart.bulls_border_color = lib.getColor(node.childNodes[num_bulls].childNodes[num_br].attributes.color);
				}
				if (node.childNodes[num_bulls].childNodes[num_br].attributes.alpha!=undefined)
				{
					chart.bulls_border_alpha = Number(node.childNodes[num_bulls].childNodes[num_br].attributes.alpha);
				}
			}
		}
	}
	//config chart data
	private function config_data(node:XMLNode):Void
	{
		var background_enabled:Array = new Array();
		var background_color:Array = new Array();
		var background_alpha:Array = new Array();
		var border_enabled:Array = new Array();
		var border_size:Array = new Array();
		var border_color:Array = new Array();
		var border_alpha:Array = new Array();
		var show_names:Array = new Array();
		
		var i:Number;
		var n:Number = 0;
		var tmp_node:XMLNode;
		
		var num_data:Number = lib.findNode(node,'block');
		if (num_data!=undefined)
		{
			for (i=0;i<node.childNodes[num_data].childNodes.length;i++)
			{
				if (node.childNodes[num_data].childNodes[i].nodeName=='set')
				{
					tmp_node = node.childNodes[num_data].childNodes[i];
					if (tmp_node.attributes.background_enabled!=undefined)
					{
						background_enabled[n] = lib.getBool(tmp_node.attributes.background_enabled);
					}
					if (tmp_node.attributes.color!=undefined)
					{
						background_color[n] = lib.getColor(tmp_node.attributes.color);
					}
					if (tmp_node.attributes.alpha!=undefined)
					{
						background_alpha[n] = Number(tmp_node.attributes.alpha);
					}
					if (tmp_node.attributes.border_enabled!=undefined)
					{
						border_enabled[n] = lib.getBool(tmp_node.attributes.border_enabled);
					}
					if (tmp_node.attributes.border_size!=undefined)
					{
						border_size[n] = Number(tmp_node.attributes.border_size);
					}
					if (tmp_node.attributes.border_color!=undefined)
					{
						border_color[n] = lib.getColor(tmp_node.attributes.border_color);
					}
					if (tmp_node.attributes.border_alpha!=undefined)
					{
						border_alpha[n] = Number(tmp_node.attributes.border_alpha);
					}
					if (tmp_node.attributes.show_name != undefined)
					{
						show_names[n] = lib.getBool(tmp_node.attributes.show_name);
					}
					n++;
				}
			}
		}
		chart.background_enabled = background_enabled;
		chart.background_color = background_color;
		chart.background_alpha = background_alpha;
		chart.border_enabled = border_enabled;
		chart.border_size = border_size;
		chart.border_color = border_color;
		chart.border_alpha = border_alpha;
		chart.show_names = show_names;
	}
}