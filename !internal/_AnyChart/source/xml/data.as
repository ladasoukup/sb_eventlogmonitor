﻿class xml.data
{
	private var node:XMLNode;
	private var lib;
	private var type:String;
	//--------------------------
	
	function data(doc:XML,t:String)
	{		
		if (t!=undefined)
		{
			type = t;
		}
		lib = new xml.library();
		var num_data:Number = lib.findNode(doc.firstChild,'data');
		if (num_data!=undefined)
		{
			node = doc.firstChild.childNodes[num_data];
		}		
	}
	//---------------------------
	//get values
	function get_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value);
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get arguments
	function get_arguments():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.argument);
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get open values
	function get_open_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.open_value);
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get close values
	function get_close_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.close_value);
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get names
	function get_names():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.name!=undefined)
						{
							res[n][k] = String(node.childNodes[i].childNodes[j].attributes.name);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get colors
	function get_colors():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.color!=undefined)
						{
							res[n][k] = lib.getColor(node.childNodes[i].childNodes[j].attributes.color);
							trace (res[n][k]);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get show_value
	function get_show_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.show_value!=undefined)
						{
							res[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.show_value);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get show names
	function get_show_names():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.show_name!=undefined)
						{
							res[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.show_name);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}	
	//candlestick chart
	function get_c_open_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.open!=undefined)
						{
							res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.open);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//close
	function get_c_close_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.close!=undefined)
						{
							res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.close);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get high values
	function get_c_high_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.high!=undefined)
						{
							res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.high);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//low
	function get_c_low_values():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.low!=undefined)
						{
							res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.low);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
	//get indexes
	function get_indexes():Array
	{
		var i:Number;
		var j:Number;
		var res:Array = new Array();
		var n:Number = 0;
		var k:Number;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='block')
			{
				res[n] = new Array();
				k = 0;
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						if (node.childNodes[i].childNodes[j].attributes.index!=undefined)
						{
							res[n][k] = Number(node.childNodes[i].childNodes[j].attributes.index);
						}
						k++;
					}
				}
				n++;
			}
		}
		return res;
	}
}