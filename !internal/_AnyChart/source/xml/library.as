﻿class xml.library
{
	function library()
	{
	}
	//----------------------
	function findNode(node:XMLNode,nodeName:String,ft:String)
	{
		if (ft==undefined)
		{
			ft = 'single';
		}
		var i:Number;
		switch (ft)
		{
			case 'single':
			  var res:Number;
			break;
			case 'all':
			  var res:Array = new Array();
			  var num:Number = 0;
			break;
		}
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName==nodeName)
			{
				if (ft=='single')
				{
					res = i;
					break;
				}else
				{
					res[num] = i;
					num++;
				}
			}
		}
		if (res!=undefined)
		{
			return res;
		}else
		{
			return undefined;
		}
	}
	//-------
	function configChartSetsBackgrounds(chart:Object,node:XMLNode,defaultType:String):Void {
		chart.background_type = (node.attributes.type != undefined) ? (node.attributes.type) : 'solid';
		chart.background_gradient_type = (node.attributes.gradient_type != undefined) ? (node.attributes.gradient_type) : defaultType;
		chart.background_gradient_rotation = Number(node.attributes.rotation != undefined) ? Number(node.attributes.rotation) : 0;
			
		var numColors:Number = findNode(node,'colors')
		chart.background_gradient_colors = new Array();
		for (var j:Number = 0;j<node.childNodes[numColors].childNodes.length;j++) {
			chart.background_gradient_colors[j] = Number(node.childNodes[numColors].childNodes[j].firstChild.nodeValue);
		}
			
		var numRatios:Number = findNode(node,'ratios')
		chart.background_gradient_ratios = new Array();
		for (var j:Number = 0;j<node.childNodes[numRatios].childNodes.length;j++) {
			chart.background_gradient_ratios[j] = Number(node.childNodes[numRatios].childNodes[j].firstChild.nodeValue);
		}
			
		var numAlphas:Number = findNode(node,'alphas')
		chart.background_gradient_alphas = new Array();
		for (var j:Number = 0;j<node.childNodes[numAlphas].childNodes.length;j++) {
			chart.background_gradient_alphas[j] = Number(node.childNodes[numAlphas].childNodes[j].firstChild.nodeValue);
		}
	}
	//-------
	function getBool(attr:String):Boolean
	{
		var res:Boolean;
		if (attr=='yes')
		{
			res = true;
		}else
		{
			if (attr=='no')
			{
				res = false;
			}else
			{
				return undefined;
			}
		}
		return res;
	}
	//get color
	function getColor(colr):Number	{
		var res:Number;
		if (colr.indexOf('0x')!=-1)
			return Number(colr);
		switch (colr.toLowerCase())
		{
			case 'aliceblue':
			  res = 0xf0f8ff;
			break;
			case 'antiquewhite':
			  res = 0xfaebd7;
			break;		
			case 'aqua':
			  res = 0x00ffff;
			break;
			case 'aquamarine':
			  res = 0x7fffd4;
			break;
			case 'azure':
			  res = 0xf0ffff;
			break;
			case 'beige':
			  res = 0xf5f5dc;
			break;
			case 'bisque':
			  res = 0xffe4c4;
			break;
			case 'black':
			  res = 0x000000;
			break;
			case 'blanchedalmond':
			  res = 0xffebcd;
			break;
			case 'blue':
			  res = 0x0000ff;
			break;
			case 'blueviolet':
			  res = 0x8a2be2;
			break;
			case 'brown':
			  res = 0xa52a2a;
			break;
			case 'burlywood':
			  res = 0xdeb887;
			break;
			case 'cadetblue':
			  res = 0x5f9ea0;
			break;
			case 'chartreuse':
			  res = 0x7fff00;
			break;
			case 'chocolate':
			  res = 0xd2691e;
			break;
			case 'coral':
			  res = 0xff7f50;
			break;
			case 'cornflowerblue':
			  res = 0x6495ed;
			break;
			case 'cornsilk':
			  res = 0xfff8dc;
			break;
			case 'crimson':
			  res = 0xdc143c;
			break;
			case 'cyan':
			  res = 0x00ffff;
			break;
			case 'darkblue':
			  res = 0x00008b;
			break;
			case 'darkcyan':
			  res = 0x008b8b;
			break;
			case 'darkgoldenrod':
			  res = 0xb8860b;
			break;
			case 'darkgray':
			  res = 0xa9a9a9;
			break;
			case 'darkgreen':
			  res = 0x006400;
			break;
			case 'darkkhaki':
			  res = 0xbdb76b;
			break;
			case 'darkmagenta':
			  res = 0x8b008b;
			break;
			case 'darkolivegreen':
			  res = 0x556b2f;
			break;
			case 'darkorange':
			  res = 0xff8c00;
			break;
			case 'darkorchid':
			  res = 0x9932cc;
			break;
			case 'darkred':
			  res = 0x8b0000;
			break;
			case 'darksalmon':
			  res = 0xe9967a;
			break;
			case 'darkseagreen':
			  res = 0x8fbc8f ;
			break;
			case 'darkslateblue':
			  res = 0x483d8b;
			break;
			case 'darkslategray':
			  res = 0x2f4f4f;
			break;
			case 'darkturquoise':
			  res = 0x00ced1;
			break;
			case 'darkviolet':
			  res = 0x9400d3;
			break;
			case 'deeppink':
			  res = 0xff1493;
			break;
			case 'deepskyvlue':
			  res = 0x00bfff;
			break;
			case 'dimgray':
			  res = 0x696969;
			break;
			case 'dodgerblue':
			  res = 0x1e90ff;
			break;
			case 'feldspar':
			  res = 0xd19275;
			break;
			case 'firebrick':
			  res = 0xb22222;
			break;
			case 'floralwhite':
			  res = 0xfffaf0;
			break;
			case 'forestgreen':
			  res = 0x228b22;
			break;
			case 'fuchsia':
			  res = 0xff00ff;
			break;
			case 'gainsboro':
			  res = 0xdcdcdc;
			break;
			case 'ghostwhite':
			  res = 0xf8f8ff;
			break;
			case 'gold':
			  res = 0xffd700;
			break;
			case 'goldenrod':
			  res = 0xdaa520;
			break;
			case 'gray':
			  res = 0x808080;
			break;
			case 'green':
			  res = 0x008000;
			break;
			case 'greenyellow':
			  res = 0xadff2f;
			break;
			case 'honeydew':
			  res = 0xf0fff0;
			break;
			case 'hotpink':
			  res = 0xff69b4;
			break;
			case 'indianred':
			  res = 0xcd5c5c;
			break;
			case 'indigo':
			  res = 0x4b0082;
			break;
			case 'ivory':
			  res = 0xfffff0;
			break;
			case 'khaki':
			  res = 0xf0e68c;
			break;
			case 'lavender':
			  res = 0xe6e6fa;
			break;
			case 'lavenderblush':
			  res = 0xfff0f5;
			break;
			case 'lawngreen':
			  res = 0x7cfc00;
			break;
			case 'lemonchiffon':
			  res = 0xfffacd;
			break;
			case 'lightblue':
			  res = 0xadd8e6;
			break;
			case 'lightcoral':
			  res = 0xf08080;
			break;
			case 'lightcyan':
			  res = 0xe0ffff;
			break;
			case 'lightgoldenrodyellow':
			  res = 0xfafad2;
			break;
			case 'lightgrey':
			  res = 0xd3d3d3;
			break;
			case 'lightgreen':
			  res = 0x90ee90;
			break;
			case 'lightpink ':
			  res = 0xffb6c1;
			break;
			case 'lightsalmon':
			  res = 0xffa07a;
			break;
			case 'lightseagreen':
			  res = 0x20b2aa;
			break;

			case 'lightskyblue':
			  res = 0x87cefa;
			break;
			case 'lightslateblue':
			  res = 0x8470ff;
			break;
			case 'lightslategray':
			  res = 0x778899;
			break;
			case 'lightsteelblue':
			  res = 0xb0c4de;
			break;
			case 'lightyellow':
			  res = 0xffffe0;
			break;
			case 'lime':
			  res = 0x00ff00;
			break;
			case 'limegreen':
			  res = 0x32cd32;
			break;
			case 'linen':
			  res = 0xfaf0e6;
			break;
			case 'magenta':
			  res = 0xff00ff;
			break;
			case 'maroon':
			  res = 0x800000;
			break;
			case 'mediumaquamarine':
			  res = 0x66cdaa;
			break;
			case 'mediumblue':
			  res = 0x0000cd;
			break;
			case 'mediumorchid':
			  res = 0xba55d3;
			break;
			case 'mediumpurple':
			  res = 0x9370d8;
			break;
			case 'mediumseagreen':
			  res = 0x3cb371;
			break;
			case 'mediumslateblue':
			  res = 0x7b68ee ;
			break;
			case 'mediumspringgreen':
			  res = 0x00fa9a;
			break;
			case 'mediumturquoise':
			  res = 0x48d1cc;
			break;
			case 'mediumvioletred':
			  res = 0xc71585;
			break;
			case 'midnightblue':
			  res = 0x191970;
			break;
			case 'mintcream':
			  res = 0xf5fffa;
			break;
			case 'mistyrose':
			  res = 0xffe4e1;
			break;
			case 'moccasin':
			  res = 0xffe4b5;
			break;
			case 'navajowhite':
			  res = 0xffdead;
			break;
			case 'navy':
			  res = 0x000080;
			break;
			case 'oldlace':
			  res = 0xfdf5e6;
			break;
			case 'olive':
			  res = 0x808000;
			break;
			case 'olivedrab':
			  res = 0x6b8e23;
			break;
			case 'orange':
			  res = 0xffa500;
			break;
			case 'orangered':
			  res = 0xff4500;
			break;
			case 'orchid':
			  res = 0xda70d6;
			break;
			case 'palegoldenrod':
			  res = 0xeee8aa;
			break;
			case 'palegreen':
			  res = 0x98fb98;
			break;
			case 'paleturquoise':
			  res = 0xafeeee;
			break;
			case 'palevioletred':
			  res = 0xd87093;
			break;
			case 'papayawhip':
			  res = 0xffefd5;
			break;
			case 'peachpuff':
			  res = 0xffdab9;
			break;
			case 'peru':
			  res = 0xcd853f;
			break;
			case 'pink':
			  res = 0xffc0cb;
			break;
			case 'plum':
			  res = 0xdda0dd;
			break;
			case 'powderblue':
			  res = 0xb0e0e6;
			break;
			case 'purple':
			  res = 0x800080;
			break;
			case 'red':
			  res = 0xff0000;
			break;
			case 'rosybrown':
			  res = 0xbc8f8f;
			break;
			case 'royalblue':
			  res = 0x4169e1;
			break;
			case 'saddlebrown':
			  res = 0x8b4513;
			break;
			case 'salmon':
			  res = 0xfa8072;
			break;
			case 'sandybrown':
			  res = 0xf4a460;
			break;
			case 'seagreen':
			  res = 0x2e8b57;
			break;
			case 'seashell':
			  res = 0xfff5ee;
			break;
			case 'sienna':
			  res = 0xa0522d;
			break;
			case 'silver':
			  res = 0xc0c0c0;
			break;
			case 'skyblue':
			  res = 0x87ceeb;
			break;
			case 'slateblue':
			  res = 0x6a5acd;
			break;
			case 'slategray':
			  res = 0x708090;
			break;
			case 'snow':
			  res = 0xfffafa;
			break;
			case 'springgreen ':
			  res = 0x00ff7f;
			break;
			case 'steelblue':
			  res = 0x4682b4;
			break;
			case 'tan':
			  res = 0xd2b48c;
			break;
			case 'teal':
			  res = 0x008080;
			break;
			case 'thistle':
			  res = 0xd8bfd8;
			break;
			case 'tomato':
			  res = 0xff6347;
			break;
			case 'turquoise':
			  res = 0x40e0d0;
			break;
			case 'violet':
			  res = 0xee82ee;
			break;
			case 'violetred':
			  res = 0xd02090;
			break;
			case 'wheat':
			  res = 0xf5deb3;
			break;
			case 'white':
			  res = 0xffffff;
			break;
			case 'whitesmoke':
			  res = 0xf5f5f5;
			break;
			case 'yellow':
			  res = 0xffff00;
			break;
			case 'yellowgreen':
			  res = 0x9acd32;
			break;
			default:
			  res = Number(colr);
			break;
		}
		return (res);
	}
	//config animation
	function config_animation(node:XMLNode,obj):Void
	{
		if (node.attributes.enabled!=undefined)
		{
			obj.animation_enabled = getBool(node.attributes.enabled);
		}
		if (node.attributes.speed!=undefined)
		{
			obj.animation_speed = Number(node.attributes.speed);
		}
		if (node.attributes.type!=undefined)
		{
			obj.animation_type = String(node.attributes.type);
		}
		if (node.attributes.appearance!=undefined)
		{
			obj.animation_attribute = String(node.attributes.appearance);
		}
		if (node.attributes.delay!=undefined)
		{
			obj.animation_delay = Number(node.attributes.delay);
		}
	}
	//config hints
	function config_hints(node:XMLNode,obj):Void
	{
		var num_text:Number = this.findNode(node,'text');
		if (num_text != undefined) {
			obj.hints_text = String(node.childNodes[num_text].firstChild.nodeValue);
		}else if (node.attributes.text != undefined)
			obj.hints_text = String(node.attributes.text);
			
		if (node.attributes.enabled!=undefined)
		{
			obj.hints_enabled = getBool(node.attributes.enabled);
		}
		if (node.attributes.horizontal_position!=undefined)
		{
			obj.hints_horizontal_position = String(node.attributes.horizontal_position);
		}
		if (node.attributes.vertical_position!=undefined)
		{
			obj.hints_vertical_position = String(node.attributes.vertical_position);
		}
		if (node.attributes.width!=undefined)
		{
			obj.hints_width = Number(node.attributes.width);
		}
		if (node.attributes.height!=undefined)
		{
			obj.hints_height = Number(node.attributes.height);
		}
		if (node.attributes.auto_size!=undefined)
		{
			obj.hints_auto_size = getBool(node.attributes.auto_size);
		}
		var num_bg:Number = this.findNode(node,'background');
		if (num_bg!=undefined)
		{
			if (node.childNodes[num_bg].attributes.enabled!=undefined)
			{
				obj.hints_background_enabled = this.getBool(node.childNodes[num_bg].attributes.enabled);
			}
			if (node.childNodes[num_bg].attributes.color!=undefined)
			{
				obj.hints_background_color = this.getColor(node.childNodes[num_bg].attributes.color);
			}			
			if (node.childNodes[num_bg].attributes.alpha!=undefined)
			{
				obj.hints_background_alpha = Number(node.childNodes[num_bg].attributes.alpha);
			}
		}
		var num_brdr:Number = findNode(node,'border');
		if (num_brdr!=undefined)
		{
			if (node.childNodes[num_brdr].attributes.enabled!=undefined)
			{
				obj.hints_border_enabled = getBool(node.childNodes[num_brdr].attributes.enabled);
			}
			if (node.childNodes[num_brdr].attributes.color!=undefined)
			{
				obj.hints_border_color = getColor(node.childNodes[num_brdr].attributes.color);
			}
			if (node.childNodes[num_brdr].attributes.size != undefined)
			{
				obj.hints_border_size = Number(node.childNodes[num_brdr].attributes.size);
			}
			if (node.childNodes[num_brdr].attributes.alpha != undefined)
			{
				obj.hints_border_alpha = Number(node.childNodes[num_brdr].attributes.alpha);
			}
		}
		var num_text:Number = findNode(node,'font');
		if (num_text!=undefined)
		{
			obj.hints_text_format = configTextFormat(node.childNodes[num_text]);
		}
	}
	//-------------------------------------------------------
	function config_names(node:XMLNode,obj):Void
	{
		if (node.attributes.show!=undefined)
		{
			obj.default_show_names = getBool(node.attributes.show);
		}
		if (node.attributes.position!=undefined)
		{
			obj.default_names_position = String(node.attributes.position);
			obj.names_position = String(node.attributes.position);
		}
		var num_text:Number = findNode(node,'font');
		if (num_text!=undefined)
		{
			obj.names_text_format = configTextFormat(node.childNodes[num_text]);
		}
		if (node.attributes.rotation!=undefined)
		{
			obj.names_rotation = Number(node.attributes.rotation);
		}
		if (node.attributes.placement!=undefined)
		{
			obj.names_placement = String(node.attributes.placement);
		}
		if (node.attributes.x_offset!=undefined)
		{
			obj.names_dx = Number(node.attributes.x_offset);
		}
		if (node.attributes.y_offset!=undefined)
		{
			obj.names_dy = Number(node.attributes.y_offset);
		}
	}
	//-------------------------------------------------------
	function config_values(node:XMLNode,obj):Void
	{
		if (node.attributes.show!=undefined)
		{
			obj.default_show_values = getBool(node.attributes.show);
		}
		if (node.attributes.position!=undefined)
		{
			obj.default_values_position = String(node.attributes.position);
		}
		if (node.attributes.prefix!=undefined)
		{
			obj.values_prefix = String(node.attributes.prefix);
		}
		if (node.attributes.postfix!=undefined)
		{
			obj.values_postfix = String(node.attributes.postfix);
		}
		if (node.attributes.decimal_separator!=undefined)
		{
			obj.decimal_separator = String(node.attributes.decimal_separator);
		}
		if (node.attributes.thousand_separator!=undefined)
		{
			obj.thousand_separator = String(node.attributes.thousand_separator);
		}
		if (node.attributes.decimal_places!=undefined)
		{
			obj.decimal_places = Number(node.attributes.decimal_places);
		}
		if (node.attributes.rotation!=undefined)
		{
			obj.values_rotation = Number(node.attributes.rotation);
		}
		if (node.attributes.x_offset!=undefined)
		{
			obj.values_dx = Number(node.attributes.x_offset);
		}
		if (node.attributes.y_offset!=undefined)
		{
			obj.values_dy = Number(node.attributes.y_offset);
		}
		var num_text:Number = findNode(node,'font');
		if (num_text!=undefined)
		{
			obj.values_text_format = configTextFormat(node.childNodes[num_text]);
		}
	}
	//-------------------------------------------------------
	//get text format
	function configTextFormat(node:XMLNode):TextFormat
	{
		var tf:TextFormat = new TextFormat;
		
		if (node.attributes.type!=undefined)
			tf.font = String(node.attributes.type);
		else if (node.attributes.name != undefined)
			tf.font = String(node.attributes.name);
			
		if (node.attributes.size!=undefined)
		{
			tf.size = Number(node.attributes.size);
		}
		if (node.attributes.bold!=undefined)
		{
			tf.bold = getBool(node.attributes.bold);
		}
		if (node.attributes.italic!=undefined)
		{
			tf.italic = getBool(node.attributes.italic);
		}
		if (node.attributes.underline!=undefined)
		{
			tf.underline = getBool(node.attributes.underline);
		}
		if (node.attributes.align!=undefined)
		{
			tf.align = String(node.attributes.align);
		}
		if (node.attributes.color!=undefined)
		{
			tf.color = getColor(node.attributes.color);
		}
		return (tf);
	}
}