﻿class xml.text_xml
{
	private var lib;
	private var node:XMLNode;
	//-----------------------
	function text_xml(doc:XML)
	{
		lib = new xml.library();
		var num_obj:Number = lib.findNode(doc.firstChild,'objects');
		if (num_obj!=undefined)
		{
			node = doc.firstChild.childNodes[num_obj];
		}
	}
	//-------------------------
	function get_texts():Array
	{
		var i:Number;
		var res:Array = new Array();
		var num:Number = 0;
		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='text')
			{
				res[num] = new objects.text(_root);
				if (node.childNodes[i].attributes.text!=undefined)
				{
					res[num].text_ = String(node.childNodes[i].attributes.text);
				}
				if (node.childNodes[i].attributes.x!=undefined)
				{
					res[num].x = Number(node.childNodes[i].attributes.x);
				}
				if (node.childNodes[i].attributes.y!=undefined)
				{
					res[num].y = Number(node.childNodes[i].attributes.y);
				}
				if (node.childNodes[i].attributes.auto_size!=undefined)
				{
					res[num].auto_size = lib.getBool(node.childNodes[i].attributes.auto_size);
				}
				if (node.childNodes[i].attributes.width!=undefined)
				{
					res[num].width = Number(node.childNodes[i].attributes.width);
				}
				if (node.childNodes[i].attributes.height!=undefined)
				{
					res[num].height = Number(node.childNodes[i].attributes.height);
				}
				if (node.childNodes[i].attributes.url!=undefined)
				{
					res[num].url = String(node.childNodes[i].attributes.url);
				}
				if (node.childNodes[i].attributes.url_target!=undefined)
				{
					res[num].url_target = String(node.childNodes[i].attributes.url_target);
				}
				if (node.childNodes[i].attributes.rotation!=undefined)
				{
					res[num].text_rotation = Number(node.childNodes[i].attributes.rotation);
				}
				var num_font:Number = lib.findNode(node.childNodes[i],'font');
				if (num_font!=undefined)
				{
					res[num].text_format = lib.configTextFormat(node.childNodes[i].childNodes[num_font]);
				}
				var num_bg:Number = lib.findNode(node.childNodes[i],'background');
				if (num_bg!=undefined)
				{
					if (node.childNodes[i].childNodes[num_bg].attributes.enabled!=undefined)
					{
						res[num].background_enabled = lib.getBool(node.childNodes[i].childNodes[num_bg].attributes.enabled);
					}
					if (node.childNodes[i].childNodes[num_bg].attributes.color!=undefined)
					{
						res[num].background_color = lib.getColor(node.childNodes[i].childNodes[num_bg].attributes.color);
					}					
					if (node.childNodes[i].childNodes[num_bg].attributes.alpha!=undefined)
					{
						res[num].background_alpha = Number(node.childNodes[i].childNodes[num_bg].attributes.alpha);
					}
				}
				var num_brdr:Number = lib.findNode(node.childNodes[i],'border');
				if (num_brdr!=undefined)
				{
					if (node.childNodes[i].childNodes[num_brdr].attributes.enabled!=undefined)
					{
						res[num].border_enabled = lib.getBool(node.childNodes[i].childNodes[num_brdr].attributes.enabled);
					}
					if (node.childNodes[i].childNodes[num_brdr].attributes.color!=undefined)
					{
						res[num].border_color = lib.getColor(node.childNodes[i].childNodes[num_brdr].attributes.color);
					}
					if (node.childNodes[i].childNodes[num_brdr].attributes.size!=undefined)
					{
						res[num].border_size = Number(node.childNodes[i].childNodes[num_brdr].attributes.size);
					}
					if (node.childNodes[i].childNodes[num_brdr].attributes.alpha!=undefined)
					{
						res[num].border_alpha = Number(node.childNodes[i].childNodes[num_brdr].attributes.alpha);
					}
				}
				res[num].show();
				num++;
			}
		}
		return res;
	}
}