﻿class xml._2dpie_chart_xml
{
	var lib;
	var chart;
	function _2dpie_chart_xml(doc:XML,_chart)
	{
		chart = _chart;
		lib = new xml.library();
		var num_type:Number = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{
			var num_chart:Number = lib.findNode(doc.firstChild.childNodes[num_type],'chart');
			if (num_chart!=undefined)
			{
				var num_names:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'names');
				if (num_names!=undefined)
				{
					lib.config_names(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_names],chart);
				}
				var num_values:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'values');
				if (num_values!=undefined)
				{
					lib.config_values(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_values],chart);
				}
				var num_captions:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'captions');
				if (num_captions!=undefined)
				{
					var num_fnt:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions],'font');
					if (num_fnt!=undefined)
					{
						chart.captions_text_format = lib.configTextFormat(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].childNodes[num_fnt]);
					}
					if (doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.rotation!=undefined)
					{
						chart.captions_rotation = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.rotation);
					}
					if (doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.x_offset!=undefined)
					{
						chart.captions_dx = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.x_offset);
					}
					if (doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.y_offset!=undefined)
					{
						chart.captions_dy = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.y_offset);
					}
					if (doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.padding!=undefined)
					{
						chart.captionsPadding = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.padding);
					}
					if (doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.prevent_captions_overlap!=undefined)
					{
						chart.checkOverlap = lib.getBool(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].attributes.prevent_captions_overlap);
					}
					var num_links:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions],'links');
					if (num_links != undefined) {
						var linksNode:XMLNode = doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_captions].childNodes[num_links];
						if (linksNode.attributes.enabled != undefined)
							chart.linksEnabled = lib.getBool(linksNode.attributes.enabled);
						if (linksNode.attributes.edge_space != undefined)
							chart.linksEdgeSize = Number(linksNode.attributes.edge_space);
						if (linksNode.attributes.slice_space != undefined)
							chart.linksSpace = Number(linksNode.attributes.slice_space);
						if (linksNode.attributes.size != undefined)
							chart.linksSize = Number(linksNode.attributes.size);
						if (linksNode.attributes.color != undefined)
							chart.linksColor = lib.getColor(linksNode.attributes.color);
						if (linksNode.attributes.alpha != undefined)
							chart.linksAlpha = Number(linksNode.attributes.alpha);
					}
				}
				var num_hints:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'hints');
				if (num_hints!=undefined)
				{
					lib.config_hints(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_hints],chart);
				}
				var num_animation:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'animation');
				if (num_animation!=undefined)
				{
					lib.config_animation(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_animation],chart);
				}
				var num_pie:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'pie_chart');
				if (num_pie!=undefined)
				{
					config_pie(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_pie]);
				}
			}
		}
		var num_data:Number = lib.findNode(doc.firstChild,'data');
		if (num_data!=undefined)
		{
			var num_block:Number = lib.findNode(doc.firstChild.childNodes[num_data],'block');
			if (num_block!=undefined)
			{
				config_data(doc.firstChild.childNodes[num_data].childNodes[num_block]);
			}
		}
	}
	//configure chart
	private function config_pie(node:XMLNode):Void
	{
		if (node.attributes.shading_mode != undefined)
			chart.highlight = lib.getBool(node.attributes.shading_mode);
		if (node.attributes.x!=undefined)
		{
			chart.x = Number(node.attributes.x);			
		}
		if (node.attributes.y!=undefined)
		{
			chart.y = Number(node.attributes.y);
		}
		if (node.attributes.radius!=undefined)
		{
			chart.radius = Number(node.attributes.radius);
		}
		if (node.attributes.rotation!=undefined)
		{
			chart.rotation = Number(node.attributes.rotation);
		}
		if (node.attributes.captions_radius!=undefined)
		{
			chart.captions_radius = String(node.attributes.captions_radius);
		}
		if (node.attributes.protrusion_radius!=undefined)
		{
			chart.protrusion_radius = Number(node.attributes.protrusion_radius);
		}
		if (node.attributes.inner_radius!=undefined)
		{
			chart.inner_radius = Number(node.attributes.inner_radius);
		}
		var num_brd:Number = lib.findNode(node,'border');
		if (num_brd!=undefined)
		{
			if (node.childNodes[num_brd].attributes.enabled!=undefined)
			{
				chart.border_enabled = lib.getBool(node.childNodes[num_brd].attributes.enabled);
			}
			if (node.childNodes[num_brd].attributes.size!=undefined)
			{
				chart.border_size = Number(node.childNodes[num_brd].attributes.size);
			}
			if (node.childNodes[num_brd].attributes.color!=undefined)
			{
				chart.border_color = lib.getColor(node.childNodes[num_brd].attributes.color);
			}
			if (node.childNodes[num_brd].attributes.alpha!=undefined)
			{
				chart.border_alpha = Number(node.childNodes[num_brd].attributes.alpha);
			}
		}
		var num_bg:Number= lib.findNode(node,'background');
		if (num_bg!=undefined)
		{
			if (node.childNodes[num_bg].attributes.enabled!=undefined)
			{
				chart.default_background_enabled = lib.getBool(node.childNodes[num_bg].attributes.enabled);
			}
			if (node.childNodes[num_bg].attributes.color!=undefined)
			{
				chart.default_background_color = lib.getColor(node.childNodes[num_bg].attributes.color);
			}
			if (node.childNodes[num_bg].attributes.alpha!=undefined)
			{
				chart.default_background_alpha = Number(node.childNodes[num_bg].attributes.alpha);
			}
			if (node.childNodes[num_bg].attributes.auto_color!=undefined)
			{
				chart.background_auto_color = lib.getBool(node.childNodes[num_bg].attributes.auto_color);
			}
			if (node.childNodes[num_bg].attributes.tone!=undefined)
			{
				chart.background_tone = lib.getColor(node.childNodes[num_bg].attributes.tone);
			}
			
			lib.configChartSetsBackgrounds(chart,node.childNodes[num_bg],'radial');
		}
		var num_labels:Number = lib.findNode(node,'labels');
		if (num_labels != undefined)
		{
			//labels settings
			var num_bg:Number = lib.findNode(node.childNodes[num_labels],'background');
			if (num_bg != undefined)
			{
				if (node.childNodes[num_labels].childNodes[num_bg].attributes.enabled != undefined)
				{
					chart.labels_background_enabled = lib.getBool(node.childNodes[num_labels].childNodes[num_bg].attributes.enabled);
				}
				if (node.childNodes[num_labels].childNodes[num_bg].attributes.color != undefined)
				{
					chart.labels_background_color = lib.getColor(node.childNodes[num_labels].childNodes[num_bg].attributes.color);
				}
				if (node.childNodes[num_labels].childNodes[num_bg].attributes.alpha != undefined)
				{
					chart.labels_background_alpha = Number(node.childNodes[num_labels].childNodes[num_bg].attributes.alpha);
				}
			}
			var num_border:Number = lib.findNode(node.childNodes[num_labels],'border');
			if (num_border != undefined)
			{
				if (node.childNodes[num_labels].childNodes[num_border].attributes.enabled != undefined)
				{
					chart.labels_border_enabled = lib.getBool(node.childNodes[num_labels].childNodes[num_border].attributes.enabled);
				}
				if (node.childNodes[num_labels].childNodes[num_border].attributes.color != undefined)
				{
					chart.labels_border_color = lib.getColor(node.childNodes[num_labels].childNodes[num_border].attributes.color);
				}
				if (node.childNodes[num_labels].childNodes[num_border].attributes.size != undefined)
				{
					chart.labels_border_size = Number(node.childNodes[num_labels].childNodes[num_border].attributes.size);
				}
				if (node.childNodes[num_labels].childNodes[num_border].attributes.alpha != undefined)
				{
					chart.labels_border_alpha = Number(node.childNodes[num_labels].childNodes[num_border].attributes.alpha);
				}
			}
			var num_font:Number = lib.findNode(node.childNodes[num_labels],'font');
			if (num_font != undefined)
			{
				chart.lables_tf = lib.configTextFormat(node.childNodes[num_labels].childNodes[num_font]);
			}
			if (node.childNodes[num_labels].attributes.enabled != undefined)
			{
				chart.show_label = lib.getBool(node.childNodes[num_labels].attributes.enabled);
			}
			if (node.childNodes[num_labels].attributes.width != undefined)
			{
				chart.labels_width = Number(node.childNodes[num_labels].attributes.width);
			}
			if (node.childNodes[num_labels].attributes.radius != undefined)
			{
				chart.labels_radius = Number(node.childNodes[num_labels].attributes.radius);
			}
			var num_links:Number = lib.findNode(node.childNodes[num_labels],'link');
			if (num_links != undefined)
			{
				if (node.childNodes[num_labels].childNodes[num_links].attributes.enabled!=undefined)
				{
					chart.labels_links_enabled = lib.getBool(node.childNodes[num_labels].childNodes[num_links].attributes.enabled);
				}
				if (node.childNodes[num_labels].childNodes[num_links].attributes.color != undefined)
				{
					chart.labels_links_color = lib.getColor(node.childNodes[num_labels].childNodes[num_links].attributes.color);
				}
				if (node.childNodes[num_labels].childNodes[num_links].attributes.size != undefined)
				{
					chart.labels_links_size = Number(node.childNodes[num_labels].childNodes[num_links].attributes.size);
				}
				if (node.childNodes[num_labels].childNodes[num_links].attributes.alpha != undefined)
				{
					chart.labels_links_alpha = Number(node.childNodes[num_labels].childNodes[num_links].attributes.alpha);
				}
				var num_dot:Number = lib.findNode(node.childNodes[num_labels].childNodes[num_links],'dot');
				if (num_dot != undefined)
				{
					if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].attributes.radius != undefined)
					{
						chart.labels_links_dot_radius = Number(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].attributes.radius);
					}
					if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].attributes.enabled != undefined)
					{
						chart.labels_links_dot_enabled = lib.getBool(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].attributes.enabled);
					}
					var num_bg:Number = lib.findNode(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot],'background');
					if (num_bg != undefined)
					{
						if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_bg].attributes.enabled != undefined)
						{
							chart.labels_links_dot_background_enabled = lib.getBool(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_bg].attributes.enabled);
						}
						if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_bg].attributes.color != undefined)
						{
							chart.labels_links_dot_background_color = lib.getColor(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_bg].attributes.color)
						}
						if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_bg].attributes.alpha != undefined)
						{
							chart.labels_links_dot_background_alpha = Number(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_bg].attributes.alpha);
						}
					}
					var num_border:Number = lib.findNode(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot],'border');
					if (num_border!=undefined)
					{
						if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.enabled != undefined)
						{
							chart.labels_links_dot_border_enabled = lib.getBool(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.enabled);
						}
						if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.size != undefined)
						{
							chart.labels_links_dot_border_size = Number(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.size);
						}
						if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.color != undefined)
						{
							chart.labels_links_dot_border_color = lib.getColor(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.color);
						}
						if (node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.alpha != undefined)
						{
							chart.labels_links_dot_border_alpha = Number(node.childNodes[num_labels].childNodes[num_links].childNodes[num_dot].childNodes[num_border].attributes.alpha);
						}
					}
				}
			}
		}
	}
	//config data
	private function config_data(node:XMLNode):Void
	{
		var i:Number;
		var n:Number = 0;
		var hint_texts:Array = new Array();
		var urls:Array = new Array();
		var urls_target:Array = new Array();
		var sounds:Array = new Array();
		var sounds_loops:Array = new Array();
		var sounds_offset:Array = new Array();
		var captions_rotations:Array = new Array();
		var captions_dxs:Array = new Array();
		var captions_dys:Array = new Array();
		var background_colors:Array = new Array();
		var background_alphas:Array = new Array();
		var border_colors:Array = new Array();
		var border_alphas:Array = new Array();
		var border_enabled:Array = new Array();
		var border_sizes:Array = new Array();
		var labels_show:Array = new Array();
		var labels_text:Array = new Array();
		var sets_background_colors:Array = new Array();
		var sets_background_ratios:Array = new Array();
		var sets_background_alphas:Array = new Array();
		var sets_background_types:Array = new Array();
		var sets_background_rotations:Array = new Array();
		var sets_background_gradient_types:Array = new Array();
		
		var lib = new xml.library();

		for (i=0;i<node.childNodes.length;i++)
		{
			if (node.childNodes[i].nodeName=='set')
			{
				if (node.childNodes[i].attributes.hint_text != undefined)
					hint_texts[i] = String(node.childNodes[i].attributes.hint_texts);
					
				if (node.childNodes[i].attributes.show_label != undefined)
					labels_show[i] = lib.getBool(node.childNodes[i].attributes.show_label);

				if (node.childNodes[i].attributes.label_text != undefined)
					labels_text[i] = String(node.childNodes[i].attributes.label_text);

				if (node.childNodes[i].attributes.border_size != undefined)				
					border_sizes[n] = Number(node.childNodes[i].attributes.border_size);

				if (node.childNodes[i].attributes.border_enabled != undefined)
					border_enabled[n] = lib.getBool(node.childNodes[i].attributes.border_enabled);

				if (node.childNodes[i].attributes.border_color != undefined)
					border_colors[n] = lib.getColor(node.childNodes[i].attributes.border_color);

				if (node.childNodes[i].attributes.border_alpha != undefined)
					border_alphas[n] = Number(node.childNodes[i].attributes.border_alpha);

				if (node.childNodes[i].attributes.url!=undefined)
					urls[n] = String(node.childNodes[i].attributes.url);

				if (node.childNodes[i].attributes.url_target!=undefined)
					urls_target[n] = String(node.childNodes[i].attributes.url_target);

				if (node.childNodes[i].attributes.sound!=undefined)
					sounds[n] = String(node.childNodes[i].attributes.sound);

				if (node.childNodes[i].attributes.sound_loops!=undefined)
					sounds_loops[n] = Number(node.childNodes[i].attributes.sound_loops);

				if (node.childNodes[i].attributes.sound_offset!=undefined)
					sounds_offset[n] = Number(node.childNodes[i].attributes.sound_offset);

				if (node.childNodes[i].attributes.caption_rotation!=undefined)
					captions_rotations[n] = Number(node.childNodes[i].attributes.caption_rotation);
				
				if (node.childNodes[i].attributes.caption_x_offset!=undefined)
					captions_dxs[n] = Number(node.childNodes[i].attributes.caption_x_offset);

				if (node.childNodes[i].attributes.caption_y_offset!=undefined)
					captions_dys[n] = Number(node.childNodes[i].attributes.caption_y_offset);

				if (node.childNodes[i].attributes.color!=undefined) {
					background_colors[n] = lib.getColor(node.childNodes[i].attributes.color);					

				}

				if (node.childNodes[i].attributes.alpha!=undefined)
					background_alphas[n] = Number(node.childNodes[i].attributes.alpha);
									
				var num_bg:Number = lib.findNode(node.childNodes[i],'background');
				if (num_bg != undefined) {
					var bgNode:XMLNode = node.childNodes[i].childNodes[num_bg];
					sets_background_types[n] = (bgNode.attributes.type != undefined) ? bgNode.attributes.type : 'solid';
					sets_background_rotations[n] = (bgNode.attributes.rotation != undefined) ? Number(bgNode.attributes.rotation) : 0;
					sets_background_gradient_types[n] = (bgNode.attributes.gradient_type != undefined) ? String(bgNode.attributes.gradient_type) : 'radial';
					
					sets_background_colors[n] = new Array();
					sets_background_ratios[n] = new Array();
					sets_background_alphas[n] = new Array();
					var numColors:Number = lib.findNode(bgNode,'colors');
					if (numColors != undefined)
						for (var j:Number = 0;j<bgNode.childNodes[numColors].childNodes.length;j++) {
							sets_background_colors[n][j] = bgNode.childNodes[numColors].childNodes[j].firstChild.nodeValue;
						}				
					var numRatios:Number = lib.findNode(bgNode,'ratios');
					if (numRatios != undefined)
						for (var j:Number = 0;j<bgNode.childNodes[numRatios].childNodes.length;j++) {
							sets_background_ratios[n][j] = bgNode.childNodes[numRatios].childNodes[j].firstChild.nodeValue;
						}
					var numAlphas:Number = lib.findNode(bgNode,'alphas');
					if (numAlphas != undefined)
						for (var j:Number = 0;j<bgNode.childNodes[numAlphas].childNodes.length;j++) {
							sets_background_alphas[n][j] = bgNode.childNodes[numAlphas].childNodes[j].firstChild.nodeValue;
						}
				}
			    n++;
			}
		}
		chart.sets_background_colors = sets_background_colors;
		chart.sets_background_ratios = sets_background_ratios;
		chart.sets_background_alphas = sets_background_alphas;
		chart.sets_background_types = sets_background_types;
		chart.sets_background_rotations = sets_background_rotations;
		chart.sets_background_gradient_types = sets_background_gradient_types;
		chart.hint_texts = hint_texts;
		chart.urls = urls;
		chart.urls_target = urls_target;
		chart.sounds = sounds;
		chart.sounds_loops = sounds_loops;
		chart.sounds_offset = sounds_offset;
		chart.captions_rotations = captions_rotations;
		chart.captions_dxs = captions_dxs;
		chart.captions_dys = captions_dys;
		chart.background_colors = background_colors;
		chart.background_alphas = background_alphas;
		chart.border_enableds = border_enabled;
		chart.border_colors = border_colors;
		chart.border_alphas = border_alphas;
		chart.border_sizes = border_sizes;
		chart.show_labels = labels_show;
		chart.labels_texts = labels_text;
	}
}