﻿class xml.grid_xml
{
	private var grid;
	private var lib;
	private var chart_type:String;
	//--------------------------------
	function grid_xml(doc:XML,gr,t:String,ct:String)
	{
		grid = gr;
		lib = new xml.library();
		var num_type:Number = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{
			var num_workspace:Number = lib.findNode(doc.firstChild.childNodes[num_type],'workspace');
			if (num_workspace!=undefined)
			{
				var num_x_axis:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_workspace],'x_axis');				
				var x_smart_enabled:Boolean;
				if (num_x_axis != undefined) {
					x_smart_enabled = lib.getBool(doc.firstChild.childNodes[num_type].childNodes[num_workspace].childNodes[num_x_axis].attributes.smart);
				}
				
				var num_y_axis:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_workspace],'y_axis');				
				var y_smart_enabled:Boolean;
				if (num_y_axis != undefined) {
					y_smart_enabled = lib.getBool(doc.firstChild.childNodes[num_type].childNodes[num_workspace].childNodes[num_y_axis].attributes.smart);
				}

				
				var num_grid:Number;
				if (ct!=undefined)
				{
					var nums:Array = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_workspace],'grid','all');
					var i:Number;
					for (i=0;i<nums.length;i++)
					{
						if (doc.firstChild.childNodes[num_type].childNodes[num_workspace].childNodes[nums[i]].attributes.chart==ct)
						{
							num_grid = nums[i];
							break;
						}
					}
				}else
				{
					num_grid = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_workspace],'grid');
				}
				if (num_grid!=undefined)
				{
					var num_ext_grid:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_workspace].childNodes[num_grid],t);
					if (num_ext_grid!=undefined)
					{
						config_grid(doc.firstChild.childNodes[num_type].childNodes[num_workspace].childNodes[num_grid].childNodes[num_ext_grid]);
					}
				}				
				if (x_smart_enabled != undefined)
					grid.xsmart = x_smart_enabled;
				if (y_smart_enabled != undefined)
					grid.ysmart = y_smart_enabled;
			}
		}
	}
	private function config_grid(node:XMLNode):Void
	{
		grid.enabled = true;
		if (node.attributes.start!=undefined)
		{
			grid.start_grid_value = Number(node.attributes.start);
		}
		if (node.attributes.end!=undefined)
		{
			grid.end_grid_value = Number(node.attributes.end);
		}
		if (node.attributes.left_space!=undefined)
		{
			grid.x += Number(node.attributes.left_space);
			grid.width -= Number(node.attributes.left_space);
		}
		if (node.attributes.right_space!=undefined)
		{
			grid.width -= Number(node.attributes.right_space);
		}
		if (node.attributes.up_space!=undefined)
		{
			grid.y += Number(node.attributes.up_space);
			grid.height -= Number(node.attributes.up_space);
		}
		if (node.attributes.down_space!=undefined)
		{
			grid.height -= Number(node.attributes.down_space);
		}
		if (node.attributes.start_value!=undefined)
		{
			grid.grid_start_value = Number(node.attributes.start_value);
		}
		if (node.attributes.end_value!=undefined)
		{
			grid.grid_end_value = Number(node.attributes.end_value);
		}
		if (node.attributes.lines_count!=undefined)
		{
			grid.lines_count = Number(node.attributes.lines_count);
		}
		if (node.attributes.lines_step!=undefined)
		{
			grid.lines_step = Number(node.attributes.lines_step);
		}
		var num_captions:Number = lib.findNode(node,'captions');
		if (num_captions!=undefined)
		{
			if (node.childNodes[num_captions].attributes.enabled != undefined) {
				grid.captions_enabled = lib.getBool(node.childNodes[num_captions].attributes.enabled);
			}
			
			if (node.childNodes[num_captions].attributes.position!=undefined)
			{
				grid.captions_position = String(node.childNodes[num_captions].attributes.position);
			}
			if (node.childNodes[num_captions].attributes.rotation!=undefined)
			{
				grid.captions_rotation = Number(node.childNodes[num_captions].attributes.rotation);
			}
			if (node.childNodes[num_captions].attributes.x_offset!=undefined)
			{
				grid.captions_dx = Number(node.childNodes[num_captions].attributes.x_offset);
			}
			if (node.childNodes[num_captions].attributes.y_offset!=undefined)
			{
				grid.captions_dy = Number(node.childNodes[num_captions].attributes.y_offset);
			}
			var num_text:Number = lib.findNode(node.childNodes[num_captions],'font');
			if (num_text!=undefined)
			{
				grid.captions_text_format = lib.configTextFormat(node.childNodes[num_captions].childNodes[num_text]);
			}
		}
		var num_lines:Number = lib.findNode(node,'lines');
		if (num_lines!=undefined)
		{
			if (node.childNodes[num_lines].attributes.size!=undefined)
			{
				grid.lines_size = Number(node.childNodes[num_lines].attributes.size);
			}
			if (node.childNodes[num_lines].attributes.color!=undefined)
			{
				grid.lines_color = lib.getColor(node.childNodes[num_lines].attributes.color);
			}
			if (node.childNodes[num_lines].attributes.alpha!=undefined)
			{
				grid.lines_alpha = Number(node.childNodes[num_lines].attributes.alpha);
			}
		}
		var num_ticks:Number = lib.findNode(node,'ticks');
		if (num_ticks!=undefined)
		{
			var num_major:Number = lib.findNode(node.childNodes[num_ticks],'major_ticks');
			if (num_major != undefined) {
				grid.majorTicks = config_tick(node.childNodes[num_ticks].childNodes[num_major], grid.majorTicks,'cross',9);
				var num_minor:Number = lib.findNode(node.childNodes[num_ticks],'minor_ticks');
				if (num_minor != undefined) {
					grid.minorTicks = config_tick(node.childNodes[num_ticks].childNodes[num_minor], grid.minorTicks,'outside',3);
				}
			}
		}
	}
	
	private function config_tick(node:XMLNode, ticks:Object, defaultType:String, defaultLength:Number):Object
	{
		var num_lines:Number = lib.findNode(node,'lines');
		var mTicksLines:XMLNode = null;
		if (num_lines != undefined) {
			mTicksLines = node.childNodes[num_lines];
		}
		ticks = {};
		ticks.start = node.attributes.start != undefined ? Number(node.attributes.start) : undefined;
		ticks.step  = node.attributes.step != undefined ? Number(node.attributes.step) : undefined;
		ticks.lines = new Object();
		ticks.lines.size = mTicksLines.attributes.size != undefined ? Number(mTicksLines.attributes.size) : 1;
		ticks.lines.color = mTicksLines.attributes.color != undefined ? lib.getColor(mTicksLines.attributes.color) : 0;
		ticks.lines.alpha = mTicksLines.attributes.alpha != undefined ? Number(mTicksLines.attributes.alpha) : 100;
		ticks.lines.length = mTicksLines.attributes.length != undefined ? Number(mTicksLines.attributes.length) : defaultLength;
		ticks.lines.type = mTicksLines.attributes.type != undefined ? String(mTicksLines.attributes.type) : defaultType;
		return ticks;
	}
}