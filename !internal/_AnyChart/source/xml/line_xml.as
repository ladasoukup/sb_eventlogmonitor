﻿class xml.line_xml
{
	private var lib;
	private var xml_obj:XMLNode;
	private var o:String;
	private var target_mc:MovieClip;
	
	function line_xml(doc:XML)
	{
		var i:Number;
		lib = new xml.library();
		xml_obj = doc.firstChild;
	}
	
	function get_lines(orientation_:String,t_mc:MovieClip,t:String,chart_type:String):Array
	{
		target_mc = t_mc;
		o = orientation_;
		if (t==undefined)
		{
			t = 'values';
		}
		var res:Array = new Array();
		var i:Number;
		var num_objects:Number = lib.findNode(xml_obj,'objects');
		var num:Number = 0;
		if (t=='values')
		{
			res = get_values_lines(chart_type);
		}
		if (t=='arguments')
		{
			res = get_arguments_lines(chart_type);
		}
		return res;
	}	
	//-----------------------------------------------------
	private function get_values_lines(chart_type:String):Array
	{
		var i:Number;
		var num_objects:Number = lib.findNode(xml_obj,'objects');
		var num:Number = 0;
		var res:Array = new Array();
		if (num_objects!=undefined)
		{
			for (i=0;i<xml_obj.childNodes[num_objects].childNodes.length;i++)
			{
				if (xml_obj.childNodes[num_objects].childNodes[i].nodeName=='line')
				{
					if ((xml_obj.childNodes[num_objects].childNodes[i].attributes.chart_type==chart_type) or (chart_type==undefined))
					{
						if (xml_obj.childNodes[num_objects].childNodes[i].attributes.value!=undefined)
						{
							res[num] = new objects.line(target_mc,o);
							res[num].value = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.value);
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.size!=undefined)
							{
								res[num].size = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.size);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.color!=undefined)
							{
								res[num].color = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].attributes.color);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha!=undefined)
							{
								res[num].alpha = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text!=undefined)
							{
								res[num].text = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position!=undefined)
							{
								res[num].text_position = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation!=undefined)
							{
								res[num].text_rotation = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset!=undefined)
							{
								res[num].text_dx = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset!=undefined)
							{
								res[num].text_dy = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset);
							}
							var num_text:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i],'font');
							if (num_text!=undefined)
							{
								res[num].text_format = lib.configTextFormat(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_text]);
							}
							var num_arrow:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i],'arrow');
							if (num_arrow!=undefined)
							{
								res[num].arrowEnabled = true;
								res[num].arrowWidth = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.width);
								res[num].arrowHeight = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.height);
								res[num].arrowType = String(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.type);
								res[num].arrowOffset = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.offset);
								var num_bg:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow],'background');
								res[num].arrowBackgroundEnabled = lib.getBool(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_bg].attributes.enabled);
								res[num].arrowBackgroundColor = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_bg].attributes.color);
								res[num].arrowBackgroundAlpha = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_bg].attributes.alpha);
								var num_brd:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow],'border');
								res[num].arrowBorderSize = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_brd].attributes.size);
								res[num].arrowBorderColor = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_brd].attributes.color);
								res[num].arrowBorderAlpha = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_brd].attributes.alpha);
							}
							num++;
						}
					}
				}
			}
		}
		return res;
	}
	//arguments
	private function get_arguments_lines(chart_type:String):Array
	{
		var i:Number;
		var num_objects:Number = lib.findNode(xml_obj,'objects');
		var num:Number = 0;
		var res:Array = new Array();
		if (num_objects!=undefined)
		{
			for (i=0;i<xml_obj.childNodes[num_objects].childNodes.length;i++)
			{
				if (xml_obj.childNodes[num_objects].childNodes[i].nodeName=='line')
				{
					if ((xml_obj.childNodes[num_objects].childNodes[i].attributes.chart_type==chart_type) or (chart_type==undefined))
					{
						if (xml_obj.childNodes[num_objects].childNodes[i].attributes.argument!=undefined)
						{
							res[num] = new objects.line(target_mc,'vertical');
							res[num].argument = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.argument);
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.size!=undefined)
							{
								res[num].size = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.size);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.color!=undefined)
							{
								res[num].color = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].attributes.color);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha!=undefined)
							{
								res[num].alpha = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.alpha);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text!=undefined)
							{
								res[num].text = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position!=undefined)
							{
								res[num].text_position = String(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_position);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation!=undefined)
							{
								res[num].text_rotation = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_rotation);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset!=undefined)
							{
								res[num].text_dx = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_x_offset);
							}
							if (xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset!=undefined)
							{
								res[num].text_dy = Number(xml_obj.childNodes[num_objects].childNodes[i].attributes.text_y_offset);
							}
							var num_text:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i],'font');
							if (num_text!=undefined)
							{
								res[num].text_format = lib.configTextFormat(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_text]);
							}
							var num_arrow:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i],'arrow');
							if (num_arrow!=undefined)
							{
								res[num].arrowEnabled = true;
								res[num].arrowWidth = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.width);
								res[num].arrowHeight = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.height);
								res[num].arrowType = String(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.type);
								res[num].arrowOffset = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].attributes.offset);
								var num_bg:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow],'background');
								res[num].arrowBackgroundEnabled = lib.getBool(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_bg].attributes.enabled);
								res[num].arrowBackgroundColor = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_bg].attributes.color);
								res[num].arrowBackgroundAlpha = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_bg].attributes.alpha);
								var num_brd:Number = lib.findNode(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow],'border');
								res[num].arrowBorderSize = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_brd].attributes.size);
								res[num].arrowBorderColor = lib.getColor(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_brd].attributes.color);
								res[num].arrowBorderAlpha = Number(xml_obj.childNodes[num_objects].childNodes[i].childNodes[num_arrow].childNodes[num_brd].attributes.alpha);
							}
							num++;
						}
					}
				}
			}
		}
		return res;
	}
}