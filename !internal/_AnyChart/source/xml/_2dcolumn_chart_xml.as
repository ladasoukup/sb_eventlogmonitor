﻿class xml._2dcolumn_chart_xml
{
	private var chart;
	private var lib;
	private var type:String;
	//--------------------------------------------
	function _2dcolumn_chart_xml(doc:XML,chart_,t:String)
	{
		if (t!=undefined)
		{
			type = t;
		}
		lib = new xml.library();
		chart = chart_;
		var num_type:Number = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{
			var num_chart:Number;
			if (t==undefined)
			{
				num_chart = lib.findNode(doc.firstChild.childNodes[num_type],'chart');
			}else
			{
				var nums:Array = lib.findNode(doc.firstChild.childNodes[num_type],'chart','all');
				var i:Number;
				for (i=0;i<nums.length;i++)
				{
					if (doc.firstChild.childNodes[num_type].childNodes[nums[i]].attributes.type==t)
					{
						num_chart = nums[i];
						break;
					}
				}
			}
			if (num_chart!=undefined)
			{
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value!=undefined)
				{
					chart.maximum_value = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.maximum_value);
				}
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value!=undefined)
				{
					chart.minimum_value = Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.minimum_value);
				}
				var num_hints:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'hints');
				if (num_hints!=undefined)
				{
					lib.config_hints(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_hints],chart);
				}
				var num_animation:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'animation');
				if (num_animation!=undefined)
				{
					lib.config_animation(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_animation],chart);
				}
				var num_names:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'names');
				if (num_names!=undefined)
				{
					lib.config_names(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_names],chart);
				}
				var num_values:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'values');
				if (num_values!=undefined)
				{
					lib.config_values(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_values],chart);
				}
				var num_column:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_chart],'column_chart');
				if (num_column!=undefined)
				{
					config_chart(doc.firstChild.childNodes[num_type].childNodes[num_chart].childNodes[num_column]);
				}
			}
		}
		var num_data:Number = lib.findNode(doc.firstChild,'data');
		if (num_data!=undefined)
		{
			config_data(doc.firstChild.childNodes[num_data]);
		}
	}
	//---------------------------------------------
	private function config_chart(node:XMLNode):Void
	{
		if (node.attributes.round_radius!=undefined)
		{
			chart.round_radius = Number(node.attributes.round_radius);
		}
		if (node.attributes.column_space!=undefined)
		{
			chart.column_space = Number(node.attributes.column_space);
		}
		if (node.attributes.block_space!=undefined)
		{
			chart.block_space = Number(node.attributes.block_space);
		}
		if (node.attributes.left_space!=undefined)
		{
			chart.x += Number(node.attributes.left_space);
			chart.width -= Number(node.attributes.left_space);
		}
		if (node.attributes.right_space!=undefined)
		{
			chart.width -= Number(node.attributes.right_space);
		}
		if (node.attributes.up_space!=undefined)
		{
			chart.y += Number(node.attributes.up_space);
			chart.height -= Number(node.attributes.up_space);
		}
		if (node.attributes.down_space!=undefined)
		{
			chart.height -= Number(node.attributes.down_space);
		}
		var num_bl:Number = lib.findNode(node,'block_names');
		if (num_bl!=undefined)
		{
			var num_fnt:Number = lib.findNode(node.childNodes[num_bl],'font');
			if (num_fnt!=undefined)
			{
				chart.block_names_text_format = lib.configTextFormat(node.childNodes[num_bl].childNodes[num_fnt]);
			}
			chart.default_show_block_names = (node.childNodes[num_bl].attributes.enabled != undefined) ? lib.getBool(node.childNodes[num_bl].attributes.enabled) : true;
			if (node.childNodes[num_bl].attributes.rotation!=undefined)
			{
				chart.block_names_rotation = Number(node.childNodes[num_bl].attributes.rotation);
			}
			if (node.childNodes[num_bl].attributes.x_offset!=undefined)
			{
				chart.block_names_dx = Number(node.childNodes[num_bl].attributes.x_offset);
			}
			if (node.childNodes[num_bl].attributes.y_offset!=undefined)
			{
				chart.block_names_dy = Number(node.childNodes[num_bl].attributes.y_offset);
			}
			if (node.childNodes[num_bl].attributes.rotation != undefined)
				chart.block_names_rotation = Number(node.childNodes[num_bl].attributes.rotation);
			else
				chart.block_names_rotation = 0;
			
			if (node.childNodes[num_bl].attributes.placement != undefined)
				chart.block_names_placement = node.childNodes[num_bl].attributes.placement;
				
			if (node.childNodes[num_bl].attributes.position != undefined)
				chart.block_names_position = node.childNodes[num_bl].attributes.position;
		}
		var num_bg:Number = lib.findNode(node,'background');
		if (num_bg!=undefined)
		{
			if (node.childNodes[num_bg].attributes.enabled!=undefined)
			{
				chart.default_background_enabled = lib.getBool(node.childNodes[num_bg].attributes.enabled);
			}
			if (node.childNodes[num_bg].attributes.auto_color!=undefined)
			{
				chart.background_auto_color = lib.getBool(node.childNodes[num_bg].attributes.auto_color);
			}
			if (node.childNodes[num_bg].attributes.color!=undefined)
			{
				chart.default_background_color = lib.getColor(node.childNodes[num_bg].attributes.color);
			}
			if (node.childNodes[num_bg].attributes.alpha!=undefined)
			{
				chart.default_background_alpha = Number(node.childNodes[num_bg].attributes.alpha);
			}
			
			lib.configChartSetsBackgrounds(chart,node.childNodes[num_bg],'linear');	
		}
		var num_brdr:Number = lib.findNode(node,'border');
		if (num_brdr!=undefined)
		{
			if (node.childNodes[num_brdr].attributes.enabled!=undefined)
			{
				chart.default_border_enabled = lib.getBool(node.childNodes[num_brdr].attributes.enabled);
			}
			if (node.childNodes[num_brdr].attributes.size!=undefined)
			{
				chart.default_border_size = Number(node.childNodes[num_brdr].attributes.size);
			}
			if (node.childNodes[num_brdr].attributes.color!=undefined)
			{
				chart.default_border_color = lib.getColor(node.childNodes[num_brdr].attributes.color);
			}
			if (node.childNodes[num_brdr].attributes.alpha!=undefined)
			{
				chart.default_border_alpha = Number(node.childNodes[num_brdr].attributes.alpha);
			}
		}
	}
	//config chart data
	private function config_data(node:XMLNode):Void
	{
		var i:Number;
		var j:Number;
		var k:Number = 0;
		var n:Number = 0;
		//arrays
		var sets_background_colors:Array = new Array();
		var sets_background_ratios:Array = new Array();
		var sets_background_alphas:Array = new Array();
		var sets_background_types:Array = new Array();
		var sets_background_rotations:Array = new Array();
		var sets_background_gradient_types:Array = new Array();
			
		var background_image_source = new Array();
		var show_names_block:Array = new Array();
		var show_values_block:Array = new Array();
		var block_names:Array = new Array();
		var background_enabled_block:Array = new Array();
		var background_color_block:Array = new Array();
		var background_alpha_block:Array = new Array();
		var background_enabled:Array = new Array();
		var background_color:Array = new Array();
		var background_alpha:Array = new Array();
		var border_enabled_block:Array = new Array();
		var border_size_block:Array = new Array();
		var border_color_block:Array = new Array();
		var border_alpha_block:Array = new Array();
		var border_enabled:Array = new Array();
		var border_size:Array = new Array();
		var border_color:Array = new Array();
		var border_alpha:Array = new Array();
		var urls:Array = new Array();
		var urls_target:Array = new Array();
		var sounds:Array = new Array();
		var sounds_loops:Array = new Array();
		var sounds_offset:Array = new Array();
		var names_rotations:Array = new Array();
		var names_dxs:Array = new Array();
		var names_dys:Array = new Array();
		var values_rotations:Array = new Array();
		var values_dxs:Array = new Array();
		var values_dys:Array = new Array();
		var block_names_rotations:Array = new Array();
		var block_names_dxs:Array = new Array();
		var block_names_dys:Array = new Array();
		var hint_texts:Array = new Array();
		//------
		for (i=0;i<node.childNodes.length;i++)
		{
			if (type==undefined)
			{
				type = node.childNodes[i].attributes.chart_type;
			}
			if ((node.childNodes[i].nodeName=='block') and ((node.childNodes[i].attributes.chart_type==type) or (node.childNodes[i].attributes.chart_type==undefined)))
			{
				k = 0;
				if (node.childNodes[i].attributes.name_rotation!=undefined)
				{
					block_names_rotations[n] = Number(node.childNodes[i].attributes.name_rotation);
				}
				if (node.childNodes[i].attributes.name_x_offset!=undefined)
				{
					block_names_dxs[n] = Number(node.childNodes[i].attributes.name_x_offset);
				}
				if (node.childNodes[i].attributes.name_y_offset!=undefined)
				{
					block_names_dys[n] = Number(node.childNodes[i].attributes.name_y_offset);
				}
				if (node.childNodes[i].attributes.name!=undefined)
				{
					block_names[n] = String(node.childNodes[i].attributes.name);
				}
				if (node.childNodes[i].attributes.background_enabled!=undefined)
				{
					background_enabled_block[n] = lib.getBool(node.childNodes[i].attributes.background_enabled);
				}
				if (node.childNodes[i].attributes.background_color!=undefined)
				{
					background_color_block[n] = lib.getColor(node.childNodes[i].attributes.background_color);
				}else if (node.childNodes[i].attributes.color!=undefined)
				{
					background_color_block[n] = lib.getColor(node.childNodes[i].attributes.color);
				}

				if (node.childNodes[i].attributes.background_alpha!=undefined)
				{
					background_alpha_block[n] = Number(node.childNodes[i].attributes.background_alpha);
				}
				if (node.childNodes[i].attributes.border_enabled!=undefined)
				{
					border_enabled_block[n] = lib.getBool(node.childNodes[i].attributes.border_enabled);
				}
				if (node.childNodes[i].attributes.border_size!=undefined)
				{
					border_size_block[n] = Number(node.childNodes[i].attributes.border_size);
				}
				if (node.childNodes[i].attributes.border_color!=undefined)
				{
					border_color_block[n] = lib.getColor(node.childNodes[i].attributes.border_color);
				}
				if (node.childNodes[i].attributes.border_alpha!=undefined)
				{
					border_alpha_block[n] = Number(node.childNodes[i].attributes.border_alpha);
				}
				if (node.childNodes[i].attributes.show_names!=undefined)
				{
					show_names_block[n] = lib.getBool(node.childNodes[i].attributes.show_names);
				}
				if (node.childNodes[i].attributes.show_values!=undefined)
				{
					show_values_block[n] = lib.getBool(node.childNodes[i].attributes.show_values);
				}
				//subarrays
				background_enabled[n] = new Array();
				background_color[n] = new Array();
				background_alpha[n] = new Array();
				border_enabled[n] = new Array();
				border_size[n] = new Array();
				border_color[n] = new Array();
				border_alpha[n] = new Array();				
				background_image_source[n] = new Array();
				urls[n] = new Array();
				urls_target[n] = new Array();
				sounds[n] = new Array();
				sounds_loops[n] = new Array();
				sounds_offset[n] = new Array();
				names_rotations[n] = new Array();
				names_dxs[n] = new Array();
				names_dys[n] = new Array();
				values_rotations[n] = new Array();
				values_dxs[n] = new Array();
				values_dys[n] = new Array();
				hint_texts[n] = new Array();
				sets_background_colors[n] = new Array();
				sets_background_ratios[n] = new Array();
				sets_background_alphas[n] = new Array();
				sets_background_types[n] = new Array();
				sets_background_rotations[n] = new Array();
				sets_background_gradient_types[n] = new Array();
				//---------
				for (j=0;j<node.childNodes[i].childNodes.length;j++)
				{
					if (node.childNodes[i].childNodes[j].nodeName=='set')
					{
						//----------------------------------------------
						if (node.childNodes[i].childNodes[j].attributes.hint_text != undefined)
							hint_texts[n][k] = String(node.childNodes[i].childNodes[j].attributes.hint_text);
							
						if (node.childNodes[i].childNodes[j].attributes.color!=undefined)
						{
							background_color[n][k] = lib.getColor(node.childNodes[i].childNodes[j].attributes.color);
						}
						if (node.childNodes[i].childNodes[j].attributes.background_enabled!=undefined)
						{
							background_enabled[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.background_enabled);
						}
						if (node.childNodes[i].childNodes[j].attributes.alpha!=undefined)
						{
							background_alpha[n][k] = Number(node.childNodes[i].childNodes[j].attributes.alpha);
						}
						if (node.childNodes[i].childNodes[j].attributes.border_enabled!=undefined)
						{
							border_enabled[n][k] = lib.getBool(node.childNodes[i].childNodes[j].attributes.border_enabled);
						}
						if (node.childNodes[i].childNodes[j].attributes.border_size!=undefined)
						{
							border_size[n][k] = Number(node.childNodes[i].childNodes[j].attributes.border_size);
						}
						if (node.childNodes[i].childNodes[j].attributes.border_color!=undefined)
						{
							border_color[n][k] = lib.getColor(node.childNodes[i].childNodes[j].attributes.border_color);
						}
						if (node.childNodes[i].childNodes[j].attributes.border_alpha!=undefined)
						{
							border_alpha[n][k] = Number(node.childNodes[i].childNodes[j].attributes.border_alpha);
						}
						if (node.childNodes[i].childNodes[j].attributes.image!=undefined)
						{
							background_image_source[n][k] = String(node.childNodes[i].childNodes[j].attributes.image);
						}
						if (node.childNodes[i].childNodes[j].attributes.url!=undefined)
						{
							urls[n][k] = String(node.childNodes[i].childNodes[j].attributes.url);
						}
						if (node.childNodes[i].childNodes[j].attributes.url_target!=undefined)
						{
							urls_target[n][k] = String(node.childNodes[i].childNodes[j].attributes.url_target);
						}
						if (node.childNodes[i].childNodes[j].attributes.sound!=undefined)
						{
							sounds[n][k] = String(node.childNodes[i].childNodes[j].attributes.sound);
						}
						if (node.childNodes[i].childNodes[j].attributes.sound_loops!=undefined)
						{
							sounds_loops[n][k] = Number(node.childNodes[i].childNodes[j].attributes.sound_loops);
						}
						if (node.childNodes[i].childNodes[j].attributes.sound_offset!=undefined)
						{
							sounds_offset[n][k] = Number(node.childNodes[i].childNodes[j].attributes.sound_offset);
						}
						if (node.childNodes[i].childNodes[j].attributes.name_rotation!=undefined)
						{
							names_rotations[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_rotation);
						}
						if (node.childNodes[i].childNodes[j].attributes.name_x_offset!=undefined)
						{
							names_dxs[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_x_offset);
						}
						if (node.childNodes[i].childNodes[j].attributes.name_y_offset!=undefined)
						{
							names_dys[n][k] = Number(node.childNodes[i].childNodes[j].attributes.name_y_offset);
						}
						if (node.childNodes[i].childNodes[j].attributes.value_rotation!=undefined)
						{
							values_rotations[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_rotation);
						}
						if (node.childNodes[i].childNodes[j].attributes.value_x_offset!=undefined)
						{
							values_dxs[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_x_offset);
						}
						if (node.childNodes[i].childNodes[j].attributes.value_y_offset!=undefined)
						{
							values_dys[n][k] = Number(node.childNodes[i].childNodes[j].attributes.value_y_offset);
						}
						var num_bg:Number = lib.findNode(node.childNodes[i].childNodes[j],'background');
						if (num_bg != undefined) {
							var bgNode:XMLNode = node.childNodes[i].childNodes[j].childNodes[num_bg];
							sets_background_types[n][k] = (bgNode.attributes.type != undefined) ? bgNode.attributes.type : undefined;
							sets_background_rotations[n][k] = (bgNode.attributes.rotation != undefined) ? Number(bgNode.attributes.rotation) : undefined;
							sets_background_gradient_types[n][k] = (bgNode.attributes.gradient_type != undefined) ? String(bgNode.attributes.gradient_type) : undefined;
							
							sets_background_colors[n][k] = new Array();
							sets_background_ratios[n][k] = new Array();
							sets_background_alphas[n][k] = new Array();
							var numColors:Number = lib.findNode(bgNode,'colors');
							if (numColors != undefined)
								for (var t:Number = 0;t<bgNode.childNodes[numColors].childNodes.length;t++) {
									sets_background_colors[n][k][t] = Number(bgNode.childNodes[numColors].childNodes[t].firstChild.nodeValue);
								}				
							var numRatios:Number = lib.findNode(bgNode,'ratios');
							if (numRatios != undefined)
								for (var t:Number = 0;t<bgNode.childNodes[numRatios].childNodes.length;t++) {
									sets_background_ratios[n][k][t] = Number(bgNode.childNodes[numRatios].childNodes[t].firstChild.nodeValue);
								}
							var numAlphas:Number = lib.findNode(bgNode,'alphas');
							if (numAlphas != undefined)
								for (var t:Number = 0;t<bgNode.childNodes[numAlphas].childNodes.length;t++) {
									sets_background_alphas[n][k][t] = Number(bgNode.childNodes[numAlphas].childNodes[t].firstChild.nodeValue);
								}
						}
						//----------------------------------------------
						k++;
					}
				}
				n++;
			}
		}
		//set chart
		chart.hint_texts = hint_texts;
		chart.background_enabled = background_enabled;
		chart.background_color = background_color;
		chart.background_alpha = background_alpha;
		chart.border_enabled = border_enabled;
		chart.border_size = border_size;
		chart.border_color = border_color;
		chart.border_alpha = border_alpha;
		chart.block_background_enabled = background_enabled_block;
		chart.block_background_color = background_color_block;
		chart.block_background_alpha = background_alpha_block;
		chart.block_border_enabled = border_enabled_block;
		chart.block_border_size = border_size_block;
		chart.block_border_color = border_color_block;
		chart.block_border_alpha = border_alpha_block;
		chart.block_names = block_names;
		chart.block_show_values = show_values_block;
		chart.block_show_names = show_names_block
		chart.block_names_rotations = block_names_rotations;
		chart.block_names_dxs = block_names_dxs;
		chart.block_names_dys = block_names_dys;
		chart.background_image_source = background_image_source;
		chart.sounds = sounds;
		chart.sounds_loops = sounds_loops;
		chart.sounds_offsets = sounds_offset;
		chart.urls = urls;
		chart.urls_target = urls_target;
		chart.names_rotations = names_rotations;
		chart.names_dxs = names_dxs;
		chart.names_dys = names_dys;
		chart.values_rotations = values_rotations;
		chart.values_dxs = values_dxs;
		chart.values_dys = values_dys;
		chart.sets_background_colors = sets_background_colors;
		chart.sets_background_ratios = sets_background_ratios;
		chart.sets_background_alphas = sets_background_alphas;
		chart.sets_background_types = sets_background_types;
		chart.sets_background_rotations = sets_background_rotations;
		chart.sets_background_gradient_types = sets_background_gradient_types;
	}
}