﻿class xml.workspace
{
	//library
	var lib;
	var ws;
	var chart_type;
	
	private var w_grid_ordering;
	private var w_chart_ordering;
	private var w_can_refresh:Boolean;
	//-------
	function get can_refresh():Boolean
	{
		return w_can_refresh;
	}
	function get chart_ordering()
	{
		return w_chart_ordering;
	}
	function get_chart_ordering(t:String)
	{
		var i:Number;
		var res:String;
		for (i=0;i<w_chart_ordering.length;i++)
		{
			if (w_chart_ordering[i].chart==t)
			{
				res = w_chart_ordering[i].value;
			}
		}
		return res;
	}
	function get grid_ordering()
	{
		return w_grid_ordering;
	}
	function get_grid_ordering(t:String)
	{
		if (t==undefined)
		{
			return w_grid_ordering;
		}else
		{
			var i:Number;
			var res:String;
			for (i=0;i<w_grid_ordering.length;i++)
			{
				if (w_grid_ordering[i].chart == t)
				{
					res = w_grid_ordering[i].value;
					break;
				}
			}
			return res;
		}
	}
	function workspace(doc:XML,wsp)
	{
		var isHorizontal:Boolean = arguments[2] == undefined ? false : Boolean(arguments[2]);
		ws = wsp;
		lib = new xml.library();
		var num_type = lib.findNode(doc.firstChild,'type');
		if (num_type!=undefined)
		{
			var num_system:Number = lib.findNode(doc.firstChild.childNodes[num_type],'system');
			if (num_system!=undefined)
			{
				var num_refr:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_system],'refresh');
				if (doc.firstChild.childNodes[num_type].childNodes[num_system].childNodes[num_refr].attributes.enabled!=undefined)
				{
					w_can_refresh = lib.getBool(doc.firstChild.childNodes[num_type].childNodes[num_system].childNodes[num_refr].attributes.enabled);
				}
			}
			var num_chart:Number = lib.findNode(doc.firstChild.childNodes[num_type],'chart');
			if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.refresh_interval != undefined)
			{
				ws.chart_refresh_interval = 1000*Number(doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.refresh_interval);
				if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.refresh_source != undefined)
				{
					ws.chart_refresh_source = doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.refresh_source;
					if (doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.refresh_content != undefined)
					{
						ws.chart_refresh_type = doc.firstChild.childNodes[num_type].childNodes[num_chart].attributes.refresh_content;
					}else
					{
						ws.chart_refresh_type = 'data';
					}
				}
			}
			var num_ws = lib.findNode(doc.firstChild.childNodes[num_type],'workspace');
			if (num_ws!=undefined)
			{
				var num_ca:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'chart_area');
				if (num_ca!=undefined)
				{
					config_chart_area(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_ca]);
				}
				var num_ba:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'base_area');
				if (num_ba!=undefined)
				{
					config_base_area(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_ba]);
				}
				var num_name:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'name');
				if (num_name!=undefined)
				{
					config_name(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_name]);
				}
				var num_subname:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'subname');
				if (num_subname!=undefined)
				{
					config_subname(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_subname]);
				}
				var num_x:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'x_axis');
				if (num_x!=undefined)
				{
					if (isHorizontal)
						config_y_axis(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_x]);
					else
						config_x_axis(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_x]);
				}
				var num_y:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'y_axis');
				if (num_y!=undefined)
				{
					if (isHorizontal)
						config_x_axis(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_y]);
					else
						config_y_axis(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_y]);
				}
				var num_bg:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'background');
				if (num_bg!=undefined)
				{
					config_background(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_bg]);
				}
				var nums_grid:Array = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'grid','all');
				if (nums_grid.length>1)
				{
					w_grid_ordering = new Array();
					var i:Number = 0;
					for (i=0;i<nums_grid.length;i++)
					{
						w_grid_ordering[i] = new Object();
						if (doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[nums_grid[i]].attributes.ordering != undefined)
						{
							w_grid_ordering[i].value = String(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[nums_grid[i]].attributes.ordering);
						}else
						{
							w_grid_ordering[i].value = 'back';
						}
						w_grid_ordering[i].chart = String(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[nums_grid[i]].attributes.chart);
					}
				}else
				{
					var num_grid:Number = lib.findNode(doc.firstChild.childNodes[num_type].childNodes[num_ws],'grid');
					if (num_grid!=undefined)
					{
						if (doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_grid].attributes.ordering!=undefined)
						{
							w_grid_ordering = String(doc.firstChild.childNodes[num_type].childNodes[num_ws].childNodes[num_grid].attributes.ordering);
						}else
						{
							w_grid_ordering = "back";
						}
					}
				}
			}
			if (w_grid_ordering == 'back')
				DepthManager.setGridBottom();

			var num_ch:Array = lib.findNode(doc.firstChild.childNodes[num_type],'chart','all');
			if (num_ch.length>1)
			{
				chart_type = new Array();
				w_chart_ordering = new Array();
				var i:Number;
				for (i=0;i<num_ch.length;i++)
				{
					w_chart_ordering[i] = new Object();					
					chart_type[i] = String(doc.firstChild.childNodes[num_type].childNodes[num_ch[i]].attributes.type);
					w_chart_ordering[i].chart = chart_type[i];
					w_chart_ordering[i].value = String(doc.firstChild.childNodes[num_type].childNodes[num_ch[i]].attributes.ordering);
				}
			}else
			{
				chart_type = new String();
				chart_type = String(doc.firstChild.childNodes[num_type].childNodes[num_ch[0]].attributes.type);
			}
		}
		var num_obj:Number = lib.findNode(doc.firstChild,'objects');
		if (num_obj != undefined) {
			if (doc.firstChild.childNodes[num_obj].attributes.ordering != undefined) {
				switch (doc.firstChild.childNodes[num_obj].attributes.ordering) {
					case 'chart_front':
						DepthManager.setObjectsTopChart();
						break;
					case 'grid_front':
						DepthManager.setObjectsTopGrid();
						break;
					case 'grid_back':
						DepthManager.setObjectsBottomGrid();
						break;
					default:
						DepthManager.setObjectsTopGrid();
						break;
				}
			}else {
				DepthManager.setObjectsTopGrid();
			}
		}else {
			DepthManager.setObjectsTopGrid();
		}
	}
	//configure chart area
	private function config_chart_area(node:XMLNode):Void
	{
		//main settings
		if (node.attributes.enabled!=undefined)
		{
			ws.chart_area_enabled = lib.getBool(node.attributes.enabled);
		}
		if (node.attributes.width!=undefined)
		{
			ws.chart_area_width = Number(node.attributes.width);
		}
		if (node.attributes.height!=undefined)
		{
			ws.chart_area_height = Number(node.attributes.height);
		}
		if (node.attributes.deep!=undefined)
		{
			ws.chart_area_deep = Number(node.attributes.deep);
		}
		if (node.attributes.x!=undefined)
		{
			ws.x = Number(node.attributes.x);
		}
		if (node.attributes.y!=undefined)
		{
			ws.y = Number(node.attributes.y);
		}
		if (node.parentNode.attributes.rotation_grade!=undefined)
		{
			ws.rotate_3d = Number(node.parentNode.attributes.rotation_grade);
		}
		//background
		var num_bg:Number = lib.findNode(node,'background');
		if (num_bg!=undefined)
		{
			if (node.childNodes[num_bg].attributes.enabled!=undefined)
			{
				ws.chart_area_background_enabled = lib.getBool(node.childNodes[num_bg].attributes.enabled);
			}
			if (node.childNodes[num_bg].attributes.color!=undefined)
			{
				ws.chart_area_background_color = lib.getColor(node.childNodes[num_bg].attributes.color);
			}
			if (node.childNodes[num_bg].attributes.alpha!=undefined)
			{
				ws.chart_area_background_alpha = Number(node.childNodes[num_bg].attributes.alpha);
			}
			if (node.childNodes[num_bg].attributes.type!=undefined)
			{
				ws.chart_area_background_type = String(node.childNodes[num_bg].attributes.type);
			}
			if (ws.chart_area_background_type == 'gradient')
			{
				ws.chart_area_gradient_type = node.childNodes[num_bg].attributes.gradient_type;
				var num_colors:Number = lib.findNode(node.childNodes[num_bg],'colors');
				var i:Number;
				ws.chart_area_gradient_colors = new Array();
				for (i=0;i<node.childNodes[num_bg].childNodes[num_colors].childNodes.length;i++)				
				{
					ws.chart_area_gradient_colors.push(Number(lib.getColor(node.childNodes[num_bg].childNodes[num_colors].childNodes[i].firstChild.nodeValue)));
				}
				var num_alphas:Number = lib.findNode(node.childNodes[num_bg],'alphas');
				ws.chart_area_gradient_alphas = new Array();
				for (i=0;i<node.childNodes[num_bg].childNodes[num_alphas].childNodes.length;i++)
				{
					ws.chart_area_gradient_alphas.push(Number(node.childNodes[num_bg].childNodes[num_alphas].childNodes[i].firstChild.nodeValue));
				}
				var num_ratios:Number = lib.findNode(node.childNodes[num_bg],'ratios');
				ws.chart_area_gradient_ratios = new Array();
				for (i=0;i<node.childNodes[num_bg].childNodes[num_ratios].childNodes.length;i++)
				{
					ws.chart_area_gradient_ratios.push(Number(node.childNodes[num_bg].childNodes[num_ratios].childNodes[i].firstChild.nodeValue));
				}
				var num_matrix:Number = lib.findNode(node.childNodes[num_bg],'matrix');
				ws.chart_area_gradient_matrix = new Object();
				for (var j:String in node.childNodes[num_bg].childNodes[num_matrix].attributes)
				{
					ws.chart_area_gradient_matrix[j] = node.childNodes[num_bg].childNodes[num_matrix].attributes[j];
				}
			}else
			{
				var num_img:Number = lib.findNode(node.childNodes[num_bg],'image');
				if (num_img!=undefined)
				{
					if (node.childNodes[num_bg].childNodes[num_img].attributes.source!=undefined)
					{
						ws.chart_area_background_image_source = String(node.childNodes[num_bg].childNodes[num_img].attributes.source);
					}
					if (node.childNodes[num_bg].childNodes[num_img].attributes.position!=undefined)
					{
						ws.chart_area_background_image_position = String(node.childNodes[num_bg].childNodes[num_img].attributes.position);
					}
				}
			}
		}
 		//border
		var num_brdr:Number = lib.findNode(node,'border');
		if (num_brdr!=undefined)
		{
			if (node.childNodes[num_brdr].attributes.enabled!=undefined)
			{
				ws.chart_area_border_enabled = lib.getBool(node.childNodes[num_brdr].attributes.enabled);
			}
			if (node.childNodes[num_brdr].attributes.size!=undefined)
			{
				ws.chart_area_border_size = Number(node.childNodes[num_brdr].attributes.size);
			}
			if (node.childNodes[num_brdr].attributes.color!=undefined)
			{
				ws.chart_area_border_color = lib.getColor(node.childNodes[num_brdr].attributes.color);
			}
			if (node.childNodes[num_brdr].attributes.alpha!=undefined)
			{
				ws.chart_area_border_alpha = Number(node.childNodes[num_brdr].attributes.alpha);
			}
		}
	}
	//config base area
	private function config_base_area(node:XMLNode):Void
	{
		if (node.attributes.enabled!=undefined)
		{
			ws.base_area_enabled = lib.getBool(node.attributes.enabled);
		}
		if (node.attributes.height!=undefined)
		{
			ws.base_area_height = Number(node.attributes.height);
		}
		if (node.attributes.deep!=undefined)
		{
			ws.base_area_deep = Number(node.attributes.deep);
		}
		//backgorund
		var num_bg:Number = lib.findNode(node,'background');
		if (num_bg!=undefined)
		{
			if (node.childNodes[num_bg].attributes.enabled!=undefined)
			{
				ws.base_area_background_enabled = lib.getBool(node.childNodes[num_bg].attributes.enabled);
			}
			if (node.childNodes[num_bg].attributes.color!=undefined)
			{
				ws.base_area_background_color = lib.getColor(node.childNodes[num_bg].attributes.color);
			}
			if (node.childNodes[num_bg].attributes.alpha!=undefined)
			{
				ws.base_area_backgorund_alpha = Number(node.childNodes[num_bg].attributes.alpha);
			}
		}
		//border
		var num_brdr:Number = lib.findNode(node,'border');
		if (num_brdr!=undefined)
		{
			if (node.childNodes[num_brdr].attributes.enabled!=undefined)
			{
				ws.base_area_border_enabled = lib.getBool(node.childNodes[num_brdr].attributes.enabled);
			}
			if (node.childNodes[num_brdr].attributes.size!=undefined)
			{
				ws.base_area_border_size = Number(node.childNodes[num_brdr].attributes.size);
			}
			if (node.childNodes[num_brdr].attributes.color!=undefined)
			{
				ws.base_area_border_color = lib.getColor(node.childNodes[num_brdr].attributes.color);
			}
			if (node.childNodes[num_brdr].attributes.alpha!=undefined)
			{
				ws.base_area_border_alpha = Number(node.childNodes[num_brdr].attributes.alpha);
			}
		}
	}
	//config name
	private function config_name(node:XMLNode):Void
	{
		if (node.attributes.text!=undefined)
		{
			ws.name = String(node.attributes.text);
		}
		var num_font:Number = lib.findNode(node,'font');
		if (num_font!=undefined)
		{
			ws.name_text_format = lib.configTextFormat(node.childNodes[num_font]);
		}
	}
	//config subname
	private function config_subname(node:XMLNode):Void
	{
		if (node.attributes.text!=undefined)
		{
			ws.subname = String(node.attributes.text);
		}
		var num_font:Number = lib.findNode(node,'font');
		if (num_font!=undefined)
		{
			ws.subname_text_format = lib.configTextFormat(node.childNodes[num_font]);
		}
	}
	//config x axis
	private function config_x_axis(node:XMLNode):Void
	{
		if (node.attributes.name!=undefined)
		{
			ws.x_axis_name = String(node.attributes.name);
		}
		if (node.attributes.rotation!=undefined)
		{
			ws.x_axis_name_text_rotation = Number(node.attributes.rotation);
		}
		
		if (node.attributes.dx != undefined)
			ws.x_axis_dx = Number(node.attributes.dx);
		if (node.attributes.dy != undefined)
			ws.x_axis_dy = Number(node.attributes.dy);
			
		if (node.attributes.direction != undefined) {
			ws.x_axis_name_text_direction = String(node.attributes.direction);
		}
		ws.x_axis_name_text_position = node.attributes.position != undefined ? String(node.attributes.position) : 'center_bottom';
		var num_font:Number = lib.findNode(node,'font');
		if (num_font!=undefined)
		{
			ws.x_axis_name_text_format = lib.configTextFormat(node.childNodes[num_font]);
		}
	}
	//config y axis
	private function config_y_axis(node:XMLNode):Void
	{
		if (node.attributes.name!=undefined)
		{
			ws.y_axis_name = String(node.attributes.name);
		}
		if (node.attributes.direction != undefined) {
			ws.y_axis_name_text_direction = String(node.attributes.direction);
		}
		if (node.attributes.rotation!=undefined)
		{
			ws.y_axis_name_text_rotation = Number(node.attributes.rotation);
		}
		if (node.attributes.dx != undefined)
			ws.y_axis_dx = Number(node.attributes.dx);
		if (node.attributes.dy != undefined)
			ws.y_axis_dy = Number(node.attributes.dy);
		ws.y_axis_name_text_position = node.attributes.position != undefined ? String(node.attributes.position) : 'left_center';
		var num_font:Number = lib.findNode(node,'font');
		if (num_font!=undefined)
		{
			ws.y_axis_name_text_format = lib.configTextFormat(node.childNodes[num_font]);
		}
	}
	//confgi background
	private function config_background(node:XMLNode):Void
	{
		if (node.attributes.enabled!=undefined)
		{
			ws.background_enabled = lib.getBool(node.attributes.enabled);
		}
		if (node.attributes.color!=undefined)
		{
			ws.background_color = lib.getColor(node.attributes.color);
		}
		if (node.attributes.alpha!=undefined)
		{
			ws.background_alpha = Number(node.attributes.alpha);
		}
		if (node.attributes.type!=undefined)
		{
			ws.background_type = String(node.attributes.type);
			if (node.attributes.type=='gradient')
			{
				ws.background_gradient_type = node.attributes.gradient_type;
				var num_colors:Number = lib.findNode(node,'colors');
				var i:Number;
				ws.background_gradient_colors = new Array();
				for (i=0;i<node.childNodes[num_colors].childNodes.length;i++)				
				{
					ws.background_gradient_colors.push(Number(lib.getColor(node.childNodes[num_colors].childNodes[i].firstChild.nodeValue)));
				}
				var num_alphas:Number = lib.findNode(node,'alphas');
				ws.background_gradient_alphas = new Array();
				for (i=0;i<node.childNodes[num_alphas].childNodes.length;i++)
				{
					ws.background_gradient_alphas.push(Number(node.childNodes[num_alphas].childNodes[i].firstChild.nodeValue));
				}
				var num_ratios:Number = lib.findNode(node,'ratios');
				ws.background_gradient_ratios = new Array();
				for (i=0;i<node.childNodes[num_ratios].childNodes.length;i++)
				{
					ws.background_gradient_ratios.push(Number(node.childNodes[num_ratios].childNodes[i].firstChild.nodeValue));
				}
				var num_matrix:Number = lib.findNode(node,'matrix');
				ws.background_gradient_matrix = new Object();
				for (var j:String in node.childNodes[num_matrix].attributes)
				{
					ws.background_gradient_matrix[j] = node.childNodes[num_matrix].attributes[j];
				}
			}
		}
		var num_img:Number = lib.findNode(node,'image');
		if (num_img!=undefined)
		{
			if (node.childNodes[num_img].attributes.source!=undefined)
			{
				ws.background_image_source = String(node.childNodes[num_img].attributes.source);
			}
			if (node.childNodes[num_img].attributes.position!=undefined)
			{
				ws.background_image_position = String(node.childNodes[num_img].attributes.position);
			}
			if (node.childNodes[num_img].attributes.x!=undefined)
			{
				ws.background_image_x = Number(node.childNodes[num_img].attributes.x);
			}
			if (node.childNodes[num_img].attributes.y!=undefined)
			{
				ws.background_image_y = Number(node.childNodes[num_img].attributes.y);
			}
			if (node.childNodes[num_img].attributes.width!=undefined)
			{
				ws.background_image_width = Number(node.childNodes[num_img].attributes.width);
			}
			if (node.childNodes[num_img].attributes.height!=undefined)
			{
				ws.background_image_height = Number(node.childNodes[num_img].attributes.height);
			}
		}
	}
}