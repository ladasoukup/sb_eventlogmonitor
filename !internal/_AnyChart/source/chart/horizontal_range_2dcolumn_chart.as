﻿class chart.horizontal_range_2dcolumn_chart
{
	
	//all gradient background
	public var background_type:String = 'solid';
	public var background_gradient_colors:Array;
	public var background_gradient_alphas:Array;
	public var background_gradient_ratios:Array;
	public var background_gradient_rotation:Number = 0;
	public var background_gradient_type:String = 'linear';
	
	public var sets_background_colors:Array;
	public var sets_background_ratios:Array;
	public var sets_background_alphas:Array;
	public var sets_background_types:Array;
	public var sets_background_rotations:Array;
	public var sets_background_gradient_types:Array;
	
	public var block_names_rotation:Number = 0;
	public var hints_text:String = '<b>{NAME}</b>, open: {OPEN}, close: {CLOSE}';
	public var hint_texts:Array;
	
	//position
	private var c_x:Number;
	private var c_y:Number;
	
	//size
	private var c_width:Number;
	private var c_height:Number;
	private var c_round_radius:Number;
	
	//values settings
	private var c_open_values:Array;
	private var c_close_values:Array;
	private var c_max_value:Number;
	private var c_min_value:Number;
	private var c_values_scale:Number;
	private var c_zero_x:Number;
	private var c_grid_max:Number;
	private var c_grid_min:Number;
		
	//columns
	public var columns:Array;
	private var c_column_space:Number;
	private var c_block_space:Number;
	
	//background
	private var c_background_enabled:Array;
	private var c_background_color:Array;
	private var c_background_alpha:Array;
	//defaults
	private var c_background_enabled_all:Boolean;
	private var c_background_color_all:Number;
	private var c_background_alpha_all:Number;
	//blocks
	private var c_background_enabled_block:Array;
	private var c_background_color_block:Array;
	private var c_background_alpha_block:Array;
	//background auto color
	private var c_background_auto_color:Boolean;
	private var c_background_tone:Number;

	//images
	private var c_background_image_source:Array;
	
	//border
	private var c_border_enabled:Array;
	private var c_border_color:Array;
	private var c_border_alpha:Array;
	private var c_border_size:Array;
	//defaults
	private var c_border_enabled_all:Boolean;
	private var c_border_color_all:Number;
	private var c_border_alpha_all:Number;
	private var c_border_size_all:Number;
	//blocks
	private var c_border_enabled_block:Array;
	private var c_border_color_block:Array;
	private var c_border_alpha_block:Array;
	private var c_border_size_block:Array;
	
	//animation
	private var c_animation_enabled:Boolean;
	private var c_animation_speed:Number;
	private var c_animation_type:String;
	private var c_animation_attribute:String;
	
	//column names
	private var c_names_text:Array;
	private var c_names_position:Array;
	private var c_names_show:Array;
	private var c_names_text_format:TextFormat;
	private var c_names_position_all:String;
	private var c_names_show_all:Boolean;
	private var c_names_show_block:Array;
	private var c_names_rotations:Array;
	private var c_names_rotation:Number;
	private var c_names_dxs:Array;
	private var c_names_dx:Number;
	private var c_names_dys:Array;
	private var c_names_dy:Number;
	
	//column values
	private var c_values_prefix:String;
	private var c_values_postfix:String;
	private var c_values_position:Array;
	private var c_values_show:Array;
	private var c_values_position_all:String;
	private var c_values_show_all:Boolean;
	private var c_values_show_block:Array;
	private var c_values_decimal_separator:String;
	private var c_values_thousand_separator:String;
	private var c_values_decimal_places:Number;
	private var c_values_text_format:TextFormat;
	private var c_values_text:Array;
	private var c_values_rotations:Array;
	private var c_values_rotation:Number;
	private var c_values_dx:Number;
	private var c_values_dxs:Array;
	private var c_values_dy:Number;
	private var c_values_dys:Array;
	
	//hints
	private var c_hints_enabled:Boolean;
	private var c_hints_horizontal_position:String;
	private var c_hints_vertical_position:String;
	private var c_hints_width:Number;
	private var c_hints_height:Number;
	private var c_hints_auto_size:Boolean;
	private var c_hints_background_enabled:Boolean;
	private var c_hints_background_color:Number;
	private var c_hints_border_enabled:Boolean;
	private var c_hints_border_color:Number;
	private var c_hints_text_format:TextFormat;
	
	public var hints_background_alpha:Number = 100;
	public var hints_border_size:Number = 1;
	public var hints_border_alpha:Number = 100;
	
	//block names
	private var c_block_names:Array;
	private var c_block_names_text_format:TextFormat;
	private var block_names_text_fields:Array;
	
	public var block_names_position:String = 'left';
	public var block_names_placement:String = 'block';
	public var default_show_block_names:Boolean = true;
	public var block_names_dx:Number = 0;
	public var block_names_dy:Number = 0;
	
	//urls
	private var c_urls:Array;
	private var c_urls_target:Array;
	private var c_urls_target_all:String;
	
	//sounds
	private var c_sounds:Array;
	private var c_sounds_loops:Array;
	private var c_sounds_start_pos:Array;
	private var c_sounds_loops_all:Number;
	private var c_sounds_start_pos_all:Number;
	
	//chart movie clip
	private var chart_mc:MovieClip;
	
	//-----------------------------------------------------
	//set x position
	function set x(new_x:Number):Void
	{
		c_x = new_x;
	}
	function get x():Number
	{
		return c_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		c_y = new_y;
	}
	function get y():Number
	{
		return c_y;
	}
	//set width
	function set width(w:Number):Void
	{
		if (w<=0)
		{
			_root.showError("Chart width should be positive");
		}else
		{
			c_width = w;
		}
	}
	function get width():Number
	{
		return c_width;
	}
	//set height
	function set height(h:Number):Void
	{
		if (h<=0)
		{
			_root.showError("Chart height should be positive");
		}else
		{
			c_height = h;
		}
	}
	function get height():Number
	{
		return c_height;
	}
	//set values
	function set open_values(v:Array):Void
	{
		c_open_values = v;
	}
	function set close_values(v:Array):Void
	{
		c_close_values = v;
	}
	function get values():Array
	{
		return c_values_text;
	}
	//set maximum value
	function set maximum_value(max:Number):Void
	{
		c_grid_max = max;
	}
	function get maximum_value():Number
	{
		return c_max_value;
	}
	function get grid_maximum():Number
	{
		return c_grid_max;
	}
	//set minimum value
	function set minimum_value(min:Number):Void
	{
		c_grid_min = min;
	}
	function get minimum_value():Number
	{
		return c_min_value;
	}
	function get grid_minimum():Number
	{
		return c_grid_min;
	}
	//set column space
	function set column_space(c:Number):Void
	{
		if (c<0)
		{
			_root.showError("Chart column_space should be positive");
		}else
		{
			c_column_space = c;
		}
	}
	//set block space
	function set block_space(s:Number):Void
	{
		if (s<0)
		{
			_root.showError("Chart block_space value should be positive");
		}else
		{
			c_block_space = s;
		}
	}
	//set background enabled
	function set background_enabled(e:Array):Void
	{
		c_background_enabled = e;
	}
	function set default_background_enabled(e:Boolean):Void
	{
		c_background_enabled_all = e;
	}
	function set blocks_background_enabled(e:Array):Void
	{
		c_background_enabled_block = e;
	}
	//set background color
	function set background_color(clr:Array):Void
	{
		c_background_color = clr;
	}
	function get background_color():Array
	{
		return c_background_color;
	}
	function set default_background_color(clr:Number):Void
	{
		c_background_color_all = clr;
	}
	function set blocks_background_color(clr:Array):Void
	{
		c_background_color_block = clr;
	}
	//set background alpha
	function set background_alpha(a:Array):Void
	{
		c_background_alpha = a;
	}
	function set default_background_alpha(a:Number):Void
	{
		c_background_alpha_all = a;
	}
	function set blocks_background_alpha(a:Array):Void
	{
		c_background_alpha_block = a;
	}
	//set border enabled
	function set border_enabled(e:Array):Void
	{
		c_border_enabled = e;
	}
	function set default_border_enabled(e:Boolean):Void
	{
		c_border_enabled_all = e;
	}
	function set blocks_border_enabled(e:Array):Void
	{
		c_border_enabled_block = e;
	}
	//set border color
	function set border_color(clr:Array):Void
	{
		c_border_color = clr;
	}
	function set default_border_color(clr:Number):Void
	{
		c_border_color_all = clr;
	}
	function set blocks_border_color(clr:Array):Void
	{
		c_border_color_block = clr;
	}
	//set border alpha
	function set border_alpha(a:Array):Void
	{
		c_border_alpha = a;
	}
	function set default_border_alpha(a:Number):Void
	{
		c_border_alpha_all = a;
	}
	function set blocks_border_alpha(a:Array):Void
	{
		c_border_alpha_block = a;
	}
	//set border size
	function set border_size(s:Array):Void
	{
		c_border_size = s;
	}
	function set default_border_size(s:Number):Void
	{
		c_border_size_all = s;
	}
	function set blocks_border_size(s:Array):Void
	{
		c_border_size_block = s;
	}	
	//animation
	function set animation_enabled(e:Boolean):Void
	{
		c_animation_enabled = e;
	}
	function set animation_speed(s:Number):Void
	{
		c_animation_speed = s;
	}
		function set animation_type(t:String):Void
	{
		c_animation_type = t;
	}
	function set animation_attribute(a:String):Void
	{
		c_animation_attribute = a;
	}
	//names
	function set names(n:Array):Void
	{
		c_names_text = n;
	}
	function set names_rotations(r:Array):Void
	{
		c_names_rotations = r;
	}
	function set names_rotation(r:Number):Void
	{
		c_names_rotation = r;
	}
	function set names_dxs(d:Array):Void
	{
		c_names_dxs = d;
	}
	function set names_dx(d:Number):Void
	{
		c_names_dx = d;
	}
	function set names_dy(d:Number):Void
	{
		c_names_dy = d;
	}
	function set names_dys(d:Array):Void
	{
		c_names_dys = d;
	}
	function set values_rotation(r:Number):Void
	{
		c_values_rotation = r;
	}
	function set values_rotations(r:Array):Void
	{
		c_values_rotations = r;
	}
	function set values_dx(d:Number):Void
	{
		c_values_dx = d;
	}
	function set values_dy(d:Number):Void
	{
		c_values_dy = d;
	}
	function set values_dxs(d:Array):Void
	{
		c_values_dxs = d;
	}
	function set values_dys(d:Array):Void
	{
		c_values_dys = d;
	}
	function get names():Array
	{
		return c_names_text;
	}
	//default names position
	function set default_names_position(pos:String):Void
	{
		c_names_position_all = pos;
	}
	//names position
	function set names_position(pos:Array):Void
	{
		c_names_position = pos;
	}
	//names text format
	function set names_text_format(tf:TextFormat):Void
	{
		c_names_text_format = tf;
	}
	//show names
	function set show_names(s:Array):Void
	{
		c_names_show = s;
	}
	//default show names
	function set default_show_names(s:Boolean):Void
	{
		c_names_show_all = s;
	}
	//block show names
	function set blocks_show_names(s:Array):Void
	{
		c_names_show_block = s;
	}
	//block names
	function set block_names(n:Array):Void
	{
		c_block_names = n;
	}
	//block names text format
	function set block_names_text_format(tf:TextFormat):Void
	{
		c_block_names_text_format = tf;
	}
	//values text
	//values prefix
	function set values_prefix(p:String):Void
	{
		c_values_prefix = p;
	}
	function get values_prefix():String
	{
		return c_values_prefix;
	}
	//values postfix
	function set values_postfix(p:String):Void
	{
		c_values_postfix = p;
	}
	function get values_postfix():String
	{
		return c_values_postfix;
	}
	//decimal separator
	function set decimal_separator(d:String):Void
	{
		c_values_decimal_separator = d;
	}
	function get decimal_separator():String
	{
		return c_values_decimal_separator;
	}
	//thousand separator
	function set thousand_separator(t:String):Void
	{
		c_values_thousand_separator = t;
	}
	function get thousand_separator():String
	{
		return c_values_thousand_separator;
	}
	//decimal places
	function set decimal_places(n:Number):Void
	{
		c_values_decimal_places = n;
	}
	function get decimal_places():Number
	{
		return c_values_decimal_places;
	}
	//show values
	function set show_values(s:Array):Void
	{
		c_values_show = s;
	}
	//default show values
	function set default_show_values(s:Boolean):Void
	{
		c_values_show_all = s;
	}
	//show values for block
	function set blocks_show_values(s:Array):Void
	{
		c_values_show_block = s;
	}
	//values position
	function set values_position(pos:Array):Void
	{
		c_values_position = pos;
	}
	//default values position
	function set default_values_position(pos:String):Void
	{
		c_values_position_all = pos;
	}
	//values text format
	function set values_text_format(tf:TextFormat):Void
	{
		c_values_text_format = tf;
	}
	function set hints_enabled(s:Boolean):Void
	{
		c_hints_enabled = s;
	}
	//set hints position
	function set hints_horizontal_position(pos:String):Void
	{
		c_hints_horizontal_position = pos;
	}
	function set hints_vertical_position(pos:String):Void
	{
		c_hints_vertical_position = pos;
	}
	//set hints size
	function set hints_width(w:Number):Void
	{
		c_hints_width = w;
	}
	function set hints_height(h:Number):Void
	{
		c_hints_height = h;
	}
	function set hints_auto_size(as:Boolean):Void
	{
		c_hints_auto_size = as;
	}
	//set hints border
	function set hints_border_enabled(e:Boolean):Void
	{
		c_hints_border_enabled = e;
	}
	function set hints_border_color(clr:Number):Void
	{
		c_hints_border_color = clr;
	}
	//set hints background
	function set hints_background_enabled(e:Boolean):Void
	{
		c_hints_background_enabled = e;
	}
	function set hints_background_color(clr:Number):Void
	{
		c_hints_background_color = clr;
	}
	function set hints_text_format(tf:TextFormat):Void
	{
		c_hints_text_format = tf;
	}
	//set urls
	function set urls(u:Array):Void
	{
		c_urls = u;
	}
	//set urls target	
	function set urls_target(ut:Array):Void
	{
		c_urls_target = ut;
	}
	//set default target
	function set default_urls_target(t:String):Void
	{
		c_urls_target_all = t;
	}
	//set sounds
	function set sounds(s:Array):Void
	{
		c_sounds = s;
	}
	//set sounds loops:
	function set sounds_loops(l:Array):Void
	{
		c_sounds_loops = l;
	}
	//set start offset
	function set sounds_offsets(o:Array):Void
	{
		c_sounds_start_pos = o;
	}
	//set default loops cound
	function set default_sound_loops(n:Number):Void
	{
		c_sounds_loops_all = n;
	}
	//set default start offset
	function set default_sound_offset(o:Number):Void
	{
		c_sounds_start_pos_all = o;
	}
	//set background auto color
	function set background_auto_color(e:Boolean):Void
	{
		c_background_auto_color = e;
	}
	function set background_tone(e:Number):Void
	{
		c_background_tone = e;
	}
	function set round_radius(r:Number):Void
	{
		c_round_radius = r;
	}
	//set images
	function set background_image_source(s:Array):Void
	{
		c_background_image_source = s;
	}
	//-----------------------------------------------------
	//constructor
	function horizontal_range_2dcolumn_chart(target_mc:MovieClip)
	{
		var i:Number = 0;
		while (target_mc['chart_'+i]!=undefined)
		{
			i++;
		}
		chart_mc = target_mc.createEmptyMovieClip('chart_'+i,DepthManager.CHART_DEPTH);
		//set defaults
		c_x = 0;
		c_y = 0;
		c_width = 550;
		c_height = 400;
		c_column_space = 10;
		c_block_space = 20;
		c_background_enabled_all = true;
		c_background_alpha_all = 100;
		c_background_color_all = 0xCCCCCC;//?
		c_background_auto_color = true;
		c_background_tone = 0xFFC700;
		c_border_enabled_all = false;
		c_border_color_all = 0x000000;
		c_border_size_all = 1;
		c_border_alpha_all = 100;
		c_animation_enabled = true;
		c_animation_speed = 10;
		c_names_text_format = new TextFormat();
		c_names_text_format.font = 'Verdana';
		c_names_text_format.bold = true;
		c_names_show_all = true;
		c_names_position_all = 'left';
		c_values_text_format = new TextFormat();
		c_values_text_format.font = 'Verdana';
		c_values_prefix = '';
		c_values_postfix = '';
		c_values_show_all = true;
		c_values_decimal_separator = '.';
		c_values_thousand_separator = '';
		c_values_decimal_places = 2;
		c_values_position_all = 'right';
		c_block_names_text_format = new TextFormat();
		c_block_names_text_format.size = 14;
		c_block_names_text_format.font = 'Verdana';
		c_block_names_text_format.bold = true;
		c_hints_enabled = true;
		c_hints_auto_size = false
		c_hints_width = 100;
		c_hints_height = 20;
		c_hints_border_enabled = true;
		c_hints_border_color = 0x000000;
		c_hints_background_enabled = true;
		c_hints_background_color = 0xFEE7C0;
		c_hints_text_format = new TextFormat();
		c_hints_text_format.font = 'Verdana';
		c_hints_text_format.align = 'center';
		c_hints_horizontal_position = 'center';
		c_hints_vertical_position = 'top';
		c_urls_target_all = '_blank';
		c_sounds_loops_all = 1;
		c_sounds_start_pos_all = 0;
		c_animation_type = 'all';
		c_animation_attribute='size';
		c_round_radius = 0;
		c_names_rotation = 0;
		c_names_dx = 0;
		c_names_dy = 0;
		c_values_rotation = 0;
		c_values_dx = 0;
		c_values_dy = 0;
	}
	//-----------------------------------------------------
	//paint chart
	function paint():Void
	{
		chart_mc._x = c_x;
		chart_mc._y = c_y;
		process_values();
		process_background();
		process_border();
		process_names();
		process_urls();
		process_sounds();
		process_values_txt();		
		config_columns();
		view_block_names();
		paint_columns();
	}
	
	
	//processing values
	public function initMaxMin():Void {
		this.process_values();
	}
	
	//processing values
	private function process_values():Void
	{
		//get max and min
		var lib:library.functions = new library.functions();
		c_max_value = lib.getMax(c_open_values);
		c_min_value = lib.getMin(c_open_values);
		if (c_max_value<lib.getMax(c_close_values))
		{
			c_max_value = lib.getMax(c_close_values);
		}
		if (c_min_value>lib.getMin(c_close_values))
		{
			c_min_value = lib.getMin(c_close_values);
		}
		if ((c_max_value>c_grid_max) or (c_grid_max==undefined))
		{
			c_grid_max = c_max_value;
		}
		if ((c_min_value<c_grid_min) or (c_grid_min==undefined))
		{
			c_grid_min = c_min_value;
		}		
		//set scale and zero y position
		if ((c_grid_max>0) and (c_grid_min>0))
		{
			c_grid_min = 0;
		}
		if ((c_grid_min<0) and (c_grid_max<0))
		{
			c_grid_max = 0;
		}
		c_values_scale = c_width/(c_grid_max - c_grid_min);
		c_zero_x = - c_values_scale * c_grid_min;
		//---------------------------------
	}
	//configure backgrounds
	private function process_background():Void
	{
		var i:Number;
		var j:Number;
		if (c_background_auto_color)
		{
			var clr_lib = new library.color_ext();
			var tmp_colors:Array = clr_lib.getColors(c_background_tone,c_open_values[0].length);
		}
		if (c_background_enabled==undefined)
		{
			c_background_enabled = new Array();
		}
		if (c_background_color==undefined)
		{
			c_background_color = new Array();
		}
		if (c_background_alpha==undefined)
		{
			c_background_alpha = new Array();
		}
		for (i=0;i<c_open_values.length;i++)
		{
			if (c_background_enabled[i]==undefined)
			{
				c_background_enabled[i] = new Array();
			}
			if (c_background_color[i]==undefined)
			{
				c_background_color[i] = new Array();
			}
			if (c_background_alpha[i]==undefined)
			{
				c_background_alpha[i] = new Array();
			}
			for (j=0;j<c_open_values[i].length;j++)
			{
				//background enabled
				if (c_background_enabled[i][j]==undefined)
				{
					if (c_background_enabled_block[i]!=undefined)
					{
						c_background_enabled[i][j] = c_background_enabled_block[i];
					}else
					{
						c_background_enabled[i][j] = c_background_enabled_all;
					}
				}
				//background color
				if (c_background_color[i][j]==undefined)
				{
					if (c_background_auto_color)
					{
						c_background_color[i][j] = tmp_colors[j];
					}else
					{
						if (c_background_color_block[i]!=undefined)
						{
							c_background_color[i][j] = c_background_color_block[i];
						}else
						{
							c_background_color[i][j] = c_background_color_all;
						}
					}
				}
				//background alpha
				if (c_background_alpha[i][j]==undefined)
				{
					if (c_background_alpha_block[i]!=undefined)
					{
						c_background_alpha[i][j] = c_background_alpha_block[i];
					}else
					{
						c_background_alpha[i][j] = c_background_alpha_all;
					}
				}
				//---------------
			}
		}
	}
	//configure borders
	private function process_border():Void
	{
		var i:Number;
		var j:Number;
		if (c_border_enabled==undefined)
		{
			c_border_enabled = new Array();
		}
		if (c_border_color==undefined)
		{
			c_border_color = new Array();
		}
		if (c_border_alpha==undefined)
		{
			c_border_alpha = new Array();
		}
		if (c_border_size==undefined)
		{
			c_border_size = new Array();
		}
		for (i=0;i<c_open_values.length;i++)
		{
			if (c_border_enabled[i]==undefined)
			{
				c_border_enabled[i] = new Array();
			}
			if (c_border_color[i]==undefined)
			{
				c_border_color[i] = new Array();
			}
			if (c_border_alpha[i]==undefined)
			{
				c_border_alpha[i] = new Array();
			}
			if (c_border_size[i]==undefined)
			{
				c_border_size[i] = new Array();
			}
			for (j=0;j<c_open_values[i].length;j++)
			{
				//border enabled
				if (c_border_enabled[i][j]==undefined)
				{
					if (c_border_enabled_block[i]!=undefined)
					{
						c_border_enabled[i][j] = c_border_enabled_block[i];
					}else
					{
						c_border_enabled[i][j] = c_border_enabled_all;
					}
				}
				//border color
				if (c_border_color[i][j]==undefined)
				{
					if (c_border_color_block[i]!=undefined)
					{
						c_border_color[i][j] = c_border_color_block[i];
					}else
					{
						c_border_color[i][j] = c_border_color_all;
					}
				}
				//border alpha
				if (c_border_alpha[i][j]==undefined)
				{
					if (c_border_alpha_block[i]!=undefined)
					{
						c_border_alpha[i][j] = c_border_alpha_block[i];
					}else
					{
						c_border_alpha[i][j] = c_border_alpha_all;
					}
				}
				//border size
				if (c_border_size[i][j]==undefined)
				{
					if (c_border_size_block[i]!=undefined)
					{
						c_border_size[i][j] = c_border_size_block[i];
					}else
					{
						c_border_size[i][j] = c_border_size_all;
					}
				}
				//---------------
			}
		}
	}
	//configure names
	private function process_names():Void
	{
		var i:Number;
		var j:Number;
		if (c_names_show==undefined)
		{
			c_names_show = new Array();
		}
		if (c_names_position==undefined || !(c_names_position instanceof Array))
		{
			c_names_position = new Array();
		}
		for (i=0;i<c_open_values.length;i++)
		{
			if (c_names_show[i]==undefined)
			{
				c_names_show[i] = new Array();
			}
			if (c_names_position[i]==undefined)
			{
				c_names_position[i] = new Array();
			}
			for (j=0;j<c_open_values[i].length;j++)
			{
				if (c_names_text[i][j]!=undefined)
				{
					if (c_names_show[i][j]==undefined)
					{
						if (c_names_show_block[i]!=undefined)
						{
							c_names_show[i][j] = c_names_show_block[i];
						}else
						{
							c_names_show[i][j] = c_names_show_all;
						}
					}
				}
				if (c_names_position[i][j]==undefined)
				{
					c_names_position[i][j] = c_names_position_all;
				}
			}
		}
	}
	//configure values text
	private function process_values_txt():Void
	{
		var lib:library.functions = new library.functions();
		c_values_text = new Array();
		var i:Number;
		var j:Number;
		if (c_values_show==undefined)
		{
			c_values_show = new Array();
		}
		if (c_values_position==undefined)
		{
			c_values_position = new Array();
		}
		for (i=0;i<c_open_values.length;i++)
		{
			if (c_values_show[i]==undefined)
			{
				c_values_show[i] = new Array();
			}
			if (c_values_position[i]==undefined)
			{
				c_values_position[i] = new Array();
			}
			c_values_text[i] = new Array();
			for (j=0;j<c_open_values[i].length;j++)
			{
				c_values_text[i][j] = 'open: '+lib.config_text(String(c_open_values[i][j]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				c_values_text[i][j] += ',close: '+lib.config_text(String(c_close_values[i][j]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				if (c_values_show[i][j]==undefined)
				{
					if (c_values_show_block[i]!=undefined)
					{
						c_values_show[i][j] = c_values_show_block[i];
					}else
					{
						c_values_show[i][j] = c_values_show_all;
					}
				}
				if (c_values_position[i][j]==undefined)
				{
					c_values_position[i][j] = c_values_position_all;
				}
			}
		}
	}
	//configure urls targets
	private function process_urls():Void
	{
		var i:Number;
		var j:Number;
		if (c_urls_target==undefined)
		{
			c_urls_target = new Array();
		}
		for (i=0;i<c_open_values.length;i++)
		{
			if (c_urls_target[i]==undefined)
			{
				c_urls_target[i] = new Array();
			}
			for (j=0;j<c_open_values[i].length;j++)
			{
				if (c_urls[i][j]!=undefined)
				{
					if (c_urls_target[i][j]==undefined)
					{
						c_urls_target[i][j] = c_urls_target_all;
					}
				}
			}
		}
	}
	//configure sounds
	private function process_sounds():Void
	{
		var i:Number;
		var j:Number;
		if (c_sounds_loops==undefined)
		{
			c_sounds_loops = new Array();
		}
		if (c_sounds_start_pos==undefined)
		{
			c_sounds_start_pos = new Array();
		}
		for (i=0;i<c_open_values.length;i++)
		{
			if (c_sounds_loops[i]==undefined)
			{
				c_sounds_loops[i] = new Array();
			}
			if (c_sounds_start_pos[i]==undefined)
			{
				c_sounds_start_pos[i] = new Array();
			}
			for (j=0;j>c_open_values[i].length;j++)
			{
				if (c_sounds[i][j]!=undefined)
				{
					if (c_sounds_loops[i][j]==undefined)
					{
						c_sounds_loops[i][j] = c_sounds_loops_all;
					}
					if (c_sounds_start_pos[i][j]==undefined)
					{
						c_sounds_start_pos[i][j] = c_sounds_start_pos_all;
					}
				}
			}
		}
	}
	//configure columns
	private function config_columns():Void
	{
		var i:Number;
		var j:Number;
		
		var block_height:Number;
		var column_height:Number;
		
		columns = new Array();
		var tmp:String;
		
		for (i=0;i<c_open_values.length;i++)
		{
			block_height = (c_height - c_block_space*(c_open_values.length-1))/c_open_values.length;
			columns[i] = new Array();
			for (j=0;j<c_open_values[i].length;j++)
			{
				column_height = (block_height - (c_open_values[i].length-1)*c_column_space)/c_open_values[i].length;				
				columns[i][j] = new chart.elements.h_2dcolumn(chart_mc);
				columns[i][j].blockName = c_block_names[i];
				//config column
				columns[i][j].y = (column_height + c_column_space)*j + (block_height + c_block_space)*i;
				columns[i][j].height = column_height;
				columns[i][j].width = Math.abs(c_close_values[i][j] - c_open_values[i][j])*c_values_scale;
				columns[i][j].background_enabled = c_background_enabled[i][j];
				columns[i][j].background_color = c_background_color[i][j];
				columns[i][j].background_alpha = c_background_alpha[i][j];
				columns[i][j].background_image_source = c_background_image_source[i][j];
				columns[i][j].border_enabled = c_border_enabled[i][j];
				columns[i][j].border_size = c_border_size[i][j];
				columns[i][j].border_color = c_border_color[i][j];
				columns[i][j].border_alpha = c_border_alpha[i][j];
				columns[i][j].animation_enabled = c_animation_enabled;
				columns[i][j].animation_speed = c_animation_speed;
				columns[i][j].animation_attribute = c_animation_attribute;
				columns[i][j].round_radius = c_round_radius;
				columns[i][j].name_text = c_names_text[i][j];
				columns[i][j].show_name = c_names_show[i][j];
				columns[i][j].name_text_format = c_names_text_format;
				columns[i][j].value_text = c_values_text[i][j];
				columns[i][j].show_value = c_values_show[i][j];
				columns[i][j].value_text_format = c_values_text_format;
				columns[i][j].show_hint = c_hints_enabled;
				columns[i][j].hint_width = c_hints_width;
				columns[i][j].hint_height = c_hints_height;
				columns[i][j].hint_auto_size = c_hints_auto_size;
				columns[i][j].hint_background_enabled = c_hints_background_enabled;
				columns[i][j].hint_background_color = c_hints_background_color;
				columns[i][j].hint_border_enabled = c_hints_border_enabled;
				columns[i][j].hint_border_color = c_hints_border_color;
				columns[i][j].hint_horizontal_position = c_hints_horizontal_position;
				columns[i][j].hint_vertical_position = c_hints_vertical_position;
				columns[i][j].hint_text_format = c_hints_text_format;
				columns[i][j].hint_border_size = hints_border_size;
				columns[i][j].hint_border_alpha = hints_border_alpha;
				columns[i][j].hint_background_alpha = hints_background_alpha;
				
				if (sets_background_types[i][j] != undefined)
					columns[i][j].background_type = sets_background_types[i][j];
				else
					columns[i][j].background_type = background_type;
					
				if (sets_background_colors[i][j].length > 0)					
					columns[i][j].background_colors = sets_background_colors[i][j];
				else
					columns[i][j].background_colors = background_gradient_colors;
					
				if (sets_background_ratios[i][j].length > 0)		
					columns[i][j].background_ratios = sets_background_ratios[i][j];
				else
					columns[i][j].background_ratios = background_gradient_ratios;
					
				if (sets_background_alphas[i][j].length > 0)				
					columns[i][j].background_alphas = sets_background_alphas[i][j];
				else
					columns[i][j].background_alphas = background_gradient_alphas;
					
				if (sets_background_gradient_types[i][j] != undefined)				
					columns[i][j].background_gradient_type = sets_background_gradient_types[i][j];
				else
					columns[i][j].background_gradient_type = background_gradient_type;
					
				if (sets_background_rotations[i][j] != undefined)
					columns[i][j].background_rotation = sets_background_rotations[i][j];
				else
					columns[i][j].background_rotation = background_gradient_rotation;
				
				if (c_names_rotations[i][j]!=undefined)
				{
					columns[i][j].name_rotation = c_names_rotations[i][j];
				}else
				{
					columns[i][j].name_rotation = c_names_rotation;
				}
				if (c_names_dxs[i][j]!=undefined)
				{
					columns[i][j].name_dx = c_names_dxs[i][j];
				}else
				{
					columns[i][j].name_dx = c_names_dx;
				}
				if (c_names_dys[i][j]!=undefined)
				{
					columns[i][j].name_dy = c_names_dys[i][j];
				}else
				{
					columns[i][j].name_dy = c_names_dy;
				}
				if (c_values_rotations[i][j]!=undefined)
				{
					columns[i][j].value_rotation = c_values_rotations[i][j];
				}else
				{
					columns[i][j].value_rotaion = c_values_rotation;
				}
				if (c_values_dxs[i][j]!=undefined)
				{
					columns[i][j].value_dx = c_values_dxs[i][j];
				}else
				{
					columns[i][j].value_dx = c_values_dx;
				}
				if (c_values_dys[i][j]!=undefined)
				{
					columns[i][j].value_dy = c_values_dys[i][j];
				}else
				{
					columns[i][j].value_dy = c_values_dy;
				}

				var lib:library.functions = new library.functions();
				
				var openValueText:String = lib.config_text(String(c_open_values[i][j]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				var closeValueText:String = lib.config_text(String(c_close_values[i][j]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				
				if (hint_texts[i][j] == undefined)
					columns[i][j].hint_text = lib.createHintTextWithOpenClose(this.hints_text,c_names_text[i][j],openValueText,closeValueText,c_block_names[i]);
				else
					columns[i][j].hint_text = lib.createHintTextWithOpenClose(hint_texts[i][j],c_names_text[i][j],openValueText,closeValueText,c_block_names[i]);

				columns[i][j].url = c_urls[i][j];
				columns[i][j].url_target = c_urls_target[i][j];				
				columns[i][j].sound = c_sounds[i][j];
				columns[i][j].sound_loops = c_sounds_loops[i][j];
				columns[i][j].sound_start_second = c_sounds_start_pos[i][j];
				if (c_close_values[i][j]>c_open_values[i][j])
				{
					columns[i][j].animation_direction = 'right';
					columns[i][j].x = c_zero_x + c_values_scale*c_open_values[i][j];
				}
				else
				{
					columns[i][j].animation_direction = 'left';
					columns[i][j].x = c_zero_x + c_values_scale*c_close_values[i][j];
					if (c_names_position[i][j]=='left')
					{
						c_names_position[i][j] = 'right';
					}else
					{
						if (c_names_position[i][j]=='right')
						{
							c_names_position[i][j] = 'left';
						}
					}
					if (c_values_position[i][j]=='left')
					{
						c_values_position[i][j]='right';
					}else
					{
						if (c_values_position[i][j]=='right')
						{
							c_values_position[i][j]='left';
						}
					}
				}
				columns[i][j].value_position = c_values_position[i][j];
				columns[i][j].name_position = c_names_position[i][j];
				//-------------
			}
		}
	}
	//painting
	private function paint_columns():Void
	{
		var i:Number;
		var j:Number;
		if ((!c_animation_enabled) or (c_animation_type=='all'))
		{
			for (i=0;i<columns.length;i++)
			{
				for (j=0;j<columns[i].length;j++)
				{
					columns[i][j].paint();
				}
			}
		}else
		{
			//set next columns:
			for (i=0;i<c_open_values.length;i++)
			{
				for (j=0;j<(c_open_values[i].length-1);j++)
				{
					columns[i][j].nxt = columns[i][j+1];
				}
				if (i!=(c_open_values.length-1))
				{
					columns[i][(c_open_values[i].length-1)].nxt = columns[i+1][0];
				}
			}
			var tmp:Array = new Array();
			for (i=0;i<c_open_values.length;i++)
			{
				tmp[i] = new Array();
				for (j=0;j<c_open_values[i].length;j++)
				{
					tmp[i][j] = new Object();
					tmp[i][j].onPaintFinish = function(evt:Object)
					{
						if (evt.target.nxt!=undefined)
						{
							evt.target.nxt.paint();
						}
					}
					columns[i][j].addEventListener("onPaintFinish",tmp[i][j]);
				}
			}
			columns[0][0].paint();
		}
	}
	//view block names
	private function view_block_names():Void
	{
		if (!default_show_block_names)
			return;
			
		var i:Number;
		var block_height:Number;
		
		for (i=0;i<c_open_values.length;i++)
		{			
			if (c_block_names[i]!=undefined)
			{
				var blockWidth:Number = 0;
				var blockX:Number;
								
				for (var j:Number = 0;j<this.c_open_values[i].length;j++) {
					if (blockX == undefined || this.columns[i][j].x < blockX) {
						blockX = this.columns[i][j].x;
					}
					trace (this.columns[i][j].width);
					if (this.columns[i][j].x + this.columns[i][j].width > blockWidth)					
						blockWidth = this.columns[i][j].x + this.columns[i][j].width;
				}
				blockWidth -= blockX;
				
				block_height = (c_height - c_block_space*(c_open_values.length-1))/c_open_values.length;			
				var x_pos:Number;
				var y_pos:Number = (block_height + c_block_space)*i + block_height/2;
				var dx:Number = 0;
				if (block_names_placement == 'chart') {
					if (block_names_position == 'right') {
						x_pos = c_width;
					}else {
						x_pos = 0;
						dx = -1;
					}
				}else {
					if (block_names_position == 'right') {
						x_pos = blockX + blockWidth;
						dx = 0;
					}else {
						x_pos = blockX;
						dx = -1;
					}
				}
				var posObj:Object = {x:x_pos,y:y_pos};
				chart_mc.localToGlobal(posObj);
				var ws = workspace.workspace_.getInstance();
				
				ws.addBlockNameTextField(block_names_position,block_names_placement, c_block_names[i],c_block_names_text_format,posObj.x,posObj.y,block_names_dx,block_names_dy,dx,-1/2,block_names_rotation);
			}
		}
	}
	//------------------------
}