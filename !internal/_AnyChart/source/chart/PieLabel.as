﻿class chart.PieLabel {
	
	public var field:TextField;
	public var labelMovie:MovieClip;
	
	public function PieLabel(slice:MovieClip) {
		labelMovie = slice.createEmptyMovieClip('labelMc',slice.getNextHighestDepth());
		labelMovie.createTextField('labelField',1,0,0,0,0);
		field = labelMovie['labelField'];
		field.selectable = false;
		field.autoSize = true;
	}
	
	public var angle:Number;
	public var radius:Number;
	
	public function set allocatedY(value:Number):Void {
		field._y = value - height/2;
	}
	
	public function get connectorX():Number {
		if (Math.cos(angle * Math.PI/180) > 0)
			return field._x;
		else 
			return field._x + field._width;
	}
	
	public function get connectorY():Number {
		return allocatedY;
	}
	
	public var sliceConnectorX:Number;
	public var sliceConnectorY:Number;
	
	public function get allocatedY():Number {
		return field._y + field._height/2;
	}
	
	public function get height():Number {
		return labelMovie._height;
	}
	
	public function get width():Number {
		return labelMovie._width;
	}
	
	public function getLowerY():Number {
		return field._y - field._height;
	}
	
	public function getUpperY():Number {
		return field._y;
	}
}