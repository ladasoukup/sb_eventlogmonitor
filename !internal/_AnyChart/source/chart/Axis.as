﻿class chart.Axis {
	private var max:Number;	
	private var min:Number;
	private var top:Number;
	private var bottom:Number;
	private var maximum_interval:Number = 10;
	private var interval_size:Number;
	
	public function setMaximumInterval(size:Number):Void {
		maximum_interval = size;
		calculate();
	}
	
	public function setData(maxValue:Number, minValue:Number):Void {
		max = maxValue;
		min = minValue;
		calculate();
	}
	
	private function calculate():Void {
		
		if (max == undefined || min == undefined || maximum_interval == undefined)
			return;

		if (max == min) {
//TODO calculate
		}
		
		var maxMagnitude:Number = this.getOrderOfMagnitude(max);
		var minMagnitude:Number = this.getOrderOfMagnitude(min);
			
		var magnitude:Number = maxMagnitude > minMagnitude ? maxMagnitude : minMagnitude;
		
		interval_size = Math.pow(10,magnitude+1);

		this.top = roundTop(max,interval_size);
		this.bottom = roundBottom(min,interval_size);
		
		var c:Number = 0;
		
		while (true) {
			var nInt:Number = reduceInterval();			
			if (nInt == undefined)
				return;
			
			var Top:Number = this.roundTop(max,nInt);
			var Bottom:Number = this.roundBottom(min,nInt);			
			var nQ:Number = (Top - Bottom)/nInt;

			if (nQ > maximum_interval)
				return;

			this.interval_size = nInt;
			this.top = Top;
			this.bottom = Bottom;
			
			if (++c > 100)
				return;
		}
	}
	
	public function getTop():Number {
		return top;		
	}
	
	public function getBottom():Number {
		return bottom;
	}
	
	public function getIntervalSize():Number {
		return interval_size;
	}
	
	private function reduceInterval():Number {
		
		var obj:Object = getENotation(interval_size);
		if (obj.mantissa == 5)
			return interval_size*2/5;
		else if (obj.mantissa == 2 || obj.mantissa == 1)
			return interval_size/2;
		else 
			return;
	}
	
	private function roundTop(val:Number,s:Number):Number {
		if (val == 0)
			return 0;
		return (Math.floor(val/s)+1)*s;
	}
	
	private function roundBottom(val:Number,s:Number):Number {
		if (val == 0)
			return 0;
		return (Math.ceil(val/s)-1)*s;
	}
	
	private function getOrderOfMagnitude(value:Number):Number {
		var obj:Object = getENotation(value);
		return getNumLength(obj.mantissa) + obj.exponent - 1;
	}
	
	private function getNumLength(value:Number):Number {
		return Math.floor(Math.abs(value)).toString().length;
	}
	
	private function getENotation(num:Number):Object {
        var isLZ:Boolean = num < 0;
		num = Math.abs(num);
        var powerTen:Number = Math.floor(Math.log(num) / Math.LN10); 
        if (num == 0) powerTen = 0;
        var tenToPower = Math.pow(10, powerTen);
        var mantissa:Number = num / tenToPower;
		if (mantissa > 10) {
			powerTen ++;
			mantissa -= 10;
		}else if (mantissa == 10) {
			mantissa = 1;
			powerTen ++;
		}
		mantissa *= isLZ ? -1 : 1;		
        return {mantissa:mantissa,exponent:powerTen};
	}
	
	public function getTicksCount():Number {
		return (top - bottom)/interval_size;
	}
}