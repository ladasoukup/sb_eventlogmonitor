﻿class chart.elements.candlestick
{
	
	//------------------------------------------
	//variables
	//position
	private var c_x:Number;
	private var c_high_y:Number;
	private var c_low_y:Number;
	private var c_open_y:Number;
	private var c_close_y:Number;
	
	//size
	private var c_width:Number;
	
	//background
	private var c_background_enabled:Boolean;
	private var c_background_color:Number;
	private var c_background_alpha:Number;
	
	//border
	private var c_border_enabled:Boolean;
	private var c_border_size:Number;
	private var c_border_color:Number;
	private var c_border_alpha:Number;
	
	//hint
	var _hint;
	
	//movie
	private var element_mc:MovieClip;
	//------------------------------------------
	//getters and setters
	//set position
	function set x(new_x:Number):Void
	{
		c_x = new_x;
	}
	function get x():Number {
		return c_x;
	}
	function set open_y(y:Number):Void
	{
		c_open_y = y;
	}
	function set close_y(y:Number):Void
	{
		c_close_y = y;
	}
	function set high_y(y:Number):Void
	{
		c_high_y = y;
	}
	function set low_y(y:Number):Void
	{
		c_low_y = y;
	}
	function get y():Number {
		var minY:Number = c_open_y;
		if (c_close_y < minY)
			minY = c_close_y;
		if (c_high_y < minY)
			minY = c_high_y;
		if (c_low_y < minY)
			minY = c_low_y;
		return minY;
	}
	function get height():Number {
		var h:Number = c_open_y;
		if (c_close_y > h) {
			h = c_close_y;
		}
		if (c_high_y > h) {
			h = c_high_y;
		}
		if (c_low_y > h) {
			h = c_low_y;
		}
		return (h - this.y);
	}
	//set size
	function set width(w:Number):Void
	{
		c_width = w;
	}
	//set background
	function set background_enabled(e:Boolean):Void
	{
		c_background_enabled = e;
	}
	function set background_color(clr:Number):Void
	{
		c_background_color = clr;
	}
	function set background_alpha(a:Number):Void
	{
		c_background_alpha = a;
	}
	//border
	function set border_enabled(e:Boolean):Void
	{
		c_border_enabled = e;
	}
	function set border_size(s:Number):Void
	{
		c_border_size = s;
	}
	function set border_color(clr:Number):Void
	{
		c_border_color = clr;
	}
	function set border_alpha(a:Number):Void
	{
		c_border_alpha = a;
	}
	
	//------------------------------------------
	//constructor
	function candlestick(target_mc:MovieClip)
	{
		var i:Number = 0;
		while (target_mc['candlesticks_mcs_'+i]!=undefined)
		{
			i++;
		}
		element_mc = target_mc.createEmptyMovieClip('candlesticks_mcs_'+i,target_mc.getNextHighestDepth());
		_hint = new library.hint(element_mc);
	}
	//paint
	function paint():Void
	{		
		//possitiom
		element_mc._x = c_x;
		element_mc._y = c_high_y;
		//paint open2close rectangle		
		if (c_border_enabled)
		{
			element_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
			//paint high2low line
			element_mc.moveTo(0,0);
			element_mc.lineTo(0,c_low_y - c_high_y);
		}
		element_mc.moveTo(-c_width/2,c_open_y - c_high_y);
		if (c_background_enabled)
		{
			element_mc.beginFill(c_background_color,c_background_alpha);
		}
		element_mc.lineTo(c_width/2,c_open_y - c_high_y);
		element_mc.lineTo(c_width/2,c_close_y - c_high_y);
		element_mc.lineTo(-c_width/2,c_close_y - c_high_y);
		element_mc.lineTo(-c_width/2,c_open_y - c_high_y);
		if (c_background_enabled)
		{
			element_mc.endFill();
		}
		//--------------------------------		
		_hint.show();
	}
}