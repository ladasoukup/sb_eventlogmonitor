﻿import mx.events.EventDispatcher
class chart.elements.dot
{
	public var blockName:String;
	public var zero_y:Number;
	//variables
	//link for painting next dot
	var nxt;
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;

	//position
	private var d_x:Number;
	private var d_y:Number;
	
	//size
	private var d_radius:Number;
	private var d_type:String;
	
	//image
	private var d_image:String;
	
	//animation
	private var d_animation_enabled:Boolean;
	private var d_animation_speed:Number;
	private var d_animation_attribute:String;
	
	//url onRelese
	private var d_url:String;
	private var d_url_target:String;
	
	//sound onRelese
	private var d_sound:String;
	private var d_sound_loops:Number;
	private var d_sound_offset:Number;
	
	//background
	private var d_background_enabled:Boolean;
	private var d_background_color:Number;
	private var d_background_alpha:Number;
	
	//border
	private var d_border_enabled:Boolean;
	private var d_border_size:Number;
	private var d_border_color:Number;
	private var d_border_alpha:Number;
	
	//name
	private var d_show_name:Boolean;
	private var d_name_text:String;
	private var d_name_position:String;
	private var d_name_text_format:TextFormat;
	private var d_name_rotation:Number;
	private var d_name_dx:Number;
	private var d_name_dy:Number;
	public var name_placement:String;
	public var chart_x:Number;
	public var chart_y:Number;
	public var chart_width:Number;
	public var chart_height:Number;
	
	//value
	private var d_show_value:Boolean;
	private var d_value_text:String;
	private var d_value_position:String;
	private var d_value_text_format:TextFormat;
	private var d_value_rotation:Number;
	private var d_value_dx:Number;
	private var d_value_dy:Number;
	
	//movie
	private var dot_mc:MovieClip;
	
	//hint
	var hint;
	//---------------------------------------------------------
	//getters and setters
	//set dot type
	function set type(t:String):Void
	{
		d_type = t;
	}
	//set x position
	function set x(new_x:Number):Void
	{
		d_x = new_x;
	}
	function get x():Number
	{
		return d_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		d_y = new_y;
	}
	function get y():Number
	{
		return d_y;
	}
	//set radius
	function set radius(r:Number):Void
	{
		r = Number(r);
		if (r<=0)
		{
			_root.showError("Dots radius should be positive");
		}else
		{
			d_radius = r;
		}
	}
	//set backgorund image
	function set image(s:String):Void
	{
		d_image = s;
	}
	//set background
	function set background_enabled(e:Boolean):Void
	{
		d_background_enabled = e;
	}
	//set background color
	function set background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("dots background color should be between 0 and 0xFFFFFF");
		}else
		{
			d_background_color = clr;
		}
	}
	//set background alpha
	function set background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("dots background alpha should be between 0 and 100");
		}else
		{
			d_background_alpha = a;
		}
	}
	//set border
	function set border_enabled(e:Boolean):Void
	{
		d_border_enabled = e;
	}
	//set border size
	function set border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("dots border size should be between 0 and 255");
		}else
		{
			d_border_size = s;
		}
	}
	//set dots bordedr color
	function set border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("dots border color should be between 0 and 0xFFFFFF");
		}else
		{
			d_border_color = clr;
		}
	}
	//set border alpha
	function set border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("dots border alpha should be between 0 and 100");
		}else
		{
			d_border_alpha = a;
		}
	}
	//set name text
	function set name_text(txt:String):Void
	{
		d_name_text = txt;
	}
	function set name_rotation(r:Number):Void
	{
		d_name_rotation = r;
	}
	function  set name_dx(d:Number):Void
	{
		d_name_dx = d;
	}
	function set name_dy(d:Number):Void
	{
		d_name_dy = d;
	}
	function set show_name(e:Boolean):Void
	{
		d_show_name = e;
	}
	function set show_value(e:Boolean):Void
	{
		d_show_value = e;
	}
	function set value_rotation(r:Number):Void
	{
		d_value_rotation = r;
	}
	function set value_dx(d:Number):Void
	{
		d_value_dx = d;
	}
	function set value_dy(d:Number):Void
	{
		d_value_dy = d;
	}
	//set name position
	function set name_position(pos:String):Void
	{
			d_name_position = pos;
			if (d_value_position!=undefined)
			{
				if (d_name_position == d_value_position)
				{
					_root.showError("Dots name position and dots value position can't be equal");
				}
			}
	}
	//set value position
	function set value_position(pos:String):Void
	{
		if ((pos!='top') and (pos!='bottom') and (pos!='center'))
		{
			_root.showError("dots value position values are 'top','bottom' or 'center'");
		}else
		{
			d_value_position = pos;
		}
	}
	//set value text
	function set value_text(txt:String):Void
	{
		d_value_text = txt;
	}
	//set name text format
	function set name_text_format(tf:TextFormat):Void
	{
		d_name_text_format = tf;
	}
	//set value text format
	function set value_text_format(tf:TextFormat):Void
	{
		d_value_text_format = tf;
	}
	//set url
	function set url(u:String):Void
	{
		d_url = u;
	}
	//set url target
	function set url_target(t:String):Void
	{
		d_url_target = t;
	}
	//set sound
	function set sound(s:String):Void
	{
		d_sound = s;
	}
	//set sound loopse
	function set sound_loops(l:Number):Void
	{
		d_sound_loops = l;
	}
	//set sound offset
	function set sound_offset(o:Number):Void
	{
		d_sound_offset = o;
	}
	//set animation
	function set animation_enabled(e:Boolean):Void
	{
		d_animation_enabled = e;
	}
	function set animation_attribute(t:String):Void
	{
		d_animation_attribute = t;
	}
	function set animation_speed(s:Number):Void
	{
		d_animation_speed = s;
	}
	//---------------------------------------------------------
	//constructor
	function dot(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['dot_movie_'+i]!=undefined)
		{
			i++;
		}
		dot_mc = target_mc.createEmptyMovieClip('dot_movie_'+i,target_mc.getNextHighestDepth());
		hint = new library.hint(dot_mc);
		this.name_placement = 'dot';
	}
	
	public static var sinuses:Array;
	public static var cosinuses:Array;
	
	//painting
	function paint():Void
	{
		if (dot.sinuses == undefined) {
			dot.sinuses = new Array();
			dot.cosinuses = new Array();
			for (var i:Number = 0;i<=360;i++) {
				dot.sinuses[i] = Math.sin(i*Math.PI/180);
				dot.cosinuses[i] = Math.cos(i*Math.PI/180);
			}
		}
		dot_mc._x = d_x;
		dot_mc._y = d_y;
		if (!d_animation_enabled)
		{						
			switch (d_type)
			{
				case 'circle':
				  paint_circle();
				break;
				case 'rectangle':
				  paint_rectangle();
				break;
				case 'image':
 				  if ((d_image!=undefined) and (d_image!=''))
	  			  {
				    var img = new objects.image(dot_mc);
				    img.x = -d_radius;
				    img.y = -d_radius;
				    img.position = 'stretch';
				    img.width = d_radius*2;
				    img.height = d_radius*2;
				    img.source = d_image;
				    img.show();
					delete img;
			      }
				break;
			}
			_show_name();
			_show_value();
			dispatchEvent ( {type: 'onPaintFinish'});
		}else
		{
			switch (d_type)
			{
				case 'circle':
  			      switch (d_animation_attribute)
				  {
					 case 'size':
				 		paint_circle_with_size_animation();						
					 break;
					 case 'alpha':
						paint_circle();
						alpha_animation();
					 break;
				  }
				break;
				case 'rectangle':
				  switch (d_animation_attribute)
				  {
					  case 'size':
					    paint_rectangle_width_size_animation();
					  break;
					  case 'alpha':
					    paint_rectangle();
						alpha_animation();
					  break;
				  }
				break;				
				case 'image':
				  if ((d_image!=undefined) and (d_image!=''))
	  			  {
				    var img = new objects.image(dot_mc);
				    img.x = -d_radius;
				    img.y = -d_radius;
				    img.position = 'stretch';
				    img.width = d_radius*2;
				    img.height = d_radius*2;
				    img.source = d_image;
				    img.show();
			      }
				  alpha_animation();
				break;
			}
		}
		hint.show();
		click_event();
	}
	public function hide():Void
	{
		var ths:Object = this;
		dot_mc.onEnterFrame = function()
		{
			this._alpha -= ths.d_animation_speed;
			if (this._alpha<=0)
			{
				this._alpha = 0;
			}
			if (this._alpha==0)
			{
				delete this.onEnterFrame;
			}
		}
	}
	//paint circle dot
	private function paint_circle():Void
	{
		if (_global.paintCircle == undefined) 
			_global.paintCircle = 0;
			
		var paintInit:Number = new Date().getTime();
		
		var i:Number = 0;		
		dot_mc._x = d_x;
		dot_mc._y = d_y;
		var lib = new library.functions();
		var x_pos:Number = d_radius;
		var y_pos:Number = 0;
		if (d_border_enabled)
		{
			dot_mc.lineStyle(d_border_size,d_border_color,d_border_alpha);
		}
		if (d_background_enabled)
		{
			dot_mc.beginFill(d_background_color,d_background_alpha);
		}
		dot_mc.moveTo(x_pos,y_pos);
		var len:Number = 2*Math.PI*d_radius;
		var step:Number = 360/len;
		var n:Number = 0;
		while (i<len) {
			var r:Number = Math.floor(n);
			x_pos = d_radius*dot.cosinuses[r];
			y_pos = d_radius*dot.sinuses[r];
			dot_mc.lineTo(x_pos,y_pos);
			n += step;
			i++;
		}
		if (d_background_enabled)
		{
			dot_mc.endFill();
		}
		var paintEnd:Number = new Date().getTime();
		
		_global.paintCircle += (paintEnd - paintInit);
	}
	//paint rectangle dot
	private function paint_rectangle():Void
	{
		if (d_border_enabled)
		{
			dot_mc.lineStyle(d_border_size,d_border_color,d_border_alpha);
		}
		if (d_background_enabled)
		{
			dot_mc.beginFill(d_background_color,d_background_alpha);
		}
		dot_mc.moveTo(-d_radius,-d_radius);
		dot_mc.lineTo(d_radius,-d_radius);
		dot_mc.lineTo(d_radius,d_radius);
		dot_mc.lineTo(-d_radius,d_radius);
		dot_mc.lineTo(-d_radius,-d_radius);
		if (d_background_enabled)
		{
			dot_mc.endFill();
		}
	}
	//paint circle with size animation
	private function paint_circle_with_size_animation():Void
	{
		var i:Number;
		dot_mc._x = d_x;
		dot_mc._y = d_y;
		var lib = new library.functions();
		var x_pos:Number;
		var y_pos:Number;
		var ths = this;
		var current_pos:Number = 0;
		var last_pos:Number = 0;
		//------------------------------------------------------
		dot_mc.onEnterFrame = function()
		{
			last_pos = current_pos;
			current_pos += ths.d_animation_speed;
			if (current_pos>360)
			{
				current_pos = 360;
			}
			this.lineStyle();
			if (ths.d_bakcground_enabled)
			{
				this.beginFill(ths.d_background_color,ths.d_background_alpha);
			}
			this.moveTo(0,0);
			x_pos = ths.d_radius*dot.cosinuses[(Math.round(last_pos))];
			y_pos = ths.d_radius*dot.sinuses[(Math.round(last_pos))];
			this.lineTo(x_pos,y_pos);
			if (ths.d_border_enabled)
			{
				this.lineStyle(ths.d_border_size,ths.d_border_color,ths.d_border_alpha);
			}
			for (i=last_pos;i<=current_pos;i++)
			{
				x_pos = ths.d_radius*Math.cos(lib.Grad2Rad(i));
				y_pos = ths.d_radius*Math.sin(lib.Grad2Rad(i));
				this.lineTo(x_pos,y_pos);
			}
			x_pos = ths.d_radius*Math.cos(lib.Grad2Rad(current_pos));
			y_pos = ths.d_radius*Math.sin(lib.Grad2Rad(current_pos));
			this.lineTo(x_pos,y_pos);
			this.lineTo(0,0);
			if (ths.c_background_enabled)
			{
				this.endFill();
			}
			if (current_pos==360)
			{
				ths._show_name();
				ths._show_value();
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};				
			}
		}
		//-------------------------------------------------------
	}
	//paint rectangle with size animation
	private function paint_rectangle_width_size_animation():Void
	{
		var ths = this;
		var current_pos:Number = -d_radius;
		var last_pos:Number;
		//----------------------------------
		dot_mc.onEnterFrame = function()
		{
			last_pos = current_pos;
			current_pos += ths.d_animation_speed;
			if (current_pos>ths.d_radius)
			{
				current_pos = ths.d_radius;
			}
			if (ths.d_background_enabled)
			{
				this.beginFill(ths.d_backgorund_color,ths.d_background_alpha);				
			}
			if (ths.d_border_enabled)
			{
				this.lineStyle(ths.d_border_size,ths.d_border_color,ths.d_border_alpha);
			}
			this.lineStyle();
			this.moveTo(-ths.d_radius,last_pos);
			this.lineTo(ths.d_radius,last_pos);
			if (ths.d_border_enabled)
			{
				this.lineStyle(ths.d_border_size,ths.d_border_color,ths.d_border_alpha);
			}			
			this.lineTo(ths.d_radius,current_pos);
			this.lineStyle();
			this.lineTo(-ths.d_radius,current_pos);
			if (ths.d_border_enabled)
			{
				this.lineStyle(ths.d_border_size,ths.d_border_color,ths.d_border_alpha);
			}
			this.lineTo(-ths.d_radius,last_pos);
			if (ths.d_background_enabled)
			{
				this.endFill();
			}
			if (current_pos == ths.d_radius)
			{
				ths._show_name();
				ths._show_value();
				if (ths.d_border_enabled)
				{
					this.lineStyle(ths.d_border_size,ths.d_border_color,ths.d_border_alpha);
					this.moveTo(-ths.d_radius,-ths.d_radius);
					this.lineTo(ths.d_radius,-ths.d_radius);
					this.moveTo(-ths.d_radius,ths.d_radius);
					this.lineTo(ths.d_radius,ths.d_radius);
				}
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};
			}
		}
		//----------------------------------
	}
	private function alpha_animation():Void
	{
		var current_pos:Number = 0;
		var ths = this;
		dot_mc._alpha = 0;
		dot_mc.onEnterFrame = function()
		{
			current_pos += ths.d_animation_speed;
			if (current_pos > 100)
			{
				current_pos = 100;
			}
			this._alpha = current_pos;
			if (current_pos==100)
			{
				ths._show_name();
				ths._show_value();
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};
			}
		}
	}
	//show name
	private function _show_name():Void
	{
		var y_pos:Number;
		var dy:Number = 0;

		if ((d_show_name) and (d_name_text!=undefined))
		{
			if ((this.name_placement == 'dot')||(this.name_placement==undefined))
			{
				switch (d_name_position)
				{
					case 'top':
					  y_pos = d_y - d_radius;
					  dy = 1;
					break;
					case 'bottom':
					  y_pos = d_y + d_radius;
					break;
					case 'center':
					  y_pos = d_y;
					  dy = 0.5;
					break;
				}
			}else
			{
				switch (d_name_position)
				{
					case 'top':
					  y_pos = chart_y;
					  dy = 1;
					break;
					case 'bottom':
					  y_pos = chart_y + chart_height;
					break;
					case 'center':
					  y_pos = chart_y + zero_y;
					  dy = 0.5;
					break;
				}
			}
			
			var pos:Object = {x:d_x,y:y_pos};
			dot_mc._parent.localToGlobal(pos);
			
			var i:Number = 0;
			
			var format:TextFormat;
			if (d_name_rotation == 0) {
				format = d_name_text_format;
			}else {
				var sze:Number = d_name_text_format.size;
				var clr:Number = d_name_text_format.color;
				format = _global[d_name_text_format.font];
				format.size = sze;
				format.color = clr;
			}
			
			var ws = workspace.workspace_.getInstance();
			ws.addSetNameTextField(d_name_position, d_name_text, format,pos.x,pos.y,d_name_rotation,d_name_dx,d_name_dy,(-1/2),-dy, undefined, name_placement == 'chart');
		}
	}
	//show value
	private function _show_value():Void
	{
		var y_pos:Number;
		var dy:Number = 0;
		if ((d_show_value) and (d_value_text!=undefined))
		{
			switch (d_value_position)
			{
				case 'top':
				  y_pos = d_y-d_radius;
				  dy = 1;
				break;
				case 'bottom':
				  y_pos = d_y+d_radius;
				break;
				case 'center':
				  y_pos = d_y;
				  dy = 0.5;
				break;
			}
			var i:Number = 0;
			var pos:Object = {x:d_x,y:y_pos};
			dot_mc._parent.localToGlobal(pos);
			var format:TextFormat;
			if (d_value_rotation == 0) {
				format = d_value_text_format;
			}else {
				var sze:Number = d_value_text_format.size;
				var clr:Number = d_value_text_format.color;
				format = _global[d_value_text_format.font];
				format.size = sze;
				format.color = clr;
			}
			
			var ws = workspace.workspace_.getInstance();
			ws.addSetValueTextField(d_value_position, d_value_text, format,pos.x,pos.y,d_value_rotation,d_value_dx,d_value_dy,(-1/2),-dy);
		}
	}
	//on click event
	private function click_event():Void
	{
		var ths = this;
		if (d_sound!=undefined)
		{
			var snd:Sound = new Sound();
			snd.loadSound(d_sound,false);
		}
		dot_mc.onRelease = function()
		{
			if (ths.d_url!=undefined)
			{
				getURL(ths.d_url,ths.d_url_target);
			}
			if (ths.d_sound!=undefined)
			{
				snd.onLoad = function()
				{
					snd.start(ths.d_sound_offset,ths.d_sound_loops);
				}
			}
		}
	}
}