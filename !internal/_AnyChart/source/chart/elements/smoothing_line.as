﻿import mx.events.EventDispatcher
class chart.elements.smoothing_line
{
	public var blockName:String;
	//variables
	//link for painting next line
	var nxt;
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;

	//position
	private var l_x:Number;
	private var l_y:Number;
	
	//dots positions
	private var l_dots_x:Array;
	private var l_dots_y:Array;
	
	//animation
	private var l_animation_enabled:Boolean;
	private var l_animation_speed:Number;
	private var l_animation_attribute:String;
	
	//line settings
	private var l_line_size:Number;
	private var l_line_color:Number;
	private var l_line_alpha:Number;
	
	private var l_step:Number;
	
	//line movie
	private var line_mc:MovieClip;
	
	//dots
	var dots:Array;
	//----------------------------------
	//getters and setters
	function set x(new_x:Number):Void
	{
		l_x = new_x;
	}
	function set y(new_y:Number):Void
	{
		l_y = new_y;
	}
	function set animation_enabled(e:Boolean):Void
	{
		l_animation_enabled = e;
	}
	function set animation_speed(s:Number):Void
	{
		l_animation_speed = s;
	}
	function set animation_attribute(a:String):Void
	{
		l_animation_attribute = a;
	}
	function set line_size(s:Number):Void
	{
		l_line_size = s;
	}
	function set line_color(clr:Number):Void
	{
		l_line_color = clr;
	}
	function set line_alpha(a:Number):Void
	{
		l_line_alpha = a;
	}
	function set step(s:Number):Void
	{
		l_step = s;
	}
	//----------------------------------
	//constructor
	function smoothing_line(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['lines_movies_'+i]!=undefined)
		{
			i++;
		}
		line_mc = target_mc.createEmptyMovieClip('lines_movies_'+i,target_mc.getNextHighestDepth());
	}
	//paint
	function paint():Void
	{
		var i:Number;
		l_dots_x = new Array();
		l_dots_y = new Array();
		for (i=0;i<dots.length;i++)
		{
			l_dots_x[i] = dots[i].x;
			l_dots_y[i] = dots[i].y;
		}		
		if (l_animation_enabled)
		{
			switch (l_animation_attribute)
			{
				case 'alpha':
				  paint_without_animation();
				  alpha_animation();
				break;
				case 'size':
				  paint_with_animation();
				break;
			}
		}else
		{
			paint_without_animation();
			dispatchEvent ( {type: 'onPaintFinish'});
		}
	}
	//---------
	//paint without animation
	private function paint_without_animation():Void
	{
		line_mc.lineStyle(l_line_size,l_line_color,l_line_alpha);
		var i:Number;
		var j:Number;
		line_mc.moveTo(l_dots_x[0],l_dots_y[0]);
		show_dot(0);
		for (i=1;i<l_dots_y.length;i++)
		{
			j = l_dots_x[i-1];
			while  (j<(l_dots_x[i]-l_step))
			{
				j += l_step;
				line_mc.lineTo(j,getVal(j));
			}
			line_mc.lineTo(l_dots_x[i],l_dots_y[i]);
			show_dot(i);
		}
	}	
	//alpha animation
	private function alpha_animation():Void
	{
		line_mc._alpha = 0;
		var ths = this;
		var current_pos:Number = 0;
		line_mc.onEnterFrame = function()
		{
			current_pos += ths.l_animation_speed;
			if (current_pos>100)
			{
				current_pos = 100;
			}
			this._alpha = current_pos;
			if (current_pos==100)
			{
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};
			}
		}
	}
	//size animation
	private function paint_with_animation():Void
	{
		line_mc.lineStyle(l_line_size,l_line_color,l_line_alpha);
		if (l_animation_speed<l_step)
		{
			l_step = l_animation_speed;
		}
		var i:Number;
		line_mc.moveTo(l_dots_x[0],l_dots_y[0]);
		show_dot(0);
		var current_pos:Number = l_dots_x[0];
		var current_num:Number = 0;
		var last_pos:Number;
		var max_pos:Number = l_dots_x[l_dots_x.length-1] - l_step;
		var ths = this;
		line_mc.onEnterFrame = function()
		{
			last_pos = current_pos;
			current_pos += ths.l_animation_speed;
			if ((current_pos + ths.l_animation_speed)>ths.l_dots_x[current_num+1])
			{
				current_num++;
				current_pos = ths.l_dots_x[current_num];
				ths.show_dot(current_num);
			}
			if (current_pos>max_pos)
			{
				current_pos = max_pos;
			}
			i = last_pos;
			while (i<current_pos)
			{
				i += ths.l_step;
				this.lineTo(i,ths.getVal(i));
			}
			this.lineTo(current_pos,ths.getVal(current_pos));
			if (current_pos==max_pos)
			{
				this.lineTo(ths.l_dots_x[ths.l_dots_x.length-1],ths.l_dots_y[ths.l_dots_y.length-1]);
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function()
				{
				}
			}
		}
	}
	//show dot
	private function show_dot(n:Number):Void
	{
		if ((l_dots_x[n]!=undefined) and (l_dots_y[n]!=undefined))
		{
			dots[n].paint();
		}
	}
	//get value
	private function getVal(t:Number)
	{		
		var res:Number = 0;
		var r:Number;
		var r_1:Number;
		var i:Number;
		var j:Number;
		for (i=0;i<l_dots_x.length;i++)
		{
			r = 1;
			r_1 = 1;
			for (j=0;j<l_dots_x.length;j++)
			{				
				if (j!=i)
				{
					r *= (l_dots_x[i]-l_dots_x[j]);
					r_1 *= (t-l_dots_x[j]);
				}				
			}
			res += l_dots_y[i]*r_1/r;
		}
		return res;
	}
}