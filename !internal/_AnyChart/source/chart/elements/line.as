﻿import mx.events.EventDispatcher
class chart.elements.line
{
	public var blockName:String;
	//variables
	//link for painting next line
	var nxt;
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;

	//position
	private var l_x:Number;
	private var l_y:Number;
	
	//dots positions
	private var l_dots_x:Array;
	private var l_dots_y:Array;
	
	//animation
	private var l_animation_enabled:Boolean;
	private var l_animation_speed:Number;
	private var l_animation_attribute:String;
	
	//line settings
	private var l_line_size:Number;
	private var l_line_color:Number;
	private var l_line_alpha:Number;
	
	//line movie
	private var line_mc:MovieClip;
	private var l_name:String;
	private var l_name_position:Number;
	private var l_name_text_format:TextFormat;
	private var l_show_name:Boolean;
	
	//dots
	var dots:Array;
	//----------------------------------
	//getters and setters
	function set x(new_x:Number):Void
	{
		l_x = new_x;
	}
	function set y(new_y:Number):Void
	{
		l_y = new_y;
	}
	function set animation_enabled(e:Boolean):Void
	{
		l_animation_enabled = e;
	}
	function set animation_speed(s:Number):Void
	{
		l_animation_speed = s;
	}
	function set animation_attribute(a:String):Void
	{
		l_animation_attribute = a;
	}
	function set line_size(s:Number):Void
	{
		l_line_size = s;
	}
	function set line_color(clr:Number):Void
	{
		l_line_color = clr;
	}
	function set line_alpha(a:Number):Void
	{
		l_line_alpha = a;
	}
	function set name(n:String):Void
	{
		l_name = n;
	}
	function set name_position(p:Number):Void
	{
		l_name_position = p;
	}
	function  set name_text_format(tf:TextFormat):Void
	{
		l_name_text_format = tf;
	}
	function set show_name(e:Boolean):Void
	{
		l_show_name = e;
	}
	//----------------------------------
	//constructor
	function line(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['lines_movies_'+i]!=undefined)
		{
			i++;
		}
		line_mc = target_mc.createEmptyMovieClip('lines_movies_'+i,target_mc.getNextHighestDepth());
	}
	//paint
	function paint():Void
	{
		var i:Number;
		l_dots_x = new Array();
		l_dots_y = new Array();
		for (i=0;i<dots.length;i++)
		{
			l_dots_x[i] = Number(dots[i].x);
			l_dots_y[i] = Number(dots[i].y);
			if (String(l_dots_y[i])=='NaN')
			{
				l_dots_y[i] = undefined;
			}
		}
		if (l_animation_enabled)
		{
			switch (l_animation_attribute)
			{
				case 'alpha':
				  paint_without_animation();
				  alpha_animation();
				break;
				case 'size':
				  paint_with_animation();
				break;
			}
		}else
		{
			paint_without_animation();
			dispatchEvent ( {type: 'onPaintFinish'});
		}
		show_block_name();
	}
	//hide line
	function _hide():Void
	{
		var current_pos:Number = 100;
		var ths:Object = this;
		var i:Number;
		for (i=0;i<dots.length;i++)
		{
			dots[i].hide();
		}
		line_mc.onEnterFrame = function()
		{
			current_pos -= ths.l_animation_speed;
			if (current_pos<=0)
			{
				current_pos = 0;
			}
			this._alpha = current_pos;
			if (current_pos==0)
			{
				ths.dispatchEvent ( {type: 'onHideFinish'});
				this.onEnterFrame = function(){};
			}
		}
		line_mc['line_name_tf'].removeTextField();
	}
	//---------
	//paint without animation
	private function paint_without_animation():Void
	{
		line_mc.lineStyle(l_line_size,l_line_color,l_line_alpha);
		var s:Number = 0;//start valid position
		var e:Number = l_dots_x.length - 1;//end valid position
		var i:Number;
		while ((l_dots_y[s]==undefined || isNaN(l_dots_y[s])) && s < e)
		{
			s++;
		}
		while ((l_dots_y[e]==undefined || isNaN(l_dots_y[s])) && e > s)
		{
			e--;
		}
		line_mc.moveTo(l_dots_x[s],l_dots_y[s]);
		show_dot(s);
		for (i=s;i<=e;i++)
		{
			if (l_dots_y[i]!=undefined)
			{
				line_mc.lineTo(l_dots_x[i],l_dots_y[i]);				
			}else
			{
				while (l_dots_y[i]==undefined)
				{
					i++;
					if (i >= e)
						return;
				}
				line_mc.moveTo(l_dots_x[i],l_dots_y[i]);
			}
			show_dot(i);
		}
	}	
	//alpha animation
	private function alpha_animation():Void
	{
		line_mc._alpha = 0;
		var ths = this;
		var current_pos:Number = 0;
		line_mc.onEnterFrame = function()
		{
			current_pos += ths.l_animation_speed;
			if (current_pos>100)
			{
				current_pos = 100;
			}
			this._alpha = current_pos;
			if (current_pos==100)
			{
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};
			}
		}
	}
	//size animation
	private function paint_with_animation():Void
	{
		var dx:Array = new Array();
		var dy:Array = new Array();
		var d_x:Number;
		var d_y:Number;
		var i:Number;
		var l:Number;
		for (i=0;i<(l_dots_y.length-1);i++)
		{
			d_x = l_dots_x[i+1]-l_dots_x[i];
			d_y = l_dots_y[i+1]-l_dots_y[i];
			if ((l_dots_y[i+1]==undefined)||(l_dots_y[i]==undefined))
			{
				d_y = 0;
			}
			l = Math.sqrt(d_x*d_x + d_y*d_y);
			dx[i] = l_animation_speed*d_x/l;
			dy[i] = l_animation_speed*d_y/l;
		}
		//set start position
		show_dot(0);
		//set end position
		var e:Number = l_dots_y.length-1;
		while (l_dots_y[e]==undefined)
		{
			e--;
		}
		//-------------------
		var ths = this;
		var current_x_position = l_dots_x[0];
		var last_x_position = l_dots_x[0];
		var current_y_position = l_dots_y[0];
		var last_y_position = l_dots_y[0];
		var current_dot = 0;
		var d:Number;
		var end_x:Number = l_dots_x[e];
		var end_y:Number = l_dots_y[e];
		line_mc.moveTo(last_x_position,last_y_position);
		//paint
		line_mc.onEnterFrame = function()
		{
			this.lineStyle(ths.l_line_size,ths.l_line_color,ths.l_line_alpha);
			if ((ths.l_dots_y[current_dot]==undefined) or (ths.l_dots_y[current_dot+1]==undefined))
			{
				this.lineStyle();
			}
			last_x_position = current_x_position;
			last_y_position = current_y_position;
			current_x_position += dx[current_dot];
			current_y_position += dy[current_dot];
			d = (ths.l_dots_x[current_dot+1] - current_x_position)*(ths.l_dots_x[current_dot+1] - current_x_position);
			d += (ths.l_dots_y[current_dot+1] - current_y_position)*(ths.l_dots_y[current_dot+1] - current_y_position);
			d = Math.sqrt(d);
			if (String(d)=='NaN')
			{
				d = ths.l_dots_x[current_dot+1] - current_x_position;
				current_y_position = 0;
			}
			if (d<ths.l_animation_speed)
			{
				current_dot++;
				ths.show_dot(current_dot);
				current_x_position = ths.l_dots_x[current_dot];
				current_y_position = ths.l_dots_y[current_dot];
				this.lineTo(current_x_position,current_y_position);									
			}
			this.moveTo(last_x_position,last_y_position);
			this.lineTo(current_x_position,current_y_position);
			if (current_x_position==end_x && current_y_position == end_y)
			{
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};
			}
		}
	}
	//show dot
	private function show_dot(n:Number):Void
	{
		if ((l_dots_x[n]!=undefined) and (l_dots_y[n]!=undefined))
		{
			dots[n].paint();
		}
	}
	//show line name
	private function show_block_name():Void
	{
		if ((l_show_name)&&(l_name!=undefined))
		{
			var f = new library.functions();
			var min_x:Number = f.getMin(l_dots_x);
			var max_x:Number = f.getMax(l_dots_x);
			var h:Number = l_name_text_format.getTextExtent(l_name).textFieldHeight;
			line_mc.createTextField('line_name_tf',line_mc.getNextHighestDepth(),min_x,l_name_position,(max_x-min_x),h);
			var block_name_tf:TextField = line_mc['line_name_tf'];
			block_name_tf.selectable = false;
			block_name_tf.text = l_name;
			block_name_tf.setTextFormat(l_name_text_format);
		}
	}
}