﻿import chart.PieLabel;
import chart.PieLabelsDistributor;
import mx.events.EventDispatcher
import flash.geom.Matrix;

class chart.elements.doughnut_2dsector
{
	
	public var animation_attribute:String = 'size';
	
	//link for painting next sector
	var nxt;
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;
	
	//position
	private var s_x:Number;
	private var s_y:Number;
	
	//radius
	private var s_radius:Number;
	private var s_inner_radius:Number;
	private var s_dr:Number;
	
	//start and end alphas
	private var s_start_alpha:Number;
	private var s_end_alpha:Number;
	
	//border settings
	private var s_border_enabled:Boolean;
	private var s_border_size:Number;
	private var s_border_color:Number;
	private var s_border_alpha:Number;
	
	//background settings
	private var s_background_enabled:Boolean;
	private var s_background_color:Number;
	private var s_background_alpha:Number;
	
	//animation settings
	private var s_animation_enabled:Boolean;
	private var s_animation_speed:Number;
	
	//caption
	private var s_caption_text:String;
	private var s_caption_text_format:TextFormat;
	private var s_caption_position:Number;
	private var s_caption_rotation:Number;
	private var s_caption_dx:Number;
	private var s_caption_dy:Number;
	
	//link
	private var s_url:String;
	private var s_url_target:String;
	private var s_sound:String;
	private var s_sound_loops:Number;
	private var s_sound_offset:Number;
	
	public var background_colors:Array;
	public var background_ratios:Array;
	public var background_alphas:Array;
	public var background_type:String = 'solid';
	public var background_rotation:Number;
	public var background_gradient_type:String = 'radial';

	//library
	private var lib;
	
	//movie
	private var sector_mc:MovieClip;
	
	var _hint;
	//--------------------------------------------------------
	//getters and setters
	//set x position
	function set x(new_x:Number):Void
	{
		s_x = new_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		s_y = new_y;
	}
	//set radius
	function set radius(r:Number):Void
	{
		if (r<=0)
		{
			_root.showError("pie chart radius should be positive");
		}else
		{
			s_radius = r;
		}
	}
	//set inner radius
	function set inner_radius(r:Number):Void
	{
		if (r<=0)
		{
			_root.showError("inner radius should be positive");
		}else
		{
			s_inner_radius = r;
		}
	}
	//set start alpha
	function set start_alpha(a:Number):Void
	{
		s_start_alpha = a;
	}
	//set end alpha
	function set end_alpha(a:Number):Void
	{
		s_end_alpha = a;
	}
	//set border enabled
	function set border_enabled(e:Boolean):Void
	{
		s_border_enabled = e;
	}
	//set border size
	function set border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("pie chart border size should be between 0 and 255");
		}else
		{
			s_border_size = s;
		}
	}
	function set dr(d:Number):Void
	{
		s_dr = d;
	}
	//set border color
	function set border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("pie chart border color should be between 0 and 0xFFFFFF");
		}else
		{
			s_border_color = clr;
		}			
	}
	//set border alpha
	function set border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("pie chart border alpha should be between 0 and 100");
		}else
		{
			s_border_alpha = a;
		}
	}
	//set backgorund enabled
	function set background_enabled(e:Boolean):Void
	{
		s_background_enabled = e;
	}
	//set background color
	function set background_color(clr:Number):Void
	{
		if ((clr<0x000000) or (clr>0xFFFFFF))
		{
			_root.showError("pie chart border color should be between 0 and 0xFFFFFF");
		}else
		{
			s_background_color = clr;
		}
	}
	//set background opacity
	function set background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("pie chart background alpha should be between 0 and 100");
		}else
		{
			s_background_alpha = a;
		}
	}
	//set animation enabled
	function set animation_enabled(e:Boolean):Void
	{
		s_animation_enabled = e;
	}
	//set animation speed
	function set animation_speed(s:Number):Void
	{
		if (s<=0)
		{
			_root.showError("animation speed values should be positive");
		}else
		{
			s_animation_speed = s;
		}
	}
	//set caption
	function set caption_text(txt:String):Void
	{
		s_caption_text = txt;
	}
	function set caption_rotation(r:Number):Void
	{
		s_caption_rotation = r;
	}
	function set caption_dx(d:Number):Void
	{
		s_caption_dx = d;
	}
	function set caption_dy(d:Number):Void
	{
		s_caption_dy = d;
	}
	function set caption_text_format(tf:TextFormat):Void
	{
		s_caption_text_format = tf;
	}
	function set caption_position(pos:Number):Void
	{
		s_caption_position = pos;
	}
	function set url(u:String):Void
	{
		s_url = u;
	}
	function set url_target(t:String):Void
	{
		s_url_target = t;
	}
	function set sound(s:String):Void
	{
		s_sound = s;
	}
	function set sound_loops(l:Number):Void
	{
		s_sound_loops = l;
	}
	function set sound_offset(o:Number):Void
	{
		s_sound_offset = o;
	}
	//--------------------------------------------------------
	//constructor
	function doughnut_2dsector(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['sector_'+i]!=undefined)
		{
			i++;
		}
		sector_mc = target_mc.createEmptyMovieClip('sector_movie_'+i,target_mc.getNextHighestDepth());		
		//-----------
		lib = new library.functions();
		_hint = new library.hint(sector_mc);
	}
	//painting
	public var onPaintFinish:Function;
	function paint():Void
	{
		sector_mc._x = s_x;
		sector_mc._y = s_y;
		if (s_animation_enabled)
		{
			if (animation_attribute == 'size')
				paint_with_animation();
			else
				paint_with_alpha_animation();
		}else
		{
			paint_without_animation();
			show_caption();
			onPaintFinish();
			dispatchEvent ( {type: 'onPaintFinish'});
		}
		_hint.show();
		click_event();
		select();
	}
	//paint without animation
	private function paint_without_animation():Void
	{
		var i:Number;
		var x_pos:Number;
		var y_pos:Number;		
		if (s_border_enabled)
		{
			sector_mc.lineStyle(s_border_size,s_border_color,s_border_alpha);
		}
		x_pos = s_inner_radius*Math.cos(lib.Grad2Rad(s_start_alpha));
		y_pos = s_inner_radius*Math.sin(lib.Grad2Rad(s_start_alpha));
		sector_mc.moveTo(x_pos,y_pos);
		if (s_background_enabled) {
			if (background_type == 'solid')
				sector_mc.beginFill(s_background_color,s_background_alpha);
			else {
				var background_matrix:Matrix = new Matrix();
				background_matrix.createGradientBox(s_radius*2,s_radius*2,background_rotation,-s_radius,-s_radius)
				sector_mc.beginGradientFill(background_gradient_type,background_colors,background_alphas,background_ratios,background_matrix);
			}
		}
		//------------------------		
		x_pos = s_radius*Math.cos(lib.Grad2Rad(s_start_alpha));
		y_pos = s_radius*Math.sin(lib.Grad2Rad(s_start_alpha));
		sector_mc.lineTo(x_pos,y_pos);
		for (i=s_start_alpha;i<=s_end_alpha;i++)
		{
			x_pos = s_radius*Math.cos(lib.Grad2Rad(i));
			y_pos = s_radius*Math.sin(lib.Grad2Rad(i));
			sector_mc.lineTo(x_pos,y_pos);
		}
		x_pos = s_radius*Math.cos(lib.Grad2Rad(s_end_alpha));
		y_pos = s_radius*Math.sin(lib.Grad2Rad(s_end_alpha));
		sector_mc.lineTo(x_pos,y_pos);
		//inner part
		x_pos = s_inner_radius*Math.cos(lib.Grad2Rad(s_end_alpha));
		y_pos = s_inner_radius*Math.sin(lib.Grad2Rad(s_end_alpha));
		sector_mc.lineTo(x_pos,y_pos);
		for (i=s_end_alpha;i>=s_start_alpha;i--)
		{
			x_pos = s_inner_radius*Math.cos(lib.Grad2Rad(i));
			y_pos = s_inner_radius*Math.sin(lib.Grad2Rad(i));
			sector_mc.lineTo(x_pos,y_pos);
		}
		x_pos = s_radius*Math.cos(lib.Grad2Rad(s_start_alpha));
		y_pos = s_radius*Math.sin(lib.Grad2Rad(s_start_alpha));
		sector_mc.lineTo(x_pos,y_pos);
		x_pos = s_radius*Math.cos(lib.Grad2Rad(s_start_alpha));
		y_pos = s_radius*Math.sin(lib.Grad2Rad(s_start_alpha));
		sector_mc.lineTo(x_pos,y_pos);
		//------------------------
		if (s_background_enabled)
		{
			sector_mc.endFill();
		}

	}
	//----------------------------------------------------------------
	private function paint_with_animation():Void
	{
		var current_position:Number = s_start_alpha;
		var last_position:Number = s_start_alpha;
		var ths = this;
		var x_pos:Number = s_radius*Math.cos(lib.Grad2Rad(s_start_alpha));		
		var y_pos:Number = s_radius*Math.sin(lib.Grad2Rad(s_start_alpha));
		var start_x_pos:Number = s_inner_radius*Math.cos(lib.Grad2Rad(s_start_alpha));		
		var start_y_pos:Number = s_inner_radius*Math.sin(lib.Grad2Rad(s_start_alpha));
		var i:Number;
		if (s_border_enabled)
		{
			var border_mc:MovieClip = sector_mc.createEmptyMovieClip('border_movie_',sector_mc.getNextHighestDepth());
			border_mc.lineStyle(s_border_size,s_border_color,s_border_alpha);
			border_mc.moveTo(s_inner_radius,0);
			border_mc.lineTo(s_radius,0);
			border_mc._rotation = current_position;
			sector_mc.lineStyle(s_border_size,s_border_color,s_border_alpha);
			sector_mc.moveTo(start_x_pos,start_y_pos);
			sector_mc.lineTo(x_pos,y_pos);
		}
		sector_mc.onEnterFrame = function()
		{
			last_position = current_position;
			current_position+=ths.s_animation_speed;
			if (current_position>ths.s_end_alpha)
			{
				current_position = ths.s_end_alpha;
			}
			if (ths.s_border_enabled)
			{
				border_mc._rotation = current_position;
			}
			x_pos = ths.s_inner_radius*Math.cos(ths.lib.Grad2Rad(last_position));
			y_pos = ths.s_inner_radius*Math.sin(ths.lib.Grad2Rad(last_position));
			this.moveTo(x_pos,y_pos);
			if (ths.s_background_enabled)
			{
				if (ths.background_type == 'solid')
					this.beginFill(ths.s_background_color,ths.s_background_alpha);
				else {
					var background_matrix:Matrix = new Matrix();
					background_matrix.createGradientBox(ths.s_radius*2,ths.s_radius*2,ths.background_rotation,-ths.s_radius,-ths.s_radius)
					this.beginGradientFill(ths.background_gradient_type,ths.background_colors,ths.background_alphas,ths.background_ratios,background_matrix);
				}
			}
			this.lineStyle();
			x_pos = ths.s_radius*Math.cos(ths.lib.Grad2Rad(last_position));
			y_pos = ths.s_radius*Math.sin(ths.lib.Grad2Rad(last_position));
			this.lineTo(x_pos,y_pos);
			if (ths.s_border_enabled)
			{
				this.lineStyle(ths.s_border_size,ths.s_border_color,ths.s_border_alpha);
			}
			for (i=last_position;i<=current_position;i++)
			{
				x_pos = ths.s_radius*Math.cos(ths.lib.Grad2Rad(i));
				y_pos = ths.s_radius*Math.sin(ths.lib.Grad2Rad(i));
				this.lineTo(x_pos,y_pos);
			}
			if (i!=current_position)
			{
				x_pos = ths.s_radius*Math.cos(ths.lib.Grad2Rad(current_position));
				y_pos = ths.s_radius*Math.sin(ths.lib.Grad2Rad(current_position));
				this.lineTo(x_pos,y_pos);
			}
			this.lineStyle();
			x_pos = ths.s_inner_radius*Math.cos(ths.lib.Grad2Rad(current_position));
			y_pos = ths.s_inner_radius*Math.sin(ths.lib.Grad2Rad(current_position));
			this.lineTo(x_pos,y_pos);
			//paint inner part
			if (ths.s_border_enabled)
			{
				this.lineStyle(ths.s_border_size,ths.s_border_color,ths.s_border_alpha);
			}
			for (i=current_position;i>=last_position;i--)
			{
				x_pos = ths.s_inner_radius*Math.cos(ths.lib.Grad2Rad(i));
				y_pos = ths.s_inner_radius*Math.sin(ths.lib.Grad2Rad(i));
				this.lineTo(x_pos,y_pos);
			}
			if (i!=last_position)
			{
				x_pos = ths.s_inner_radius*Math.cos(ths.lib.Grad2Rad(last_position));
				y_pos = ths.s_inner_radius*Math.sin(ths.lib.Grad2Rad(last_position));
				this.lineTo(x_pos,y_pos);
			}
			this.lineStyle();
			x_pos = ths.s_radius*Math.cos(ths.lib.Grad2Rad(last_position));
			y_pos = ths.s_radius*Math.sin(ths.lib.Grad2Rad(last_position));
			this.lineTo(x_pos,y_pos);
			//----------------
			if (ths.s_background_enabled)
			{
				this.endFill();
			}			
			if (current_position==ths.s_end_alpha)
			{				
				if (ths.s_border_enabled)
				{
					border_mc._rotation = ths.end_alpha;
					this.lineStyle(ths.s_border_size,ths.s_border_color,ths.s_border_alpha);
					x_pos = ths.s_inner_radius*Math.cos(ths.lib.Grad2Rad(ths.s_end_alpha));
					y_pos = ths.s_inner_radius*Math.sin(ths.lib.Grad2Rad(ths.s_end_alpha));
					this.moveTo(x_pos,y_pos);
					x_pos = ths.s_radius*Math.cos(ths.lib.Grad2Rad(ths.s_end_alpha));
					y_pos = ths.s_radius*Math.sin(ths.lib.Grad2Rad(ths.s_end_alpha));
					this.lineTo(x_pos,y_pos);
					x_pos = ths.s_inner_radius*Math.cos(ths.lib.Grad2Rad(ths.s_start_alpha));
					y_pos = ths.s_inner_radius*Math.sin(ths.lib.Grad2Rad(ths.s_start_alpha));
					this.moveTo(x_pos,y_pos);
					x_pos = ths.s_radius*Math.cos(ths.lib.Grad2Rad(ths.s_start_alpha));
					y_pos = ths.s_radius*Math.sin(ths.lib.Grad2Rad(ths.s_start_alpha));
					this.lineTo(x_pos,y_pos);
				}
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				ths.onPaintFinish();
				ths.show_caption();
				delete this.onEnterFrame;
			}
		}
	}
	
	private function paint_with_alpha_animation():Void {
		sector_mc._alpha = 0;
		paint_without_animation();		
		var ths:Object = this;
		sector_mc.onEnterFrame = function():Void {
			this._alpha += ths.s_animation_speed;
			if (this._alpha >= 100) {
				this._alpha = 100;
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				ths.show_caption();
				ths.onPaintFinish();
				this.onEnterFrame = null;
			}
		}			
	}
	
	public var label:PieLabel;
	
	//show caption
	private function show_caption():Void
	{
		if ((s_caption_text!=undefined) and (s_caption_position!=undefined))
		{
			label = new PieLabel(sector_mc);
			label.field.text = s_caption_text;
			if (s_caption_rotation != 0) {
				label.field.embedFonts = true;				
				var tf:TextFormat = _global[s_caption_text_format.font];
				tf.size = s_caption_text_format.size;
				tf.color = s_caption_text_format.color;
				label.field.setTextFormat(tf);
				label.field._rotation = s_caption_rotation;
			}else {
				label.field.setTextFormat(s_caption_text_format);
			}
			
			var dx:Number = 0;
			var dy:Number = -0.5;			
			var centerAngle:Number = (s_start_alpha + s_end_alpha)/2;
			
			label.angle = centerAngle;
			label.radius = s_caption_position*s_radius;
			label.allocatedY = label.radius*Math.sin(centerAngle*Math.PI/180);
			label.sliceConnectorX = s_radius*Math.cos(centerAngle*Math.PI/180);
			label.sliceConnectorY = s_radius*Math.sin(centerAngle*Math.PI/180);
			PieLabelsDistributor.addLabel(label);
		}
	}
	//setlect event
	private function select():Void
	{
		var lib = new library.functions();
		if (s_dr>0)
		{
			var dx:Number = s_dr*Math.cos(lib.Grad2Rad((s_start_alpha+s_end_alpha)/2));
			var dy:Number = s_dr*Math.sin(lib.Grad2Rad((s_start_alpha+s_end_alpha)/2));
			var obj:Object = new Object();
			var ths = this;
			obj.onShow = function()
			{
				ths.sector_mc._x += dx;
				ths.sector_mc._y += dy;
			}
			obj.onUnShow = function()
			{
				ths.sector_mc._x -= dx;
				ths.sector_mc._y -= dy;
			}
			_hint.addEventListener('onShow',obj);
			_hint.addEventListener('onUnShow',obj);
		}
	}
	//click
	private function click_event():Void
	{
		var ths = this;
		if (s_sound!=undefined)
		{
			var sn:Sound = new Sound();
			sn.loadSound(s_sound,false);
		}
		sector_mc.onRelease = function()
		{			
			if (ths.s_url!=undefined)
			{
				getURL(ths.s_url,ths.s_url_target);
			}
			if (ths.s_sound!=undefined)
			{
				sn.onLoad = function()
				{
					sn.start(ths.s_sound_offset,ths.s_sound_loops);
				}
			}
		}
	}
}