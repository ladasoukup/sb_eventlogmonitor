﻿import mx.events.EventDispatcher
class chart.elements.moving_line
{
	//variables
	//link for painting next line
	var nxt;
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;
	
	private var l_line_size:Number;
	private var l_line_color:Number;
	private var l_line_alpha:Number;
	
	private var l_animation_enabled:Boolean;
	private var l_animation_speed:Number;
	private var l_animation_type:String;
	
	//-------------------------------------------------
	private var line_mc:MovieClip;
	//-------------------------------------------------
	public function moving_line(target_mc:MovieClip)
	{
		var i:Number = 0;
		while (target_mc['lines_mcs_'+i]!=undefined)
		{
			i++;
		}
		line_mc = target_mc.createEmptyMovieClip('lines_mcs_'+i,target_mc.getNextHighestDepth());
	}
}