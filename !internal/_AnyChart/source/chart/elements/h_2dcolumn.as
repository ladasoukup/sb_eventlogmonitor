﻿import mx.events.EventDispatcher
import flash.geom.Matrix

class chart.elements.h_2dcolumn
{
	public var blockName:String;
	public var value:Number;
	
	var nxt;
	
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;
	
	//position
	private var c_x:Number;
	private var c_y:Number;
	
	//size
	private var c_width:Number;
	private var c_height:Number;
	
	//round rectangle
	private var c_round_radius:Number;
	
	//background
	private var c_background_enabled:Boolean;
	private var c_background_color:Number;
	private var c_background_alpha:Number;
	private var c_background_image_source:String;
	
	//border
	private var c_border_enabled:Boolean;
	private var c_border_color:Number;
	private var c_border_size:Number;
	private var c_border_alpha:Number;
	
	//animation
	private var c_animation_enabled:Boolean;
	private var c_animation_speed:Number;
	private var c_animation_direction:String;
	
	public var name_placement:String = 'smth';
	
	private var c_animation_attribute:String;
	
	//name
	private var c_name_text:String;
	private var c_name_text_format:TextFormat;
	private var c_name_position:String;
	private var c_show_name:Boolean;
	private var name_text_field:TextField;
	private var c_name_rotation:Number;
	private var c_name_dx:Number;
	private var c_name_dy:Number;
	
	//value
	private var c_value_text:String;
	private var c_value_text_format:TextFormat;
	private var c_value_position:String;
	private var c_show_value:Boolean;
	private var value_text_field:TextField;
	private var c_value_rotation:Number;
	private var c_value_dx:Number;
	private var c_value_dy:Number;
	
	//hint
	private var c_hint_show:Boolean;
	private var c_hint_horizontal_position:String;
	private var c_hint_vertical_position:String;
	private var c_hint_width:Number;
	private var c_hint_height:Number;
	private var c_hint_auto_size:Boolean;
	private var c_hint_background_enabled:Boolean;
	private var c_hint_background_color:Number;
	private var c_hint_border_enabled:Boolean;
	private var c_hint_border_color:Number;
	private var c_hint_text:String;
	private var c_hint_text_format:TextFormat;
	
	public var hint_background_alpha:Number;
	public var hint_border_size:Number;
	public var hint_border_alpha:Number;
	
	//click events
	//url
	private var c_url:String;
	private var c_url_target:String;
	//sound
	private var c_sound:Sound;
	private var c_sound_loops:Number;
	private var c_sound_start_second:Number;
	
	public var background_colors:Array;
	public var background_ratios:Array;
	public var background_alphas:Array;
	public var background_type:String = 'solid';
	public var background_rotation:Number;
	public var background_gradient_type:String = 'linear';
	
	//column movie clip
	private var column_mc:MovieClip;
	//--------------------------------------------------------------
	//getters and setters
	//set x postion
	function set x(new_x:Number):Void
	{
		c_x = new_x;
		column_mc._x = c_x;
	}
	function get x():Number {
		return c_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		c_y = new_y;
		column_mc._y = c_y;
	}
	//set column width
	function set width(w:Number):Void
	{
		c_width = w;
	}
	function get width():Number {
		return c_width;
	}
	//set column height
	function set height(h:Number):Void
	{
		c_height = h;
	}
	//set background enabled
	function set background_enabled(e:Boolean):Void
	{
		c_background_enabled = e;
	}	
	//set background color
	function set background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("Column background color will be positive value below 0xFFFFFF");
		}else
		{
			c_background_color = clr;
		}
	}
	//set background alpha
	function set background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("Column background alpha will be positive value below 100");
		}else
		{
			c_background_alpha = a;
		}
	}

	//set border enabled
	function set border_enabled(e:Boolean):Void
	{
		c_border_enabled = e;
	}
	//set border size
	function set border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("Column border size should be between 0 and 255");
		}else
		{
			c_border_size = s;
		}
	}
	//set border color
	function set border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("Column border color should be between 0 and 0xFFFFFF");
		}else
		{
			c_border_color = clr;
		}
	}
	//set border alpha
	function set border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("Column border should be between 0 and 100");
		}else
		{
			c_border_alpha = a;
		}
	}
	//set animation enabled
	function set animation_enabled(e:Boolean):Void
	{
		c_animation_enabled = e;
	}
	//set animation speed
	function set animation_speed(s:Number):Void
	{
		if ((s<0) or (s>500))
		{
			_root.showError("Column animation speed should be between 0 and 500");
		}else
		{
			c_animation_speed = s;
		}
	}
	//set animation direction
	function set animation_direction(d:String):Void
	{
		c_animation_direction = d;
	}
	//set animation target attribute
	function set animation_attribute(a:String):Void
	{
		c_animation_attribute = a;
	}
	//set name text
	function set name_text(txt:String):Void
	{
		c_name_text = txt;
	}
	function set name_rotation(r:Number):Void
	{
		c_name_rotation = r;
	}
	function set name_dx(d:Number):Void
	{
		c_name_dx = d;
	}
	function set name_dy(d:Number):Void
	{
		c_name_dy = d;
	}
	function set value_rotation(r:Number):Void
	{
		c_value_rotation = r;
	}
	function set value_dx(d:Number):Void
	{
		c_value_dx = d;
	}
	function set value_dy(d:Number):Void
	{
		c_value_dy = d;
	}
	//set name text format
	function set name_text_format(tf:TextFormat):Void
	{
		c_name_text_format = tf;
	}
	//set name position
	function set name_position(pos:String):Void
	{
		c_name_position = pos;
	}
	//set show_name
	function set show_name(s:Boolean):Void
	{
		c_show_name = s;
	}
	//set value text
	function set value_text(txt:String):Void
	{
		c_value_text = txt;
	}
	//set value position
	function set value_position(pos:String):Void
	{
		c_value_position = pos;
	}
	//set value text format
	function set value_text_format(tf:TextFormat):Void
	{
		c_value_text_format = tf;
	}
	//set show_value
	function set show_value(s:Boolean):Void
	{
		c_show_value = s;
	}
	//set hint enabled
	function set show_hint(s:Boolean):Void
	{
		c_hint_show = s;
	}
	//set hint position
	function set hint_horizontal_position(pos:String):Void
	{
		c_hint_horizontal_position = pos;
	}
	function set hint_vertical_position(pos:String):Void
	{
		c_hint_vertical_position = pos;
	}
	//set hint size
	function set hint_width(w:Number):Void
	{
		c_hint_width = w;
	}
	function set hint_height(h:Number):Void
	{
		c_hint_height = h;
	}
	function set hint_auto_size(as:Boolean):Void
	{
		c_hint_auto_size = as;
	}
	//set hint border
	function set hint_border_enabled(e:Boolean):Void
	{
		c_hint_border_enabled = e;
	}
	function set hint_border_color(clr:Number):Void
	{
		c_hint_border_color = clr;
	}
	//set hint background
	function set hint_background_enabled(e:Boolean):Void
	{
		c_hint_background_enabled = e;
	}
	function set hint_background_color(clr:Number):Void
	{
		c_hint_background_color = clr;
	}
	//set hint text
	function set hint_text(txt:String):Void
	{
		c_hint_text = txt;
	}
	function set hint_text_format(tf:TextFormat):Void
	{
		c_hint_text_format = tf;
	}
	//set url onClick
	function set url(u:String):Void
	{
		c_url = u;
	}
	function set url_target(t:String):Void
	{
		c_url_target = t;
	}
	//set sound onClick
	function set sound(s:String):Void
	{
		if (s!=undefined)
		{
			c_sound = new Sound();
			c_sound.loadSound(s,false);		
		}
	}
	//set sound loops
	function set sound_loops(l:Number):Void
	{
		c_sound_loops = l;
	}
	//set sound offset
	function set sound_start_second(s:Number):Void
	{
		c_sound_start_second = s;
	}
	//set background image
	function set background_image_source(src:String):Void
	{
		c_background_image_source = src;
	}
	//set round radius
	function set round_radius(r:Number):Void
	{
		c_round_radius = r;
	}
    //--------------------------------------------------------------
	//class constructor
	function h_2dcolumn(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['column_clip_'+i]!=undefined)
		{
			i++;
		}
		column_mc = target_mc.createEmptyMovieClip('column_clip_'+i,target_mc.getNextHighestDepth());
	}
	//paint
	function paint():Void
	{
		column_mc._x = c_x;
		column_mc._y = c_y;
		if (c_animation_enabled)
		{
			switch (c_animation_attribute)
			{
				case 'size':
 				  paint_with_animation();
				break;
				case 'alpha':
				  paint_with_alpha_animation();
				break;
			}
		}else
		{
			paint_without_animation();
		}
		//show hint
		_show_hint();
		click_event();
	}
	function repaint():Void
	{
		column_mc.clear();
		paint_without_animation();
	}
	//paint column without animation
	private function paint_without_animation():Void
	{
		if (c_round_radius==0)
		{
			if (c_background_image_source!=undefined)
			{
				var img = new objects.image(column_mc);
				img.x = 0;
				img.y = 0;
				img.position = 'stretch';
				img.width = c_width;
				img.height = c_height;
				img.source = c_background_image_source;
				img.show();
			}else
			{
				column_mc.moveTo(0,0);
				if (c_background_enabled)
				{
					if (background_type == 'solid')
						column_mc.beginFill(c_background_color,c_background_alpha);
					else {
						var background_matrix:Matrix = new Matrix();
						background_matrix.createGradientBox(c_width,c_height,background_rotation,0,0)
						column_mc.beginGradientFill(background_gradient_type,background_colors,background_alphas,background_ratios,background_matrix);
				}

				}
				if (c_border_enabled)
				{
					column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
				}
				column_mc.lineTo(c_width,0);
				column_mc.lineTo(c_width,c_height);
				column_mc.lineTo(0,c_height);
				column_mc.lineTo(0,0);
				if (c_background_enabled)
				{
					column_mc.endFill();
				}
			}
		}else
		{
			var i:Number;
			var lib = new library.functions();
			var x_pos:Number;
			var y_pos:Number;
			if (c_animation_direction=='left')
			{
				column_mc.moveTo(c_width,0);
			}else
			{
				column_mc.moveTo(0,0);
			}
			if (c_background_enabled)
			{
				if (background_type == 'solid')
					column_mc.beginFill(c_background_color,c_background_alpha);
				else {
					var background_matrix:Matrix = new Matrix();
					background_matrix.createGradientBox(c_width,c_height,background_rotation,0,0)
					column_mc.beginGradientFill(background_gradient_type,background_colors,background_alphas,background_ratios,background_matrix);
				}

			}
			if (c_border_enabled)
			{
				column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
			}
			if (c_animation_direction=='left')
			{
				column_mc.lineTo(c_width,c_height);
				column_mc.lineTo(c_round_radius,c_height);
				for (i=90;i<=180;i++)
				{
					x_pos = c_round_radius*(Math.cos(lib.Grad2Rad(i)) + 1);
					y_pos = c_height + c_round_radius*(Math.sin(lib.Grad2Rad(i)) - 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(0,c_height - c_round_radius);
				column_mc.lineTo(0,c_round_radius);
				for (i=180;i<=270;i++)
				{
					x_pos = c_round_radius*(Math.cos(lib.Grad2Rad(i)) + 1);
					y_pos = c_round_radius*(Math.sin(lib.Grad2Rad(i)) + 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(c_round_radius,0);
				column_mc.lineTo(c_width,0);
			}else
			{
				column_mc.lineTo(c_width - c_round_radius,0);
				for (i=270;i<=360;i++)
				{
					x_pos = c_width + c_round_radius*(Math.cos(lib.Grad2Rad(i)) - 1);
					y_pos = c_round_radius*(Math.sin(lib.Grad2Rad(i)) + 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(c_width,c_round_radius);
				column_mc.lineTo(c_width,c_height - c_round_radius);
				for (i=0;i<90;i++)
				{
					x_pos = c_width + c_round_radius*(Math.cos(lib.Grad2Rad(i)) - 1);
					y_pos = c_height + c_round_radius*(Math.sin(lib.Grad2Rad(i)) - 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(c_width-c_round_radius,c_height);
				column_mc.lineTo(0,c_height);
				column_mc.lineTo(0,0);
			}
			if (c_background_enabled)
			{
				column_mc.endFill();
			}
		}
		_show_name();
		_show_value();
		dispatchEvent ( {type: 'onPaintFinish'});
	}
	//paint column with animation
	private function paint_with_animation():Void
	{
		var x_p:Number = 0;
		if (c_animation_direction=='left')
		{
			x_p = c_width;
		}
		//paint moveable line
		var border_mc:MovieClip;
		border_mc = column_mc.createEmptyMovieClip('border_movie',column_mc.getNextHighestDepth());												   
		if (c_border_enabled)
		{
			column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
			column_mc.moveTo(x_p,0);
			column_mc.lineTo(x_p,c_height);
			column_mc.moveTo(0,0);
			border_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
		}		
		if (c_round_radius==0)
		{
			border_mc.moveTo(0,0);
			border_mc.lineTo(0,c_height);
		}else
		{
			var i:Number;
			var lib = new library.functions();
			var x_pos:Number;
			var y_pos:Number;
			border_mc.moveTo(0,0);
			if (c_background_enabled)
			{
				if (background_type == 'solid')
					border_mc.beginFill(c_background_color,c_background_alpha);
				else {
					var background_matrix:Matrix = new Matrix();
					background_matrix.createGradientBox(c_width,c_height,background_rotation,0,0)
					border_mc.beginGradientFill(background_gradient_type,background_colors,background_alphas,background_ratios,background_matrix);
				}

			}
			if (c_animation_direction=='left')
			{
				border_mc.lineStyle();
				border_mc.lineTo(0,c_height);
				if (c_border_enabled)
				{
					border_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
				}
				for (i=90;i<=180;i++)
				{
					x_pos = c_round_radius*Math.cos(lib.Grad2Rad(i));
					y_pos = c_height + c_round_radius*(Math.sin(lib.Grad2Rad(i))-1);
					border_mc.lineTo(x_pos,y_pos);
				}
				border_mc.lineTo(-c_round_radius,c_height-c_round_radius);
				border_mc.lineTo(-c_round_radius,c_round_radius);
				for (i=180;i<=270;i++)
				{
					x_pos = c_round_radius*Math.cos(lib.Grad2Rad(i));
					y_pos = c_round_radius*(Math.sin(lib.Grad2Rad(i))+1);
					border_mc.lineTo(x_pos,y_pos);
				}
				border_mc.lineTo(0,0);
			}else
			{
				border_mc.lineStyle();
				border_mc.lineTo(0,c_height);
				if (c_border_enabled)
				{
					border_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
				}
				for (i=90;i>=0;i--)
				{
					x_pos = c_round_radius*Math.cos(lib.Grad2Rad(i));
					y_pos = c_height + c_round_radius*(Math.sin(lib.Grad2Rad(i))-1);
					border_mc.lineTo(x_pos,y_pos);
				}
				border_mc.lineTo(c_round_radius,c_height - c_round_radius);
				border_mc.lineTo(c_round_radius,c_round_radius);
				for (i=360;i>=270;i--)
				{
					x_pos = c_round_radius*Math.cos(lib.Grad2Rad(i));
					y_pos = c_round_radius*(Math.sin(lib.Grad2Rad(i)) + 1);
					border_mc.lineTo(x_pos,y_pos);
				}
				border_mc.lineTo(0,0);
			}
			if (c_background_enabled)
			{
				border_mc.endFill();
			}
		}
		var ths = this;
		//paint column
		var current_position:Number;
		if (c_animation_direction=='left')
		{
			current_position = c_width;
			c_animation_speed = - Math.abs(c_animation_speed);
		}else
		{
			current_position = 0;
			c_round_radius = -c_round_radius;
		}		
		var max_val:Number = c_width - current_position + c_round_radius;
		var last_position:Number = current_position;
		//show name
		border_mc._x =  current_position;
		var show_name_on_end:Boolean = true;
		if (((c_name_position=='left') and (c_animation_direction=='right')) or ((c_name_position=='right') and (c_animation_direction=='left')))
		{
			_show_name();
			show_name_on_end = false;
		}		
		//show value
		var show_value_on_end:Boolean = true;
		if (((c_value_position=='left') and (c_animation_direction=='right')) or ((c_value_position=='left') and (c_animation_direction=='right')))
		{
			_show_value();
			show_value_on_end = false;
		}
		//
		column_mc.onEnterFrame = function()
		{
			last_position = current_position;
			current_position += ths.c_animation_speed;
			if (((current_position>max_val) and (ths.c_animation_direction=='right')) or ((current_position<max_val) and (ths.c_animation_direction=='left')))			
			{
				current_position = max_val;
			}
			this.moveTo(current_position,0);
			if (ths.c_background_enabled)
			{
				if (ths.background_type == 'solid')
					this.beginFill(ths.c_background_color,ths.c_background_alpha);
				else {
					var background_matrix:Matrix = new Matrix();
					background_matrix.createGradientBox(ths.c_width,ths.c_height,ths.background_rotation,0,0)
					this.beginGradientFill(ths.background_gradient_type,ths.background_colors,ths.background_alphas,ths.background_ratios,background_matrix);
				}
			}
			if (ths.c_border_enabled)
			{
				this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
			}
			border_mc._x =  current_position;
			this.lineTo(last_position,0);
			this.lineStyle();
			this.lineTo(last_position,ths.c_height);
			if (ths.c_border_enabled)
			{
				this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
			}
			this.lineTo(current_position,ths.c_height);
			this.lineStyle();
			this.lineTo(current_position,0);
			if (ths.c_background_enabled)
			{
				this.endFill();
			}
			if (current_position == max_val)
			{
				if (ths.c_border_enabled)
				{
					this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
					this.moveTo(ths.c_width - max_val + ths.c_round_radius,0);
					this.lineTo(ths.c_width - max_val + ths.c_round_radius,ths.c_height);
				}				
				if (show_name_on_end)
				{
					ths._show_name();
				}
				if (show_value_on_end)
				{
					ths._show_value();
				}
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				delete this.onEnterFrame;
			}
		}
		//-
	}
	//paint with alpaha animation
	private function paint_with_alpha_animation():Void
	{
		var ths = this;
		if (c_round_radius==0)
		{
			if (c_background_image_source!=undefined)
			{
				var img = new objects.image(column_mc);
				img.x = 0;
				img.y = 0;
				img.position = 'stretch';
				img.width = c_width;
				img.height = c_height;
				img.source = c_background_image_source;
				img.show();
			}else
			{
				column_mc.moveTo(0,0);
				if (c_background_enabled)
				{
					column_mc.beginFill(c_background_color,c_background_alpha);
				}
				if (c_border_enabled)
				{
					column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
				}
				column_mc.lineTo(c_width,0);
				column_mc.lineTo(c_width,c_height);
				column_mc.lineTo(0,c_height);
				column_mc.lineTo(0,0);
				if (c_background_enabled)
				{
					column_mc.endFill();
				}
			}
		}else
		{
			var i:Number;
			var lib = new library.functions();
			var x_pos:Number;
			var y_pos:Number;
			if (c_animation_direction=='left')
			{
				column_mc.moveTo(c_width,0);
			}else
			{
				column_mc.moveTo(0,0);
			}
			if (c_background_enabled)
			{
				column_mc.beginFill(c_background_color,c_background_alpha);
			}
			if (c_border_enabled)
			{
				column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
			}
			if (c_animation_direction=='left')
			{
				column_mc.lineTo(c_width,c_height);
				column_mc.lineTo(c_round_radius,c_height);
				for (i=90;i<=180;i++)
				{
					x_pos = c_round_radius*(Math.cos(lib.Grad2Rad(i)) + 1);
					y_pos = c_height + c_round_radius*(Math.sin(lib.Grad2Rad(i)) - 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(0,c_height - c_round_radius);
				column_mc.lineTo(0,c_round_radius);
				for (i=180;i<=270;i++)
				{
					x_pos = c_round_radius*(Math.cos(lib.Grad2Rad(i)) + 1);
					y_pos = c_round_radius*(Math.sin(lib.Grad2Rad(i)) + 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(c_round_radius,0);
				column_mc.lineTo(c_width,0);
			}else
			{
				column_mc.lineTo(c_width - c_round_radius,0);
				for (i=270;i<=360;i++)
				{
					x_pos = c_width + c_round_radius*(Math.cos(lib.Grad2Rad(i)) - 1);
					y_pos = c_round_radius*(Math.sin(lib.Grad2Rad(i)) + 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(c_width,c_round_radius);
				column_mc.lineTo(c_width,c_height - c_round_radius);
				for (i=0;i<90;i++)
				{
					x_pos = c_width + c_round_radius*(Math.cos(lib.Grad2Rad(i)) - 1);
					y_pos = c_height + c_round_radius*(Math.sin(lib.Grad2Rad(i)) - 1);
					column_mc.lineTo(x_pos,y_pos);
				}
				column_mc.lineTo(c_width-c_round_radius,c_height);
				column_mc.lineTo(0,c_height);
				column_mc.lineTo(0,0);
			}
			if (c_background_enabled)
			{
				column_mc.endFill();
			}
		}
		//-------------------------------------
		var current_alpha:Number = 0;
		column_mc._alpha = 0;
		column_mc.onEnterFrame = function()
		{
			current_alpha += ths.c_animation_speed;
			if (current_alpha>100)
			{
				current_alpha = 10;
			}
			this._alpha = current_alpha;
			if (current_alpha==100)
			{
				ths._show_name();
				ths._show_value();
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};	
			}
		}
	}
	//function for showing name
	private function _show_name():Void
	{
		if ((c_show_name) and (c_name_text!=undefined) and (length(c_name_text)>0))
		{
			var i:Number = 0;
			var x:Number;
			var y:Number = c_y + c_height/2;
			var move_:Number = 0;
			switch (c_name_position)
			{
				case 'left':
				  x = c_x;
				  move_ = 1;
				break;
				case 'right':
				  x = c_x + c_width;
				  move_ = 0;
				break;
				case 'center':
				  x = c_x + c_width/2;
				  move_ = 0.5;
				break;
			}

			
			var format:TextFormat;
			if (c_name_rotation == 0)
				format = c_name_text_format;
			else {
				format = _global[c_name_text_format.font];
				format.size = c_name_text_format.size;
				format.color = c_name_text_format.color;
			}
			
			var pos:Object = {x:x,y:y};
			column_mc._parent.localToGlobal(pos);
			
			var ws = workspace.workspace_.getInstance();				
			ws.addSetNameTextField(c_name_position, c_name_text, format,pos.x,pos.y,c_name_rotation,(value > 0 ? c_name_dx : -c_name_dx),c_name_dy,-move_,(-1/2),true, name_placement == 'chart');
		}
	}
	//function for showing value
	private function _show_value():Void
	{
		if (c_show_value)
		{
			var i:Number = 0;
			var x:Number;
			var y:Number = c_height/2 + this.c_y;
			var move_:Number = 0;
			switch (c_value_position)
			{
				case 'left':
				  x = c_x;
				  move_ = -1;
				break;
				case 'right':
				  x = c_x + c_width;
				  move_ = 0;
				break;
				case 'center':
				  x = c_x + c_width/2;
				  move_ = -0.5;
				break;
			}

			var format:TextFormat;
			if (c_value_rotation == 0)
				format = c_value_text_format;
			else {
				format = _global[c_value_text_format.font];
				format.size = c_value_text_format.size;
				format.color = c_value_text_format.color;
			}
			
			var pos:Object = {x:x,y:y};
			column_mc._parent.localToGlobal(pos);
			trace (x+','+move_+c_value_text);
			
			var ws = workspace.workspace_.getInstance();
			ws.addSetValueTextField(c_value_position, c_value_text, format,pos.x,pos.y,c_value_rotation,(value > 0 ? c_value_dx : -c_value_dx),c_value_dy,move_,(-1/2),true);
		}
	}
	//showing hint
	private function _show_hint()
	{
		if (c_hint_show)
		{
			var hnt:library.hint = new library.hint(column_mc);
			hnt.horizontal_position = this.c_hint_horizontal_position;
			hnt.vertical_position = this.c_hint_vertical_position;
			hnt.auto_size = this.c_hint_auto_size;
			hnt.width = this.c_hint_width;
			hnt.height = this.c_hint_height;				
			hnt.background_enabled = this.c_hint_background_enabled;
			hnt.background_color = this.c_hint_background_color;
			hnt.background_alpha = this.hint_background_alpha;
			hnt.border_enabled = this.c_hint_border_enabled;
			hnt.border_color = this.c_hint_border_color;
			hnt.border_size = this.hint_border_size;
			hnt.border_alpha = this.hint_border_alpha;
			hnt.text = this.c_hint_text;
			hnt.text_format = this.c_hint_text_format;
			hnt.show();
		}
	}
	//click event
	private function click_event()
	{
		if ((c_url!=undefined) or (c_sound!=undefined))
		{
			var ths = this;
			column_mc.onRelease = function()
			{
				if (ths.c_url!=undefined)
				{
					getURL(ths.c_url,ths.c_url_target);
				}
				if (ths.c_sound!=undefined)
				{
					ths.c_sound.start(ths.c_sound_start_second,ths.c_sound_loops);
				}
			}
		}
	}
	//
}