﻿import mx.events.EventDispatcher
class chart.elements._3dcolumn
{
	public var blockName:String;
	public var value:Number;
	
	//next item
	var nxt;
	
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;
	
	//position
	private var c_x:Number;
	private var c_y:Number;
	
	//size
	private var c_width:Number;
	private var c_height:Number;
	
	//background
	private var c_background_enabled:Boolean;
	private var c_background_color:Number;
	private var c_background_alpha:Number;
	
	//border
	private var c_border_enabled:Boolean;
	private var c_border_color:Number;
	private var c_border_size:Number;
	private var c_border_alpha:Number;
	
	//animation
	private var c_animation_enabled:Boolean;
	private var c_animation_speed:Number;
	private var c_animation_direction:String;
	private var c_animation_attribute:String;
	
	//name
	private var c_name_text:String;
	private var c_name_text_format:TextFormat;
	private var c_name_position:String;
	private var c_show_name:Boolean;
	private var c_name_rotation:Number;
	private var c_name_dx:Number;
	private var c_name_dy:Number;
	private var name_text_field:TextField;
	public var name_placement:String = 'smth';
	
	//value
	private var c_value_text:String;
	private var c_value_text_format:TextFormat;
	private var c_value_position:String;
	private var c_show_value:Boolean;
	private var value_text_field:TextField;
	private var c_value_rotation:Number;
	private var c_value_dx:Number;
	private var c_value_dy:Number;
	
	//hint
	private var c_hint_show:Boolean;
	private var c_hint_horizontal_position:String;
	private var c_hint_vertical_position:String;
	private var c_hint_width:Number;
	private var c_hint_height:Number;
	private var c_hint_auto_size:Boolean;
	private var c_hint_background_enabled:Boolean;
	private var c_hint_background_color:Number;
	private var c_hint_border_enabled:Boolean;
	private var c_hint_border_color:Number;
	private var c_hint_text:String;
	private var c_hint_text_format:TextFormat;
	
	public var hint_border_size:Number;
	public var hint_border_alpha:Number;
	public var hint_background_alpha:Number;
	
	//click events
	//url
	private var c_url:String;
	private var c_url_target:String;
	//sound
	private var c_sound:Sound;
	private var c_sound_loops:Number;
	private var c_sound_start_second:Number;
	
	//column movie clip
	private var column_mc:MovieClip;
	
	//rotation
	private var c_rotate_3d:Number;
	//deep
	private var c_deep:Number;
	//background settings
	private var c_1:Number;
	private var c_2:Number;
	
	//libraries
	private var lib;
	private var clr_lib;
	
	//3d
	private var c_dx:Number;
	private var c_dy:Number;
	
	//--------------------------------------------------------------
	//getters and setters
	//set x postion
	function set x(new_x:Number):Void
	{
		c_x = new_x;
		column_mc._x = c_x;
	}
	//set y position
	function set y(new_y:Number):Void
	{
		c_y = new_y;
		column_mc._y = c_y;
	}
	function get y():Number {
		return c_y;
	}
	//set column width
	function set width(w:Number):Void
	{
		c_width = w;
	}
	//set column height
	function set height(h:Number):Void
	{
		c_height = h;
	}
	function get height():Number {
		return c_height;
	}
	//set background enabled
	function set background_enabled(e:Boolean):Void
	{
		c_background_enabled = e;
	}	
	//set background color
	function set background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("Column background color should be positive value below 0xFFFFFF");
		}else
		{
			c_background_color = clr;
		}
	}
	//set background alpha
	function set background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("Column background alpha should be positive value below 100");
		}else
		{
			c_background_alpha = a;
		}
	}
	//set border enabled
	function set border_enabled(e:Boolean):Void
	{
		c_border_enabled = e;
	}
	//set border size
	function set border_size(s:Number):Void
	{
		if ((s<0) or (s>255))
		{
			_root.showError("Column border size should be between 0 and 255");
		}else
		{
			c_border_size = s;
		}
	}
	//set border color
	function set border_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("Column border color should be between 0 and 0xFFFFFF");
		}else
		{
			c_border_color = clr;
		}
	}
	//set border alpha
	function set border_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("Column border should be between 0 and 100");
		}else
		{
			c_border_alpha = a;
		}
	}
	//set animation enabled
	function set animation_enabled(e:Boolean):Void
	{
		c_animation_enabled = e;
	}
	//set animation speed
	function set animation_speed(s:Number):Void
	{
		if ((s<0) or (s>500))
		{
			_root.showError("Column animation speed should be between 0 and 500");
		}else
		{
			c_animation_speed = s;
		}
	}
	//set animation direction
	function set animation_direction(d:String):Void
	{
		if ((d!='up') and (d!='down'))
		{
			_root.showError("Column animation direction value is 'up' or 'down'");
		}else
		{
			c_animation_direction = d;
		}
	}
	function set animation_attribute(a:String):Void
	{
		c_animation_attribute = a;
	}
	//set name text
	function set name_text(txt:String):Void
	{
		c_name_text = txt;
	}
	//set name text format
	function set name_text_format(tf:TextFormat):Void
	{
		c_name_text_format = tf;
	}
	//set name position
	function set name_position(pos:String):Void
	{
		c_name_position = pos;
	}
	//set show_name
	function set show_name(s:Boolean):Void
	{
		c_show_name = s;
	}
	function set name_rotation(r:Number):Void
	{
		c_name_rotation = r;
	}
	function set name_dx(d:Number):Void
	{
		c_name_dx = d;
	}
	function set name_dy(d:Number):Void
	{
		c_name_dy = d;
	}
	//set value text
	function set value_text(txt:String):Void
	{
		c_value_text = txt;
	}
	function set value_rotation(r:Number):Void
	{
		c_value_rotation = r;
	}
	function set value_dx(d:Number):Void
	{
		c_value_dx = d;
	}
	function set value_dy(d:Number):Void
	{
		c_value_dy = d;
	}
	//set value position
	function set value_position(pos:String):Void
	{
		c_value_position = pos;
	}
	//set value text format
	function set value_text_format(tf:TextFormat):Void
	{
		c_value_text_format = tf;
	}
	//set show_value
	function set show_value(s:Boolean):Void
	{
		c_show_value = s;
	}
	//set hint enabled
	function set show_hint(s:Boolean):Void
	{
		c_hint_show = s;
	}
	//set hint position
	function set hint_horizontal_position(pos:String):Void
	{
		c_hint_horizontal_position = pos;
	}
	function set hint_vertical_position(pos:String):Void
	{
		c_hint_vertical_position = pos;
	}
	//set hint size
	function set hint_width(w:Number):Void
	{
		c_hint_width = w;
	}
	function set hint_height(h:Number):Void
	{
		c_hint_height = h;
	}
	function set hint_auto_size(as:Boolean):Void
	{
		c_hint_auto_size = as;
	}
	//set hint border
	function set hint_border_enabled(e:Boolean):Void
	{
		c_hint_border_enabled = e;
	}
	function set hint_border_color(clr:Number):Void
	{
		c_hint_border_color = clr;
	}
	//set hint background
	function set hint_background_enabled(e:Boolean):Void
	{
		c_hint_background_enabled = e;
	}
	function set hint_background_color(clr:Number):Void
	{
		c_hint_background_color = clr;
	}
	//set hint text
	function set hint_text(txt:String):Void
	{
		c_hint_text = txt;
	}
	function set hint_text_format(tf:TextFormat):Void
	{
		c_hint_text_format = tf;
	}
	//set url onClick
	function set url(u:String):Void
	{
		c_url = u;
	}
	function set url_target(t:String):Void
	{
		c_url_target = t;
	}
	//set sound onClick
	function set sound(s:String):Void
	{
		if (s!=undefined)
		{
			c_sound = new Sound();
			c_sound.loadSound(s,false);		
		}
	}
	//set sound loops
	function set sound_loops(l:Number):Void
	{
		c_sound_loops = l;
	}
	//set sound offset
	function set sound_start_second(s:Number):Void
	{
		c_sound_start_second = s;
	}
	//set 3d_roation
	function set rotate_3d(a:Number):Void
	{
		if ((a<0) or (a>90))
		{
			_root.showError("3d column 3d rotation should be between 0 and 90");
		}else
		{
			c_rotate_3d = a;
		}
	}
	//set deep
	function set deep(d:Number):Void
	{
		if (d<0)
		{
			_root.showError("Vadli 3d column deep should be positive");
		}else
		{
			c_deep = d;
		}
	}
	function set background_c1(c:Number):Void
	{
		c_1 = c;
	}
	function set background_c2(c:Number):Void
	{
		c_2 = c;
	}
    //--------------------------------------------------------------
	//class constructor
	function _3dcolumn(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['column_clip_'+i]!=undefined)
		{
			i++;
		}
		column_mc = target_mc.createEmptyMovieClip('column_clip_'+i,target_mc.getNextHighestDepth());
		lib = new library.functions();
		clr_lib = new library.color_ext();
	}
	//paint
	function paint():Void
	{
		column_mc._x = c_x;
		column_mc._y = c_y;
		
		c_dx = c_deep*Math.cos(lib.Grad2Rad(c_rotate_3d));
		c_dy = c_deep*Math.sin(lib.Grad2Rad(c_rotate_3d));
		
		if (c_animation_enabled)
		{
			switch (c_animation_attribute)
			{
				case 'size':
				  paint_with_animation();
				break;
				case 'alpha':
				  paint_with_alpha_animation();
				break;
			}
		}else
		{
			paint_without_animation();
		}
		//show hint
		_show_hint();
		click_event();
	}
	function repaint():Void
	{
		column_mc.clear();
		paint_without_animation();
	}
	//paint column without animation
	private function paint_without_animation():Void
	{
		var dx:Number = c_dx;
		var dy:Number = c_dy;
		//paint main part
		column_mc.moveTo(0,0);
		if (c_background_enabled)
		{
			column_mc.beginFill(c_background_color,c_background_alpha);
		}
		if (c_border_enabled)
		{
			column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
		}
		//
		column_mc.lineTo(c_width,0);
		column_mc.lineTo(c_width,c_height);
		column_mc.lineTo(0,c_height);
		column_mc.lineTo(0,0);
		//---
		if (c_background_enabled)
		{
			column_mc.endFill();
		}
		column_mc.moveTo(0,0);
		if (c_background_enabled)
		{
			var clr:Number = clr_lib.transfer(c_background_color,c_1);
			column_mc.beginFill(clr,c_background_alpha);
		}
		//paint up part
		column_mc.lineTo(dx,-dy);
		column_mc.lineTo(c_width+dx,-dy);
		column_mc.lineTo(c_width,0);
		column_mc.lineTo(0,0);
		if (c_background_enabled)
		{
			column_mc.endFill();
		}
		column_mc.moveTo(c_width,0);
		if (c_background_enabled)
		{
			clr = clr_lib.transfer(c_background_color,c_2);
			column_mc.beginFill(clr,c_background_alpha);
		}
		//paint right part
		column_mc.lineTo(c_width+dx,-dy);
		column_mc.lineTo(c_width+dx,c_height-dy);
		column_mc.lineTo(c_width,c_height);
		column_mc.lineTo(c_width,0);
		//--
		if (c_background_enabled)
		{
			column_mc.endFill();
		}
		_show_name();
		_show_value();
		dispatchEvent ( {type: 'onPaintFinish'});
	}
	//paint column with animation
	private function paint_with_animation():Void
	{
		var dx:Number = c_dx;
		var dy:Number = c_dy;
		if ((c_border_enabled) and (c_animation_direction=='up'))
		{
			column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
			column_mc.moveTo(0,c_height);
			column_mc.lineTo(c_width,c_height);
			column_mc.moveTo(c_width,c_height);
			column_mc.lineTo(c_width+dx,c_height-dy);
			column_mc.moveTo(0,0);
		}		
		var ths = this;
		var border_mc:MovieClip;
		//paint moveable line
		border_mc = column_mc.createEmptyMovieClip('border_movie',column_mc.getNextHighestDepth());												   
		if (c_border_enabled)
		{
			border_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
		}
		if (c_animation_direction=='up')
		{
			border_mc.moveTo(0,0);
			if (c_background_enabled)
			{
				border_mc.beginFill(clr_lib.transfer(c_background_color,c_1),c_background_alpha);
			}
			border_mc.lineTo(c_width,0);
			border_mc.lineTo(c_width+dx,-dy);
			border_mc.lineTo(dx,-dy);
			border_mc.lineTo(0,0);
			if (c_background_enabled)
			{
				border_mc.endFill();
			}			
		}else
		{
			border_mc.moveTo(0,0);
			border_mc.lineTo(c_width,0);
			border_mc.moveTo(c_width,0);
			border_mc.lineTo(c_width+dx,-dy);
			if (c_border_enabled)
			{
				column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
			}
			column_mc.moveTo(0,0);
			if (c_background_enabled)
			{
				column_mc.beginFill(clr_lib.transfer(c_background_color,c_1),c_background_alpha);				
			}
			column_mc.lineTo(c_width,0);
			column_mc.lineTo(c_width+dx,-dy);
			column_mc.lineTo(dx,-dy);
			column_mc.lineTo(0,0);
			if (c_background_enabled)
			{
				column_mc.endFill();
			}
		}
		//paint column
		var current_position:Number;
		if (c_animation_direction=='up')
		{
			current_position = c_height;
			c_animation_speed = - Math.abs(c_animation_speed);
		}else
		{
			current_position = 0;
		}		
		var max_val:Number = c_height - current_position;
		var last_position:Number = current_position;
		//show name
		border_mc._y =  current_position;
		var show_name_on_end:Boolean = true;
		if (((c_name_position=='top') and (c_animation_direction=='down')) or ((c_name_position=='bottom') and (c_animation_direction=='up')))
		{
			_show_name();
			show_name_on_end = false;
		}		
		//show value
		var show_value_on_end:Boolean = true;
		if (((c_value_position=='top') and (c_animation_direction=='down')) or ((c_value_position=='bottom') and (c_animation_direction=='up')))
		{
			_show_value();
			show_value_on_end = false;
		}
		//
		column_mc.onEnterFrame = function()
		{
			last_position = current_position;
			current_position += ths.c_animation_speed;
			if (((current_position<max_val) and (ths.c_animation_direction=='up')) or ((current_position>max_val) and (ths.c_animation_direction=='down')))			
			{
				current_position = max_val;
			}
			border_mc._y =  current_position;
			this.moveTo(0,current_position);
			if (ths.c_background_enabled)
			{
				this.beginFill(ths.c_background_color,ths.c_background_alpha);
			}
			if (ths.c_border_enabled)
			{
				this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
			}
			this.lineTo(0,last_position);
			this.lineStyle();
			this.lineTo(ths.c_width,last_position);
			if (ths.c_border_enabled)
			{
				this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
			}
			this.lineTo(ths.c_width,current_position);
			this.lineStyle();
			this.lineTo(0,current_position);
			if (ths.c_background_enabled)
			{
				this.endFill();
			}			
			this.moveTo(ths.c_width,last_position);
			if (ths.c_background_enabled)
			{
				this.beginFill(ths.clr_lib.transfer(ths.c_background_color,ths.c_2),ths.c_background_alpha);				
			}
			//paint right part
			if (ths.c_border_enabled)
			{
				this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
			}
			this.lineTo(ths.c_width,current_position);
			this.lineStyle();
			this.lineTo(ths.c_width+dx,current_position-dy);
			if (ths.c_border_enabled)
			{
				this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
			}
			this.lineTo(ths.c_width+dx,last_position-dy);
			this.lineStyle();
			this.lineTo(ths.c_width,last_position);
			if (ths.c_background_enabled)
			{
				this.endFill();
			}
			//--
			if (current_position == max_val)
			{
				if (ths.c_border_enabled)
				{
					this.lineStyle(ths.c_border_size,ths.c_border_color,ths.c_border_alpha);
					//
					if (ths.c_animation_direction=='up')
					{
						this.moveTo(0,ths.c_height);
						this.lineTo(ths.c_width,ths.c_height);
						this.moveTo(ths.c_width,ths.c_height);
						this.lineTo(ths.c_width+dx,ths.c_height-dy);
					}else
					{
						this.moveTo(0,0);
						this.lineTo(ths.c_width,0);
						this.moveTo(ths.c_width,ths.c_height);
						this.lineTo(ths.c_width+dx,ths.c_height-dy);
					}
				}
				if (show_name_on_end)
				{
					ths._show_name();
				}
				if (show_value_on_end)
				{
					ths._show_value();
				}
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = function(){};
			}
		}
		//-
	}
	//paint with alpha animation
	private function paint_with_alpha_animation():Void
	{
		var ths = this;
		var dx:Number = c_dx;
		var dy:Number = c_dy;
		//paint main part
		column_mc.moveTo(0,0);
		if (c_background_enabled)
		{
			column_mc.beginFill(c_background_color,c_background_alpha);
		}
		if (c_border_enabled)
		{
			column_mc.lineStyle(c_border_size,c_border_color,c_border_alpha);
		}
		//
		column_mc.lineTo(c_width,0);
		column_mc.lineTo(c_width,c_height);
		column_mc.lineTo(0,c_height);
		column_mc.lineTo(0,0);
		//---
		if (c_background_enabled)
		{
			column_mc.endFill();
		}
		column_mc.moveTo(0,0);
		if (c_background_enabled)
		{
			var clr:Number = clr_lib.transfer(c_background_color,c_1);
			column_mc.beginFill(clr,c_background_alpha);
		}
		//paint up part
		column_mc.lineTo(dx,-dy);
		column_mc.lineTo(c_width+dx,-dy);
		column_mc.lineTo(c_width,0);
		column_mc.lineTo(0,0);
		if (c_background_enabled)
		{
			column_mc.endFill();
		}
		column_mc.moveTo(c_width,0);
		if (c_background_enabled)
		{
			clr = clr_lib.transfer(c_background_color,c_2);
			column_mc.beginFill(clr,c_background_alpha);
		}
		//paint right part
		column_mc.lineTo(c_width+dx,-dy);
		column_mc.lineTo(c_width+dx,c_height-dy);
		column_mc.lineTo(c_width,c_height);
		column_mc.lineTo(c_width,0);
		//--
		if (c_background_enabled)
		{
			column_mc.endFill();
		}
		//-------------------------------------
		var current_alpha:Number = 0;
		column_mc._alpha = 0;
		column_mc.onEnterFrame = function()
		{
			current_alpha += ths.c_animation_speed;
			if (current_alpha>100)
			{
				current_alpha = 100;
			}
			this._alpha = current_alpha;
			if (current_alpha==100)
			{
				ths._show_name();
				ths._show_value();
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				this.onEnterFrame = null;
				delete this.onEnterFrame;	
			}
		}
	}
	//function for showing name
	private function _show_name():Void
	{
		if ((c_show_name) and (c_name_text!=undefined) and (length(c_name_text)>0))
		{
			var i:Number = 0;
			var x:Number = c_x + c_width/2;
			var y:Number = c_y;
			var move_:Number = 0;
			switch (c_name_position)
			{
				case 'top':
				  y = c_y - c_dy;
				  move_ = 1;
				break;
				case 'bottom':
				  y = c_y + c_height;
				break;
				case 'center':
				  y = c_y + c_height/2;
				  move_ = 0.5;
				break;
			}
			
			var format:TextFormat;
			if (c_name_rotation == 0)
				format = c_name_text_format;
			else {
				format = _global[c_name_text_format.font];
				format.size = c_name_text_format.size;
				format.color = c_name_text_format.color;
			}
			
			var pos:Object = {x:x,y:y};
			column_mc._parent.localToGlobal(pos);
			
			var ws = workspace.workspace_.getInstance();				
			ws.addSetNameTextField(c_name_position, c_name_text, format,pos.x,pos.y,c_name_rotation,c_name_dx,(value > 0 ? c_name_dy : -c_name_dy),(-1/2),-move_,undefined,name_placement=='chart');
		}
	}
	//function for showing value
	private function _show_value():Void
	{
		if (c_show_value)
		{
			var i:Number = 0;
			var x:Number = c_x + c_width/2;
			var y:Number = c_y;
			var move_:Number = 0;
			switch (c_value_position)
			{
				case 'top':
				  y = c_y - c_dy;
				  move_ = 1;
				break;
				case 'bottom':
				  y = c_y + c_height;
				break;
				case 'center':
				  y = c_y + c_height/2;
				  move_ = 0.5;
				break;
			}

			var format:TextFormat;
			if (c_value_rotation == 0)
				format = c_value_text_format;
			else {
				format = _global[c_value_text_format.font];
				format.size = c_value_text_format.size;
				format.color = c_value_text_format.color;
			}
			
			var pos:Object = {x:x,y:y};
			column_mc._parent.localToGlobal(pos);
			
			var ws = workspace.workspace_.getInstance();				
			ws.addSetValueTextField(c_value_position, c_value_text, format,pos.x,pos.y,c_value_rotation,c_value_dx,(value > 0 ? c_value_dy : -c_value_dy),(-1/2),-move_);
		}
	}
	//showing hint
	private function _show_hint()
	{
		if (c_hint_show)
		{
			var hnt:library.hint = new library.hint(column_mc);
			var ths = this;
			hnt.horizontal_position = this.c_hint_horizontal_position;
			hnt.vertical_position = this.c_hint_vertical_position;
			hnt.auto_size = this.c_hint_auto_size;
			hnt.width = this.c_hint_width;
			hnt.height = this.c_hint_height;				
			hnt.background_enabled = this.c_hint_background_enabled;
			hnt.background_color = this.c_hint_background_color;
			hnt.background_alpha = this.hint_background_alpha;
			hnt.border_enabled = this.c_hint_border_enabled;
			hnt.border_color = this.c_hint_border_color;
			hnt.border_size = this.hint_border_size;
			hnt.border_alpha = this.hint_border_alpha;
			hnt.text = this.c_hint_text;
			hnt.text_format = this.c_hint_text_format;
			hnt.show();
		}
	}
	//click event
	private function click_event()
	{
		if ((c_url!=undefined) or (c_sound!=undefined))
		{
			var ths = this;
			column_mc.onRelease = function()
			{
				if (ths.c_url!=undefined)
				{
					getURL(ths.c_url,ths.c_url_target);
				}
				if (ths.c_sound!=undefined)
				{
					ths.c_sound.start(ths.c_sound_start_second,ths.c_sound_loops);
				}
			}
		}
	}
	//
}