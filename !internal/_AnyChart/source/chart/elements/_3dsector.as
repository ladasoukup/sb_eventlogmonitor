﻿import chart.PieLabelsDistributor;
import chart.PieLabel;
import mx.events.EventDispatcher
import flash.geom.Matrix;

class chart.elements._3dsector
{
	public var label:PieLabel;
	
	public var highlight:Boolean = true;
	
	//link for painting next sector
	var nxt;
	//Event dispatcher variables
	public var addEventListener, removeEventListener, dispatchEvent:Function;
	
	//position
	private var s_x:Number;
	private var s_y:Number;
	
	//radius
	private var s_radius:Number;
	
	//alphas
	private var s_start_alpha:Number;
	private var s_end_alpha:Number;
	
	//border
	private var s_border_enabled:Boolean;
	private var s_border_size:Number;
	private var s_border_color:Number;
	private var s_border_alpha:Number;
	
	//background
	private var s_background_enabled:Boolean;
	private var s_background_color:Number;
	private var s_background_alpha:Number;
	
	//caption text
	private var s_caption_text:String;
	private var s_caption_position:Number;
	private var s_caption_text_format:TextFormat;
	private var s_caption_rotation:Number;
	private var s_caption_dx:Number;
	private var s_caption_dy:Number;
	
	//3d
	private var s_background_c:Number;
	private var s_height:Number;
	private var s_inner_alpha:Number;
	
	//animation
	private var s_animation_enabled:Boolean;
	private var s_animation_attribute:String;
	private var s_animation_speed:Number;
	
	//url
	private var s_url:String;
	private var s_url_target:String;
	
	//sound
	private var s_sound:String;
	private var s_sound_loops:Number;
	private var s_sound_offset:Number;
	
	//unsetable
	private var s_radius_x:Number;
	private var s_radius_y:Number;
	
	//movie
	private var sector_mc:MovieClip;
	private var main_mc:MovieClip;
	private var border_mc:MovieClip;
	
	
	public var onPaintFinish:Function;
	
	var _hint;
	//--------------------------------------------------
	function set x(new_x:Number):Void
	{
		s_x = new_x;
	}
	function set y(new_y:Number):Void
	{
		s_y = new_y;
	}
	function set radius(r:Number):Void
	{
		s_radius = r;
	}
	function set start_alpha(a:Number):Void
	{
		s_start_alpha = a;
	}
	function set end_alpha(a:Number):Void
	{
		s_end_alpha = a;
	}
	function set background_enabled(e:Boolean):Void
	{
		s_background_enabled = e;
	}
	function set background_color(clr:Number):Void
	{
		s_background_color = clr;
	}
	function set background_alpha(a:Number):Void
	{
		s_background_alpha = a;
	}
	function set border_enabled(e:Boolean):Void
	{
		s_border_enabled = e;
	}
	function set border_size(s:Number):Void
	{
		s_border_size = s;
	}
	function set border_color(clr:Number):Void
	{
		s_border_color = clr;
	}
	function set border_alpha(a:Number):Void
	{
		s_border_alpha = a;
	}
	function set animation_enabled(e:Boolean):Void
	{
		s_animation_enabled = e;
	}
	function set animation_speed(s:Number):Void
	{
		s_animation_speed = s;
	}
	function set animation_attribute(a:String):Void
	{
		s_animation_attribute = a;
	}
	function set background_c(c:Number):Void
	{
		s_background_c = c;
	}
	function set inner_alpha(a:Number):Void
	{
		s_inner_alpha = a;
	}
	function set height(h:Number):Void
	{
		s_height = h;
	}
	function set caption_text(txt:String):Void
	{
		s_caption_text = txt;
	}
	function set caption_rotation(r:Number):Void
	{
		s_caption_rotation = r;
	}
	function set caption_dx(d:Number):Void
	{
		s_caption_dx = d;
	}
	function set caption_dy(d:Number):Void
	{
		s_caption_dy = d;
	}
	function set caption_text_format(tf:TextFormat):Void
	{
		s_caption_text_format = tf;
	}
	function set caption_position(pos:Number):Void
	{
		s_caption_position = pos;
	}
	function set url(u:String):Void
	{
		s_url = u;
	}
	function set url_target(t:String):Void
	{
		s_url_target = t;
	}
	function set sound(s:String):Void
	{
		s_sound = s;
	}
	function set sound_loops(l:Number):Void
	{
		s_sound_loops = l;
	}
	function set sound_offset(o:Number):Void
	{
		s_sound_offset = o;
	}
	//--------------------------------------------------
	//constructor
	function _3dsector(target_mc:MovieClip)
	{
		EventDispatcher.initialize(this);
		var i:Number = 0;
		while (target_mc['elements_'+i]!=undefined)
		{
			i++;
		}
		sector_mc = target_mc.createEmptyMovieClip('elements_'+i,target_mc.getNextHighestDepth());
		_hint = new library.hint(sector_mc);
	}
	//--------------------------------------------------
	function paint():Void
	{
		sector_mc._x = s_x;
		sector_mc._y = s_y;
		if (!s_animation_enabled)
		{
			paint_without_animation();
			show_caption();
			dispatchEvent ( {type: 'onPaintFinish'});
			onPaintFinish();
		}else
		{
			paint_without_animation();
			alpha_animation();
		}
		_hint.show();
		click_event();
	}
	
	private function setFill():Void  {
		if (!s_background_enabled)
			return;
			
		if (!highlight) {
			main_mc.beginFill(s_background_color,s_background_alpha);
			return;
		}
		
		var clr_lib = new library.color_ext();
		var highlightColor:Number = clr_lib.transfer(s_background_color,2);
		var shadowColor:Number = clr_lib.transfer(s_background_color,0.8);
		var colors:Array = [highlightColor,shadowColor];
		var alphas:Array = [100, 100];
		var ratios:Array = [0, 255];
		
	    var fillType:String = "linear"
    	var matrix:Matrix = new Matrix();	    
		
		var w:Number = s_radius_x*2;
		var h:Number = (s_radius_y + s_height)*2;
		var x:Number = -s_radius_x;
		var y:Number = -s_radius_y+s_height;
		
		matrix.createGradientBox(w, h, -Math.PI/3.3, x, y);
		
		main_mc.beginGradientFill(fillType,colors,alphas,ratios,matrix);
	}
	
	public function getRadians(angle:Number):Number {
		return angle * Math.PI/180;
	}
		
	public function getAngleFourth(angle:Number):Number {
		var sin:Number = Math.sin(-angle * Math.PI/180);
		var cos:Number = Math.cos(angle * Math.PI/180);
		if (sin >= 0 && cos > 0)
			return 4;
		else if (sin < 0 && cos >= 0)
			return 1;
		else if (sin < 0 && cos < 0)
			return 2;
		else
			return 3;
	}
	
	//---------------------------------------------------
	private function paint_without_animation():Void
	{
		var lib = new library.functions();
		s_radius_x = s_radius;
		s_radius_y = s_radius*Math.sin(lib.Grad2Rad(s_inner_alpha));
		//define clips
		main_mc = sector_mc.createEmptyMovieClip('main_movie',sector_mc.getNextHighestDepth());
		border_mc = sector_mc.createEmptyMovieClip('border_movie',sector_mc.getNextHighestDepth());
		//paint main clip
		var i:Number;
		var x_pos:Number = 0;
		var y_pos:Number = 0;
		main_mc.moveTo(x_pos,y_pos);
		if (s_border_enabled)
		{
			main_mc.lineStyle(s_border_size,s_border_color,s_border_alpha);
		}
		setFill();
		for (i=s_start_alpha;i<=s_end_alpha;i++)
		{
			x_pos = s_radius_x*Math.cos(lib.Grad2Rad(i));
			y_pos = s_radius_y*Math.sin(lib.Grad2Rad(i));
			main_mc.lineTo(x_pos,y_pos);
		}
		x_pos = s_radius_x*Math.cos(lib.Grad2Rad(s_end_alpha));
		y_pos = s_radius_y*Math.sin(lib.Grad2Rad(s_end_alpha));
		main_mc.lineTo(x_pos,y_pos);
		main_mc.lineTo(0,0);
		if (s_background_enabled)
		{
			main_mc.endFill();
		}
		//paint border part
		var start_border_alpha:Number;
		var end_border_alpha:Number;
		s_start_alpha = Number(s_start_alpha);
		s_end_alpha = Number(s_end_alpha);
		
		var startA:Number = s_start_alpha;
		var endA:Number = s_end_alpha;
		
		while (startA < 0)
			startA += 360;
		while (endA < 0)
			endA += 360;
			
		while (startA > 360)
			startA -= 360;
		
		while (endA > 360)
			endA -= 360;
		
		var startF:Number = getAngleFourth(startA);
		var endF:Number = getAngleFourth(startA);
		
		var hasSide:Boolean = false;
		hasSide = (startF == 1 || startF == 2);
		hasSide = hasSide || (startF == 3 && (endF == 1 || endF == 2));
		hasSide = hasSide || (startF == 4 && (endF == 1 || endF == 2 || endF == 3));
		hasSide = hasSide || (startA > endA);
		if (hasSide) {
			if (startA > 180)
				startA = 0;
			if (endA > 180)
				endA = 180;
		}
		start_border_alpha = startA;
		end_border_alpha = endA;
		if (hasSide)
		{
			var clr_lib = new library.color_ext();
			var bg_2:Number = clr_lib.transfer(s_background_color,s_background_c);
			if (s_border_enabled)
			{
				border_mc.lineStyle(s_border_size,s_border_color,s_border_alpha);
			}
			x_pos = s_radius_x*Math.cos(lib.Grad2Rad(start_border_alpha));
			y_pos = s_radius_y*Math.sin(lib.Grad2Rad(start_border_alpha));
			border_mc.moveTo(x_pos,y_pos);
			if (s_background_enabled)
			{
				if (!highlight) {
					border_mc.beginFill(bg_2,s_background_alpha);
					return;
				}
				
				var clr_lib = new library.color_ext();
				var highlightColor:Number = clr_lib.transfer(s_background_color,1.4);
				var shadowColor:Number = clr_lib.transfer(s_background_color,0.6);
				var colors:Array = [highlightColor,shadowColor];
				var alphas:Array = [100, 100];
				var ratios:Array = [0, 255];
				
				var fillType:String = "linear"
				var matrix:Matrix = new Matrix();	    
				
				var w:Number = s_radius_x*2;
				var h:Number = (s_radius_y + s_height)*2;
				var x:Number = -s_radius_x;
				var y:Number = -s_radius_y+s_height;
				
				matrix.createGradientBox(w, h, -Math.PI/3.3, x, y);
				
				border_mc.beginGradientFill(fillType,colors,alphas,ratios,matrix);
				
			}
			for (i=start_border_alpha;i<=end_border_alpha;i++)
			{
				x_pos = s_radius_x*Math.cos(lib.Grad2Rad(i));
				y_pos = s_radius_y*Math.sin(lib.Grad2Rad(i));
				border_mc.lineTo(x_pos,y_pos);
			}
			x_pos = s_radius_x*Math.cos(lib.Grad2Rad(end_border_alpha));
			y_pos = s_radius_y*Math.sin(lib.Grad2Rad(end_border_alpha));
			border_mc.lineTo(x_pos,y_pos);
			border_mc.lineTo(x_pos,y_pos + s_height);
			for (i=end_border_alpha;i>=start_border_alpha;i--)
			{
				x_pos = s_radius_x*Math.cos(lib.Grad2Rad(i));
				y_pos = s_radius_y*Math.sin(lib.Grad2Rad(i)) + s_height;
				border_mc.lineTo(x_pos,y_pos);
			}
			x_pos = s_radius_x*Math.cos(lib.Grad2Rad(start_border_alpha));
			y_pos = s_radius_y*Math.sin(lib.Grad2Rad(start_border_alpha)) + s_height;
			border_mc.lineTo(x_pos,y_pos);
			border_mc.lineTo(x_pos,y_pos - s_height);
			if (s_background_enabled)
			{
				border_mc.endFill();
			}
		}
	}
	//alpha animation
	private function alpha_animation():Void
	{
		var current_pos:Number = 0;
		var ths = this;
		sector_mc._alpha = current_pos;
		sector_mc.onEnterFrame = function()
		{
			current_pos += ths.s_animation_speed;
			if (current_pos>=100)
			{
				current_pos = 100;
			}
			this._alpha = current_pos;
			if (current_pos==100)
			{
				ths.show_caption();
				ths.dispatchEvent ( {type: 'onPaintFinish'});
				ths.onPaintFinish();
				this.onEnterFrame = function(){};
			}
		}
	}
	//show caption
	private function show_caption():Void
	{
		if ((s_caption_text!=undefined) and (s_caption_position!=undefined))
		{
			label = new PieLabel(sector_mc);
			label.field.text = s_caption_text;
			if (s_caption_rotation != 0) {
				label.field.embedFonts = true;				
				var tf:TextFormat = _global[s_caption_text_format.font];
				tf.size = s_caption_text_format.size;
				tf.color = s_caption_text_format.color;
				label.field.setTextFormat(tf);
				label.field._rotation = s_caption_rotation;
			}else {
				label.field.setTextFormat(s_caption_text_format);
			}
			
			var dx:Number = 0;
			var dy:Number = -0.5;			
			var centerAngle:Number = (s_start_alpha + s_end_alpha)/2;
			
			label.angle = centerAngle;
			label.radius = s_caption_position*s_radius;
			label.allocatedY = label.radius*Math.sin(centerAngle*Math.PI/180);
			label.sliceConnectorX = s_radius*Math.cos(centerAngle*Math.PI/180);
			label.sliceConnectorY = s_radius*Math.sin(centerAngle*Math.PI/180);
			PieLabelsDistributor.addLabel(label);
		}
	}
	
	private function click_event():Void
	{
		if (((s_sound!=undefined) and (s_sound!='')) or ((s_url!=undefined) and (s_url!='')))
		{
			var ths = this;
			if (s_sound!=undefined)
			{
				var snd:Sound = new Sound();
				snd.loadSound(s_sound,false);
			}
			sector_mc.onRelease = function()
			{
				if (ths.s_url!=undefined)
				{
					getURL(ths.s_url,ths.s_url_target);
				}
				if (ths.s_sound!=undefined)
				{
					snd.onLoad = function()
					{
						snd.start(ths.s_sound_offset,ths.s_sound_loops);
					}
				}
			}			
		}
	}
}