﻿import chart.PieLabelsDistributor;

class chart._2dpie_chart
{
	public var hints_text:String = '<b>{NAME}</b>, {VALUE}';
	public var hint_texts:Array;
	
	//labels settings
	public var labels_background_enabled:Boolean;
	public var labels_background_color:Number;
	public var labels_background_alpha:Number;
	public var labels_border_enabled:Boolean;
	public var labels_border_color:Number;
	public var labels_border_size:Number;
	public var labels_border_alpha:Number;
	public var labels_position_type:String;
	public var labels_x:Number;
	public var labels_y:Number;
	public var labels_width:Number;
	public var labels_height:Number;
	public var labels_radius:Number;
	public var lables_tf:TextFormat;
	public var show_label:Boolean;
	public var show_labels:Array;	
	public var labels_texts:Array;
	
	public var labels_links_enabled:Boolean;
	public var labels_links_size:Number;
	public var labels_links_color:Number;
	public var labels_links_alpha:Number;
	
	public var labels_links_dot_radius:Number;
	public var labels_links_dot_enabled:Boolean;
	public var labels_links_dot_background_enabled:Boolean;
	public var labels_links_dot_background_color:Number;
	public var labels_links_dot_background_alpha:Number;
	public var labels_links_dot_border_enabled:Boolean;
	public var labels_links_dot_border_size:Number;
	public var labels_links_dot_border_color:Number;
	public var labels_links_dot_border_alpha:Number;
	
	public var linksEnabled:Boolean = false;
	public var linksEdgeSize:Number = 20;
	public var linksSize:Number = 1;
	public var linksColor:Number = 0;
	public var linksAlpha:Number = 100;
	public var linksSpace:Number = 8;
	public var checkOverlap:Boolean = false;
	public var captionsPadding:Number = 30;
	
	//position
	private var p_x:Number;
	private var p_y:Number;
	
	//radius
	private var p_radius:Number;
	private var p_select_radius:Number;
	
	//values
	private var p_values:Array;
	
	//border
	private var p_border_enabled:Boolean;
	private var p_border_size:Number;
	private var p_border_color:Number;
	private var p_border_alpha:Number;
	
	//names settings
	private var p_show_names:Array;
	private var p_show_names_all:Boolean;	
	private var p_names_text:Array;
	
	//values settings
	private var p_show_values:Array;
	private var p_show_values_all:Boolean;
	private var p_values_prefix:String;
	private var p_values_postfix:String;
	private var p_values_decimal_places:Number;
	private var p_values_decimal_separator:String;
	private var p_values_thousand_separator:String;
	
	private var p_captions_text_format:TextFormat;
	private var p_captions_radius:Number;
	private var p_captions_rotations:Array;
	private var p_captions_dxs:Array;
	private var p_captions_dys:Array;
	private var p_captions_rotation:Number;
	private var p_captions_dx:Number;
	private var p_captions_dy:Number;
	
	//alphas
	private var p_alphas:Array;	
	
	//hitns
	private var p_hints_enabled:Boolean;
	private var p_hints_width:Number;
	private var p_hints_height:Number;
	private var p_hints_auto_size:Boolean;
	private var p_hints_background_enabled:Boolean;
	private var p_hints_background_color:Number;
	private var p_hints_border_enabled:Boolean;
	private var p_hints_border_color:Number;
	private var p_hints_text_format:TextFormat;
	private var p_hints_horizontal_position:String;
	private var p_hints_vertical_position:String;
	
	public var hints_border_size:Number = 1;
	public var hints_border_alpha:Number = 100;
	public var hints_background_alpha:Number = 100;
	
	//animation
	private var p_animation_enabled:Boolean;
	private var p_animation_speed:Number;
	private var p_animation_type:String;
	
	//backround
	private var p_background_enabled:Boolean;
	private var p_background_colors:Array;
	private var p_background_alphas:Array;
	private var p_background_alphas_all:Number;
	private var p_background_colors_all:Number;
	private var p_background_auto_color:Boolean;
	private var p_background_tone:Number;
	
	//all gradient background
	public var background_type:String = 'solid';
	public var background_gradient_colors:Array;
	public var background_gradient_alphas:Array;
	public var background_gradient_ratios:Array;
	public var background_gradient_rotation:Number = 0;
	public var background_gradient_type:String = 'radial';
	
	//borders
	public var border_enableds:Array;
	public var border_colors:Array;
	public var border_alphas:Array;
	public var border_sizes:Array;
	
	//urls
	private var p_urls:Array;
	private var p_urls_target:Array;
	private var p_urls_target_all:String;
	
	//sounds
	private var p_sounds:Array;
	private var p_sounds_offset:Array;
	private var p_sounds_loops:Array;
	private var p_sounds_offset_all:Number;
	private var p_sounds_loops_all:Number;
	
	public var sets_background_colors:Array;
	public var sets_background_ratios:Array;
	public var sets_background_alphas:Array;
	public var sets_background_types:Array;
	public var sets_background_rotations:Array;
	public var sets_background_gradient_types:Array;
	
	//rotation
	private var p_rotation:Number;
	
	//movie
	private var chart_mc:MovieClip;
	//sector
	var sectors:Array;
	//----------------------------------------
	//getters and setters
	//set x center position
	function set x(new_x:Number):Void
	{
		p_x = new_x;
	}
	//set y center position
	function set y(new_y:Number):Void
	{
		p_y = new_y;
	}
	function get x():Number
	{
		return p_x;
	}
	function get y():Number
	{
		return p_y;
	}
	//set radius
	function set radius(r:Number):Void
	{
		p_radius = r;
	}
	function get radius():Number
	{
		return p_radius;
	}
	//set values
	function set values(v:Array):Void
	{
		p_values = v;
	}
	function get values():Array
	{
		return p_values;
	}
	//set border enabled
	function set border_enabled(e:Boolean):Void
	{
		p_border_enabled = e;
	}
	//set border size
	function set border_size(s:Number):Void
	{
		p_border_size = s;
	}
	//set border alpha
	function set border_alpha(a:Number):Void
	{
		p_border_alpha = a;
	}
	//set border color
	function set border_color(clr:Number):Void
	{
		p_border_color = clr;
	}
	//set animation enabled
	function set animation_enabled(e:Boolean):Void
	{
		p_animation_enabled = e;
	}
	//set animation speed
	function set animation_speed(s:Number):Void
	{
		if (s<=0)
		{
			_root.showError("Animation speed should be positive");
		}else
		{
			p_animation_speed = s;
		}
	}
	//set animation type
	function set animation_type(t:String):Void
	{
		if ((t!='step') and (t!='all'))
		{
			_root.showError("Pie chart animation should be 'step' or 'all'");
		}else
		{
			p_animation_type = t;
		}
	}
	//set background
	function set background_enabled(e:Boolean):Void
	{
		p_background_enabled = e;
	}
	//set background colors
	function set background_colors(clr:Array):Void
	{
		p_background_colors = clr;
	}
	function get background_colors():Array
	{
		return p_background_colors;
	}
	//set background alphas
	function set background_alphas(a:Array):Void
	{
		p_background_alphas = a;
	}
	//set default background color
	function set default_background_color(clr:Number):Void
	{
		if ((clr<0) or (clr>0xFFFFFF))
		{
			_root.showError("Chart background color should be between 0x000000 and 0xFFFFFF");
		}else
		{
			p_background_colors_all = clr;
		}
	}
	//set background alpha
	function set default_background_alpha(a:Number):Void
	{
		if ((a<0) or (a>100))
		{
			_root.showError("Chart background alpha should be between 0 and 100");
		}else
		{
			p_background_alphas_all = a;
		}
	}
	//set background auto color
	function set background_auto_color(e:Boolean):Void
	{
		p_background_auto_color = e;
	}
	//set background tone
	function set background_tone(t:Number):Void
	{
		if ((t<0) or (t>0xFFFFFF))
		{
			_root.showError("Chart background tone should be between 0x000000 and 0xFFFFFF");
		}else
		{
			p_background_tone = t;
		}
	}
	//set rotation
	function set rotation(r:Number):Void
	{
		p_rotation = r;
	}
	//set names
	function set default_show_names(s:Boolean):Void
	{
		p_show_names_all = s;
	}
	function set show_names(s:Array):Void
	{
		p_show_names = s;
	}
	//set names
	function set names(n:Array):Void
	{
		p_names_text = n;
	}
	function get names():Array
	{
		return p_names_text;
	}
	//set values
	function set default_show_values(s:Boolean):Void
	{
		p_show_values_all = s;
	}
	function set show_values(s:Array):Void
	{
		p_show_values = s;
	}
	function set values_prefix(p:String):Void
	{
		p_values_prefix = p;
	}
	function set values_postfix(p:String):Void
	{
		p_values_postfix = p;
	}
	function set decimal_places(p:Number):Void
	{
		p_values_decimal_places = p;
	}
	function set decimal_separator(s:String):Void
	{
		p_values_decimal_separator = s;
	}
	function set thousand_separator(s:String):Void
	{
		p_values_thousand_separator = s;
	}
	//config hints
	function set hints_enabled(e:Boolean):Void
	{
		p_hints_enabled = e;
	}
	function set hints_width(w:Number):Void
	{
		p_hints_width = w;
	}
	function set hints_height(h:Number):Void
	{
		p_hints_height = h;
	}
	function set hints_auto_size(s:Boolean):Void
	{
		p_hints_auto_size = s;
	}
	function set hints_background_enabed(e:Boolean):Void
	{
		p_hints_enabled = e;
	}
	function set hints_background_color(clr:Number):Void
	{
		p_hints_background_color = clr;
	}
	function set hints_border_enabled(e:Boolean):Void
	{
		p_hints_border_enabled = e;
	}
	function set hints_border_color(clr:Number):Void
	{
		p_hints_border_color = clr;
	}
	function set hints_text_format(tf:TextFormat):Void
	{
		p_hints_text_format = tf;
	}
	function set hints_horizontal_position(pos:String):Void
	{
		p_hints_horizontal_position = pos;
	}
	function set hints_vertical_position(pos:String):Void
	{
		p_hints_vertical_position = pos;
	}
	function set captions_text_format(tf:TextFormat):Void
	{
		p_captions_text_format = tf;
	}
	function set captions_radius(r:Number):Void
	{
		p_captions_radius = r;
	}
	function set captions_rotation(r:Number):Void
	{
		p_captions_rotation = r;
	}
	function set captions_rotations(r:Array):Void
	{
		p_captions_rotations = r;
	}
	function set captions_dx(d:Number):Void
	{
		p_captions_dx = d;
	}
	function set captions_dxs(d:Array):Void
	{
		p_captions_dxs = d;
	}
	function set captions_dy(d:Number):Void
	{
		p_captions_dy = d;
	}
	function set captions_dys(d:Array):Void
	{
		p_captions_dys = d;
	}
	function set protrusion_radius(d:Number):Void
	{
		p_select_radius = d;
	}
	function set sounds(s:Array):Void
	{
		p_sounds = s;
	}
	function set sounds_offset(o:Array):Void
	{
		p_sounds_offset = o;
	}
	function set sounds_loops(l:Array):Void
	{
		p_sounds_loops = l;
	}
	function set default_sounds_offset(o:Number):Void
	{
		p_sounds_offset_all = o;
	}
	function set default_sounds_loos(l:Number):Void
	{
		p_sounds_loops_all = l;
	}
	function set urls(u:Array):Void
	{
		p_urls = u;
	}
	function set urls_target(t:Array):Void
	{
		p_urls_target = t;
	}
	function set default_urls_target(t:String):Void
	{
		p_urls_target_all = t;
	}
	//----------------------------------------
	//constructor
	function _2dpie_chart(target_mc:MovieClip)
	{
		var i:Number = 0;
		while (target_mc['chart_'+i]!=undefined)
		{
			i++;
		}
		chart_mc = target_mc.createEmptyMovieClip('chart_'+i,DepthManager.CHART_DEPTH);
		//init defaults		
		p_border_enabled = true;
		p_border_size = 1;
		p_border_color = 0x0000000;
		p_border_alpha = 100;
		p_radius = 100;
		p_animation_enabled = true;
		p_animation_speed = 10;
		p_animation_type = 'step';
		p_background_enabled = true;
		p_background_auto_color = true;
		p_background_colors_all = 0xC1C1C1;//??
		p_background_tone = 0xFFC700;
		p_background_alphas_all = 100;
		p_rotation = 0;
		p_show_values_all = true;
		p_show_names_all = true;
		p_values_prefix = '';
		p_values_postfix = '';
		p_values_decimal_places = 2;
		p_values_decimal_separator = '.';
		p_values_thousand_separator = '';
		p_hints_enabled = true;
		p_hints_auto_size = false
		p_hints_width = 100;
		p_hints_height = 20;
		p_hints_border_enabled = true;
		p_hints_border_color = 0x000000;
		p_hints_background_enabled = true;
		p_hints_background_color = 0xFEE7C0;
		p_hints_text_format = new TextFormat();
		p_hints_text_format.font = 'Verdana';
		p_hints_text_format.align = 'center';
		p_hints_horizontal_position = 'center';
		p_hints_vertical_position = 'top';
		p_captions_radius = 1.4;
		p_select_radius = 10;
		p_sounds_loops_all = 1;
		p_sounds_offset_all = 0;
		p_urls_target_all = '_self';
		p_captions_rotation = 0;
		p_captions_dx = 0;
		p_captions_dy = 0;
		p_captions_text_format = new TextFormat();
		p_captions_text_format.font = 'Verdana';
	}
	//-----------------------------------------
	function paint():Void
	{
		chart_mc._x = p_x;		
		chart_mc._y = p_y;
		process_data();
		config_background();
		config_names();
		config_values();
		config_links();
		config_sectors();		
		paint_sectors();
	}
	function remove():Void
	{
		chart_mc.removeMovieClip();
	}
	//process data
	private function process_data():Void
	{
		p_alphas = new Array();
		var i:Number;
		var s:Number = 0;
		for (i=0;i<p_values.length;i++)
		{
			s += p_values[i];
		}
		//-----------
		var da:Number = 360/s;
		//set alphas
		p_alphas[0] = p_rotation;
		for (i=1;i<p_values.length;i++)
		{
			p_alphas[i] = p_alphas[i-1]+p_values[i-1]*da;
		}
		p_alphas[p_values.length]=360 + p_alphas[0];
	}
	
	public var animation_attribute:String = 'size';
	
	//config sectors
	private function config_sectors():Void
	{
		var lib = new library.functions();
		sectors = new Array();
		var i:Number = 0;
		var txt:String;
		var hnt_text:String;
		var index:Number = 0;
		while (i<p_values.length)
		{
			if (p_values[i] <= 0) {
				i++;
				continue;
			}
			sectors[index] = new chart.elements._2dsector(chart_mc);
			sectors[index].radius = p_radius;
			sectors[index].start_alpha = p_alphas[i];
			sectors[index].end_alpha = p_alphas[i+1];
			sectors[index].border_enabled = (this.border_enableds[i] != undefined)?this.border_enableds[i]:this.p_border_enabled;
			sectors[index].border_size = (this.border_sizes[i] != undefined)?this.border_sizes[i]:this.p_border_size;;
			sectors[index].border_color = (this.border_colors[i] != undefined)?this.border_colors[i]:this.p_border_color;
			sectors[index].border_alpha = (this.border_alphas[i] != undefined)?this.border_alphas[i]:this.p_border_alpha;
			sectors[index].animation_enabled = p_animation_enabled;
			sectors[index].animation_speed = p_animation_speed;
			sectors[index].background_enabled = p_background_enabled;
			sectors[index].background_color = p_background_colors[i];
			sectors[index].background_alpha = p_background_alphas[i];
			sectors[index].animation_attribute = animation_attribute;
			sectors[index].dr = p_select_radius;
			sectors[index].url = p_urls[i];
			sectors[index].url_target = p_urls_target[i];
			sectors[index].sound = p_sounds[i];
			sectors[index].sound_loops = p_sounds_loops[i];
			sectors[index].sound_offset = p_sounds_offset[i];
			
			if (sets_background_types[index] != undefined)
				sectors[index].background_type = sets_background_types[index];
			else
				sectors[index].background_type = background_type;
				
			if (sets_background_colors[index].length > 0)					
				sectors[index].background_colors = sets_background_colors[index];
			else
				sectors[index].background_colors = background_gradient_colors;
				
			if (sets_background_ratios[index].length > 0)		
				sectors[index].background_ratios = sets_background_ratios[index];
			else
				sectors[index].background_ratios = background_gradient_ratios;
					
			if (sets_background_alphas[index].length > 0)				
				sectors[index].background_alphas = sets_background_alphas[index];
			else
				sectors[index].background_alphas = background_gradient_alphas;
					
			if (sets_background_gradient_types[index] != undefined)				
				sectors[index].background_gradient_type = sets_background_gradient_types[index];
			else
				sectors[index].background_gradient_type = background_gradient_type;
					
			if (sets_background_rotations[index] != undefined)
				sectors[index].background_rotation = sets_background_rotations[index];
			else
				sectors[index].background_rotation = background_gradient_rotation;
					
			//labels settings
/*			sectors[i].label_background_enabled = this.labels_background_enabled;
			sectors[i].label_background_color = this.labels_background_color;
			sectors[i].label_background_alpha = this.labels_background_alpha;
			sectors[i].label_border_enabled = this.labels_border_enabled;
			sectors[i].label_border_color = this.labels_border_color;
			sectors[i].label_border_size = this.labels_border_size;
			sectors[i].label_border_alpha = this.labels_border_alpha;
			sectors[i].label_position_type = this.labels_position_type;
			sectors[i].label_x = this.labels_x;
			sectors[i].label_y = this.labels_y;
			sectors[i].label_width = this.labels_width;
			sectors[i].label_height = this.labels_height;
			sectors[i].label_radius = this.labels_radius;
			sectors[i].label_text_format = this.labels_tf;
			if (show_labels[i] != undefined)
			{
				sectors[i].label_show = show_labels[i];
			}else
			{
				sectors[i].label_show = this.show_label;
			}
			sectors[i].label_text = this.labels_text[i];
			sectors[i].label_link_enabled = this.labels_links_enabled;
			sectors[i].label_link_size = this.labels_links_size;
			sectors[i].label_link_color = this.labels_links_color;
			sectors[i].label_link_alpha = this.labels_links_alpha;
			sectors[i].label_link_dot_enabled = this.labels_links_dot_enabled;
			sectors[i].label_link_dot_radius = this.labels_links_dot_radius;
			sectors[i].label_link_dot_background_enabled = this.labels_links_dot_background_enabled;
			sectors[i].label_link_dot_background_color = this.labels_links_dot_background_color;
			sectors[i].label_link_dot_background_alpha = this.labels_links_dot_background_alpha;
			sectors[i].label_link_dot_border_enabled = this.labels_links_dot_border_enabled;
			sectors[i].label_link_dot_border_size = this.labels_links_dot_border_size;
			sectors[i].label_link_dot_border_color = this.labels_links_dot_border_color;
			sectors[i].label_link_dot_border_alpha = this.labels_links_dot_border_alpha;*/
			
			txt = '';			
			hnt_text = '';
			if (p_names_text[i]!=undefined)
			{
				if (p_show_names[i])
				{
					txt = p_names_text[i];
					hnt_text = '<b>'+p_names_text[i]+'</b>, ';
				}
			}
			hnt_text += lib.config_text(String(p_values[i]),p_values_decimal_places,p_values_thousand_separator,p_values_decimal_separator,p_values_prefix,p_values_postfix);
			if (p_show_values[i])
			{
				if (txt!='')
				{
					txt += ', ' + lib.config_text(String(p_values[i]),p_values_decimal_places,p_values_thousand_separator,p_values_decimal_separator,p_values_prefix,p_values_postfix);
				}else
				{
					txt = lib.config_text(String(p_values[i]),p_values_decimal_places,p_values_thousand_separator,p_values_decimal_separator,p_values_prefix,p_values_postfix);
				}
			}
			sectors[index].caption_text = txt;
			if (p_hints_enabled)
			{
				sectors[index]._hint.horizontal_position = p_hints_horizontal_position;
				sectors[index]._hint.vertical_position = p_hints_vertical_position;
				sectors[index]._hint.width = p_hints_width;
				sectors[index]._hint.height = p_hints_height;
				sectors[index]._hint.auto_size = p_hints_auto_size;
				sectors[index]._hint.background_enabled = p_hints_background_enabled;
				sectors[index]._hint.background_color = p_hints_background_color;
				sectors[index]._hint.border_enabled = p_hints_border_enabled;
				sectors[index]._hint.border_color = p_hints_border_color;
				sectors[index]._hint.border_size = hints_border_size;
				sectors[index]._hint.border_alpha = hints_border_alpha;
				sectors[index]._hint.background_alpha = hints_background_alpha;

				var lib:library.functions = new library.functions();
				
				if (hint_texts[index] == undefined)
					sectors[index]._hint.text = lib.createHintText(this.hints_text,p_names_text[i],lib.config_text(String(p_values[i]),p_values_decimal_places,p_values_thousand_separator,p_values_decimal_separator,p_values_prefix,p_values_postfix));				
				else
					sectors[index]._hint.text = lib.createHintText(hint_texts[index],p_names_text[i],lib.config_text(String(p_values[i]),p_values_decimal_places,p_values_thousand_separator,p_values_decimal_separator,p_values_prefix,p_values_postfix));				
				
				sectors[index]._hint.text_format = p_hints_text_format;
			}
			sectors[index].caption_text_format = p_captions_text_format;
			sectors[index].caption_position = p_captions_radius;
			if (p_captions_rotations[i]!=undefined)
			{
				sectors[index].caption_rotation = p_captions_rotations[i];
			}else
			{
				sectors[index].caption_rotation = p_captions_rotation;
			}
			if (p_captions_dxs[i]!=undefined)
			{
				sectors[index].caption_dx = p_captions_dxs[i];
			}else
			{
				sectors[index].caption_dx = p_captions_dx;
			}
			if (p_captions_dys[i]!=undefined)
			{
				sectors[index].caption_dy = p_captions_dys[i];
			}else
			{
				sectors[index].caption_dy = p_captions_dy;
			}
			i++;
			index++;
		}
	}
	//painting sectors
	private function  paint_sectors():Void
	{
		var i:Number;
		if (!p_animation_enabled) {
			simplePaint();
		}else {
			switch (p_animation_type) {
				case 'step':
					paintStep();
					break;
				case 'all':
					simplePaint();
					break;
			}
		}
	}
	
	private function paintStep():Void {
		var ths:Object = this;
		var list_obj:Array = new Array();
		for (var i:Number =0;i<(p_values.length-1);i++)
		{
			list_obj[i] = new Object();
			list_obj[i].onPaintFinish = function(evt:Object)
			{
				if (evt.target.nxt!=undefined)
				{
					evt.target.nxt.paint();
				}else {
					ths.onAllPaintFinish();
				}
			}
			sectors[i].nxt = sectors[i+1];
			sectors[i].addEventListener("onPaintFinish",list_obj[i]);
		}
		sectors[0].paint();
	}
	
	
	private function simplePaint():Void {
		var ths = this;
		var c:Number = 0;
		var t:Number = sectors.length;
		for (var i:Number=0;i<p_values.length;i++) {			
			sectors[i].onPaintFinish = function():Void {
				c++;
				if (c == t)
					ths.onAllPaintFinish();
			}
			sectors[i].paint();
		}
	}
	
	private function onAllPaintFinish():Void {		
		var ws = workspace.workspace_.getInstance();
		var p1:Object = {x: 0, y:ws.chart_area_y};
		var p2:Object = {x: 0, y:ws.chart_area_y + ws.chart_area_height};
		ws.workspace_mc.localToGlobal(p1);
		ws.workspace_mc.localToGlobal(p2);
		chart_mc.globalToLocal(p1);
		chart_mc.globalToLocal(p2);
		PieLabelsDistributor.distribute(this,p1.y,p2.y - p1.y);
	}
	
	//set backgrounds
	private function config_background():Void
	{
		var i:Number;
		if (p_background_auto_color)
		{
			var clr_lib = new library.color_ext();
			var tmp_colors:Array = clr_lib.getColors(p_background_tone,p_values.length);
		}
		if (p_background_colors==undefined)
		{
			p_background_colors = new Array();
		}
		if (p_background_alphas==undefined)
		{
			p_background_alphas = new Array();
		}
		for (i=0;i<p_values.length;i++)
		{
			if (p_background_colors[i]==undefined)
			{
				if (p_background_auto_color)
				{
					p_background_colors[i] = tmp_colors[i];
				}else
				{
					p_background_colors[i] = p_background_colors_all;
				}
			}
			if (p_background_alphas[i]==undefined)
			{
				p_background_alphas[i] = p_background_alphas_all;
			}
		}
	}
	//config names
	private function config_names():Void
	{
		var i:Number;
		if (p_show_names==undefined)
		{
			p_show_names = new Array();
		}
		for (i=0;i<p_values.length;i++)
		{
			if (p_show_names[i]==undefined)
			{
				p_show_names[i] = p_show_names_all;
			}
		}
	}
	//config values
	private function config_values():Void
	{
		var i:Number;
		if (p_show_values==undefined)
		{
			p_show_values = new Array();
		}
		for (i=0;i<p_values.length;i++)
		{
			p_show_values[i] = p_show_values_all;
		}
	}
	//config links
	private function config_links():Void
	{
		var i:Number;
		if (p_urls_target==undefined)
		{
			p_urls_target = new Array();
		}
		if (p_sounds_loops==undefined)
		{
			p_sounds_loops = new Array();
		}
		if (p_sounds_offset==undefined)
		{
			p_sounds_offset = new Array();
		}
		for (i=0;i<p_values.length;i++)
		{
			if (p_urls_target[i]==undefined)
			{
				p_urls_target[i] = p_urls_target_all;
			}
			if (p_sounds_loops[i]==undefined)
			{
				p_sounds_loops[i] = p_sounds_loops_all;
			}
			if (p_sounds_offset[i]==undefined)
			{
				p_sounds_offset[i] = p_sounds_offset_all;
			}
		}
	}
}