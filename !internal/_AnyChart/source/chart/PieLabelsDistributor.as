﻿import chart.PieLabel;

class chart.PieLabelsDistributor {
	
	private static var pieLabels:Array = new Array();
	
	public static var minGap:Number = 4;
	
	public static function addLabel(label:PieLabel):Void {
		label.field._visible = false;
		pieLabels.push(label);
	}
	
	public static function distribute(pChart:Object,minY:Number,height:Number):Void {
		var leftLabels:Array = new Array();
		var rightLabels:Array = new Array();
		
		if (!pChart.checkOverlap) {
			for (var i:Number = 0;i<pieLabels.length;i++) {
				pieLabels[i].field._x = pieLabels[i].radius * Math.cos(pieLabels[i].angle * Math.PI/180);
				if (Math.cos(pieLabels[i].angle * Math.PI/180) < 0)
					pieLabels[i].field._x -= pieLabels[i].field._width;
				else if (Math.cos(pieLabels[i].angle * Math.PI/180) == 0)
					pieLabels[i].field._x -= pieLabels[i].field._width/2;					
			}
			showAll();
			drawSimpleLinks(pChart);
			return;
		}
		
		for (var i:Number = 0;i<pieLabels.length;i++) {
			var angle:Number = pieLabels[i].angle * Math.PI/180;
			if (Math.cos(angle) > 0)
				rightLabels.push(pieLabels[i]);
			else
				leftLabels.push(pieLabels[i]);
		}
		trace (leftLabels.length+','+rightLabels.length+','+pieLabels.length);
		sort();
		doDistribute(leftLabels,minY,height);
		for (var i:Number = 0;i<leftLabels.length;i++) {
			trace (leftLabels[i].allocatedY+','+leftLabels[i].field.text+','+leftLabels[i].field);
			leftLabels[i].field._x = - pChart.radius - leftLabels[i].width - pChart.captionsPadding;
		}
		
		doDistribute(rightLabels,minY,height);				
		for (var i:Number = 0;i<rightLabels.length;i++) {
			rightLabels[i].field._x = pChart.radius + pChart.captionsPadding;
		}
		showAll();
		drawLinks(pChart);
	}
	
	private static function drawLinks(pChart:Object):Void {
		for (var i:Number = 0;i<pieLabels.length;i++) {
			drawLink(pieLabels[i],pChart);
		}
	}
	
	private static function drawSimpleLinks(pChart:Object):Void {
		for (var i:Number = 0;i<pieLabels.length;i++) {
			drawSimpleLink(pieLabels[i],pChart);
		}
	}
	
	private static function drawLink(pieLabel:PieLabel,pChart:Object):Void {
		if (!pChart.linksEnabled)
			return;
		var e:Number = pChart.linksEdgeSize;
		if (Math.cos(pieLabel.angle*Math.PI/180) > 0)
			e = -pChart.linksEdgeSize;

		var r:Number = pChart.radius + pChart.linksSpace;
		var y:Number = r*Math.sin(pieLabel.angle * Math.PI/180);
		var x:Number = r*Math.cos(pieLabel.angle * Math.PI/180);
		pieLabel.labelMovie.lineStyle(pChart.linksSize,pChart.linksColor,pChart.linksAlpha);
		pieLabel.labelMovie.moveTo(pieLabel.sliceConnectorX,pieLabel.sliceConnectorY);
		pieLabel.labelMovie.lineTo(x,y);
		pieLabel.labelMovie.lineTo(pieLabel.connectorX + e,y);
		pieLabel.labelMovie.lineTo(pieLabel.connectorX,pieLabel.connectorY);
	}
	
	private static function drawSimpleLink(pieLabel:PieLabel,pChart:Object):Void {
		if (!pChart.linksEnabled)
			return;

		pieLabel.labelMovie.lineStyle(pChart.linksSize,pChart.linksColor,pChart.linksAlpha);
		pieLabel.labelMovie.moveTo(pieLabel.sliceConnectorX,pieLabel.sliceConnectorY);
		pieLabel.labelMovie.lineTo(pieLabel.connectorX,pieLabel.connectorY);
	}
	
	private static function doDistribute(targetLabels:Array,minY:Number, height:Number):Void {
		if (isOverlap(targetLabels)) {
            adjustInwards(targetLabels);
        }
        // if still overlapping, do something else...
        if (isOverlap(targetLabels)) {
            adjustDownwards(targetLabels,minY, height);
        }
        if (isOverlap(targetLabels)) { 
            adjustUpwards(targetLabels,minY, height);
        }
        if (isOverlap(targetLabels)) {  
            spreadEvenly(targetLabels,minY, height);
        }
		
	}
	
	private static function showAll():Void {
		for (var i:Number = 0;i<pieLabels.length;i++) {
			pieLabels[i].field._visible = true;
		}
	}
	
	private static function isOverlap(labels:Array):Boolean {
		var y:Number = labels[0].getLowerY();;
		for (var i:Number = 0;i<labels.length;i++) {
			if (y > labels[i].getLowerY())
				return true;
			y = labels[i].getUpperY();
		}
        return false;
    }

	private static function adjustInwards(labels:Array):Void {
		var lower:Number = 0;
		var upper:Number = labels.length - 1;
		while (upper > lower) {
			if (lower < upper - 1) {
				var r0:PieLabel = labels[lower];
				var r1:PieLabel = labels[lower+1];
				if (r1.getLowerY() < r0.getUpperY()) {
					var adjust:Number = r0.getUpperY() - r1.getLowerY() + minGap;
					r1.allocatedY = r1.allocatedY + adjust;
				}					
			}
			var r2:PieLabel = labels[upper - 1];
			var r3:PieLabel = labels[upper];
			if (r2.getUpperY() > r3.getLowerY()) {
				var adjust:Number = (r2.getUpperY() - r3.getLowerY()) + minGap;
				r2.allocatedY = r2.allocatedY - adjust;
			}
			lower ++;
			upper --;
		}
    }
	
	private static function adjustDownwards(labels:Array,minY:Number,height:Number):Void {
		for (var i:Number = 0;i< labels.length;i++) {
			var record0:PieLabel = labels[i];
            var record1:PieLabel = labels[i+1];
            if (record1.getLowerY() < record0.getUpperY()) {
                record1.allocatedY = Math.min(minY + height, record0.getUpperY() + minGap + record1.height/2);
            }
		}
    }
	
	private static function adjustUpwards(labels:Array,minY:Number,height:Number):Void {
		for (var i:Number = labels.length - 1;i>0;i--) {
			var record0:PieLabel = labels[i];
            var record1:PieLabel = labels[i - 1];
			if (record1.getUpperY() > record0.getLowerY()) {
                record1.allocatedY = Math.max(minY, record0.getLowerY() - minGap - record1.height/2);
			}
		}
	}
	
	private static function spreadEvenly(labels:Array,minY:Number,height:Number):Void {
		var y:Number = minY;
		var sumOfLabelsHeight:Number = 0;
		var i:Number;
		for (i=0;i<labels.length;i++) {
			sumOfLabelsHeight += labels[i].height;
		}
		var d:Number = (height - sumOfLabelsHeight)/(labels.length);
		if (d < 0)
			d = 0;
		var y:Number = minY;
		for (i=1;i<=labels.length;i++) {
			labels[labels.length - i].allocatedY = y;
			y += labels[labels.length - i].height + d;
		}
    }
		
	private static function sort():Void {
		for (var i:Number = 0;i<pieLabels.length - 1;i++) {
			for (var j:Number = i;j<pieLabels.length;j++) {
				if (pieLabels[j].allocatedY < pieLabels[i].allocatedY)
					swap(i,j);
			}
		}		
	}
	
	private static function swap(i:Number,j:Number):Void {
		var res:Array = new Array();
		for (var c:Number = 0;c<pieLabels.length;c++) {
			if (c == i)
				res[c] = pieLabels[j];
			else if (c == j)
				res[c] = pieLabels[i];
			else
				res[c] = pieLabels[c];
		}
		pieLabels = res;
	}
}