﻿class chart.dot_chart
{
	public var hints_text:String = '<b>{NAME}</b>, {ARGUMENT}, {VALUE}';
	public var hint_texts:Array;
	
	//attributes
	//position
	private var d_x:Number;
	private var d_y:Number;
	
	//size
	private var d_width:Number;
	private var d_height:Number;
	
	//values
	private var d_values:Array;
	private var d_arguments:Array;
	
	public var block_names:Array;
	
	//dots background settings
	private var d_background_enabled:Array;
	private var d_background_color:Array;
	private var d_background_alpha:Array;
	private var d_background_enabled_block:Array;
	private var d_background_color_block:Array;
	private var d_background_alpha_block:Array;
	private var d_background_enabled_all:Boolean;
	private var d_background_color_all:Number;
	private var d_background_alpha_all:Number;
	//auto color
	private var d_background_auto_color:Boolean;
	private var d_background_tone:Number;
	
	//dots border settings
	private var d_border_enabled:Array;
	private var d_border_size:Array;
	private var d_border_color:Array;
	private var d_border_alpha:Array;
	private var d_border_enabled_block:Array;
	private var d_border_size_block:Array;
	private var d_border_color_block:Array;
	private var d_border_alpha_block:Array;
	private var d_border_enabled_all:Boolean;
	private var d_border_size_all:Number;
	private var d_border_color_all:Number;
	private var d_border_alpha_all:Number;
	
	//animation settings
	private var d_animation_enabled:Boolean;
	private var d_animation_type:String;
	private var d_animation_speed:Number;
	private var d_animation_attribute:String;
	
	//names
	private var d_names:Array;
	private var d_show_name:Array;
	private var d_show_name_block:Array;
	private var d_show_name_all:Boolean;
	private var d_names_position:String;
	private var d_names_text_format:TextFormat;
	private var d_names_rotations:Array;
	private var d_names_dxs:Array;
	private var d_names_dys:Array;
	private var d_names_rotation:Number;
	private var d_names_dx:Number;
	private var d_names_dy:Number;
	
	//values text
	private var d_values_text:Array;
	private var d_show_values:Array;
	private var d_show_values_block:Array;
	private var d_show_values_all:Boolean;
	private var d_values_prefix:String;
	private var d_values_postfix:String;
	private var d_values_decimal_places:Number;
	private var d_values_decimal_separator:String;
	private var d_values_thousand_separator:String;
	private var d_values_position:String;
	private var d_values_text_format:TextFormat;
	private var d_values_rotations:Array;
	private var d_values_rotation:Number;
	private var d_values_dxs:Array;
	private var d_values_dys:Array;
	private var d_values_dx:Number;
	private var d_values_dy:Number;
	
	//arguments text
	private var d_arguments_text:Array;
	private var d_show_argument:Array;
	private var d_show_argument_block:Array;
	private var d_show_argument_all:Boolean;
	private var d_arguments_prefix:String;
	private var d_arguments_postfix:String;
	private var d_arguments_decimal_places:Number;
	private var d_arguments_decimal_separator:String;
	private var d_arguments_thousand_separator:String;
	
	//dots type
	private var d_dot_type:Array;
	private var d_dot_type_block:Array;
	private var d_dot_type_all:String;
	private var d_dot_image:Array;
	private var d_dot_image_block:Array;
	private var d_dot_image_all:String;
	private var d_dot_radius:Array;
	private var d_dot_radius_block:Array;
	private var d_dot_radius_all:Number;
	
	//hints
	private var d_hints_enabled:Boolean;
	private var d_hints_auto_size:Boolean;
	private var d_hints_width:Number;
	private var d_hints_height:Number;
	private var d_hints_horizontal_position:String;
	private var d_hints_vertical_position:String;
	private var d_hints_background_enabled:Boolean;
	private var d_hints_background_color:Number;
	private var d_hints_border_enabled:Boolean;
	private var d_hints_border_color:Number;
	private var d_hints_text_format:TextFormat;
	
	public var hints_border_size:Number = 1;
	public var hints_border_alpha:Number = 100;
	public var hints_background_alpha:Number = 100;
	
	//chart movie
	private var chart_mc:MovieClip;
	
	//max & min
	private var d_maximum_value:Number;
	private var d_minimum_value:Number;
	private var d_maximum_argument:Number;
	private var d_minimum_argument:Number;
	private var d_values_grid_maximum:Number;
	private var d_values_grid_minimum:Number;
	private var d_arguments_grid_maximum:Number;
	private var d_arguments_grid_minimum:Number;
	
	//painting settings
	private var d_values_scale:Number;
	private var d_zero_value:Number;
	private var d_arguments_scale:Number;
	private var d_zero_argument:Number;
	
	//urls
	private var d_urls:Array;
	private var d_urls_target:Array;
	private var d_urls_target_block:Array;
	private var d_urls_target_all:String;
	
	//sounds
	private var d_sounds:Array;
	private var d_sounds_loops:Array;
	private var d_sounds_start_pos:Array;
	private var d_sounds_loops_all:Number;
	private var d_sounds_start_pos_all:Number;
	
	//chart dots
	var dots:Array;
	//---------------------------------------------------
	//getters and setters
	function set x(new_x:Number):Void
	{
		d_x = new_x;
	}
	function set y(new_y:Number):Void
	{
		d_y = new_y;
	}
	function set width(w:Number):Void
	{
		d_width = w;
	}
	function set height(h:Number):Void
	{
		d_height = h;
	}
	function set values(v:Array):Void
	{
		d_values = v;
	}
	function get values():Array
	{
		var res:Array = new Array();
		var i:Number;
		var j:Number;
		for (i=0;i<d_values_text.length;i++)
		{
			res[i] = new Array();
			for (j=0;j<d_values_text[i].length;j++)
			{
				res[i][j] = d_arguments_text[i][j] + ', ' + d_values_text[i][j];
			}
		}
		return res;
	}
	function set arguments(a:Array):Void
	{
		d_arguments = a;
	}
	function get arguments():Array
	{
		return d_arguments_text;
	}
	function set background_enabled(e:Array):Void
	{
		d_background_enabled = e;
	}
	function set background_color(clr:Array):Void
	{
		d_background_color = clr;
	}
	function get background_color():Array
	{
		return d_background_color;
	}
	function set background_alpha(a:Array):Void
	{
		d_background_alpha = a;
	}
	function set block_backround_enabled(e:Array):Void
	{
		d_background_enabled_block = e;
	}
	function set block_background_color(clr:Array):Void
	{
		d_background_color_block = clr;
	}
	function get block_background_color():Array 
	{
		return d_background_color_block;
	}
	function set block_backgorund_alpha(a:Array):Void
	{
		d_background_alpha_block = a;
	}
	function set default_background_enabled(e:Boolean):Void
	{
		d_background_enabled_all = e;
	}
	function set default_background_color(clr:Number):Void
	{
		d_background_color_all = clr;
	}
	function set default_background_alpha(a:Number):Void
	{
		d_background_alpha_all = a;
	}
	function set border_enabled(e:Array):Void
	{
		d_border_enabled = e;
	}
	function set border_size(s:Array):Void
	{
		d_border_size = s;
	}
	function set border_color(clr:Array):Void
	{
		d_border_color = clr;
	}
	function set border_alpha(a:Array):Void
	{
		d_border_alpha = a;
	}
	function set block_border_enabled(e:Array):Void
	{
		d_border_enabled_block = e;
	}
	function set block_border_size(s:Array):Void
	{
		d_border_size_block = s;
	}
	function set block_border_color(clr:Array):Void
	{
		d_border_color_block = clr;
	}
	function set block_border_alpha(a:Array):Void
	{
		d_border_alpha_block = a;
	}
	function set default_border_enabled(e:Boolean):Void
	{
		d_border_enabled_all = e;
	}
	function set default_border_size(s:Number):Void
	{
		d_border_size_all = s;
	}
	function set default_border_color(clr:Number):Void
	{
		d_border_color_all = clr;
	}
	function set default_border_alpha(a:Number):Void
	{
		d_border_alpha_all = a;
	}
	function set background_auto_color(e:Boolean):Void
	{
		d_background_auto_color = e;
	}
	function set background_tone(t:Number):Void
	{
		d_background_tone = t;
	}
	function set animation_enabled(e:Boolean):Void
	{
		d_animation_enabled = e;
	}
	function set animation_type(t:String):Void
	{
		d_animation_type = t;
	}
	function set animation_attribute(a:String):Void
	{
		d_animation_attribute = a;
	}
	function set animation_speed(s:Number):Void
	{
		d_animation_speed = s;
	}
	function set names(n:Array):Void
	{
		d_names = n;
	}
	function set names_rotations(r:Array):Void
	{
		d_names_rotations = r;
	}
	function set names_rotation(r:Number):Void
	{
		d_names_rotation = r;
	}
	function set values_rotations(r:Array):Void
	{
		d_values_rotations = r;
	}
	function set values_rotation(r:Number):Void
	{
		d_values_rotation = r;
	}
	function set values_dxs(d:Array):Void
	{
		d_values_dxs = d;
	}
	function set values_dx(d:Number):Void
	{
		d_values_dx = d;
	}
	function set values_dys(d:Array):Void
	{
		d_values_dys = d;
	}
	function set values_dy(d:Number):Void
	{
		d_values_dy = d;
	}
	function set names_dxs(d:Array):Void
	{
		d_names_dxs = d;
	}
	function set names_dx(d:Number):Void
	{
		d_names_dx = d;
	}
	function set names_dys(d:Array):Void
	{
		d_names_dys = d;
	}
	function set names_dy(d:Number):Void
	{
		d_names_dy = d;
	}
	function get names():Array
	{
		return d_names;
	}
	function set show_names(s:Array):Void
	{
		d_show_name = s;
	}
	function set block_show_names(s:Array):Void
	{
		d_show_name_block = s;
	}
	function set default_show_names(s:Boolean):Void
	{
		d_show_name_all = s;
	}
	function set show_values(s:Array):Void
	{
		d_show_values = s;
	}
	function set block_show_values(s:Array):Void
	{
		d_show_values_block = s;
	}
	function set default_show_values(s:Boolean):Void
	{
		d_show_values_all = s;
	}
	function set values_prefix(p:String):Void
	{
		d_values_prefix = p;
	}
	function get values_prefix():String
	{
		return d_values_prefix;
	}
	function set values_postfix(p:String):Void
	{
		d_values_postfix = p;
	}
	function get values_postfix():String
	{
		return d_values_postfix;
	}
	function set decimal_places(p:Number):Void
	{
		d_values_decimal_places = p;
	}
	function get decimal_places():Number
	{
		return d_values_decimal_places;
	}
	function set decimal_separator(s:String):Void
	{
		d_values_decimal_separator = s;
	}
	function get decimal_separator():String
	{
		return d_values_decimal_separator;
	}
	function set thousand_separator(s:String):Void
	{
		d_values_thousand_separator = s;
	}
	function get thousand_separator():String
	{
		return d_values_thousand_separator;
	}
	function set show_arguments(s:Array):Void
	{
		d_show_argument = s;
	}
	function set block_show_aguments(s:Array):Void
	{
		d_show_argument_block = s;
	}
	function set default_show_arguments(s:Boolean):Void
	{
		d_show_argument_all = s;
	}
	function set arguments_prefix(p:String):Void
	{
		d_arguments_prefix = p;
	}
	function get arguments_prefix():String
	{
		return d_arguments_prefix;
	}
	function set arguments_postfix(p:String):Void
	{
		d_arguments_postfix = p;
	}
	function get arguments_postfix():String
	{
		return d_arguments_postfix;
	}
	function set arguments_decimal_places(p:Number):Void
	{
		d_arguments_decimal_places = p;
	}
	function get arguments_decimal_places():Number
	{
		return d_arguments_decimal_places;
	}
	function set arguments_deciaml_separator(s:String):Void
	{
		d_arguments_decimal_separator = s;
	}
	function get arguments_decimal_separator():String
	{
		return d_arguments_decimal_separator;
	}
	function set arguments_thousand_separator(s:String):Void
	{
		d_arguments_thousand_separator = s;
	}
	function get arguments_thousand_separator():String
	{
		return d_arguments_thousand_separator;
	}
	function set hints_enabled(e:Boolean):Void
	{
		d_hints_enabled = e;
	}
	function set hints_width(w:Number):Void
	{
		d_hints_width = w;
	}
	function set hints_height(h:Number):Void
	{
		d_hints_height = h;
	}
	function set hints_auto_size(a:Boolean):Void
	{
		d_hints_auto_size = a;
	}
	function set hints_horizontal_position(p:String):Void
	{
		d_hints_horizontal_position = p;
	}
	function set hints_vertical_position(p:String):Void
	{
		d_hints_vertical_position = p;
	}
	function set hints_background_enabled(e:Boolean):Void
	{
		d_hints_background_enabled = e;
	}
	function set hints_background_color(clr:Number):Void
	{
		d_hints_background_color = clr;
	}
	function set hints_border_enabled(e:Boolean):Void
	{
		d_hints_border_enabled = e;
	}
	function set hints_border_color(clr:Number):Void
	{
		d_hints_border_color = clr;
	}
	function set hints_text_format(tf:TextFormat):Void
	{
		d_hints_text_format = tf;
	}
	function set dots_size(s:Array):Void
	{
		d_dot_radius = s;
	}
	function set block_dots_size(s:Array):Void
	{
		d_dot_radius_block = s;
	}
	function set default_dots_size(s:Number):Void
	{
		d_dot_radius_all = s;
	}
	function set dots_type(t:Array):Void
	{
		d_dot_type = t;
	}
	function set block_dots_type(t:Array):Void
	{
		d_dot_type_block = t;
	}
	function set default_dots_type(t:String):Void
	{
		d_dot_type_all = t;
	}
	function set dots_image(i:Array):Void
	{
		d_dot_image = i;
	}
	function set block_dots_image(i:Array):Void
	{
		d_dot_image_block = i;
	}
	function set default_dot_image(i:String):Void
	{
		d_dot_image_all = i;
	}
	function set values_position(pos:String):Void
	{
		d_values_position = pos;
	}	
	function set names_position(pos:String):Void
	{
		d_names_position = pos;
	}
	function set names_text_format(tf:TextFormat):Void
	{
		d_names_text_format = tf;
	}
	function set values_text_format(tf:TextFormat):Void
	{
		d_values_text_format = tf;
	}
	function set maximum_value(val:Number):Void
	{
		d_values_grid_maximum = val;
	}
	function get maximum_value():Number
	{
		return d_maximum_value;
	}
	function set minimum_value(val:Number):Void
	{
		d_values_grid_minimum = val;
	}
	function get minimum_value():Number
	{
		return d_minimum_value;
	}
	function set maximum_argument(a:Number):Void
	{
		d_arguments_grid_maximum = a;
	}
	function get maximum_argument():Number
	{
		return d_maximum_argument;
	}
	function set minimum_argument(m:Number):Void
	{
		d_arguments_grid_minimum = m;
	}
	function get minimum_argument():Number
	{
		return d_minimum_argument;
	}
	function get values_grid_maximum():Number
	{
		return d_values_grid_maximum;
	}
	function get values_grid_minimum():Number
	{
		return d_values_grid_minimum;
	}
	function get arguments_grid_maximum():Number
	{
		return d_arguments_grid_maximum;
	}
	function get arguments_grid_minimum():Number
	{
		return d_arguments_grid_minimum;
	}
	function set urls(u:Array):Void
	{
		d_urls = u;
	}
	function set urls_target(ut:Array):Void
	{
		d_urls_target = ut;
	}
	function set default_urls_target(t:String):Void
	{
		d_urls_target_all = t;
	}
	//set sounds
	function set sounds(s:Array):Void
	{
		d_sounds = s;
	}
	//set sounds loops:
	function set sounds_loops(l:Array):Void
	{
		d_sounds_loops = l;
	}
	//set start offset
	function set sounds_offsets(o:Array):Void
	{
		d_sounds_start_pos = o;
	}
	//set default loops cound
	function set default_sound_loops(n:Number):Void
	{
		d_sounds_loops_all = n;
	}
	//set default start offset
	function set default_sound_offset(o:Number):Void
	{
		d_sounds_start_pos_all = o;
	}
	function get x():Number
	{
		return d_x;
	}
	function get y():Number
	{
		return d_y;
	}
	function get width():Number
	{
		return d_width;
	}
	function get height():Number
	{
		return d_height;
	}
	//---------------------------------------------------
	//constructor
	function dot_chart(target_mc:MovieClip)
	{
		var i:Number = 0;
		while (target_mc['chart_movie_'+i]!=undefined)
		{
			i++;
		}
		chart_mc = target_mc.createEmptyMovieClip('chart_movie_'+i,DepthManager.CHART_DEPTH);
		//defaults
		d_border_enabled_all = false;
		d_border_size_all = 1;
		d_border_color_all = 0x000000;
		d_border_alpha_all = 100;
		d_background_enabled_all = true;
		d_background_color_all = 0xC1C1C1;
		d_background_alpha_all = 100;
		d_background_auto_color = true;
		d_background_tone = 0xFFC700;
		d_show_values_all = true;
		d_values_position = 'bottom';
		d_show_argument_all = true;
		d_dot_type_all = 'circle';
		d_dot_radius_all = 10;
		d_animation_enabled = true;
		d_animation_type = 'all';
		d_animation_attribute = 'alpha';
		d_animation_speed = 10;
		d_hints_enabled = true;
		d_hints_auto_size = false;
		d_hints_width = 100;
		d_hints_height = 20;
		d_hints_border_enabled = true;
		d_hints_border_color = 0x000000;
		d_hints_background_enabled = true;
		d_hints_background_color = 0xFEE7C0;
		d_hints_text_format = new TextFormat();
		d_hints_text_format.font = 'Verdana';
		d_hints_text_format.align = 'center';
		d_hints_horizontal_position = 'center';
		d_hints_vertical_position = 'top';
		d_urls_target_all = '_blank';
		d_sounds_loops_all = 1;
		d_sounds_start_pos_all = 0;
		d_names_position = 'top';
		d_names_text_format = new TextFormat();
		d_names_text_format.font = 'Verdana';
		d_names_text_format.bold = true;
		d_values_text_format = new TextFormat();
		d_values_text_format.font = 'Verdana';		
		d_arguments_decimal_places = 2;
		d_values_decimal_places = 2;
		d_arguments_prefix = '';
		d_values_prefix = '';
		d_arguments_postfix = '';
		d_values_postfix = '';
		d_arguments_decimal_separator = '.';
		d_values_decimal_separator = '.';
		d_arguments_thousand_separator = '';
		d_values_thousand_separator = '';
		d_values_rotation = 0;
		d_names_rotation = 0;
		d_names_dx = 0;
		d_names_dy = 0;
		d_values_dx = 0;
		d_values_dy = 0;
	}
	//----------------------------------------------------
	
	function initMaxMin():Void {
		process_data();
	}
	
	function paint():Void
	{
		chart_mc._x = d_x;
		chart_mc._y = d_y;
		process_data();
		process_borders();
		process_backgrounds();
		process_values();
		process_arguments();
		process_dots();
		process_sounds();
		process_urls();
		config_dots();
		paint_dots();
	}
	//----------------------------------------------------	
	//process data
	private function process_data():Void
	{
		var i:Number;
		var j:Number;
		var lib = new library.functions();
		d_maximum_value = lib.getMax(d_values);
		d_minimum_value = lib.getMin(d_values);
		d_maximum_argument = lib.getMax(d_arguments);
		d_minimum_argument = lib.getMin(d_arguments);
		if ((d_values_grid_maximum<d_maximum_value) or (d_values_grid_maximum==undefined))
		{
			d_values_grid_maximum = d_maximum_value;
		}
		if ((d_values_grid_minimum>d_minimum_value) or (d_values_grid_minimum==undefined))
		{
			d_values_grid_minimum = d_minimum_value;
		}
		if ((d_arguments_grid_minimum>d_minimum_argument) or (d_arguments_grid_minimum==undefined))
		{
			d_arguments_grid_minimum = d_minimum_argument;
		}
		if ((d_arguments_grid_maximum<d_maximum_argument) or (d_arguments_grid_maximum==undefined))
		{
			d_arguments_grid_maximum = d_maximum_argument;
		}
		//set scale
		d_values_scale = d_height/(d_values_grid_maximum - d_values_grid_minimum);
		d_zero_value = d_values_grid_maximum*d_values_scale;
		d_arguments_scale = d_width/(d_arguments_grid_maximum - d_arguments_grid_minimum);
		d_zero_argument = -d_arguments_grid_minimum*d_arguments_scale;
	}
	//aptin dots
	private function paint_dots():Void
	{
		var i:Number;
		var j:Number;
		if ((!d_animation_enabled) or (d_animation_type=='all'))
		{
			for (i=0;i<dots.length;i++)
			{
				for (j=0;j<dots[i].length;j++)
				{
					dots[i][j].paint();
				}
			}
		}else
		{
			//set next columns:
			for (i=0;i<d_values.length;i++)
			{
				for (j=0;j<(d_values[i].length-1);j++)
				{
					dots[i][j].nxt = dots[i][j+1];
				}
				if (i!=(d_values.length-1))
				{
					dots[i][(d_values[i].length-1)].nxt = dots[i+1][0];
				}
			}
			var tmp:Array = new Array();
			for (i=0;i<d_values.length;i++)
			{
				tmp[i] = new Array();
				for (j=0;j<d_values[i].length;j++)
				{
					tmp[i][j] = new Object();
					tmp[i][j].onPaintFinish = function(evt:Object)
					{
						if (evt.target.nxt!=undefined)
						{
							evt.target.nxt.paint();
						}
					}
					dots[i][j].addEventListener("onPaintFinish",tmp[i][j]);
				}
			}
			dots[0][0].paint();
		}
	}
	//set dots attributes
	private function config_dots():Void
	{
		var i:Number;
		var j:Number;
		var txt:String;
		dots = new Array();
		for (i=0;i<d_values.length;i++)
		{
			dots[i] = new Array();
			for (j=0;j<d_values[i].length;j++)
			{
				dots[i][j] = new chart.elements.dot(chart_mc);
				dots[i][j].x = d_zero_argument + d_arguments[i][j]*d_arguments_scale;
				dots[i][j].y = d_zero_value - d_values[i][j]*d_values_scale;
				dots[i][j].radius = d_dot_radius[i][j];
				dots[i][j].type = d_dot_type[i][j];
				dots[i][j].image = d_dot_image[i][j];
				dots[i][j].background_enabled = d_background_enabled[i][j];
				dots[i][j].background_color = d_background_color[i][j];
				dots[i][j].background_alpha = d_background_alpha[i][j];
				dots[i][j].border_enabled = d_border_enabled[i][j];
				dots[i][j].border_size = d_border_size[i][j];
				dots[i][j].border_color = d_border_color[i][j];
				dots[i][j].border_alpha = d_border_alpha[i][j];
				dots[i][j].name_text = d_names[i][j];
				dots[i][j].name_position = d_names_position;
				dots[i][j].name_text_format = d_names_text_format;
				dots[i][j].show_name = d_show_name[i][j];
				dots[i][j].url = d_urls[i][j];
				dots[i][j].url_target = d_urls_target[i][j];
				if (d_names_rotations[i][j]!=undefined)
				{
					dots[i][j].name_rotation = d_names_rotations[i][j];
				}else
				{
					dots[i][j].name_rotation = d_names_rotation;
				}
				if (d_values_rotations[i][j]!=undefined)
				{
					dots[i][j].value_rotation = d_values_rotations[i][j];
				}else
				{
					dots[i][j].value_rotation = d_values_rotation;
				}
				if (d_names_dxs[i][j]!=undefined)
				{
					dots[i][j].name_dx = d_names_dxs[i][j];
				}else
				{
					dots[i][j].name_dx = d_names_dx;
				}
				if (d_names_dys[i][j]!=undefined)
				{
					dots[i][j].name_dy = d_names_dys[i][j];
				}else
				{
					dots[i][j].name_dy = d_names_dy;
				}
				if (d_values_dxs[i][j]!=undefined)
				{
					dots[i][j].value_dx = d_values_dxs[i][j];
				}else
				{
					dots[i][j].value_dx = d_values_dx;
				}
				if (d_values_dys[i][j]!=undefined)
				{
					dots[i][j].value_dy = d_values_dys[i][j];
				}else
				{
					dots[i][j].value_dy = d_values_dy;
				}
				if (d_show_argument[i][j])
				{
					txt = d_arguments_text[i][j];
					if (d_show_values[i][j])
					{
						txt += ', '+d_values_text[i][j];
					}
				}else
				{
					if (d_show_values[i][j])
					{
						txt = d_values_text[i][j];
					}
				}
				dots[i][j].value_text = txt;
				dots[i][j].value_position = d_values_position;
				dots[i][j].show_value = d_show_values[i][j];
				dots[i][j].value_text_format = d_values_text_format;
				dots[i][j].animation_enabled = d_animation_enabled;
				dots[i][j].animation_attribute = d_animation_attribute;
				dots[i][j].animation_speed = d_animation_speed;
				if (d_hints_enabled)
				{
					dots[i][j].hint.auto_size = d_hints_auto_size;
					dots[i][j].hint.width = d_hints_width;
					dots[i][j].hint.height = d_hints_height;
					dots[i][j].hint.horizontal_position = d_hints_horizontal_position;
					dots[i][j].hint.vertical_position = d_hints_vertical_position;
					dots[i][j].hint.background_enabled = d_hints_background_enabled;
					dots[i][j].hint.background_color = d_hints_background_color;
					dots[i][j].hint.border_enabled = d_hints_border_enabled;
					dots[i][j].hint.border_color = d_hints_border_color;
					dots[i][j].hint.border_size = hints_border_size;
					dots[i][j].hint.border_alpha = hints_border_alpha;
					dots[i][j].hint.background_alpha = hints_background_alpha;

					var lib:library.functions = new library.functions();
					if (hint_texts[i][j] == undefined)
						dots[i][j].hint.text = lib.createHintText(this.hints_text,d_names[i][j],d_values_text[i][j],d_arguments_text[i][j]);
					else
						dots[i][j].hint.text = lib.createHintText(hint_texts[i][j],d_names[i][j],d_values_text[i][j],d_arguments_text[i][j]);

					dots[i][j].hint.text_format = d_hints_text_format;
				}
			}
		}
	}
	//config dots
	private function process_dots():Void
	{
		var i:Number;
		var j:Number;
		if (d_dot_type==undefined)
		{
			d_dot_type = new Array();
		}
		if (d_dot_radius==undefined)
		{
			d_dot_radius = new Array();
		}
		if ((d_dot_image==undefined) and ((d_dot_image_block!=undefined) or (d_dot_image_all!=undefined)))
		{
			d_dot_image = new Array();
		}
		for (i=0;i<d_values.length;i++)
		{
			if ((d_dot_image[i]==undefined) and ((d_dot_image_block!=undefined) or (d_dot_image_all!=undefined)))
			{
				d_dot_image[i] = new Array();
			}
			if (d_dot_type[i]==undefined)
			{
				d_dot_type[i] = new Array();				
			}
			if (d_dot_radius[i]==undefined)
			{
				d_dot_radius[i] = new Array();
			}
			for (j=0;j<d_values[i].length;j++)
			{
				if (d_dot_type[i][j]==undefined)
				{
					if (d_dot_type_block[i]!=undefined)
					{
						d_dot_type[i][j] = d_dot_type_block[i];
					}else
					{
						d_dot_type[i][j] = d_dot_type_all;
					}
				}
				if (d_dot_radius[i][j]==undefined)
				{
					if (d_dot_radius_block[i]!=undefined)
					{
						d_dot_radius[i][j] = d_dot_radius_block[i];
					}else
					{
						d_dot_radius[i][j] = d_dot_radius_all;
					}
				}
				if (d_dot_image[i][j]==undefined)
				{
					if (d_dot_image_block[i]!=undefined)
					{
						d_dot_image[i][j] = d_dot_image_block[i];
					}
					if (d_dot_image_all!=undefined)
					{
						d_dot_image[i][j] = d_dot_image_all;
					}
				}
			}
		}
	}
	//config arguments text
	private function process_arguments():Void
	{
		var i:Number;
		var j:Number;
		var lib = new library.functions();
		d_arguments_text = new Array();
		if (d_show_argument==undefined)
		{
			d_show_argument = new Array();
		}
		for (i=0;i<d_values.length;i++)
		{
			d_arguments_text[i] = new Array();
			if (d_show_argument[i]==undefined)
			{
				d_show_argument[i] = new Array();
			}
			for (j=0;j<d_values[i].length;j++)
			{
				if (d_show_argument[i][j]==undefined)
				{
					if (d_show_argument_block[i]!=undefined)
					{
						d_show_argument[i][j] = d_show_argument_block[i];
					}else
					{
						d_show_argument[i][j] = d_show_argument_all;
					}
				}
				d_arguments_text[i][j] = lib.config_text(String(d_arguments[i][j]),d_arguments_decimal_places,d_arguments_thousand_separator,d_arguments_decimal_separator,d_arguments_prefix,d_arguments_postfix);
			}
		}
	}
	//config values text
	private function process_values():Void
	{
		var i:Number;
		var j:Number;
		var lib = new library.functions();
		d_values_text = new Array();
		if (d_show_values==undefined)
		{
			d_show_values = new Array();
		}
		for (i=0;i<d_values.length;i++)
		{
			d_values_text[i] = new Array();
			if (d_show_values[i]==undefined)
			{
				d_show_values[i] = new Array();
			}
			for (j=0;j<d_values[i].length;j++)
			{
				if (d_show_values[i][j]==undefined)
				{
					if (d_show_values_block[i]!=undefined)
					{
						d_show_values[i][j] = d_show_values_block[i];
					}else
					{
						d_show_values[i][j] = d_show_values_all;
					}
				}
				d_values_text[i][j] = lib.config_text(String(d_values[i][j]),d_values_decimal_places,d_values_thousand_separator,d_values_decimal_separator,d_values_prefix,d_values_postfix);
			}
		}
	}
	//config background
	private function process_backgrounds():Void
	{
		var i:Number;
		var j:Number;
		if (d_background_auto_color)
		{
			var clr_lib = new library.color_ext();
			var tmp_colors:Array = clr_lib.getColors(d_background_tone,d_values.length);
			delete clr_lib;
		}
		if (d_background_enabled===undefined)
		{
			d_background_enabled = new Array();
		}
		if (d_background_color==undefined)
		{
			d_background_color = new Array();
		}
		if (d_background_alpha==undefined)
		{
			d_background_alpha = new Array();
		}
		for (i=0;i<d_values.length;i++)
		{
			if (d_background_enabled[i]==undefined)
			{
				d_background_enabled[i] = new Array();
			}
			if (d_background_color[i]==undefined)
			{
				d_background_color[i] = new Array();
			}
			if (d_background_alpha[i]==undefined)
			{
				d_background_alpha[i] = new Array();
			}
			for (j=0;j<d_values[i].length;j++)
			{
				if (d_background_enabled[i][j]==undefined)
				{
					if (d_background_enabled_block[i]!=undefined)
					{
						d_background_enabled[i][j] = d_background_enabled_block[i];
					}else
					{
						d_background_enabled[i][j] = d_background_enabled_all;
					}					
				}
				if (d_background_color[i][j]==undefined)
				{
					if (!d_background_auto_color)
					{
						if (d_background_color_block[i]!=undefined)
						{
							d_background_color[i][j] = d_background_color_block[i];
						}else
						{
							d_background_color[i][j] = d_background_color_all;
						}
					}else
					{
						d_background_color[i][j] = tmp_colors[i];
					}
				}
				if (d_background_alpha[i][j]==undefined)
				{
					if (d_background_alpha_block[i]!=undefined)
					{
						d_background_alpha[i][j] = d_background_alpha_block[i];
					}else
					{
						d_background_alpha[i][j] = d_background_alpha_all;
					}
				}
			}
		}
	}
	//config borders
	private function process_borders():Void
	{
		var i:Number;
		var j:Number;
		if (d_border_enabled==undefined)
		{
			d_border_enabled = new Array();
		}
		if (d_border_size==undefined)
		{
			d_border_size = new Array();
		}
		if (d_border_color==undefined)
		{
			d_border_color = new Array();			
		}
		if (d_border_alpha==undefined)
		{
			d_border_alpha = new Array();
		}
		for (i=0;i<d_values.length;i++)
		{
			if (d_border_enabled[i]==undefined)
			{
				d_border_enabled[i] = new Array();
			}
			if (d_border_size[i]==undefined)
			{
				d_border_size[i] = new Array();
			}
			if (d_border_color[i]==undefined)
			{
				d_border_color[i] = new Array();
			}
			if (d_border_alpha[i]==undefined)
			{
				d_border_alpha[i] = new Array();
			}
			for (j=0;j<d_values[i].length;j++)
			{
				if (d_border_enabled[i][j]==undefined)
				{
					if (d_border_enabled_block[i]!=undefined)
					{
						d_border_enabled[i][j] = d_border_enabled_block[i];
					}else
					{
						d_border_enabled[i][j] = d_border_enabled_all;
					}
				}
				if (d_border_size[i][j]==undefined)
				{
					if (d_border_size_block[i]!=undefined)
					{
						d_border_size[i][j] = d_border_size_block[i];
					}else
					{
						d_border_size[i][j] = d_border_size_all;
					}
				}
				if (d_border_color[i][j]==undefined)
				{
					if (d_border_color_block[i]!=undefined)
					{
						d_border_color[i][j] = d_border_color_block[i];
					}else
					{
						d_border_color[i][j] = d_border_color_all;
					}
				}
				if (d_border_alpha[i][j]==undefined)
				{
					if (d_border_alpha_block[i]!=undefined)
					{
						d_border_alpha[i][j] = d_border_alpha_block;
					}else
					{
						d_border_alpha[i][j] = d_border_alpha_all;
					}
				}
			}
		}
	}
	//config sounds
	private function process_sounds():Void
	{
		var i:Number;
		var j:Number;
		if (d_sounds_loops==undefined)
		{
			d_sounds_loops = new Array();			
		}
		if (d_sounds_start_pos==undefined)
		{
			d_sounds_start_pos = new Array();
		}
		for (i=0;i<d_values.length;i++)
		{
			if (d_sounds_loops[i]==undefined)
			{
				d_sounds_loops[i] = new Array();				
			}
			if (d_sounds_start_pos[i]==undefined)
			{
				d_sounds_start_pos[i] = new Array();
			}
			for (j=0;j<d_values[i].length;j++)
			{
				if (d_sounds_loops[i][j]==undefined)
				{
					d_sounds_loops[i][j] = d_sounds_loops_all;
				}
				if (d_sounds_start_pos[i][j]==undefined)
				{
					d_sounds_start_pos[i][j] = d_sounds_start_pos_all;
				}
			}
		}
	}
	//config urls
	private function process_urls():Void
	{
		var i:Number;
		var j:Number;
		if (d_urls_target==undefined)
		{
			d_urls_target = new Array();
		}
		for (i=0;i<d_values.length;i++)
		{
			if (d_urls_target[i]==undefined)
			{
				d_urls_target[i] = new Array();
			}
			for (j=0;j<d_values[i].length;j++)
			{
				if (d_urls_target[i][j]==undefined)
				{
					if (d_urls_target_block[i]!=undefined)
					{
						d_urls_target[i][j] = d_urls_target_block[i];
					}else
					{
						d_urls_target[i][j] = d_urls_target_all;
					}
				}
			}
		}
	}
}