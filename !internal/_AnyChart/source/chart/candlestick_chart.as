﻿import library.*;

class chart.candlestick_chart
{
	
	public var hints_text:String = 'open: {OPEN}<br />close: {CLOSE}<br />high: {HIGH}<br />low: {LOW}';
	
	//variables
	//position
	private var c_x:Number;
	private var c_y:Number;
	
	//size
	private var c_width:Number;
	private var c_height:Number;
	
	//bulls
	private var c_bulls_background_enabled:Boolean;
	private var c_bulls_background_color:Number;
	private var c_bulls_background_alpha:Number;
	private var c_bulls_border_enabled:Boolean;
	private var c_bulls_border_size:Number;
	private var c_bulls_border_color:Number;
	private var c_bulls_border_alpha:Number;
	
	//bears
	private var c_bears_background_enabled:Boolean;
	private var c_bears_background_color:Number;
	private var c_bears_background_alpha:Number;
	private var c_bears_border_enabled:Boolean;
	private var c_bears_border_size:Number;
	private var c_bears_border_color:Number;
	private var c_bears_border_alpha:Number;
	
	//current settings
	private var c_background_enabled:Array;
	private var c_background_color:Array;
	private var c_background_alpha:Array;
	private var c_border_enabled:Array;
	private var c_border_size:Array;
	private var c_border_color:Array;
	private var c_border_alpha:Array;
	
	//values
	private var c_open_values:Array;
	private var c_close_values:Array;
	private var c_high_values:Array;
	private var c_low_values:Array;
	private var c_indexes:Array;
	private var c_maximum_value:Number;
	private var c_minimum_value:Number;
	private var c_grid_maximum:Number;
	private var c_grid_minimum:Number;
	private var c_maximum_index:Number;
	private var c_minimum_index:Number;
	private var c_grid_maximum_index:Number;
	private var c_grid_minimum_index:Number;
	
	public var show_names:Array;
	public var default_show_names:Boolean = true;;
	public var names_position:String = 'bottom';
	public var names_text_format:TextFormat;
	public var names_rotation:Number = 0;
	public var names_placement:String = 'set';
	public var names_dx:Number = 0;
	public var names_dy:Number = 0;
	
	//painter
	private var c_zero_x:Number;
	private var c_x_scale:Number;
	private var c_zero_y:Number;
	private var c_y_scale:Number;
	
	//hints
	private var c_hints_enabled:Boolean;
	public var hints_auto_size:Boolean = false;
	private var c_hints_width:Number;
	private var c_hints_height:Number;
	private var c_hints_background_enabled:Boolean;
	private var c_hints_background_color:Number;
	public var hints_background_alpha:Number;
	private var c_hints_border_enabled:Boolean;
	private var c_hints_border_color:Number;
	public var hints_border_size:Number;
	public var hints_border_alpha:Number;
	private var c_hints_horizontal_position:String;
	private var c_hints_vertical_position:String;
	private var c_hints_text_format:TextFormat;
	
	//values
	private var c_values_prefix:String;
	private var c_values_postfix:String;
	private var c_values_thousand_separator:String;
	private var c_values_decimal_separator:String;
	private var c_values_decimal_places:Number;
	
	private var chart_mc:MovieClip;
	
	private var c_candlesticks_width:Number;
	
	var candlesticks:Array;
	
	public var names:Array = new Array();
	
	public var candlestick_space:Number = 10;
	//-------------------------------------------------
	//getters and setters
	//position
	function set candlesticks_width(w:Number):Void
	{
		c_candlesticks_width = w;
	}
	function set x(new_x:Number):Void
	{
		c_x = new_x;
	}
	function get x():Number
	{
		return c_x;
	}
	function set y(new_y:Number):Void
	{
		c_y = new_y;
	}
	function get y():Number
	{
		return c_y;
	}
	//size
	function set width(w:Number):Void
	{
		c_width = w;
	}
	function get width():Number
	{
		return c_width;
	}
	function set height(h:Number):Void
	{
		c_height = h;
	}
	function get height():Number
	{
		return c_height;
	}
	//values
	function set open_values(v:Array):Void
	{
		c_open_values = v;
	}
	function set close_values(v:Array):Void
	{
		c_close_values = v;
	}
	function set high_values(v:Array):Void
	{
		c_high_values = v;
	}
	function set low_values(v:Array):Void
	{
		c_low_values = v;
	}
	function set indexes(ind:Array):Void
	{
		var i:Number;
		for (i=0;i<ind.length;i++)
		{
			if (ind[i]==undefined)
			{
				_root.showError("You should set indexes in all sets or omit them everywhere");
			}
		}
		c_indexes = ind;
	}
	//config bulls
	function set bulls_background_enabled(e:Boolean):Void
	{
		c_bulls_background_enabled = e;
	}
	function set bulls_background_color(clr:Number):Void
	{
		c_bulls_background_color = clr;
	}
	function set bulls_background_alpha(a:Number):Void
	{
		c_bulls_background_alpha = a;
	}
	function set bulls_border_enabled(e:Boolean):Void
	{
		c_bulls_border_enabled = e;
	}
	function set bulls_border_size(s:Number):Void
	{
		c_bulls_border_size = s;
	}
	function set bulls_border_alpha(a:Number):Void
	{
		c_bulls_border_alpha = a;
	}
	function set bulls_border_color(value:Number):Void {
		c_bulls_border_color = value;
	}
	//beras
	function set bears_background_enabled(e:Boolean):Void
	{
		c_bears_background_enabled = e;
	}
	function set bears_background_color(clr:Number):Void
	{
		c_bears_background_color = clr;		
	}
	function set bears_backgorund_alpha(a:Number):Void
	{
		c_bears_background_alpha = a;
	}
	function set bears_border_enabled(e:Boolean):Void
	{
		c_bears_border_enabled = e;
	}
	function set bears_border_size(s:Number):Void
	{
		c_bears_border_size = s;
	}
	function set bears_border_alpha(a:Number):Void
	{
		c_bears_border_alpha = a;
	}
	function set bears_border_color(value:Number):Void {
		c_bears_border_color = value;
	}
	//set all settings
	function set backround_enabled(e:Array):Void
	{
		c_background_enabled = e;
	}
	function set background_color(clr:Array):Void
	{
		c_background_color = clr;
	}
	function set background_alpha(a:Array):Void
	{
		c_background_alpha = a;
	}
	function set border_enabled(e:Array):Void
	{
		c_border_enabled = e;
	}
	function set border_size(s:Array):Void
	{
		c_border_size = s;
	}
	function set border_color(clr:Array):Void
	{
		c_border_color = clr;
	}
	function set border_alpha(a:Array):Void
	{
		c_border_alpha = a;
	}
	//hints
	function set hints_enabled(e:Boolean):Void
	{
		c_hints_enabled = e;
	}
	function set hints_width(w:Number):Void
	{
		c_hints_width = w;
	}
	function set hints_height(h:Number):Void
	{
		c_hints_height = h;
	}
	function set hints_background_enabled(e:Boolean):Void
	{
		c_hints_background_enabled = e;
	}
	function set hints_background_color(clr:Number):Void
	{
		c_hints_background_color = clr;
	}
	function set hints_border_enabled(e:Boolean):Void
	{
		c_hints_border_enabled = e;
	}
	function set hints_border_color(clr:Number):Void
	{
		c_hints_border_color = clr;
	}
	function set hints_horizontal_position(pos:String):Void
	{
		c_hints_horizontal_position = pos;
	}
	function set hints_vertical_position(pos:String):Void
	{
		c_hints_vertical_position = pos;
	}
	//maximum and minimum
	function set grid_maximum(m:Number):Void
	{
		c_grid_maximum = m;
	}
	function get grid_maximum():Number
	{
		return c_grid_maximum;
	}
	function get maximum_value():Number
	{
		return c_maximum_value;
	}
	function set grid_minimum(m:Number):Void	
	{
		c_grid_minimum = m;
	}
	function get grid_minimum():Number
	{
		return c_grid_minimum;
	}
	function get minimum_value():Number
	{
		return c_minimum_value;
	}
	function get maximum_index():Number
	{
		return c_maximum_index;
	}
	function get minimum_index():Number
	{
		return c_minimum_index;
	}
	function set index_grid_maximum(m:Number):Void
	{
		c_grid_maximum_index = m;
	}
	function get index_grid_maximum():Number
	{
		return c_grid_maximum_index;
	}
	function set index_grid_minimum(m:Number):Void
	{
		c_grid_minimum_index = m;
	}
	function get index_grid_minimum():Number
	{
		return c_grid_minimum_index;
	}
	//values text
	function set values_prefix(p:String):Void
	{
		c_values_prefix = p;
	}
	function get values_prefix():String
	{
		return c_values_prefix;
	}
	function set values_postfix(p:String):Void
	{
		c_values_postfix = p;
	}
	function get values_postfix():String
	{
		return c_values_postfix;
	}
	function set decimal_separator(s:String):Void
	{
		c_values_decimal_separator = s;
	}
	function get decimal_separator():String
	{
		return c_values_decimal_separator;
	}
	function set thousnad_separator(s:String):Void
	{
		c_values_thousand_separator = s;
	}
	function get thousand_separator():String
	{
		return c_values_thousand_separator;
	}
	function set decimal_places(p:Number):Void
	{
		c_values_decimal_places = p;
	}
	function get decimal_places():Number
	{
		return c_values_decimal_places;
	}
	//-------------------------------------------------
	//constructor
	function candlestick_chart(target_mc:MovieClip)
	{
		names_text_format = new TextFormat();
		names_text_format.font = 'Verdana';
		var i:Number = 0;
		chart_mc = target_mc.createEmptyMovieClip('chart_movie_'+i,DepthManager.CHART_DEPTH);
		//default values
		//bulls
		c_bulls_background_enabled = true;
		c_bulls_background_color = 0xFFFFFF;
		c_bulls_background_alpha = 100;
		c_bulls_border_enabled = true;
		c_bulls_border_size = 1;
		c_bulls_border_color = 0x000000;
		c_bulls_border_alpha = 100;
		//bears
		c_bears_background_enabled = true;
		c_bears_background_color = 0x000000;
		c_bears_background_alpha = 100;
		c_bears_border_enabled = true;
		c_bears_border_size = 1;
		c_bears_border_color = 0x000000;
		c_bears_border_alpha = 100;
		//hints
		c_hints_enabled = true;
		c_hints_width = 100;
		c_hints_height = 60;
		c_hints_border_enabled = true;
		c_hints_border_color = 0x000000;
		hints_border_size = 1;
		hints_border_alpha = 100;
		c_hints_background_enabled = true;
		c_hints_background_color = 0xFEE7C0;
		hints_background_alpha = 100;
		c_hints_text_format = new TextFormat();
		c_hints_text_format.font = 'Verdana';
		c_hints_text_format.align = 'left';
		c_hints_horizontal_position = 'center';
		c_hints_vertical_position = 'top';
		//candlesticks
		c_candlesticks_width = 4;
		//values text
		c_values_decimal_places = 2;
		c_values_decimal_separator = '.';
		c_values_thousand_separator = '';
		c_values_prefix = '';
		c_values_postfix = '';
	}
	//---------------------------------------------------
	function paint():Void
	{		
		process_arguments();
		process_settings();
		process_values();
		config_candlesticks();
		paint_candlesticks();
		chart_mc._x = c_x;
		chart_mc._y = c_y;
	}
	function remove():Void
	{
		chart_mc.removeMovieClip();
	}
	//----------------------------------------------------
	//configure candlesticks
	private function config_candlesticks():Void
	{
		var lib = new functions();
		var i:Number;
		var open_text:String;
		var close_text:String;
		var high_text:String;
		var low_text:String;
		candlesticks = new Array();
		
		for (i=0;i<c_high_values.length;i++)
		{
			candlesticks[i] = new chart.elements.candlestick(chart_mc);
			candlesticks[i].width = c_candlesticks_width;
			candlesticks[i].x = c_zero_x + c_indexes[i]*c_x_scale;
			candlesticks[i].open_y = c_zero_y - c_open_values[i]*c_y_scale;
			candlesticks[i].close_y = c_zero_y - c_close_values[i]*c_y_scale;
			candlesticks[i].high_y = c_zero_y - c_high_values[i]*c_y_scale;
			candlesticks[i].low_y = c_zero_y - c_low_values[i]*c_y_scale;
			candlesticks[i].background_enabled = c_background_enabled[i];
			candlesticks[i].background_color = c_background_color[i];
			candlesticks[i].background_alpha = c_background_alpha[i];
			candlesticks[i].border_enabled = c_border_enabled[i];
			candlesticks[i].border_size = c_border_size[i];
			candlesticks[i].border_color = c_border_color[i];
			candlesticks[i].border_alpha = c_border_alpha[i];
			if (c_hints_enabled)
			{
				open_text = lib.config_text(String(c_open_values[i]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				close_text = lib.config_text(String(c_close_values[i]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				high_text = lib.config_text(String(c_high_values[i]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				low_text = lib.config_text(String(c_low_values[i]),c_values_decimal_places,c_values_thousand_separator,c_values_decimal_separator,c_values_prefix,c_values_postfix);
				candlesticks[i]._hint.horizontal_position = c_hints_horizontal_position;
				candlesticks[i]._hint.vertical_position = c_hints_vertical_position;
				candlesticks[i]._hint.auto_size = hints_auto_size;
				candlesticks[i]._hint.width = c_hints_width;
				candlesticks[i]._hint.height = c_hints_height;
				candlesticks[i]._hint.background_enabled = c_hints_background_enabled;
				candlesticks[i]._hint.background_color = c_hints_background_color;
				candlesticks[i]._hint.background_alpha = hints_background_alpha;
				candlesticks[i]._hint.border_enabled = c_hints_border_enabled;
				candlesticks[i]._hint.border_color = c_hints_border_color;
				candlesticks[i]._hint.border_size = hints_border_size;
				candlesticks[i]._hint.border_alpha = hints_border_alpha;
				candlesticks[i]._hint.text_format = c_hints_text_format;
				candlesticks[i]._hint.text = library.functions.getInstance().getCandlestickHintText(hints_text, names[i], open_text,close_text,high_text,low_text);
			}
		}
	}
	
	public function initMaxMin():Void {
		this.process_arguments();
		this.process_values();
	}
	
	//----------------------------------------------------
	//process values
	private function process_values():Void
	{
		var lib = new library.functions();
		//------------------------------------------------
		c_maximum_index = lib.getMax(c_indexes);
		c_minimum_index = lib.getMin(c_indexes);
		c_maximum_value = lib.getMax([lib.getMax(c_high_values),lib.getMax(c_low_values), lib.getMax(c_open_values),  lib.getMax(c_close_values)]);
		c_minimum_value = lib.getMin([lib.getMin(c_high_values),lib.getMin(c_low_values), lib.getMin(c_open_values),  lib.getMin(c_close_values)]);;
		//------------------------------------------------
		if ((c_maximum_value>c_grid_maximum) or (c_grid_maximum==undefined))
		{
			c_grid_maximum = c_maximum_value;
		}
		if ((c_minimum_value<c_grid_minimum) or (c_grid_minimum==undefined))
		{
			c_grid_minimum = c_minimum_value;
		}
		if ((c_maximum_index>c_grid_maximum_index) or (c_grid_maximum_index==undefined))
		{
			c_grid_maximum_index = c_maximum_index;
		}
		if ((c_minimum_index<c_grid_minimum_index) or (c_grid_minimum_index==undefined))
		{
			c_grid_minimum_index = c_minimum_index;
		}
		trace (c_candlesticks_width);
		if (c_x_scale == undefined) {
			var dx:Number = c_grid_maximum_index - c_grid_minimum_index + 1;
			var w:Number = (c_width - ((dx - 1) * candlestick_space)) / (dx);
			trace (w);
			c_candlesticks_width = w;			
		}else {
			var dx:Number = c_grid_maximum_index - c_grid_minimum_index + 1;
			var w:Number = (c_width - ((dx - 1) * candlestick_space)) / (dx);
			c_x += c_candlesticks_width/2;
			c_width -= c_candlesticks_width;
		}
		c_x_scale = (c_width)/(c_grid_maximum_index - c_grid_minimum_index);
		c_zero_x = - c_x_scale*c_grid_minimum_index;
		//------------------------------------------------
		c_y_scale = c_height/(c_grid_maximum - c_grid_minimum);
		c_zero_y = c_y_scale*c_grid_maximum;				
	}
	//----------------------------------------------------
	//process arguments
	private function process_arguments():Void
	{
		var i:Number;
		if ((c_indexes!=undefined) and (c_indexes.length!=0))
		{
			var j:Number;
			var tmp:Number;
			for (i=0;i<(c_indexes.length-1);i++)
			{
				for (j=(i+1);j<c_indexes.length;j++)
				{
					if (c_indexes[j]<c_indexes[i])
					{
						tmp = c_indexes[i];
						c_indexes[i] = c_indexes[j];
						c_indexes[j] = tmp;
						
						tmp = c_open_values[i];
						c_open_values[i] = c_open_values[j];
						c_open_values[j] = tmp;
						
						tmp = c_close_values[i];
						c_close_values[i] = c_close_values[j];
						c_close_values[j] = tmp;
						
						tmp = c_high_values[i];
						c_high_values[i] = c_high_values[j];
						c_high_values[j] = tmp;
						
						tmp = c_low_values[i];
						c_low_values[i] = c_low_values[j];
						c_low_values[j] = tmp;
						
						tmp = c_open_values[i];
						c_open_values[i] = c_open_values[j];
						c_open_values[j] = tmp;
						
						tmp = c_background_enabled[i];
						c_background_enabled[i] = c_background_enabled[j];
						c_background_enabled[j] = tmp;
						
						tmp = c_background_color[i];
						c_background_color[i] = c_background_color[j];
						c_background_color[j] = tmp;
						
						tmp = c_background_alpha[i];
						c_background_alpha[i] = c_background_alpha[j];
						c_background_alpha[j] = tmp;
						
						tmp = c_border_enabled[i];
						c_border_enabled[i] = c_border_enabled[j];
						c_border_enabled[j] = tmp;
						
						tmp = c_border_size[i];
						c_border_size[i] = c_border_size[j];
						c_border_size[j] = tmp;
						
						tmp = c_border_color[i];
						c_border_color[i] = c_border_color[j];
						c_border_color[j] = tmp;
						
						tmp = c_border_alpha[i];
						c_border_alpha[i] = c_border_alpha[j];
						c_border_alpha[j] = tmp;
					}
				}
			}
		}else
		{
			c_indexes = new Array();
			for (i=0;i<c_high_values.length;i++)
			{
				c_indexes[i] = i;
			}
		}
	}
	//----------------------------------------------------
	//process settings
	private function process_settings():Void
	{
		var i:Number;
		if (c_background_color==undefined)
		{
			c_background_color = new Array();
		}
		if (c_background_alpha==undefined)
		{
			c_background_alpha = new Array();
		}
		if (c_background_enabled==undefined)
		{
			c_background_enabled = new Array();
		}
		if (c_border_enabled==undefined)
		{
			c_border_enabled = new Array();
		}
		if (c_border_size==undefined)
		{
			c_border_size = new Array();
		}
		if (c_border_alpha==undefined)
		{
			c_border_alpha = new Array();
		}
		if (c_border_color==undefined)
		{
			c_border_color = new Array();
		}
		for (i=0;i<c_high_values.length;i++)
		{
			if (c_background_enabled[i]==undefined)
			{
				if (c_open_values[i]>c_close_values[i])
				{
					c_background_enabled[i] = c_bears_background_enabled;
				}else
				{
					c_background_enabled[i] = c_bulls_background_enabled;
				}
			}
			if (c_background_color[i]==undefined)
			{
				if (c_open_values[i]>c_close_values[i])
				{
					c_background_color[i] = c_bears_background_color;
				}else
				{
					c_background_color[i] = c_bulls_background_color;
				}
			}
			if (c_background_alpha[i]==undefined)
			{
				if (c_open_values[i]>c_close_values[i])
				{
					c_background_alpha[i] = c_bears_background_alpha;
				}else
				{
					c_background_alpha[i] = c_bulls_background_alpha;
				}
			}
			if (c_border_enabled[i]==undefined)
			{
				if (c_open_values[i]>c_close_values[i])
				{
					c_border_enabled[i] = c_bears_border_enabled;
				}else
				{
					c_border_enabled[i] = c_bulls_border_enabled;
				}
			}
			if (c_border_size[i]==undefined)
			{
				if (c_open_values[i]>c_close_values[i])
				{
					c_border_size[i] = c_bears_border_size;
				}else
				{
					c_border_size[i] = c_bulls_border_size;
				}
			}
			if (c_border_color[i]==undefined)
			{
				if (c_open_values[i]>c_close_values[i])
				{
					c_border_color[i] = c_bears_border_color;
				}else
				{
					c_border_color[i] = c_bulls_border_color;
				}
			}
			if (c_border_alpha[i]==undefined)
			{
				if (c_open_values[i]>c_close_values[i])
				{
					c_border_alpha[i] = c_bears_border_alpha;
				}else
				{
					c_border_alpha[i] = c_bulls_border_alpha;
				}
			}
		}
	}
	
	private function paint_name(index:Number):Void {
		var show:Boolean = show_names[index] == undefined ? default_show_names : show_names[index];
		if (!show || names[index] == undefined)
			return;
		var x:Number = candlesticks[index].x;
		var y:Number = candlesticks[index].y;
		var mdx:Number = -1/2;
		var mdy:Number = 0;
		switch (names_position) {
			case 'top':
				mdy = -1;
				break;
			case 'bottom':
				y = candlesticks[index].y + candlesticks[index].height;
				break;
		}		
		var pos:Object = {x:x+c_x,y:y+c_y};
		chart_mc.localToGlobal(pos);
		if (names_rotation != 0) {
			var format:TextFormat = _global[names_text_format.font];
			format.size = names_text_format.size;
			format.color = names_text_format.color;
		}
		workspace.workspace_.getInstance().addSetNameTextField(names_position, names[index], format, pos.x,pos.y,names_rotation, names_dx, names_dy,mdx,mdy,false,names_placement == 'chart');
	}
	
	//----------------------------------------------------------
	//paint candlesticks
	private function paint_candlesticks():Void
	{
		var i:Number;
		for (i=0;i<c_high_values.length;i++)
		{
			candlesticks[i].paint();
			paint_name(i);
		}
	}
}