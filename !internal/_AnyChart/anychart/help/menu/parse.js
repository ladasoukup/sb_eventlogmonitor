var structure = new Array();
var id = new Number();
var level = new Number();
var tree;
//--------------------------------------------------
function getStructure(xml)
{
  id = 0;
  level = 0;
  //-----------------------------------------
  var parser = new DOMImplementation();
  var domDoc = parser.loadXML(xml);
  var docRoot = domDoc.getDocumentElement();
  //-----------------------------------------
  //get structure
  processStructure(docRoot);
  //display the data
  showMenu();
}
//--------------------------------
function processStructure(node)
{
  var obj = new Object();
  obj.level = '';
  //check level
  var tmp_node = node;
  while (tmp_node.nodeName!="root")
  {
	obj.level+=' ';
    tmp_node = tmp_node.getParentNode();
  }
  //get attributes
  if (node.getAttributes().getNamedItem("url")!=null)
  {
    obj.url = node.getAttributes().getNamedItem("url").getNodeValue();
  }
  if (node.getAttributes().getNamedItem("label")!=null)
  {
    obj.label = node.getAttributes().getNamedItem("label").getNodeValue();
  }
  //set node
  structure[structure.length] = obj;
  //check subnodes
  if (node.getChildNodes().length>0)
  {
    var i = new Number();
    for (i=0;i<node.getChildNodes().length;i++)
	{
	  processStructure(node.getChildNodes().item(i));
	}
  }
  //-----------------
}
//--------------------------------------
function showMenu()
{
  var i = new Number();
  tree = new treemenu('tree', true, true, false);
  tree.put(1,structure[0].label,'main',structure[0].url,'','','');
  for (i=1;i<structure.length;i++)
  {
    tree.add(0,structure[i].level+structure[i].label,'main',structure[i].url,'','','');
  }
  document.getElementById('menu_div').innerHTML = tree;
}