' --------------------------------------------------------------------
' This file contains some common MySQL commands that might help you
' debug problems. Don't type the - in front of the commands!!
' --------------------------------------------------------------------
1) First, you must log into MySQL.
  a) Start the MySQL Command Line Client
  b) Log in with your root password (created when you installed MySQL)

2) Switch to the sbeventlog database
-use sbeventlog 

3) Display the database tables for sbeventlog (current database)
-show tables

4) Now, check the integrity of the database tables
-check table sbevtlog_alerts
-check table sbevtlog_cfg
-check table sbevtlog_computers
-check table sbevtlog_cron
-check table sbevtlog_events
-check table sbevtlog_filters
-check table sbevtlog_queue_alert
-check table sbevtlog_queue_mail
-check table sbevtlog_queue_secret
