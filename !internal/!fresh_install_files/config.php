<?php
define("AdminEmail", "user@example.com");
define("PageCharSet", "UTF-8");
define("Language", "en");
define("UserAuth", false);

// SQL SETTINGS
define("EZSQL_DB_USER", "root");			// <-- db user
define("EZSQL_DB_PASSWORD", "password");		// <-- db password
define("EZSQL_DB_NAME", "sbeventlogmonitor");		// <-- db pname
define("EZSQL_DB_HOST", "localhost");			// <-- server host
//define("EZSQL_DB_CHARSET", "utf8");			// <-- server CHARSET (leave empty for default)
define("DB_PREFIX","sbEvtLog_");

// SMARTY SETTINGS
define("SMARTY_template_dir", "./templates/");
define("SMARTY_cache_dir", "./templates_cache/");
define("SMARTY_compile_check", false);
define("SMARTY_caching", true);
define("SMARTY_cache_lifetime", (60 * 120));

?>