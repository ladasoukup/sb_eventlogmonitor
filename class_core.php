<?php
class sb_core {
	var $version = "0.77b beta3";
	var $app_name = "SB EventLog Monitor";
	var $debug_on = true;
	var $AutoRefresh_enable = false;

	function __construct() {
		global $smarty;
		$smarty->register_block('dynamic', array(&$this, 'smarty_block_dynamic'), false);
		$smarty->register_function('anychart', array(&$this, 'charts_insert_html'));
		$smarty->assign("app_name", $this->app_name);
		$smarty->assign("version", $this->version);
		if ($this->debug_on) {
			error_reporting(E_ALL);
		} else {
			error_reporting(E_ALL & ~E_NOTICE);
		}
	}

	function ezsql_set_charset($charset) {
	global $db;
		if (!empty($charset)) {
			$db->query("set character_set_results=".$charset);
			$db->query("set character_set_connection=".$charset);
			$db->query("set character_set_client=".$charset);
		}
	}

	function object2array($object) {
		$return = NULL;
		if(is_array($object)) {
			foreach($object as $key => $value)
				$return[$key] = $this->object2array($value);
			} else {
				$var = get_object_vars($object);
				if($var) {
					foreach($var as $key => $value)
						$return[$key] = $this->object2array($value);
				} else {
					return strval($object); // strval and everything is fine
				}
			}
		return $return;
	}

	function GetCacheID($lang = Language) {
		global $smarty;
		$_SERVER["QUERY_STRING"] = str_replace("sub_group=-*-NULL-*-", "sub_group=", $_SERVER["QUERY_STRING"]);
		$GET_sub = $_GET["sub"];
		if (empty($GET_sub)) $GET_sub = "-NULL-";
		$ret = $_GET["module"] . "|" . $GET_sub . "|" . $lang . "|" . $_SESSION["evtlog_user_group"] . "|" . md5($_SERVER["QUERY_STRING"]);
		$smarty->assign("cacheID", $ret);
		return $ret;
	}

	function GetTranslations() {
		global $smarty;

		$xml_path = "./languages/_languages.xml";
		if (file_exists($xml_path)) {
			$xml_data = simplexml_load_file($xml_path);
			foreach ($xml_data->language as $xml_line){
				$xml_line = $this->object2array($xml_line);
				$GLOBALS['_i18n_LangTable_'][$xml_line["langID"]] = $xml_line["langTitle"];
			}
			$smarty->assign("_i18n_LangTable_", $GLOBALS['_i18n_LangTable_']);
		}
	}

	function LoadTranslationTable($lang = Language) {
		//$GLOBALS['_i18n_TEXTS_']["Summary"] = "POKUS...";

		$path = 'languages/' . $lang . '.lng';
		if (file_exists($path)) {
			$entries = file($path);

			foreach ($entries as $row) {
				if (substr(ltrim($row),0,2) == '//') // ignore comments
					continue;

				$keyValuePair = explode('=', $row);
				// multiline values: the first line with an equal sign '=' will start a new key=value pair
				$key = trim($keyValuePair[0]);
				if (isset($keyValuePair[1])) { $value = trim($keyValuePair[1]); } else { unset($value); }

				if ((!empty($key)) && (!empty($value))) {
					$GLOBALS['_i18n_TEXTS_'][$key] = $value;
				}
			}
			return true;
		}
		return false;

	}

	function ModuleRun($module) {
		global $db, $safesql, $smarty, $cacheID, $template_file, $group_access_denied;

		$module_file = "./modules/" . $module . ".php";
		if (file_exists($module_file)) {
			include($module_file);
		} else {
			trigger_error("Module Error: \"" . $module . "\"", E_USER_ERROR);
		}
	}

	function GetConfig($cfg_var) {
		global $db,$safesql;
		$ret = false;
		$query = $safesql->query("SELECT cfg_val FROM ".DB_PREFIX."cfg WHERE cfg_var='%s' LIMIT 1", array($cfg_var));
		$ret = $db->get_var($query);
		return $ret;
	}

	function PutConfig($cfg_var, $cfg_val) {
		global $db, $safesql;
		$ret = false;
		$query = $safesql->query("UPDATE ".DB_PREFIX."cfg SET cfg_val='%s' WHERE cfg_var='%s'", array($cfg_val, $cfg_var));
		$ret = $db->query($query);
		//$db->debug();

		if ($this->GetConfig($cfg_var) != $cfg_val) {
			$query = $safesql->query("INSERT INTO ".DB_PREFIX."cfg SET cfg_var='%s', cfg_val='%s'", array($cfg_var, $cfg_val));
			$ret = $db->query($query);
			//$db->debug();
		}
		return $ret;
	}

	function GetDefaults() {
		global $langID;

		// IIS CGI/FastCGI fix
		if (!isset($_SERVER["REQUEST_URI"])) $_SERVER["REQUEST_URI"] = $_SERVER["PHP_SELF"] ."?". $_SERVER["QUERY_STRING"];

		if (!isset($langID)) $langID = Language;
		if (!isset($_GET["sub"])) $_GET["sub"] = "";
		if (!isset($_GET["sub_group"])) $_GET["sub_group"] = "-*-NULL-*-";
		if (!isset($_GET["f_group"])) $_GET["f_group"] = "";
		if (!isset($_GET["module"])) $_GET["module"] = "start_redir";
		if (!isset($_GET["time_period"])) $_GET["time_period"] = 0;
		if (!isset($_SESSION["evtlog_user_group"])) $_SESSION["evtlog_user_group"] = "";

		if (!isset($_GET["f_evt_error"])) $_GET["f_evt_error"] = "";
		if (!isset($_GET["f_evt_warning"])) $_GET["f_evt_warning"] = "";
		if (!isset($_GET["f_evt_audit_failure"])) $_GET["f_evt_audit_failure"] = "";
		if (!isset($_GET["f_evt_audit_success"])) $_GET["f_evt_audit_success"] = "";
		if (!isset($_GET["f_evt_information"])) $_GET["f_evt_information"] = "";

		if (!isset($_GET["action"])) $_GET["action"] = "";
		if (!isset($_GET["f_evt_noise"])) $_GET["f_evt_noise"] = "";
		if (!isset($_GET["f_evt_code"])) $_GET["f_evt_code"] = "";
		if (!isset($_GET["f_evt_source"])) $_GET["f_evt_source"] = "";
		if (!isset($_GET["f_evt_category"])) $_GET["f_evt_category"] = "";
		if (!isset($_GET["f_evt_user"])) $_GET["f_evt_user"] = "";
		if (!isset($_GET["evt_computer"])) $_GET["evt_computer"] = "";
		if (!isset($_GET["f_date_from"])) $_GET["f_date_from"] = "";
		if (!isset($_GET["f_date_to"])) $_GET["f_date_to"] = "";
		if (!isset($_GET["order"])) $_GET["order"] = "";
		if (!isset($_GET["sort"])) $_GET["sort"] = "";
		if (!isset($_GET["from"])) $_GET["from"] = "";
		if (!isset($_GET["f_date_to"])) $_GET["f_date_to"] = "";

		if (($_GET["module"] == "showSummary") && (empty($_GET["time_period"]))) {
			$_GET["time_period"] = "0";
			$_SERVER["QUERY_STRING"] = "module=showSummary&time_period=0&sub_group=".$_GET["sub_group"];
		}
	}

	function enable_AutoRefresh() {
		$this->AutoRefresh_enable = true;
	}

	function is_AutoRefresh() {
		return ($this->AutoRefresh_enable);
	}

	function MailQueue($mail_from, $mail_to, $mail_subject, $mail_template, $mail_data, $mail_date="0000-00-00 00:00:00", $mail_priority=50) {
		global $db,$safesql,$smarty;

		if (empty($mail_template)) $mail_template="mail_default";
		$mail_template .= ".tpl";
		$smarty->assign("mail_data", $mail_data);
		$smarty->clear_cache($mail_template); // !!! IMPORTANT !!!
		$mail_html_text = $smarty->fetch($mail_template);
		$mail_subject = $mail_subject;

		$mail_to_array = explode(";", $mail_to);
		if (is_array($mail_to_array)) {
			foreach($mail_to_array as $mail_to_array_item) {
				$query_data = array($mail_date, $mail_priority, $mail_from, trim($mail_to_array_item), $mail_subject, $mail_html_text);
				$query = $safesql->query("INSERT INTO ".DB_PREFIX."queue_mail SET mail_date='%s', mail_priority=%i, mail_from='%s', mail_to='%s', mail_subject='%s', mail_text='%s', error_count=0",$query_data);
				$db->query($query);
				if ($this->debug_on === true) $db->debug();
			}
		}
	}

	function GetRSS($rss_url, $limit = 10, $cache_lifetime = 3600) {
		$ret = array();
		$replace_from = array('&#60;br /&#62;', '&#38;quot;', '&#60;', '&#62;', '&#34;', '&#38;');
		$replace_to = array('<br />', '"', '<', '>', '"', '&');

		include_once "./class_lastRSS.php";
		$rss = new lastRSS;
		// setup transparent cache
		$rss->cache_dir = SMARTY_cache_dir;
		$rss->cache_time = $cache_lifetime;
		$rss->date_format = "d.m.Y";
		if ($limit > 0) $rss->items_limit = $limit;
		// load some RSS file
		if ($rs = $rss->get($rss_url)) {
			if(is_array($rs["items"])) {
				foreach ($rs["items"] as $item) {
					if (is_array($item)) {
						$new_item = array();
						foreach ($item as $key => $value) {
							$new_item[$key] = str_replace($replace_from, $replace_to, $value);
						}
						$ret[] = $new_item;
					}
				}
			}
		}
		return $ret;
	}

	function GetComputers($group = "", $as_submenu = false, $prefix_sufix = "") {
		global $safesql, $db;

		$query_data = array($group, $group);
		if ($as_submenu) {
			$query = $safesql->query("SELECT computer_name as sub, computer_name as title FROM ".DB_PREFIX."computers[ WHERE (computer_group = '%S' OR computer_group LIKE '%~%S~%')] ORDER BY computer_name", $query_data);
			$ret = $db->get_results($query, ARRAY_A);
			if (!empty($ret)) array_unshift($ret, "");
		} else {
			$query = $safesql->query("SELECT computer_name FROM ".DB_PREFIX."computers[ WHERE (computer_group = '%S' OR computer_group LIKE '%~%S~%')] ORDER BY computer_name", $query_data);
			$ret = $db->get_col($query, 0);
			if (!empty($prefix_sufix)) {
				if(is_array($ret)) {
					for ($loop=0; $loop<sizeof($ret); $loop++) {
						$ret[$loop] = $prefix_sufix . $ret[$loop] . $prefix_sufix;
					}
				}
			}
		}
		if (empty($ret)) $ret[] = "";
		//$db->debug();
		return($ret);
	}

	function GetGroups($as_submenu = false, $all_groups = false, $prefix_sufix = "") {
		global $safesql, $db;
		$ret = array();

		// check only active groups

		$query = $safesql->query("SELECT computer_group FROM ".DB_PREFIX."groups_computer ORDER BY computer_group", array());
		$groups = $db->get_col($query, 0);
		if ($as_submenu) if (!empty($groups)) array_unshift($groups, "");

		if(is_array($groups)) {
			foreach($groups as $group) {
				$access_level = $this->CanAccessGroup($group, $_SESSION["evtlog_user_group"]);
				if (($access_level > 0) || ($all_groups === true)) $ret[] = $group;
			}
		}

		if (!empty($prefix_sufix)) {
			if(is_array($ret)) {
				for ($loop=0; $loop<sizeof($ret); $loop++) {
					$ret[$loop] = $prefix_sufix . $ret[$loop] . $prefix_sufix;
				}
			}
		}

		return($ret);
	}

	function GetGroupsUser($as_submenu = false) {
		global $safesql, $db;

		$query = $safesql->query("SELECT user_group FROM ".DB_PREFIX."groups_user ORDER BY user_group", array());
		$ret = $db->get_col($query, 0);
		if ($as_submenu) if (!empty($ret)) array_unshift($ret, "");
		return($ret);
	}

	function CanAccessGroup($groups_computer, $groups_user) {
		global $safesql, $db;
		$ret = 0;
		if (!empty($groups_user)) {
			$query = $safesql->query("SELECT access_level FROM ".DB_PREFIX."groups_access WHERE groups_computer='%s' AND groups_user='%s' LIMIT 1", array($groups_computer, $groups_user));
			$query2 = $safesql->query("SELECT access_level FROM ".DB_PREFIX."groups_access WHERE groups_computer='%s' AND groups_user='%s' LIMIT 1", array("", $groups_user));
			$ret1 = $db->get_var($query);
			$ret2 = $db->get_var($query2);
			if ($ret1 > $ret2) { $ret = $ret1; } else { $ret = $ret2; }
		} else {
			$ret = 1;
		}
		return($ret);
	}

	function CanAccessModule($module, $groups_user) {
		global $safesql, $db;
		$ret = 0;

		switch ($module) {
			case "showSummary":
				$ret = $db->get_var($safesql->query("SELECT access_summary FROM ".DB_PREFIX."groups_user WHERE user_group='%s'", array($groups_user)));
				break;

			case "reports":
				$ret = $db->get_var($safesql->query("SELECT access_summary FROM ".DB_PREFIX."groups_user WHERE user_group='%s'", array($groups_user)));
				break;

			case "showEventLog":
				$ret = $db->get_var($safesql->query("SELECT access_events FROM ".DB_PREFIX."groups_user WHERE user_group='%s'", array($groups_user)));
				break;

			case "showAlerts":
				$ret = $db->get_var($safesql->query("SELECT access_alerts FROM ".DB_PREFIX."groups_user WHERE user_group='%s'", array($groups_user)));
				break;

			case "settings":
				$ret = $db->get_var($safesql->query("SELECT access_settings FROM ".DB_PREFIX."groups_user WHERE user_group='%s'", array($groups_user)));
				break;

			default:
				$ret = 100;
				break;
		}
		return($ret);
	}

	function charts_generate_xml($chart_data, $chart_name, $chart_template) {
		global $smarty, $cacheID;
		if (empty($chart_data)) {
			// no data
			$chart_template = "charts_xml_" . $chart_template . "_nodata.tpl";
		} else {
			$chart_template = "charts_xml_" . $chart_template . ".tpl";
		}
		$smarty->assign("chart_data", $chart_data);
		$smarty->clear_cache($chart_template); // !!! IMPORTANT !!!
		$xml_data = $smarty->fetch($chart_template);

		$xml_path = SMARTY_cache_dir . 'graph_' . $chart_name . '_' . str_replace("|", "^", $cacheID) . '.xml';
		$ret = file_put_contents ($xml_path, $xml_data);
		return($ret);
	}

	function charts_insert_html($param, &$smarty) {
		global $cacheID;
		$error = null;

		if (!isset($param["name"])) $error = "name";
		if (!isset($param["chart"])) $error = "chart";
		if (!isset($param["width"])) $error = "width";
		if (!isset($param["height"])) $error = "height";

		if ($error != null) {
			$ret = '<div style="font-weight: bold; color: red;">Smarty->charts_insert_html</div>';
			$ret .= '<div style="">Unspecified "' . $error . '" param!</div>';
		} else {
			$xml_path = SMARTY_cache_dir . 'graph_' . $param["name"] . '_' . str_replace("|", "^", $cacheID) . '.xml';
			$ret =  "\n" . '<div id="AnyChart_' . $param["name"] . '">##You need to install Adobe Flash player.##</div>';
			$ret .=  "\n" . '<script type="text/javascript">';
			$ret .=  "\n" . 'var so = new SWFObject("./charts_anychart/'.$param["chart"].'.swf?XMLFile=' . $xml_path . '", "AnyChart_swf_' . $param["name"] . '", "'.$param["width"].'", "'.$param["height"].'", "9", "#ffffff");';
			//$ret .=  "\n" . 'so.useExpressInstall("expressinstall.swf");';
			//$ret .=  "\n" . 'so.addParam("wmode", "opaque");';
			$ret .=  "\n" . 'so.write("AnyChart_' . $param["name"] . '");';
			$ret .=  "\n" . '</script>' . "\n";
		}
		return($ret);
	}

	function smarty_block_dynamic($param, $content, &$smarty) {
    	return $content;
	}


	function auto_home_url() {
		$host = "http".($_SERVER["HTTPS"] == "on" ? "s" : "")."://".
		// www.server.cz(:port)
		$_SERVER["HTTP_HOST"].($_SERVER["SERVER_PORT"] != 80 ? ":".$_SERVER["SERVER_PORT"] : "");
		// go to root?
		$script_dir = ($relurl{0} != "/" ? dirname($_SERVER["SCRIPT_NAME"])."/" : "").dirname($relurl);
		// $arr_dir now holds every dir name down to script
		$arr_dir = explode("/", $script_dir);
		// put canonicalized path parts here
		$arr_realpath = array();
		// now go throught the dirs
		for ($i = $j = 0; $i < count($arr_dir); $i++)
			// if we have empty dir or . (i.e. from exploding // or /./) ignore it
			if ($arr_dir[$i] != "" && $arr_dir[$i] != ".") {
				// if we have ".." roll-back, but not beyond root
				if ($arr_dir[$i] == "..")
					$j = ($j > 0 ? $j - 1 : 0);
				// store part
				else
					$arr_realpath[$j++] = $arr_dir[$i];
			}
		// add filename
		$arr_realpath[$j] = basename($relurl);
		// remove rolled-back parts
		array_splice($arr_realpath, $j + 1);
		return("$host/".implode("/", $arr_realpath));
	}

	function Debug($msg) {
		if ($this->debug_on === true) echo $msg . "<br />\n";
	}

	function DebugArray($array) {
		if ($this->debug_on === true){
			$export = var_export($array,true);
			echo "<pre>" . $export . "</pre>";
		}
	}
}
?>