<?php
// SBEventLogMonitor 2006 CRON - ParseData
// (c) Ladislav Soukup [root@soundboss.cz]
$time["start"] = microtime();
set_time_limit(0);
define("APP_TOKEN", "SB_ELM");
include_once "./config.php";
include_once "./class_smarty/Smarty.class.php";
$smarty = new Smarty();
$smarty->template_dir = SMARTY_template_dir;
$smarty->compile_dir = SMARTY_cache_dir;
$smarty->cache_dir = SMARTY_cache_dir;
$smarty->compile_check = true;
$smarty->caching = false;
include_once "./class_core.php";
$core = new sb_core;

header('Content-type: text/html; charset='.PageCharSet, true);
$langID = Language;
$core->GetDefaults();
$cacheID = $core->GetCacheID($langID);
$smarty->load_filter('output','i18n');
$core->LoadTranslationTable($langID);

// SQL CLASS
include_once "./class_ezsql.php";
$db->hide_errors();
$db->query("SELECT id FROM ".DB_PREFIX."computers LIMIT 1");
if ($db->last_error != null){
	$smarty->display("_error_mysql.tpl");
	die();
}
if (defined('EZSQL_DB_CHARSET')) $core->ezsql_set_charset(EZSQL_DB_CHARSET);
include_once "./class_safesql.php";
$safesql = new SafeSQL_MySQL;
// END - SQL CLASS
define("NOW_DT", date("Y-m-d H:i:s"));
define("NOW", time());

// CRON - start
echo "Cron started... " . date("Y-m-d H:i:s") . "<br />\n";
$smarty->assign("version", $core->version);

$query = "SELECT * FROM ".DB_PREFIX."cron WHERE cron_enable='1' AND cron_time<'".NOW_DT."' ORDER BY cron_priority ASC";
$cron_arr = $db->get_results($query,ARRAY_A);
if (is_array($cron_arr)) {
	foreach($cron_arr as $cron){
		// SET NEXT RUN
		$new_time = date("Y-m-d H:i:s",NOW+$cron["cron_repeat"]-15);
		$query = "UPDATE ".DB_PREFIX."cron SET cron_time='".$new_time."' WHERE id='".$cron["id"]."'";
		$db->query($query);
		// FIND CRON MODULE
		$cron_mod = "./modules_cron/".$cron["cron_module"];
		if (file_exists($cron_mod)){
			echo "Running CRON module: ".$cron["cron_module"]."<br />\n";
			include $cron_mod;
		} else {
			$err_msg = "CRON MODULE NOT FOUND: ".$cron_mod;
			echo $err_msg."<br />\n";
			trigger_error($err_msg, E_USER_NOTICE);
		}
		flush();
	}
}

// CRON STOPs HERE
echo "Cron ended... " . date("Y-m-d H:i:s") . "<br />\n";
?>