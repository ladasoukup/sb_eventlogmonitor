<?php
// SBEventLogMonitor 2006 RPC server
// (c) Ladislav Soukup [root@soundboss.cz]
$time["start"] = microtime();
set_time_limit(0);
define("APP_TOKEN", "SB_ELM");
include_once "./config.php";
include_once "./class_smarty/Smarty.class.php";
$smarty = new Smarty();
$smarty->template_dir = SMARTY_template_dir;
$smarty->compile_dir = SMARTY_cache_dir;
$smarty->cache_dir = SMARTY_cache_dir;
$smarty->compile_check = true;
$smarty->caching = false;
include_once "./class_core.php";
$core = new sb_core;
if ($core->debug_on) {
	define("ERROR_LOG_FILE", "./runlogs/".date("Ymd")."_RPC-error.log");
	error_reporting(E_ALL);
	ini_set("log_errors", true);
	ini_set("error_log", ERROR_LOG_FILE);
}

header('Content-type: text/html; charset='.PageCharSet, true);
$langID = Language;
$core->GetDefaults();
$cacheID = $core->GetCacheID($langID);
$smarty->load_filter('output','i18n');
$core->LoadTranslationTable($langID);

// SQL CLASS
include_once "./class_ezsql.php";
$db->hide_errors();
$db->query("SELECT id FROM ".DB_PREFIX."computers LIMIT 1");
if ($db->last_error != null){
	$smarty->display("_error_mysql.tpl");
	die();
}
if (defined('EZSQL_DB_CHARSET')) $core->ezsql_set_charset(EZSQL_DB_CHARSET);
include_once "./class_safesql.php";
$safesql = new SafeSQL_MySQL;
// END - SQL CLASS
define("NOW_DT", date("Y-m-d H:i:s"));
define("NOW", time());

// FUNCTIONs
function EvtTypeConvert($EvtType) {
	$ret = strtolower($EvtType);
	if ($ret == "1") $ret = "error";
	if ($ret == "2") $ret = "warning";
	if ($ret == "3") $ret = "information";
	if ($ret == "4") $ret = "audit_success";
	if ($ret == "5") $ret = "audit_failure";
	
	return $ret;
}

function ClientAddOrModify($remote_ip, $curr_computer="handshake", $agent_version="N/A") {
	global $safesql, $db;
	
	$query = $safesql->query("SELECT * FROM ".DB_PREFIX."secrets WHERE ip_addr='%s' LIMIT 1", array($remote_ip));
	$secret_data = $db->get_row($query, ARRAY_A);
	if ($db->num_rows > 0) {
		$query = $safesql->query("UPDATE ".DB_PREFIX."secrets SET hostname='%s', agent_version='%s' WHERE ip_addr='%s'", array($curr_computer, $agent_version, $remote_ip));
		$db->query($query);
	} else {
		$expire = date("Y-m-d H:i:s", time() + 604800);
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."secrets SET ip_addr='%s', hostname='%s', agent_version='%s', secret_expire='%s'", array($remote_ip, $curr_computer, $agent_version, $expire));
		$db->query($query);
	}
}

// FUNCTIONs end

// RPC start
$POSTED_DATA = file_get_contents('php://input');
if(!isset($_GET["secretrequest"])) $_GET["secretrequest"] = "";
if(!isset($_GET["secret"])) $_GET["secret"] = "";
if(!isset($_SERVER["REMOTE_ADDR"])) $_SERVER["REMOTE_ADDR"] = "0.0.0.0";
$remote_ip = $_SERVER["REMOTE_ADDR"];

$debug_info = "\r\n------------------------------------------------------------------------\r\n";
$debug_info .= "Date and time: " . date("Y-m-d H:i:s") . " (" . time() . ")\r\n";
$debug_info .= "Client: " . $remote_ip . "\r\n";

$query = $safesql->query("DELETE FROM ".DB_PREFIX."secrets WHERE secret_expire<'%s'", array(date("Y-m-d H:i:s")));
$db->query($query);
// Is this a "secret regenerate" request?
if ($_GET["secretrequest"] == 1) {
	ClientAddOrModify($remote_ip);
	$debug_info .= "Handshake...";
	if ($core->debug_on) file_put_contents("./runlogs/".date("Ymd")."_RPC.txt", $debug_info, FILE_APPEND);
	echo "kGbUicYe\r\n";
	die();
}

// PARSE POSTED DATA...
echo "OK\r\n";
include_once "./modules_cron/parse_data_filters.php";
$clear_alerts = false;
$clear_events = false;
$last_event_time = 0;
$xml_data = $core->object2array(simplexml_load_string($POSTED_DATA));
if(is_array($xml_data)) {
	$curr_computer = strtolower($xml_data["computer_name"]);
	$agent_version = strtolower($xml_data["agent_version"]);
	ClientAddOrModify($remote_ip, $curr_computer, $agent_version);
	$debug_info .= "Computer name: " . $curr_computer . "\r\n";
	$debug_info .= "Agent version: " . $agent_version . "\r\n";
	$debug_info .= "\r\nRPC data: " . var_export($xml_data, true) . "\r\n\r\n";
	
	// Is computer in "Monitored computers" list???
	$query_data = array($curr_computer);
	$query = $safesql->query("SELECT id FROM ".DB_PREFIX."computers WHERE computer_name = '%s'", $query_data);
	$curr_computer_id = $db->get_var($query);
	if (empty($curr_computer_id)) {
		$query_data = array($curr_computer, date("Y-m-d H:i:s", time() + 60));
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."computers SET computer_name='%s', computer_group='', computer_os='', computer_harvest_interval='3600', computer_enable=0, computer_next_harvest='%s'", $query_data);
		$smarty->clear_cache(null, "showEventLog");
	} else {
		$query_data = array($curr_computer, date("Y-m-d H:i:s", time() + 60), $curr_computer_id);
		$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_name='%s', computer_enable=0, computer_next_harvest='%s' WHERE id=%i", $query_data);
	}
	$db->query($query);
	
	// update Computer's DAT file...
	$f = fopen("./data/" . $curr_computer . ".dat", "w");
	fputs($f, date("YmdHis") . ".000000+000");
	fclose($f);
	
	// Computer OS
	$curr_computer_os = $xml_data["computer_os"];
	$curr_computer_os = str_replace("Microsoft ", "", $curr_computer_os);
	$query_data = array($curr_computer_os, $curr_computer);
	$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_os = '%s' WHERE computer_name = '%s'", $query_data);
	$db->query($query);
	
	// computer description
	$curr_computer_description = $xml_data["computer_description"];
	$query_data = array($curr_computer_description, $curr_computer);
	$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_description = '%s' WHERE computer_name = '%s'", $query_data);
	$db->query($query);
	
	// computer domain
	$curr_computer_domain = strtolower($xml_data["computer_domain"]);
	$query_data = array($curr_computer_domain, $curr_computer);
	$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_domain = '%s' WHERE computer_name = '%s'", $query_data);
	$db->query($query);
	
	// SINGLE EVENT fix
	if (!empty($xml_data["event"]["time-generated"])) {
		$core->Debug(" * SINGLE EVENT - Fixing array");
		$temp = $xml_data["event"];
		unset($xml_data["event"]);
		$xml_data["event"][0] = $temp;
	}

	foreach($xml_data["event"] as $xml_event) {
		// $core->DebugArray($xml_event);
		$sql_data_A = array(
			"evt_computer" => $curr_computer,
			"evt_code" => $xml_event["code"],
			"evt_type" => EvtTypeConvert(iconv("UTF-8", PageCharSet, $xml_event["type"])),
			"evt_category" => iconv("UTF-8", PageCharSet, $xml_event["category"]),
			"evt_logfile" => iconv("UTF-8", PageCharSet, $xml_event["logfile"]),
			"evt_message" => iconv("UTF-8", PageCharSet, $xml_event["message"]),
			"evt_source" => iconv("UTF-8", PageCharSet, $xml_event["source"]),
			"evt_user" => iconv("UTF-8", PageCharSet, $xml_event["user"]),
			"evt_time_generated" => date("Y-m-d H:i:s", $xml_event["time-generated"])
		);
		if ($xml_event["time-generated"] > $last_event_time) $last_event_time = $xml_event["time-generated"];
		
		$is_noise = false; $is_alert = false; $is_email = false;
		evtFilter_check($sql_data_A, $is_noise, $is_alert, $is_email);
		$core->Debug("evtFilter_check returns... N-".$is_noise." A-".$is_alert." E-".$is_email);
		$sql_data = array(	$sql_data_A["evt_computer"],
							$sql_data_A["evt_code"],
							$sql_data_A["evt_type"],
							$sql_data_A["evt_category"],
							$sql_data_A["evt_logfile"],
							$sql_data_A["evt_message"],
							$sql_data_A["evt_source"],
							$sql_data_A["evt_user"],
							$sql_data_A["evt_time_generated"],
							$is_noise
					);
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."events SET evt_computer='%s', evt_code=%i, evt_type='%s', evt_category='%s', evt_logfile='%s', evt_message='%s', evt_source='%s', evt_user='%s', evt_time_generated='%s', evt_noise=%i", $sql_data);
		$db->query($query);
		$clear_events = true;
		if ($is_alert) {
			$query = $safesql->query("INSERT INTO ".DB_PREFIX."alerts SET evt_computer='%s', evt_code=%i, evt_type='%s', evt_category='%s', evt_logfile='%s', evt_message='%s', evt_source='%s', evt_user='%s', evt_time_generated='%s', evt_noise=%i", $sql_data);
			$db->query($query);
			$clear_alerts = true;
		}
	}
	// SAVE dat file!
	$f = fopen("./data/" . $curr_computer . ".dat", "w");
	fputs($f, date("YmdHis", $last_event_time) . "000000+000");
	fclose($f);
}
// RPC stop
if ($clear_events) $smarty->clear_cache(null, "showEventLog|" . $curr_computer);
if ($clear_events) $smarty->clear_cache(null, "showEventLog|-NULL-");
if ($clear_alerts) $smarty->clear_cache(null, "showAlerts");
if ($clear_alerts) $smarty->clear_cache(null, "showSummary");
if ($clear_alerts) $smarty->clear_cache(null, "rss");

// DEBUG
if ($core->debug_on) file_put_contents("./runlogs/".date("Ymd")."_RPC.txt", $debug_info, FILE_APPEND);
// DEBUG -end
?>