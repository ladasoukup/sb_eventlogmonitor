<?php
// SBEventLogMonitor 2007 SQL test
// (c) Ladislav Soukup [root@soundboss.cz]
set_time_limit(0);
define("APP_TOKEN", "SB_ELM");
include_once "./config.php";
include_once "./class_smarty/Smarty.class.php";
$smarty = new Smarty();
$smarty->template_dir = SMARTY_template_dir;
$smarty->compile_dir = SMARTY_cache_dir;
$smarty->cache_dir = SMARTY_cache_dir;
$smarty->compile_check = true;
$smarty->caching = false;
include_once "./class_core.php";
$core = new sb_core;
$core->GetDefaults();

header('Content-type: text/html; charset='.PageCharSet, true);
// SQL CLASS
include_once "./class_ezsql.php";
$db->hide_errors();
$db->query("SELECT id FROM ".DB_PREFIX."computers LIMIT 1");
if ($db->last_error != null){
	$smarty->display("_error_mysql.tpl");
	die();
}
if (defined('EZSQL_DB_CHARSET')) $core->ezsql_set_charset(EZSQL_DB_CHARSET);
include_once "./class_safesql.php";
$safesql = new SafeSQL_MySQL;
// END - SQL CLASS
define("NOW_DT", date("Y-m-d H:i:s"));
define("NOW", time());

//set default group
if ($_GET["sub_group"] == "-*-NULL-*-") {
	$all_sub_groups = $core->GetGroups(true);
	if (!isset($all_sub_groups[0])) $all_sub_groups[0] = "";
	$_GET["sub_group"] = $all_sub_groups[0];
	$_SERVER["QUERY_STRING"] = "module=".$_GET["module"]."&time_period=".$_GET["time_period"]."&sub_group=".$_GET["sub_group"];
}

function DoQueryTest($query) {
	global $db, $delim, $module, $section;
	
	echo $module. $delim . $section . $delim;
	
	$start = microtime();
	$ret = $db->get_results($query);
	$end = microtime();
	echo ($end - $start) . $delim;
	$explain = $db->get_row("EXPLAIN " . $query, ARRAY_A);
	if (isset($explain["table"])) {
		echo $explain["table"] . $delim . $explain["type"] . $delim . $explain["possible_keys"] . $delim;
		echo $explain["key"] . $delim . $explain["key_len"] . $delim . $explain["ref"] . $delim;
		echo $explain["rows"] . $delim . $explain["Extra"] . $delim;
	} else {
		echo $delim . $delim . $delim . $delim . $delim . $delim . $delim . $delim;
	}
	echo $query . $delim;
	echo "<br />\n";
}

// TEST start here...
$delim = "\t";
$module = "";
$section = "";

$evt_types = array("error", "warning", "information", "audit_success", "audit_failure");
$date_today = date("Y-m-d"); $date_yesterday = date("Y-m-d", time()-86400); $date_2days = date("Y-m-d", time()-172800);
if (!empty($_GET["sub_group"])) {
	$computers_in_group = $this->GetComputers($_GET["sub_group"], false);
} else {
	$computers_in_group = "";
}
$sumary_time_period_start = date("Y-m-d H:i:s", time() - 43200);
$sumary_time_period_end = date("Y-m-d H:i:s");

$module = "Summary page";
$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."computers[ WHERE computer_group='%S']", array($_GET["sub_group"]));
DoQueryTest($query);

$section = "Events Summary";
foreach ($evt_types as $db_name) {
	$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query2 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_today." 00:00:00' AND evt_time_generated < '".$date_today." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query3 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_yesterday." 00:00:00' AND evt_time_generated < '".$date_yesterday." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query4 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."events WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_time_generated < '".$date_2days." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	DoQueryTest($query);
	DoQueryTest($query2);
	DoQueryTest($query3);
	DoQueryTest($query4);
}

$section = "Alerts Summary";
foreach ($evt_types as $db_name) {
	$query = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query2 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_today." 00:00:00' AND evt_time_generated < '".$date_today." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query3 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_yesterday." 00:00:00' AND evt_time_generated < '".$date_yesterday." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	$query4 = $safesql->query("SELECT count(id) FROM ".DB_PREFIX."alerts WHERE evt_type='".$db_name."' AND evt_time_generated > '".$date_2days." 00:00:00' AND evt_time_generated < '".$date_2days." 23:59:59' AND evt_noise = 0[ AND evt_computer in (%Q)]", array($computers_in_group));
	DoQueryTest($query);
	DoQueryTest($query2);
	DoQueryTest($query3);
	DoQueryTest($query4);
}

$section = "Top Error computers";
foreach($evt_types as $summary_type) {
	$query_data = array($summary_type, $sumary_time_period_start, $sumary_time_period_end, $computers_in_group);
	$query = $safesql->query("SELECT evt_computer, count(id) as evt_count  FROM ".DB_PREFIX."events WHERE evt_type='%s' AND evt_noise = 0[ AND evt_time_generated>'%S'][ AND evt_time_generated<'%S'][ AND evt_computer in (%Q)] group by evt_computer ORDER BY evt_count DESC LIMIT 7", $query_data);
	DoQueryTest($query);
}

$section = "Last alerts";
$query = $safesql->query("SELECT DISTINCT evt_computer FROM ".DB_PREFIX."alerts WHERE evt_time_generated>'%s'[ AND evt_computer in (%Q)] ORDER BY evt_time_generated DESC LIMIT 10", array(date("Y-m-d 00:00:00", time() - 172800), $computers_in_group));
DoQueryTest($query);
$last_alerts_computers = $db->get_col($query, 0);
$last_alerts = "";
if (is_array($last_alerts_computers)) {
	foreach($last_alerts_computers as $evt_computer) {
		$query = $safesql->query("SELECT * FROM ".DB_PREFIX."alerts WHERE evt_computer='%s' AND evt_noise = 0 ORDER BY evt_time_generated DESC LIMIT 1", array($evt_computer));
		DoQueryTest($query);
	}
}



$module = "Events";
$section = "";

DoQueryTest("SELECT id FROM sbEvtLog_computers LIMIT 1");
$section = "Access level";
DoQueryTest("SELECT access_events FROM sbEvtLog_groups_user WHERE user_group='ADMIN'");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='SERVER' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='' AND groups_user='ADMIN' LIMIT 1");
$section = "";
DoQueryTest("SELECT computer_name as sub, computer_name as title FROM sbEvtLog_computers WHERE (computer_group = 'SERVER' OR computer_group LIKE '%~SERVER~%') ORDER BY computer_name");
DoQueryTest("SELECT computer_group FROM sbEvtLog_groups_computer ORDER BY computer_group");
$section = "Access Level";
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='SERVER' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='TEST' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='VISTA' AND groups_user='ADMIN' LIMIT 1");
DoQueryTest("SELECT access_level FROM sbEvtLog_groups_access WHERE groups_computer='' AND groups_user='ADMIN' LIMIT 1");
$section = "";
DoQueryTest("SELECT computer_name FROM sbEvtLog_computers WHERE (computer_group = 'SERVER' OR computer_group LIKE '%~SERVER~%') ORDER BY computer_name");
DoQueryTest("SELECT id, evt_computer, evt_code, evt_type, evt_category, evt_logfile, evt_source, evt_user, evt_time_generated, evt_noise FROM sbEvtLog_events WHERE 1=1 AND evt_computer in ('devil','server') AND evt_type in ('error','','','audit_success','information') AND evt_source LIKE '%%.NET Runtime 2.0 Error Reporting%%' AND evt_time_generated > '2007-03-15 00:00:00' AND evt_noise in ('0') ORDER BY evt_time_generated DESC LIMIT 50");
$section = "Auto complete";
DoQueryTest("SELECT DISTINCT evt_source FROM sbEvtLog_events LIMIT 200");
DoQueryTest("SELECT DISTINCT evt_user FROM sbEvtLog_events LIMIT 200");
DoQueryTest("SELECT DISTINCT evt_category FROM sbEvtLog_events LIMIT 200");
DoQueryTest("SELECT DISTINCT evt_code FROM sbEvtLog_events LIMIT 200");
$section = "";
?>