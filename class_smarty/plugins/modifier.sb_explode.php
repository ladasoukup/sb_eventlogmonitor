<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty replace modifier plugin
 *
 * Type:     modifier<br>
 * Name:     explode<br>
 * Purpose:  simple PHP explode
 * @param string
 * @param string
 * @return array
 */
function smarty_modifier_sb_explode($string, $search)
{
	return explode($search, $string);
}

/* vim: set expandtab: */

?>
