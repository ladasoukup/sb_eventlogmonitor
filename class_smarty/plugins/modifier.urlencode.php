<?php
/**
 * Smarty plugin
 */


/**
 * Smarty urlencode modifier plugin
 *
 * Type:     modifier<br>
 * Name:     urlencode<br>
 * Purpose:  do urlencode
 * @param string
 * @return string
 */
function smarty_modifier_urlencode($string)
{
    return urlencode($string);
}

/* vim: set expandtab: */

?> 