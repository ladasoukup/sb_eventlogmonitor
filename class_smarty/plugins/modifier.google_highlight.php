<?php
/*
* Smarty plugin
*
* Type: modifier
* Name: google_highlight
* Version: 1.1
* Date: 2003-05-06
* Author: Tom Anderson toma@etree.org
* Purpose: Highlight search terms in text identical to google
* Notes: This function uses a static associative array so
* multiple calls to the function with the same terms do not
* force the search string to be re-parsed. It is likely that
* this function will be called within a loop of some sort, so
* this method will save more resources than it takes.
*
* Example smarty code:
*
{assign var=s value="This is a string I want to search through"}
{assign var=t value="This \"to search\" through"}
These are the terms searched: {$t|google_highlight:$t|replace:'"':''}
<br />
Search Results: {$s|google_highlight:$t}
*
*
*/
function smarty_modifier_google_highlight ($text, $search)
{
// Init vars
static $_search_phrases;
$i = 0;
$colors = array('#ffff00','#00ffff','#99ff99','#ff9999','#ff66ff','#880000', '#00aa00', '#886800', '#004699', '#990099');
// Load saved terms if they have already been parsed.
if (!$_terms = (array)$_search_phrases[$search]) {
	// pull out quoted strings
	preg_match_all( '/"(.*?)"/', $search, $_quotes);
	// split on whitespace
	$_terms = array_merge((array)$_quotes[1], explode(' ', preg_replace( '/".*?"/', ' ', $search )));
	$_search_phrases[$search] = $_terms;
}
// Loop through each term and highlight
foreach (array_unique($_terms) as $val) {
	// Strip any stray non-matched double quotes and check for empty string
	if (!$val = trim(str_replace('"', '', $val))) continue;
	if ($i == 10) $i = 0;
	$font_color = ($i > 4) ? 'white': 'black';
	$style = '<b style="color:' . $font_color . ';background-color:' . $colors[$i++] . '">';
	// AUTHOR NOTE: this is buggy because if you search for 'style' after at least one replace,
	// the str_replace will find it inside the <b tag. What's needed to fix this: a regex
	// to be used in a preg_replace call that will not replace anything between <b style...
	// $text = str_replace($val, "$style$val</b>", $text);
	$text = preg_replace("/($val)/i", $style . '$1' . '</b>', $text);
}
return $text;
}
?>