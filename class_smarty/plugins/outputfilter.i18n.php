<?php
	function smarty_outputfilter_i18n($tpl_source, &$smarty) {
		if (!is_array($GLOBALS['_i18n_TEXTS_'])) {
			// NO TRANSLATION LOADED!!!
		}
		
		// Now replace the matched language strings with the entry in the file
		return preg_replace_callback('/##(.+?)##/', '_compile_lang', $tpl_source);
	}

	/**
	 * _compile_lang
	 * Called by smarty_outputfilter_i18n function it processes every language
	 * identifier, and inserts the language string in its place.
	 *
	 */
	function _compile_lang($key) {
		$return = $key[1];
		$translation = trim($GLOBALS['_i18n_TEXTS_'][$key[1]]);
		if (!empty($translation)) $return = $translation;
		return $return;
	}
?>