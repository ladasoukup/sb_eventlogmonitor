{include file="_wnd_start.tpl"}

<div id="menu_bar" style="background-color: #dedede; padding: 5px;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 0px none;">
<tr><td>
<a href="index.php?module=help" class="button_txt">##Home##</a>
{if $smarty.get.hlp_sub != ""}
<a href="index.php?module=help&amp;hlp_main={$smarty.get.hlp_main}" class="button_txt">##Up one level##</a>
{/if}
<a href="#" class="button_txt" onclick="self.print(); return false;">##Print##</a>
<a href="#" class="button_txt" onclick="self.close(); return false;">##Close window##</a>
</td><td align="right">
##GUI language##:
<select id="LangSelect" name="LangSelect" onChange="GuiSwitchLang();">
{foreach from=$_i18n_LangTable_ key=key item=item}
	<option value="{$key}"{if $key == $langID} SELECTED{/if}>{$item}</option>
{/foreach}
</select>
</td></tr>
</table>
</div>

<div id="menu_text" style="background-color: #fff; padding: 5px;">
{include file=$help_template}
</div><br />

<div id="menu_bar_footer" style="background-color: #dedede; padding: 5px;">
<a href="index.php?module=help" class="button_txt">##Home##</a>
{if $smarty.get.hlp_sub != ""}
<a href="index.php?module=help&amp;hlp_main={$smarty.get.hlp_main}" class="button_txt">##Up one level##</a>
{/if}
<a href="#" class="button_txt" onclick="self.print(); return false;">##Print##</a>
<a href="#" class="button_txt" onclick="self.close(); return false;">##Close window##</a>
</div>

{include file="_wnd_end.tpl"}
