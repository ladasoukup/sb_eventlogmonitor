{include file="_page_start.tpl"}

<div style="font-size: 1px; height: 20px;">&nbsp;</div>

{if $import == "ok"}
{assign var="js_progressbar_title" value="##Rules has been imported##..."}
{else}
{assign var="js_progressbar_title" value="##Error importing rules##..."}
{/if}

{assign var="js_progressbar_url" value=$smarty.server.REQUEST_URI|add_url_param:'sub':'filters'}
{assign var="js_progressbar_url_title" value="##back to overview##"}
{assign var="js_progressbar_timer" value="5"}

{if $import == "ok"}
{assign var="js_progreesbar_color" value="green"}
{else}
{assign var="js_progreesbar_color" value="red"}
{/if}

{include file="js_progressbar.tpl"}

{include file="_page_end.tpl"}
