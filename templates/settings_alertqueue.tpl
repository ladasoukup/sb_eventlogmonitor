{include file="_page_start.tpl"}
<h2>##Alert queue##</h2>
<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Filter##</td>
<td class="td_header">##Computer##</td>
<td class="td_header">##Sender##</td>
<td class="td_header">##Recipient##</td>
<td class="td_header">##Subject##</td>
<td class="td_header">##Send after##</td>
<td class="td_header">##Count##</td>
<td class="td_header">##Email body##</td>
</tr>
{section name=loop loop=$alert_queue}
<tr class="tr_data">
<td class="td_data_v">{$alert_queue[loop].filter_title}</td>
<td class="td_data_v">{$alert_queue[loop].evt_computer}</td>
<td class="td_data_v">{$alert_queue[loop].queue_mail_from}</td>
<td class="td_data_v">{$alert_queue[loop].queue_mailto}</td>
<td class="td_data_v">{$alert_queue[loop].queue_mail_title}</td>
<td class="td_data_v">{$alert_queue[loop].queue_wait_to}</td>
<td class="td_data_v">{$alert_queue[loop].queue_email_count}</td>
<td class="td_data_v">
<a href="#NEED-JavaScript" class="button_txt" onClick="OpenWindow('index.php?module=settings&amp;sub=alertqueue_detail&amp;id={$alert_queue[loop].id}', 'alertsDetail', 790, 580); return false;">...</a>
</td>
</tr>
{/section}
</table>
{include file="_page_end.tpl"}