<?xml version="1.0" encoding="{$smarty.const.PageCharSet}"?>
<?xml-stylesheet type="text/xsl" href="rss.xsl" ?>
<rss version="2.0">
  <channel>
    <title>{$app_name}</title>
    <link>{$rss_server_url}</link>
	<description>##Last alerts##</description>
    <lastBuildDate>{$rss_date|date_format:"%a, %d %b %Y %H:%M:%S"}</lastBuildDate>
    <ttl>360</ttl>
	{section name=loop loop=$last_alerts}
	<item>
		<title>{$last_alerts[loop].evt_computer} - {$last_alerts[loop].evt_source}</title>
		<description>{$last_alerts[loop].evt_message}</description>
		<link>{$rss_server_url}index.php?module=showAlertsDetailrss&amp;id={$last_alerts[loop].id}</link>
		<pubDate>{$last_alerts[loop].evt_time_generated|date_format:"%a, %d %b %Y %H:%M:%S"}</pubDate>
	</item>
	{/section}

  </channel>
</rss>