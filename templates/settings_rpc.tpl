{include file="_page_start.tpl"}
<h2>##RPC secrets##</h2>
<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##IP address##</td>
<td class="td_header">##Computer##</td>
<td class="td_header">##Agent version##</td>
<td class="td_header">##Expire##</td>
</tr>
{section name=loop loop=$secrets}
<tr class="tr_data">
<td class="td_data_v">{$secrets[loop].ip_addr}</td>
<td class="td_data_v">{$secrets[loop].hostname}</td>
<td class="td_data_v">{$secrets[loop].agent_version|default:'N/A'}</td>
<td class="td_data_v">{$secrets[loop].secret_expire}</td>
</tr>
{/section}
</table>
{include file="_page_end.tpl"}