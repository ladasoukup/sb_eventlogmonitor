{include file="_page_start.tpl"}
<script language="JavaScript" type="text/javascript">
	PageUnloadWarningText = "##There are unsaved changes. If You will leave this page, all changes will be lost.##";
</script>
<h2>##Alert filters##</h2>
<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Export##</td>
<td class="td_header">##Filter title##</td>
<td class="td_header">##Noise## / ##Alert##</td>
<td class="td_header">##Email## [*]</td>
<td class="td_header">##Once per##</td>
<td class="td_header">##Enable##</td>
<td class="td_header">##Priority##</td>
<td class="td_header">##Rule##</td>
<td class="td_header">##Delete##</td>
</tr>
{section name=loop loop=$filters}
<tr class="tr_data" id="tab_row_{$smarty.section.loop.index}">

<td class="td_data_v">
<a href="{$smarty.server.REQUEST_URI|add_url_param:'sub':'filters_export'|add_url_param:'id':$filters[loop].id}" class="button_txt">##XML##</a>
</td>

<td class="td_data_v">
<input type="hidden" name="id[{$smarty.section.loop.index}]" value="{$filters[loop].id}" />
<input type="text" name="filter_title[{$smarty.section.loop.index}]" value="{$filters[loop].filter_title}" size="30" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<input type="radio" name="filter_is[{$smarty.section.loop.index}]" value="noise"{if $filters[loop].filter_isNoise == "1"} CHECKED{/if} onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />##Noise##
<input type="radio" name="filter_is[{$smarty.section.loop.index}]" value="alert"{if $filters[loop].filter_isAlert == "1"} CHECKED{/if} onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />##Alert##
</td>

<td class="td_data_v">
<input type="text" name="filter_mailto[{$smarty.section.loop.index}]" value="{$filters[loop].filter_mailto}" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
<input type="checkbox" name="filter_isEmail[{$smarty.section.loop.index}]" value="YES"{if $filters[loop].filter_isEmail} checked{/if} onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<input type="text" size="3" name="filter_email_ignore[{$smarty.section.loop.index}]" value="{$filters[loop].filter_email_ignore}" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" /> ##min.##
</td>

<td class="td_data_v">
<select name="filter_active[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option value="0"{if $filters[loop].filter_active == "0"} SELECTED{/if}>##NO##</option>
	<option value="1"{if $filters[loop].filter_active == "1"} SELECTED{/if}>##YES##</option>
</select>
</td>

<td class="td_data_v" align="center">
<select name="filter_priority[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
{section name=loop2 loop=$priority}
	<option value="{$priority[loop2]}"{if $filters[loop].filter_priority == $priority[loop2]} SELECTED{/if}>{$priority[loop2]}</option>
{/section}
</select>
</td>

<td class="td_data_v" align="center">
<a href="#NEED-JavaScript" class="button_txt" onClick="OpenWindow('index.php?module=settings&amp;sub=filters_detail&amp;id={$filters[loop].id}', 'filterDetail', 790, 580); return false;">...</a>
</td>

<td class="td_data_v" align="center">
<input type="checkbox" name="filter_delete[{$smarty.section.loop.index}]" value="YES" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

</tr>
{/section}

<tr><td class="td_data_v" colspan="9">&nbsp;</td></tr>
<!--NEW-->

<tr class="tr_data" style="background: #FFDC7C;" id="tab_row_new">

<td class="td_data_v">&nbsp;</td>

<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="new_id" value="-NEW-" />
<input type="text" name="new_filter_title" value="" onChange="SettingsHighlight('tab_row_new', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<input type="radio" name="new_filter_is" value="noise" />##Noise##
<input type="radio" name="new_filter_is" value="alert" CHECKED />##Alert##
</td>

<td class="td_data_v">
<input type="text" name="new_filter_mailto" value="" />
<input type="checkbox" name="new_filter_isEmail" value="YES" />
</td>

<td class="td_data_v">
<input type="text" size="3" name="new_filter_email_ignore" value="0" /> ##min.##
</td>

<td class="td_data_v">&nbsp;</td>
<td class="td_data_v" align="center">&nbsp;</td>
<td class="td_data_v" align="center">&nbsp;</td>
<td class="td_data_v" align="center">&nbsp;</td>

</tr>

<!--NEW-->

<tr><td colspan="9" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" id="btn_submit" class="button_txt" name="OK" value="##Save changes##" onClick="WarningBeforePageLeave = false;" />
</td></tr>

</table>
</form>

<h2>##Import## / ##Export##</h2>
<form action="{$smarty.server.REQUEST_URI|add_url_param:'sub':'filters_import'}" method="post" enctype="multipart/form-data">
<input type="file" name="filter_import" accept="text/xml" />&nbsp;&nbsp;
<input type="submit" class="button_txt" name="OK" value="##Import rules##" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" class="button_txt" name="btn" value="##Backup all filters##" OnClick="window.location.href='{$smarty.server.REQUEST_URI|add_url_param:'sub':'filters_export'}'" />
</form>
{include file="_page_end.tpl"}