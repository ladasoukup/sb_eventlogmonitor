{include file="_page_start.tpl"}
<script language="JavaScript" type="text/javascript">
	PageUnloadWarningText = "##There are unsaved changes. If You will leave this page, all changes will be lost.##";
</script>
<h2>##Users &amp; Passwords##</h2>

<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header">
<td class="td_header">##Username##</td>
<td class="td_header">##Group##</td>
<td class="td_header">##Email##</td>
<td class="td_header">##Password## (##hash##)</td>
<td class="td_header">##New password##</td>
<td class="td_header">##Password check##</td>
<td class="td_header">##Delete##</td>
</tr>
{section name=loop loop=$users}
<tr class="tr_data" id="tab_row_{$smarty.section.loop.index}">
<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="user[{$smarty.section.loop.index}]" value="{$users[loop].user}" />
{$users[loop].user}
</td>

<td class="td_data_v">
<select name="user_group[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	{section name=loop2 loop=$user_group}
	<option value="{$user_group[loop2]}"{if $users[loop].user_group == $user_group[loop2]} SELECTED{/if}>{$user_group[loop2]}</option>
	{/section}
</select>
</td>

<td class="td_data_v">
<input type="text" name="email[{$smarty.section.loop.index}]" size="20"  value="{$users[loop].email}" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<input type="hidden" name="pass[{$smarty.section.loop.index}]" value="{$users[loop].pass}" />
{$users[loop].pass}
</td>

<td class="td_data_v">
<input type="password" name="pass_n1[{$smarty.section.loop.index}]" size="15" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<input type="password" name="pass_n2[{$smarty.section.loop.index}]" size="15" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

<td class="td_data_v" align="right"><input type="checkbox" name="user_delete[{$smarty.section.loop.index}]" value="YES" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" /></td>

</tr>
{/section}

<tr><td class="td_data_v" colspan="7">&nbsp;</td></tr>
<!--NEW-->
<tr style="background: #FFDC7C;" id="tab_row_new">

<td class="td_data_v"><input type="text" name="new_user" size="15" value="" onChange="SettingsHighlight('tab_row_new', 'btn_submit', true);" /></td>

<td class="td_data_v">
<select name="new_user_group">
	{section name=loop2 loop=$user_group}
	<option value="{$user_group[loop2]}">{$user_group[loop2]}</option>
	{/section}
</select>
</td>

<td class="td_data_v"><input type="text" name="new_email" size="20" /></td>

<td class="td_data_v">&nbsp;</td>

<td class="td_data_v"><input type="password" name="new_pass_n1" size="15" /></td>

<td class="td_data_v"><input type="password" name="new_pass_n2" size="15" /></td>

<td class="td_data_v" align="right">&nbsp;</td>

</tr>
<!--NEW-->

<tr><td colspan="7" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" id="btn_submit" class="button_txt" name="OK" value="##Save changes##" onClick="WarningBeforePageLeave = false;" />
</td></tr>
</table>
</form>

{include file="_page_end.tpl"}
