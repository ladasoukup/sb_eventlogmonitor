<div align="center">
<div style="border: 1px dotted black; width: 550px; margin-top: 10px; padding: 5px;">
<strong>{$js_progressbar_title}</strong><br />

{if $js_progreesbar_hide_countdown != "true"}
##You will be redirected in## <span id="time2redir">?</span> ##seconds##. <a href="{$js_progressbar_url}">{$js_progressbar_url_title}</a>.<br /><br />
{else}
<span id="time2redir" style="display: none;">?</span><br />
{/if}

<script src="core_xp_progress.js" type="text/javascript">
/***********************************************
* WinXP Progress Bar- By Brian Gosselin- http://www.scriptasylum.com/
* Script featured on Dynamic Drive- http://www.dynamicdrive.com
* Please keep this notice intact
***********************************************/
</script>
<script type="text/javascript">
ShowLoadingBar = false;
var counter={$js_progressbar_timer|default:10};
function countDown(){ldelim}
	counter = counter-1;
	document.getElementById("time2redir").innerHTML=counter;
	if (counter<1){ldelim}
		this.location.href='{$js_progressbar_url|replace:"&amp;":"&"}';
	{rdelim} else {ldelim}
		setTimeout("countDown()", 1000);
	{rdelim}
{rdelim}
counter = counter+1;
var bar1= createBar(300,15,'white',1,'black','{$js_progreesbar_color}',85,7,3,"");
countDown();
</script><br />

{if $js_progreesbar_hide_countdown != "true"}##If You don't want to wait, just follow the above link.##<br /><br />{/if}
</div>
</div>