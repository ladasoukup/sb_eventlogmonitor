{include file="_wnd_start.tpl"}
<h2>##Alert filters## - ##Rule##</h2>
<script language="javascript" type="text/javascript" src="core_actb.js"></script>
<script language="JavaScript" type="text/javascript">
// AutoComplete INIT...
var actb_a = new Array();
var actb_evt_computer = new Array({$actb_evt_computer});
var actb_evt_computer_group = new Array({$actb_evt_computer_group});
var actb_evt_code = new Array({$actb_evt_code});
var actb_evt_type = new Array("error", "warning", "information", "audit_success", "audit_failure");
var actb_evt_category = new Array({$actb_evt_category});
var actb_evt_logfile = new Array({$actb_evt_logfile});
var actb_evt_message = new Array({$actb_evt_message});
var actb_evt_source = new Array({$actb_evt_source});
var actb_evt_user = new Array({$actb_evt_user});
</script>
<form name="form_rules" action="{$smarty.server.REQUEST_URI}" method="post">
<div id="rules_block">
{section name=loop loop=$filter_data}

<div id="rule_{$smarty.section.loop.index}">
<select id="evt_rule_condition[{$smarty.section.loop.index}]" name="evt_rule_condition[{$smarty.section.loop.index}]" style="width: 60px;">
	<option value="and"{if $filter_data[loop].evt_rule_condition=="and"} SELECTED{/if}>##AND##</option>
	<option value="or"{if $filter_data[loop].evt_rule_condition=="or"} SELECTED{/if}>##OR##</option>
</select>

<select id="evt_data_type[{$smarty.section.loop.index}]" name="evt_data_type[{$smarty.section.loop.index}]" onchange="SetAutoComplete();">
	<option style="color: #ccc;" value="---"{if $filter_data[loop].evt_data_type=="---"} SELECTED{/if}>- ##SELECT## -</option>
	<option value="evt_computer"{if $filter_data[loop].evt_data_type=="evt_computer"} SELECTED{/if}>##Computer##</option>
	<option value="evt_computer_group"{if $filter_data[loop].evt_data_type=="evt_computer_group"} SELECTED{/if}>##Group##</option>
	<option value="evt_code"{if $filter_data[loop].evt_data_type=="evt_code"} SELECTED{/if}>##Event code##</option>
	<option value="evt_type"{if $filter_data[loop].evt_data_type=="evt_type"} SELECTED{/if}>##Event type##</option>
	<option value="evt_category"{if $filter_data[loop].evt_data_type=="evt_category"} SELECTED{/if}>##Event category##</option>
	<option value="evt_logfile"{if $filter_data[loop].evt_data_type=="evt_logfile"} SELECTED{/if}>##Event log##</option>
	<option value="evt_message"{if $filter_data[loop].evt_data_type=="evt_message"} SELECTED{/if}>##Description##</option>
	<option value="evt_source"{if $filter_data[loop].evt_data_type=="evt_source"} SELECTED{/if}>##Event source##</option>
	<option value="evt_user"{if $filter_data[loop].evt_data_type=="evt_user"} SELECTED{/if}>##User##</option>
</select>

<select id="evt_comparation[{$smarty.section.loop.index}]" name="evt_comparation[{$smarty.section.loop.index}]">
	<option style="color: #ccc;" value="---"{if $filter_data[loop].evt_comparation=="---"} SELECTED{/if}>- ##COMPARATION## -</option>
	<option style="color: green;" value="equal"{if $filter_data[loop].evt_comparation=="equal"} SELECTED{/if}>##is equal##</option>
	<option style="color: red;" value="not_equal"{if $filter_data[loop].evt_comparation=="not_equal"} SELECTED{/if}>##is not equal##</option>
	<option style="color: green;" value="like"{if $filter_data[loop].evt_comparation=="like"} SELECTED{/if}>##is like##</option>
	<option style="color: red;" value="not_like"{if $filter_data[loop].evt_comparation=="not_like"} SELECTED{/if}>##is not like##</option>
	<option style="color: blue;" value="regex"{if $filter_data[loop].evt_comparation=="regex"} SELECTED{/if}>##regex##</option>
	<option style="color: blue;" value="preg_match"{if $filter_data[loop].evt_comparation=="preg_match"} SELECTED{/if}>##preg_match##</option>
</select>

<script language="JavaScript" type="text/javascript">
actb_a[{$smarty.section.loop.index}] = new Array();
</script>
<input type="text" id="evt_data_value[{$smarty.section.loop.index}]" name="evt_data_value[{$smarty.section.loop.index}]" size="50" value="{$filter_data[loop].evt_data_value}" onfocus="actb(this, event, actb_a[{$smarty.section.loop.index}]);" autocomplete="off" />
<img src="templates_img/filter_detail_up.gif" onClick="MoveRuleUp({$smarty.section.loop.index});" width="10" height="16" alt="" border="0" style="cursor: pointer;" />
<img src="templates_img/filter_detail_down.gif" onClick="MoveRuleDown({$smarty.section.loop.index});" width="10" height="16" alt="" border="0" style="cursor: pointer;" />
<img src="templates_img/filter_detail_delete.gif" onClick="DeleteRule({$smarty.section.loop.index});" width="10" height="16" alt="" border="0" style="cursor: pointer;" />

</div>
{assign var="rule_id" value=$smarty.section.loop.index}
{/section}

</div> <!-- rules_block -->

<div style="margin-top: 15px;">
<a href="#" onclick="AddNewRule(); return false;" class="button_txt">##Add another rule##</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" class="button_txt" name="OK" value="##Save changes##" />
</div>
</form>

<script language="JavaScript" type="text/javascript">
var rule_id = {$rule_id};
document.getElementById("evt_rule_condition[0]").disabled = true;

{literal}
function AddNewRule() {
	var newHTMLcode = "";
	var storedData;
	rule_id = rule_id + 1;
	actb_a[rule_id] = new Array();

	storedData = StoreRules_arr();
	newHTMLcode = document.getElementById("rule_0").innerHTML;
	newHTMLcode = replaceAll(newHTMLcode, "[0]", "[" + rule_id + "]");
	newHTMLcode = replaceAll(newHTMLcode, "MoveRuleUp(0)", "MoveRuleUp(" + rule_id + ")");
	newHTMLcode = replaceAll(newHTMLcode, "MoveRuleDown(0)", "MoveRuleDown(" + rule_id + ")");
	newHTMLcode = replaceAll(newHTMLcode, "DeleteRule(0)", "DeleteRule(" + rule_id + ")");
	newHTMLcode = replaceAll(newHTMLcode, "actb_a[0]", "actb_a[" + rule_id + "]");
	newHTMLcode = '<div id="rule_' + rule_id + '">' + newHTMLcode + '</div>\n';
	document.getElementById("rules_block").innerHTML += newHTMLcode;
	
	RestoreRules_arr(storedData);
	document.getElementById("evt_data_type[" + rule_id + "]").value = "---";
	document.getElementById("evt_comparation[" + rule_id + "]").value = "---";
	document.getElementById("evt_rule_condition[" + rule_id + "]").disabled = false;
	document.getElementById("evt_data_value[" + rule_id + "]").value = "";
}

function MoveRuleUp(id) {
	var storedData;
	var temp = new Array(5);
	if (id != 0) {
		rule_id = rule_id + 1;
		storedData = StoreRules_arr();
		temp = storedData[id];
		storedData[id] = storedData[id - 1];
		storedData[id - 1] = temp;
		RestoreRules_arr(storedData);
		rule_id = rule_id - 1;
	}
	document.getElementById("evt_rule_condition[0]").value = "and";
}

function MoveRuleDown(id) {
	var storedData;
	var temp = new Array(5);
	if (id != rule_id) {
		rule_id = rule_id + 1;
		storedData = StoreRules_arr();
		temp = storedData[id];
		storedData[id] = storedData[id + 1];
		storedData[id + 1] = temp;
		RestoreRules_arr(storedData);
		rule_id = rule_id - 1;
	}
	document.getElementById("evt_rule_condition[0]").value = "and";
}

function DeleteRule(id) {
	var startID = id;
	var storedData;
	
	while (startID < rule_id + 1) {
		MoveRuleDown(startID);
		startID++;
	}
	document.getElementById("rule_" + rule_id).innerHTML="";
	
	storedData = StoreRules_arr();
	document.getElementById("rules_block").innerHTML = replaceAll(document.getElementById("rules_block").innerHTML, '<div id="rule_' + rule_id + '"></div>', "");
	RestoreRules_arr(storedData);
	rule_id = rule_id - 1;
}

function StoreRules_arr() {
	var storedData = new Array(999);
	var temp = new Array(5);
	for(i=0; i<rule_id; i++) {
		temp[0] = document.getElementById("evt_rule_condition[" + i + "]").value;
		temp[1] = document.getElementById("evt_data_type[" + i + "]").value;
		temp[2] = document.getElementById("evt_comparation[" + i + "]").value;
		temp[3] = document.getElementById("evt_data_value[" + i + "]").value;
		storedData[i] = new Array(temp[0], temp[1], temp[2], temp[3]);
	}
	return storedData;
}

function RestoreRules_arr(storedData) {
	var temp = new Array(5);
	for(i=0; i<rule_id; i++) {
		temp = storedData[i];
		document.getElementById("evt_rule_condition[" + i + "]").value = temp[0];
		document.getElementById("evt_data_type[" + i + "]").value = temp[1];
		document.getElementById("evt_comparation[" + i + "]").value = temp[2];
		document.getElementById("evt_data_value[" + i + "]").value = temp[3];
	}
}

function SetAutoComplete() {
	var AC_evt_data_type = "";
	for(i=0; i<=rule_id; i++) {
		AC_evt_data_type = document.getElementById("evt_data_type[" + i + "]").value;
		actb_a[i] = new Array();
		if (AC_evt_data_type == "evt_computer") actb_a[i] = actb_evt_computer;
		if (AC_evt_data_type == "evt_computer_group") actb_a[i] = actb_evt_computer_group;
		if (AC_evt_data_type == "evt_code") actb_a[i] = actb_evt_code;
		if (AC_evt_data_type == "evt_type") actb_a[i] = actb_evt_type;
		if (AC_evt_data_type == "evt_category") actb_a[i] = actb_evt_category;
		if (AC_evt_data_type == "evt_logfile") actb_a[i] = actb_evt_logfile;
		if (AC_evt_data_type == "evt_message") actb_a[i] = actb_evt_message;
		if (AC_evt_data_type == "evt_source") actb_a[i] = actb_evt_source;
		if (AC_evt_data_type == "evt_user") actb_a[i] = actb_evt_user;
	}
}

SetAutoComplete();
{/literal}
</script>
{include file="_wnd_end.tpl"}
