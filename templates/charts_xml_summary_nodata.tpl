<?xml version="1.0"?>
<root>
	<type>
		<system>
			<refresh enabled="no"/>
		</system>
		<workspace>
			<chart_area width="1" height="1" x="200" y="200" enabled="no"/>
		</workspace>

		<chart type="3DPie">
			<values show="no" decimal_places="0"/>
			<names show="no"/>
			<animation enabled="yes" speed="8" type="step"/>
			<pie_chart radius="60" x="90" y="60" rotation="0">
				<border enabled="yes" color="0xCCCCCC"/> 
			</pie_chart>	  
			<hints auto_size="yes" horizontal_position="left">
				<font type="Verdana" size="10" align="left" bold="no" italic="no" underline="no"/>
			</hints>
		</chart>
		
		<legend enabled="no" x="1" y="1"></legend>
	</type>

	<data>
		<block>
		</block>
	</data>
	
	<objects>
		<text text="##No values to display...##" x="10" y="50" width="150" height="20" auto_size="no">
			<font type="Verdana" size="10" color="0xFF7C7C" bold="yes" italic="yes" underline="no" align="center"/>
			<background enabled="no" color="White"/>
		</text>
	</objects>
</root>