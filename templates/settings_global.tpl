{include file="_page_start.tpl"}
<h2>##Global config##</h2>

<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Variable##</td>
<td class="td_header">##Value##</td>
</tr>
{section name=loop loop=$global_cfg}
<tr class="tr_data">
<td class="td_data_v" style="font-weight: bold; text-align: right; padding-right: 10px;">
<input type="hidden" name="cfg_var[{$smarty.section.loop.index}]" value="{$global_cfg[loop].cfg_var}" />
<input type="hidden" name="cfg_group[{$smarty.section.loop.index}]" value="{$global_cfg[loop].cfg_group}" />
{$global_cfg[loop].cfg_desc}
</td>

<td class="td_data_v">
{if $global_cfg[loop].cfg_type == "bool"}
<select name="cfg_val[{$smarty.section.loop.index}]">
	<option value="true"{if $global_cfg[loop].cfg_val == "true"} SELECTED{/if}>##YES##</option>
	<option value="false"{if $global_cfg[loop].cfg_val == "false"} SELECTED{/if}>##NO##</option>
</select>
{else}
<input type="text" name="cfg_val[{$smarty.section.loop.index}]" value="{$global_cfg[loop].cfg_val}" size="50" />
{/if}
</td>

</tr>
{/section}
<tr><td colspan="4" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" class="button_txt" name="OK" value="##Save changes##" />
</td></tr>
</table>
</form>


<!--
<script type="text/javascript" src="tab-view.js"></script>
<div id="dhtmlgoodies_tabView1">
	<div class="dhtmlgoodies_aTab">
		Content of tab 1
  
	</div>
	<div class="dhtmlgoodies_aTab">
		Content of tab 2
	</div>
	<div class="dhtmlgoodies_aTab">
		Content of tab 3
	</div>

</div>
<script type="text/javascript">
initTabs('dhtmlgoodies_tabView1', Array('SMTP','bat-prepare','RPC'), 0, 700);
</script>
-->

{include file="_page_end.tpl"}
