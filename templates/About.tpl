{include file="_page_start.tpl"}

<div style="font-size: 1px; height: 15px;">&nbsp;</div>

<table cellspacing="0" cellpadding="0" border="0" style="border: 0px none;">
<tr><td>

<table cellspacing="0" cellpadding="0" border="0">
<tr class="tr_header">
<td colspan="2" class="td_header">##About##</td>
</tr>

<tr>
<td class="td_header_v">SB&nbsp;EventLogMonitor</td>
<td class="td_data_v">Copyright (c) 2006 - {$smarty.now|date_format:"%Y"}, Ladislav Soukup; <a href="http://eventlog-monitor.info/" target="_blank">http://eventlog-monitor.info/</a></td>
</tr>

<tr>
<td class="td_header_v">##Application version##</td>
<td class="td_data_v">{$version}</td>
</tr>

<tr>
<td class="td_header_v">##SWF Charts##</td>
<td class="td_data_v">Powered by <a href="http://www.anychart.com"> AnyChart - Flash Charting Solutions</a></td>
</tr>

<tr>
<td class="td_header_v">##Icons##</td>
<td class="td_data_v">Seth Bareiss / <a href="mailto:sethness@users.sourceforge.net">sethness@users.sourceforge.net</a></td>
</tr>

<tr>
<td class="td_header_v">##Windows agent##</td>
<td class="td_data_v">Adrian Wright /  <a href="mailto:adeyblue@users.sourceforge.net">adeyblue@users.sourceforge.net</a></td>
</tr>

<tr>
<td class="td_header_v">##Recent donations##</td>
<td class="td_data_v">
{section name=loop loop=$project_donors}
{$project_donors[loop].title}
{/section}
</td>
</tr>

<tr>
<td class="td_header_v">##Server version##</td>
<td class="td_data_v">{$server_version}</td>
</tr>

<tr>
<td class="td_header_v">PHP</td>
<td class="td_data_v">{$php_version}</td>
</tr>

<tr>
<td class="td_header_v">SMARTY</td>
<td class="td_data_v">{$smarty.version} (<a href="http://smarty.php.net/" target="_blank">http://smarty.php.net/</a>)</td>
</tr>

<tr>
<td class="td_header_v">ezSQL</td>
<td class="td_data_v">{$smarty.const.EZSQL_VERSION} (Copyright (c) Justin Vincent, <a href="http://php.justinvincent.com/" target="_blank">http://php.justinvincent.com/</a>)</td>
</tr>

<tr>
<td class="td_header_v">SafeSQL</td>
<td class="td_data_v">2.1 (Copyright (c) 2001-2004 ispi of Lincoln, Inc; Monte Ohrt)</td>
</tr>

<tr>
<td class="td_header_v">htmlMimeMail5</td>
<td class="td_data_v">Copyright (c) 2005 Richard Heyes</td>
</tr>

<tr>
<td class="td_header_v">CalendarPopup.js</td>
<td class="td_data_v">May 17, 2003 (Copyright (c) Matt Kruse, <a href="http://www.mattkruse.com/" target="_blank">http://www.mattkruse.com/</a>)</td>
</tr>

<tr>
<td class="td_header_v">TextBox Hint</td>
<td class="td_data_v">1.0 (Copyright (c) 2006 Polaris Digital Limited; Sam Clark, <a href="http://www.polaris-digital.com/" target="_blank">http://www.polaris-digital.com/</a>)</td>
</tr>

<tr>
<td class="td_header_v">curl.exe</td>
<td class="td_data_v"><a href="http://curl.haxx.se/" target="_blank">http://curl.haxx.se/</a></td>
</tr>

<tr>
<td class="td_header_v">CSS layout</td>
<td class="td_data_v">based on Stuart A Nicholls CSS demo (<a href="http://www.cssplay.co.uk/" target="_blank">http://www.cssplay.co.uk/</a>)</td>
</tr>

<tr>
<td class="td_header_v">##WMI EvtLog collector##</td>
<td class="td_data_v">based on VBS code by Software Poetry, Inc. (<a href="http://www.softwarepoetry.com/webob/" target="_blank">http://www.softwarepoetry.com/webob/</a>)</td>
</tr>

</table>

</td><td style="vertical-align: top; padding-left: 20px; padding-top: 20px;">

<div style="padding-bottom: 15px;"><a href="http://sourceforge.net/donate/index.php?group_id=183663" target="_blank"><img src="http://images.sourceforge.net/images/project-support.jpg" width="88" height="32" border="0" alt="Support This Project" /></a></div>
<div style="padding-bottom: 5px;"><a href="http://sourceforge.net" target="_blank"><img src="templates_img/sflogo.png" width="88" height="31" border="0" alt="SourceForge.net Logo" /></a></div>
<div style="padding-bottom: 5px;"><a href="http://php.net" target="_blank"><img src="templates_img/powered_php5.png" width="80" border="0" alt="Powered by PHP" /></a></div>
<div style="padding-bottom: 5px;"><a href="http://www.mysql.com" target="_blank"><img src="templates_img/powered_mysql.png" width="80" border="0" alt="Powered by MySQL" /></a></div>
<div style="padding-bottom: 5px;"><a href="http://smarty.php.net" target="_blank"><img src="templates_img/powered_smarty.png" width="80" border="0" alt="SMARTY template engine" /></a></div>

<div style="padding-bottom: 5px;"><a href="http://www.anychart.com" target="_blank"><img src="charts_anychart/AnyChart-Logo.png" width="185" border="0" alt="AnyChart - Flash Charting Solutions" /></a></div>



</td></tr>
</table>

<h2>##Released under the GNU General Public License##</h2>
<pre>This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</pre>

<a href="gnu-gpl.txt" target="_blank">##The GNU General Public License (GPL)##</a>
<!--
<tr>
<td class="td_header_v"></td>
<td class="td_data_v"></td>
</tr>
-->

{include file="_page_end.tpl"}
