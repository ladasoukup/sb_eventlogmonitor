{include file="_wnd_start.tpl"}
{assign var="sbelm_hide_footer" value="true"}
<h2 style="margin: 10px 10px;">##Group##</h2>
<div style="margin: 0px 10px; height: 200px; overflow: scroll; border: 1px solid #ccc;">
{section name="loop" loop=$groups}
<input type="checkbox" id="{$groups[loop]}" name="{$groups[loop]}" value="checked" />{$groups[loop]} <br />
{/section}
</div>
<div align="center" style="margin: 15px 0px;">
<a href="#" class="button_txt" onclick="SaveGroups(); return false;">##Save changes##</a>
</div>


<script language="JavaScript" type="text/javascript">
var comp_group_id = "{$smarty.get.id}";
var comp_group_label_id = "{$smarty.get.id}_label";

{literal}
function LoadGroups() {
	var groups = opener.document.getElementById(comp_group_id).value;
	var groups_A;
	groups = replaceAll(groups, "~~", "");
	groups_A = groups.split("~");
	for (i=0; i<groups_A.length; i++) {
		document.getElementById(groups_A[i]).checked = true;
	}
}

function SaveGroups() {
	var groups = "";
	var groups_txt = "";
	var overlen = false;
{/literal}
	var tab_row_id = '{$smarty.get.tab_row_id}';
	var btn_submit_id = '{$smarty.get.btn_submit_id}';
	{section name="loop" loop=$groups}
	if (document.getElementById("{$groups[loop]}").checked == true) groups += "{$groups[loop]}~";
	{/section}
{literal}
	
	if (groups != "") { 
		groups = "~~" + groups + "~";
	} else {
		groups = "...";
	}
	groups_txt = replaceAll(groups, "~~", "");
	groups_txt = replaceAll(groups_txt, "~", ", ");
	len = groups_txt.length;
	if (len > 20) { len = 20; overlen = true; }
	groups_txt = groups_txt.substring(0, len);
	if (overlen == true) groups_txt += "...";
	opener.document.getElementById(comp_group_id).value = groups;
	opener.document.getElementById(comp_group_label_id).innerHTML = groups_txt;
	
	opener.SettingsHighlight(tab_row_id, btn_submit_id, true);
	
	this.close();
}

{/literal}

LoadGroups();
</script>
{include file="_wnd_end.tpl"}
