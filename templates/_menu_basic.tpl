{if $submenu != ""}
<a href="{$smarty.server.REQUEST_URI|add_url_param:'sub':''|add_url_param:'from':''}" class="submenu_title">{$submenu_title}</a>
<div class="submenu_content">
{if $submenu_group != ""}
<select name="submenu_group" class="submenu_title" onchange="SwitchSubGroup(escape(this.options[this.selectedIndex].value), '{$smarty.server.REQUEST_URI|add_url_param:'sub_group':'***SUB_GROUP***'|add_url_param:'sub':''|add_url_param:'from':''}')">
{section name=loop loop=$submenu_group}
	<option value="{$submenu_group[loop]}"{if $submenu_group[loop] == $smarty.get.sub_group} SELECTED{/if}>{$submenu_group[loop]|default:'##All computers##'}</option>
{/section}
</select>
{/if}
{section name=loop loop=$submenu}
{if $submenu[loop].sub == $smarty.get.sub}
<a href="{$smarty.server.REQUEST_URI|add_url_param:'sub':$submenu[loop].sub|add_url_param:'from':''}" class="button_submenu" style="background-color: #ffdc7c;">{$submenu[loop].title}</a>
{else}
<a href="{$smarty.server.REQUEST_URI|add_url_param:'sub':$submenu[loop].sub|add_url_param:'from':''}" class="button_submenu">{$submenu[loop].title}</a>
{/if}
{/section}
</div>
{/if}