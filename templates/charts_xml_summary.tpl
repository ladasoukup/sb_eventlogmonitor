<?xml version="1.0"?>
<root>
	<type>
		<system>
			<refresh enabled="no"/>
		</system>
		<workspace>
			<chart_area width="1" height="1" x="200" y="200" enabled="no"/>
		</workspace>

		<chart type="3DPie">
			<values show="no" decimal_places="0"/>
			<names show="no"/>
			<animation enabled="yes" speed="8" type="step"/>
			<pie_chart radius="75" x="85" y="55" rotation="0">
				<border enabled="yes" color="0xCCCCCC"/> 
			</pie_chart>	  
			<hints auto_size="yes" horizontal_position="left">
				<font type="Verdana" size="10" align="left" bold="no" italic="no" underline="no"/>
			</hints>
		</chart>
		
		<legend enabled="no" x="1" y="1"></legend>
	</type>

	<data>
		<block>
{section name=loop loop=$chart_data}
			<set value="{$chart_data[loop].value}" color="{$chart_data[loop].color}" name="{$chart_data[loop].name}" url="{$chart_data[loop].url}" url_target="{$chart_data[loop].url_target}"/>
{/section}
		</block>
	</data>
</root>