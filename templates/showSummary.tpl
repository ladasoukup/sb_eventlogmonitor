{include file="_page_start.tpl"}

<div style="font-size: 1px; height: 15px;">&nbsp;</div>
<table cellspacing="0" cellpadding="0" border="0" width="798">
<tr class="tr_header">
<td colspan="6" class="td_header">##Summary## - ##EventLog##</td>
</tr>
<tr>
<td class="td_header_v" width="200">##Total monitored machines##</td>
<td colspan="4" class="td_data_v">{$summary_count_computers|default:'0'}</td>
<td rowspan="7" width="170" class="td_data_v">{anychart name="event" width="170" height="120" chart="3DPie"}</td>
</tr>

{section name=loop loop=$summary_count_evt}
<tr class="evt_{$summary_count_evt[loop].db_name}">
<td class="td_header_v btn_sort" onclick="window.location.href='index.php?module=showEventLog&sub=&sub_group={$smarty.get.sub_group}&f_evt_{$summary_count_evt[loop].db_name}={$summary_count_evt[loop].db_name}';">
{if $summary_count_evt[loop].db_name == "error"}##Error##
{elseif $summary_count_evt[loop].db_name == "warning"}##Warning##
{elseif $summary_count_evt[loop].db_name == "information"}##Information##
{elseif $summary_count_evt[loop].db_name == "audit_success"}##Audit success##
{elseif $summary_count_evt[loop].db_name == "audit_failure"}##Audit failure##
{/if}
</td>
<td class="td_data_v">{$summary_count_evt[loop].count|default:'0'}</td>
<td class="td_data_v"><a href="index.php?module=showEventLog&amp;sub=&amp;sub_group={$smarty.get.sub_group}&amp;f_evt_{$summary_count_evt[loop].db_name}={$summary_count_evt[loop].db_name}&amp;f_date_from={$date_today} 00:00:00&amp;f_date_to={$date_today} 23:59:59" class="summary_link">##today##: {$summary_count_evt[loop].count_today|default:'0'}</a></td>
<td class="td_data_v"><a href="index.php?module=showEventLog&amp;sub=&amp;sub_group={$smarty.get.sub_group}&amp;f_evt_{$summary_count_evt[loop].db_name}={$summary_count_evt[loop].db_name}&amp;f_date_from={$date_yesterday} 00:00:00&amp;f_date_to={$date_yesterday} 23:59:59" class="summary_link">##yesterday##: {$summary_count_evt[loop].count_yesterday|default:'0'}</a></td>
<td class="td_data_v"><a href="index.php?module=showEventLog&amp;sub=&amp;sub_group={$smarty.get.sub_group}&amp;f_evt_{$summary_count_evt[loop].db_name}={$summary_count_evt[loop].db_name}&amp;f_date_from={$date_2days} 00:00:00&amp;f_date_to={$date_2days} 23:59:59" class="summary_link">##two days ago##: {$summary_count_evt[loop].count_2days|default:'0'}</a></td>
</tr>
{/section}

<tr>
<td class="td_header_v">##TOTAL##</td>
<td class="td_data_v" colspan="4">{$summary_count_sum|default:'0'}</td>
</tr>
</table>

<div style="font-size: 1px; height: 15px;">&nbsp;</div>
{section name=loop loop=$summary_time_interals}
<a href="index.php?module=showSummary&amp;time_period={$smarty.section.loop.index}&amp;sub_group={$smarty.get.sub_group}" class="button_txt{if $smarty.section.loop.index==$smarty.get.time_period} button_txt_active{/if}" style="display: block; width: 111px; height: 18px; text-align: left; float: left;">{$summary_time_interals[loop]}</a>
{/section}
<div style="clear: left; font-size: 1px; height: 0px;">&nbsp;</div>

<table cellspacing="0" cellpadding="0" border="0" width="798">
<tr class="tr_header">
<td colspan="2" width="170" class="td_header">##Summary## - ##Error##</td>
<td class="td_spacer">&nbsp;</td>
<td colspan="2" width="170" class="td_header">##Summary## - ##Warning##</td>
<td class="td_spacer">&nbsp;</td>
<td colspan="2" width="170" class="td_header">##Summary## - ##Audit failure##</td>
<td class="td_spacer">&nbsp;</td>
<td colspan="2" width="170" class="td_header">##Summary## - ##Audit success##</td>
</tr>

{section name=loop loop=7}
<tr style="text-transform: uppercase;">
{if $summary_error[loop].evt_computer != ""}
<td width="90" class="td_header_v btn_sort" onclick="window.location.href='index.php?module=showEventLog&sub={$summary_error[loop].evt_computer}&sub_group={$smarty.get.sub_group}&f_evt_error=error&f_date_from={$sumary_time_period_start}&f_date_to={$sumary_time_period_end}';">{$summary_error[loop].evt_computer}</td>
{else}
<td width="90" class="td_header_v">---</td>
{/if}
<td class="td_data_v" style="background-color: #{$summary_graph_colors[loop]};">{$summary_error[loop].evt_count|default:'0'}</td>
<td class="td_spacer">&nbsp;</td>

{if $summary_warning[loop].evt_computer != ""}
<td width="90" class="td_header_v btn_sort" onclick="window.location.href='index.php?module=showEventLog&sub={$summary_warning[loop].evt_computer}&sub_group={$smarty.get.sub_group}&f_evt_warning=warning&f_date_from={$sumary_time_period_start}&f_date_to={$sumary_time_period_end}';">{$summary_warning[loop].evt_computer}</td>
{else}
<td width="90" class="td_header_v">---</td>
{/if}
<td class="td_data_v" style="background-color: #{$summary_graph_colors[loop]};">{$summary_warning[loop].evt_count|default:'0'}</td>
<td class="td_spacer">&nbsp;</td>

{if $summary_audit_failure[loop].evt_computer != ""}
<td width="90" class="td_header_v btn_sort" onclick="window.location.href='index.php?module=showEventLog&sub={$summary_audit_failure[loop].evt_computer}&sub_group={$smarty.get.sub_group}&f_evt_audit_failure=audit_failure&f_date_from={$sumary_time_period_start}&f_date_to={$sumary_time_period_end}';">{$summary_audit_failure[loop].evt_computer}</td>
{else}
<td width="90" class="td_header_v">---</td>
{/if}
<td class="td_data_v" style="background-color: #{$summary_graph_colors[loop]};">{$summary_audit_failure[loop].evt_count|default:'0'}</td>
<td class="td_spacer">&nbsp;</td>

{if $summary_audit_success[loop].evt_computer != ""}
<td width="90" class="td_header_v btn_sort" onclick="window.location.href='index.php?module=showEventLog&sub={$summary_audit_success[loop].evt_computer}&sub_group={$smarty.get.sub_group}&f_evt_audit_success=audit_success&f_date_from={$sumary_time_period_start}&f_date_to={$sumary_time_period_end}';">{$summary_audit_success[loop].evt_computer}</td>
{else}
<td width="90" class="td_header_v">---</td>
{/if}
<td class="td_data_v" style="background-color: #{$summary_graph_colors[loop]};">{$summary_audit_success[loop].evt_count|default:'0'}</td>

</tr>
{/section}
<tr>

<td colspan="2" class="td_data_v">{anychart name="error" width="170" height="120" chart="3DPie"}</td>
<td class="td_spacer">&nbsp;</td>
<td colspan="2" class="td_data_v">{anychart name="warning" width="170" height="120" chart="3DPie"}</td>
<td class="td_spacer">&nbsp;</td>
<td colspan="2" class="td_data_v">{anychart name="audit_failure" width="170" height="120" chart="3DPie"}</td>
<td class="td_spacer">&nbsp;</td>
<td colspan="2" class="td_data_v">{anychart name="audit_success" width="170" height="120" chart="3DPie"}</td>

</tr>
</table>


<div style="font-size: 1px; height: 15px;">&nbsp;</div>
<table cellspacing="0" cellpadding="0" border="0" width="798">
<tr class="tr_header">
<td colspan="6" class="td_header">##Summary## - ##Alerts##</td>
</tr>
<tr>
<td class="td_header_v" width="200">##Total active filters##</td>
<td colspan="4" class="td_data_v">{$alert_filters_count}</td>
<td rowspan="7" width="170" class="td_data_v">{anychart name="alert" width="170" height="120" chart="3DPie"}</td>
</tr>

{section name=loop loop=$summary_count_alerts}
<tr class="evt_{$summary_count_alerts[loop].db_name}">
<td class="td_header_v btn_sort" onclick="window.location.href='index.php?module=showAlerts&sub=&sub_group={$smarty.get.sub_group}&f_evt_{$summary_count_alerts[loop].db_name}={$summary_count_alerts[loop].db_name}';">
{if $summary_count_alerts[loop].db_name == "error"}##Error##
{elseif $summary_count_alerts[loop].db_name == "warning"}##Warning##
{elseif $summary_count_alerts[loop].db_name == "information"}##Information##
{elseif $summary_count_alerts[loop].db_name == "audit_success"}##Audit success##
{elseif $summary_count_alerts[loop].db_name == "audit_failure"}##Audit failure##
{/if}
</td>
<td class="td_data_v">{$summary_count_alerts[loop].count|default:'0'}</td>

<td class="td_data_v"><a href="index.php?module=showAlerts&amp;sub=&amp;sub_group={$smarty.get.sub_group}&amp;f_evt_{$summary_count_alerts[loop].db_name}={$summary_count_alerts[loop].db_name}&amp;f_date_from={$date_today} 00:00:00&amp;f_date_to={$date_today} 23:59:59" class="summary_link">
##today##: {$summary_count_alerts[loop].count_today|default:'0'}</a></td>
<td class="td_data_v"><a href="index.php?module=showAlerts&amp;sub=&amp;sub_group={$smarty.get.sub_group}&amp;f_evt_{$summary_count_alerts[loop].db_name}={$summary_count_alerts[loop].db_name}&amp;f_date_from={$date_yesterday} 00:00:00&amp;f_date_to={$date_yesterday} 23:59:59" class="summary_link">
##yesterday##: {$summary_count_alerts[loop].count_yesterday|default:'0'}</a></td>
<td class="td_data_v"><a href="index.php?module=showAlerts&amp;sub=&amp;sub_group={$smarty.get.sub_group}&amp;f_evt_{$summary_count_alerts[loop].db_name}={$summary_count_alerts[loop].db_name}&amp;f_date_from={$date_2days} 00:00:00&amp;f_date_to={$date_2days} 23:59:59" class="summary_link">
##two days ago##: {$summary_count_alerts[loop].count_2days|default:'0'}</a></td>
</tr>
{/section}

<tr>
<td class="td_header_v">##TOTAL##</td>
<td class="td_data_v" colspan="4">{$summary_count_alerts_sum|default:'0'}</td>
</tr>
</table>


<div style="font-size: 1px; height: 15px;">&nbsp;</div>
<table cellspacing="0" cellpadding="0" border="0" width="798">
<tr class="tr_header">
<td colspan="5" class="td_header">##Last alerts##</td>
</tr>
{section name=loop loop=$last_alerts}
<tr class="tr_data evt_{$last_alerts[loop].evt_type}" onClick="OpenWindow('index.php?module=showAlertsDetail&sub={$last_alerts[loop].evt_computer}&id={$last_alerts[loop].id}', 'evtDetail', 650, 500);">
<td class="td_data" style="width: 16px;"><img src="templates_img/evt_{$last_alerts[loop].evt_type}.gif" width="16" heigt="16" alt="{$last_alerts[loop].evt_type}" /></td>
<td class="td_data" style="width: 120px;">{$last_alerts[loop].evt_time_generated}</td>
<td class="td_data">{$last_alerts[loop].evt_computer}</td>
<td class="td_data">{$last_alerts[loop].evt_source}</td>
<td class="td_data">{$last_alerts[loop].evt_message|truncate:85:"...":false}</td>
</tr>
{/section}
</table>

{include file="_page_end.tpl"}
