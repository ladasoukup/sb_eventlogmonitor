{section name=loop loop=$mainmenu}
{if $mainmenu[loop].title == "---"}
<div style="font-size: 1px; height: 20px;">&nbsp;</div>
{else}
{if $mainmenu[loop].module == $smarty.get.module}
<a href="index.php?module={$mainmenu[loop].module}" class="button_menu" style="color: #204d9f;">{$mainmenu[loop].title}</a>
{else}
<a href="index.php?module={$mainmenu[loop].module}" class="button_menu">{$mainmenu[loop].title}</a>
{/if}
{/if}
{/section}

{if $smarty.const.UserAuth}
<div style="font-size: 1px; height: 5px;">&nbsp;</div>
<a href="index.php?module=logout" class="button_menu">##Logout##</a>
{/if}


{if $smarty.get.module=="showEventLog"}
{include file="_menu_showEventLog.tpl"}
{elseif $smarty.get.module=="showAlerts"}
{include file="_menu_showEventLog.tpl"}
{elseif $smarty.get.module=="showSummary"}
{include file="_menu_showEventLog.tpl"}
{else}
{include file="_menu_basic.tpl"}
{/if}


{if $autoRefresh != ""}
<div class="submenu_title">
<a href="" onclick="window.location.reload(); return false;"><img src="templates_img/ico_reload.gif" height="16" style=" padding: 2px; float: right;" /></a>
##Auto refresh##</div>
<div class="submenu_content">
<select id="page_auto_refresh" name="page_auto_refresh" class="submenu_title" onchange="setAutoRefersh(escape(this.options[this.selectedIndex].value));">
{section name=loop loop=$autoRefresh}
	<option value="{$autoRefresh[loop].value}">{$autoRefresh[loop].title}</option>
{/section}
</select>
</div>
<script language="JavaScript" type="text/javascript">StartAutoRefresh();</script>
{/if}


{if $smarty.get.module=="showSummary"}
<div style="margin: 25px 0px; text-align: center;">
##This page is powered by##
<a href="http://www.anychart.com" target="_blank"><img src="charts_anychart/AnyChart.png" alt="AnyChart - Flash Charting Solutions" border="0" width="164" height="314"/></a>
</div>
{/if}