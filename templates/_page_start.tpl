<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$langID}" lang="{$langID}">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.const.PageCharSet}" />
		<meta http-equiv="content-language" content="{$langID}" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="SB EventLog Monitor {$version} - Web GUI" />
		<meta name="robots" content="NONE" />
		<link rel="icon" href="favicon.gif" />
		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="alternate" type="application/rss+xml" title="##Last alerts##" href="index.php?module=rss" />
		<link rel="STYLESHEET" type="text/css" href="core.css" />
		<link rel="STYLESHEET" type="text/css" href="core_table.css" />
		<link rel="STYLESHEET" type="text/css" href="tab-view.css" />
		<link rel="STYLESHEET" type="text/css" media="print" href="print.css" />
		<script src="core.js" type="text/javascript"></script>
		<title>{$app_name} {$version}</title>
	</head>
<body>
<script type="text/javascript" src="swfobject.js"></script>
<div id="head"><div id="pad1"></div><img src="templates_img/sbelm_logo.gif" width="450" height="50" alt="&nbsp;{$app_name}"/></div>
<div id="version">
{dynamic}{$smarty.now|date_format:"%d.%m.%Y %H:%M:%S"}{/dynamic}
{if $smarty.const.SMARTY_caching == true}&nbsp;(##generated at## {$smarty.now|date_format:"%H:%M:%S"}){/if}
&nbsp;|&nbsp;
<a href="http://eventlog-monitor.info" target="_blank">{$app_name} {$version}</a><br />
<a href="index.php?module=help" class="button_txt" style="display: block; height: 16px; margin-top: 1px; float: right;" onClick="OpenWindow('index.php?module=help&hlp_main={$smarty.get.module}&hlp_sub={$smarty.get.sub}', 'HELP', 650, 500); return false;"><img src="templates_img/ico_help.gif" width="16" height="16" alt="" border="0" style="float: left;" />&nbsp;##Help##</a>
<a href="index.php?module=rss" class="button_txt" style="display: block; height: 16px; margin-top: 1px; margin-right: 10px; margin-left: 10px; float: right;"><img src="templates_img/feed-icon-16x16.png" width="16" height="16" alt="##RSS##" border="0" style="float: left;" />&nbsp;##RSS##</a>
##GUI language##:
<select id="LangSelect" name="LangSelect" onChange="GuiSwitchLang();">
{foreach from=$_i18n_LangTable_ key=key item=item}
	<option value="{$key}"{if $key == $langID} SELECTED{/if}>{$item}</option>
{/foreach}
</select>
</div>


<div id="left">
<div class="pad2"></div>
<!--[if lte IE 6]>
<div style="font-size: 1px; height: 2px;">&nbsp;</div>
<![endif]-->
{include file="_menu.tpl"}
</div>

<div id="content">
<div class="pad2"></div>

<!--[if lte IE 6]>
<div style="font-size: 1px; height: 2px;">&nbsp;</div>
<![endif]-->