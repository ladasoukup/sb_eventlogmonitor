<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$smarty.const.Language}" lang="{$smarty.const.Language}">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="content-language" content="{$smarty.const.Language}" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	{literal}<style type="text/css">
	/*<![CDATA[*/
	body {font-family: trebuchet ms, tahoma, verdana, arial, sans-serif; font-size: 14px; margin: 5px; padding: 5px;}
	h1 {font-size: 18px; font-weight: bold; margin: 10px 0px;}
	h2 {font-size: 16px; font-weight: bold; margin: 10px 0px;}
	h3 {font-size: 14px; font-weight: bold; margin: 5px 0px;}
	table {padding: 0px; border: 1px solid #808080;}
	tr.tr_header {font-weight: bold; height: 20px; background: #dedcd6;}
	td {padding-left: 3px;	padding-right: 3px;}
	td.td_header {border: 1px solid #808080;}
	td.td_data {border-top: 1px solid #808080;}
	td.td_header_v {border-top: 1px solid #808080; border-right: 1px solid #808080; font-weight: bold; background: #dedcd6; vertical-align: top;}
	td.td_data_v {border-top: 1px solid #808080;}
	/*]]>*/
	</style>{/literal}
  </head>
<body>
<div style="background: #ddd; font-size: 36px; color:#fff;">{$app_name}</div>