{include file="_wnd_start.tpl"}
<script language="JavaScript">
{if $reload_parent == "True"}
opener.setTimeout('window.location.reload()', 1500);
self.close();
{/if}
var prevId = IdArray_prev({$event_data.id}, true);
var nextId = IdArray_next({$event_data.id}, true);
</script>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="td_header_v" width="100">##Date and time##</td>
<td class="td_data_v">{$event_data.evt_time_generated|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Computer##</td>
<td class="td_data_v" style="text-transform: uppercase;">{$event_data.evt_computer|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event type##</td>
<td class="td_data_v">{$event_data.evt_type|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event code##</td>
<td class="td_data_v">{$event_data.evt_code|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event category##</td>
<td class="td_data_v">{$event_data.evt_category|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event source##</td>
<td class="td_data_v">{$event_data.evt_source|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##User##</td>
<td class="td_data_v">{$event_data.evt_user|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Description##</td>
<td class="td_data_v"><div class="evt_detail_message">{$event_data.evt_message|nl2br}</div></td>
</tr>
</table>

<div style="text-align: center; padding: 15px;">

<a href="#" onclick="window.location.href='index.php?module={$smarty.get.module}&sub={$smarty.get.sub}&id=' + prevId; return false;" class="button_txt">&lt;&lt; ##Newer## &lt;&lt;</a>
&nbsp;&nbsp;&nbsp;
<a href="#" onclick="window.location.href='index.php?module={$smarty.get.module}&sub={$smarty.get.sub}&id=' + nextId; return false;" class="button_txt">&gt;&gt; ##Older## &gt;&gt;</a>
&nbsp;&nbsp;&nbsp;

{if $smarty.get.module=="showAlertsDetail"}
{if $event_data.evt_noise==0}
<a href="index.php?module=showAlertsDetail&amp;action=repaired&amp;id={$event_data.id}" class="button_txt">##Mark as repaired##</a>
{else}
<a href="index.php?module=showAlertsDetail&amp;action=notrepaired&amp;id={$event_data.id}" class="button_txt">##Mark as NOT repaired##</a>
{/if}
{/if}

</div>
<div style="text-align: center; padding: 15px; padding-top: 0px;">

<a href="#" onclick="OpenWindow('http://eventid.net/display.asp?eventid={$event_data.evt_code}&source={$event_data.evt_source}', 'eventid_net', 780, 580); self.close(); return false;" class="button_txt">##More on eventid.net##</a>
&nbsp;&nbsp;&nbsp;
<a href="#" onclick="self.print(); return false;" class="button_txt">##Print event##</a>
&nbsp;&nbsp;&nbsp;
<a href="#" onclick="self.close(); return false;" class="button_txt">##Close window##</a>
</div>
{include file="_wnd_end.tpl"}
