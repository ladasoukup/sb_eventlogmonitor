{if $submenu_group != ""}
<div class="submenu_title">##Group##</div>
<div class="submenu_content">
<select name="submenu_group" class="submenu_title" onchange="SwitchSubGroup(escape(this.options[this.selectedIndex].value), '{$smarty.server.REQUEST_URI|add_url_param:'sub_group':'***SUB_GROUP***'|add_url_param:'sub':''|add_url_param:'from':''}')">
{section name=loop loop=$submenu_group}
	<option value="{$submenu_group[loop]}"{if $submenu_group[loop] == $smarty.get.sub_group} SELECTED{/if}>{$submenu_group[loop]|default:'##All groups##'}</option>
{/section}
</select>
</div>
{/if}

{if $submenu != ""}
<div class="submenu_title" style="margin-top: 5px;">##Computer##</div>
<div class="submenu_content">
<select name="submenu" class="submenu_title" onchange="SwitchSubGroup(escape(this.options[this.selectedIndex].value), '{$smarty.server.REQUEST_URI|add_url_param:'sub':'***SUB_GROUP***'|add_url_param:'from':''}')">
{section name=loop loop=$submenu}
	<option value="{$submenu[loop].sub}"{if $submenu[loop].sub == $smarty.get.sub} SELECTED{/if}>{$submenu[loop].title|default:'##All computers##'}</option>
{/section}
</select>
</div>
{/if}