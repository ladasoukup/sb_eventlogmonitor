<root>
	<type>
		<system>
			<refresh enabled='no'/>
		</system>
		<chart type="Stacked Horizontal 3DColumn">
			<hints auto_size="no" horizontal_position="center"/>
			<names show="no"/>
			<values show="no" decimal_places="0"/>
		</chart>
		
		<workspace>
			<background enabled="yes" type="gradient" gradient_type="linear">
				<colors>
					<color>0xFFFFDF</color>
					<color>0xFFDC7C</color>
				</colors>
				<alphas>
					<alpha>100</alpha>
					<alpha>100</alpha>
				</alphas>
				<ratios>
					<ratio>0</ratio>
					<ratio>0xFF</ratio>
				</ratios>
				<matrix r="1.58"/>
			</background>
			<chart_area height="500" width="720" y="10" x="100">
				<background enabled="no"/>
				<border enabled="no"/>
			</chart_area>
			
			<base_area enabled="no"/>
 
		</workspace>
		
		<legend enabled="no" x="830" y="10" rows_auto_count="yes">
		</legend>
	</type>

	<data>
		<block name="&nbsp;">
		</block>
	</data>
	
	<objects>
		<text text="##No values to display...##" x="50" y="220" width="900" height="35" auto_size="no">
			<font type="Verdana" size="20" color="0xFF7C7C" bold="yes" italic="yes" underline="no" align="center"/>
			<background enabled="no" color="White"/>
		</text>
	</objects>
</root>