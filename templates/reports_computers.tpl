{include file="_page_start.tpl"}
<div style="font-size: 1px; height: 15px;">&nbsp;</div>

<div id="filtry" style="display: block;">
<form name="report" action="index.php">
<input type="hidden" name="module" value="{$smarty.get.module}" />
<input type="hidden" name="sub" value="{$smarty.get.sub}" />
<input type="hidden" name="sub_group" value="{$smarty.get.sub_group}" />
<table border="0" width="100%" style="border: 0px none #fff;">
<tr><td valign="top">

<table border="0" style="border: 0px none #fff;">
<tr>
<td class="form_title">##Event type##:</td>
<td>
<img src="templates_img/evt_error.gif" width="16" heigt="16" alt="error" /><input type="radio" name="f_evt_type" value="error" style="height: 20px;"{if $smarty.get.f_evt_type == "error"} CHECKED{/if} onclick="report.submit();" />&nbsp;&nbsp;
<img src="templates_img/evt_warning.gif" width="16" heigt="16" alt="warning" /><input type="radio" name="f_evt_type" value="warning" style="height: 20px;"{if $smarty.get.f_evt_type == "warning"} CHECKED{/if} onclick="report.submit();" />&nbsp;&nbsp;
<img src="templates_img/evt_audit_failure.gif" width="16" heigt="16" alt="audit_failure" /><input type="radio" name="f_evt_type" value="audit_failure" style="height: 20px;"{if $smarty.get.f_evt_type == "audit_failure"} CHECKED{/if} onclick="report.submit();" />&nbsp;&nbsp;
<img src="templates_img/evt_audit_success.gif" width="16" heigt="16" alt="audit_success" /><input type="radio" name="f_evt_type" value="audit_success" style="height: 20px;"{if $smarty.get.f_evt_type == "audit_success"} CHECKED{/if} onclick="report.submit();" />&nbsp;&nbsp;
<img src="templates_img/evt_information.gif" width="16" heigt="16" alt="information" /><input type="radio" name="f_evt_type" value="information" style="height: 20px;"{if $smarty.get.f_evt_type == "information"} CHECKED{/if} onclick="report.submit();" />&nbsp;&nbsp;
</td>
<td style="width: 12px;">&nbsp;</td>
<td class="form_title">##Group##:</td>
<td>
<select id="sub_group" name="sub_group" class="submenu_title" onchange="report.submit(); return false;">
{section name=loop loop=$all_groups}
	<option value="{$all_groups[loop]}"{if $all_groups[loop] == $smarty.get.sub_group} SELECTED{/if}>{$all_groups[loop]|default:'##All groups##'}</option>
{/section}
</select>
</td>
</tr>
<tr>

<td class="form_title">##Time period##:</td>
<td>
<select name="time_period" class="submenu_title" onchange="report.submit(); return false;">
{section name=loop loop=$reports_time_periods}
	<option value="{$smarty.section.loop.index}"{if $smarty.section.loop.index == $smarty.get.time_period} SELECTED{/if}>{$reports_time_periods[loop]}</option>
{/section}
</select>
</td>
<td style="width: 12px;">&nbsp;</td>
<td class="form_title">&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>

</td><td valign="bottom">
<div align="right"><a href="#" class="button_txt" onclick="report.submit(); return false;">- ##Apply filter## -</a></div>
</td></tr></table>

</form>
</div>

{anychart name="computers" width="1000" height="540" chart="StackedHorizontal3DColumn"}

{include file="_page_end.tpl"}
