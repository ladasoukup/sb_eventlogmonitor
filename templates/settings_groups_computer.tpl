{include file="_page_start.tpl"}
<script language="JavaScript" type="text/javascript">
	PageUnloadWarningText = "##There are unsaved changes. If You will leave this page, all changes will be lost.##";
</script>
<h2>##Computer groups##</h2>
<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Group##</td>
<td class="td_header">##Delete##</td>
<td class="td_header">##Computers##</td>
</tr>
{section name=loop loop=$groups_computer}
<tr class="tr_data" id="tab_row_{$smarty.section.loop.index}">
<td class="td_data_v" style="font-weight: bold;">
<input type="text" name="groups_computer[{$groups_computer[loop].group}]" value="{$groups_computer[loop].group}" size="20" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>
<td class="td_data_v" align="center"><input type="checkbox" name="groups_computer_delete[{$groups_computer[loop].group}]" value="YES" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" /></td>
<td class="td_data_v">{$groups_computer[loop].computers}</td>
</tr>
{/section}

<tr><td class="td_data_v" colspan="3">&nbsp;</td></tr>
<!--NEW-->
<tr style="background: #FFDC7C;" id="tab_row_new">
<td class="td_data_v" style="font-weight: bold;">
<input type="text" name="new_groups_computer" value="" size="20" onChange="SettingsHighlight('tab_row_new', 'btn_submit', true);" />
</td>

<td class="td_data_v" style="font-weight: bold;">&nbsp;</td>

<td class="td_data_v" style="font-weight: bold;">&nbsp;</td>

</tr>
<!--NEW-->

<tr><td colspan="3" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" id="btn_submit" class="button_txt" name="OK" value="##Save changes##" onClick="WarningBeforePageLeave = false;" />
</td></tr>
</table>

</form>
{include file="_page_end.tpl"}