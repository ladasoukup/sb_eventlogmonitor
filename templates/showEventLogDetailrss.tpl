{include file="_wnd_start.tpl"}
{if $reload_parent == "True"}<script language="JavaScript">
opener.setTimeout('window.location.reload()', 1500);
self.close();
</script>{/if}
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="td_header_v" width="100">##Date and time##</td>
<td class="td_data_v">{$event_data.evt_time_generated|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Computer##</td>
<td class="td_data_v" style="text-transform: uppercase;">{$event_data.evt_computer|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event type##</td>
<td class="td_data_v">{$event_data.evt_type|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event code##</td>
<td class="td_data_v">{$event_data.evt_code|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event category##</td>
<td class="td_data_v">{$event_data.evt_category|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event source##</td>
<td class="td_data_v">{$event_data.evt_source|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##User##</td>
<td class="td_data_v">{$event_data.evt_user|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Description##</td>
<td class="td_data_v"><div class="evt_detail_message">{$event_data.evt_message|nl2br}</div></td>
</tr>
</table>

<div style="text-align: center; padding: 15px;">
<a href="#" onclick="self.print(); return false;" class="button_txt">##Print event##</a>
&nbsp;&nbsp;&nbsp;
{if $smarty.get.module=="showAlertsDetailrss"}
{if $event_data.evt_noise==0}
<a href="index.php?module=showAlertsDetailrss&amp;action=repaired&amp;id={$event_data.id}" class="button_txt">##Mark as repaired##</a>
{else}
<a href="index.php?module=showAlertsDetailrss&amp;action=notrepaired&amp;id={$event_data.id}" class="button_txt">##Mark as NOT repaired##</a>
{/if}
&nbsp;&nbsp;&nbsp;
{/if}
</div>

{include file="_wnd_end.tpl"}
