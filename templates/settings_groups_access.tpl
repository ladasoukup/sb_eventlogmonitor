{include file="_page_start.tpl"}
<script language="JavaScript" type="text/javascript">
	PageUnloadWarningText = "##There are unsaved changes. If You will leave this page, all changes will be lost.##";
</script>
<h2>##Access control##</h2>

<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header">
<td class="td_header">&nbsp;</td>
{section name=loop_u loop=$groups_user}
<td class="td_header">{$groups_user[loop_u]}</td>
{/section}
</tr>

{section name=loop_c loop=$groups_computer}
<tr id="tab_row_{$smarty.section.loop_c.index}">
	<td class="td_header_v" style="text-align: right;">{$groups_computer[loop_c]|default:'##All computers##'}</td>
	{section name=loop_u loop=$groups_user}
	<td class="td_data_v" style="text-align: center;">
	{assign var=idx_c value=$groups_computer[loop_c]}
	{assign var=idx_u value=$groups_user[loop_u]}
	<input type="checkbox" name="access_table[{$idx_c}][{$idx_u}]" value="1"{if $access_table.$idx_c.$idx_u == 1}CHECKED{/if} onChange="SettingsHighlight('tab_row_{$smarty.section.loop_c.index}', 'btn_submit', true);" />
	</td>
	{/section}
</tr>
{/section}

<tr><td class="td_data_v" colspan="{$cols_count}">&nbsp;</td></tr>

<tr><td colspan="{$cols_count}" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" id="btn_submit" class="button_txt" name="OK" value="##Save changes##" onClick="WarningBeforePageLeave = false;" />
</td></tr>

</table>
</form>
{include file="_page_end.tpl"}
