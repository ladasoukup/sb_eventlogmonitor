<root>
	<type>
		<system>
			<refresh enabled='no'/>
		</system>
		<chart type="Stacked Horizontal 3DColumn">
			<animation type="all" appearance="size" speed="35"/>
			<column_chart column_space="0">
				<background alpha="70"/>
				<block_names>
					<font size="9" type="Tahoma"/>
				</block_names>
			</column_chart>
			<hints auto_size="yes" horizontal_position="center">
				<font type="Verdana" size="12" align="left" bold="no" italic="no" underline="no"/>
				<text>{literal}{BLOCK_NAME} ({NAME}) - {VALUE}{/literal}</text>
			</hints>
			<names show="yes"/>
			<values show="no" decimal_places="0"/>
		</chart>
		
		<workspace>
			<background enabled="yes" type="gradient" gradient_type="linear">
				<colors>
					<color>0xFFFFDF</color>
					<color>0xFFDC7C</color>
				</colors>
				<alphas>
					<alpha>100</alpha>
					<alpha>100</alpha>
				</alphas>
				<ratios>
					<ratio>0</ratio>
					<ratio>0xFF</ratio>
				</ratios>
				<matrix r="1.58"/>
			</background>
			<chart_area height="500" width="720" y="10" x="100">
				<background enabled="no"/>
				<border enabled="no"/>
			</chart_area>
			
			<base_area enabled="no"/>
			
			<grid>
				<values lines_step="{$graph_y_grid_step|default:'5'}">
					<lines color="0xC7C7C7"/>
					<captions x_offset="0">
						<font size="8" color="Black" type="Tahoma"/>
					</captions>
				</values>
			</grid>  
		</workspace>
		
		<legend enabled="yes" x="830" y="10" rows_auto_count="yes">
			<scroller enabled='no'/>
			<names width='140' enabled='yes'/>
			<values enabled='no'/>
			<header enabled="no" names='Name' values='Value'/>
		</legend>
	</type>

	<data>
{section name=loop loop=$chart_data}
		<block name="{$chart_data[loop].name}">
{assign var=temp value=$chart_data[loop].data}
		{section name=loop2 loop=$temp}
			<set value="{$temp[loop2].value}" name="{$temp[loop2].name}"{if $temp[loop2].color != ""} color="{$temp[loop2].color}"{/if} url="{$temp[loop2].url}" url_target="_self"/>
{/section}
		</block>
{/section}
	</data>

</root>