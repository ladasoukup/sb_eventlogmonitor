{include file="_page_start.tpl"}
<script language="JavaScript" type="text/javascript">
	PageUnloadWarningText = "##There are unsaved changes. If You will leave this page, all changes will be lost.##";
</script>
<table border="0" style="border: 0px none;">
<tr><td>
<h2>##Monitored computers##</h2>
</td><td style="width: 15px;">&nbsp;</td><td>
<select name="submenu_group" class="submenu_title" onchange="SwitchSubGroup(escape(this.options[this.selectedIndex].value), '{$smarty.server.REQUEST_URI|add_url_param:'f_group':'***SUB_GROUP***'}')">
{section name=loop loop=$groups_computer}
	<option value="{$groups_computer[loop]}"{if $groups_computer[loop] == $smarty.get.f_group} SELECTED{/if}>{$groups_computer[loop]|default:'##All groups##'}</option>
{/section}
</select>
</td></tr></table>

<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Computer##</td>
<td class="td_header">##Group##</td>
<td class="td_header">##Description## / ##Operating system##</td>
<td class="td_header" width="140">##Period##</td>
<td class="td_header" width="60">##Collect##</td>
<td class="td_header" width="120">##Next collect##</td>
<td class="td_header">##Delete##</td>
</tr>
{section name=loop loop=$s_computers}
<tr class="tr_data" id="tab_row_{$smarty.section.loop.index}">
<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="id[{$smarty.section.loop.index}]" value="{$s_computers[loop].id}" />
<input type="hidden" name="computer_name[{$smarty.section.loop.index}]" value="{$s_computers[loop].computer_name}" />
{$s_computers[loop].computer_name}<br />{$s_computers[loop].computer_domain|default:"---"}
</td>

<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="computer_group[{$smarty.section.loop.index}]" id="computer_group_{$smarty.section.loop.index}" value="{$s_computers[loop].computer_group}" />
<a href="#NEED-JavaScript" class="button_txt" style="display: block; width: 120px; text-align: left; overflow: hidden;" onClick="OpenWindow('index.php?module=settings&amp;sub=computers_groupsel&amp;id=computer_group_{$smarty.section.loop.index}&amp;tab_row_id=tab_row_{$smarty.section.loop.index}&amp;btn_submit_id=btn_submit', 'computer_group', 300, 300); return false;">
<span id="computer_group_{$smarty.section.loop.index}_label">{$s_computers[loop].computer_group|replace:'~~':''|replace:'~':', '|truncate:20:"...":true|default:"..."}</span>
</a>

</td>

<td class="td_data_v" style="font-weight: bold;">
{$s_computers[loop].computer_description|default:'---'|truncate:50}<br />
{$s_computers[loop].computer_os|default:'- ##UNKNOWN## -'}
</td>

<td class="td_data_v">
<input type="text" name="computer_harvest_interval[{$smarty.section.loop.index}]" value="{$s_computers[loop].computer_harvest_interval}" size="5" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
<select name="computer_harvest_interval_unit[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
<option label="##minutes##" value="60"{if $s_computers[loop].computer_harvest_interval_unit == "60"} SELECTED{/if}>##minutes##</option>
<option label="##hours##" value="3600"{if $s_computers[loop].computer_harvest_interval_unit == "3600"} SELECTED{/if}>##hours##</option>
<option label="##days##" value="86400"{if $s_computers[loop].computer_harvest_interval_unit == "86400"} SELECTED{/if}>##days##</option>
</select>
</td>
<td class="td_data_v">
<select name="computer_enable[{$smarty.section.loop.index}]" style="width: 150px;" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option value="0"{if $s_computers[loop].computer_enable == "0"} SELECTED{/if}>##Agent##</option>
	<option value="1"{if $s_computers[loop].computer_enable == "1"} SELECTED{/if}>##all##</option>
	<option value="2"{if $s_computers[loop].computer_enable == "2"} SELECTED{/if}>##Error##</option>
	<option value="3"{if $s_computers[loop].computer_enable == "3"} SELECTED{/if}>##Error##, ##Warning##</option>
	<option value="4"{if $s_computers[loop].computer_enable == "4"} SELECTED{/if}>##Error##, ##Warning##, ##Information##</option>
</select>
</td>

<td class="td_data_v">{$s_computers[loop].computer_next_harvest}</td>
  
<td class="td_data_v" align="right"><input type="checkbox" name="s_computer_delete[{$smarty.section.loop.index}]" value="YES"  onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);"/></td>

</tr>
{/section}

<tr><td class="td_data_v" colspan="7">&nbsp;</td></tr>
<!--NEW-->
<tr style="background: #FFDC7C;" id="tab_row_new">
<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="new_id" value="-NEW-" />
<input type="text" name="new_computer_name" value="" size="10" onChange="SettingsHighlight('tab_row_new', 'btn_submit', true);" />
</td>

<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="new_computer_group" id="new_computer_group" value="" />
<a href="#NEED-JavaScript" class="button_txt" onClick="OpenWindow('index.php?module=settings&amp;sub=computers_groupsel&amp;id=new_computer_group', 'computer_group', 300, 300); return false;">
<span id="new_computer_group_label">...</span>
</a>

</td>

<td class="td_data_v" style="font-weight: bold;">&nbsp;</td>

<td class="td_data_v">
<input type="text" name="new_computer_harvest_interval" value="60" size="5" />
<select name="new_computer_harvest_interval_unit">
<option label="##minutes##" value="60" SELECTED>##minutes##</option>
<option label="##hours##" value="3600">##hours##</option>
<option label="##days##" value="86400">##days##</option>
</select>
</td>

<td class="td_data_v" colspan="2">
<select name="new_computer_enable">
	<option value="0">##Agent##</option>
	<option value="1" SELECTED>##all##</option>
	<option value="2">##Error##</option>
	<option value="3">##Error##, ##Warning##</option>
	<option value="4">##Error##, ##Warning##, ##Information##</option>
</select>
</td>

<td class="td_data_v" align="right">&nbsp;</td>

</tr>
<!--NEW-->

<tr><td colspan="7" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" id="btn_submit" class="button_txt" name="OK" value="##Save changes##" onClick="WarningBeforePageLeave = false;" />
</td></tr>
</table>

</form>
{include file="_page_end.tpl"}