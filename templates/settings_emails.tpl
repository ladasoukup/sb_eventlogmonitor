{include file="_page_start.tpl"}
<h2>##Email queue##</h2>
<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Sender##</td>
<td class="td_header">##Recipient##</td>
<td class="td_header">##Subject##</td>
<td class="td_header">##Send after##</td>
<td class="td_header">##Priority##</td>
<td class="td_header">##Errors##</td>
<td class="td_header">##Email body##</td>
</tr>
{section name=loop loop=$mail_queue}
<tr class="tr_data">
<td class="td_data_v">{$mail_queue[loop].mail_from}</td>
<td class="td_data_v">{$mail_queue[loop].mail_to}</td>
<td class="td_data_v">{$mail_queue[loop].mail_subject}</td>
<td class="td_data_v">{$mail_queue[loop].mail_date}</td>
<td class="td_data_v">{$mail_queue[loop].mail_priority}</td>
<td class="td_data_v">{$mail_queue[loop].error_count}</td>
<td class="td_data_v">
<a href="#NEED-JavaScript" class="button_txt" onClick="OpenWindow('index.php?module=settings&amp;sub=emails_detail&amp;id={$mail_queue[loop].id}', 'emailsDetail', 790, 580); return false;">...</a>
</td>
</tr>
{/section}
</table>
{include file="_page_end.tpl"}