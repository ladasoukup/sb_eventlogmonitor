{include file="_page_start.tpl"}
<div style="font-size: 1px; height: 15px;">&nbsp;</div>

<div id="filtry" style="display: block;">
<form name="report" action="index.php">
<input type="hidden" name="module" value="{$smarty.get.module}" />
<input type="hidden" name="sub" value="{$smarty.get.sub}" />
<input type="hidden" name="sub_group" value="{$smarty.get.sub_group}" />
<table border="0" width="100%" style="border: 0px none #fff;">
<tr><td valign="top">
<table border="0" style="border: 0px none #fff;">
<tr>
<td class="form_title">##Event type##:</td>
<td>
<img src="templates_img/evt_error.gif" width="16" heigt="16" alt="error" /><input type="checkbox" name="f_evt_error" value="error" style="height: 20px;"{if $smarty.get.f_evt_error == "error"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_warning.gif" width="16" heigt="16" alt="warning" /><input type="checkbox" name="f_evt_warning" value="warning" style="height: 20px;"{if $smarty.get.f_evt_warning == "warning"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_audit_failure.gif" width="16" heigt="16" alt="audit_failure" /><input type="checkbox" name="f_evt_audit_failure" value="audit_failure" style="height: 20px;"{if $smarty.get.f_evt_audit_failure == "audit_failure"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_audit_success.gif" width="16" heigt="16" alt="audit_success" /><input type="checkbox" name="f_evt_audit_success" value="audit_success" style="height: 20px;"{if $smarty.get.f_evt_audit_success == "audit_success"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_information.gif" width="16" heigt="16" alt="information" /><input type="checkbox" name="f_evt_information" value="information" style="height: 20px;"{if $smarty.get.f_evt_information == "information"} CHECKED{/if} />&nbsp;&nbsp;
</td>
<td style="width: 12px;">&nbsp;</td>
<td class="form_title">##Group##:</td>
<td>
<select id="sub_group" name="sub_group" class="submenu_title" onchange="document.getElementById('evt_computer').value=''; report.submit(); return false;">
{section name=loop loop=$all_groups}
	<option value="{$all_groups[loop]}"{if $all_groups[loop] == $smarty.get.sub_group} SELECTED{/if}>{$all_groups[loop]|default:'##All groups##'}</option>
{/section}
</select>
</td>
</tr>
<tr>

<td class="form_title">##Time period##:</td>
<td>
<select name="time_period" class="submenu_title" onchange="report.submit(); return false;">
{section name=loop loop=$reports_time_periods}
	<option value="{$smarty.section.loop.index}"{if $smarty.section.loop.index == $smarty.get.time_period} SELECTED{/if}>{$reports_time_periods[loop]}</option>
{/section}
</select>
</td>
<td style="width: 12px;">&nbsp;</td>
<td class="form_title">##Computer##:</td>
<td>
<select id="evt_computer" name="evt_computer" class="submenu_title" onchange="report.submit(); return false;">
{section name=loop loop=$all_computers}
	<option value="{$all_computers[loop].sub}"{if $all_computers[loop].sub == $smarty.get.evt_computer} SELECTED{/if}>{$all_computers[loop].title|default:'##All computers##'}</option>
{/section}
</select>
</td>
</tr>
</table>
</td><td valign="bottom">
<div align="right"><a href="#" class="button_txt" onclick="report.submit(); return false;">- ##Apply filter## -</a></div>
</td></tr></table>
</form>
</div>

{anychart name="evt_type" width="1000" height="540" chart="StackedHorizontal3DColumn"}

{include file="_page_end.tpl"}
