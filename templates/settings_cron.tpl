{include file="_page_start.tpl"}
<script language="JavaScript" type="text/javascript">
	PageUnloadWarningText = "##There are unsaved changes. If You will leave this page, all changes will be lost.##";
</script>
<h2>##Scheduled tasks##</h2>
<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Task##</td>
<td class="td_header">##Parameters##</td>
<td class="td_header" width="140">##Period##</td>
<td class="td_header" width="60">##Enable##</td>
<td class="td_header">##Priority##</td>
<td class="td_header" width="120">##Next run##</td>
</tr>
{section name=loop loop=$s_cron}
<tr class="tr_data" id="tab_row_{$smarty.section.loop.index}">
<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="cron_title[{$smarty.section.loop.index}]" value="{$s_cron[loop].cron_title}" />
<input type="hidden" name="cron_module[{$smarty.section.loop.index}]" value="{$s_cron[loop].cron_module}" />
##{$s_cron[loop].cron_title}##
</td>

<td class="td_data_v">
<input type="text" name="cron_params[{$smarty.section.loop.index}]" value="{$s_cron[loop].cron_params}" size="15" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<input type="text" name="cron_repeat[{$smarty.section.loop.index}]" value="{$s_cron[loop].cron_repeat}" size="5" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
<select name="cron_repeat_unit[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option label="##minutes##" value="60"{if $s_cron[loop].cron_repeat_unit == "60"} SELECTED{/if}>##minutes##</option>
	<option label="##hours##" value="3600"{if $s_cron[loop].cron_repeat_unit == "3600"} SELECTED{/if}>##hours##</option>
	<option label="##days##" value="86400"{if $s_cron[loop].cron_repeat_unit == "86400"} SELECTED{/if}>##days##</option>
</select>
</td>

<td class="td_data_v">
<select name="cron_enable[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option value="0"{if $s_cron[loop].cron_enable == "0"} SELECTED{/if}>##NO##</option>
	<option value="1"{if $s_cron[loop].cron_enable == "1"} SELECTED{/if}>##YES##</option>
</select>
</td>

<td class="td_data_v">
<select name="cron_priority[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
{section name=loop2 loop=$priority}
	<option value="{$priority[loop2]}"{if $s_cron[loop].cron_priority == $priority[loop2]} SELECTED{/if}>{$priority[loop2]}</option>
{/section}
</select>
</td>

<td class="td_data_v">
<input type="text" name="cron_time[{$smarty.section.loop.index}]" value="{$s_cron[loop].cron_time}" size="18" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

</tr>
{/section}

<tr><td colspan="6" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" id="btn_submit" class="button_txt" name="OK" value="##Save changes##" onClick="WarningBeforePageLeave = false;" />
</td></tr>
</table>

</form>
{include file="_page_end.tpl"}