﻿<h1>Config globales</h1>
Vous pouvez paramétrer ici plusieurs valeurs globales.
<h2>Email</h2>
<h3>Serveur SMTP</h3>
<h3>Port SMTP</h3>
<h3>Authentification SMTP</h3>
<h3>Nom de compte SMTP</h3>
<h3>Mot de passe SMTP</h3>
<h3>Courrier-de</h3>
Adresse mail utilisée comme expéditeur des alertes.

<h2>Clef secrète client/serveur</h2>
<h3>Code permettant l'exécution de bat-prepare.php</h3>
Spécifier ici la clef utilisée dans le script <em>_run.bat</em> pour valider les appelants autorisés.
Cette clef doit être la même que celle des paramètres des scripts <em>cron.php</em> and <em>bat-prepare.php</em> postée par "<em>?secret=</em>" 