﻿<h1>Tâches programmées</h1>
<h2>Valeurs</h2>
<h3>Paramétres</h3>
Valeur dépendant du module. Vous pouvez trouver plus d'information, plus bas, pour chaque module.
<h3>Période</h3>
Combien de fois devrait être exécuté ce module. Vous pouvez le définir en minutes, heures ou jours.
<h3>Permettre</h3>
Permettre ou neutraliser le module spécifique.
<h3>Priorité</h3>
Ordre dans lequel sont exécutés les modules. La priorité <em>10</em> sera exécutée d'abord.
<h3>Prochain lancement</h3>
Quand le module sera exécuté. Vous pouvez changer cela manuellement, mais ce n'est pas recommandé.<br />
<em>Si vous le changer manuellement, garder svp ce format: YYYY-MM-DD hh:ii:ss (example: 2007-01-15 15:20:30)</em>

<h2>Tâches</h2>
<h3>Analyser les datas d'événement</h3>
Ce module est utilisé pour analyser des événements rassemblés (à l'aide du collecteur).<br />
<em>Pas de paramétres</em>

<h3>Envoyer les emails de la file d'attente</h3>
Ce module enverra des email de la file d'attente d'email.<br />
<em><strong>Paramétres</strong>: Le nombre d'email maximum à être envoyés.</em>

<h3>Supprimer les vieux événements</h3>
Ce module supprime "prune" les vieux événements de la base de donnée.<br />
<em><strong>Paramétres</strong>: Combien de jours restent t'ils dans la base.</em>

<h3>Supprimer les vieilles alertes</h3>
Ce module supprime "prune" les anciennes alertes de la base de donnée.<br />
<em><strong>Paramétres</strong>: Combien de jours restent t'ils dans la base.

<h3>Supprimer les vieilles alertes</h3>
Ce module est utilisé pour supprimer TOUS LES fichiers en cache. Le cache est supprimé quand il expire ou quand des données ont été changées (seulement les données, affectent le dossier de cache).
Bien que quelques fichiers puissent rester dans le cache tant qu'ils ne sont pas validés. Ce module supprimera TOUT dans le répertoire de cache.<br/>
<em>Pas de paramétres</em>

<h3>Optimiser les tables de MySQL</h3>
Ce module fera l'optimisation de la base de données. Parce que les données sont insérées et effacées, la base de données doit parfois être optimisée (cela peut l'accélérer). <br/>
em>Pas de paramétres</em>
