﻿<h1>Filtres d'alertes</h1>
Vous pouvez placer des règles de filtrage ici. Les règles sont employées pour filtrer des événements <strong>important</strong> ou <strong>Réparé</strong>
Hors de tous les événements rassemblés. 
<h3>Réparé</h3>
Les événements "Réparé", sont des événements que vous n'avez pas besoin de voir. Ces événements sont cachés dans la vue standard.
Noise is Events, that You don't need to see. These Events are hidden from standart views.
Vous pouvez les voir en clicquant la case <em>Montrer "Réparé"</em> sur la page <em>Jounal d'événements</em> ou sur la page <em>Alertes</em> page.
<h3>Alertes</h3>
L'alerte est un événement important. La copie de cet événement sera stocké dans une table spéciale et peut être regardé sur la page <em>Alertes</em>.<br/>
Les alertes peuvent être également expédié par mail. Indiquer l'<em>Email</em> et coché la case à côté du champ Email.
<h3>Une fois par</h3>
Pour empêcher la « saturation » de votre email, vous pouvez limiter le nombre d'alertes email.<br/>
Cette limite est fixée par la paire <strong>ordinateur et Filtres</strong>. Cette « limite » est employée seulement quand l'événement est respecté
cette règle sur cet ordinateur plus d'une fois dans le temps défini.<br/>
Vous obtiendrez le premier événement par email. Les prochains événements seront expédiés après la période DANS UN SEUL MESSAGE.<br/>
<em>S'appliquent seulement sur des alertes email. Les événements seront stockés sur les pages Journal d'événements et Alertes.</em>
<h3>Permettre</h3>
Vous pouvez provisoire neutraliser n'importe quelle règle.
<h3>Priorité</h3>
C'est IMPORTANT. les règles sont traitées de la priorité « 10 » à « 500 ». Quand n'importe quel événement respecte n'importe quelle règle, le traitement est stoppé<br />
<em>Si l'événement répond à plus d'une règle, seulement la première règle sera traité .</em>
<h3>Supprime</h3>
Si coché, la règle sera effacée.
<h3>bouton Export (XML)</h3>
Ceci exportera UNE règle dans un fichier XML. Ce fichier peut être importé sur N'IMPORTE QUEL moniteur de journal d'événement "SB EventLog Monitor" (même version ou supérieure).
<h3>Sauvegarde tous les filtres</h3>
Ceci exportera TOUTES LES règles (filtres) dans un fichier XML. Ce fichier peut être importé sur N'IMPORTE QUEL moniteur de journal d'événement "SB EventLog Monitor" (même version ou supérieure).
<h3>Importation des règles</h3>
Vous pouvez choisir n'importe quel fichier XML exporté de la même version (incluant les anciennes et la majeure partie des futures) du moniteur d'EventLog
et importer celles ci. Oui… Vous pouvez importer une règle simple ou toutes (plus d'une règles à la fois).
<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=filters_detail">Règles (... boutton)</a></h3>
C'est le bouton le plus important à cette page (à droite après sauver les changements). Ceci ouvrira l'editeur de règle -
Vous pouvez indiquer la règle. Vous pouvez lire plus à ce sujet sur <a href="index.php?module=help&hlp_main=settings&hlp_sub=filters_detail">Page "Création de règles"</a>.
