﻿<h1>Paramètres</h1>
<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=computers">Ordinateurs surveillés</a></h3>
Définir les ordinateurs surveillés.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=filters">Filtres d’alertes</a></h3>
Création des filtres contrôlés utilisés pour vous alerter (Repère /email) au sujet des événements importants. 

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=global">Config Globales</a></h3>
Les config globales comme les options SMTP, ...

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=cron">Tâches programmées</a></h3>
Indiquer la période ou une tâche spéciale (pruning, analyse, email…) sera exécuté. 

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=users">Utilisateurs & Mots de passe</a></h3>
Management des utilisateurs et de leurs mots de passe. 

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=emails">File d'attente d'email</a></h3>
Voir les emails en attente d’envoi. 

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=alertqueue">File d'attente d’alerte</a></h3>
Voir toute la file d'attente des emails d’alerte. C'est la file d'attente spéciale employée par le dispositif "UNE FOIS PAR".
Lire plus à ce sujet dans la section "<a href="index.php?module=help&hlp_main=settings&hlp_sub=filters">filtres d’alertes</a>" section.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=rpc">Clef secrète client/serveur </a></h3>
Voir toutes les clefs de communication pour des machines utilisant le service Windows pour envoyer les événements.
