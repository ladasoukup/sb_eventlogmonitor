﻿<h1>Ordinateurs surveillés</h1>
Vous pouvez indiquer ici quels ordinateurs devraient être surveillés et comment.
<h2>Il y a peu de champs qui peuvent être modifiés :</h2>
<h3>Ordinateur</h3>
<em>Peut être seulement modifié en ajoutant le nouvel ordinateur </em><br />
Indiquer ici le nom de l'ordinateur à surveiller. Veuillez employer les noms NETBIOS si possible. Vous pouvez également employer les noms pleinement qualifiés (FQDN).<br />
exemple: "<em>desktop-05</em>" ou "<em>my-server.example.com</em>"
<h3>Groupes</h3>	
Vous pouvez ajouter l'ordinateur à n'importe quel groupe. Des groupes peuvent être employés dans <a href="index.php?module=help&hlp_main=settings&hlp_sub=filters">Filtres d'alerte</a>
et vous pouvez également les employer à la page de « Journal d'événements » pour regarder seulement les ordinateurs qui sont membres d'un groupe spécifique.<br />
exemple: "<em>DC</em>", "<em>WEB-SERVER</em>" ou "<em>VPN</em>".
<h3>Période</h3>
Indiquer la période de collecte des événements. Vous pouvez employer des minutes, des heures ou des jours.<br />
<em>Cette valeur est seulement employée quand les événements sont rassemblés par le script du serveur (TIRÉ).</em>
<h3>Collecte</h3>
Vous pouvez choisir comment les événements de la machine cible seront collectés . Il y a deux manières… « Tiré » ou « poussé ». <br />
Tiré - les événements sont collectés par le script VBS du serveur.<br />
Poussé - les événements sont collectés par le service Windows s'éxecutant sur les ordinateurs cible.<br />
<strong>Si vous installez le service Windows sur n'importe quel ordinateur qui est déjà surveillé, cette valeur sera changée en « poussé » quand le script RPC recevra n'importe quelles données du service Windows.</strong><br />
<em>Vous pouvez définir à « poussé » pour cesser de collecté par le serveur.</em>
<h3>Supprime</h3>
Coché ceci si vous voulez supprimer l'ordinateur cible et ses événements. Les alertes ne seront pas supprimées. 

<h2>Il y a également plusieurs champs « inaltérables » :</h2>
<h3>Systèmes d'exploitation</h3>
Cet OS a été détecté sur l'ordinateur surveillé. Les événements seront collectés.
<h3>Prochaine collecte</h3>
Ceci indique quand les événements seront collectés à partir de l'ordinateur cible.
