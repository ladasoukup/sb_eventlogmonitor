﻿<h1>Filtres d'alertes - Editeur de règle</h1>
Dans cette fenêtre, vous pouvez paramétrer une ou plusieurs règles.
<h3>ET / OU</h3>
<em>ET</em> - cette "ET" dernière ligne doit être VRAI (tous les deux)<br />
<em>OU</em> - cette "OU" dernière ligne doit être VRAI<br /><br />
Il peux y avoir plusieurs<em>OU</em> dans une règle. Seulement une des conditions dans la ligne doit être VRAI.
<strong>Quand tu emploies "ET", la ligne est "cassée".</strong><br />
<em>1=1 ET 5=5 OU 5=4 OU 6=3 ET 7=3 OU 3=3</em> sont VRAI<br />
<code style="width: 300px;">1=1 - Vrai<br />
5=5 OR 5=4 OR 6=3 - Vrai (5=5)<br />
7=3 OR 3=3 - Vrai (3=3)</code>
Tout les éléments sont VRAI, alors le résultat est VRAI

<h3>-CHOSIR- (second champ)</h3>
Choisissez ce que vous voulez observer dans les événements.<br />
<em>Ordinateur</em> - Nom d'ordinateur name<br />
<em>Groupe</em> - Groupe d'ordinateurs<br />
<em>Code d'événement</em> - Code de l'événement<br />
<em>Type d'événement</em> - Type de l'événment - <strong>information</strong>, <strong>error</strong>, <strong>warning</strong>, <strong>audit_success</strong>, <strong>audit_failure</strong><br />
<em>Catégorie d'événement</em> - <br />
<em>Event log</em> - <br />
<em>Description</em> - Description de l'événement<br />
<em>Source d'événement</em> - Source d'événement<br />
<em>Utilisateur</em> - Nom d'utilisateur<br />

<h3>Comparaison</h3>
Vous pouvez choisir différentes méthodes de comparaison...<br />
<em>est égal / n'est pas égal</em> - Il doit y avoir correspondance totale<br />
<em>is like / is not like</em> - Il doit y avoir correspondance partielle<br />
<em>regex / preg_match</em> - Vous pouvez aussi utiliser les modèles <em>regex</em> ou <em>preg_match</em>.

<h3>-Valeur à comparer-</h3>
Entrez ici la valeur à comparer avec le champ "<em>-CHOSIR- (second champ)</em>" en utilisant la méthode de "<em>Comparaison</em>" choisie.

<h3>Boutton AJOUTER UNE AUTRE REGLE</h3>
Ajoute une nouvelle ligne pour cette règle ;)

<h3>Flêches Haut / Bas</h3>
Déplace la ligne vers le haut ou vers le bas. Cela est utile lorsque vous souhaitez changer l'ordre des conditions.

<h3>Supprimer (X Rouge)</h3>
Supprimer la ligne.