<h1>Summary page</h1>
You can see a quick overview of all collected events and alerts. All values and graphs
can be clicked on for more information and will take you to EventLog or Alerts page and will set pre-defined filters for you.

<h2>Summary - EventLog</h2>
This is overview of all events divided by event Type (error, warning, information, audit success and audit failure).<br />
For each event Type you can view the TOP 7 computers. You may limit this view by time (last 12 hours, today, yesterday, two days ago, last week and all).

<h2>Summary - Alerts</h2>
The same overview as for EventLog, but here you can see only Alerts (events that matched to alert filter rule).

<h2>Last alerts</h2>
List of last alerts. Only alerts for last 48 hours are shown and only one per each monitored computer.