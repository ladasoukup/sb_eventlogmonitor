<h1>Monitored computers</h1>
You can specify here what computers should be monitored and how.
<h2>There are few fields that can be modified:</h2>
<h3>Computer</h3>
<em>Can be only modified when adding new computer</em><br />
Specify here name of computer to monitor. When applicable, please use NETBIOS names. you can also use fully qualified domain names (FQDN).<br />
example: "<em>desktop-05</em>" or "<em>my-server.example.com</em>"

<h3>Group</h3>
A computer can be a member of one or more <a href="index.php?module=help&hlp_main=settings&hlp_sub=groups_computer">Computer groups</a>. Computer groups can be used in <a href="index.php?module=help&hlp_main=settings&hlp_sub=filters">Alert filters</a>
as well as the "EventLog" page to view the members of that group.<br />

<h3>Period</h3>
Specify period or interval between event collections. you can use minutes, hours or days. you can specify a value of "0" (zero) if you want collection to occur every time the collection script is run.<br />
<em>This value is only used when events are collected by server-side script (all except Agent).</em>

<h3>Collect</h3>
You can select how and what type of events will be collected from the target machine.<br />
<ul>
<li><strong>Agent</strong> - events are collected by "EventLog Monitor agent" installed on the remote computer</li>
<li><strong>all</strong> - events are collected locally by VB/WMI script - all events are collected</li>
<li><strong>Error</strong> - events are collected locally by VB/WMI script - only Errors are collected</li>
<li><strong>Error, Warning</strong> - events are collected locally by VB/WMI script - only Errors and Warnings are collected</li>
<li><strong>Error, Warning, Information</strong> - events are collected locally by VB/WMI script - Errors, Warnings and Information are collected</li>
</ul>
<strong>If you install the Agent (Windows service) to any computer that is already monitored, this value will be changed to "Agent" when the RPC script receives any data from Agent.</strong><br />
<em>You can set this to "Agent" to stop collecting by server-side collector (VB/WMI script).</em>

<h3>Delete</h3>
Check this if you want to delete target computer and it's events. Alerts will NOT be deleted.

<h2>There are also several "read-only" fields:</h2>
<h3>Description / Operating system</h3>
Computer description as defined by the target computer (windows computer description).<br />
OS Version that was detected on the target computer.
<h3>Next collect</h3>
This specifies the next time events will be collected from the target computer (When using server-side collector).
