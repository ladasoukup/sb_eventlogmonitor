<h1>Global config</h1>
You can set several "global" values here.
<h2>E-mail</h2>
<h3>SMTP server host</h3>
<h3>SMTP server port</h3>
<h3>SMTP authentication</h3>
<h3>SMTP username</h3>
<h3>SMTP password</h3>
<h3>Mail-from</h3>
E-mail address used as sender of Alert e-mails.

<h2>Passcodes and Communication keys</h2>
<h3>Passcode allowing execution of bat-prepare.php</h3>
Specify here PASSCODE used in <em>_run.bat</em> script to validate authorized "callers".
This Passcode must be same as the Passcode posted as "<em>?secret=</em>" parameter for <em>cron.php</em> and <em>bat-prepare.php</em> scripts.