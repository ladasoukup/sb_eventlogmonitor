<h1>EventLog</h1>
This is the most important part of {$app_name} as all collected events can be found here.
You can view all events from all computers, group of computers, or individual computers on one page. This page allows you click on any event in order to view the 
event detail.<br />
All events have an icons and color highlight to associate them with a specific event type.<br />

<h2>Sorting and Filtering</h2>
For better orientation in events, you can sort them and filter them.
<h3>Sorting</h3>
You can sort events by clicking on any column name. Clicking on the same column will change sort order from ascending to descending and vice versa.
<h3>Filtering - groups / computers</h3>
In the cases where you would like to see events from only one computer or from only one computer group, you can choose either within the "Group" and "Computer" dropdowns. 
This option is on the left side (right below the menu).<br />
When you select a specific Group you will see events only from computers that belongs to this group.
This selection limits the Computer within the "Computer" dropdown. Selecting a single computer when a group is specified (or otherwise) will show events for that Computer<br />
When you select any Computer, you will only see events from this Computer.
<h3>Filtering - more</h3>
You may want to set more filters than just Group or Computer... Event filtering can be done by clicking on the "Filters" link above the EventLog column headers.<br />
<ul>
<li><strong>Event type</strong> - limit view to specific event types</li>
<li><strong>Date from and Date to</strong> - limit view to only selected period of time</li>
<li><strong>Show "Noise"</strong> - If checked, view events categorized as Noise(*) (By Alert filter rules)</li>
<li><strong>Event ID</strong> - limit view to events by EventID (0-65536)</li>
<li><strong>Event category</strong> - limit view to events by Event Category (ex. Server,Account Logon, etc)</li>
<li><strong>Event source</strong> - limit view to events by Event Source (ex. NTBackup, W3WVC, etc)</li>
<li><strong>User</strong> - events "caused" by this user</li>
</ul>
There is AutoComplete function for event ID, event category, event source and User.
AutoComplete will read all unique values from the whole events database and will try to help you
with correct spelling and/or suggesting a value.

<br /><br /><em>(*) Noise are events, that you want to ignore (ex. Wrong VNC protocol version, ...) </em>