<h1>Access control</h1>
You can Allow / Deny access to specific Computer groups for each User group.<br />
When you allow access to "All computers", all other checkboxes (for this user group) will be checked after you save the settings.<br /><br />
Each User group can access (view) only computers that are members of Allowed Computer group.<br />
