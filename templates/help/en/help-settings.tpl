<h1>Settings</h1>
<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=computers">Monitored computers</a></h3>
You can specify computers that you want to monitor.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=groups_computer">Computer groups</a></h3>
You can specify computer groups here, computers can belong to multiple groups.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=users">Users & Passwords</a></h3>
You can manage user accounts here.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=groups_user">User groups</a></h3>
You can manage user group access here.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=groups_access">Access control</a></h3>
You can manage group access rights to computer groups.


<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=filters">Alert filters</a></h3>
You can create and manage filters used for alerting (web / e-mail) you about important events.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=global">Global config</a></h3>
Manage global config like SMTP options, batch passcodes ...

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=cron">Scheduled tasks</a></h3>
You can specify how often special tasks (pruning, parsing, etc.) are executed.


<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=e-mails">E-mail queue</a></h3>
You can view all queued e-mail here (mail that has not been sent).

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=alertqueue">Alert queue</a></h3>
You can see all queue alert e-mail here. This is special queue used by "ONCE PER" feature.
You can read more about this in "<a href="index.php?module=help&hlp_main=settings&hlp_sub=filters">Alert filters</a>" section.

<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=rpc">RPC secrets</a></h3>
You can see all communication keys for machines using Windows service to post events.
