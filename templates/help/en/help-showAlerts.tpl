<h1>Alerts</h1>
This section looks like the <a href="index.php?module=help&hlp_main=showEventLog&hlp_sub=">EventLog</a>.<br />
There are two changes:
<ol>
<li>This page show you only Alerts (events, that matched to Alert filter rule)</li>
<li>Instead of "Noise", you can find here "Repaired"</li>
</ol>
<h2>Repaired</h2>
Since Alerts are the same as events (just matches to an Alert filter), the status of "Repaired" is used as opposed to noise.
This setting will hide the Alert from the default view.
If you want to see all Alerts, you have to check Show "Repaired" option (on the filters panel).<br />
You can mark Alert as repaired be clicking on the "Mark as Repaired" button in the Alert detail windows.