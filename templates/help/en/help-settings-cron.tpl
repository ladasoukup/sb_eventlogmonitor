<h1>Scheduled tasks</h1>
<h2>Values</h2>
<h3>Parameters</h3>
Module dependent value. you can find more info later for each module.
<h3>Period</h3>
How often should be this module executed. you can set it in minutes, hours or days.
<h3>Enable</h3>
Enable or disable specific module.
<h3>Priority</h3>
Order in which are modules executed. Priority <em>10</em> will be executed first.
<h3>Next run</h3>
The next time the module will be executed. you can change this time manually, but it is not recommended.<br />
<em>If you will set it manually, please keep this format: YYYY-MM-DD hh:ii:ss (example: 2007-01-15 15:20:30)</em>

<h2>Tasks</h2>
<h3>Parse event dates</h3>
This module is used to parse collected events (using server-side collector).<br />
<em>No parameters here</em>

<h3>Send e-mail from queue</h3>
This module will send e-mail from e-mail queue.<br />
<em><strong>Parameters</strong>: number of maximum e-mails to be send.</em>

<h3>Delete old events</h3>
This module will "prune" old events from database.<br />
<em><strong>Parameters</strong>: How many days should be left in DB.</em>

<h3>Delete old alerts</h3>
This module will "prune" old Alerts from database<br />
<em><strong>Parameters</strong>: How many days should be left in DB.</em>

<h3>Clear cached files</h3>
This module is used to delete ALL cached files. Cache is deleted when it expires or when data has been changed (only data, that affect the cache file).
Although some files may stay in cache even they are not valid. this module will delete EVERYTHING in cache directory.<br />
<em>No parameters here</em>

<h3>Optimize MySQL tables</h3>
This module will perform database optimization. This is run to reclaim the unused space and to defragment the data file (it can speed-up database). 
It is not likely that you need to do this more than once a week or month. <strong>Note that the database locks the table during the time Optimize is running.</strong><br />
<em>No parameters here</em>
