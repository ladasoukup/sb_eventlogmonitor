<h1>Alert filters</h1>
You can set filtering rules here. Rules are used to filter <strong>important</strong> or <strong>noise</strong>
eventsout of all collected events.
<h3>Noise</h3>
Use this filter when you don't want to see certain events. These events are hidden from standard views and 
can viewed by checking <em>Show "Noise"</em> on <em>EventLog</em> or <em>Alerts</em> page.
<h3>Alert</h3>
Use this filter when you want to define an important Event. A copy of this event will be stored in special table and can be viewed on <em>Alerts</em> page.<br />
Alerts can be also mailed to you, just specify <em>e-mail</em> and select the checkbox next to the e-mail field.
<h3>Once per</h3>
This is used to prevent e-mail "flooding" and you can limit number of e-mail alerts.<br />
This limit is set to <strong>Computer and Filter</strong> pair. This "limit" is used only when the event matched
this rule on this computer more then one time in this time period.<br />
You will receive only first event via e-mail, all events proceeding will be mailed after this period of time IN ONE MESSAGE.<br />
<em>This only applies to e-mail alerts and events can be viewed within the EventLog and Alerts pages as usual.</em>
<h3>Enable</h3>
You can disable / enable any rules here.
<h3>Priority</h3>
This is IMPORTANT- Rules are processed from Priority "10" to "500". When an event matches to any rule, processing is STOPPED.<br />
<em>If event can be matched to more then one rule, only the first rule takes precedence..</em>
<h3>Delete</h3>
When checked, rule(s) will be deleted.
<h3>Export button (XML)</h3>
This will save ONE rule as XML file. This file can be imported back to ANY EventLog Monitor (same or greater version).
<h3>Backup all filters</h3>
This will save ALL rules (filters) into ONE XML file. This file can be imported back to ANY EventLog Monitor (same or greater version).
<h3>Import rules</h3>
You can select any XML file exported from the same version (including older and most of the future) of EventLog Monitor
and import it / them back. You can also import single rule or the full backup (more rules at once).
<h3><a href="index.php?module=help&hlp_main=settings&hlp_sub=filters_detail">RULE (... button)</a></h3>
This is the most important button on this page (right after Save changes). This will open the Rule editor where you can specify the rules.
You can read more about this on the <a href="index.php?module=help&hlp_main=settings&hlp_sub=filters_detail">"Rule creation" page</a>.
