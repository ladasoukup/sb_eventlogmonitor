<h1>Computer groups</h1>
You can create or delete Computer groups here.<br />
Computers can be assigned to one or more Groups on the <a href="index.php?module=help&hlp_main=settings&hlp_sub=computers">Monitored computers</a> page.