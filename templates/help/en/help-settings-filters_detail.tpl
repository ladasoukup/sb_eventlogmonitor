<h1>Alert filters - Rule editor</h1>
In this window, you can setup / manage all rule conditions.
<h3>And / Or</h3>
<em>AND</em> - this and last line must be TRUE (both)<br />
<em>OR</em> - this OR last line must be TRUE.<br /><br />
You can have more then one <em>OR</em> conditions in a rule, only one of conditions in the line must be TRUE for this rule to match the filter.
<strong>When you use AND, the line is completed.</strong><br />
<em>1=1 AND 5=5 OR 5=4 OR 6=3 AND 7=3 OR 3=3</em> WILL BE TRUE<br />
<code style="width: 300px;">1=1 - true<br />
5=5 OR 5=4 OR 6=3 - true (5=5)<br />
7=3 OR 3=3 - true (3=3)</code>
All "block" are TRUE, so the result is TRUE

<h3>-SELECT- (second field)</h3>
Select what you want to check in the Event.<br />
<em>Computer</em> - computer name<br />
<em>Group</em> - computer group<br />
<em>Event code</em> - Code of Event<br />
<em>Event type</em> - Type of event - <strong>information</strong>, <strong>error</strong>, <strong>warning</strong>, <strong>audit_success</strong>, <strong>audit_failure</strong><br />
<em>Event category</em> - <br />
<em>Event log</em> - <br />
<em>Description</em> - Description of Event<br />
<em>Event source</em> - Source application<br />
<em>User</em> - User name<br />

<h3>Comparison</h3>
You can select several Comparisons methods...<br />
<em>is equal / is not equal</em> - there must be FULL match<br />
<em>is like / is not like</em> - only partial match is enough<br />
<em>regex / preg_match</em> - you can also use <em>regex</em> or <em>preg_match</em> patterns.

<h3>-VALUE TO COMPARE-</h3>
Select the value that you want compare: "<em>-SELECT- (second field)</em>" using "<em>Comparison</em>" method you have selected.

<h3>Add another rule button</h3>
Just add new line for this rule

<h3>up / down arrows</h3>
Move the line up or down by one line. This is to change the order of conditions.

<h3>Delete (red X)</h3>
Delete this line.