<h1>User groups</h1>
You can cretae or delete User groups here. You can assign user to groups on the 
<a href="index.php?module=help&hlp_main=settings&hlp_sub=users">Users & Passwords</a> page.
<h3>Basic Access control</h3>
You can also set basic access rights for each group.
<ul>
<li><strong>Summary</strong> - Allow / Deny access to the Summary page</li>
<li><strong>EventLog</strong> - Allow / Deny access to the EventLog page</li>
<li><strong>Alerts</strong> - Allow / Deny access to the Alert page</li>
<li><strong>Settings</strong> - Allow / Deny access to the Settings page</li>
</ul>
More access rights can be set on the <a href="index.php?module=help&hlp_main=settings&hlp_sub=group_access">Access control</a> page.