{include file="_page_start.tpl"}
<script language="JavaScript" type="text/javascript">
	PageUnloadWarningText = "##There are unsaved changes. If You will leave this page, all changes will be lost.##";
</script>
<h2>##User groups##</h2>
<form action="{$smarty.server.REQUEST_URI}" method="post">
<table border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header">##Group##</td>
<td class="td_header">##Summary##</td>
<td class="td_header">##EventLog##</td>
<td class="td_header">##Alerts##</td>
<td class="td_header">##Settings##</td>
<td class="td_header">##Delete##</td>

</tr>
{section name=loop loop=$groups_user}
<tr class="tr_data" id="tab_row_{$smarty.section.loop.index}">
<td class="td_data_v" style="font-weight: bold;">
<input type="hidden" name="id[{$smarty.section.loop.index}]" value="{$groups_user[loop].id}" />
<input type="text" name="user_group[{$smarty.section.loop.index}]" value="{$groups_user[loop].user_group}" size="20" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<select name="access_summary[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option value="0"{if $groups_user[loop].access_summary == "0"} SELECTED{/if}>##Denied##</option>
	<option value="100"{if $groups_user[loop].access_summary == "100"} SELECTED{/if}>##Allowed##</option>
</select>
</td>

<td class="td_data_v">
<select name="access_events[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option value="0"{if $groups_user[loop].access_events == "0"} SELECTED{/if}>##Denied##</option>
	<option value="100"{if $groups_user[loop].access_events == "100"} SELECTED{/if}>##Allowed##</option>
</select>
</td>

<td class="td_data_v">
<select name="access_alerts[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option value="0"{if $groups_user[loop].access_alerts == "0"} SELECTED{/if}>##Denied##</option>
	<option value="100"{if $groups_user[loop].access_alerts == "100"} SELECTED{/if}>##Allowed##</option>
</select>
</td>

<td class="td_data_v">
<select name="access_settings[{$smarty.section.loop.index}]" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);">
	<option value="0"{if $groups_user[loop].access_settings == "0"} SELECTED{/if}>##Denied##</option>
	<option value="100"{if $groups_user[loop].access_settings == "100"} SELECTED{/if}>##Allowed##</option>
</select>
</td>

<td class="td_data_v" align="right"><input type="checkbox" name="groups_user_delete[{$smarty.section.loop.index}]" value="YES" onChange="SettingsHighlight('tab_row_{$smarty.section.loop.index}', 'btn_submit', true);" /></td>
</tr>
{/section}

<tr><td class="td_data_v" colspan="6">&nbsp;</td></tr>
<!--NEW-->
<tr style="background: #FFDC7C;" id="tab_row_new">
<td class="td_data_v" style="font-weight: bold;">
<input type="text" name="new_user_group" value="" size="20" onChange="SettingsHighlight('tab_row_new', 'btn_submit', true);" />
</td>

<td class="td_data_v">
<select name="new_access_summary">
	<option value="0">##Denied##</option>
	<option value="100" SELECTED>##Allowed##</option>
</select>
</td>

<td class="td_data_v">
<select name="new_access_events">
	<option value="0">##Denied##</option>
	<option value="100" SELECTED>##Allowed##</option>
</select>
</td>

<td class="td_data_v">
<select name="new_access_alerts">
	<option value="0">##Denied##</option>
	<option value="100" SELECTED>##Allowed##</option>
</select>
</td>

<td class="td_data_v">
<select name="new_access_settings">
	<option value="0" SELECTED>##Denied##</option>
	<option value="100">##Allowed##</option>
</select>
</td>

<td class="td_data_v">&nbsp;</td>

</tr>
<!--NEW-->

<tr><td colspan="6" class="td_data" style="text-align: center; padding-top: 10px; padding-bottom: 10px;">
<input type="submit" id="btn_submit" class="button_txt" name="OK" value="##Save changes##" onClick="WarningBeforePageLeave = false;" />
</td></tr>
</table>

</form>
{include file="_page_end.tpl"}