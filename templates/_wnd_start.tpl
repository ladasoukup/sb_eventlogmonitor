<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$langID}" lang="{$langID}">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset={$smarty.const.PageCharSet}" />
		<meta http-equiv="content-language" content="{$langID}" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="description" content="{$app_name} - Web GUI" />
		<meta name="robots" content="NONE" />
		<link rel="icon" href="favicon.gif" />
		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="STYLESHEET" type="text/css" href="core.css" />
		<link rel="STYLESHEET" type="text/css" href="core_table.css" />
		<link rel="STYLESHEET" type="text/css" media="print" href="print.css" />
		<script src="core.js" type="text/javascript"></script>
		<title>{$app_name} {$version}</title>
	</head>
<body style="overflow: auto;">

