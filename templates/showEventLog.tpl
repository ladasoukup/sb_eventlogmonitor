{include file="_page_start.tpl"}
<script language="javascript" type="text/javascript" src="core_actb.js"></script>
<script language="JavaScript" type="text/javascript">
{literal}
function div_filtry_showhide(){
	if (document.getElementById('show_filter').value == '1'){
		document.getElementById('filtry').style.display = 'none';
		document.getElementById('show_filter').value = '0';
	} else {
		document.getElementById('filtry').style.display = 'block';
		document.getElementById('show_filter').value = '1';
	}
}
{/literal}
// AutoComplete INIT...
var actb_evt_code = new Array({$actb_evt_code});
var actb_evt_category = new Array({$actb_evt_category});
var actb_evt_source = new Array({$actb_evt_source});
var actb_evt_user = new Array({$actb_evt_user});
// Calendar INIT...
var JS_TXT_TODAY = "##today##";
var JS_TXT_Days = new Array('##Sunday##','##Monday##','##Tuesday##','##Wednesday##','##Thursday##','##Friday##','##Saturday##','##Sun##','##Mon##','##Tue##','##Wed##','##Thu##','##Fri##','##Sat##');
var JS_TXT_Months = new Array('##January##','##February##','##March##','##April##','##May##','##June##','##July##','##August##','##September##','##October##','##November##','##December##','##Jan##','##Feb##','##Mar##','##Apr##','##May##','##Jun##','##Jul##','##Aug##','##Sep##','##Oct##','##Nov##','##Dec##');
var JS_TXT_UshortDays = new Array('##us_Sun##','##us_Mon##','##us_Tue##','##us_Wed##','##us_Thu##','##us_Fri##','##us_Sun##');
var JS_TXT_LongMonths = new Array('##January##','##February##','##March##','##April##','##May##','##June##','##July##','##August##','##September##','##October##','##November##','##December##');
var JS_TXT_ShortMonths = new Array('##s_Jan##','##s_Feb##','##s_Mar##','##s_Apr##','##s_May##','##s_Jun##','##s_Jul##','##s_Aug##','##s_Sep##','##s_Oct##','##s_Nov##','##s_Dec##');
</script>
<script src="core_CalendarPopup.js" type="text/javascript"></script>
<script language="JavaScript">var cal1 = new CalendarPopup("pcaldiv1");</script>

<div id="pcaldiv1" style="position:absolute; visibility:hidden; background-color:#fff;"></div>
<a href="#" class="filtry" onclick="div_filtry_showhide(); return false;">##Filters##</a>
<div id="filtry" {if $smarty.get.show_filter == "1"}style="display: block;"{else}style="display: none;"{/if}>
<form name="frm_filtry" action="index.php">
<input type="hidden" name="module" value="{$smarty.get.module}" />
<input type="hidden" name="sub" value="{$smarty.get.sub}" />
<input type="hidden" name="sub_group" value="{$smarty.get.sub_group}" />
<!--<input type="hidden" name="from" value="{$smarty.get.from}" />-->
<input type="hidden" id="order" name="order" value="{$smarty.get.order}" />
<input type="hidden" id="sort" name="sort" value="{$smarty.get.sort}" />
<input type="hidden" id="show_filter" name="show_filter" value="{$smarty.get.show_filter}" />

<table border="0" style="border: 0px none #fff;">
<tr>
<td class="form_title">##Event type##:</td>
<td>
<img src="templates_img/evt_error.gif" width="16" heigt="16" alt="error" /><input type="checkbox" name="f_evt_error" value="error" style="height: 20px;"{if $smarty.get.f_evt_error == "error"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_warning.gif" width="16" heigt="16" alt="warning" /><input type="checkbox" name="f_evt_warning" value="warning" style="height: 20px;"{if $smarty.get.f_evt_warning == "warning"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_audit_failure.gif" width="16" heigt="16" alt="audit_failure" /><input type="checkbox" name="f_evt_audit_failure" value="audit_failure" style="height: 20px;"{if $smarty.get.f_evt_audit_failure == "audit_failure"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_audit_success.gif" width="16" heigt="16" alt="audit_success" /><input type="checkbox" name="f_evt_audit_success" value="audit_success" style="height: 20px;"{if $smarty.get.f_evt_audit_success == "audit_success"} CHECKED{/if} />&nbsp;&nbsp;
<img src="templates_img/evt_information.gif" width="16" heigt="16" alt="information" /><input type="checkbox" name="f_evt_information" value="information" style="height: 20px;"{if $smarty.get.f_evt_information == "information"} CHECKED{/if} />&nbsp;&nbsp;
</td>
<td style="width: 25px;">&nbsp;</td>
<td class="form_title">##Event code##:</td>
<td><input type="text" name="f_evt_code" value="{$smarty.get.f_evt_code}" size="40" onfocus="actb(this, event, actb_evt_code);" autocomplete="off" /></td>
</tr>

<tr>
<td class="form_title">##Date from##:</td>
<td><input type="text" name="f_date_from" value="{$smarty.get.f_date_from}" />
<a href="#" class="button_txt" onClick="cal1.select(document.forms['frm_filtry'].f_date_from,'date_from','yyyy-MM-dd'); return false;" name="date_from" id="date_from">...</a>
</td>
<td style="width: 25px;">&nbsp;</td>
<td class="form_title">##Event category##:</td>
<td><input type="text" name="f_evt_category" value="{$smarty.get.f_evt_category}" size="40" onfocus="actb(this, event, actb_evt_category);" autocomplete="off" /></td>
</tr>

<tr>
<td class="form_title">##Date to##:</td>
<td><input type="text" name="f_date_to" value="{$smarty.get.f_date_to}" />
<a href="#" class="button_txt" onClick="cal1.select(document.forms['frm_filtry'].f_date_to,'date_to','yyyy-MM-dd'); return false;" name="date_to" id="date_to">...</a>
</td>
<td style="width: 25px;">&nbsp;</td>
<td class="form_title">##Event source##:</td>
<td><input type="text" name="f_evt_source" value="{$smarty.get.f_evt_source}" size="40" onfocus="actb(this, event, actb_evt_source);" autocomplete="off" /></td>
</tr>

<tr>
<td class="form_title">
<td><input type="checkbox" name="f_evt_noise" value="True" style=""{if $smarty.get.f_evt_noise == "True"} CHECKED{/if} />&nbsp;{if $smarty.get.module=="showAlerts"}##Show "Repaired"##{else}##Show "Noise"##{/if}</td>
</td>
<td style="width: 25px;">&nbsp;</td>
<td class="form_title">##User##:</td>
<td><input type="text" name="f_evt_user" value="{$smarty.get.f_evt_user}" size="40" onfocus="actb(this, event, actb_evt_user);" autocomplete="off" /></td>
</tr>

</table>
</form>
<div align="right">
<a href="#" class="button_txt" onclick="frm_filtry.submit(); return false;">- ##Apply filter## -</a>
<a href="#" class="button_txt" onclick="document.getElementById('show_filter').value = ''; frm_filtry.submit(); return false;">- ##Hide## -</a>
</div>
</div>

<table width="98%" border="0" cellpadding="0" cellspacing="0">
<tr class="tr_header" style="text-transform: uppercase;">
<td class="td_header col_ico">i</td>
{section name=loop loop=$event_table_head}
{if $smarty.get.order == $event_table_head[loop].db_col AND $smarty.get.sort == "DESC"}
<td class="td_header btn_sort" OnClick="document.getElementById('order').value = '{$event_table_head[loop].db_col}'; document.getElementById('sort').value = 'ASC'; frm_filtry.submit();">
{else}
<td class="td_header btn_sort" OnClick="document.getElementById('order').value = '{$event_table_head[loop].db_col}'; document.getElementById('sort').value = 'DESC'; frm_filtry.submit();">
{/if}
{if $smarty.get.order == $event_table_head[loop].db_col}<img src="templates_img/sort_{$smarty.get.sort|lower}.gif" class="btn_sort" />{/if}
{$event_table_head[loop].title}</td>
{/section}
{if $smarty.get.f_evt_noise=="True"}<td class="td_header">{if $smarty.get.module=="showAlerts"}##Repaired##{else}##Noise##{/if}</td>{/if}
</tr>

{section name=loop loop=$event_data}
<script type="text/javascript">IdArray_add({$event_data[loop].id});</script>
<tr class="tr_data evt_{$event_data[loop].evt_type}" onClick="OpenWindow('index.php?module={$event_detail_module}&sub={$event_data[loop].evt_computer}&id={$event_data[loop].id}', 'evtDetail', 650, 550);">
<td class="td_data"><img src="templates_img/evt_{$event_data[loop].evt_type}.gif" width="16" heigt="16" alt="{$event_data[loop].evt_type}" /></td>
<td class="td_data">{$event_data[loop].evt_time_generated|default:'&nbsp;'}</td>
{if $show_evt_computer == "true"}
<td class="td_data" style="text-transform: uppercase;">{$event_data[loop].evt_computer|default:'&nbsp;'}</td>
{/if}
<td class="td_data">{$event_data[loop].evt_code|default:'&nbsp;'}</td>
<td class="td_data">{$event_data[loop].evt_category|default:'&nbsp;'}</td>
<td class="td_data">{$event_data[loop].evt_source|default:'&nbsp;'}</td>
<td class="td_data">{$event_data[loop].evt_user|default:'&nbsp;'}</td>
{if $smarty.get.f_evt_noise=="True"}<td class="td_data" style="text-align: center;">{if $event_data[loop].evt_noise==1}##yes##{/if}</td>{/if}
</tr>
{/section}
</table>

<div style="text-align: center; margin-top: 20px; margin-bottom: 20px;">
{if $BLOCK_list_newer != "true"}
<a href="{$smarty.server.REQUEST_URI|add_url_param:'from':$list_newer}" class="button_txt" style="width: 100px;">&lt;&lt; ##Newer## &lt;&lt;</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
{/if}
{if $BLOCK_list_older != "true"}
<a href="{$smarty.server.REQUEST_URI|add_url_param:'from':$list_older}" class="button_txt" style="width: 100px;">&gt;&gt; ##Older## &gt;&gt;</a>
{/if}
</div>

{include file="_page_end.tpl"}
