<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="td_header_v" width="100">##Date and time##</td>
<td class="td_data_v">{$mail_data.evt_time_generated|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Computer##</td>
<td class="td_data_v">{$mail_data.evt_computer|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event type##</td>
<td class="td_data_v">{$mail_data.evt_type|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event code##</td>
<td class="td_data_v">{$mail_data.evt_code|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event category##</td>
<td class="td_data_v">{$mail_data.evt_category|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Event source##</td>
<td class="td_data_v">{$mail_data.evt_source|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##User##</td>
<td class="td_data_v">{$mail_data.evt_user|default:'&nbsp;'}</td>
</tr><tr>
<td class="td_header_v">##Description##</td>
<td class="td_data_v">{$mail_data.evt_message|nl2br}</td>
</tr>
</table>
<br /><br />
