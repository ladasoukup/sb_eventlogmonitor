<?php
// SBEventLogMonitor 2006 CRON - ParseData
// (c) Ladislav Soukup [root@soundboss.cz]
$time["start"] = microtime();
set_time_limit(0);
define("APP_TOKEN", "SB_ELM");
include_once "./config.php";
include_once "./class_smarty/Smarty.class.php";
$smarty = new Smarty();
$smarty->template_dir = SMARTY_template_dir;
$smarty->compile_dir = SMARTY_cache_dir;
$smarty->cache_dir = SMARTY_cache_dir;
$smarty->compile_check = true;
$smarty->caching = false;
include_once "./class_core.php";
$core = new sb_core;
// SQL CLASS
include_once "./class_ezsql.php";
$db->hide_errors();
$SECRET_KEY = $db->get_var("SELECT cfg_val FROM ".DB_PREFIX."cfg WHERE cfg_var='bat-prepare_secret' LIMIT 1");
if ($db->last_error != null){
	trigger_error("MySQL error!", E_USER_ERROR);
	die();
}
if (defined('EZSQL_DB_CHARSET')) $core->ezsql_set_charset(EZSQL_DB_CHARSET);include_once "./class_safesql.php";
$safesql = new SafeSQL_MySQL;
// END - SQL CLASS
define("NOW_DT", date("Y-m-d H:i:s"));
define("NOW", time());

if ($SECRET_KEY == $_GET["secret"]) {
	echo NOW . " - Generating WMI-EVENTS.txt...\n\n";
	$filename = "./data/WMI-EVENTS.txt";
	$query = "SELECT * FROM ".DB_PREFIX."computers WHERE computer_enable > 0 AND computer_next_harvest < '".NOW_DT."' ORDER BY computer_name ASC";
	$computers = $db->get_results($query, ARRAY_A);
	//$db->debug();
	
	$f = fopen($filename, "w");
	fputs($f, "REM Created at ".NOW_DT."\r\n");
	if (is_array($computers)) {
		foreach($computers as $computer){
			// SET NEXT RUN
			$new_time = date("Y-m-d H:i:s", NOW + $computer["computer_harvest_interval"] - 30);
			$query = "UPDATE ".DB_PREFIX."computers SET computer_next_harvest='".$new_time."' WHERE id='".$computer["id"]."'";
			$db->query($query);
			// Write it.
			fputs($f, $computer["computer_name"] . ";" . $computer["computer_enable"] . "\r\n");
		}
	}
	fclose($f);
} else {
	trigger_error("Wrong SECRET KEY!!!", E_USER_ERROR);
	DIE ("ACCESS DENIED!!!");
}
?>