#include "StdAfx.h"
#include "EventLogMon.h"
#include "LogManager.h"
#include "comfunct.h"
#include "free.h"
#include "EvtLogMessages.h"
#include "RuntimeErrorW.h"
#include "errors.h"

const WCHAR* EventLogMon::baseReg = L"System\\CurrentControlSet\\Services\\EventLogMonitor";

EventLogMon::EventLogMon(bool bFindTime, DWORD dwTimeToWait)
: bTimer(false),
  dwTimeBetweenChecks(dwTimeToWait),
  hOurKey(NULL),
  hPauseEvent(NULL),
  hExitEvent(NULL)// throws(RuntimeErrorW)
{
	if(SHRegOpenUSKey(baseReg, KEY_READ | KEY_WRITE, NULL, &hOurKey, TRUE) != ERROR_SUCCESS)
	{
		std::wstring err = L"Unable to open registry key: ";
		err += baseReg;
		Errors::ThrowException(err.c_str());
	}

	// Create the Pause event, it is initially set to signalled so any attempts
	// to wait on it fail and execution continues.
	hPauseEvent = CreateEvent(NULL, TRUE, TRUE, L"EventLogMonPauseEvent");

	// Create the exit event which, when signalled, will stop event listening and end the program
	hExitEvent = CreateEvent(NULL, TRUE, FALSE, L"EventLogMonExitEvent");

	// don't display critical error boxes since they will not be displayed on the users desktop
	// so will not be dismissed and so stall our service
	SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX | SEM_NOALIGNMENTFAULTEXCEPT); 

	if(bFindTime)
	{
		Config conf;
		StringCollection domains;
		FindDomains(domains);
		for(StringCollection::iterator iter = domains.begin(); iter != domains.end(); ++iter)
		{
			if(ReadConfiguration(*iter, conf))
			{
				break;
			}
		}
		if(conf.dwTimeToWait > 0)
		{
			bTimer = true;
			dwTimeBetweenChecks = conf.dwTimeToWait;
		}
	}
}

EventLogMon::~EventLogMon()
{
	if(hOurKey) SHRegCloseUSKey(hOurKey);
	if(hPauseEvent) CloseHandle(hPauseEvent);
	if(hExitEvent) CloseHandle(hExitEvent);
}

void EventLogMon::Run()
{
	// Waiting for this thread makes sure that we don't pull out the service from under the feet of
	// the log manager which may be processing last minute events.

	// Perfom main processing in another thread leaving this one to wait for exit
	HANDLE hFinishedReading = CreateThread(NULL, 0, DoMainLoop, this, 0, NULL);

	// The 10 minute waitable timer was setup in this thread 
	// (via the new EventLogMon call in EventLogMonMain) so the APC is queued for this thread
	// APC's only get called if the thread is in an alertable wait which
	// requires WaitForSingleObjectEx
	while(WaitForSingleObjectEx(hFinishedReading, INFINITE, TRUE) != WAIT_OBJECT_0)
	{} // empty loop ensures we get back to waiting on the main loop thread exit ASAP

	CloseHandle(hFinishedReading);
}

DWORD WINAPI EventLogMon::DoMainLoop(LPVOID lpParams)
{
	EventLogMon* logMon = static_cast<EventLogMon*>(lpParams);

	try
	{
		LogManager logMan(logMon->GetRegKey());

		// could be loads of events here (especially when first run)
		// so we turn down the thread priority so everybody else has 
		// a chance to work
		HANDLE hThread = GetCurrentThread();
		SetThreadPriority(hThread, THREAD_PRIORITY_BELOW_NORMAL);
		logMan.CatchUp();
		SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);

		// Resume processing as normal

		if(logMon->IsTimed())
		{
			logMan.DoTimedWaitingMode(logMon->GetTimeToWait());
		}
		else
		{
			logMan.DoEventWaitingMode();
		}
	}
	catch(const RuntimeErrorW& err)
	{
		// Failed to open the events file so exit the service
		Errors::ReportException(err, MSG_FAIL_START, EVENTLOG_ERROR_TYPE);
	}
	return 0;
}
