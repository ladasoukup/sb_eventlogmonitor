#include "StdAfx.h"
#include "LogManager.h"
#include "ServerInteract.h" // for definition of SendXMLFile
#include "EventLogMon.h"
#include <shlobj.h>
#include <sstream>
#include <algorithm>
#include "comfunct.h"
#include <iphlpapi.h>
#include "free.h"
#include <boost/scoped_array.hpp>
#include "SystemInformation.h"
#include "LogEnum.h"
#include "errors.h"

DWORD LogManager::dwTypesToLog = 0x1F; // all types is the default

LogManager::LogManager(HUSKEY hBaseKey)
{
	FindAndOpenLogs(hBaseKey);
	DWORD dwType = REG_DWORD, dwSize = sizeof(DWORD);
	SHGetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"TypesToLog", &dwType, &LogManager::dwTypesToLog, &dwSize);

	WCHAR szBuf[MAX_PATH] = L"";
	GetModuleFileName(NULL, szBuf, MAX_PATH);
	PathRemoveFileSpec(szBuf);
	PathAppend(szBuf, L"events.xml");
	xmlFileName = szBuf;
	hXmlOutput = CreateFile(xmlFileName.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hXmlOutput == NULL || hXmlOutput == INVALID_HANDLE_VALUE)
	{
		Errors::ThrowException(L"Unable to create/open the events.xml file");
	}

	hXMLFileWriteEvent = CreateEvent(NULL, TRUE, TRUE, L"EventLogMonFileWriteEvent");
	hSendXMLEvent = CreateEvent(NULL, TRUE, TRUE, L"EventLogMonSendEvent");
	h10MinSendTimer = CreateWaitableTimer(NULL, FALSE, L"EventLogMon10MinSendTimer");	
	
	WCHAR xmlBackupPath[MAX_PATH] = L"";
	if(!SHRegGetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"XMLBackup", xmlBackupPath, 0))
	{ //Returns 0 on success
		HANDLE xmlBackup = CreateFile(xmlBackupPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_DELETE_ON_CLOSE, NULL);
		if((xmlBackup != NULL) && (xmlBackup != INVALID_HANDLE_VALUE) && (GetFileSize(xmlBackup, NULL) != 0))
		{
			ResetEvent(hSendXMLEvent);
			xmlSendThread = CreateThread(NULL, 0, ServerInteract::SendXMLFile, xmlBackup, 0, NULL);
			RegisterWaitForSingleObject(&xmlSendWaitHandle, xmlSendThread, ServerInteract::SendXMLFileCleanup, this, INFINITE, WT_EXECUTEONLYONCE | WT_EXECUTELONGFUNCTION | WT_EXECUTEINPERSISTENTTHREAD);
		}
		else DeleteFile(xmlBackupPath);
	}
	ResetFile();
	// Send Every 10 minutes regardless of if we hit the limit to send
	LARGE_INTEGER li = {0};
	li.QuadPart = -6000000000LL; // 10 minutes in 100 nanosecond units
	SetWaitableTimer(h10MinSendTimer, &li, 60 * 10 * 1000, // 10 minutes in milliseconds
					 Do10MinSend, this, FALSE);
}

LogManager::~LogManager()
{
	CancelWaitableTimer(h10MinSendTimer);
	bool backupEvents = false;
	for(logIter iter = logs.begin(); iter != logs.end(); ++iter)
	{
		if(iter->GetNumEvents() > 0)
		{
			backupEvents |= 1;
		}
		WaitForAndResetXMLFileWriteAccess();
		for(LogEventIter eventIter = iter->GetEventsBegin(); eventIter != iter->GetEventsEnd(); ++eventIter)
		{
			eventIter->WriteXML(hXmlOutput);
		}
		SetEvent(hXMLFileWriteEvent);
	}
	if(backupEvents)
	{
		WaitForAndResetXMLFileWriteAccess();
		WriteClosingXML();
		BackupUnsentEvents();
		SetEvent(hXMLFileWriteEvent);
		FlushLastReadMessages();
	}
	CloseHandle(h10MinSendTimer);
	CloseHandle(hXmlOutput);
	CloseHandle(hSendXMLEvent);
	CloseHandle(hXMLFileWriteEvent);
}

void LogManager::CatchUp(DWORD dwEventsReadBeforeSend)
{
	DWORD dwThreadPriority = GetThreadPriority(GetCurrentThread());
	if(dwThreadPriority == THREAD_PRIORITY_BELOW_NORMAL)
	{
		dwEventsReadBeforeSend = 300;
	}
	for(logIter iter = logs.begin(); iter != logs.end(); ++iter)
	{
		LogCatchUp(iter, dwEventsReadBeforeSend);
	}
}

void LogManager::LogCatchUp(const logIter& log, DWORD dwEventsReadBeforeSend)
{
	while(log->ReadNewEvents(dwEventsReadBeforeSend)) // returns true when we're over the event size threshold
	{
		WriteLogXML(log);
		SendXML();
	}
}

void LogManager::DoEventWaitingMode()
{
	std::vector<HANDLE> events;
	for(logIter iter = logs.begin(); iter != logs.end(); ++iter)
	{
		iter->SetupEvent();
		events.push_back(iter->GetWriteEvent());
	}

	HANDLE hExitEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonExitEvent");
	HANDLE hPauseEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonPauseEvent");

	events.push_back(hExitEvent);

	DWORD eventSignalled = 0;
	const DWORD dwNumHandles = static_cast<DWORD>(events.size());
	DWORD dwExitEventPosition = (dwNumHandles - 1) - WAIT_OBJECT_0;

	while((eventSignalled = 
		WaitForMultipleObjectsEx(dwNumHandles, &events[0], FALSE, INFINITE, FALSE)) 
		!= dwExitEventPosition) 
	{
		eventSignalled -= WAIT_OBJECT_0;
		logIter log = logs.begin() + eventSignalled;
		LogCatchUp(log); // uses default event holding size
		logs[eventSignalled].ResetWriteEvent();

		// A 1 second wait time is specified so we don't go into the 
		// empty block and unnessecarily use the cpu too often
		//
		// The event gets signalled again when our service recieves a 
		// SERVICE_CONTROL_CONTINUE notification
		//
		// If it is signalled (i.e. we're not paused) then execution
		// continues without waiting for the 1 second to elapse
		while(WaitForSingleObject(hPauseEvent, 1000) != WAIT_OBJECT_0){}
	}
	CloseHandle(hPauseEvent);
	CloseHandle(hExitEvent);
}

void LogManager::DoTimedWaitingMode(const DWORD dwSecondsToWait)
{
	HANDLE hTimer = CreateWaitableTimer(NULL, FALSE, L"EventLogMonTimer");
	HANDLE hExitEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonExitEvent");
	HANDLE hPauseEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonPauseEvent");

	const HANDLE hWaitObjects[2] = {hTimer, hExitEvent};
	LARGE_INTEGER li = {0};
	li.QuadPart = dwSecondsToWait * -10000000LL; // Fire every dwSecondsToWait seconds in the future
	SetWaitableTimer(hTimer, &li, dwSecondsToWait * 1000, NULL, NULL, FALSE);

	while(WaitForMultipleObjectsEx(2, hWaitObjects, FALSE, INFINITE, FALSE)
		== WAIT_OBJECT_0)
	{
		CatchUp();
		while(WaitForSingleObject(hPauseEvent, 1000) != WAIT_OBJECT_0){}
	}
	// If we're here the exit event fired and all reading has finished
	CancelWaitableTimer(hTimer);
	CloseHandle(hTimer);
	CloseHandle(hPauseEvent);
	CloseHandle(hExitEvent);
}

void CALLBACK LogManager::Do10MinSend(LPVOID lpContext, DWORD dwLowTime, DWORD dwHighTime)
{
	LogManager* logMan = static_cast<LogManager*>(lpContext);
	for(logIter log = logMan->logs.begin(); log != logMan->logs.end(); ++log)
	{
		logMan->WriteLogXML(log);
	}
	logMan->SendXML();
	// This wait is needed otherwise the function will exit cleaning up the stack while 
	// the XML send thread is using the logMan pointer, causing access violations
	WaitForSingleObject(logMan->GetSendXMLEvent(), INFINITE);
	UNREFERENCED_PARAMETER(dwLowTime);
	UNREFERENCED_PARAMETER(dwHighTime);
}

void LogManager::WriteLogXML(const logIter& log)
{
	WaitForAndResetXMLFileWriteAccess();
	for(LogEventIter eventIter = log->GetEventsBegin(); eventIter != log->GetEventsEnd(); ++eventIter)
	{
		eventIter->WriteXML(hXmlOutput);
	}
	SetEvent(hXMLFileWriteEvent);
	log->ClearEvents(); // We've written them to file so we don't need them anymore
	log->WriteLastRecordToRegistry();
}

void LogManager::FlushLastReadMessages() const
{
	for(logIter_const iter = logs.begin(); iter != logs.end(); ++iter)
	{
		iter->WriteLastRecordToRegistry();
	}
}

void LogManager::BackupUnsentEvents()
{
	WCHAR buf[4096] = L"";

	GetModuleFileName(NULL, buf, 4096);

	PathRemoveFileSpec(buf);
	PathAppend(buf, L"xmlBackup.xml");

	std::wstring backup(buf);

	GetModuleFileName(NULL, buf, 4096);
	PathRemoveFileSpec(buf);
	PathAppend(buf, L"events.xml");

	std::wstring eventFile(buf);

	FlushFileBuffers(hXmlOutput);
	CloseHandle(hXmlOutput);

	// Do the backup

	MoveFileEx(eventFile.c_str(), backup.c_str(), MOVEFILE_REPLACE_EXISTING | MOVEFILE_WRITE_THROUGH);

	// Now we've backed it up, create another events file

	hXmlOutput = CreateFile(eventFile.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	ResetFile();

	SHRegSetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"XMLBackup", backup.c_str(), 0);
}

void LogManager::WaitForAndResetXMLFileWriteAccess()
{
	WaitForSingleObject(hXMLFileWriteEvent, INFINITE);
	ResetEvent(hXMLFileWriteEvent);
}

void LogManager::ResetFile()
{
	SetFilePointer(hXmlOutput, 0, NULL, FILE_BEGIN);
	SetEndOfFile(hXmlOutput);
	WriteBeginXML();
	FlushFileBuffers(hXmlOutput);
}

void LogManager::WriteBeginXML()
{
	const BYTE BOM[2] = {0xFF, 0xFE}; // The Byte Order Mark
	DWORD dwBytesWritten = 0;
	WriteFile(hXmlOutput, BOM, 2, &dwBytesWritten, NULL);

	const WCHAR* xmlHeader = L"<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<events>\n";
	DWORD dwBytesToWrite = static_cast<DWORD>(wcslen(xmlHeader) * sizeof(WCHAR));
	WriteFile(hXmlOutput, xmlHeader, dwBytesToWrite, &dwBytesWritten, NULL);

	SystemInformation sd;
	sd.WriteCompName(hXmlOutput);
	sd.WriteDescription(hXmlOutput);
	sd.WriteDomain(hXmlOutput);
	sd.WriteOSVersion(hXmlOutput);
	sd.WriteTimeOffset(hXmlOutput);
	sd.WriteAgentVersion(hXmlOutput);

	FlushFileBuffers(hXmlOutput);
}

void LogManager::WriteClosingXML()
{
	const WCHAR endingXML[] = L"\n</events>";
	DWORD dwBytesWritten = 0;
	WriteFile(hXmlOutput, endingXML, sizeof(endingXML) - sizeof(WCHAR), &dwBytesWritten, NULL);
	FlushFileBuffers(hXmlOutput);
}

void LogManager::ClearAllEvents()
{
	for(logIter iter = logs.begin(); iter != logs.end(); ++iter)
	{
		iter->ClearEvents();
	}
}

void LogManager::SendXML()
{
	WaitForSingleObject(hSendXMLEvent, INFINITE); // only allow the send after previous sends have completed
	ResetEvent(hSendXMLEvent);
	xmlSendThread = CreateThread(NULL, 0, ServerInteract::SendXMLFile, this, 0, NULL);
	RegisterWaitForSingleObject(&xmlSendWaitHandle, xmlSendThread, ServerInteract::SendXMLFileCleanup, this, INFINITE, WT_EXECUTEONLYONCE | WT_EXECUTEDEFAULT);
}

void LogManager::FindAndOpenLogs(HUSKEY hBaseKey)
{
	StringCollection names;
	LogEnumerator logEnum(hBaseKey, names, logs);
	std::for_each(names.begin(), names.end(), logEnum);
}
