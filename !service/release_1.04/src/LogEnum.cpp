#include "stdafx.h"
#include "LogEnum.h"
#include <shlwapi.h>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include "log.h"
#include "free.h"
#include "EvtLogMessages.h"
#include "RuntimeErrorW.h"
#include "errors.h"

LogEnumerator::LogEnumerator(HUSKEY hBaseKey, StringCollection& logNames, boost::ptr_vector<Log>& logs)
	:
	hBase(hBaseKey),
	logCollection(logs)
{
	HUSKEY eventLogs = NULL; // this key is either a smoker or has a bad cold (it's huskey geddit :)
	SHRegOpenUSKey(L"System\\CurrentControlSet\\Services\\EventLog\\", KEY_ENUMERATE_SUB_KEYS, NULL, &eventLogs, TRUE);

	DWORD dwIndex = static_cast<DWORD>(-1), dwBufSize = 200;
	std::wstring logName(dwBufSize, 0);

	while(SHRegEnumUSKey(eventLogs, ++dwIndex, &logName[0], &dwBufSize, SHREGENUM_HKLM) == ERROR_SUCCESS)
	{
		logNames.push_back(logName.c_str());
		dwBufSize = 200;
	}
	SHRegCloseUSKey(eventLogs);
}

void LogEnumerator::operator()(std::wstring& logName)
{
	try
	{
		Log* newLog = new(std::nothrow) Log(logName, hBase);
		if(newLog)
		{
			logCollection.push_back(newLog);
		}
	}
	catch(const RuntimeErrorW& err)
	{
		Errors::ReportException(err, MSG_FAIL_LOG_OPEN, EVENTLOG_WARNING_TYPE);
	}
}
