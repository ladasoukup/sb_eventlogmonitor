#include "StdAfx.h"
#include "Log.h"
#include <sstream>
#include <algorithm>
#include "sids.h"
#include <boost/scoped_array.hpp>
#include "free.h"
#include "LogManager.h"
#include "RuntimeErrorW.h"
#include "errors.h"

Log::Log(std::wstring& logName, HUSKEY hBaseKey) //throws RuntimeErrorW
	: name(logName),
	hWriteEvent(NULL)
{
	if(SHRegOpenUSKey(logName.c_str(), KEY_READ | KEY_WRITE, hBaseKey, &hKey, TRUE) != ERROR_SUCCESS)
	{
		if(SHRegCreateUSKey(logName.c_str(), KEY_READ | KEY_WRITE, hBaseKey, &hKey, SHREGSET_FORCE_HKLM) != ERROR_SUCCESS)
		{
			Errors::ThrowException(logName.c_str());
		}
	}
	DWORD dwLastRecordRead = 0, dwType = REG_DWORD, dwDataSize = sizeof(DWORD), dwDefault = 0;
	SHRegQueryUSValue(hKey, L"LastEventRecord", &dwType, static_cast<LPVOID>(&dwLastRecordRead),
					  &dwDataSize, TRUE, &dwDefault, sizeof(dwDefault));
	
	UpdateLastRecordRead(dwLastRecordRead);
	hLog = OpenEventLog(NULL, logName.c_str());
}

Log::~Log(void)
{
	CloseEventLog(hLog);
	if(hWriteEvent != NULL)
	{
		CloseHandle(hWriteEvent);
	}
	if(hKey != NULL)
	{
		SHRegCloseUSKey(hKey);
	}
}

void Log::SetupEvent()
{
	hWriteEvent = ::CreateEvent(NULL, TRUE, FALSE, name.c_str());
	NotifyChangeEventLog(hLog, hWriteEvent);
}

HANDLE Log::GetWriteEvent() const
{
	return hWriteEvent;
}

bool Log::ReadNewEvents(const DWORD dwEventsToReadBeforeCompletion)
{
	DWORD dwOldestRecord = 0, dwNumRecords = 0, dwNewestRecord = 0;

	GetOldestEventLogRecord(hLog, &dwOldestRecord);
	GetNumberOfEventLogRecords(hLog, &dwNumRecords);
	dwNewestRecord = dwOldestRecord + dwNumRecords;

	if(dwLastRecordRead < dwOldestRecord)
	{
		// Don't bother trying to read non-existant events
		UpdateLastRecordRead(dwOldestRecord);
	}

	// This size will pretty much always fail, this is intentional as the 
	// first ReadEventLog will return the actual size to allocate
	DWORD dwBufSize = sizeof(EVENTLOGRECORD);

	// The while loop always uses this block of memory and resizes it if
	// it's not big enough for the current record
	boost::scoped_array<BYTE> eventInfoBuf(new(std::nothrow) BYTE[dwBufSize]);
	EVENTLOGRECORD* eventInfo = reinterpret_cast<EVENTLOGRECORD*>(eventInfoBuf.get());
	if(eventInfo == NULL)
	{
		// If we can't get memory for whatever reason, dont bother trying to read them
		// Next time we're called we'll hopefully have enough
		return false;
	}

	dwNumRecords = 0; // Re-use this as a count of how many we've read

	SidLookup sids;

	for(; dwLastRecordRead < dwNewestRecord; ++dwLastRecordRead, ++dwNumRecords)
	{
		DWORD dwBytesRead = 0, dwBytesNeeded = 0;
		if(!ReadEventLog(hLog, EVENTLOG_SEEK_READ | EVENTLOG_FORWARDS_READ, dwLastRecordRead, eventInfo, dwBufSize, &dwBytesRead, &dwBytesNeeded))
		{
			if(GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				// Yep, not enough memory new'ed. Delete and renew the amount of bytes needed and try again
				dwBufSize = dwBytesNeeded;
				eventInfoBuf.reset(new(std::nothrow) BYTE[dwBufSize]);
				eventInfo = reinterpret_cast<EVENTLOGRECORD*>(eventInfoBuf.get());
				if(eventInfo == NULL) 
				{
					// Quit reading events if we don't have enough buffer memory.
					// Hopefully next time a read is triggered we'll have enough
					// We could just try and read further events hoping that they'll
					// be smaller but then we'd leak some.
					break;
				}

				if(!ReadEventLog(hLog, EVENTLOG_SEEK_READ | EVENTLOG_FORWARDS_READ, dwLastRecordRead, eventInfo, dwBufSize, &dwBytesRead, &dwBytesNeeded))
				{
					continue; // try the next one
				}
			}
			else continue;
		}
		// Check this is an event the user wants to collect
		if(eventInfo->EventType & LogManager::dwTypesToLog)
		{
			try
			{
				Event* event = new(std::nothrow) Event(eventInfo, name);
				if(event != NULL)
				{
					SID* user = event->GetSID();

					std::wstring userName;
					if(IsValidSid(user))
					{
						sids.GetAccountName(user, userName);
					}
					else
					{
						userName = L"N/A";
					}
					event->SetUserName(userName);
					events.push_back(event);
				}
			}
			catch(const RuntimeErrorW& err)
			{
				UNREFERENCED_PARAMETER(err); 
				// catch the exception produced by FormatMessage
				// but do nothing except continue
			}
		}

		if(events.size() >= dwEventsToReadBeforeCompletion)
		{
			return true;
		}
	}
	return false;
}

void Log::WriteLastRecordToRegistry() const
{
	SHRegWriteUSValue(hKey, L"LastEventRecord", REG_DWORD, &dwLastRecordRead, sizeof(DWORD), SHREGSET_FORCE_HKLM);
}

void Log::ClearEvents()
{
	events.clear();
}
