#include "stdafx.h"
#include "EventLogMon.h"
#include <sstream>
#include <fstream>
#include "EvtLogMessages.h"
#include <dbghelp.h>
#include "free.h"
#include "RuntimeErrorW.h"
#include <boost/lexical_cast.hpp>
#include <boost/scoped_ptr.hpp>
#include "errors.h"

//#define STANDALONE_DEBUGGING // define this to disable the service related code

DWORD CALLBACK EventLogMonNotificationHandler(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContextData)
{
	EventLogMon* logMon = static_cast<EventLogMon*>(lpContextData);
	static SERVICE_STATUS status = {0};
	if(status.dwServiceType != SERVICE_WIN32_OWN_PROCESS)
	{
		PopulateStatusToDefault(status);
	}
	switch(dwControl)
	{
		case SERVICE_CONTROL_PAUSE:
		{ 
			status.dwCurrentState = SERVICE_PAUSED;

			ResetEvent(logMon->GetPauseEvent());
		}
		break;
		case SERVICE_CONTROL_CONTINUE:
		{
			status.dwCurrentState = SERVICE_RUNNING;

			SetEvent(logMon->GetPauseEvent());
		}
		break;
		case SERVICE_CONTROL_STOP:
		case SERVICE_CONTROL_SHUTDOWN:
		{
			status.dwCurrentState = SERVICE_STOP_PENDING;
			
			SetEvent(logMon->GetExitEvent());
		}
		break;
	}
	SetServiceStatus(logMon->GetServiceHandle(), &status);
	UNREFERENCED_PARAMETER(lpEventData);
	UNREFERENCED_PARAMETER(dwEventType);
	return 0;
}

LONG WINAPI ExceptionHandler(EXCEPTION_POINTERS* ep)
{
	std::wstring dumpDir(MAX_PATH, 0);
	GetModuleFileName(NULL, &dumpDir[0], MAX_PATH);
	dumpDir.erase(dumpDir.find_last_of(L"\\"));
	dumpDir += L"\\dumps\\";
	CreateDirectory(dumpDir.c_str(), NULL); // The dumps directory might not exist yet

	int i = 0;

	for(; i < 10; ++i)
	{
		std::wstringstream dumpFile;
		dumpFile << dumpDir << L"evtDump" << i << L".dmp";
		HANDLE hFile = CreateFile(dumpFile.str().c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
		if(hFile != NULL && hFile != INVALID_HANDLE_VALUE)
		{
			MINIDUMP_EXCEPTION_INFORMATION mi;
			mi.ExceptionPointers = ep;
			mi.ThreadId = GetCurrentThreadId();
			mi.ClientPointers = TRUE;
			DWORD dwDebugLevel = MiniDumpWithIndirectlyReferencedMemory | MiniDumpWithHandleData | MiniDumpScanMemory | MiniDumpWithPrivateReadWriteMemory;
			MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hFile, static_cast<MINIDUMP_TYPE>(dwDebugLevel), &mi, NULL, NULL);
			CloseHandle(hFile);
			StringCollection insertionStrings;
			insertionStrings.push_back(dumpFile.str());
			Errors::LogEventMessage(EVENTLOG_ERROR_TYPE, MSG_CRASH, insertionStrings);
			break;
		}
	}

	// Now we delete the file numbered 1 more than the one we just created
	// so we'll always have the 9 most current dumps
	if(++i >= 9) i = 0;	
	std::wstringstream fileToDelete;
	fileToDelete << dumpDir << L"evtDump" << i << L".dmp";
	DeleteFile(fileToDelete.str().c_str());
	return EXCEPTION_EXECUTE_HANDLER;
}

// The real entrypoint to the service
// 

#ifndef STANDALONE_DEBUGGING

void WINAPI EventLogMonMain(DWORD argc, LPTSTR* argv)
#define RETVAL 

#else

int wmain(int argc, LPTSTR* argv)
#define RETVAL 0

#endif
{
	// These macros just shut up the compiler warnings about unreferenced variables
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	bool bExitImmediately = false;

	// Used for error reporting
	SetUnhandledExceptionFilter(&ExceptionHandler);

#ifdef _DEBUG

	// this gives us enough time to attach a debugger to the service.
	// Be sure to add the ServicesPipeTimeout value to the registry
	// as described at http://support.microsoft.com/kb/824344
	// so Windows doesn't think we've stalled in startup and report an error
	Sleep(30000);

#endif

	DWORD dwServiceWaitTime = 0, dwSize = sizeof(DWORD), dwDefault = static_cast<DWORD>(-1);
	SHRegGetUSValue(EventLogMon::baseReg, L"OnlineWait", NULL, &dwServiceWaitTime, &dwSize, TRUE, &dwDefault, sizeof(DWORD));

	bool bFindTime = (dwServiceWaitTime == -1);

	boost::scoped_ptr<EventLogMon> logMon(NULL);
	try
	{
		logMon.reset(new(std::nothrow) EventLogMon(bFindTime, dwServiceWaitTime));
		if(logMon.get() == NULL)
		{
			bExitImmediately = true;
		}
	}
	catch(const RuntimeErrorW& err)
	{
		// report the error and exit the service
		Errors::ReportException(err, MSG_FAIL_START, EVENTLOG_ERROR_TYPE);	
		bExitImmediately = true;
	}

#ifndef STANDALONE_DEBUGGING

	SERVICE_STATUS status;
	SERVICE_STATUS_HANDLE servHandle;

	PopulateStatusToDefault(status);
	
	servHandle = RegisterServiceCtrlHandlerEx(L"EventLogMonitor", EventLogMonNotificationHandler, logMon.get());
	if(servHandle == 0)
	{
		return RETVAL;
	}

	status.dwCurrentState = SERVICE_RUNNING;
	++status.dwCheckPoint;
	SetServiceStatus(servHandle, &status);

	if(bExitImmediately)
	{
		// setting the service status to SERVICE_RUNNING before (or instead of) immediately setting it to
		// SERVICE_STOPPED when we need to exit prevents Windows from reporting that the
		// service started and stopped without doing anything

		status.dwCurrentState = SERVICE_STOPPED;
		SetServiceStatus(servHandle, &status);
		return RETVAL;
	}

	logMon->SetServiceHandle(servHandle);

#endif

	logMon->Run();

#ifndef STANDALONE_DEBUGGING

	status.dwCurrentState = SERVICE_STOPPED;
	SetServiceStatus(servHandle, &status);

#endif

}

#ifndef STANDALONE_DEBUGGING
int main()
{
	SERVICE_TABLE_ENTRY DispatchTable[]={{L"EventLogMonitor", EventLogMonMain},{NULL,NULL}};  
	StartServiceCtrlDispatcher(DispatchTable);
	return 0;
}
#endif
