#ifndef COMFUNCT_H_
#define COMFUNCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef BUILD_COMFUNCT_DLL
#define EXPORT_COMFUNCT __declspec(dllexport)
#else
#define EXPORT_COMFUNCT __declspec(dllimport)
#endif

#include <wininet.h> /* For HINTERNET definition */

/* 
   Function: SetStatusA/W
   Description: Simple wrapper for sending a SB_SETTEXT message to the specified part of the status bar
   
   Parameters: HWND - the handle to the parent of the status bar
               int - the part of the status bar to set the text of
                     2 - The percentage volume
               LPC(W)STR  - The text to set
               
   Return value: None
*/

EXPORT_COMFUNCT void __cdecl SetStatusA(HWND, UINT, int, LPCSTR);
EXPORT_COMFUNCT void __cdecl SetStatusW(HWND, UINT, int, LPCWSTR);

/* 
   Function: ConnectHandleA/W
   Description: Initialises Wininet with the specified user-agent string
   
   Parameters: LPC(W)STR - String containing the user agent to use for internet activity
               
   Return value: HINTERNET handle that can be passed InternetConnect or ServerConnect
                 This function can also return a NULL handle, use GetLastError to retrieve the error information
*/

EXPORT_COMFUNCT HINTERNET __cdecl ConnectHandleA(LPCSTR);
EXPORT_COMFUNCT HINTERNET __cdecl ConnectHandleW(LPCWSTR);

/* 
   Function: ServerConnectA/W
   Description: Sets up a connection to a internet host
   
   Parameters: HINTERNET - Handle returned by a call to InternetOpen or ConnectHandle
               LPC(W)STR - String containing the hostname without the "http://" prefix, e.g. "www.google.com"
			   INTERNET_PORT - Port to connect on
               
   Return value: HINTERNET handle that can be passed InternetConnect or ServerConnect
                 This function can also return a NULL handle, use GetLastError to retrieve the error information
*/

EXPORT_COMFUNCT HINTERNET __cdecl ServerConnectA(HINTERNET, LPCSTR, INTERNET_PORT);
EXPORT_COMFUNCT HINTERNET __cdecl ServerConnectW(HINTERNET, LPCWSTR, INTERNET_PORT);

/* 
   Function: ASCIIConversion
   Description: Converts a wide string to its ASCII equivalent
   
   Parameters: LPCWSTR - Wide string to convert. If NULL is passed in the return value is also NULL
               
   Return value: char* to the converted string, this pointer must be passed to FreePointer function when it is no longer needed
*/

EXPORT_COMFUNCT char* __cdecl ASCIIConversion(LPCWSTR);

/* 
   Function: UnicodeConversion
   Description: Converts an ASCII string to its wide equivalent
   
   Parameters: LPCSTR - ASCII string to convert. If NULL is passed in the return value is also NULL
               
   Return value: char* to the converted string, this pointer must be passed to FreePointer function when it is no longer needed
*/

EXPORT_COMFUNCT WCHAR* __cdecl UnicodeConversion(LPCSTR);

/* 
   Function: ChangeTextFont
   Description: Simple wrapper for the WM_SETFONT message.
				Calls EnumChildWindows to cycle through and change the font of all child windows of the HWND parameter
   
   Parameters: HWND - Handle to the parent window whose children are to have their font changed
			   HFONT - Handle of font to apply
               
   Return value: None/void
*/

EXPORT_COMFUNCT void __cdecl ChangeTextFont(HWND, HFONT);

/* 
   Function: FreePointer
   Description: Frees the pointers allocated by the ASCII- and Unicode Conversion functions
   
   Parameters: void* - the pointer to free
               
   Return value: None/void
*/

EXPORT_COMFUNCT void __cdecl FreePointer(void*);

/* 
   Function: GetLocalTextA/W
   Description: Gets the current language's version of the interface strings
   
   Parameters: unsigned int - the index of the string to rertrieve
               
   Return value: Const pointer to the string
*/

EXPORT_COMFUNCT const char* __cdecl GetLocalTextA(unsigned int);
EXPORT_COMFUNCT const WCHAR* __cdecl GetLocalTextW(unsigned int);

/* 
   Function: SetupLanguage
   Description: Loads the language DLL and finds the address of the function it contains
   
   Parameters: None
               
   Return value: None/void
*/

EXPORT_COMFUNCT void __cdecl SetupLanguage(void);

/* 
   Function: GetCurrentLanguage
   Description: Gets the current language name from the name of the DLL. Used for downloading the correct language update
   
   Parameters: None
               
   Return value: Pointer to language substring
*/

EXPORT_COMFUNCT char* __cdecl GetCurrentLanguage(void);

/* 
   Function: MultiMediaKeyHook
   Description: Keyboard hook procedure acting on the multimedia keys play, pause, stop, next track and mute
   
   Parameters: int - hook action code
			   WPARAM - scan code of keys
			   LPARAM - extended flags
               
   Return value: return value of CallNextHookEx
*/

EXPORT_COMFUNCT LRESULT CALLBACK MultiMediaKeyHook(int, WPARAM, LPARAM);

#ifdef UNICODE
#define SetStatus SetStatusW
#define ServerConnect ServerConnectW
#define ConnectHandle ConnectHandleW
#define GetLocalText GetLocalTextW
#else
#define SetStatus SetStatusA
#define ServerConnect ServerConnectA
#define ConnectHandle ConnectHandleA
#define GetLocalText GetLocalTextA
#endif

#ifdef __cplusplus
}
#endif

#endif
