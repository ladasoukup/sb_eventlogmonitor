#include "stdafx.h"
#include <string>
#include <map>

typedef std::map<std::wstring, std::wstring> sidMap;

class SidLookup
{
private:
	static sidMap unknownSids;
	void InitializeMap();
public:
	SidLookup();
	void GetAccountSID(const WCHAR* wszSidToFind, std::wstring& wszAccountName) const;
	void GetAccountName(SID* pSid, std::wstring& wstrAccountName) const;
};
