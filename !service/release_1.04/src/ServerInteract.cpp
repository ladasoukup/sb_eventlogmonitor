#include "StdAfx.h"
#include "ServerInteract.h"
#include "LogManager.h"
#include "EventLogMon.h"
#include <lm.h>
#include <iphlpapi.h>
#include <wininet.h>
#include <boost/algorithm/string/case_conv.hpp> // std::transform
#include "comfunct.h"
#include <sstream>
#include <boost/scoped_array.hpp>
#include <boost/scoped_ptr.hpp>
#include "free.h"

ServerInteract::ServerInteract(HANDLE hXMLFile) : hXML(hXMLFile)
{
	hConnection = ConnectHandle(L"EventLogMonitor");

	bSecure = SHRegGetBoolUSValue(EventLogMon::baseReg, L"Secure", TRUE, false);

	// New way of querying the registry avoids setting hardcoded limits on our string buffers
	HKEY hKey = NULL;
	LSTATUS err = RegOpenKeyEx(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, 0, KEY_READ, &hKey);
	if(!err)
	{
		GetRegString(hKey, L"Server", server);
		if(!server.empty())
		{
			std::wstring::size_type pos = std::wstring::npos;
			if((pos = server.find(L":")) != std::wstring::npos)
			{
				std::wstringstream converter;
				converter << server.substr(pos + 1);
				converter >> port;
				server.erase(pos);
			}
			else port = (bSecure ? 443 : 80);
		}
		GetRegString(hKey, L"Path", path);
		GetRegString(hKey, L"Key",  keyData);
		RegCloseKey(hKey);
	}

	if(server.empty() || path.empty())
	{
		FindDomainAndReadConfig();
	}
	if(keyData.empty())
	{
		keyData = L"NAK";
	}
}

ServerInteract::~ServerInteract()
{
	InternetCloseHandle(hConnection);
	InternetCloseHandle(hServer);
}

bool ServerInteract::FindAvailableShares(const std::wstring& server)
{
	SHARE_INFO_0* si = NULL;

	DWORD numEntries = 0, totalEntries = 0;
	std::wstring serverAsShare(server);
	serverAsShare.insert(0, L"\\\\");

	if(!NetShareEnum(const_cast<LPWSTR>(serverAsShare.c_str()), 0, 
					 reinterpret_cast<LPBYTE*>(&si), MAX_PREFERRED_LENGTH, 
					 &numEntries, &totalEntries, 0))
	{
		for(DWORD i = 0; i < numEntries; ++i)
		{
			if(wcsstr(si[i].shi0_netname, L"NetLogon") != NULL)
			{
				NetApiBufferFree(si);
				return true;
			}
		}
	}
	NetApiBufferFree(si);
	return false;
}

bool ServerInteract::FindNetlogonShare(std::wstring &netLogonLocation)
{
	SERVER_INFO_100* si = NULL;

	DWORD numServers = 0, totalServersAndWorkstations = 0;
	if(!NetServerEnum(NULL, 100, reinterpret_cast<LPBYTE*>(&si), MAX_PREFERRED_LENGTH, &numServers, &totalServersAndWorkstations, 
				  SV_TYPE_SERVER_UNIX | SV_TYPE_NOVELL | SV_TYPE_SERVER_NT | SV_TYPE_WFW | SV_TYPE_DOMAIN_CTRL, NULL, 0))
	{
		if(numServers > 0)
		{
			for(DWORD i = 0; i < numServers; ++i)
			{
				std::wstring serverName(si[i].sv100_name);
				if(FindAvailableShares(serverName))
				{
					netLogonLocation = serverName;
					NetApiBufferFree(si);
					return true;
				}
			}
		}
	}
	else
	{
		ULONG uBytesNeeded = 0;
		GetNetworkParams(NULL, &uBytesNeeded);

		boost::scoped_array<BYTE> fiBuf(new (std::nothrow) BYTE[uBytesNeeded]); // scoped_array means we don't have to bother calling delete ourselves
		
		FIXED_INFO* fi = NULL;
		fi = reinterpret_cast<FIXED_INFO*>(fiBuf.get());
		GetNetworkParams(fi, &uBytesNeeded);

		IP_ADDR_STRING* pIPAddr = &fi->DnsServerList;
		while(pIPAddr) 
		{
			WCHAR* wideIP = UnicodeConversion(pIPAddr->IpAddress.String);
			std::wstring serverName(wideIP);
			if(FindAvailableShares(serverName))
			{
				netLogonLocation = serverName;
				NetApiBufferFree(si);
				FreePointer(wideIP);
				return true;
			}
			FreePointer(wideIP);
			pIPAddr = pIPAddr ->Next;
		}

		GetAdaptersInfo(NULL, &uBytesNeeded);

		boost::scoped_array<BYTE> iafBuf(new(std::nothrow) BYTE[uBytesNeeded]);

		IP_ADAPTER_INFO* iaf = reinterpret_cast<IP_ADAPTER_INFO*>(iafBuf.get());
		GetAdaptersInfo(iaf, &uBytesNeeded);

		if(iaf != NULL)
		{
			pIPAddr = &iaf->GatewayList;
			while(pIPAddr) 
			{
				WCHAR* wideIP = UnicodeConversion(pIPAddr->IpAddress.String);
				std::wstring serverName(wideIP);
				FreePointer(wideIP);
				if(FindAvailableShares(serverName))
				{
					netLogonLocation = serverName;
					NetApiBufferFree(si);
					return true;
				}
				pIPAddr = pIPAddr->Next;
			}
		}
	}
	NetApiBufferFree(si);
	return false;
}

DWORD WINAPI ServerInteract::SendXMLFile(LPVOID lpParam)
{
	HANDLE hFile = NULL;
	bool bFileOnly = false;
	LogManager* logMan = NULL;
	try
	{
		logMan = static_cast<LogManager*>(lpParam);
		hFile = logMan->GetXMLFile();
	}
	catch(...)
	{
		hFile = static_cast<HANDLE>(lpParam);
		bFileOnly = true;
	}
	ServerInteract serv(hFile);
	if(!serv.HasServerPath())
	{
		return 0;
	}
	else
	{
		DWORD retVal = 0;
		if(bFileOnly)
		{
			retVal = serv.SendData();
			SHRegSetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"XMLBackup", L"", 0);
			CloseHandle(hFile);
		}
		else
		{
			logMan->WaitForAndResetXMLFileWriteAccess();
			logMan->WriteClosingXML();
			retVal = serv.SendData();
			SHRegSetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"XMLBackup", L"", 0);
		}
		return retVal;
	}
}

VOID CALLBACK ServerInteract::SendXMLFileCleanup(LPVOID lpContext, BOOLEAN bWaited)
{
	LogManager* logMan = static_cast<LogManager*>(lpContext);
	DWORD dwThreadExitValue = 0;
	GetExitCodeThread(logMan->GetThreadHandle(), &dwThreadExitValue);
	if(dwThreadExitValue != 1)
	{
		logMan->BackupUnsentEvents();
	}
	else logMan->ResetFile();
	SetEvent(logMan->GetXMLFileWriteEvent());
	CloseHandle(logMan->GetThreadHandle());

	UnregisterWait(logMan->GetWaitThreadHandle());
	logMan->SetWaitThreadHandle((HANDLE)NULL);
	SetEvent(logMan->GetSendXMLEvent());
	
	UNREFERENCED_PARAMETER(bWaited);
}

bool ServerInteract::ConnectToServer()
{
	hServer = ServerConnect(hConnection, server.c_str(), port);
	if(hServer != NULL)
	{
		return true;
	}
	else return false;
}

DWORD ServerInteract::SendData()
{
	if(!ConnectToServer())
	{
		return 0;
	}
	
	// This construct is used to give us 2 tries at sending the data
	// If we have an invalid key PostXML will fail, set keyData to default
	// and go around the loop again, requesting a new key as it goes
	// If we still fail to send the XML, the invalid key check after the loop
	// helps determine the return code which SendXMLFileCleanup will use
	// to signal whether it should backup the XML data to try at the next service start

	bool bFailed = false;
	for(DWORD i = 0; i < 2; ++i)
	{
		if(bFailed || (keyData.find(L"NAK") != std::wstring::npos))
		{
			std::wstring keyRequestPath = path.c_str(), newKey;
			keyRequestPath += L"?secretrequest=1";
			
			if(!GetKey(keyRequestPath, newKey))
			{
				keyRequestPath += L"&secret=";
				keyRequestPath += keyData;
				GetKey(keyRequestPath, keyData);
				SHRegSetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Key", keyData.c_str(), 0);
			}
			else 
			{
				keyData = newKey;
				SHRegSetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Key", keyData.c_str(), 0);
			}
		}

		std::wstring pathWithKey = path.c_str();
		pathWithKey += L"?secret=";
		pathWithKey += keyData;

		// The (== true) is not necessary, it's just here to suppress warning C4706 in VS
		if((bFailed = PostXML(pathWithKey)) == true)
		{
			break;
		}
	}

	return bFailed; // return true if we succeeded, false if we failed
}

bool ServerInteract::GetKey(const std::wstring& path, std::wstring& newKey) const
{
	DWORD dwFlags = INTERNET_FLAG_NO_UI | INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_RELOAD | INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_HYPERLINK | INTERNET_FLAG_NO_COOKIES;
	if(bSecure)
	{
		dwFlags |= INTERNET_FLAG_SECURE;
	}
	HINTERNET hRequest = HttpOpenRequest(hServer, L"GET", path.c_str(), NULL, NULL, NULL, dwFlags, 0);
	if(hRequest != NULL)
	{
		if(HttpSendRequest(hRequest, NULL, 0, NULL, 0))
		{
			DWORD dwLength = sizeof(DWORD), dwInfo = 0;
			HttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, static_cast<LPVOID>(&dwInfo), &dwLength, NULL);
			if(dwInfo < HTTP_STATUS_AMBIGUOUS)
			{
				std::string keyBuf, key;
				ReadData(hRequest, keyBuf);
				InternetCloseHandle(hRequest);

				key = keyBuf.c_str();
				std::string::size_type pos = key.find("\r\n");
				if(pos != std::string::npos)
				{
					key.erase(pos); // get rid of everything after the newline characters
				}
				// If we got a wrong secret response or something else went wrong
				// Secrets are 8 characters long so if the newlines weren't at pos 8 then we didn't
				// get back expected data
				if((newKey.find(L"WRONG") != std::wstring::npos) || (key.length() != 8)) 
				{
					InternetCloseHandle(hRequest);
					return false;
				}
				WCHAR* data = UnicodeConversion(key.c_str());
				newKey = data;
				FreePointer(data);
				return true;
			}
		}
	}
	InternetCloseHandle(hRequest);
	return false;
}

void ServerInteract::ReadData(const HINTERNET hRequest, std::string& file) const
{
	bool bFinished = false;
	DWORD dwLength = 0;
	InternetQueryDataAvailable(hRequest, &dwLength, 0, 0);
	
	if(dwLength <= 0)
	{
		return;
	}

	while(!bFinished)
	{
		DWORD dwChunkSize = 0;

		InternetQueryDataAvailable(hRequest, &dwChunkSize, 0, 0);

		if(dwChunkSize == 0)
		{
			bFinished = true;
			break;
		}

		boost::scoped_array<BYTE> chunkBuf(new (std::nothrow) BYTE[dwChunkSize + 1]);
		BYTE* chunk = chunkBuf.get();

		if(chunk != NULL)
		{
			DWORD dwBytesDownloaded = 0, dwCount = 0;

			while(dwBytesDownloaded != dwChunkSize)
			{
				if(!InternetReadFile(hRequest, &chunk[dwCount], dwChunkSize - dwCount, &dwBytesDownloaded))
				{
					bFinished = true;
					break;
				}
				dwCount += dwBytesDownloaded;
			}
			chunk[dwChunkSize] = '\0'; // ensure NULL termination
			file += reinterpret_cast<char*>(chunk);
		}
	}
}

bool ServerInteract::PostXML(const std::wstring &postPath) const
{
	LPCWSTR szTypes[] = {L"*/*", NULL};
	DWORD dwFlags = INTERNET_FLAG_NO_CACHE_WRITE;
	bool success = false;
	if(bSecure)
	{
		dwFlags |= INTERNET_FLAG_SECURE;
	}
	HINTERNET hPost = HttpOpenRequest(hServer, L"POST", postPath.c_str(), NULL, NULL, szTypes, dwFlags, 0);
	if(hPost != NULL)
	{
		HANDLE hMapping = CreateFileMapping(hXML, NULL, PAGE_READONLY, 0, 0, L"xmlData");
		LPVOID xmlStart = MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0);
		if(HttpSendRequest(hPost, NULL, 0, xmlStart, GetFileSize(hXML, NULL)))
		{
			std::string data;
			ReadData(hPost, data);

			UnmapViewOfFile(xmlStart);
			CloseHandle(hMapping);

			std::string response = data.c_str();

			std::string::size_type off = response.find("\r\n");
			if(off != std::string::npos)
			{
				response.erase(off);
			}
			boost::algorithm::to_lower(response);

			off = response.find("ok");
			if(off != std::string::npos)
			{
				success = true;
			}
		}
	}	
	InternetCloseHandle(hPost);
	return success;
}

void ServerInteract::FindDomainAndReadConfig()
{
	StringCollection domains;
	
	FindDomains(domains);

	Config conf;

	if(domains.empty())
	{
		std::wstring shareLocation;
		if(FindNetlogonShare(shareLocation))
		{
			ReadConfiguration(shareLocation, conf);
		}
	}
	else
	{
		for(StringCollection::iterator iter = domains.begin(); iter != domains.end(); ++iter)
		{
			if(ReadConfiguration(*iter, conf))
			{
				break;
			}
		}
	}
}
