#ifndef SYSINFO_H
#define SYSINFO_H

#pragma once

#include "stdafx.h"

class SystemInformation
{
private:
	OSVERSIONINFOEX os;

	void GetOsVersion(std::wstringstream& product) const;
	BOOL IsWOW64() const;
	void WriteIt(HANDLE hFile, std::wstring& data) const;

public:
	SystemInformation();
	void WriteDescription(HANDLE hFile) const;
	void WriteDomain(HANDLE hFile) const;
	void WriteTimeOffset(HANDLE hFile) const;
	void WriteOSVersion(HANDLE hFile) const;
	void WriteAgentVersion(HANDLE hFile) const;
	void WriteCompName(HANDLE hFile) const;
};

#endif
