#pragma once

#include "stdafx.h"
#include <wininet.h>

class ServerInteract
{
public:
	ServerInteract(HANDLE hXMLFile);
	~ServerInteract();
	DWORD SendData();
	void ReadData(const HINTERNET hRequest, std::string& data) const;
	bool PostXML(const std::wstring& postPath) const;
	bool HasServerPath() const {return !(server[0] == L'\0');}

	static DWORD WINAPI SendXMLFile(LPVOID lpParam);
	static VOID CALLBACK SendXMLFileCleanup(LPVOID lpContext, BOOLEAN bWaited);

private:
	void FindDomainAndReadConfig();
	bool GetKey(const std::wstring& path, std::wstring& newKey) const;
	bool ConnectToServer();
	bool FindNetlogonShare(std::wstring& netlogonShareLocation);
	bool FindAvailableShares(const std::wstring& server);

	HANDLE hXML;
	HINTERNET hConnection;
	HINTERNET hServer;
	std::wstring server, path, keyData;
	BOOL bSecure;
	INTERNET_PORT port;
};
