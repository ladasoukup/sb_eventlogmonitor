#ifndef LOGMANAGER_H
#define LOGMANAGER_H
#pragma once

#include "stdafx.h"
#include "log.h"
#include <boost/ptr_container/ptr_vector.hpp>

typedef boost::ptr_vector<Log> LogCollection;
typedef LogCollection::iterator logIter;
typedef LogCollection::const_iterator logIter_const;

class LogManager
{
public:
	LogManager(HUSKEY hBaseKey);
	~LogManager();
	void AddLog(Log* addedLog) {logs.push_back(addedLog);}
	void FlushLastReadMessages() const;
	void DoEventWaitingMode();
	void DoTimedWaitingMode(const DWORD dwTimeToWait);
	void BackupUnsentEvents();
	void CatchUp(DWORD dwEventsReadBeforeSend = 100);
	void LogCatchUp(const logIter& log, DWORD dwEventsToRead = 100);
	void WaitForAndResetXMLFileWriteAccess();
	void WriteBeginXML();
	void WriteEventXML();
	void WriteClosingXML();
	void ResetFile();
	void ClearAllEvents();
	void SendXML();
	void WriteLogXML(const logIter& log);

	HANDLE GetWaitThreadHandle() const {return xmlSendWaitHandle;}
	void SetWaitThreadHandle(HANDLE hNewHandle) {xmlSendWaitHandle = hNewHandle;}
	HANDLE GetThreadHandle() const {return xmlSendThread;}
	HANDLE GetXMLFile() const {return hXmlOutput;}
	HANDLE GetXMLFileWriteEvent() const {return hXMLFileWriteEvent;}
	HANDLE GetSendXMLEvent() const {return hSendXMLEvent;}
	static void CALLBACK Do10MinSend(LPVOID lpContext, DWORD dwTimerLowValue, DWORD dwTimerHighValue);
	static DWORD dwTypesToLog;

private:
	LogCollection logs;
	HANDLE hXmlOutput, xmlSendWaitHandle, xmlSendThread, hSendXMLEvent, hXMLFileWriteEvent;
	HANDLE h10MinSendTimer;
	std::wstring xmlFileName;
	void FindAndOpenLogs(HUSKEY hBaseKey);
};

#endif
