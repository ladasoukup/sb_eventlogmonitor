#ifndef FREE_H
#define FREE_H

#include "stdafx.h"
#include <string>

#pragma once

// stores the results from ReadConfiguration
struct Config
{
	std::wstring server;
	std::wstring path;
	DWORD dwTimeToWait; // only used in EventLogMon constructor
	Config() : dwTimeToWait(0) {} // online mode is default
};

// prototypes of functions not part of a class

void FindDomains(StringCollection& domains);
void GetRegString(HKEY hKey, const std::wstring& value, std::wstring& buf);
bool ReadConfiguration(std::wstring& domain, Config& post);
BOOL IsWOW64();
BOOL IsLowerCaseLetterOrSpace(WCHAR character);
BOOL GetModuleMessage(HMODULE hMessageLib, DWORD dwFlags, DWORD dwMessageID, LPWSTR& buf, va_list* inserts); // throws RuntimeErrorW

#endif
