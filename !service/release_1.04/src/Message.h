#ifndef MESSAGE_H
#define MESSAGE_H
#pragma once

#include "stdafx.h"
#include <map>
#include <stdexcept>

struct Category
{
	WORD id;
	std::wstring name;
};

struct Replacement
{
	std::wstring::size_type lengthToReplace;
	std::wstring string;
};

struct StringOffset
{
	std::wstring::size_type offsetInString;
	DWORD_PTR offsetPointer;
};

struct SourceDlls
{
	StringCollection event;
	StringCollection category;
};

typedef std::map<std::wstring, SourceDlls> DllLocations;

class Message
{
public:
	Message(const std::wstring& eventSource, const std::wstring& logLocation, DWORD dwEventID, std::vector<DWORD_PTR>& insertOffsets, std::wstring& details, Category& cat);
	std::wstring GetFullMessage() const {return message;}

private:

	void GetDllPath(std::wstring& messageSubKey, const std::wstring& type, std::wstring& messageDll);
	void GetParameterStrings(const DWORD count, std::vector<Replacement>& outputStrings);
	bool FormatFullMessage(StringCollection& dlls);
	bool CheckAndExplodeDelimitedDlls(std::wstring& messageDlls, StringCollection& retDlls, const WCHAR delimiter);
	bool GrabFormatMessageOutput(const std::wstring& messageDll, const DWORD dwMessageID, bool useInsertionStrings, std::wstring& outputString) const;

	std::wstring message, // the full message text
		location, // which log the event lives in
		eventSource, // the source of the event HHCTRL, VMWare Workstation etc
		regKey; // the sources reg key under HKLM\\System\\CurrentControlSet\\Services\\EventLog\\<location>
	DWORD eventID;
	std::vector<StringOffset> parameterOffsets;
	std::vector<DWORD_PTR> insertionOffsets;
	static DllLocations dllPaths;
};

// free functions in Message.cpp

bool IsNonXmlChar(WCHAR ch);

#endif
