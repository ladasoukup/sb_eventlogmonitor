#include "stdafx.h"
#include "free.h"
#include <boost/scoped_array.hpp>
#include <fstream>
#include "comfunct.h"
#include <lm.h> // for NetServerEnum
#include "eventlogmon.h" // for EventLogMon::baseRegKey
#include <sstream>
#include <iphlpapi.h> // for GetNetworkParams
#include <boost/lexical_cast.hpp>
#include "logmanager.h" // just for wTypesToLog that'll be filled in in ReadConfiguration
#include "errors.h"

bool ReadConfiguration(std::wstring& domain, Config& conf)
{
	std::wifstream config;
	std::wstring line;
	StringCollection lines;

	std::wstring configFile(domain);
	configFile.insert(0, L"\\\\");
	configFile += L"\\Netlogon\\eventlogmon.cfg";

	char* asciiName = ASCIIConversion(configFile.c_str());
	config.open(asciiName);
	FreePointer(asciiName);

	if(config.is_open())
	{
		while(std::getline(config, line))
		{
			lines.push_back(line);
		}
	}

	if(!lines.empty())
	{
		conf.server = lines[0];
		BOOL bSecure = FALSE;
		if(conf.server.find(L"https") != std::wstring::npos)
		{
			conf.server.erase(0, 8); // remove the protocol
			bSecure = TRUE;
		} else conf.server.erase(0, 7);

		SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Secure", REG_DWORD, &bSecure, sizeof(BOOL));

		std::wstring::size_type serverEnd = conf.server.find_first_of(L"/"); //find the / after the server
		conf.path = conf.server.substr(serverEnd + 1); // make a new string out of the rest of the server path from the position after the first / 
		conf.server.erase(serverEnd); // get rid of the elements we just copied to path
		
		SHRegSetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Server", conf.server.c_str(), 0);
		SHRegSetPath(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Path", conf.path.c_str(), 0);

		try
		{
			conf.dwTimeToWait = boost::lexical_cast<DWORD>(lines[1]);
			if(lines.size() > 2)
			{
				LogManager::dwTypesToLog = boost::lexical_cast<WORD>(lines[2]);
			}
		}
		catch(const boost::bad_lexical_cast& bl)
		{
			UNREFERENCED_PARAMETER(bl);
		}

		SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"OnlineWait", REG_DWORD, &conf.dwTimeToWait, sizeof(DWORD));
		SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"TypesToLog", REG_DWORD, &LogManager::dwTypesToLog, sizeof(DWORD));
		return true;
	}
	return false;
}

void FindDomains(StringCollection& domains)
{
	DWORD numDomains = 0, numTotal = 0;
	SERVER_INFO_100* si = NULL, getnpSI = {0};

	if(NetServerEnum(NULL, 100, reinterpret_cast<LPBYTE*>(&si), MAX_PREFERRED_LENGTH, &numDomains, &numTotal, SV_TYPE_DOMAIN_ENUM, NULL, 0))
	{
		DWORD dwLength = 0;
		GetNetworkParams(NULL, &dwLength);
		boost::scoped_array<BYTE> fiBuffer(new BYTE[dwLength]);

		FIXED_INFO* fi = reinterpret_cast<FIXED_INFO*>(fiBuffer.get());
		if(!GetNetworkParams(fi, &dwLength)) // returns 0 on success
		{
			getnpSI.sv100_name = UnicodeConversion(fi->DomainName);
		}
		numDomains = 1; // GetNetworkParams only returns 1 domain
		si = &getnpSI;
	}
	domains.reserve(numDomains);
	for(DWORD i = 0; i < numDomains; ++i)
	{
		domains.push_back(si[i].sv100_name);
	}
	if(getnpSI.sv100_name)
	{
		FreePointer(getnpSI.sv100_name);
	}
	NetApiBufferFree(si);
}

void GetRegString(HKEY hKey, const std::wstring& value, std::wstring& buf)
{
	DWORD dwType = REG_SZ, dwLength = 0;
	LSTATUS err = RegQueryValueEx(hKey, value.c_str(), NULL, &dwType, NULL, &dwLength);
	if(!err && dwLength > 1) // if length is 1 then the value is empty
	{
		buf.resize(dwLength / sizeof(WCHAR)); // RegQueryValueEx returns the number of bytes not the number of characters 
		RegQueryValueEx(hKey, value.c_str(), NULL, &dwType, reinterpret_cast<LPBYTE>(&buf[0]), &dwLength);
		buf.resize(buf.size() - 1); // the original size includes the NULL, which we don't need to store
	}
}

BOOL IsLowerCaseLetterOrSpace(WCHAR character)
{
	return IsCharLower(character) || iswspace(character);
}

BOOL GetModuleMessage(HMODULE hMessageLib, DWORD dwFlags, DWORD dwMessageID, LPWSTR& buf, va_list* inserts) //throws RuntimeErrorW
{
	BOOL bRes = FALSE;
	__try
	{
		bRes = (FormatMessage(dwFlags, hMessageLib, dwMessageID, 0, reinterpret_cast<LPWSTR>(&buf), 1, inserts) == 0);
	}
	__except(GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION)
	{
		if(hMessageLib) FreeLibrary(hMessageLib);
		Errors::ThrowException(L"FormatMessage caused an access violation");
	}
	return bRes;
}
