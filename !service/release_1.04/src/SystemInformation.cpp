#include "StdAfx.h"
#include "SystemInformation.h"
#include <iphlpapi.h>
#include "comfunct.h"
#include <sstream>
#include <cctype>
#include <algorithm>
// lexical_cast.hpp generates a "conditional expression is constant" warning
// which is safe to disable
#pragma warning(push)
#pragma warning(disable : 4127)
#include <boost/lexical_cast.hpp>
#pragma warning(pop)
#include <boost/scoped_array.hpp>
#include "free.h"

SystemInformation::SystemInformation(void)
{
	os.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	GetVersionEx(reinterpret_cast<OSVERSIONINFO*>(&os));
}

void SystemInformation::WriteIt(HANDLE hFile, std::wstring& data) const
{
	DWORD dwBytes = 0;
	WriteFile(hFile, data.c_str(), static_cast<DWORD>(data.length() * sizeof(WCHAR)), &dwBytes, NULL);
}

void SystemInformation::WriteDomain(HANDLE hFile) const
{
	ULONG uBytesNeeded = 0;
	GetNetworkParams(NULL, &uBytesNeeded);
	boost::scoped_array<BYTE> fiBuf(new(std::nothrow) BYTE[uBytesNeeded]);
	FIXED_INFO* fi = reinterpret_cast<FIXED_INFO*>(fiBuf.get());
	
	GetNetworkParams(fi, &uBytesNeeded);

	std::wstring domain = L"<computer_domain>";

	if(fi != NULL)
	{
		WCHAR* szDomain = UnicodeConversion(fi->DomainName);
		if(szDomain)
		{
			domain += szDomain;
		}
		FreePointer(szDomain);
	}
	domain += L"</computer_domain>\n";

	WriteIt(hFile, domain);
}

void SystemInformation::WriteDescription(HANDLE hFile) const
{
	std::wstring compDesc = L"<computer_description>";
	WCHAR szDescription[1000] = L"";
	SHRegGetPath(HKEY_LOCAL_MACHINE, L"System\\CurrentControlSet\\Services\\lanmanserver\\parameters", L"srvcomment", szDescription, 0);
	compDesc += szDescription;
	compDesc += L"</computer_description>\n";

	WriteIt(hFile, compDesc);
}

void SystemInformation::WriteOSVersion(HANDLE hFile) const
{
	std::wstringstream product;
	product << L"Windows ";
	GetOsVersion(product);
	std::wstring prodString(L"<computer_os>");
	prodString += product.str();
	prodString += L"</computer_os>\n";
	WriteIt(hFile, prodString);
}

void SystemInformation::GetOsVersion(std::wstringstream& product) const
{
	// determine whether we're running on an x64 machine
	std::wstring x64;
	if(IsWOW64())
	{
		x64 = L"x64 ";
	}

	// We don't support NT 4
	if(os.dwMajorVersion == 5)
	{
		switch(os.dwMinorVersion)
		{
			case 0:
			{
				product << L"2000 ";
			}
			break;
			case 1:
			{
				product << L"XP ";
				if(os.wSuiteMask & VER_SUITE_PERSONAL)
				{
					product << L"Home ";
				}
				else product << L"Professional ";
				product << x64;
				product << L"Edition ";
			}
			break;
			case 2:
			{
				product << L"Server 2003 " << x64;
			}
			break;
		}
	}
	else if(os.dwMajorVersion == 6)
	{
		if(os.wProductType & VER_NT_WORKSTATION)
		{
			product << L"Vista ";
			if(os.wSuiteMask & VER_SUITE_PERSONAL)
			{
				product << L"Home " << L"Edition ";
			}
			product << x64;
		}
		else product << L"Server 2008 " << x64;
	}
 
	switch (os.wSuiteMask)
	{
		case VER_SUITE_DATACENTER:
		{
			product << L"Datacenter Edition ";
		}
		break;
		case VER_SUITE_COMPUTE_SERVER:
		{
			product << L"Compute Cluster Edition ";
		}
		break;
		case VER_SUITE_ENTERPRISE:
		{
			product << L"Enterprise Edition ";
		}
		break;
		case VER_SUITE_BLADE:
		{
			product << L"Web Edition ";
		}
		break;
		case VER_SUITE_STORAGE_SERVER:
		{
			product << L"Storage Edition R2 ";
		}
		break;
	}
	std::wstring servicePack = os.szCSDVersion;
	if(servicePack != L"")
	{
		std::wstring spString(3, 0);
		std::remove_copy_if(servicePack.begin(), servicePack.end(), spString.begin(), IsLowerCaseLetterOrSpace);
		product << spString;
	}
}

BOOL SystemInformation::IsWOW64() const
{
	typedef BOOL (WINAPI*WOWFUNC)(HANDLE, BOOL*);
	BOOL bIs64Bit = FALSE;
	WOWFUNC isWow64 = reinterpret_cast<WOWFUNC>(GetProcAddress(GetModuleHandle(L"kernel32.dll"), "IsWow64Process"));
	if(isWow64 != NULL)
	{
		isWow64(GetCurrentProcess(), &bIs64Bit);
	}
	return bIs64Bit;
}

void SystemInformation::WriteTimeOffset(HANDLE hFile) const
{
	TIME_ZONE_INFORMATION tzi = {0};
	DWORD dwRes = GetTimeZoneInformation(&tzi);
	LONG bias = tzi.Bias;
	if(dwRes == TIME_ZONE_ID_STANDARD || dwRes == TIME_ZONE_ID_UNKNOWN)
	{
		bias += tzi.StandardBias;
	}
	else
	{
		bias += tzi.DaylightBias;
	}
	// the bias is the number of minutes to UTC, not from UTC
	// So the bias we get is the inverse of what we want
	// For example British Summer Time is UTC + 1
	// but the function above will return a bias of -60 minutes
	// since that's the number of minutes to be added to the 
	// local time to get to UTC
	float biasInHours = static_cast<float>(bias) / 60;
	biasInHours *= -1;

	std::wstring hourBias = boost::lexical_cast<std::wstring>(biasInHours);
	if(biasInHours > 0)
	{
		hourBias.insert(0, L"+");
	}

	std::wstring timeOffset = L"<time_offset>";
	timeOffset += hourBias;
	timeOffset += L"</time_offset>\n";

	WriteIt(hFile, timeOffset);
}

void SystemInformation::WriteAgentVersion(HANDLE hFile) const
{
	static std::wstring versionString;
	if(versionString.empty())
	{
		std::wstring fileName(MAX_PATH, 0);
		GetModuleFileName(NULL, &fileName[0], MAX_PATH);
		DWORD dwHandle = 0;
		DWORD dwByteSize = GetFileVersionInfoSize(fileName.c_str(), &dwHandle);
		if(dwByteSize > 0)
		{
			boost::scoped_array<BYTE> verBuf(new (std::nothrow) BYTE[dwByteSize]);
			BYTE* ver = verBuf.get();
			
			if(ver != NULL)
			{
				if(GetFileVersionInfo(fileName.c_str(), dwHandle, dwByteSize, ver))
				{
					VS_FIXEDFILEINFO* buf = NULL;
					UINT size = 0;
					if(VerQueryValue(ver, L"\\", reinterpret_cast<LPVOID*>(&buf), &size))
					{
						std::wstringstream version;
						version << HIWORD(buf->dwFileVersionMS) << L'.' << LOWORD(buf->dwFileVersionMS) << L'.';
						version << HIWORD(buf->dwFileVersionLS) << L'.' << LOWORD(buf->dwFileVersionLS);
						std::wstring ELMVersion(version.str());
						versionString = L"<agent_version>";
						versionString += version.str();
						versionString += L"</agent_version>\n";
					}
				}
			}
		}
	}
	WriteIt(hFile, versionString);
}

void SystemInformation::WriteCompName(HANDLE hFile) const
{
	std::wstringstream data;
	DWORD dwLengthRequired = 0;
	std::wstring computerName;

	GetComputerNameEx(ComputerNamePhysicalNetBIOS, NULL, &dwLengthRequired);
	computerName.resize(dwLengthRequired);
	GetComputerNameEx(ComputerNamePhysicalNetBIOS, &computerName[0], &dwLengthRequired);

	data << L"<computer_name>" << computerName.c_str() << L"</computer_name>\n";
	computerName = data.str();
	WriteIt(hFile, computerName);
}
