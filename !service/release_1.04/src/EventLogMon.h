#ifndef SERVICE_H
#define SERVICE_H
#pragma once

#include "stdafx.h"

class EventLogMon
{
public:
	EventLogMon(bool bFindTime, DWORD dwTimeToWait); // throws RuntimeErrorW
	~EventLogMon();
	void Run();
	void SetServiceHandle(SERVICE_STATUS_HANDLE newHandle) {servHandle = newHandle;}
	SERVICE_STATUS_HANDLE GetServiceHandle() const {return servHandle;}
	HUSKEY GetRegKey() const {return hOurKey;}
	bool IsTimed() const {return bTimer;}
	DWORD GetTimeToWait() const {return dwTimeBetweenChecks;}
	HANDLE GetExitEvent() const {return hExitEvent;}
	HANDLE GetPauseEvent() const {return hPauseEvent;}

	static DWORD WINAPI SendXMLFile(LPVOID lpParam);
	static DWORD WINAPI DoMainLoop(LPVOID lpParams);

	static const WCHAR* baseReg;

private:
	bool bTimer;
	HANDLE hExitEvent, hPauseEvent;
	HUSKEY hOurKey;
	SERVICE_STATUS_HANDLE servHandle;
	DWORD dwTimeBetweenChecks;
};

#endif
