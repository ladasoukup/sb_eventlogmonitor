#ifndef STDAFX_H
#define STDAFX_H

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define WIN32_LEAN_AND_MEAN

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.

// Allow use of features specific to Windows 2000 or later.
#define WINVER 0x0501
#define _WIN32_WINNT 0x0501
#define _WIN32_WINDOWS 0x0501

#ifndef UNICODE
#define UNICODE
#define _UNICODE
#endif

#ifndef _MSC_VER
#define VER_SUITE_STORAGE_SERVER            0x00002000
#define VER_SUITE_COMPUTE_SERVER            0x00004000
#endif

// Windows Header Files:
#include <windows.h>
#include <shlwapi.h>

// C/C++ Library Header Files
#include <string>
#include <vector>
#include <cstring> // For wcscmp et al

inline void PopulateStatusToDefault(SERVICE_STATUS& status)
{
	status.dwServiceType        = SERVICE_WIN32_OWN_PROCESS; 
	status.dwCurrentState       = SERVICE_RUNNING;
	status.dwControlsAccepted   = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE | SERVICE_ACCEPT_SHUTDOWN;
	status.dwWin32ExitCode      = NO_ERROR;
	status.dwServiceSpecificExitCode = 0;
	status.dwCheckPoint         = 0;
	status.dwWaitHint           = 0;
}

typedef std::vector<std::wstring> StringCollection;

#endif
