#ifndef EVENT_H
#define EVENT_H
#pragma once

#include "stdafx.h"
#include "Message.h"
#include <memory>

class Event
{
public:
	Event(const EVENTLOGRECORD* eventInf, const std::wstring& logLocation);
	Event(const Event& copy);
	const Event& operator=(const Event& copy);
	DWORD GetType() const {return type;}
	DWORD GetID() const {return eventID;}
	time_t GetGeneratedTime() const {return timeGenerated;}
	SID* GetSID() const {return userSID;}
	std::wstring GetSource() const {return sourceName;}
	std::wstring GetMessageText() const {return eventMessage.get()->GetFullMessage();}
	void SetUserName(const std::wstring& newName) {userName = newName;}
	void WriteXML(const HANDLE hXmlOutput) const;

private:
	std::auto_ptr<Message> eventMessage;
	DWORD eventID, timeGenerated;
	SID* userSID;
	Category cat;
	std::wstring sourceName, userName, location;
	DWORD type; // informational, error etc, as a number
};

#endif
