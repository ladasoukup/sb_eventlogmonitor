#define WIN32_LEAN_AND_MEAN

// we don't use any XP features but the FIXED_INFO declaration needs a winver of 2000SP1 to be accessible
// strange considering it's been around since Win98
#define WINVER 0x0501
#define _WIN32_WINNT 0x0501
#define _WIN32_WINDOWS 0x0501

#include <windows.h>
#include <commctrl.h>
#include "resource.h"
#include <fstream>
#include <string>
#include <sstream>
#include <iphlpapi.h>
#include <lm.h>
#include <wininet.h>
#include "../comfunct.h"
#include <vector>
#include <shlwapi.h>
#include <iomanip>
#include <algorithm>
#include "UTF16.h"
#include <shlobj.h>

INT_PTR CALLBACK MainDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
DWORD ValidateSelections(HWND);
bool GetNetLogonShare(std::wstring& location);
void WriteRegFile(const std::wstring& serverIP, const std::wstring& path, DWORD dwOnlineTime, bool bSecure, DWORD dwEvents);
void GetCurrentIP(std::wstring& serverIP);
DWORD WriteNetlogonScript(const std::wstring& fileName, const std::string& serverPath, DWORD dwTimeInSecs, DWORD dwEvents);
int GetControlText(const HWND hParent, const DWORD controlID, std::wstring& text);
INTERNET_PORT GetPort(std::wstring& server, bool bSecure);
DWORD GetCheckedEvents(HWND hwnd);

namespace
{
	std::vector<std::wstring> language;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hNull, LPSTR lpCmdLine, int nShow)
{
	InitCommonControls();
	DialogBox(hInstance, MAKEINTRESOURCE(CONFIG_DIALOG), NULL, MainDlgProc);
	return 0;
}

INT_PTR CALLBACK MainDlgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		case WM_INITDIALOG:
		{
			std::wifstream strings;
			strings.imbue(std::locale(std::locale::classic(), new UTF16));
			strings.open("strings.txt", std::ios::binary);

			if(strings.is_open())
			{
				WCHAR empty;
				strings.read(&empty, 1);

				// UI Strings
				for(DWORD resVal = PATH_GROUP; resVal <= EVENT_AUDIT_FAILURE; ++resVal)
				{
					std::wstring text;
					std::getline(strings, text, L'\r');
					strings.read(&empty, 1); //Throw away the \n
					//language.push_back(text);
					SetDlgItemText(hwnd, resVal, text.c_str());
				}
				// All Other Messages
				std::wstring text;
				while(std::getline(strings, text, L'\r'))
				{
					language.push_back(text);
					strings.read(&empty, 1); //Throw away the \n
				}
			}
			SendDlgItemMessage(hwnd, SERVER_PROTOCOL, CB_ADDSTRING, 0, (LPARAM)L"http");
			SendDlgItemMessage(hwnd, SERVER_PROTOCOL, CB_ADDSTRING, 0, (LPARAM)L"https");
			SendMessage(hwnd, WM_COMMAND, MAKEWPARAM(CONT_TIME_MODE, 0), 0);
			SetDlgItemInt(hwnd, TIMED_DELAY, 300, FALSE);

			std::wstring currentIP;
			GetCurrentIP(currentIP);
			SetDlgItemText(hwnd, SERVER_IP, currentIP.c_str());
		}
		break;
		case WM_COMMAND: 
		{
			switch(LOWORD(wParam))
			{
				case SAVE_BUTTON:
				{
					DWORD dwControlFailedValidation = 0;
					EnableWindow(GetDlgItem(hwnd, SAVE_BUTTON), FALSE);
					if(dwControlFailedValidation = ValidateSelections(hwnd))
					{
						std::wstringstream error;
						//const DWORD dwPos = VALIDATION_FAILED - CONTROL_BASE;
						const DWORD dwPos = VALIDATION_FAILED - VALIDATION_BASE;
						//error << language[dwPos] << language[dwControlFailedValidation - CONTROL_BASE];
						error << language[dwPos] << language[dwControlFailedValidation - VALIDATION_BASE];
						MessageBox(hwnd, error.str().c_str(), L"EventLogMonitor Configuration", MB_OK | MB_ICONERROR);
					}
					EnableWindow(GetDlgItem(hwnd, SAVE_BUTTON), TRUE);
				}
				break;
				case CONT_TIME_MODE:
				{
					CheckDlgButton(hwnd, CONT_TIME_MODE, BST_CHECKED);
					CheckDlgButton(hwnd, CONT_EVENT_MODE, BST_UNCHECKED);
					EnableWindow(GetDlgItem(hwnd, TIMED_DELAY), TRUE);
				}
				break;
				case CONT_EVENT_MODE:
				{
					CheckDlgButton(hwnd, CONT_EVENT_MODE, BST_CHECKED);
					CheckDlgButton(hwnd, CONT_TIME_MODE, BST_UNCHECKED);
					EnableWindow(GetDlgItem(hwnd, TIMED_DELAY), FALSE);
				}
				break;
			}
		}
		break;
		case WM_CLOSE:
		case WM_DESTROY:
		{
			EndDialog(hwnd, 0);
		}
		break;
		default:
		{
			return FALSE;
		}
		break;
	}
	return TRUE;
}

DWORD ValidateSelections(HWND hwnd)
{
	std::wstring hostServerPath, IP, path;
	HINTERNET hConnect = ConnectHandle(L"EventLogMon Config Network Test");
	DWORD dwLength = 0;
	bool bSecure = false;

	if((GetControlText(hwnd, SERVER_PROTOCOL, hostServerPath)) && (hostServerPath[0] == L'h'))
	{
		if(hostServerPath.find(L"s") != std::wstring::npos)
		{
			bSecure = true;
		}
		hostServerPath += L"://";

		if(GetControlText(hwnd, SERVER_IP, IP))
		{
			INTERNET_PORT port = GetPort(IP, bSecure);
			HINTERNET hServer = ServerConnect(hConnect, IP.c_str(), port);
			hostServerPath += IP;
			hostServerPath += L"/";

			std::wstring rpcPath;
			if(GetControlText(hwnd, PATH_TO_RPC, rpcPath))
			{
				std::wstring::size_type pathStartPos = hostServerPath.length();
				hostServerPath += rpcPath;
				std::replace(hostServerPath.begin(), hostServerPath.end(), L'\\', L'/'); // Backslahes count as escape characters when imported by into the registry so we transform them into forward slashes
				if(hostServerPath.find(L"rpc.php") == std::wstring::npos)
				{
					if(hostServerPath[hostServerPath.length() - 1] != L'/') // check if we have the final path seperator
					{
						hostServerPath += L'/';
					}
					hostServerPath += L"rpc.php";
				}
				path.assign(hostServerPath, pathStartPos, std::wstring::npos);

				HINTERNET hRequest = HttpOpenRequest(hServer, L"Get", path.c_str(), NULL, NULL, NULL, INTERNET_FLAG_NO_UI | INTERNET_FLAG_RELOAD | INTERNET_FLAG_PRAGMA_NOCACHE, NULL);
				if(hRequest)
				{
					if(!HttpSendRequest(hRequest, NULL, 0, NULL, 0))
					{
						InternetCloseHandle(hRequest);
						InternetCloseHandle(hServer);
						InternetCloseHandle(hConnect);
						return NOT_CONNECTABLE;
					}
				}
				InternetCloseHandle(hRequest);
			}
			else
			{
				InternetCloseHandle(hServer);
				return PATH_TO_RPC;
			}
			InternetCloseHandle(hServer);
		} 
		else
		{
			InternetCloseHandle(hConnect);
			return SERVER_IP;
		}
		InternetCloseHandle(hConnect);
	}
	else return SERVER_PROTOCOL;

	DWORD dwTimeInSecs = 0;

	if(IsDlgButtonChecked(hwnd, CONT_TIME_MODE))
	{
		BOOL bTranslated = FALSE;
		dwTimeInSecs = GetDlgItemInt(hwnd, TIMED_DELAY, &bTranslated, FALSE);
		if(dwTimeInSecs <= 0 || !bTranslated)
		{
			return TIMED_DELAY;
		}
	}

	DWORD dwEvents = 0;
	if((dwEvents = GetCheckedEvents(hwnd)) == 0)
	{
		return NO_EVENT_TYPES_SELECTED;
	}

	std::wstringstream message;
	std::wstring fileName;
	if(!GetNetLogonShare(fileName)) // returns false on failure
	{
		WCHAR szDir[MAX_PATH] = L"";
		GetModuleFileName(NULL, szDir, MAX_PATH);
		PathRemoveFileSpec(szDir);
		fileName = szDir;
		message << language[NO_NETLOGON - VALIDATION_BASE] << fileName << L"\r\n";
		message << language[NETLOGON_INSTRUCTIONS - VALIDATION_BASE];
	}
	fileName += L"\\eventlogmon.cfg";
	char* asciiServerPath = ASCIIConversion(hostServerPath.c_str());
	DWORD dwError = WriteNetlogonScript(fileName, asciiServerPath, dwTimeInSecs, dwEvents);
	FreePointer(asciiServerPath);
	if(message.str() != L"")
	{
		MessageBox(hwnd, message.str().c_str(), L"EventLogMonitor Configuration", MB_OK | MB_ICONEXCLAMATION);
		message.str(L"");
	}
	if(dwError) // the test for whether WriteNetlogonScript failed is held until we've displayed the instructions on what should be done with the config file
	{
		message << language[dwError - VALIDATION_BASE] << fileName.erase(fileName.find_last_of(L"\\"));
		MessageBox(hwnd, message.str().c_str(), L"EventLogMonitor Configuration", MB_OK | MB_ICONERROR);
		message.str(L"");
	}
	else
	{
		message << language[CONFIGFILE_SUCCESS - VALIDATION_BASE] << fileName.erase(fileName.find_last_of(L"\\"));
	}

	WriteRegFile(IP, path, dwTimeInSecs, bSecure, dwEvents);
	
	message << L"\r\n" << language[REGFILE_INFO - VALIDATION_BASE];
	MessageBox(hwnd, message.str().c_str(), L"EventLogMonitor Configuration", MB_OK | MB_ICONINFORMATION);
	return 0; // if we errored earlier then the message has already been displayed, returning it here would only make it show again
}

bool GetNetLogonShare(std::wstring& location)
{
	SERVER_INFO_100* si = NULL, getnpSI = {0};
	DWORD numDomains = 0, numTotal = 0, i = 0;
	bool retVal = false;

	if(NetServerEnum(NULL, 100, (LPBYTE*)&si, MAX_PREFERRED_LENGTH, &numDomains, &numTotal, SV_TYPE_DOMAIN_ENUM, NULL, 0))
	{ // It failed, use GetNetworkParams to get the domain name
		FIXED_INFO* fi = NULL;
		DWORD dwLength = 0;
		bool bUseWin32 = false;
		GetNetworkParams(fi, &dwLength);

		fi = (FIXED_INFO*)new(std::nothrow) BYTE[dwLength];
		if(!fi)
		{
			fi = (FIXED_INFO*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwLength);
			bUseWin32 = true;
		}

		if(!GetNetworkParams(fi, &dwLength)) // returns 0 on success
		{
			getnpSI.sv100_name = UnicodeConversion(fi->DomainName);
			numDomains = 1;
			si = &getnpSI;
		}
		if(!bUseWin32)
		{
			delete [] fi;
		}
		else FreePointer(fi);
	}
	for(; i < numDomains; ++i)
	{
		std::wstring domainNetLogon = si[i].sv100_name;
		domainNetLogon.insert(0, L"\\\\");
		domainNetLogon += L"\\NetLogon\\";
		size_t length = domainNetLogon.size();
		bool useWin32 = false;
		WCHAR* buf = new(std::nothrow) WCHAR[length + 1];
		if(!buf)
		{
			buf = (WCHAR*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, (length + 1) * sizeof(WCHAR));
			useWin32 = true;
		}
		domainNetLogon.copy(buf, length);
		buf[length] = L'\0';
		if(SHValidateUNC(NULL, buf, 0)) // the netlogon path exists
		{
			location = domainNetLogon;
			retVal = true;
		}
		if(!useWin32)
		{
			delete [] buf;
		}
		else FreePointer(buf);
		if(retVal == true)
		{
			break;
		}
	}
	if(getnpSI.sv100_name)
	{
		FreePointer(getnpSI.sv100_name);
	}
	NetApiBufferFree(si);
	return retVal;
}

void WriteRegFile(const std::wstring& serverIP, const std::wstring& path, DWORD dwOnlineTime, bool bSecure, DWORD dwEvents)
{
	std::wstringstream regFile;
	DWORD dwSecure = bSecure; // Just so we can output it as a DWORD more easily
	regFile << L"Windows Registry Editor Version 5.00\r\n\r\n";
	regFile << L"[HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\EventLogMonitor]\r\n";
	regFile << L"\"Server\" = \"" << serverIP.c_str() << "\"\r\n";
	regFile << L"\"Path\" = \"" << path.c_str() << "\"\r\n";
	regFile << L"\"Secure\" = dword:" << dwSecure << "\r\n";
	regFile << L"\"OnlineWait\" = dword:" << dwOnlineTime << "\r\n";
	regFile << L"\"TypesToLog\" = dword:" << std::hex << dwEvents;

	WCHAR szPath[MAX_PATH] = L"";
	GetModuleFileName(NULL, szPath, MAX_PATH);
	PathRemoveFileSpec(szPath);

	std::wstring fileName(szPath);
	fileName += L"\\ELMConfig.reg";

	HANDLE hRegFile = CreateFile(fileName.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
	DWORD dwBytesWritten = 0;
	char* asciiFile = ASCIIConversion(regFile.str().c_str());
	WriteFile(hRegFile, asciiFile, (DWORD)regFile.str().length(), &dwBytesWritten, NULL);
	CloseHandle(hRegFile);
	FreePointer(asciiFile);
}

void GetCurrentIP(std::wstring& ip)
{
	DWORD dwBufLen = sizeof(IP_ADAPTER_INFO);
	bool bUseWin32 = false;
	PIP_ADAPTER_INFO pIpInfo = (PIP_ADAPTER_INFO)new(std::nothrow) BYTE[dwBufLen];
	if(pIpInfo == NULL)
	{
		pIpInfo = (PIP_ADAPTER_INFO)HeapAlloc(GetProcessHeap(), 0, dwBufLen);
		bUseWin32 = true;
	}
	if(GetAdaptersInfo(pIpInfo, &dwBufLen) == ERROR_BUFFER_OVERFLOW)
	{
		if(bUseWin32)
		{
			HeapReAlloc(GetProcessHeap(), 0, pIpInfo, dwBufLen);
		}
		else 
		{
			delete [] pIpInfo;
			pIpInfo = (PIP_ADAPTER_INFO)new(std::nothrow) BYTE[dwBufLen];
		}
		GetAdaptersInfo(pIpInfo, &dwBufLen);
	}
	WCHAR* wszIPAddress = UnicodeConversion(pIpInfo->IpAddressList.IpAddress.String);
	ip = wszIPAddress;
	FreePointer(wszIPAddress);
	if(bUseWin32)
	{
		FreePointer(pIpInfo);
	}
	else delete [] pIpInfo;
}

DWORD WriteNetlogonScript(const std::wstring& fileName, const std::string& serverPath, DWORD dwTimeInSecs, DWORD dwEvents)
{
	HANDLE hFile = CreateFile(fileName.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
	if(hFile == NULL || hFile == INVALID_HANDLE_VALUE)
	{
		return CONFIGFILE_FAILED;
	}
	DWORD dwBytesWritten = 0;
	WriteFile(hFile, serverPath.c_str(), (DWORD)serverPath.length(), &dwBytesWritten, NULL);
	WriteFile(hFile, "\r\n", 2, &dwBytesWritten, NULL); // add in the newline

	std::stringstream converter;
	converter << dwTimeInSecs << "\r\n";

	WriteFile(hFile, converter.str().c_str(), (DWORD)converter.str().length(), &dwBytesWritten, NULL);
	converter.str("");
	converter.clear();
	converter << dwEvents;
	WriteFile(hFile, converter.str().c_str(), (DWORD)converter.str().length(), &dwBytesWritten, NULL);
	CloseHandle(hFile);
	return 0;
}

int GetControlText(const HWND hParent, const DWORD controlID, std::wstring& text)
{
	HWND hControl = GetDlgItem(hParent, controlID);
	int length = GetWindowTextLength(hControl);
	text.resize(length + 1); // GetWindowTextLength return doesn't include the NULL
	int retVal = GetWindowText(hControl, &text[0], length + 1);
	text.resize(length); // remove the NULL
	return retVal;
}

INTERNET_PORT GetPort(std::wstring& server, bool bSecure)
{
	std::wstring::size_type pos = std::wstring::npos;
	INTERNET_PORT port;
	if((pos = server.find(L":")) != std::wstring::npos)
	{
		std::wstringstream converter;
		converter << server.substr(pos + 1);
		converter >> port;
	}
	else port = (bSecure ? 443 : 80);
	return port;
}

DWORD GetCheckedEvents(HWND hwnd)
{
	DWORD dwChecked = 0;
	WORD wVal = 1;
	for(DWORD i = EVENT_ERRORS; i <= EVENT_AUDIT_FAILURE; ++i, wVal *= 2)
	{
		if(IsDlgButtonChecked(hwnd, i) == BST_CHECKED)
		{
			dwChecked |= wVal;
		}
	}
	return dwChecked;
}