#ifndef LOG_H
#define LOG_H
#pragma once

#include "stdafx.h"
#include "Event.h"
#include <boost/ptr_container/ptr_vector.hpp>

typedef boost::ptr_vector<Event> EventCollection;
typedef EventCollection::iterator LogEventIter;

class Log
{
public:
	Log(std::wstring& name, HUSKEY hBaseKey); //throws RuntimeErrorW
	~Log();
	void UpdateLastRecordRead(DWORD record) {dwLastRecordRead = record;}
	HANDLE GetWriteEvent() const;
	HANDLE GetLogHandle() const {return hLog;}
	void ResetWriteEvent() {ResetEvent(hWriteEvent);}
	void SetupEvent();
	void SetRegKey(HUSKEY hLogKey) {hKey = hLogKey;}
	bool ReadNewEvents(const DWORD dwEventsToReadBeforeCompletion);
	void WriteLastRecordToRegistry() const;
	void ClearEvents();
	LogEventIter GetEventsBegin() {return events.begin();}
	LogEventIter GetEventsEnd() {return events.end();}
	DWORD GetNumEvents() const {return static_cast<DWORD>(events.size());}

private:
	EventCollection events;
	std::wstring name;
	HUSKEY hKey;
	HANDLE hLog;
	HANDLE hWriteEvent;
	DWORD dwLastRecordRead;
};

#endif
