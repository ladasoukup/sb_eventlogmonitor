#include "stdafx.h"
#include "sids.h"
#include <sddl.h> /* For ConvertSidToStringSid */
#include <map>

sidMap SidLookup::unknownSids;

SidLookup::SidLookup()
{
	if(unknownSids.empty())
	{
		InitializeMap();
	}
}

void SidLookup::GetAccountSID(const WCHAR* wszSIDToFind, std::wstring& accountName) const
{
	sidMap::iterator iter = unknownSids.find(wszSIDToFind);

	if(iter == unknownSids.end())
	{
		accountName = L"Unknown"; /*If we didn't find it in our list, return Unknown */
	}
	else
	{
		accountName = iter->second;
	}
}

void SidLookup::GetAccountName(SID* pSid, std::wstring& wstrAccountName) const
{
	DWORD dwNameChars = 0, dwDomainChars = 0;
	std::wstring domainAccount, accountName;
	SID_NAME_USE sidName;
	WCHAR* szSid = NULL;

	/* This will fail but give us the correct buffer lengths to allocate */
	LookupAccountSid(NULL, pSid, NULL, &dwNameChars, NULL, &dwDomainChars, &sidName);

	if(dwNameChars > 0 && dwDomainChars > 0) /* If we did get buffer lengths, resize the strings and call the function again */
	{
		accountName.resize(dwNameChars - 1); // subtract one since we don't need the NULLs
		domainAccount.resize(dwDomainChars - 1);
		LookupAccountSid(NULL, pSid, &accountName[0], &dwNameChars, &domainAccount[0], &dwDomainChars, &sidName);
	}
	else if(GetLastError() == ERROR_NONE_MAPPED)
	/* No system mapping for this SID, try our own substitution method */
	{
		ConvertSidToStringSid(pSid, &szSid);
		GetAccountSID(szSid, domainAccount);
	}
	else /* Some other error happened */
	{
		wstrAccountName = L"";
		return;
	}
	
	if(domainAccount != L"Unknown") /* If we don't have an unknown SID, copy the domain and/or account name into the buffer */
	{
		if(dwDomainChars > 0) /* If we have a domain identifier, lets concatente the account name on */
		{
			wstrAccountName = domainAccount;
			wstrAccountName += L"\\";
			wstrAccountName += accountName;
		}
		else 
		{
			wstrAccountName = accountName; /* Otherwise just copy the account name over */
		}
	}
	else /* If we got an unknown SID, we'll tack it on in string form */
	{
		wstrAccountName = domainAccount;
		wstrAccountName += L" - ";
		wstrAccountName += szSid;
		LocalFree(szSid);
	}
}

void SidLookup::InitializeMap()
{
	// SID list retrived from http://www.alexkeizer.nl/MicrosoftWin32Security++Enumerating+Sids.aspx

	WCHAR wszSIDs[30][13] = 
	{
		L"S-1-5-32-548", L"S-1-5-32-544", L"S-1-5-7", L"S-1-5-11", L"S-1-5-32-551", L"S-1-5-3",
		L"S-1-3-1", L"S-1-3-3", L"S-1-3-0", L"S-1-3-2", L"S-1-5-1", L"S-1-5-546", L"S-1-5-4",
		L"S-1-2-0", L"S-1-5-2", L"S-1-0-0", L"S-1-5-32-554", L"S-1-5-32-550", L"S-1-5-8",
		L"S-1-5-32-553", L"S-1-5-32-552", L"S-1-5-12", L"S-1-5-10", L"S-1-5-9", L"S-1-5-6",
		L"S-1-5-18", L"S-1-5-32-549", L"S-1-5-13", L"S-1-5-32-545", L"S-1-1-0"
	};

	WCHAR wszSIDNames[30][19] = 
	{
		L"AccountOps", L"Admins", L"AnonymousLogon", L"AuthenticatedUsers", L"BackupOps", L"Batch",
		L"CreatorGroup", L"CreatorGroupServer", L"CreatorOwner", L"CreatorOwnerServer", L"Dialup",
		L"Guests", L"Interactive", L"Local", L"Network", L"Null", L"PreW2kAccess", L"PrintOps",
		L"Proxy", L"RasServers", L"Replicator", L"Restricted Code", L"Self", L"ServerLogon",
		L"Service", L"System", L"SystemOps", L"TerminalServer", L"Users", L"World"
	};

	for(int i = 0; i < 30; ++i)
	{
		unknownSids.insert(std::make_pair(wszSIDs[i], wszSIDNames[i]));
	}
}
