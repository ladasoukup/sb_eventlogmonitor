Configuration tool
------------

To configure, just fill in the protocol, location of the server and rpc.php file and the desired mode for the service to run in.

After clicking the "Save" button in the dialog, the tool will test for the presence of the rpc.php at the location specified. Upon successful contact, the tool will try to save eventlogmon.cfg to \\<your-Domain>\netlogon. The tool will also create ELMConfig.reg in the same directory as the tool. This can be used to configure the service if you do not have a domain environment or do not want to use the domain based configuration.

Modes
------------

The service can be used in two modes, Timed and Online. In Timed mode, all event logs are checked for new entries every x seconds as specified in the configuration tool. In Online mode, individual logs are read when a new event is written to them. This method has a minimum resolution of 5 seconds as imposed by Windows

After initial startup, events are sent when 100 have been collected from a log or after 10 minutes has passed whichever occurs first. This keeps memory consumption to as little as possible while also minimizing the amount of network traffic.


Installation
------------

To install the service, just execute the MSI file. No further user interaction is required. The files will be copied to %ProgramFiles%\EventLogMonitor and the service will be started after setup finishes.

In a domain environment, the service can be deployed using group policy. Upon startup, the service will try to read eventlogmon.cfg from \\<your-Domain>\netlogon, if the file is unavailable the service will then search computers attached to the domain for a netlogon share before attempting to read the file again. eventlogmon.cfg is only read during the initial setup after which the settings are saved to the local registry.

If you are in a non-domain environment or otherwise do not want to use the netlogon method of setup, the ELMConfig.reg file created by the configuration tool must be merged with the local registry before installing the service.

Note: NT4 computers must have Service Pack 6 applied, and Internet Explorer 4 SP2 with the Active Desktop Update. There are no special requirements for installation on 2000 and above.

FAQ's
------------

I'm seeing lots of traffic on the network immediately after installation

When starting for the first time, the service starts reading from the first event in each log. After 300 events have been read, they are output to events.xml (roughly 300 - 500 KB)
and the file is sent to the server. This happens for every 300 events until the service has read all events present on the computer. For example, if there are 5,000 total log entries on a machine, you can expect to see a maximum of 16 files sent to the server.

In some cases it may be prudent to stagger the installation of the service to avoid network performance degredation.

Won't all that reading on startup cause my computer(s) to crawl?

Not at all, the thread that does the reading is set to below normal priority so it will not impact any user experience. Event reading post startup runs at normal priority.

Can the agents connect and send events to a non standard http(s) port?

Yes they can. To use a non-default port add it to the Server IP field on the configuration tool with a colon (:) seperating it from the IP. For example, to use port 79 on the server 192.168.4.36 you would add "192.168.4.36:79" without the quotes to the Server IP field. Likewise to use port 5000 on computer 192.168.110.128 you would add "192.168.110.128:5000" without the quotes to the field.


Known Issues
-------------
