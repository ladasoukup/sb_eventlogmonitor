#ifndef LOGENUM_H
#define LOGENUM_H

#pragma once

#include "stdafx.h"
#include <shlwapi.h>
#include <boost/ptr_container/ptr_vector.hpp>
#include "log.h"

struct LogEnumerator
{
private:
	boost::ptr_vector<Log>& logCollection;
	HUSKEY hBase;
	DWORD dwLogTypes;
public:
	LogEnumerator(HUSKEY hBaseKey, StringCollection& logNames, boost::ptr_vector<Log>& outLogs, DWORD dwTypesToLog);
	void operator()(std::wstring& logName);
};

#endif
