#ifndef SYSINFO_H
#define SYSINFO_H

#pragma once

#include "stdafx.h"
#include <boost/scoped_array.hpp>

namespace SystemInformation
{
	void GetOsVersion(std::wstring& product);
	BOOL IsWOW64();
	void WriteIt(HANDLE hFile, std::wstring& data);
	void WriteDescription(HANDLE hFile);
	void WriteDomain(HANDLE hFile);
	void WriteTimeOffset(HANDLE hFile);
	void WriteOSVersion(HANDLE hFile);
	void WriteAgentVersion(HANDLE hFile);
	void WriteCompName(HANDLE hFile);
	void GetRegString(HKEY hKey, const std::wstring& value, std::wstring& buf);
	DWORD GetAllProcessIDs(boost::scoped_array<DWORD>& pids);
	bool GetInteractiveProcessToken(HANDLE hProcess, HANDLE& hRetProcToken);
	bool TogglePriv(const WCHAR* privName, bool enabled);
}

#endif
