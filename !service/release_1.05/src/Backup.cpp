#include "StdAfx.h"
#include "Backup.h"
#include "EventFile.h"
#include "RuntimeErrorW.h"
#include "errors.h"
#include "EvtLogMessages.h"
#include "ServerInteract.h"
#include <shlobj.h>
#include "Network.h"

Backup::Backup()
{
	CollectFiles();
}

void Backup::CollectFiles()
{
	std::wstring folder;
	FindSaveFolder(folder);
	std::wstring searchPath(folder);
	searchPath += L"Evt*.tmp";
	WIN32_FIND_DATA wfd = {0};
	HANDLE hSearch = FindFirstFile(searchPath.c_str(), &wfd);
	if(hSearch != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(!(wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				std::wstring file(folder);
				file += wfd.cFileName;
				try
				{
					EventFile* evtFile = new(std::nothrow) EventFile(file, GENERIC_READ);
					if(evtFile != NULL)
					{
						files.push_back(evtFile);
					}
				}
				catch(const RuntimeErrorW& err)
				{
					// ERROR_SHARING_VIOLATION indicates the file we found is the one were
					// currently logging to
					if(err.errorVal() != ERROR_SHARING_VIOLATION)
					{
						Errors::ReportException(err, MSG_FAIL_OPEN_LOGFILE, EVENTLOG_WARNING_TYPE);
					}
				}
			}
		}
		while(FindNextFile(hSearch, &wfd));
	}
	FindClose(hSearch);
}

void Backup::CommitAll()
{
	if(!files.empty())
	{
		bool bRetry = true;
		do
		{
			ServerInteract server;
			if(server.HasServerPath())
			{
				for(EvtFileIter iter = files.begin(); iter != files.end(); ++iter)
				{
					if(server.SendFile(*iter))
					{
						iter->DeleteOnClose(); // the events have been sent so we don't need the file anymore
					}
				}
			}
			else
			{
				Network::ReadConfigAsLoggedOnUser();
				bRetry = !bRetry;
			}
		}
		while(!bRetry);
	}
}

DWORD WINAPI Backup::ThreadEntry(LPVOID lpParams)
{
	Backup b;
	b.CommitAll();
	// Allow others to send now that we've finished
	// We need to have our own copy of the event handle
	// (as opposed to using the HANDLE in LogManager)
	// because the main loop thread may finish and destruct LogManager while were sending, 
	// and so invalidating the event handle. With our own copy that's not possible
	HANDLE hSendEvent = OpenEvent(EVENT_MODIFY_STATE, FALSE, L"EventLogMonSendEvent");
	SetEvent(hSendEvent);
	CloseHandle(hSendEvent);
	UNREFERENCED_PARAMETER(lpParams);
	return 0;
}

void Backup::FindSaveFolder(std::wstring& folderLoc)
{
	static std::wstring folder;
	if(folder.empty())
	{
		WCHAR* temp = NULL;
		_get_wpgmptr(&temp); // gets the path to the process
		std::wstring buf = temp;
		buf.erase(buf.rfind(L"\\") + 1); // remove the program name
		buf += L"EvtXml\\";
		if(!PathIsDirectory(buf.c_str()))
		{
			CreateDirectory(buf.c_str(), NULL);
		}
		folder = buf;
	}
	folderLoc = folder; 
}

void SendFilesToServer()
{
	// Start off the sending of the backup files
	// in a seperate thread, we don't need the handle so we immediately close it
	CloseHandle(CreateThread(NULL, 0, Backup::ThreadEntry, NULL, 0, NULL));
}
