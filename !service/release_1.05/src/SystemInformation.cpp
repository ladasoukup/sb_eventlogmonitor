#include "StdAfx.h"
#include "SystemInformation.h"
#include <iphlpapi.h>
#include "comfunct.h"
#include <sstream>
#include "lexical_cast_proxy.h"
#include <boost/scoped_array.hpp>
#include <psapi.h>
#include "sids.h"
#include "Network.h"

namespace SystemInformation
{
	void WriteIt(HANDLE hFile, std::wstring& data)
	{
		DWORD dwBytes = 0;
		WriteFile(hFile, data.c_str(), static_cast<DWORD>(data.length() * sizeof(WCHAR)), &dwBytes, NULL);
	}

	void WriteDomain(HANDLE hFile)
	{
		boost::scoped_array<BYTE> fiBuf;
		Network::FillNetworkParams(fiBuf);
		FIXED_INFO* fi = reinterpret_cast<FIXED_INFO*>(fiBuf.get());

		std::wstring domain = L"<computer_domain>";

		if(fi != NULL)
		{
			WCHAR* szDomain = UnicodeConversion(fi->DomainName);
			if(szDomain)
			{
				domain += szDomain;
			}
			FreePointer(szDomain);
		}
		domain += L"</computer_domain>\n";

		WriteIt(hFile, domain);
	}

	void WriteDescription(HANDLE hFile)
	{
		std::wstring compDesc = L"<computer_description>";
		std::wstring description;
		HKEY hKey = NULL;
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"System\\CurrentControlSet\\Services\\lanmanserver\\parameters", 0, KEY_READ, &hKey) == ERROR_SUCCESS)
		{
			GetRegString(hKey, L"srvcomment", description);
			RegCloseKey(hKey);
		}
		compDesc += description;
		compDesc += L"</computer_description>\n";

		WriteIt(hFile, compDesc);
	}

	void WriteOSVersion(HANDLE hFile)
	{
		std::wstring product = L"Windows ";
		GetOsVersion(product);
		std::wstring prodString(L"<computer_os>");
		prodString += product;
		prodString += L"</computer_os>\n";
		WriteIt(hFile, prodString);
	}

	void GetOsVersion(std::wstring& product)
	{
		std::wstringstream productBuf;
		OSVERSIONINFOEX os = {0};
		os.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		GetVersionEx(reinterpret_cast<OSVERSIONINFO*>(&os));

		// determine whether we're running on an x64 machine
		std::wstring x64;
		if(IsWOW64())
		{
			x64 = L"x64 ";
		}

		// We now support NT 4
		if(os.dwMajorVersion == 4)
		{
			productBuf << L"NT4 ";
			if(os.wProductType == VER_NT_WORKSTATION)
			{
				productBuf << L"Workstation ";
			}
			else
			{
				productBuf << L"Server ";
			}
		}
		else if(os.dwMajorVersion == 5)
		{
			switch(os.dwMinorVersion)
			{
				case 0:
				{
					productBuf << L"2000 ";
				}
				break;
				case 1:
				{
					productBuf << L"XP ";
					if(os.wSuiteMask & VER_SUITE_PERSONAL)
					{
						productBuf << L"Home ";
					}
					else productBuf << L"Professional ";
					productBuf << x64;
					productBuf << L"Edition ";
				}
				break;
				case 2:
				{
					productBuf << L"Server 2003 " << x64;
				}
				break;
			}
		}
		else if(os.dwMajorVersion == 6)
		{
			if(os.wProductType == VER_NT_WORKSTATION)
			{
				productBuf << L"Vista ";
				if(os.wSuiteMask & VER_SUITE_PERSONAL)
				{
					productBuf << L"Home " << L"Edition ";
				}
				productBuf << x64;
			}
			else productBuf << L"Server 2008 " << x64;
		}
	 
		if(os.wSuiteMask & VER_SUITE_DATACENTER)
		{
			productBuf << L"Datacenter Edition ";
		}
		else if(os.wSuiteMask & VER_SUITE_COMPUTE_SERVER)
		{
			productBuf << L"Compute Cluster Edition ";
		}
		else if(os.wSuiteMask & VER_SUITE_ENTERPRISE)
		{
			productBuf << L"Enterprise Edition ";
		}
		else if(os.wSuiteMask & VER_SUITE_BLADE)
		{
			productBuf << L"Web Edition ";
		}
		else if(os.wSuiteMask & VER_SUITE_STORAGE_SERVER)
		{
			productBuf << L"Storage Edition ";
		}
		productBuf << os.szCSDVersion;
		product += productBuf.str();
	}

	BOOL IsWOW64()
	{
		typedef BOOL (WINAPI*WOWFUNC)(HANDLE, BOOL*);
		BOOL bIs64Bit = FALSE;
		WOWFUNC isWow64 = reinterpret_cast<WOWFUNC>(GetProcAddress(GetModuleHandle(L"kernel32.dll"), "IsWow64Process"));
		if(isWow64 != NULL)
		{
			isWow64(GetCurrentProcess(), &bIs64Bit);
		}
		return bIs64Bit;
	}

	void WriteTimeOffset(HANDLE hFile)
	{
		TIME_ZONE_INFORMATION tzi = {0};
		DWORD dwRes = GetTimeZoneInformation(&tzi);
		LONG bias = tzi.Bias;
		if(dwRes == TIME_ZONE_ID_STANDARD || dwRes == TIME_ZONE_ID_UNKNOWN)
		{
			bias += tzi.StandardBias;
		}
		else
		{
			bias += tzi.DaylightBias;
		}
		// the bias is the number of minutes to UTC, not from UTC
		// So the bias we get is the inverse of what we want
		// For example British Summer Time is UTC + 1
		// but the function above will return a bias of -60 minutes
		// since that's the number of minutes to be added to the 
		// local time to get to UTC
		float biasInHours = static_cast<float>(bias) / 60;
		biasInHours *= -1;

		std::wstring hourBias = boost::lexical_cast<std::wstring>(biasInHours);
		if(biasInHours > 0)
		{
			hourBias.insert(0, L"+");
		}

		std::wstring timeOffset = L"<time_offset>";
		timeOffset += hourBias;
		timeOffset += L"</time_offset>\n";

		WriteIt(hFile, timeOffset);
	}

	void WriteAgentVersion(HANDLE hFile)
	{
		static std::wstring versionString;
		if(versionString.empty())
		{
			std::wstring fileName(MAX_PATH, 0);
			GetModuleFileName(NULL, &fileName[0], MAX_PATH);
			DWORD dwHandle = 0;
			DWORD dwByteSize = GetFileVersionInfoSize(fileName.c_str(), &dwHandle);
			if(dwByteSize > 0)
			{
				boost::scoped_array<BYTE> verBuf(new (std::nothrow) BYTE[dwByteSize]);
				BYTE* ver = verBuf.get();
				
				if(ver != NULL)
				{
					if(GetFileVersionInfo(fileName.c_str(), dwHandle, dwByteSize, ver))
					{
						VS_FIXEDFILEINFO* buf = NULL;
						UINT size = 0;
						if(VerQueryValue(ver, L"\\", reinterpret_cast<LPVOID*>(&buf), &size))
						{
							std::wstringstream version;
							version << HIWORD(buf->dwFileVersionMS) << L'.' << LOWORD(buf->dwFileVersionMS) << L'.';
							version << HIWORD(buf->dwFileVersionLS) << L'.' << LOWORD(buf->dwFileVersionLS);
							std::wstring ELMVersion(version.str());
							versionString = L"<agent_version>";
							versionString += version.str();
							versionString += L"</agent_version>\n";
						}
					}
				}
			}
		}
		WriteIt(hFile, versionString);
	}

	void WriteCompName(HANDLE hFile)
	{
		std::wstring data(L"<computer_name>");
		DWORD dwLengthRequired = 0;
		std::wstring computerName;

		GetComputerName(NULL, &dwLengthRequired);
		computerName.resize(dwLengthRequired - 1); // we don't need the NULL
		GetComputerName(&computerName[0], &dwLengthRequired);

		data += computerName;
		data += L"</computer_name>\n";
		WriteIt(hFile, data);
	}

	void GetRegString(HKEY hKey, const std::wstring& value, std::wstring& buf)
	{
		DWORD dwType = REG_SZ, dwLength = 0;
		LSTATUS err = RegQueryValueEx(hKey, value.c_str(), NULL, &dwType, NULL, &dwLength);
		if(!err && dwLength > 1) // if length is 1 then the value is empty
		{
			buf.resize(dwLength / sizeof(WCHAR)); // RegQueryValueEx returns the number of bytes not the number of characters 
			RegQueryValueEx(hKey, value.c_str(), NULL, &dwType, reinterpret_cast<LPBYTE>(&buf[0]), &dwLength);
			buf.resize(buf.size() - 1); // the original size includes the NULL, which we don't need to store
		}
	}

	DWORD GetAllProcessIDs(boost::scoped_array<DWORD>& pids)
	{
		DWORD nPIDs = 30, bufNeeded = 0;
		do
		{
			pids.reset(new(std::nothrow) DWORD[nPIDs]);
			if (!pids.get() || ! EnumProcesses(pids.get(), nPIDs * sizeof(DWORD), &bufNeeded ) )
			{
				return 0;
			}
			nPIDs += 8;
		} while ( (int)((nPIDs * sizeof(DWORD)) - bufNeeded ) <= 0 );

		// return the actual number of PIDs read
		return bufNeeded / sizeof(DWORD);
	}

	// part of the code from http://win32.mvps.org/security/opt_gti.cpp
	bool GetInteractiveProcessToken( HANDLE hProcess, HANDLE& hRetProcToken)
	{
		HANDLE hProcToken = NULL;
		bool success = false;

		// these three keep track of what we found in the token
		bool haveLocalSid = false, haveLogonSid = false, haveInteractiveSid = false;

		// begin looking
		if (OpenProcessToken(hProcess, TOKEN_QUERY | TOKEN_DUPLICATE | TOKEN_IMPERSONATE, &hProcToken) )
		{
			Sids sids;

			// get buffer size needed
			DWORD dwNeeded = 0;
			GetTokenInformation(hProcToken, TokenGroups, NULL, 0, &dwNeeded );

			// make it so
			boost::scoped_array<BYTE> tokenGroups(new (std::nothrow) BYTE[dwNeeded]);
			PTOKEN_GROUPS pGroups = reinterpret_cast<PTOKEN_GROUPS>(tokenGroups.get());

			if ((tokenGroups.get()) && (GetTokenInformation(hProcToken, TokenGroups, static_cast<LPVOID>(tokenGroups.get()), dwNeeded, &dwNeeded ) ))
			{
				if ( pGroups->GroupCount > 0 )
				{
					for (DWORD i = 0; i < pGroups->GroupCount; ++i )
					{
						// test for Local SID
						if (EqualSid(pGroups->Groups[i].Sid, sids.GetLocal()))
						{
							haveLocalSid = true;
						}
						// test for Logon SID
						if (Sids::IsLogon(pGroups->Groups[i].Sid))
						{
							haveLogonSid = true;
						}
						// test for Interactive SID
						if (EqualSid(pGroups->Groups[i].Sid, sids.GetInteractive()))
						{
							haveInteractiveSid = true;
						}
					}
				}
			}

			if (haveLocalSid && haveInteractiveSid && haveLogonSid )
			{
				hRetProcToken = hProcToken;
				success = true;
			}
			else CloseHandle(hProcToken);
		}
		return success;
	}

	bool TogglePriv(const WCHAR *privName, bool enabled)
	{
		bool success = false;
		HANDLE hToken = NULL;

		if(OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		{
			LUID privValue = {0};
			if(LookupPrivilegeValue(NULL, privName, &privValue))
			{
				TOKEN_PRIVILEGES tkp;
				tkp.PrivilegeCount = 1;
				tkp.Privileges[0].Luid = privValue;
				tkp.Privileges[0].Attributes = enabled ? SE_PRIVILEGE_ENABLED : 0;

				success = (AdjustTokenPrivileges( hToken, FALSE, &tkp, sizeof tkp, NULL, NULL ) != 0);
			}
			CloseHandle(hToken);
		}
		return success;
	}
}
