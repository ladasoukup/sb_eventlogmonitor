#ifndef LOG_H
#define LOG_H
#pragma once

#include "stdafx.h"
#include "Event.h"
#include <boost/ptr_container/ptr_vector.hpp>

typedef boost::ptr_vector<Event> EventCollection;
typedef EventCollection::iterator LogEventIter;
typedef EventCollection::const_iterator ConstLogEventIter;

class Sids;

class Log
{
public:
	Log(std::wstring& name, HUSKEY hBaseKey, DWORD dwLogTypes); //throws RuntimeErrorW
	~Log();
	void UpdateLastRecordRead(DWORD record) {dwLastRecordRead = record;}
	HANDLE GetWriteEvent() const {return hWriteEvent;}
	HANDLE GetLogHandle() const {return hLog;}
	void ResetWriteEvent() {ResetEvent(hWriteEvent);}
	void SetupEvent();
	bool ReadNewEvents(const DWORD dwEventsToReadBeforeCompletion); // throws RuntimeErrorW
	void WriteLastRecordToRegistry() const;
	void ClearEvents();
	void AddEvent(const EVENTLOGRECORD* eventInfo, Sids& sids);
	LogEventIter GetEventsBegin() {return events.begin();}
	LogEventIter GetEventsEnd() {return events.end();}
	ConstLogEventIter GetEventsBegin() const {return events.begin();}
	ConstLogEventIter GetEventsEnd() const {return events.end();}
	DWORD GetNumEvents() const {return static_cast<DWORD>(events.size());}
	void Refresh();

private:
	EventCollection events;
	std::wstring name;
	HUSKEY hKey;
	HANDLE hLog;
	HANDLE hWriteEvent;
	DWORD dwLastRecordRead;
	DWORD dwTypesToLog;
};

#endif
