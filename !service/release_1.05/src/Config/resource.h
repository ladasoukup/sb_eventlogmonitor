//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Config.rc
//
#define IDI_ICON1                       100
#define CONFIG_DIALOG                   101
#define CONTROL_BASE                    1001
#define PATH_GROUP                      1001
#define PROTOCOL_TEXT                   1002
#define SERVER_IP_TEXT                  1003
#define RPC_PATH_TEXT                   1004
#define MODE_GROUP                      1005
#define CONT_TIME_MODE                  1006
#define SECONDS_TEXT                    1007
#define CONT_EVENT_MODE                 1008
#define EVENT_MODE_TEXT                 1009
#define SAVE_BUTTON                     1010
#define EVENTS_GROUP                    1011
#define EVENT_ERRORS                    1012
#define EVENT_WARNING                   1013
#define EVENT_INFORMATION               1014
#define EVENT_AUDIT_SUCCESS             1015
#define EVENT_AUDIT_FAILURE             1016

#define VALIDATION_BASE					1050
#define SERVER_PROTOCOL                 1050
#define SERVER_IP                       1051
#define PATH_TO_RPC                     1052
#define TIMED_DELAY                     1053
#define NOT_CONNECTABLE                 1054
#define CONFIGFILE_SUCCESS              1055
#define CONFIGFILE_FAILED               1056
#define REGFILE_INFO                    1057
#define VALIDATION_FAILED               1058
#define NO_NETLOGON                     1059
#define NETLOGON_INSTRUCTIONS           1060
#define NO_EVENT_TYPES_SELECTED			1061

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1062
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
