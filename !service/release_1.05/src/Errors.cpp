#include "StdAfx.h"
#include "Errors.h"
#include "RuntimeErrorW.h"
#include "lexical_cast_proxy.h"
#include "Message.h" // for GetModuleMessage

namespace Errors
{
	void LogEventMessage(WORD wSeverity, DWORD dwMsg, StringCollection& insertionStrings)
	{
		HANDLE hEvent = RegisterEventSource(NULL, L"EventLogMonitor");
		if(hEvent)
		{
			std::vector<LPCWSTR> pointers;
			for(StringCollection::size_type i = 0; i < insertionStrings.size(); ++i)
			{
				pointers.push_back(insertionStrings[i].c_str());
			}
			ReportEvent(hEvent, wSeverity, 0, dwMsg, NULL, static_cast<WORD>(insertionStrings.size()), 0, &pointers[0], NULL);
			DeregisterEventSource(hEvent);
		}
	}

	// This is a seperate function because it's not allowed to have
	// objects that require unwinding in a SEH exception
	void ThrowException(const WCHAR* error)
	{
		throw RuntimeErrorW(error, GetLastError());
	}

	void ReportException(const RuntimeErrorW& err, DWORD message, WORD severity)
	{
		LPWSTR errorDesc = NULL;
		StringCollection strings;
		std::wstring errorNum = boost::lexical_cast<std::wstring>(err.errorVal());
		strings.push_back(err.what());
		strings.push_back(errorNum);
		if(GetModuleMessage(NULL, FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, err.errorVal(), errorDesc, NULL) == false)
		{
			strings.push_back(errorDesc);
			LocalFree(errorDesc);
		}
		else strings.push_back(L"Error retrieving description");
		LogEventMessage(severity, message, strings);	
	}
}
