#pragma once

#include "stdafx.h"
#include <wininet.h>
#include "eventfile.h"

class ServerInteract
{
public:
	ServerInteract();
	~ServerInteract();
	bool SendFile(const EventFile& evtFile);
	bool PostXML(const std::string& postPath, HANDLE hFile) const;
	bool HasServerPath() const {return !server.empty();}

private:
	//bool GetKey(const std::wstring& path, std::wstring& newKey) const;
	bool ConnectToServer();
	void ReadData(const HINTERNET hRequest, std::string& data) const;

	HINTERNET hConnection, hServer;
	std::string server, path;
	BOOL bSecure;
	INTERNET_PORT port;
};
