#ifndef EVENT_H
#define EVENT_H
#pragma once

#include "stdafx.h"
#include "Message.h"
#include <memory>
#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

class Event
{
public:
	Event(const EVENTLOGRECORD* eventInf, const std::wstring& logLocation);
	Event(const Event& copy);
	const Event& operator=(const Event& copy);
	DWORD GetType() const {return type;}
	DWORD GetID() const {return eventID;}
	DWORD GetGeneratedTime() const {return timeGenerated;}
	SID* GetSID() const {return reinterpret_cast<SID*>(sidBuf.get());}
	std::wstring GetSource() const {return sourceName;}
	std::wstring GetMessageText() const {return eventMessage->GetFullMessage();}
	void SetUserName(const std::wstring& newName) {userName = newName;}
	void WriteXML(const HANDLE hXmlOutput) const;

private:
	boost::shared_ptr<Message> eventMessage;
	DWORD eventID, timeGenerated;
	boost::shared_array<BYTE> sidBuf;
	Category cat;
	std::wstring sourceName, userName, location;
	DWORD type; // informational, error etc, as a number
};

#endif
