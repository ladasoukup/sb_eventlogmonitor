#include "StdAfx.h"
#include "Event.h"
#include "sids.h"
#include <sstream>
#include <algorithm>

Event::Event(const EVENTLOGRECORD* eventInf, const std::wstring& logLocation) : location(logLocation)
{
	switch(eventInf->EventType)
	{
		// The receiving web service uses 0-1-2-3-4-5 as valid event types
		// and not 0-1-2-4-8-16 as Windows does. So we map those higher than 2
		// into the value expected by the server script
		case EVENTLOG_INFORMATION_TYPE:
		{
			type = 3;
		}
		break;
		case EVENTLOG_AUDIT_SUCCESS:
		{
			type = 4;
		}
		break;
		case EVENTLOG_AUDIT_FAILURE:
		{
			type = 5;
		}
		break;
		default: // 0, 1 and 2 are the same for both
		{
			type = eventInf->EventType;
		}
		break;
	}
	eventID = LOWORD(eventInf->EventID);

	// we use this to avoid nesting reinterpret_casts in the pointer arithmetic sections below
	const BYTE* eventInfoBase = reinterpret_cast<const BYTE*>(eventInf); 

	timeGenerated = eventInf->TimeGenerated;
	if(eventInf->UserSidLength > 0)
	{
		// SID's are of variable length
		// the old method only worked coincedentally (because the 
		// EVENTLOGRECORD was still on the stack)
		sidBuf.reset(new (std::nothrow) BYTE[eventInf->UserSidLength]);
		PSID dest;
		if((dest = reinterpret_cast<PSID>(sidBuf.get())) != NULL)
		{
			PSID source = const_cast<PSID>(reinterpret_cast<const void*>(eventInfoBase + eventInf->UserSidOffset));
			CopySid(eventInf->UserSidLength, dest, source);
		}
	}
	cat.id = eventInf->EventCategory;

	sourceName = reinterpret_cast<const WCHAR*>(eventInfoBase + 56);
	DWORD_PTR stringOffset = eventInf->StringOffset;

	std::wstring details;
	std::vector<DWORD_PTR> insertionOffsets;

	for(WORD i = 0; i < eventInf->NumStrings; ++i)
	{
		insertionOffsets.push_back(reinterpret_cast<DWORD_PTR>(eventInfoBase+stringOffset));
		std::wstring messagePart = reinterpret_cast<const WCHAR*>(eventInfoBase + stringOffset);
		DWORD offsetAdvance = static_cast<DWORD>(messagePart.length() + 1);
		offsetAdvance *= sizeof(WCHAR);
		stringOffset += offsetAdvance;
		details += messagePart;
		details += L", ";
	}

	eventMessage.reset(new(std::nothrow) Message(sourceName, logLocation, eventInf->EventID, insertionOffsets, details, cat));

	if(cat.id == 0)
	{
		cat.name = L"None";
	}
}

void Event::WriteXML(const HANDLE hXmlOutput) const
{
	std::wstringstream formatter;
	formatter << L"<event>\n";
	formatter << L"<code>" << eventID << L"</code>\n";
	formatter << L"<type>" << type << L"</type>\n";
	formatter << L"<category>" << cat.name.c_str() << L"</category>\n";
	formatter << L"<logfile>" << location.c_str() << L"</logfile>\n";
	formatter << L"<message>" << eventMessage.get()->GetFullMessage().c_str() << L"</message>\n";
	formatter << L"<source>" << sourceName.c_str() << L"</source>\n";
	formatter << L"<time-generated>" << timeGenerated << L"</time-generated>\n";
	formatter << L"<user>" << userName.c_str() << L"</user>\n";
	formatter << L"</event>\n";
	
	DWORD dwBytesWritten = 0;

	WriteFile(hXmlOutput, formatter.str().c_str(), static_cast<DWORD>(formatter.str().size() * sizeof(WCHAR)), &dwBytesWritten, NULL);
}
