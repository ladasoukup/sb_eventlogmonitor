#ifndef RUNTIME_ERROR_W
#define RUNTIME_ERROR_W

#include "stdafx.h"

#pragma once

class RuntimeErrorW
{
public:
	explicit RuntimeErrorW(const std::wstring& err, DWORD dwErr) : text(err), dwLastError(dwErr) {}
	const WCHAR* what() const {	return text.c_str();}
	DWORD errorVal() const { return dwLastError; }
private:
	std::wstring text;
	DWORD dwLastError;
};



#endif
