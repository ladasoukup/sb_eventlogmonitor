#ifndef NETWORK_H
#define NETWORK_H

#pragma once

#include "stdafx.h"
#include <boost/scoped_array.hpp>
#include <iphlpapi.h>

// stores the results from ReadConfiguration
struct Config
{
	std::wstring server;
	std::wstring path;
	DWORD dwTimeToWait; // only used in EventLogMon constructor
	Config() : dwTimeToWait(0) {} // online mode is default
};

namespace Network
{
	DWORD WINAPI ReadConfigAsLoggedOnUser(LPVOID lpParams = NULL);
	void FindDomainAndReadConfig(Config& conf);
	void FindDomains(StringCollection& domains);
	bool FindNetlogonShare(std::wstring& netlogonShareLocation);
	bool ServerHasNetlogon(const std::wstring& server);
	bool ReadConfiguration(const std::wstring& domain, Config& conf);
	void FindServers(StringCollection& servers);
	void FillNetworkParams(boost::scoped_array<BYTE>& fi);
	void FillAdaptersInfo(boost::scoped_array<BYTE>& ip);
	void CollectIPStrings(IP_ADDR_STRING* start, StringCollection& address);
}

#endif
