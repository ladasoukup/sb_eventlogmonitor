#include "StdAfx.h"
#include "Network.h"
#include <lm.h>
#include <iphlpapi.h>
#include "comfunct.h"
#include "EventLogMon.h"
#include <boost/scoped_array.hpp>
#include "lexical_cast_proxy.h"
#include <fstream>
#include "LogManager.h"
#include "SystemInformation.h"

namespace Network
{
	DWORD WINAPI ReadConfigAsLoggedOnUser(LPVOID lpParams)
	{
		Config* conf = reinterpret_cast<Config*>(lpParams);
		// try to acquire SeDebugPrivilege, if fail, then do without
		// TogglePriv(SE_DEBUG_NAME, true);

		boost::scoped_array<DWORD> pids;
		DWORD nPids = SystemInformation::GetAllProcessIDs(pids);

		for (DWORD i = 0; i < nPids; ++i)
		{
			HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pids[i] );
			if(!hProcess)
			{
				continue;
			}

			HANDLE hIntToken = NULL;
			// now, to the meat of the matter
			if (SystemInformation::GetInteractiveProcessToken(hProcess, hIntToken))
			{
				HANDLE hImpToken = NULL;
				// Get an impersonation token from the process (primary) one we just acquired
				DuplicateToken(hIntToken, SecurityImpersonation, &hImpToken);

				// on NT4 the LocalSystem account doesn't have enough priviledges
				// to read from the network shares, so we use the currently 
				// logged on user's permissions, and try with those
				// (defaultly, all members who are part of the Users
				// group have permission to read from the netlogon share)
				//
				// NT4 has to read the configuration from \\server\netlogon
				// instead of \\domain\netlogon as it doesn't have any notion of DFS
				// this means the finding and reading of the configuration will take longer
				SetThreadToken(NULL, hImpToken);

				Config temp;
				Network::FindDomainAndReadConfig(temp);
				// Ensure we stop impersonation
				SetThreadToken(NULL, NULL);
				if(conf)
				{
					*conf = temp;
				}
				CloseHandle(hIntToken);
				CloseHandle(hImpToken);
				CloseHandle(hProcess);
				break;
			}
			else 
			CloseHandle(hProcess);
		}
		// we no longer need SeDebugPriv
		//TogglePriv(SE_DEBUG_NAME, false);
		return 0;
	}

	void FindDomainAndReadConfig(Config& conf)
	{
		StringCollection domains;
		
		FindDomains(domains);

		std::wstring shareLocation;
		if(FindNetlogonShare(shareLocation))
		{
			domains.push_back(shareLocation);
		}

		for(StringCollection::iterator iter = domains.begin(); iter != domains.end(); ++iter)
		{
			if(ReadConfiguration(*iter, conf))
			{
				break;
			}
		}
	}
	
	bool FindNetlogonShare(std::wstring& netLogonLocation)
	{
		bool success = false;
		StringCollection servers;

		FindServers(servers);		

		for(StringCollection::size_type i = 0; i < servers.size(); ++i)
		{
			if(ServerHasNetlogon(servers[i]))
			{
				netLogonLocation = servers[i];
				success = true;
				break;
			}
		}
		return success;
	}

	bool ReadConfiguration(const std::wstring& domain, Config& conf)
	{
		std::wstring configFile(domain);
		configFile.insert(0, L"\\\\");
		configFile += L"\\Netlogon\\eventlogmon.cfg";

		char* asciiName = ASCIIConversion(configFile.c_str());
		std::wifstream config(asciiName);
		FreePointer(asciiName);

		StringCollection lines;
		if(config.is_open())
		{
			std::wstring line;
			while(std::getline(config, line))
			{
				lines.push_back(line);
			}
			// End Impersonation here so we get our Service rights back
			// and are able to write to our registry key
			// which the impersonated user might not be able to
			SetThreadToken(NULL, NULL);
		}

		bool success = false;

		if(!lines.empty())
		{
			conf.server = lines[0];
			BOOL bSecure = FALSE;
			if(conf.server.find(L"https") != std::wstring::npos)
			{
				conf.server.erase(0, 8); // remove the protocol
				bSecure = TRUE;
			} else conf.server.erase(0, 7);

			SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Secure", REG_DWORD, &bSecure, sizeof(BOOL));

			std::wstring::size_type serverEnd = conf.server.find_first_of(L"/"); //find the / after the server
			conf.path = conf.server.substr(serverEnd + 1); // make a new string out of the rest of the server path from the position after the first / 
			conf.server.erase(serverEnd); // get rid of the elements we just copied to path
			
			SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Server", REG_SZ, conf.server.c_str(), static_cast<DWORD>(conf.server.length() * sizeof(WCHAR)));
			SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"Path", REG_SZ, conf.path.c_str(), static_cast<DWORD>(conf.path.length() * sizeof(WCHAR)));

			DWORD dwTypesToLog = 0x1F; // all types the default
			try
			{
				conf.dwTimeToWait = boost::lexical_cast<DWORD>(lines[1]);
				if(lines.size() > 2)
				{
					dwTypesToLog = boost::lexical_cast<WORD>(lines[2]);
				}
			}
			catch(const boost::bad_lexical_cast& bl)
			{
				// catch the exception but do nothing else
				// as the parameters are at sane default values
				UNREFERENCED_PARAMETER(bl);
			}

			SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"OnlineWait", REG_DWORD, &conf.dwTimeToWait, sizeof(DWORD));
			SHSetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"TypesToLog", REG_DWORD, &dwTypesToLog, sizeof(DWORD));
			success = true;
		}
		return success;
	}

	void FindDomains(StringCollection& domains)
	{
		DWORD numDomains = 0, numTotal = 0;
		SERVER_INFO_100* si = NULL, getnpSI = {0};

		if(NetServerEnum(NULL, 100, reinterpret_cast<LPBYTE*>(&si), MAX_PREFERRED_LENGTH, &numDomains, &numTotal, SV_TYPE_DOMAIN_ENUM, NULL, 0))
		{
			boost::scoped_array<BYTE> fiBuffer;
			FillNetworkParams(fiBuffer);

			PFIXED_INFO fi = reinterpret_cast<PFIXED_INFO>(fiBuffer.get());
			if(fi)
			{
				getnpSI.sv100_name = UnicodeConversion(fi->DomainName);
				numDomains = 1; // GetNetworkParams only returns 1 domain
				si = &getnpSI;
			}
		}
		for(DWORD i = 0; i < numDomains; ++i)
		{
			domains.push_back(si[i].sv100_name);
		}
		if(getnpSI.sv100_name)
		{
			FreePointer(getnpSI.sv100_name);
		}
		NetApiBufferFree(si);
	}

	bool ServerHasNetlogon(const std::wstring& server)
	{
		SHARE_INFO_0* si = NULL;
		bool success = false;

		std::wstring serverAsShare(server);
		serverAsShare.insert(0, L"\\\\");

		DWORD dwErr = NetShareGetInfo(const_cast<LPWSTR>(serverAsShare.c_str()), L"NetLogon", 0, reinterpret_cast<LPBYTE*>(&si));
		if(dwErr == NERR_Success)
		{
			success = true;
			NetApiBufferFree(si);
		}
		return success;
	}

	void FindServers(StringCollection& servers)
	{
		DWORD serversRead = 0, totalServers = 0;
		PSERVER_INFO_100 si = NULL;
		if(NetServerEnum(NULL, 100, reinterpret_cast<LPBYTE*>(&si), MAX_PREFERRED_LENGTH, &serversRead, &totalServers, 
					  SV_TYPE_ALL, NULL, 0) == NERR_Success)
		{
			for(DWORD i = 0; i < serversRead; ++i)
			{
				servers.push_back(si[i].sv100_name);
			}
			NetApiBufferFree(si);
		}
		else
		{
			boost::scoped_array<BYTE> fi(NULL), ip(NULL);
			FillNetworkParams(fi);

			PFIXED_INFO pFi = reinterpret_cast<PFIXED_INFO>(fi.get());
			IP_ADDR_STRING* pIPAddr = &(pFi->DnsServerList);
			CollectIPStrings(pIPAddr, servers);

			FillAdaptersInfo(ip);
			PIP_ADAPTER_INFO pIp = reinterpret_cast<PIP_ADAPTER_INFO>(ip.get());
			CollectIPStrings(&(pIp->GatewayList), servers);
		}
	}

	void CollectIPStrings(IP_ADDR_STRING* start, StringCollection& address)
	{
		std::string ip;
		IP_ADDR_STRING* pIPAddr = start;
		while(pIPAddr && ((ip = pIPAddr->IpAddress.String) != ""))
		{
			PWSTR ipAddress = UnicodeConversion(ip.c_str());
			address.push_back(ipAddress);
			FreePointer(ipAddress);
			pIPAddr = pIPAddr->Next;
		}
	}

	void FillNetworkParams(boost::scoped_array<BYTE>& fi)
	{
		DWORD dwLength = 0;
		GetNetworkParams(NULL, &dwLength);
		fi.reset(new (std::nothrow) BYTE[dwLength]);
		PFIXED_INFO pFi = reinterpret_cast<PFIXED_INFO>(fi.get());
		GetNetworkParams(pFi, &dwLength);
	}

	void FillAdaptersInfo(boost::scoped_array<BYTE>& ip)
	{
		DWORD dwLength = 0;
		GetAdaptersInfo(NULL, &dwLength);
		ip.reset(new (std::nothrow) BYTE[dwLength]);
		PIP_ADAPTER_INFO pIp = reinterpret_cast<PIP_ADAPTER_INFO>(ip.get());
		GetAdaptersInfo(pIp, &dwLength);
	}
}
