#ifndef BACKUP_H
#define BACKUP_H

#pragma once

#include "stdafx.h"
#include <boost/ptr_container/ptr_vector.hpp>
#include "eventfile.h"

typedef boost::ptr_vector<EventFile> EvtFileCollection;
typedef boost::ptr_vector<EventFile>::iterator EvtFileIter;

void SendFilesToServer();

class Backup
{
public:
	Backup();
	static void FindSaveFolder(std::wstring& folder);
	void CommitAll();
	static DWORD WINAPI ThreadEntry(LPVOID lpParams);

private:
	EvtFileCollection files;
	void CollectFiles();
};

#endif
