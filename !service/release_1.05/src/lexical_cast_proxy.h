// lexical_cast header generates a "potentially uninitialized local variable 'result' used"
// error with the /W4 compiler switch so we disable the warning to shut it up

#pragma warning(push)
#pragma warning(disable : 4701)
#include <boost/lexical_cast.hpp>
#pragma warning(pop)
