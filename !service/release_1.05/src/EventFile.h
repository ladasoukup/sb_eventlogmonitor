#ifndef EVENTFILE_H
#define EVENTFILE_H

#pragma once

#include "stdafx.h"
#include "LogManager.h" // for LogIter

void GetNewFileName(std::wstring& newFile);

class EventFile
{
public:
	EventFile(ACCESS_MASK perms); // throws RuntimeErrorW
	EventFile(const std::wstring& file, ACCESS_MASK perms); // throws RuntimeErrorW
	~EventFile();
	HANDLE GetHandle() const { return hFile;}
	void WriteEnd();
	void WriteStart();
	void DeleteOnClose();
	EventFile& operator<<(const Log& log);

private:
	void Init(ACCESS_MASK perms);
	std::wstring fileName;
	CRITICAL_SECTION cSection;
	HANDLE hFile;
	bool bDelete;
};


#endif
