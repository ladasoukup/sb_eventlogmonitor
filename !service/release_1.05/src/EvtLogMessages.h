 /* EvtLogMessages.txt contains the text for the Agents event log postings. 
 If you change it in any way you should recompile it with mc.exe 
 which is found in the Platform SDK's bin folder, or in Visual Studio's
 bin directory with the following command:

 mc.exe -h <path_to_this_folder> -r <path_to_this_folder> EvtLogMessages.txt

 If you add Unicode characters, you'll also need to pass both the -u and -U options
 to make mc.exe interpret the input file as Unicode.
 */
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_SYSTEM                  0x0
#define FACILITY_STUBS                   0x3
#define FACILITY_RUNTIME                 0x2
#define FACILITY_IO_ERROR_CODE           0x4


//
// Define the severity codes
//
#define MESSAGE_WARNING                  0x2
#define MESSAGE_SUCCESS                  0x0
#define MESSAGE_INFORMATIONAL            0x1
#define MESSAGE_ERROR                    0x3


//
// MessageId: MSG_CRASH
//
// MessageText:
//
// EventLogMonitor Agent encountered an exception and exited.%r
// A crash dump has been created at:%r
// %1.%r
// Please submit this dump to:%r
// http://sourceforge.net/tracker/?func=add&group_id=183663&atid=906148
//
#define MSG_CRASH                        ((DWORD)0xC0020001L)

//
// MessageId: MSG_FAIL_LOG_OPEN
//
// MessageText:
//
// EventLogMonitor Agent failed to create/open a registry key to cache information about the %1 log.%r
// Error: %2 - %3%r
// Duplicate events may be reported next time the service starts.%r
//
#define MSG_FAIL_LOG_OPEN                ((DWORD)0x80020002L)

//
// MessageId: MSG_FAIL_START
//
// MessageText:
//
// EventLogMonitor Agent failed to start because of the following error:%r
// %1%r
// Details: %2 - %3
//
#define MSG_FAIL_START                   ((DWORD)0xC0020003L)

//
// MessageId: MSG_FAIL_OPEN_LOGFILE
//
// MessageText:
//
// Event file %1 could not be opened. Events contained in this file have not been sent to the server.%r
// Details: %2 - %3
//
#define MSG_FAIL_OPEN_LOGFILE            ((DWORD)0x80020004L)

