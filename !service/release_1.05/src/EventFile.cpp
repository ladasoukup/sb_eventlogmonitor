#include "StdAfx.h"
#include "EventFile.h"
#include "Errors.h"
#include "RuntimeErrorW.h"
#include "SystemInformation.h"
#include "log.h"
#include "backup.h"
#include <algorithm>
#include "Event.h"
#include <boost/bind.hpp>

EventFile::EventFile(ACCESS_MASK perms) : bDelete(false) // throws RuntimeErrorW
{
	GetNewFileName(fileName);
	Init(perms);
}

EventFile::EventFile(const std::wstring& file, ACCESS_MASK perms) : fileName(file), bDelete(false) // throws RuntimeErrorW
{
	Init(perms);
}

EventFile::~EventFile()
{
	EnterCriticalSection(&cSection);
	WriteEnd();
	LeaveCriticalSection(&cSection);
	DeleteCriticalSection(&cSection);
	CloseHandle(hFile);
	if(bDelete)
	{
		DeleteFile(fileName.c_str());
	}
}

void EventFile::Init(ACCESS_MASK perms)
{
	hFile = CreateFile(fileName.c_str(), perms, 0, NULL, OPEN_ALWAYS, 
		FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile == NULL || hFile == INVALID_HANDLE_VALUE)
	{
		Errors::ThrowException(fileName.c_str());
	}
	InitializeCriticalSection(&cSection);
	WriteStart();
}

void EventFile::WriteStart()
{
	using namespace SystemInformation;
	const BYTE BOM[2] = {0xFF, 0xFE}; // The Byte Order Mark
	DWORD dwBytesWritten = 0;

	const WCHAR* xmlHeader = L"<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n<events>\n";
	DWORD dwBytesToWrite = static_cast<DWORD>(wcslen(xmlHeader) * sizeof(WCHAR));

	EnterCriticalSection(&cSection);
	WriteFile(hFile, BOM, 2, &dwBytesWritten, NULL);
	WriteFile(hFile, xmlHeader, dwBytesToWrite, &dwBytesWritten, NULL);

	WriteCompName(hFile);
	WriteDescription(hFile);
	WriteDomain(hFile);
	WriteOSVersion(hFile);
	WriteTimeOffset(hFile);
	WriteAgentVersion(hFile);

	FlushFileBuffers(hFile);
	LeaveCriticalSection(&cSection);
}

void EventFile::WriteEnd()
{
	const WCHAR endingXML[] = L"\n</events>";
	DWORD dwBytesWritten = 0;

	EnterCriticalSection(&cSection);

	WriteFile(hFile, endingXML, sizeof(endingXML) - sizeof(WCHAR), &dwBytesWritten, NULL);
	FlushFileBuffers(hFile);

	LeaveCriticalSection(&cSection);
}

void EventFile::DeleteOnClose()
{
	bDelete = true;
}

void GetNewFileName(std::wstring& newFile)
{
	WCHAR backupName[MAX_PATH] = L"";
	std::wstring folder;
	Backup::FindSaveFolder(folder);

	// Save the event files in <user>/Local Settings/Application Data
	GetTempFileName(folder.c_str(), L"Evt", 0, backupName);
	newFile = backupName;
}

EventFile& EventFile::operator<<(const Log& log)
{
	EnterCriticalSection(&cSection);
	std::for_each(
		log.GetEventsBegin(),
		log.GetEventsEnd(),
		boost::bind(&Event::WriteXML, _1, hFile)
		);
	LeaveCriticalSection(&cSection);
	return *this;
}
