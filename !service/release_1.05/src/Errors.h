#ifndef ERRORS_H
#define ERRORS_H

#pragma once

#include "stdafx.h"
#include "RuntimeErrorW.h"

namespace Errors
{
	void LogEventMessage(WORD dwSeverity, DWORD dwMsg, StringCollection& insertionStrings);
	void ThrowException(const WCHAR* error);
	void ReportException(const RuntimeErrorW& err, DWORD message, WORD severity);
}

#endif
