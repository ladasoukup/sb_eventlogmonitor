#include "stdafx.h"
#include "sids.h"
#include <map>
#include <sstream>
#include <iomanip>

sidMap Sids::unknownSids;

Sids::Sids()
{
	// build the interactive SID
	SID_IDENTIFIER_AUTHORITY siaNT = SECURITY_NT_AUTHORITY; // "-5-"
	AllocateAndInitializeSid(&siaNT, 1, 4, 0, 0, 0, 0, 0, 0, 0, &interactive);

	// build "BUILTIN\LOCAL" SID
	SID_IDENTIFIER_AUTHORITY siaLSA = SECURITY_LOCAL_SID_AUTHORITY;
	AllocateAndInitializeSid(&siaLSA, 1, 0, 0, 0, 0, 0, 0, 0, 0, &local);
	if(unknownSids.empty())
	{
		InitializeMap();
	}
}

Sids::~Sids()
{
	FreeSid(local);
	FreeSid(interactive);
}

void Sids::FindAccount(const std::wstring& sidToFind, std::wstring& accountName) const
{
	sidMap::iterator iter = unknownSids.find(sidToFind);

	if(iter == unknownSids.end())
	{
		accountName = L"Unknown"; /*If we didn't find it in our list, return Unknown */
	}
	else
	{
		accountName = iter->second;
	}
}

void Sids::GetAccountName(SID* pSid, std::wstring& wstrAccountName) const
{
	DWORD dwNameChars = 0, dwDomainChars = 0;
	std::wstring domainAccount, accountName, sid;
	SID_NAME_USE sidName;

	/* This will fail but give us the correct buffer lengths to allocate */
	LookupAccountSid(NULL, pSid, NULL, &dwNameChars, NULL, &dwDomainChars, &sidName);

	if(dwNameChars > 0 && dwDomainChars > 0) /* If we did get buffer lengths, resize the strings and call the function again */
	{
		accountName.resize(dwNameChars - 1); // subtract one since we don't need the NULLs
		domainAccount.resize(dwDomainChars - 1);
		LookupAccountSid(NULL, pSid, &accountName[0], &dwNameChars, &domainAccount[0], &dwDomainChars, &sidName);
	}
	else if(GetLastError() == ERROR_NONE_MAPPED)
	/* No system mapping for this SID, try our own substitution method */
	{
		ConvertToString(pSid, sid);
		FindAccount(sid, domainAccount);
	}
	else /* Some other error happened */
	{
		wstrAccountName = L"";
		return;
	}
	
	if(domainAccount != L"Unknown") /* If we don't have an unknown SID, copy the domain and/or account name into the buffer */
	{
		if(dwDomainChars > 0) /* If we have a domain identifier, lets concatente the account name on */
		{
			wstrAccountName = domainAccount;
			wstrAccountName += L"\\";
			wstrAccountName += accountName;
		}
		else 
		{
			wstrAccountName = accountName; /* Otherwise just copy the account name over */
		}
	}
	else /* If we got an unknown SID, we'll tack it on in string form */
	{
		wstrAccountName = domainAccount;
		wstrAccountName += L" - ";
		wstrAccountName += sid;
	}
}

void Sids::InitializeMap()
{
	// SID list retrived from http://www.alexkeizer.nl/MicrosoftWin32Security++Enumerating+Sids.aspx

	WCHAR wszSIDs[30][13] = 
	{
		L"S-1-5-32-548", L"S-1-5-32-544", L"S-1-5-7", L"S-1-5-11", L"S-1-5-32-551", L"S-1-5-3",
		L"S-1-3-1", L"S-1-3-3", L"S-1-3-0", L"S-1-3-2", L"S-1-5-1", L"S-1-5-546", L"S-1-5-4",
		L"S-1-2-0", L"S-1-5-2", L"S-1-0-0", L"S-1-5-32-554", L"S-1-5-32-550", L"S-1-5-8",
		L"S-1-5-32-553", L"S-1-5-32-552", L"S-1-5-12", L"S-1-5-10", L"S-1-5-9", L"S-1-5-6",
		L"S-1-5-18", L"S-1-5-32-549", L"S-1-5-13", L"S-1-5-32-545", L"S-1-1-0"
	};

	WCHAR wszSIDNames[30][19] = 
	{
		L"AccountOps", L"Admins", L"AnonymousLogon", L"AuthenticatedUsers", L"BackupOps", L"Batch",
		L"CreatorGroup", L"CreatorGroupServer", L"CreatorOwner", L"CreatorOwnerServer", L"Dialup",
		L"Guests", L"Interactive", L"Local", L"Network", L"Null", L"PreW2kAccess", L"PrintOps",
		L"Proxy", L"RasServers", L"Replicator", L"Restricted Code", L"Self", L"ServerLogon",
		L"Service", L"System", L"SystemOps", L"TerminalServer", L"Users", L"World"
	};

	for(int i = 0; i < 30; ++i)
	{
		unknownSids.insert(std::make_pair(wszSIDs[i], wszSIDNames[i]));
	}
}

void Sids::ConvertToString(SID *pSid, std::wstring& string) const
{
	// we don't need LoadLibrary here as Advapi32.dll will already be loaded
	// because were statically linked to it

	typedef BOOL (WINAPI*CONVERT)(PSID, LPTSTR*);
	HMODULE hAdvapi = GetModuleHandle(L"Advapi32.dll");
	CONVERT sidToString = reinterpret_cast<CONVERT>(GetProcAddress(hAdvapi, "ConvertSIDToStringSIDW"));
	if(sidToString)
	{
		LPTSTR sidBuf = NULL;
		sidToString(pSid, &sidBuf);
		string = sidBuf;
		LocalFree(sidBuf);
	}
	else // this is NT4, code adapted from http://msdn2.microsoft.com/en-us/library/aa376396.aspx
	{
		std::wstringstream sidBuilder;

		// Get the identifier authority value from the SID.

		PSID_IDENTIFIER_AUTHORITY psia = GetSidIdentifierAuthority(pSid);

		// Get the number of subauthorities in the SID.

		DWORD dwSubAuthorities = *GetSidSubAuthorityCount(pSid);

		// Add 'S' prefix and revision number to the string.

		sidBuilder << L"S-" << SID_REVISION << L"-";

		// Add a SID identifier authority to the string.

		if ( (psia->Value[0] != 0) || (psia->Value[1] != 0) )
		{
			sidBuilder << std::hex << psia->Value[0] << std::setw(2);
			for(int i = 1; i < 6; ++i)
			{
				sidBuilder << psia->Value[i];
			}
		}
		else
		{
			sidBuilder << static_cast<ULONG>(
						(psia->Value[5]      )   +
						(psia->Value[4] <<  8)   +
						(psia->Value[3] << 16)   +
						(psia->Value[2] << 24)   );
		}

		// Add SID subauthorities to the string.
		//
		for (DWORD dwCounter=0 ; dwCounter < dwSubAuthorities; ++dwCounter)
		{
			sidBuilder << L"-" << *GetSidSubAuthority(pSid, dwCounter);
		}
		string = sidBuilder.str();
	}
}

// part of the code from http://win32.mvps.org/security/opt_gti.cpp
bool Sids::IsLogon( PSID pSid )
{
	SID_IDENTIFIER_AUTHORITY sia = SECURITY_NT_AUTHORITY;

	// a logon SID has: sia = 5, subauth count = 3, first subauth = 5
	// the following three lines test these three conditions
	if ((memcmp( GetSidIdentifierAuthority( pSid ), &sia, sizeof(sia)) == 0) // is sia == 5?
		&& (*GetSidSubAuthorityCount( pSid ) == 3) // is subauth count == 3?
		&& (*GetSidSubAuthority( pSid, 0 ) == 5) // first subauth == 5?
		)
		return true;
	else
		return false;
}

BOOL Sids::IsValid(const PSID pSid) const
{
	return IsValidSid(pSid);
}
