#include "stdafx.h"
#include <string>
#include <map>

typedef std::map<std::wstring, std::wstring> sidMap;

class Sids
{
private:
	static sidMap unknownSids;
	void InitializeMap();
	void ConvertToString(SID* pSid, std::wstring& string) const;
	PSID local, interactive;
public:
	Sids();
	~Sids();
	PSID GetLocal() const {return local;}
	PSID GetInteractive() const {return interactive;}
	void FindAccount(const std::wstring& sidToFind, std::wstring& wszAccountName) const;
	void GetAccountName(SID* pSid, std::wstring& wstrAccountName) const;
	static bool IsLogon( PSID pSid );
	BOOL IsValid(const PSID pSid) const;

};
