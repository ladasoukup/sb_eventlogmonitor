#ifndef LOGMANAGER_H
#define LOGMANAGER_H
#pragma once

#include "stdafx.h"
#include "log.h"
#include <boost/ptr_container/ptr_vector.hpp>
#include <memory>

typedef boost::ptr_vector<Log> LogCollection;
typedef LogCollection::iterator logIter;
typedef LogCollection::const_iterator logIter_const;

class EventFile; // forward declaration

class LogManager
{
public:
	LogManager(HUSKEY hBaseKey);
	~LogManager();
	void AddLog(Log* addedLog) {logs.push_back(addedLog);}
	void FlushLastReadMessages() const;
	void DoEventWaitingMode();
	void DoTimedWaitingMode(const DWORD dwTimeToWait);
	void CatchUp(DWORD dwEventsReadBeforeSend = 100);
	bool LogCatchUp(Log& log, DWORD dwEventsToRead = 100);
	void ResetLogFile();
	void ClearAllEvents();
	void WriteLogXML(Log& log) const;
	HANDLE GetSendEvent() const {return hSendEvent;}

	static void CALLBACK Do10MinSend(LPVOID lpContext, DWORD dwTimerLowValue, DWORD dwTimerHighValue);

private:
	LogCollection logs;
	HANDLE h10MinSendTimer;
	void FindAndOpenLogs(HUSKEY hBaseKey, DWORD dwTypesToLog);
	std::auto_ptr<EventFile> file;
	HANDLE hSendEvent;
};

#endif
