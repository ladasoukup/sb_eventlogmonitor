#include "StdAfx.h"
#include "EventLogMon.h"
#include "LogManager.h"
#include "comfunct.h"
#include "EvtLogMessages.h"
#include "RuntimeErrorW.h"
#include "errors.h"
#include "Network.h"

const WCHAR* EventLogMon::baseReg = L"System\\CurrentControlSet\\Services\\EventLogMonitor";

EventLogMon::EventLogMon(bool bFindTime, DWORD dwTimeToWait)
: bTimer(false),
  hExitEvent(NULL),
  hPauseEvent(NULL),
  hSendEvent(NULL),
  hOurKey(NULL),
  dwTimeBetweenChecks(dwTimeToWait) // throws(RuntimeErrorW)
{
	if(SHRegOpenUSKey(baseReg, KEY_READ | KEY_WRITE, NULL, &hOurKey, TRUE) != ERROR_SUCCESS)
	{
		std::wstring err = L"Unable to open registry key: ";
		err += baseReg;
		Errors::ThrowException(err.c_str());
	}

	hSendEvent = CreateEvent(NULL, TRUE, TRUE, L"EventLogMonSendEvent");

	// Create the Pause event, it is initially set to signalled so any attempts
	// to wait on it fail and execution continues.
	hPauseEvent = CreateEvent(NULL, TRUE, TRUE, L"EventLogMonPauseEvent");

	// Create the exit event which, when signalled, will stop event listening and end the program
	hExitEvent = CreateEvent(NULL, TRUE, FALSE, L"EventLogMonExitEvent");

	// don't display critical error boxes since they will not be displayed on the users desktop
	// so will not be dismissed and so stall our service
	SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX | SEM_NOALIGNMENTFAULTEXCEPT); 

	if(bFindTime)
	{
		CloseHandle(CreateThread(NULL, 0, StubFindThread, this, 0, NULL));
	}
	else bTimer = (dwTimeBetweenChecks > 0);
}

EventLogMon::~EventLogMon()
{
	if(hOurKey) SHRegCloseUSKey(hOurKey);
	if(hPauseEvent) CloseHandle(hPauseEvent);
	if(hExitEvent) CloseHandle(hExitEvent);
	if(hSendEvent) CloseHandle(hSendEvent);
}

void EventLogMon::Run()
{
	// Waiting for this thread makes sure that we don't pull out the service from under the feet of
	// the log manager which may be processing last minute events.

	SERVICE_STATUS status = {0};
	PopulateStatusToDefault(status);

	// Perfom main processing in another thread leaving this one to wait for exit
	HANDLE hMainThread = CreateThread(NULL, 0, DoMainLoop, this, 0, NULL);

	// Wait for the main loop to finish before we begin exiting the process
	while(WaitForSingleObject(hMainThread, INFINITE) != WAIT_OBJECT_0)
	{} // empty loop ensures we get back to waiting on the main loop thread exit ASAP

	CloseHandle(hMainThread);

	status.dwCurrentState = SERVICE_STOP_PENDING;
	status.dwWaitHint = 3000;

	// Now wait for any event sending we might be doing to finish
	// The event will be set when the Backup class finishes doing its work
	do
	{
		// This tells windows that we're still alive and kicking, stopping it from 
		// terminating the process forcefully
		++status.dwCheckPoint;
		SetServiceStatus(GetServiceHandle(), &status);
	}
	while(WaitForSingleObject(hSendEvent, 1000) == WAIT_TIMEOUT);
}

DWORD WINAPI EventLogMon::DoMainLoop(LPVOID lpParams)
{
	EventLogMon* logMon = static_cast<EventLogMon*>(lpParams);

	LogManager logMan(logMon->GetRegKey());

	// could be loads of events here (especially when first run)
	// so we turn down the thread priority so everybody else has 
	// a chance to work
	HANDLE hThread = GetCurrentThread();
	SetThreadPriority(hThread, THREAD_PRIORITY_BELOW_NORMAL);
	logMan.CatchUp();
	SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);

	// Resume processing as normal

	if(logMon->IsTimed())
	{
		logMan.DoTimedWaitingMode(logMon->GetTimeToWait());
	}
	else
	{
		logMan.DoEventWaitingMode();
	}
	return 0;
}

DWORD WINAPI EventLogMon::StubFindThread(LPVOID lpParam)
{
	Config conf;
	HANDLE hFindEvent = OpenEvent(EVENT_MODIFY_STATE, FALSE, L"EventLogMonFindEvent");
	HANDLE hRealFindThread = CreateThread(NULL, 0, &Network::ReadConfigAsLoggedOnUser, &conf, 0, NULL);
	WaitForSingleObject(hRealFindThread, INFINITE);
	if(conf.dwTimeToWait > 0)
	{
		EventLogMon* logMon = reinterpret_cast<EventLogMon*>(lpParam);
		logMon->dwTimeBetweenChecks = conf.dwTimeToWait;
		logMon->bTimer = true;
	}
	SetEvent(hFindEvent);
	CloseHandle(hFindEvent);
	CloseHandle(hRealFindThread);
	return 0;
}