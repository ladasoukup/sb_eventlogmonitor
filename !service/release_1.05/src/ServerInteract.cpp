#include "StdAfx.h"
#include "ServerInteract.h"
#include "LogManager.h"
#include "EventLogMon.h"
#include <wininet.h>
#include <boost/algorithm/string/case_conv.hpp> // string tolower
#include <boost/scoped_ptr.hpp>
#include "comfunct.h"
#include <sstream>
#include "SystemInformation.h"

ServerInteract::ServerInteract() 
: port(80),
  bSecure(FALSE),
  hConnection(ConnectHandleA("EventLogMonitor")),
  hServer(NULL)
{
	using SystemInformation::GetRegString;

	bSecure = SHRegGetBoolUSValue(EventLogMon::baseReg, L"Secure", TRUE, false);

	// New way of querying the registry avoids setting hardcoded limits on our string buffers
	HKEY hKey = NULL;
	LSTATUS err = RegOpenKeyEx(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, 0, KEY_READ, &hKey);
	if(!err)
	{
		std::wstring wideServer;
		GetRegString(hKey, L"Server", wideServer);
		if(!wideServer.empty())
		{
			// Wide version of the HTTP functions aren't available on NT4 so the reg strings
			// need to be ascii-ficated 
			char* asciiServer = ASCIIConversion(wideServer.c_str());
			server = asciiServer;
			FreePointer(asciiServer);
			std::string::size_type pos = std::string::npos;
			if((pos = server.find(":")) != std::string::npos)
			{
				std::stringstream converter;
				converter << server.substr(pos + 1);
				converter >> port;
				server.erase(pos);
			}
			else port = (bSecure ? 443 : 80);
		}
		std::wstring widePath;
		GetRegString(hKey, L"Path", widePath);
		RegCloseKey(hKey);
		// now convert the path to ascii
		char* asciiPath = ASCIIConversion(widePath.c_str());
		path = asciiPath;
		FreePointer(asciiPath);
	}
	ConnectToServer();
}

ServerInteract::~ServerInteract()
{
	InternetCloseHandle(hConnection);
	InternetCloseHandle(hServer);
}

bool ServerInteract::ConnectToServer()
{
	hServer = ServerConnectA(hConnection, server.c_str(), port);
	return hServer != NULL;
}

bool ServerInteract::SendFile(const EventFile& evtFile)
{
	return PostXML(path, evtFile.GetHandle());
}

void ServerInteract::ReadData(const HINTERNET hRequest, std::string& file) const
{
	bool bFinished = false;
	DWORD dwLength = 0;
	InternetQueryDataAvailable(hRequest, &dwLength, 0, 0);
	
	if(dwLength <= 0)
	{
		return;
	}

	while(!bFinished)
	{
		DWORD dwChunkSize = 0;

		InternetQueryDataAvailable(hRequest, &dwChunkSize, 0, 0);

		if(dwChunkSize == 0)
		{
			bFinished = true;
			break;
		}

		boost::scoped_array<BYTE> chunkBuf(new (std::nothrow) BYTE[dwChunkSize + 1]);
		BYTE* chunk = chunkBuf.get();

		if(chunk != NULL)
		{
			DWORD dwBytesDownloaded = 0, dwCount = 0;

			while(dwBytesDownloaded != dwChunkSize)
			{
				if(!InternetReadFile(hRequest, &chunk[dwCount], dwChunkSize - dwCount, &dwBytesDownloaded))
				{
					bFinished = true;
					break;
				}
				dwCount += dwBytesDownloaded;
			}
			chunk[dwChunkSize] = '\0'; // ensure NULL termination
			file += reinterpret_cast<char*>(chunk);
		}
	}
}

bool ServerInteract::PostXML(const std::string &postPath, HANDLE hFile) const
{
	LPCSTR szTypes[] = {"*/*", NULL};
	DWORD dwFlags = INTERNET_FLAG_NO_CACHE_WRITE;
	bool bSuccess = false;
	if(bSecure)
	{
		dwFlags |= INTERNET_FLAG_SECURE;
	}
	HINTERNET hPost = HttpOpenRequestA(hServer, "POST", postPath.c_str(), NULL, NULL, szTypes, dwFlags, 0);
	if(hPost != NULL)
	{
		HANDLE hMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, L"xmlData");
		LPVOID xmlStart = MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0);
		if(HttpSendRequestA(hPost, NULL, 0, xmlStart, GetFileSize(hFile, NULL)))
		{
			std::string data;
			ReadData(hPost, data);

			std::string::size_type off = data.find("\r\n");
			if(off != std::string::npos)
			{
				data.erase(off);
			}
			boost::algorithm::to_lower(data);

			if(data == "ok")
			{
				bSuccess = true;
			}
		}
		UnmapViewOfFile(xmlStart);
		CloseHandle(hMapping);
		InternetCloseHandle(hPost);
	}
	return bSuccess;
}
