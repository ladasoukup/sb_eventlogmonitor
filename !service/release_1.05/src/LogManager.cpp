#include "StdAfx.h"
#include "LogManager.h"
#include "EventLogMon.h" // for baseReg
#include <boost/scoped_array.hpp>
#include "LogEnum.h"
#include "Errors.h"
#include "EventFile.h"
#include "Backup.h"
#include "RuntimeErrorW.h"
#include <algorithm>
#include <boost/bind.hpp>

LogManager::LogManager(HUSKEY hBaseKey) : file(NULL)
{
	DWORD dwType = REG_DWORD, dwSize = sizeof(DWORD), dwTypesToLog = 0x1F; // all types is the default
	SHGetValue(HKEY_LOCAL_MACHINE, EventLogMon::baseReg, L"TypesToLog", &dwType, &dwTypesToLog, &dwSize);
	FindAndOpenLogs(hBaseKey, dwTypesToLog);

	ResetLogFile();

	hSendEvent = OpenEvent(SYNCHRONIZE | EVENT_MODIFY_STATE, FALSE, L"EventLogMonSendEvent");

	h10MinSendTimer = CreateWaitableTimer(NULL, FALSE, L"EventLogMon10MinSendTimer");	

	// Send Every 10 minutes (in milliseconds)
	LONG period = 10 * 60 * 1000;
	LARGE_INTEGER li = {0};
	li.QuadPart = Int32x32To64(period, -10000); // First firing in 10 minutes (in 100 nanosecond units), relative time
	SetWaitableTimer(h10MinSendTimer, &li, period,
					 Do10MinSend, this, FALSE);
}

LogManager::~LogManager()
{
	CancelWaitableTimer(h10MinSendTimer);
	std::for_each(logs.begin(), logs.end(), boost::bind(&LogManager::WriteLogXML, this, _1));
	// This doesn't destroy the event as we still have the HANDLE returned
	// by CreateEvent in EventLogMon
	CloseHandle(hSendEvent);
}

void LogManager::CatchUp(DWORD dwEventsReadBeforeWrite)
{
    int threadPriority = GetThreadPriority(GetCurrentThread());
	if(threadPriority == THREAD_PRIORITY_BELOW_NORMAL)
	{
		dwEventsReadBeforeWrite = 300;
	}
	std::for_each(logs.begin(), logs.end(), boost::bind(&LogManager::LogCatchUp, this, _1, dwEventsReadBeforeWrite));
}

bool LogManager::LogCatchUp(Log& log, DWORD dwEventsReadBeforeWrite)
{
	bool bLogReset = false;
	try
	{
		while(log.ReadNewEvents(dwEventsReadBeforeWrite)) // returns true when we're over the event size threshold
		{
			WriteLogXML(log);
			ResetLogFile();
		}
	}
	catch(const RuntimeErrorW& err)
	{
		// the event log has had its events cleared since we last read it
		// so we must refresh the handle in order to be able to read again
		log.Refresh();
		bLogReset = true;
		UNREFERENCED_PARAMETER(err);
	}
	return bLogReset;
}

void LogManager::DoEventWaitingMode()
{
	std::vector<HANDLE> events;
	for(logIter iter = logs.begin(); iter != logs.end(); ++iter)
	{
		iter->SetupEvent();
		events.push_back(iter->GetWriteEvent());
	}

	HANDLE hExitEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonExitEvent");
	HANDLE hPauseEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonPauseEvent");

	events.push_back(hExitEvent);

	DWORD eventSignalled = 0;
	const DWORD dwNumHandles = static_cast<DWORD>(events.size());
	const DWORD dwExitEventPosition = (dwNumHandles - 1) - WAIT_OBJECT_0;

	if(dwNumHandles > 1)
	{
		while((eventSignalled = 
			WaitForMultipleObjectsEx(dwNumHandles, &events[0], FALSE, INFINITE, TRUE)) 
			!= dwExitEventPosition)
		{
			// The wait was ended by the 10 minute timer routine
			// we don't need to do anything as Do10MinSend has already completed
			// so just go back to waiting on the events
			if(eventSignalled == WAIT_IO_COMPLETION) continue;

			eventSignalled -= WAIT_OBJECT_0;
			Log& log = *(logs.begin() + eventSignalled);
			if(LogCatchUp(log)) // uses default event holding size
			{
				// the log was refreshed so we need to recreate the 
				// wait event for the log
				log.SetupEvent(); 
				events[eventSignalled] = log.GetWriteEvent();
			}
			log.ResetWriteEvent();

			// A 1 second wait time is specified so we don't go into the 
			// empty block and unnessecarily use the cpu too often
			//
			// The event gets signalled again when our service recieves a 
			// SERVICE_CONTROL_CONTINUE notification
			//
			// If it is signalled (i.e. we're not paused) then execution
			// continues without waiting for the 1 second to elapse
			while(WaitForSingleObject(hPauseEvent, 1000) != WAIT_OBJECT_0){}
		}
	}
	CloseHandle(hPauseEvent);
	CloseHandle(hExitEvent);
}

void LogManager::DoTimedWaitingMode(const DWORD dwSecondsToWait)
{
	HANDLE hTimer = CreateWaitableTimer(NULL, FALSE, L"EventLogMonTimer");
	HANDLE hExitEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonExitEvent");
	HANDLE hPauseEvent = OpenEvent(SYNCHRONIZE, FALSE, L"EventLogMonPauseEvent");

	if(hTimer != NULL)
	{
		const HANDLE hWaitObjects[2] = {hTimer, hExitEvent};
		LARGE_INTEGER li = {0};
		li.QuadPart = Int32x32To64(dwSecondsToWait, -10000000); // Fire every dwSecondsToWait seconds in the future
		SetWaitableTimer(hTimer, &li, dwSecondsToWait * 1000, NULL, NULL, FALSE);
		DWORD handleSignalled = 0;

		while((handleSignalled = 
			WaitForMultipleObjectsEx(2, hWaitObjects, FALSE, INFINITE, TRUE))
			!= WAIT_OBJECT_0 + 1)
		{
			if(handleSignalled == WAIT_IO_COMPLETION) continue;
			CatchUp();
			while(WaitForSingleObject(hPauseEvent, 1000) != WAIT_OBJECT_0){}
		}
		// If we're here the exit event fired and all reading has finished
		CancelWaitableTimer(hTimer);
		CloseHandle(hTimer);
	}
	CloseHandle(hPauseEvent);
	CloseHandle(hExitEvent);
}

void CALLBACK LogManager::Do10MinSend(LPVOID lpContext, DWORD dwLowTime, DWORD dwHighTime)
{
	LogManager* logMan = static_cast<LogManager*>(lpContext);

	std::for_each(logMan->logs.begin(), logMan->logs.end(), boost::bind(&LogManager::WriteLogXML, logMan, _1));

	logMan->ResetLogFile();
	HANDLE hSendEvent = logMan->GetSendEvent();
	// Make sure we only send one lot of event files at a time
	if(WaitForSingleObject(hSendEvent, 50) == WAIT_OBJECT_0)
	{
		ResetEvent(hSendEvent);
		SendFilesToServer();
	}

	UNREFERENCED_PARAMETER(dwLowTime);
	UNREFERENCED_PARAMETER(dwHighTime);
}

void LogManager::WriteLogXML(Log& log) const
{
	*file << log;
	log.ClearEvents(); // We've written them to file so we don't need them anymore
	log.WriteLastRecordToRegistry();
}

void LogManager::FlushLastReadMessages() const
{
	std::for_each(logs.begin(), logs.end(), boost::bind(&Log::WriteLastRecordToRegistry, _1));
}

void LogManager::ResetLogFile()
{
	file.reset(new EventFile(GENERIC_READ | GENERIC_WRITE));
}

void LogManager::ClearAllEvents()
{
	std::for_each(logs.begin(), logs.end(), boost::bind(&Log::ClearEvents, _1));
}

void LogManager::FindAndOpenLogs(HUSKEY hBaseKey, DWORD dwTypesToLog)
{
	StringCollection names;
	LogEnumerator logEnum(hBaseKey, names, logs, dwTypesToLog);
	std::for_each(names.begin(), names.end(), logEnum);
}
