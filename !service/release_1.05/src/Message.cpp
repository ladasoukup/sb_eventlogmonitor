#include "StdAfx.h"
#include "Message.h"
#include <sstream>
#include <algorithm>
#include <memory>
#include <boost/scoped_array.hpp>
#include "SystemInformation.h"
#include "Errors.h"

DllLocations Message::dllPaths; // defaultly initialise static member

Message::Message(const std::wstring& source, const std::wstring& location, DWORD dwEventID, std::vector<DWORD_PTR>& insertOffsets, std::wstring& details, Category& cat) : location(location), eventSource(source), eventID(dwEventID), insertionOffsets(insertOffsets)
{
	regKey = L"System\\CurrentControlSet\\Services\\EventLog\\";
	regKey += location;
	regKey += L"\\";
	regKey += eventSource;
	regKey += L"\\";

	std::wstring messageDll;
	DllLocations::iterator iter = dllPaths.find(eventSource), iterEnd = dllPaths.end();
	StringCollection categoryDlls, eventDlls;

	if(iter != iterEnd)
	{
		eventDlls = iter->second.event;
		categoryDlls = iter->second.category;
	}
	else
	{
		GetDllPath(regKey, L"EventMessageFile", messageDll);
		if(!CheckAndExplodeDelimitedDlls(messageDll, eventDlls, L';'))
		{
			if(!messageDll.empty())
			{ // messageDll contains one dll path
				eventDlls.push_back(messageDll);
			}
		}
		std::wstring unexplodedCategory;
		GetDllPath(regKey, L"CategoryMessageFile", unexplodedCategory);
		if(!CheckAndExplodeDelimitedDlls(unexplodedCategory, categoryDlls, L';'))
		{
			if(!unexplodedCategory.empty())
			{ // unexplodedCategory contains one dll path
				categoryDlls.push_back(unexplodedCategory);
			}
		}
	}

	if(!eventDlls.empty())
	{
		if(!FormatFullMessage(eventDlls))
		{
			details.erase(std::remove(details.begin(), details.end(), L','), details.end());
			message = details;
		}
	}
	else 
	{
		//If there isn't a matching DLL string for this event, just dump the prettified details

		details.erase(std::remove(details.begin(), details.end(), L','), details.end());
		message = details;
	}

	for(StringCollection::size_type index = 0; index < categoryDlls.size(); ++index)
	{
		GrabFormatMessageOutput(categoryDlls[index], cat.id, false, cat.name);
	}

	if(iter == iterEnd)
	{
		SourceDlls dlls;
		dlls.event = eventDlls;
		dlls.category = categoryDlls;
		dllPaths[eventSource] = dlls; 
		// create an entry in the map so next time we encounter te event source we already have the Dlls in memory
	}
}

void Message::GetDllPath(std::wstring& regSubKey, const std::wstring& szDllType, std::wstring& messageDll)
{
	// Querying the registry this way allows for arbitrary string lengths
	// Whereas the old way (now in the else clause) has a limit of MAX_PATH * 4 characters
	HKEY hRegKey = NULL;
	LSTATUS res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regSubKey.c_str(), 0, KEY_READ, &hRegKey);
	if(!res)
	{
		std::wstring buf;
		SystemInformation::GetRegString(hRegKey, szDllType, buf);
		if(!buf.empty())
		{
			if(buf.find(L";") != std::wstring::npos)
			{
				buf.push_back(L';');
			}
			// Check if the there are environment variables that need to be replaced
			if(buf.find(L"%") != std::wstring::npos)
			{
				DWORD dwNewSize = ExpandEnvironmentStrings(buf.c_str(), &messageDll[0], 0);
				messageDll.resize(dwNewSize);
				ExpandEnvironmentStrings(buf.c_str(), &messageDll[0], dwNewSize);
			}
			else
			{
				messageDll = buf;
			}
		}
		RegCloseKey(hRegKey);
	}
}

bool Message::CheckAndExplodeDelimitedDlls(std::wstring& messageDlls, StringCollection& retDlls, const WCHAR delimiter)
{
	std::wstring::size_type delimiterPos = 0, lastDelimiterPos = 0;
	if((delimiterPos = messageDlls.find(delimiter)) != std::wstring::npos)
	{
		while(delimiterPos < std::wstring::npos)
		{
			retDlls.push_back(std::wstring(messageDlls, lastDelimiterPos, delimiterPos - lastDelimiterPos));
			++delimiterPos;
			lastDelimiterPos = delimiterPos;
			delimiterPos = messageDlls.find(delimiter, delimiterPos);
		}
		return true;
	}
	else return false;
}

bool Message::FormatFullMessage(StringCollection& dlls)
{
	bool inserts = false;
	if(insertionOffsets.size() > 0)
	{
		inserts = true;
	}

	std::wstring outputMessage; // buffer for details
	
	for(StringCollection::size_type i = 0; i < dlls.size(); ++i)
	{
		GrabFormatMessageOutput(dlls[i], eventID, inserts, outputMessage);
	}

	//  initialized to -1 so the first time the while loop is executed offset plus 1 = 0
	std::wstring::size_type offset = static_cast<std::wstring::size_type>(-1);
	DWORD i = 0;
	StringOffset off;
	while((offset = outputMessage.find(L"%%", offset + 1)) != std::wstring::npos)
	{
		off.offsetInString = offset;
		off.offsetPointer = reinterpret_cast<DWORD_PTR>(&outputMessage[offset]);
		parameterOffsets.push_back(off);
		++i;
	}
	if(i != 0)
	{
		std::vector<Replacement> insertStrings;
		GetParameterStrings(i, insertStrings);
		signed int iter = static_cast<signed int>(insertStrings.size() - 1);

		// Iterating backward through here so the inserted strings don't get overwritten.
		// This occurs when a string is sufficiently long as to go past the next strings'
		// starting offset
		for(; iter >= 0; --iter)
		{
			outputMessage.replace(parameterOffsets[iter].offsetInString, insertStrings[iter].lengthToReplace, insertStrings[iter].string);
		}
	}
	// Remove the characters which break the XML tree (angle brackets, ampersands etc)
	message.resize(outputMessage.size());
	std::remove_copy_if(
		outputMessage.begin(),
		outputMessage.end(),
		message.begin(),
		IsNonXmlChar
		);
	return true;
}

bool Message::GrabFormatMessageOutput(const std::wstring& messageDll, const DWORD dwMessageID, bool useInsertionStrings, std::wstring& outputString) const
{
	bool success = false;
	HMODULE hMessageLib = LoadLibraryEx(messageDll.c_str(), NULL, LOAD_LIBRARY_AS_DATAFILE);
	DWORD dwFlags = FORMAT_MESSAGE_FROM_HMODULE | FORMAT_MESSAGE_ALLOCATE_BUFFER;
	if(hMessageLib != NULL)
	{
		va_list* inserts = NULL;
		if(useInsertionStrings == true)
		{
			inserts = const_cast<va_list*>(
					  reinterpret_cast<const va_list*>(&insertionOffsets[0]) // the insertionOffsets member is const due to the function being const
					  );
			dwFlags |= FORMAT_MESSAGE_ARGUMENT_ARRAY;
		}
		else dwFlags |= FORMAT_MESSAGE_IGNORE_INSERTS;
		WCHAR* buf = NULL;
		if(GetModuleMessage(hMessageLib, dwFlags, dwMessageID, buf, inserts) == 0)
		{
			outputString += buf;
			LocalFree(buf);
			success = true;
		}
		FreeLibrary(hMessageLib);
	}
	return success;
}

void Message::GetParameterStrings(const DWORD count, std::vector<Replacement>& outputStrings)
{
	std::wstring filter(L"ParameterMessageFile");
    std::wstring parameterDll;
	GetDllPath(regKey, filter, parameterDll);
	boost::scoped_array<DWORD> paramBuf(new (std::nothrow) DWORD[count]);
	DWORD* parameterNumbers = paramBuf.get();

	for(DWORD i = 0; i < count; ++i)
	{
		std::wstringstream converter, num;
		converter << (reinterpret_cast<WCHAR*>(parameterOffsets[i].offsetPointer) + 2); // get the digits after the "%%"
		converter >> parameterNumbers[i];
		num << parameterNumbers[i];

		Replacement replacement;
		replacement.lengthToReplace = num.str().length() + 2; // the length of the digits + 2 for the "%%"
		GrabFormatMessageOutput(parameterDll, parameterNumbers[i], false, replacement.string);
		outputStrings.push_back(replacement);
	}
}

BOOL GetModuleMessage(HMODULE hMessageLib, DWORD dwFlags, DWORD dwMessageID, LPWSTR& buf, va_list* inserts) //throws RuntimeErrorW
{
	BOOL bRes = FALSE;
	__try
	{
		bRes = (FormatMessage(dwFlags, hMessageLib, dwMessageID, 0, reinterpret_cast<LPWSTR>(&buf), 1, inserts) == 0);
	}
	__except(GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION)
	{
		if(hMessageLib) FreeLibrary(hMessageLib);
		Errors::ThrowException(L"FormatMessage caused an access violation");
	}
	return bRes;
}
