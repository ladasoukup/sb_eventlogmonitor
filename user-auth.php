<?php
if (!isset($_SESSION["evtlog_user"])) $_SESSION["evtlog_user"] = "";
if (!isset($_SESSION["evtlog_pass"])) $_SESSION["evtlog_pass"] = "";
if (!isset($_SESSION["evtlog_user_group"])) $_SESSION["evtlog_user_group"] = "";

if (!empty($_POST["evtlog_pass"])) {
	$_SESSION["evtlog_user_group"] = "";
	$_SESSION["evtlog_user"] = $_POST["evtlog_user"];
	$_SESSION["evtlog_pass"] = $_POST["evtlog_pass"];
	// TRANSLATION TABLE
	$smarty->load_filter('output','i18n');
	$core->GetTranslations();
	if (empty($GLOBALS['_i18n_LangTable_'][$langID])) $langID="cs";
	$core->LoadTranslationTable($langID);
	$smarty->assign("langID", $langID);
	if (user_authenticate($_SESSION["evtlog_user"], $_SESSION["evtlog_pass"], true)) {
		$smarty->display("!login_redir.tpl", $langID);
	} else {
		$smarty->display("!login_error.tpl", $langID);
	}
	die("");
}

if (user_authenticate($_SESSION["evtlog_user"], $_SESSION["evtlog_pass"], true)) {
	// OK ;)
} else {
	// YOU MUST LOGIN...
	
	// TRANSLATION TABLE
	$smarty->load_filter('output','i18n');
	$core->GetTranslations();
	if (empty($GLOBALS['_i18n_LangTable_'][$langID])) $langID="cs";
	$core->LoadTranslationTable($langID);
	$smarty->assign("langID", $langID);
	
	$smarty->assign("version", "");
	$smarty->display("!login.tpl", $langID);
	die("");
}


function user_authenticate($user, $pass, $set_groups = false) {
	// the stuff below is just an example useage that restricts
	// user names and passwords to only alpha-numeric characters.
	if(!ctype_alnum($user)){
		// invalid user name
		return FALSE;
	}
	
	if(!ctype_alnum($pass)){
		// invalid password
		return FALSE;
	}
	
	// get the information from the htpasswd file
	$pass_file = "./.htpasswd";
	if(file_exists($pass_file) && is_readable($pass_file)){
		// the password file exists, open it
		if($fp=fopen($pass_file,'r')){
			while($line=fgets($fp)){
				// for each line in the file remove line endings
				$line=preg_replace('`[\r\n]$`','',$line);
				@list($fuser, $fpass, $fgroup) = explode(':',$line);
				if ($fuser == $user) {
				// the submitted user name matches this line
					// in the file
					// the salt is the first 2
					// characters for DES encryption
					$salt=substr($fpass,0,2);
						
					// use the salt to encode the
					// submitted password
					$test_pw = crypt($pass, $salt);
						
					if ($test_pw == $fpass) {
						// authentication success.
						fclose($fp);
						if ($set_groups) $_SESSION["evtlog_user_group"] = $fgroup;
						return TRUE;
					}else{
						return FALSE;
					}
				}
			}
			fclose($fp);
		}else{
			// could not open the password file
			return FALSE;
		}
	}else{
		return FALSE;
	}
}
?>