<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

$tables_A = array(DB_PREFIX."alerts", DB_PREFIX."cfg", DB_PREFIX."computers", DB_PREFIX."cron", DB_PREFIX."events", DB_PREFIX."filters", DB_PREFIX."queue_mail");
foreach ($tables_A as $table) {
	$core->Debug("* Optimize table \"" . $table . "\"");
	$db->query("OPTIMIZE TABLE ".$table);
	$db->query("FLUSH TABLE ".$table);
}
?>