<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");
include_once "./modules_cron/mail_alert_queue.php";

if (!empty($cron["cron_params"])) {
	$send_max = $cron["cron_params"];
} else {
	$send_max=15;
}

$max_errors = 5;
$smtp_host = $core->GetConfig("mail_smtp_host");
$smtp_port = $core->GetConfig("mail_smtp_port");
$smtp_auth = false;
if ($core->GetConfig("mail_smtp_auth") == "true") $smtp_auth = true;
$smtp_user = $core->GetConfig("mail_smtp_user");
$smtp_pass = $core->GetConfig("mail_smtp_pass");

include_once "./modules_cron/mail_send/htmlMimeMail5.php";

// Find all dead emails
$sql=$db->get_results("SELECT * FROM ".DB_PREFIX."queue_mail WHERE error_count>'".$max_errors."'",ARRAY_A);
if (!empty($sql)){
	foreach($sql as $data){
		$core->MailQueue($data["mail_to"], $data["mail_from"], "UNDELIVERED - " . $data["mail_subject"], "mail_queue_error", array("mail_orig" => $data["mail_text"], "mail_to" => $data["mail_to"]), date("Y-m-d H:i:s"), 80);
		$db->query("DELETE FROM ".DB_PREFIX."queue_mail WHERE id='".$data["id"]."'");
		echo "Undelivered mail: ".$data["mail_from"]." -- ".$data["mail_to"]."<br />\n";
	}
}
// SENDING EMAILs
$sql=$db->get_results("SELECT * FROM ".DB_PREFIX."queue_mail WHERE mail_date<'".NOW_DT."' ORDER BY mail_date ASC, mail_priority ASC LIMIT ".$send_max."",ARRAY_A);
if (!empty($sql)){
	foreach($sql as $data){
		@ini_set("sendmail_from",$data["mail_from"]);
		$mail = new htmlMimeMail5();
		$mail->setSMTPParams($smtp_host, $smtp_port, null, $smtp_auth, $smtp_user, $smtp_pass);

		$mail->SetFrom($data["mail_from"]);
		$mail->SetSubject(iconv(PageCharSet, "UTF-8", $data["mail_subject"]));
		$mail->setHTML(iconv(PageCharSet, "UTF-8", $data["mail_text"]), "./");
		$mail->setText(strip_tags(iconv(PageCharSet, "UTF-8", $data["mail_text"])));
		if ($data["mail_priority"]<25) $mail->SetHeader("X-Priority","1 (Highest)");
		if ($data["mail_priority"]>75) $mail->SetHeader("X-Priority","5 (Lowest)");
		$mail->SetHeader("X-Mailer", "SB EventLog Monitor QUEUE MAILER via PHP/" . phpversion());
		echo "Sending msg. id: ".$data["id"]." to ".$data["mail_to"]."...";
		if ($mail->send(array($data["mail_to"]), "smtp")){
			$return="OK";
			echo "OK<br />\n";
			$db->query("DELETE FROM ".DB_PREFIX."queue_mail WHERE id='".$data["id"]."'");
		} else {
			$return="error";
			echo "Failed!<br />\n";
			$date=date("Y-m-d H:i:s",time()+(60*10)-10);
			$db->query("UPDATE ".DB_PREFIX."queue_mail SET mail_date='".$date."', error_count='".($data["error_count"]+1)."' WHERE id='".$data["id"]."'");
		}
		unset($mail);
		flush();
	}
}
?>