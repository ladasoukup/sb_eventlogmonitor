<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

$query_data = array(date("Y-m-d H:i:s"));
$query = $safesql->query("SELECT * FROM ".DB_PREFIX."queue_alert WHERE queue_wait_to<'%s'", $query_data);
$queue_alert = $db->get_results($query, ARRAY_A);

if (is_array($queue_alert)) {
	foreach($queue_alert as $alert) {
		if ($alert["queue_email_count"] > 0) {
			$email_data = array("filter_title" => $alert["queue_mail_title"], "evt_computer" => $alert["evt_computer"], "queue_email" => $alert["queue_email"]);
			$core->MailQueue($alert["queue_mail_from"], $alert["queue_mailto"], $alert["queue_mail_title"], "mail_filter_alert_queue", $email_data, date("Y-m-d H:i:s", time() - 30), 60);
		}
		$query = $safesql->query("DELETE FROM ".DB_PREFIX."queue_alert WHERE id=%i", array($alert["id"]));
		$db->query($query);
	}
}


?>