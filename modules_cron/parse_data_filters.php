<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

// Load Filters
$query = $safesql->query("SELECT * FROM ".DB_PREFIX."filters WHERE filter_active=1 ORDER BY filter_priority ASC, filter_title ASC", array());
$evt_filters = $db->get_results($query, ARRAY_A);
//$db->debug();

echo " * " . sizeof($evt_filters) . " active filter rules loaded.<br />\n";
for ($loop=0; $loop<sizeof($evt_filters); $loop++){
	if (!empty($evt_filters[$loop]["filter_data"]))
		$evt_filters[$loop]["filter_data"] = unserialize($evt_filters[$loop]["filter_data"]);
}
$core->DebugArray($evt_filters);

// Load and Build "computer x group" table
$query = $safesql->query("SELECT computer_name, computer_group FROM ".DB_PREFIX."computers", array());
$computer_group_raw = $db->get_results($query, ARRAY_A);
if (is_array($computer_group_raw)) {
	foreach($computer_group_raw as $raw_value) {
		$computer_group[$raw_value["computer_name"]] = $raw_value["computer_group"];
	}
}
$core->DebugArray($computer_group);

// Load Mail-From
$alert_mail_from = $core->GetConfig("mail_from");


function evtFilter_check($evt_data, &$is_noise, &$is_alert, &$is_email) {
	global $core, $db, $safesql, $smarty, $evt_filters, $computer_group, $alert_mail_from, $alert_mail_subject;
	
	// add "computer group"
	$evt_data["evt_computer_group"] = $computer_group[$evt_data["evt_computer"]];
	
	$core->Debug("<hr />");
	$core->Debug("<strong>NEW EVENT...</strong>");
	$core->DebugArray($evt_data);
	
	for ($loop=0; $loop<sizeof($evt_filters); $loop++){
		$rule = $evt_filters[$loop]["filter_data"];
		$match = true;
		$is_noise = false; $is_alert = false; $is_email = false;
		
		$core->Debug("<hr />");
		if (is_array($rule)) {
			$core->Debug("RULE \"".$evt_filters[$loop]["filter_title"]."\"");
			for($loop_r=0; $loop_r<sizeof($rule); $loop_r++) {
				//$core->DebugArray($rule[$loop_r]);
				$core->Debug("\"" . $evt_data[$rule[$loop_r]["evt_data_type"]] . "\"  <i>" .$rule[$loop_r]["evt_comparation"] . "</i>  \"" . $rule[$loop_r]["evt_data_value"] . "\"");
				
				$check_inner = false;
				if ($rule[$loop_r]["evt_rule_condition"] != "or") $match_OR = false;
				
				if ($rule[$loop_r]["evt_data_type"] == "evt_computer_group") {
					// GROUP COMPARE !!!
					if (($rule[$loop_r]["evt_comparation"] == "equal") || ($rule[$loop_r]["evt_comparation"] == "like")) {
						if ($evt_data[$rule[$loop_r]["evt_data_type"]] == $rule[$loop_r]["evt_data_value"]) {
							$check_inner = true;
						} else {
							$temp = "~" . $rule[$loop_r]["evt_data_value"] . "~";
							if (strpos($evt_data[$rule[$loop_r]["evt_data_type"]], $temp) !== false ) {
								$check_inner = true;
							}
						}
					} else if (($rule[$loop_r]["evt_comparation"] == "not_equal") || ($rule[$loop_r]["evt_comparation"] == "not_like")) {
						$check_inner = true;
						if ($evt_data[$rule[$loop_r]["evt_data_type"]] == $rule[$loop_r]["evt_data_value"]) {
							$check_inner = false;
						} else {
							$temp = "~" . $rule[$loop_r]["evt_data_value"] . "~";
							if (strpos($evt_data[$rule[$loop_r]["evt_data_type"]], $temp) !== false ) {
								$check_inner = false;
							}
						}
					} else {
						// NOT SUPPORTED!!!
						echo "Regex and Preg_match are NOT support for GROUPS<br />\n";
					}
					
					
				} else {
					// OTHER VARIABLES COMPARE...
					switch ($rule[$loop_r]["evt_comparation"]) {
						case "equal":
							if ($evt_data[$rule[$loop_r]["evt_data_type"]] == $rule[$loop_r]["evt_data_value"]) $check_inner = true;
							break;
						case "not_equal":
							if ($evt_data[$rule[$loop_r]["evt_data_type"]] != $rule[$loop_r]["evt_data_value"]) $check_inner = true;
							break;
						case "like":
							if (strpos($evt_data[$rule[$loop_r]["evt_data_type"]], $rule[$loop_r]["evt_data_value"]) !== false ) $check_inner = true;
							break;
						case "not_like":
							if (strpos($evt_data[$rule[$loop_r]["evt_data_type"]], $rule[$loop_r]["evt_data_value"]) === false ) $check_inner = true;
							break;
						case "regex":
							if (eregi($rule[$loop_r]["evt_data_value"], $evt_data[$rule[$loop_r]["evt_data_type"]]) !== false) $check_inner = true;
							break;
						case "preg_match":
							if (preg_match($rule[$loop_r]["evt_data_value"], $evt_data[$rule[$loop_r]["evt_data_type"]]) != false) $check_inner = true;  // 0 or FALSE
							break;
					}
				}
				
				if ($check_inner){
					$match = true;
					$match_OR = true;
				} else {
					if ($match_OR !== true) {
						$match = false;
						if ((isset($rule[($loop_r + 1)]["evt_rule_condition"])) && ($rule[($loop_r + 1)]["evt_rule_condition"] != "or")) {
							$core->Debug("---BREAK---");
							break;
						}
					}
				}
				
				if ((isset($rule[($loop_r + 1)]["evt_rule_condition"])) && ($rule[($loop_r + 1)]["evt_rule_condition"] == "or")) $core->debug("--OR--");
				
			}
		}
		
		if ($match) {
			if ($evt_filters[$loop]["filter_isNoise"]) $is_noise = true;
			if ($evt_filters[$loop]["filter_isAlert"]) $is_alert = true;
			if ($evt_filters[$loop]["filter_isEmail"]) $is_email = true;
			
			echo " * FILTER MATCH - \"" . $evt_filters[$loop]["filter_title"] . "\", action: ";
			echo "N-" . $is_noise . " A-" . $is_alert . " E-" . $is_email . "<br />\n"; flush();
			
			// SEND EMAIL
			if ($is_email) {
				$query_data = array($evt_filters[$loop]["id"], $evt_data["evt_computer"]);
				$query = $safesql->query("SELECT * FROM ".DB_PREFIX."queue_alert WHERE filter_id=%i AND evt_computer='%s' LIMIT 1", $query_data);
				$queue_alert = $db->get_row($query, ARRAY_A);
				
				$email_data = array("filter_title" => $evt_filters[$loop]["filter_title"]);
				$email_data = array_merge($email_data, $evt_data);
				$alert_mail_subject = "EventLog: " . $email_data["filter_title"] . " - " . $email_data["evt_computer"];
				
				if ((!empty($queue_alert)) AND ($evt_filters[$loop]["filter_email_ignore"] > 0 )) {
					// Add alert to ALERT QUEUE
					
					$mail_template = "mail_filter_match_event.tpl";
					$smarty->assign("mail_data", $email_data);
					$smarty->clear_cache($mail_template); // !!! IMPORTANT !!!
					$queue_email = $queue_alert["queue_email"] . $smarty->fetch($mail_template);
					
					$query_data = array($queue_email, ($queue_alert["queue_email_count"] + 1), $queue_alert["id"]);
					$query = $safesql->query("UPDATE ".DB_PREFIX."queue_alert SET queue_email='%s', queue_email_count=%i WHERE id=%i", $query_data);
					$db->query($query);
					
				} else {
					// Send email Alert
					$core->MailQueue($alert_mail_from, $evt_filters[$loop]["filter_mailto"], $alert_mail_subject, "mail_filter_match", $email_data, date("Y-m-d H:i:s", time() - 30), 50);
					if ($evt_filters[$loop]["filter_email_ignore"] > 0) {
						// Add recored to ALERT QUEUE
						$query_data = array($evt_filters[$loop]["id"], $evt_filters[$loop]["filter_title"], $evt_data["evt_computer"], $alert_mail_from, $evt_filters[$loop]["filter_mailto"], $alert_mail_subject, date("Y-m-d H:i:s", time() + (60 * $evt_filters[$loop]["filter_email_ignore"])));
						$query = $safesql->query("INSERT INTO ".DB_PREFIX."queue_alert SET filter_id=%i, filter_title='%s', evt_computer='%s', queue_mail_from='%s', queue_mailto='%s', queue_email='', queue_mail_title='%s', queue_email_count=0, queue_wait_to='%s'", $query_data);
						$db->query($query);
					}
				}
			}
			break;
		}
	}
}
?>