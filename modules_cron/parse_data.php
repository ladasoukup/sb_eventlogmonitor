<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

// FUNCTIONs
function EvtDateTime_convert($EvtDateTime) {
	global $core;
	
	$ret = date("Y-m-d H:i:s", mktime(substr($EvtDateTime, 8, 2), substr($EvtDateTime, 10, 2), substr($EvtDateTime, 12, 2), substr($EvtDateTime, 4, 2), substr($EvtDateTime, 6, 2), substr($EvtDateTime, 0, 4)));
	// $core->Debug("EvtDateTime_convert " . $EvtDateTime . " ==> " . $ret);
	return $ret;
}

function EvtTypeConvert($EvtType) {
	$ret = strtolower($EvtType);
	if ($ret == "1") $ret = "error";
	if ($ret == "2") $ret = "warning";
	if ($ret == "3") $ret = "information";
	if ($ret == "4") $ret = "audit_success";
	if ($ret == "5") $ret = "audit_failure";
	
	return $ret;
}
// FUNCTIONs (end)

$clear_events_null = false;
$clear_alerts = false;
$clear_events = false;

$XMLinDir = array();
$FilesInDir = scandir("./data");
foreach ($FilesInDir as $file) {
	if (substr($file, -8) == ".evt-xml") {
		$XMLinDir[] = "./data/" . $file;
	}
}

if (is_array($XMLinDir)) {
	include_once "./modules_cron/parse_data_filters.php";
	$clear_alerts = false;
	$clear_events_null = false;
	
	foreach($XMLinDir as $xml_path) {
		$clear_events = false;
		$curr_computer = "";
		list($curr_computer) = explode(".", str_replace("./data/", "", $xml_path));
		
		echo "Parsing file \"" . $xml_path . "\" <br />\n";
		$xml_data = $core->object2array(simplexml_load_file($xml_path));
		if(is_array($xml_data)) {
			// Computer OS
			$curr_computer_os = $xml_data["computer_os"];
			$curr_computer_os = str_replace("Microsoft ", "", $curr_computer_os);
			$query_data = array($curr_computer_os, $curr_computer);
			$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_os = '%s' WHERE computer_name = '%s'", $query_data);
			$db->query($query);
			
			// computer description
			$curr_computer_description = $xml_data["computer_description"];
			$query_data = array($curr_computer_description, $curr_computer);
			$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_description = '%s' WHERE computer_name = '%s'", $query_data);
			$db->query($query);
			
			// computer domain
			$curr_computer_domain = strtolower($xml_data["computer_domain"]);
			$query_data = array($curr_computer_domain, $curr_computer);
			$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_domain = '%s' WHERE computer_name = '%s'", $query_data);
			$db->query($query);

			// computer reported
			$curr_computer_last_reported = $xml_data["computer_last_reported"];
			$query_data = array($curr_computer_last_reported, $curr_computer);
			$query = $safesql->query("UPDATE ".DB_PREFIX."computers SET computer_last_reported = '%s' WHERE computer_name = '%s'", $query_data);
			$db->query($query);

			// SINGLE EVENT fix
			if (!empty($xml_data["event"]["time-generated"])) {
				$core->Debug(" * SINGLE EVENT - Fixing array");
				$temp = $xml_data["event"];
				unset($xml_data["event"]);
				$xml_data["event"][0] = $temp;
			}
			
			foreach($xml_data["event"] as $xml_event) {
				// $core->DebugArray($xml_event);
				$sql_data_A = array(
					"evt_computer" => $curr_computer,
					"evt_code" => $xml_event["code"],
					"evt_type" => EvtTypeConvert(iconv("UTF-8", PageCharSet, $xml_event["type"])),
					"evt_category" => iconv("UTF-8", PageCharSet, $xml_event["category"]),
					"evt_logfile" => iconv("UTF-8", PageCharSet, $xml_event["logfile"]),
					"evt_message" => iconv("UTF-8", PageCharSet, $xml_event["message"]),
					"evt_source" => iconv("UTF-8", PageCharSet, $xml_event["source"]),
					"evt_user" => iconv("UTF-8", PageCharSet, $xml_event["user"]),
					"evt_time_generated" => EvtDateTime_convert($xml_event["time-generated"])
				);
				
				if (($xml_event["time-generated"] != $xml_event["message"]) && ($xml_event["message"] != $xml_event["logfile"]))  {
					$is_noise = false; $is_alert = false; $is_email = false;
					evtFilter_check($sql_data_A, $is_noise, $is_alert, $is_email);
					$core->Debug("evtFilter_check returns... N-".$is_noise." A-".$is_alert." E-".$is_email);
					$sql_data = array(	$sql_data_A["evt_computer"],
										$sql_data_A["evt_code"],
										$sql_data_A["evt_type"],
										$sql_data_A["evt_category"],
										$sql_data_A["evt_logfile"],
										$sql_data_A["evt_message"],
										$sql_data_A["evt_source"],
										$sql_data_A["evt_user"],
										$sql_data_A["evt_time_generated"],
										$is_noise
								);
					$query = $safesql->query("INSERT INTO ".DB_PREFIX."events SET evt_computer='%s', evt_code=%i, evt_type='%s', evt_category='%s', evt_logfile='%s', evt_message='%s', evt_source='%s', evt_user='%s', evt_time_generated='%s', evt_noise=%i", $sql_data);
					$db->query($query);
					$clear_events = true;
					$clear_events_null = true;
					if ($is_alert) {
						$query = $safesql->query("INSERT INTO ".DB_PREFIX."alerts SET evt_computer='%s', evt_code=%i, evt_type='%s', evt_category='%s', evt_logfile='%s', evt_message='%s', evt_source='%s', evt_user='%s', evt_time_generated='%s', evt_noise=%i", $sql_data);
						$db->query($query);
						$clear_alerts = true;
					}
				}
			}
		}
		if ($clear_events) $smarty->clear_cache(null, "showEventLog|" . $curr_computer);
		unlink($xml_path);
	}
}
if ($clear_events_null) $smarty->clear_cache(null, "showEventLog|-NULL-");
if ($clear_alerts) $smarty->clear_cache(null, "showAlerts");
if (($clear_events) || ($clear_events_null) || ($clear_alerts)) $smarty->clear_cache(null, "showSummary");
if ($clear_alerts) $smarty->clear_cache(null, "rss");
?>