<?php
if (APP_TOKEN != "SB_ELM") die("ACCESS DENIED");

$db->query("TRUNCATE ".DB_PREFIX."autocomplete");
$ac_sources = array("evt_logfile", "evt_source", "evt_user", "evt_category", "evt_code");
if (is_array($ac_sources)) {
	foreach($ac_sources as $ac_source) {
		$sql_temp = $db->get_col("SELECT DISTINCT ".$ac_source." FROM ".DB_PREFIX."events LIMIT 1000", 0);
		sort($sql_temp);
		if (is_array($sql_temp)) { for ($loop=0; $loop<sizeof($sql_temp); $loop++) { $sql_temp[$loop] = "'". trim(str_replace("\\", "\\\\", $sql_temp[$loop])) . "'"; } }
		$actb_temp = implode(",", $sql_temp);
		$query_data = array($ac_source, serialize($actb_temp));
		$query = $safesql->query("INSERT INTO ".DB_PREFIX."autocomplete SET ac_field='%s', ac_values='%s'", $query_data);
		$db->query($query);
	}
}

?>